﻿/**
 * e_wechat Column
 * @author Administrator
 * 文件名 Extjs_Column_E_wechat
 * Columns名 column_e_wechat
 */

Ext.define('column_e_wechat', {
    columns : [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            width : 40
        }
        ,
        {
            header : '微信名称',
            dataIndex : 'name',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '微信号',
            dataIndex : 'account',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '访问参数',
            dataIndex : 'apiid',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '开发者ID',
            dataIndex : 'appid',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '开发者密码',
            dataIndex : 'appsecret',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '访问令牌',
            dataIndex : 'accesstoken',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '令牌',
            dataIndex : 'token',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '消息加解密密钥',
            dataIndex : 'encodingaeskey',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '备注',
            dataIndex : 'remark',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '操作人',
            dataIndex : 'addemployeename',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '新增时间',
            dataIndex : 'adddate',
            sortable : true,
            flex : 2,
			renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value);
			}
        }
    ]
});
