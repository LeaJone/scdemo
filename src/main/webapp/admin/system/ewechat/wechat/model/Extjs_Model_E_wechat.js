﻿/**
 * e_wechat Model
 * @author Administrator
 * 文件名     Extjs_Model_E_wechat
 * Model名    model_e_wechat
 */

Ext.define('model_e_wechat', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'name',
            type : 'string'
        }
        ,
        {
            name : 'account',
            type : 'string'
        }
        ,
        {
            name : 'apiid',
            type : 'string'
        }
        ,
        {
            name : 'appid',
            type : 'string'
        }
        ,
        {
            name : 'appsecret',
            type : 'string'
        }
        ,
        {
            name : 'accesstoken',
            type : 'string'
        }
        ,
        {
            name : 'token',
            type : 'string'
        }
        ,
        {
            name : 'encodingaeskey',
            type : 'string'
        }
        ,
        {
            name : 'remark',
            type : 'string'
        }
        ,
        {
            name : 'adddate',
            type : 'string'
        }
        ,
        {
            name : 'addemployeename',
            type : 'string'
        }
    ]
});
