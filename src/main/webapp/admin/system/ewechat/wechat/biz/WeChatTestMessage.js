﻿/**
 * 自助回复测试添加界面
 * @author Administrator
 */
Ext.define('system.ewechat.wechat.biz.WeChatTestMessage', {
	extend : 'Ext.window.Window',

	util : Ext.create('system.util.util'),
	
	initComponent:function(){
	    var me = this;
	    
	    var title = new Ext.create('system.widget.FsdTextField', {
	    	fieldLabel : '内容',
	    	labelAlign : 'right',
	    	labelWidth : 60,
	        width : 780,
	    	name : 'title'
	    });
	   
	    var abstracts = Ext.create('system.widget.FsdTextArea', {
	    	fieldLabel : '摘要',
	    	labelAlign : 'right',
	    	labelWidth : 60,
	        width : 780,
	    	rows : 16,
	    	name : 'abstracts'
	    });
	    
	    var form = Ext.create('Ext.form.Panel', {
			xtype : 'form',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [title, abstracts]
        });
	        
	    Ext.apply(this,{
	        width : 880,
	        height : 400,
			autoScroll : true,
	        bodyStyle : "background-color:#ffffff;", // 设置背景颜色 白色
	        resizable : false, // 改变大小
	        items : [form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '访问',
			    iconCls : 'acceptIcon',
			    handler : function() {
                    var pram = {content : title.getValue(), apiid : 'lzfsd'};
                    var param = {jsonData : Ext.encode(pram)};
                    me.util.ExtAjaxRequest('e_wechat/test_Message.do', param,
                    function(response, options, respText){
                    	abstracts.setValue(respText.data);
                    }, null, me);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});