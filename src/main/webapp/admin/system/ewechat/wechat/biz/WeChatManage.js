﻿/**
 * 微信账号管理界面
 * @author Administrator
 */
Ext.define('system.ewechat.wechat.biz.WeChatManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*', 
	             'system.widget.FsdButton',
	             'system.ewechat.wechat.model.Extjs_Column_E_wechat', 
	             'system.ewechat.wechat.model.Extjs_Model_E_wechat'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createTable()];
		me.callParent();
	},
	
    oper : Ext.create('system.ewechat.wechat.biz.WeChatOper'),
    
	createTable : function() {
	    var me = this;

		/**
		 * 查询文本框
		 */
		var queryText =  Ext.create("Ext.form.TextField", {
			name : 'queryParam',
			emptyText : '请输入微信名称',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});

		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_e_wechat', // 显示列
			model : 'model_e_wechat',
			baseUrl : 'e_wechat/load_data.do',
			border : false,
			tbar : [{
				xtype : "fsdbutton",
				text : "添加",
				iconCls : 'page_addIcon',
				popedomCode : 'wxzhbj',
				handler : function() {
					me.oper.add(grid);
				}
			}, {
				xtype : "fsdbutton",
				text : "修改",
				iconCls : 'page_editIcon',
				popedomCode : 'wxzhbj',
				handler : function() {
					me.oper.editor(grid);
				}
			}, {
				xtype : "fsdbutton",
				text : "删除",
				iconCls : 'page_deleteIcon',
				popedomCode : 'wxzhsc',
				handler : function() {
				    me.oper.del(grid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid);
				}
			},  '->', queryText, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			},/* {
				text : '测试',
				iconCls : 'page_deleteIcon',
				handler : function() {
					var win = Ext.create('system.ewechat.wechat.biz.WeChatTestMessage');
			        win.setTitle('自动回复测试');
			        win.modal = true;
			        win.show();
				}
			}*/]			
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  queryText.getValue();
			var pram = {name : name};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params, {jsonData : Ext.encode(pram)});
		});

		return grid;
	}
});