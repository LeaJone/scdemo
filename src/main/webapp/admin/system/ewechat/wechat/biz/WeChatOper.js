/**
 * 微信账号管理界面操作类
 * @author Administrator
 */
Ext.define('system.ewechat.wechat.biz.WeChatOper', {
	
	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 添加
     */
	add : function(grid){
	    var win = Ext.create('system.ewechat.wechat.biz.WeChatEdit');
        win.setTitle('微信账号添加');
        win.grid = grid;
        win.modal = true;
        win.show();
	},
	
	/**
     * 表单提交
     */
     formSubmit : function(formpanel, grid, isclose){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('e_wechat/save_WeChat_qx_wxzhbj.do' , formpanel.form , '正在提交数据,请稍候.....',
			function(form, action){
            	if (isclose)
            		formpanel.up('window').close();
			    grid.getStore().reload();
				Ext.MessageBox.alert('提示', '保存成功!');
			});
        }
     },
	
	/**
     * 修改
     */
	editor : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{
        	// 先得到主键
            var id = data[0].data.id;
            var win = Ext.create('system.ewechat.wechat.biz.WeChatEdit');
            win.setTitle('微信账号修改');
            win.grid = grid;
            win.modal = true;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm(), 'e_wechat/load_WeChatByid.do', param, null, 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},
 	
 	/**
     * 查看
     */
	view : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
	    if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要查看的项目！');
        }else if (data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能查看一个项目！');
        }else {
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.ewechat.wechat.biz.WeChatEdit');
            win.setTitle('微信账号查看');
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm(), 'e_wechat/load_WeChatByid.do', param, null, 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},
	
	/**
     * 删除
     */
	del : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要删除的项目！');
        }else {
        	Ext.MessageBox.show({
        		title : '询问',
        		msg : '您确定要删除该项目吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                   	    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
            			});
                        var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('e_wechat/del_WeChat_qx_wxzhsc.do', param,
                        function(response, options){
	                       	grid.getStore().reload();
                        }, null, me.form);
                    }
                }
            });
        }
	}
});