﻿/**
 * 微信账号添加界面
 * @author Administrator
 */
Ext.define('system.ewechat.wechat.biz.WeChatEdit', {
	extend : 'Ext.window.Window',
	requires : ['system.widget.FsdTreeComboBox'],
	grid : null,
	oper : Ext.create('system.ewechat.wechat.biz.WeChatOper'),
	initComponent : function(){
	    var me = this;
	    
	    var name = new Ext.create('system.widget.FsdTextField', {
	    	fieldLabel : '微信名称',
	    	labelAlign :'right',
	    	labelWidth : 120,
	        width : 340,
	    	name : 'name',
	    	maxLength : 25,
	    	allowBlank : false,
	    	blankText : '请输入微信名称',
	    	maxLengthText : '不能超过25个字符'
	    });
	    
	    var account = new Ext.create('system.widget.FsdTextField', {
	    	fieldLabel : '微信号',
	    	labelAlign : 'right',
	    	labelWidth : 120,
	        width : 340,
	    	name : 'account',
	    	maxLength : 50,
	    	allowBlank : true,
	    	blankText : '请输入微信号',
	    	maxLengthText : '不能超过50个字符'
	    });
	    
	    var apiid = new Ext.create('system.widget.FsdTextField', {
	    	fieldLabel : '访问参数',
	    	labelAlign : 'right',
	    	labelWidth : 120,
	        width : 340,
	    	name : 'apiid',
	    	maxLength : 50,
	    	allowBlank : false,
	    	blankText : '请输入访问参数',
	    	maxLengthText : '不能超过50个字符'
	    });
	    
	    var appid = new Ext.create('system.widget.FsdTextField', {
	    	fieldLabel : '开发者ID',
	    	labelAlign : 'right',
	    	labelWidth : 120,
	        width : 340,
	    	name : 'appid',
	    	maxLength : 40,
	    	allowBlank : false,
	    	blankText : '请输入开发者ID',
	    	maxLengthText : '不能超过40个字符'
	    });
	    
	    var appsecret = new Ext.create('system.widget.FsdTextField', {
	    	fieldLabel : '开发者密码',
	    	labelAlign : 'right',
	    	labelWidth : 120,
	        width : 340,
	    	name : 'appsecret',
	    	maxLength : 40,
	    	allowBlank : false,
	    	blankText : '请输入开发者密码',
	    	maxLengthText : '不能超过40个字符'
	    });
	    
	    var accesstoken = new Ext.create('system.widget.FsdTextField', {
	    	fieldLabel : '访问令牌',
	    	labelAlign : 'right',
	    	labelWidth : 120,
	        width : 340,
	    	name : 'accesstoken',
	    	maxLength : 150,
	    	allowBlank : true,
	    	blankText : '请输入访问令牌',
	    	maxLengthText : '不能超过150个字符'
	    });
	    
	    var token = new Ext.create('system.widget.FsdTextField', {
	    	fieldLabel : '令牌',
	    	labelAlign : 'right',
	    	labelWidth : 120,
	        width : 340,
	    	name : 'token',
	    	maxLength : 50,
	    	allowBlank : false,
	    	blankText : '请输入令牌',
	    	maxLengthText : '不能超过50个字符'
	    });
	    
	    var encodingaeskey = new Ext.create('system.widget.FsdTextField', {
	    	fieldLabel : '消息加解密密钥',
	    	labelAlign : 'right',
	    	labelWidth : 120,
	        width : 340,
	    	name : 'encodingaeskey',
	    	maxLength : 50,
	    	allowBlank : true,
	    	blankText : '请输入消息加解密密钥',
	    	maxLengthText : '不能超过50个字符'
	    });
	   
	    var remark = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel : '备注',
	    	labelAlign : 'right',
	    	labelWidth : 120,
	        width : 340,
	    	name : 'remark',	    	
	    	maxLength : 50,
	    	allowBlank : true,
	    	blankText : '请输入备注',
	    	maxLengthText : '不能超过50个字符'
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [{
                name : "id",
                xtype : "hidden"
            }, name, account, apiid, appid, appsecret, accesstoken, token, encodingaeskey, remark]
        });
	        
	    Ext.apply(this, {
	        width : 440,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;", // 设置背景颜色 白色
	        resizable : false, // 改变大小
	        items : [form],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'wxzhbj',
	        	name : 'btnsave',
			    text : '　保存　',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.formSubmit(form, me.grid, true);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});