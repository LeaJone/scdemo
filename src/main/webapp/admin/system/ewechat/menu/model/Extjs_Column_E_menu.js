﻿/**
 *e_menu Column
 *@author CodeSystem
 *文件名     Extjs_Column_E_menu
 *Columns名  column_e_menu
 */

Ext.define('column_e_menu', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            width : 40
        }
        ,
        {
            header : '编码',
            dataIndex : 'code',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '菜单名称',
            dataIndex : 'name',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '菜单类型',
            dataIndex : 'typename',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '跳转链接',
            dataIndex : 'aurl',
            sortable : true,
            flex : 3
        }
        ,
        {
            header : '顺序号',
            dataIndex : 'sort',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '备注',
            dataIndex : 'remark',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '操作人',
            dataIndex : 'addemployeename',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '新建时间',
            dataIndex : 'adddate',
            sortable : true,
            flex : 2,
			renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value);
			}
        }
    ]
});
