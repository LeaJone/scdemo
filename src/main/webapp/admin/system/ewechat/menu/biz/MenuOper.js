/**
 * 微信菜单操作类
 * @author lw
 */

Ext.define('system.ewechat.menu.biz.MenuOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 添加
     */
	add : function(grid , treepanel, wxid){
	    var win = Ext.create('system.ewechat.menu.biz.MenuEdit', {
	    	title : '微信菜单添加',
	        grid : grid,
	        treepanel : treepanel,
	        wxid : wxid,
	        modal : true
	    });
        win.show();
	},
	
	/**
     * 表单提交
     */
     formSubmit : function(formpanel , grid , treepanel){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('e_menu/save_Menu_qx_wxcdbj.do', formpanel.form, '正在提交数据,请稍候.....',
			function(form, action){
			    formpanel.up('window').close();
			    grid.getStore().reload();
			    treepanel.getStore().reload();
				Ext.MessageBox.alert('提示', '保存成功！');
			});
        }
     },
     
     /**
      * 删除
      */
     del : function(grid , treepanel){
    	 var me = this;
 		 // 获取选中的行
 	     var data = grid.getSelectionModel().getSelection();
         if (data.length == 0) {
             Ext.MessageBox.alert('提示', '请先选中要删除的项目！');
         }else {
        	 Ext.MessageBox.show({
        		 title : '询问',
        		 msg : '您确定要删除吗?',
 			     width : 250,
 				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                     if (btn == 'yes') {
                    	 var dir = new Array();　
                         Ext.Array.each(data, function(items) {
                             var id = items.data.id;
                             dir.push(id);
             			});
             			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('e_menu/del_Menu_qx_wxcdsc.do', param,
                        function(response, options){
                        	grid.getStore().reload();
            			    treepanel.getStore().load();
                        }, null, me.form);
                     }
                 }
             });
         }

 	},
 	
 	/**
     * 修改
     */
	editor : function(grid, treepanel, wxid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{

            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            
            var win = Ext.create('system.ewechat.menu.biz.MenuEdit');
            win.setTitle('微信菜单修改');
            win.modal = true;
            win.grid = grid;
            win.treepanel =  treepanel;
            win.wxid = wxid;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'e_menu/load_MenuByid.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},
 	
 	/**
     * 查看
     */
	view : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            
            var win = Ext.create('system.ewechat.menu.biz.MenuEdit');
            win.setTitle('微信菜单查看');
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'e_menu/load_MenuByid.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},
	
	/**
	 * 加载微信平台菜单
	 */
	loadMenuTree : function(menupanel, wxid){
	    var me = this;
	    var pram = {wxid : wxid};
        var param = {jsonData : Ext.encode(pram)};
	    me.util.ExtAjaxRequest('e_menu/load_WeChatMenu.do', param,
	    function(response, options, respText){
	    	if(respText.data != ""){
	    		menupanel.getRootNode().removeAll();
	    		menupanel.getRootNode().appendChild(respText.data);
	    		menupanel.expandAll();
	    	}
	    },
	    function(response, options){
	    }, menupanel);
	},
	
	/**
	 * 同步微信平台菜单
	 */
	synchroMenuTree : function(wxid, form, menupanel){
	    var me = this;
	    var pram = {wxid : wxid};
        var param = {jsonData : Ext.encode(pram)};
	    me.util.ExtAjaxRequest('e_menu/synchro_WeChatMenu.do', param,
	    function(response, options, respText){
            Ext.MessageBox.alert('提示', '微信平台菜单同步成功！');
            me.loadMenuTree(menupanel, wxid);
	    },
	    function(response, options){
	    }, form);
	}
	
});