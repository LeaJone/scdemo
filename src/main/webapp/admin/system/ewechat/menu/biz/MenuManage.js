﻿/**

 * 微信菜单管理界面
 * @author lw
 */

Ext.define('system.ewechat.menu.biz.MenuManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.ewechat.menu.model.Extjs_Column_E_menu',
	             'system.ewechat.menu.model.Extjs_Model_E_menu',
	             'system.admin.tools.model.Extjs_Column_SortOperate',
		         'system.admin.tools.model.Extjs_Model_SortOperate'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.ewechat.menu.biz.MenuOper'),
	
	createContent : function(){
		var me = this;
		
		var queryid = '';
		var wxid = '';
		
		/**
		 * 左边的树面板
		 */
		var menupanel = Ext.create("system.widget.FsdTreePanel", {
			icon : 'resources/admin/icon/chart_organisation.png',
			baseUrl : 'e_menu/load_WeChatMenu.do',
			title: '微信平台菜单',
			rootText : '微信菜单',
			rootId : me.oper.util.getParams("company").id,
			region : 'west', // 设置方位
			collapsible : true,//折叠
			width: 250,
			split: true,
	        zTreeRoot : 'children',//一次性加载
			disabled : true
		});
		
		/**
		 * 左边的树面板
		 */
		var treepanel = Ext.create("system.widget.FsdTreePanel", {
			icon : 'resources/admin/icon/chart_organisation.png',
			baseUrl : 'e_menu/load_AllMenuTree.do',
			title: '本地菜单',
			rootText : '自定义菜单',
			region : 'west', // 设置方位
			collapsible : true,//折叠
			width: 182,
			split: true,
	        zTreeRoot : 'children',//一次性加载
			disabled : true
		});
		
		
		var queryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入菜单名称',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});
		
		var wechat =  Ext.create("system.widget.FsdComboBoxData" , {
			name : 'wechat',
			fieldLabel : '选择微信号',
	    	labelAlign:'right',
	    	labelWidth:80,
			width : 350,
			zBaseUrl : 'e_wechat/load_data.do',
			zValueField : 'id',
		    zDisplayField : 'name'
		});
		
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_e_menu', // 显示列
			model: 'model_e_menu',
			baseUrl : 'e_menu/load_data.do',
			disabled : true,
			zAutoLoad : false,
			tbar : [{
				xtype : "fsdbutton",
				text : "添加",
				iconCls : 'page_addIcon',
				popedomCode : 'wxcdbj',
				handler : function(button) {
					me.oper.add(grid , treepanel, wxid);
				}
			}, {
				xtype : "fsdbutton",
				text : "修改",
				iconCls : 'page_editIcon',
				popedomCode : 'wxcdbj',
				handler : function(button) {
					me.oper.editor(grid , treepanel, wxid);
				}
			}, {
				xtype : "fsdbutton",
				text : "删除",
				iconCls : 'page_deleteIcon',
				popedomCode : 'wxcdsc',
				handler : function(button) {
				    me.oper.del(grid , treepanel);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid);
				}
			}, {
				text : '排序',
				iconCls : 'up',
				handler : function() {
					var name =  queryText.getValue();
					var pram = {fid : queryid , name : name};
					me.oper.util.sortOperate(
							grid, 
							'column_name', 
							'model_sortoperate',
							grid.baseUrl,
							'e_menu/sort_Menu_qx_wxcdbj.do',
							function(){return pram;},
				        	function(){treepanel.getStore().load();});
				}
			}, '-', {
				xtype : "fsdbutton",
				text : '同步菜单',
				iconCls : 'up',
				popedomCode : 'wxcdtb',
				handler : function() {
					me.oper.synchroMenuTree(wxid, me, menupanel);
				}
			}, '->', queryText , {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  queryText.getValue();
			var pram = {fid : queryid , name : name};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		var contentpanel = Ext.create("Ext.panel.Panel", {
			frame:true,
			split: true,
			region: 'center',
			layout: 'fit',
			title: '菜单信息管理',
			items: [grid]
		});

		var page1_jExtPanel1_obj = new Ext.panel.Panel({
			bodyPadding: 3,
			header : false,
			border : 0,
			split: true,
			layout: 'border',
			items: [menupanel, treepanel, contentpanel],
			tbar : [
			    wechat ,{
				text : '加载',
				iconCls : 'magnifierIcon',
				listeners: {
					click : function(obj, e, eOpts ){
						if (obj.getText() == "加载"){
							if (wechat.validate()){
								wxid = wechat.getValue();
								queryid = wxid;
								var pram = {wxid : wxid};
								treepanel.zTreeParams = pram;
								treepanel.setRootId(wxid);
								treepanel.setDisabled(false);
								treepanel.getStore().reload();
								treepanel.expandAll();
								grid.setDisabled(false);
								grid.getStore().loadPage(1);
								menupanel.zTreeParams = pram;
								menupanel.setRootId(wxid);
								menupanel.setDisabled(false);
								menupanel.getStore().reload();
								wechat.setDisabled(true);
								obj.setText("重置");
							}
						}else{
							treepanel.setDisabled(true);
							grid.getStore().removeAll();
							grid.setDisabled(true);
							menupanel.setDisabled(true);
							wechat.setDisabled(false);
							obj.setText("加载");
						} 
	                }
	        	}
			}, {
				text : '微信菜单刷新',
				iconCls : 'tbar_synchronizeIcon',
				listeners: {
					click : function(obj, e, eOpts ){
						if (!menupanel.disabled){
							menupanel.getStore().reload();
						}
	                }
	        	}
			}]
		});
		
		treepanel.on('itemclick', function(view,record,item,index,e){
			queryid = record.raw.id;
			grid.getStore().loadPage(1);
		});
		return page1_jExtPanel1_obj;
	}
});