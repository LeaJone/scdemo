﻿/**
 * 微信菜单添加界面
 * @author lw
 */

Ext.define('system.ewechat.menu.biz.MenuEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	treepanel : null,
	wxid : null,
	oper : Ext.create('system.ewechat.menu.biz.MenuOper'),
	initComponent:function(){
	    var me = this;
	    
	    var wechatid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '微信编号',
	    	name: 'wechatid',
	    	value: me.wxid
	    });
	    
	    var parentname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属菜单名称',
	    	name: 'parentname'
	    });
	    var parentid = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属菜单',
	    	labelAlign:'right',
	    	labelWidth:80,
	    	rootText : '菜单',
	    	rootId : me.wxid,
	        width : 360,
	        name : 'parentid',
	        baseUrl:'e_menu/load_AsyncMenuTree.do',//访问路劲
	        allowBlank: false,
	        blankText: '请选择所属菜单',
	        hiddenName : 'parentname'//隐藏域Name
	    });
	    
	    var code = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '菜单编码',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'code',
	    	maxLength: 25,
	    	allowBlank: false,
	    	blankText: '请输入菜单编码',
	    	maxLengthText: '菜单编码不能超过25个字符'
	    });
	    
	    var name = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '菜单名称',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'name',
	    	maxLength: 10,
	    	allowBlank: false,
	    	blankText: '请输入菜单名称',
	    	maxLengthText: '菜单名称不能超过10个字符'
	    });

	    var type = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'菜单类型',
	        labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
    	    name : 'type',//提交到后台的参数名
	        key : 'e_wxcdlx',//参数
	        allowBlank: false,
	        blankText: '请选择菜单类型',
    	    hiddenName : 'typename',//提交隐藏域Name
	    });  
	    var typename = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '菜单类型名称',
	    	name: 'typename'
	    });
	    
	    var sort = new Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '排序编号',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'sort',
	    	maxLength: 3,
	    	minValue: 0,
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank: false,
	    	blankText: '请输入排序编号',
	    	maxLengthText: '排序编号不能超过3个字符'
	    });
	    
	    var aurl = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '跳转链接',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'aurl',
	    	maxLength: 200,
	    	allowBlank: true,
	    	blankText: '请输入跳转链接',
	    	maxLengthText: '跳转链接不能超过200个字符'
	    });
	    
	    var remark = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'remark',
	    	maxLength: 50,
	    	maxLengthText: '备注不能超过50个字符'
	    });
	    
	    me.on('show' , function(){
	    	parentid.setText(parentname.getValue());
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [{
                name: "id",
                xtype: "hidden"
            } , wechatid, parentname, typename, parentid, code, name,
            type, aurl, sort, remark]
        });
	    
	    Ext.apply(this,{
	        width:450,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
				popedomCode : 'wxcdbj',
			    handler : function() {
    				me.oper.formSubmit(form, me.grid , me.treepanel);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});