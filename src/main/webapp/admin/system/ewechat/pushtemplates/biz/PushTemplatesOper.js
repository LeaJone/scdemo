/**
 * 推送模板管理界面操作类
 * @author lw
 */
Ext.define('system.ewechat.pushtemplates.biz.PushTemplatesOper', {
	
	form : null,
	util : Ext.create('system.util.util'),

	/**
	 * 添加
	 */
	add : function(grid, wxid){
		var win = Ext.create('system.ewechat.pushtemplates.biz.PushTemplatesEdit', {
			title : '推送模板添加',
			grid : grid,
			wxid : wxid,
			modal : true
		});
		win.show();
	},

	/**
     * 表单提交
     */
     formSubmit : function(formpanel, grid){
        var me = this;
        if (formpanel.form.isValid()) {
        	me.util.ExtFormSubmit('e_pushtemplates/save_qx_wxtsmbbj.do' , formpanel.form , '正在提交数据,请稍候.....',
        			function(form, action){
        			    formpanel.up('window').close();
        			    grid.getStore().reload();
        				Ext.MessageBox.alert('提示', '保存成功！');
        			});
        }
     },
	
	/**
     * 修改
     */
	editor : function(grid, wxid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{
        	// 先得到主键
            var id = data[0].data.id;
            var win = Ext.create('system.ewechat.pushtemplates.biz.PushTemplatesEdit', {
    	    	title : '推送模板修改',
    	        grid : grid,
    	        wxid : wxid,
    	        modal : true
    	    });
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'e_pushtemplates/load_pushtemplatesByid.do' , param , null , 
	        function(response, options, respText, data){
	            win.show();
	        });
        }
	},
 	
 	/**
     * 查看
     */
	view : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if(data.length > 0){
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.ewechat.pushtemplates.biz.PushTemplatesEdit');
            win.setTitle('推送模板查看');
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'e_pushtemplates/load_pushtemplatesByid.do' , param , null , 
	        function(response, options, respText, data){
	            win.show();
	        });
        }
	},
	
	/**
     * 是否启用停用
     */
	isEnabled : function(grid, isQT){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
		        msg : '您确定要修改状态吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
					    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
			            });
             			var pram = {ids : dir, isenabled : isQT};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('e_pushtemplates/enabled_qx_wxtsmbbj.do' , param ,
                        function(response, options){
            				Ext.MessageBox.alert('提示', '状态修改成功！');
                        	grid.getStore().reload(); 
                        });
                    }
                }
            });
        }
	}
});