﻿/**
 * 推送模板管理界面
 * @author lw
 */
Ext.define('system.ewechat.pushtemplates.biz.PushTemplatesManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.ewechat.pushtemplates.model.Extjs_Column_E_pushtemplates', 
	             'system.ewechat.pushtemplates.model.Extjs_Model_E_pushtemplates'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		
		me.wxid = '';
		me.items = [ me.createTable() ];
		me.tbar = [
		    me.wechat ,{
			text : '加载',
			iconCls : 'magnifierIcon',
			listeners: {
				click : function(obj, e, eOpts ){
					if (obj.getText() == "加载"){
						if (me.wechat.validate()){
							me.wxid = me.wechat.getValue();
							var pram = {wxid : me.wxid};
							me.grid.setDisabled(false);
							me.grid.getStore().loadPage(1);
							me.wechat.setDisabled(true);
							obj.setText("重置");
						}
					}else{
						me.grid.getStore().removeAll();
						me.grid.setDisabled(true);
						me.wechat.setDisabled(false);
						obj.setText("加载");
					} 
                }
        	}
		}];
		me.callParent();
	},
	
    oper : Ext.create('system.ewechat.pushtemplates.biz.PushTemplatesOper'),
    
	createTable : function() {
	    var me = this;

		/**
		 * 查询文本框
		 */
		var queryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入模板名',
			enableKeyEvents : true,
			width : 130
		});
		var typename =  Ext.create("Ext.form.TextField" , {
			name : 'typename',
			emptyText : '请输入模板类型名',
			enableKeyEvents : true,
			width : 130
		});
		
		me.wechat =  Ext.create("system.widget.FsdComboBoxData" , {
			name : 'wechat',
			fieldLabel : '选择微信号',
	    	labelAlign:'right',
	    	labelWidth:80,
			width : 350,
			zBaseUrl : 'e_wechat/load_data.do',
			zValueField : 'id',
		    zDisplayField : 'name'
		});

		// 创建表格
		me.grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_e_pushtemplates', // 显示列
			model : 'model_e_pushtemplates',
			baseUrl : 'e_pushtemplates/load_pagedata.do',
			border : false,
			disabled : true,
			zAutoLoad : false,
			tbar : [{
				xtype : "fsdbutton",
				text : "添加",
				iconCls : 'page_addIcon',
				popedomCode : 'wxtsmbbj',
				handler : function() {
					me.oper.add(me.grid, me.wxid);
				}
			}, {
				xtype : "fsdbutton",
				text : "修改",
				iconCls : 'page_editIcon',
				popedomCode : 'wxtsmbbj',
				handler : function() {
					me.oper.editor(me.grid, me.wxid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(me.grid);
				}
			}, '-', {
				xtype : "fsdbutton",
				text : "启用",
				iconCls : 'tickIcon',
				popedomCode : 'wxtsmbbj',
				handler : function(button) {
				    me.oper.isEnabled(me.grid, true);
				}
			}, {
				xtype : "fsdbutton",
				text : "停用",
				iconCls : 'crossIcon',
				popedomCode : 'wxtsmbbj',
				handler : function(button) {
				    me.oper.isEnabled(me.grid, false);
				}
			}, '->', queryText, typename, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					me.grid.getStore().reload();
				}
			}]			
		});
		
		me.grid.getStore().on('beforeload', function(s) {
			var pram = {
					name : queryText.getValue(), 
					typename : typename.getValue(),
					wechatid : me.wxid};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});

		return me.grid;
	}
});