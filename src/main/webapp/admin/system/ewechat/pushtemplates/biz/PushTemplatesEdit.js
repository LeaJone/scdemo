/**
 * 推送模板编辑界面
 */

Ext.define('system.ewechat.pushtemplates.biz.PushTemplatesEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	wxid : '',
	
	oper : Ext.create('system.ewechat.pushtemplates.biz.PushTemplatesOper'),
	
	initComponent:function(){
	    var me = this;
	    
	    var wechatid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '微信ID',
	    	name: 'wechatid',
	    	value: me.wxid
	    });
	    
	    var code = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '编码',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'code',
	    	maxLength: 25,
	    	allowBlank: false
	    });
	    
	    var name = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '名称',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'name',
	    	maxLength: 25,
	    	allowBlank: false
	    });
	    
	    var urlpath = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '跳转地址',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'urlpath',
	    	maxLength: 200
	    });
	    
	    var remark = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'remark',
	    	maxLength: 100
	    });
	    
	    var templateid = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '模板ID',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'templateid',
	    	maxLength: 100,
	    	allowBlank: false
	    });
	    
	    var details = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '描述',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'details',
	    	rows : 8,
	    	allowBlank: false
	    });
	    
//	    me.on('show' , function(){
//	    	parentid.setText(parentname.getValue());
//	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '15 15 10 5',
			items : [{
                name: "id",
                xtype: "hidden"
            } , wechatid,
            code, name, urlpath,
            remark, templateid, details]//lmtp2col , 
        });
	    
	    Ext.apply(this,{
	        layout : 'fit',
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
			    handler : function() {
    				me.oper.formSubmit(form, me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    }),
	    this.callParent(arguments);
	}
});