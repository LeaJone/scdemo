﻿/**
 *E_PushTemplates Column
 *@author CodeSystem
 *文件名     Extjs_Column_E_pushtemplates
 *Columns名  column_e_pushtemplates
 *xtype: 'textfield', 'numbercolumn', 'booleancolumn', 'datecolumn'   format : '0.00'
 *align: 'left', 'center', 'right'
 */

Ext.define('column_e_pushtemplates', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            align : 'center',
            width : 40
        }
        ,
        {
            header : '编码',
            dataIndex : 'code',
            sortable : true,
            align : 'center',
            flex : 1
        }
        ,
        {
            header : '名称',
            dataIndex : 'name',
            sortable : true,
            align : 'center',
            flex : 1
        }
        ,
        {
            header : '跳转地址',
            dataIndex : 'urlpath',
            sortable : true,
            align : 'center',
            flex : 3
        }
        ,
        {
            header : '模板ID',
            dataIndex : 'templateid',
            sortable : true,
            align : 'center',
            flex : 3
        }
        ,
        {
            header : '状态',
            dataIndex : 'statecode',
            sortable : true,
            align : 'center',
            flex : 1,
            renderer :function(value, metaData, record ){
				if(value == 'e_tsmbztqy'){
					value="<font style='color:green;'>启用</font>";
				}else{
					value="<font style='color:red;'>停用</font>";
				}
				return value;
			}
        }
        ,
        {
            header : '备注',
            dataIndex : 'remark',
            sortable : true,
            align : 'center',
            flex : 1
        }
    ]
});
