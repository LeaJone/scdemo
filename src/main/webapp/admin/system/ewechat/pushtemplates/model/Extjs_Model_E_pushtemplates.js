﻿/**
 *E_PushTemplates Model
 *@author CodeSystem
 *文件名     Extjs_Model_E_pushtemplates
 *Model名    model_e_pushtemplates
 */

Ext.define('model_e_pushtemplates', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'code',
            type : 'string'
        }
        ,
        {
            name : 'name',
            type : 'string'
        }
        ,
        {
            name : 'typename',
            type : 'string'
        }
        ,
        {
            name : 'urlpath',
            type : 'string'
        }
        ,
        {
            name : 'templateid',
            type : 'string'
        }
        ,
        {
            name : 'statecode',
            type : 'string'
        }
        ,
        {
            name : 'statename',
            type : 'string'
        }
        ,
        {
            name : 'remark',
            type : 'string'
        }
    ]
});
