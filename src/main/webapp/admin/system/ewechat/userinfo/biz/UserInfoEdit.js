﻿/**
 * 微信用户编辑界面
 * @author lw
 */
Ext.define('system.ewechat.userinfo.biz.UserInfoEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.ewechat.userinfo.biz.UserInfoOper'),
	initComponent:function(){
	    var me = this;
	    
	    var realname = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '真实姓名',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'realname',
	    	maxLength: 25
	    });
	    
	    var employeeid = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '人员ID',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'employeeid',
	    	maxLength: 32
	    });
	    
	    var openid = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: 'OpenID',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'openid',
	    	maxLength: 50,
	    	allowBlank: false
	    });
	    
	    var nickname = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '昵称',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'nickname',
	    	maxLength: 25,
	    	allowBlank: false
	    });
	    
	    var sex = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '性别',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'sex',
	    	maxLength: 5,
	    	allowBlank: false
	    });
	    
	    var language = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '语言',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'language',
	    	maxLength: 10,
	    	allowBlank: false
	    });
	    
	    var city = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '城市',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'city',
	    	maxLength: 25,
	    	allowBlank: false
	    });
	    
	    var province = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '省份',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'province',
	    	maxLength: 25,
	    	allowBlank: false
	    });
	    
	    var country = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '国家',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'country',
	    	maxLength: 25,
	    	allowBlank: false
	    });
	    
	    var headimgurl = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '头像',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'headimgurl',
	    	maxLength: 200,
	    	allowBlank: false
	    });
	    
	    Ext.apply(this,{
	        width:400,
	        height:370,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[{
				xtype : 'form',
				id : 'userForm',
				defaultType : 'textfield',
				header : false,// 是否显示标题栏
				border : 0,
				padding : '20 0 0 20',
				items : [{
                    name: "id",
                    xtype: "hidden"
                } , realname, employeeid, openid, nickname, sex, language, city, province, country, headimgurl]
	        }],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'jgbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.formSubmit(Ext.getCmp('userForm'), me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    }),
	    this.callParent(arguments);
	}
});