﻿/**

 * 机构管理界面
 * @author lumingbao
 */

Ext.define('system.ewechat.userinfo.biz.UserInfoManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.ewechat.userinfo.model.Extjs_Column_E_userinfo',
	             'system.ewechat.userinfo.model.Extjs_Model_E_userinfo'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.ewechat.userinfo.biz.UserInfoOper'),
	
	createContent : function(){
		var me = this;
		
		var realname = Ext.create('Ext.form.TextField', {
			emptyText : '请输入姓名',
			enableKeyEvents : true,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			},
			width : 130
		});
		
		var nickname = Ext.create('Ext.form.TextField', {
			emptyText : '请输入昵称',
			enableKeyEvents : true,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			},
			width : 130
		});
		
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_e_userinfo', // 显示列
			model: 'model_e_userinfo',
			baseUrl : 'e_userinfo/load_pagedata.do',
			tbar : [{
				xtype : "fsdbutton",
				text : "添加",
				iconCls : 'page_addIcon',
				popedomCode : 'wxyhdjbj',
				handler : function(button) {
					me.oper.add(grid);
				}
			}, {
				xtype : "fsdbutton",
				text : "修改",
				iconCls : 'page_editIcon',
				popedomCode : 'wxyhdjbj',
				handler : function(button) {
					me.oper.editor(grid);
				}
			}, {
				xtype : "fsdbutton",
				text : "删除",
				iconCls : 'page_deleteIcon',
				popedomCode : 'wxyhdjsc',
				handler : function(button) {
				    me.oper.del(grid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid);
				}
			}, '->', realname, nickname, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var pram = {realname : realname.getValue() , nickname : nickname.getValue()};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		return grid;
	}
});