﻿/**
 *E_UserInfo Column
 *@author CodeSystem
 *文件名     Extjs_Column_E_userinfo
 *Columns名  column_e_userinfo
 *xtype: 'textfield', 'numbercolumn', 'booleancolumn', 'datecolumn'   format : '0.00'
 *align: 'left', 'center', 'right'
 */

Ext.define('column_e_userinfo', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            align : 'center',
            width : 40
        }
        ,
        {
            header : '人员ID',
            dataIndex : 'employeeid',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '真实姓名',
            dataIndex : 'realname',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : 'OpenID',
            dataIndex : 'openid',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '昵称',
            dataIndex : 'nickname',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '性别',
            dataIndex : 'sex',
            sortable : true,
            align : 'center',
            flex : 1
        }
        ,
        {
            header : '添加时间',
            dataIndex : 'adddate',
            sortable : true,
            align : 'center',
            flex : 1,
			renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value, false);
			}
        }
    ]
});
