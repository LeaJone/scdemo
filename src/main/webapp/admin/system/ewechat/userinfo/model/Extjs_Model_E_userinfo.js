﻿/**
 *E_UserInfo Model
 *@author CodeSystem
 *文件名     Extjs_Model_E_userinfo
 *Model名    model_e_userinfo
 */

Ext.define('model_e_userinfo', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'employeeid',
            type : 'string'
        }
        ,
        {
            name : 'realname',
            type : 'string'
        }
        ,
        {
            name : 'openid',
            type : 'string'
        }
        ,
        {
            name : 'nickname',
            type : 'string'
        }
        ,
        {
            name : 'sex',
            type : 'string'
        }
        ,
        {
            name : 'adddate',
            type : 'string'
        }
    ]
});
