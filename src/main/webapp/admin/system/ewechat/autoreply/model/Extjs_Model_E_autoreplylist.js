﻿/**
 *e_autoreplylist Model
 *@author CodeSystem
 *文件名     Extjs_Model_E_autoreplylist
 *Model名    model_e_autoreplylist
 */

Ext.define('model_e_autoreplylist', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'title',
            type : 'string'
        }
        ,
        {
            name : 'description',
            type : 'string'
        }
        ,
        {
            name : 'imageurl',
            type : 'string'
        }
        ,
        {
            name : 'linkurl',
            type : 'string'
        }
        ,
        {
            name : 'sort',
            type : 'int'
        }
        ,
        {
            name : 'remark',
            type : 'string'
        }
    ]
});
