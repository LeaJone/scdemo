﻿/**
 *e_autoreply Model
 *@author CodeSystem
 *文件名     Extjs_Model_E_autoreply
 *Model名    model_e_autoreply
 */

Ext.define('model_e_autoreply', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,{
            name : 'name',
            type : 'string'
        }
        ,
        {
            name : 'messagetypename',
            type : 'string'
        }
        ,
        {
            name : 'menuname',
            type : 'string'
        }
        ,
        {
            name : 'keyword',
            type : 'string'
        }
        ,
        {
            name : 'replytypename',
            type : 'string'
        }
        ,
        {
            name : 'sort',
            type : 'int'
        }
        ,
        {
            name : 'statusname',
            type : 'string'
        }
        ,
        {
            name : 'remark',
            type : 'string'
        }
        ,
        {
            name : 'adddate',
            type : 'string'
        }
        ,
        {
            name : 'addemployeename',
            type : 'string'
        }
    ]
});
