﻿/**
 *e_systemcode Column
 *@author CodeSystem
 *文件名     Extjs_Column_E_systemcode
 *Columns名  column_e_systemcode
 */

Ext.define('column_e_systemcode', {
    columns: [
        {
            header : '系统代码',
            dataIndex : 'code',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '功能描述',
            dataIndex : 'description',
            sortable : true,
            flex : 1
        }
    ]
});
