﻿/**
 *e_systemcode Model
 *@author CodeSystem
 *文件名     Extjs_Model_E_systemcode
 *Model名    model_e_systemcode
 */

Ext.define('model_e_systemcode', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'code',
            type : 'string'
        }
        ,
        {
            name : 'description',
            type : 'string'
        }
    ]
});
