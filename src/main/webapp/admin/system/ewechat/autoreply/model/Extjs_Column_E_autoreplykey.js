﻿/**
 *e_autoreplykey Column
 *@author CodeSystem
 *文件名     Extjs_Column_E_autoreplykey
 *Columns名  column_e_autoreplykey
 */

Ext.define('column_e_autoreplykey', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            width : 40
        }
        ,
        {
            header : '关键字',
            dataIndex : 'keyword',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '是否全匹配',
            dataIndex : 'ismatch',
            sortable : true,
            flex : 1,
			renderer :function(value ,metaData ,record ){
				if(value == 'true'){
					value="<font style='color:red;'>全匹配</font>";
				}else{
					value="<font style='color:blue;'>模糊匹配</font>";
				}
				return value;
			}
        }
        ,
        {
            header : '备注',
            dataIndex : 'remark',
            sortable : true,
            flex : 1
        }
    ]
});
