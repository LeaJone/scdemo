﻿/**
 *e_autoreplykey Model
 *@author CodeSystem
 *文件名     Extjs_Model_E_autoreplykey
 *Model名    model_e_autoreplykey
 */

Ext.define('model_e_autoreplykey', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'keyword',
            type : 'string'
        }
        ,
        {
            name : 'ismatch',
            type : 'string'
        }
        ,
        {
            name : 'remark',
            type : 'string'
        }
    ]
});
