﻿/**
 *e_autoreplylist Column
 *@author CodeSystem
 *文件名     Extjs_Column_E_autoreplylist
 *Columns名  column_e_autoreplylist
 */

Ext.define('column_e_autoreplylist', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            width : 40
        }
        ,
        {
            header : '标题',
            dataIndex : 'title',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '描述',
            dataIndex : 'description',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '图片地址',
            dataIndex : 'imageurl',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '链接地址',
            dataIndex : 'linkurl',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '顺序号',
            dataIndex : 'sort',
            sortable : true,
            flex : 1
        }
    ]
});
