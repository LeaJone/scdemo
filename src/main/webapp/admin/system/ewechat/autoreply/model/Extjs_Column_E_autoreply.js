﻿/**
 *e_autoreply Column
 *@author CodeSystem
 *文件名     Extjs_Column_E_autoreply
 *Columns名  column_e_autoreply
 */

Ext.define('column_e_autoreply', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            width : 40
        }
        ,
        {
            header : '规则名',
            dataIndex : 'name',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '消息类型',
            dataIndex : 'messagetypename',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '菜单名',
            dataIndex : 'menuname',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '关键字',
            dataIndex : 'keyword',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '回复类型',
            dataIndex : 'replytypename',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '顺序号',
            dataIndex : 'sort',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '状态',
            dataIndex : 'statusname',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '备注',
            dataIndex : 'remark',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '新增人',
            dataIndex : 'addemployeename',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '新增时间',
            dataIndex : 'adddate',
            sortable : true,
            flex : 1,
			renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value);
			}
        }
    ]
});
