﻿/**
 * 自动回复管理界面
 * @author lw
 */
Ext.define('system.ewechat.autoreply.biz.AutoReplyManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.widget.FsdButton',
	             'system.ewechat.autoreply.model.Extjs_Column_E_autoreply', 
	             'system.ewechat.autoreply.model.Extjs_Model_E_autoreply',
	             'system.admin.tools.model.Extjs_Column_SortOperate',
		         'system.admin.tools.model.Extjs_Model_SortOperate'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		
		me.wxid = '';
		me.items = [ me.createTable() ];
		me.tbar = [
		    me.wechat ,{
			text : '加载',
			iconCls : 'magnifierIcon',
			listeners: {
				click : function(obj, e, eOpts ){
					if (obj.getText() == "加载"){
						if (me.wechat.validate()){
							me.wxid = me.wechat.getValue();
							var pram = {wxid : me.wxid};
							me.grid.setDisabled(false);
							me.grid.getStore().loadPage(1);
							me.wechat.setDisabled(true);
							obj.setText("重置");
						}
					}else{
						me.grid.getStore().removeAll();
						me.grid.setDisabled(true);
						me.wechat.setDisabled(false);
						obj.setText("加载");
					} 
                }
        	}
		}];
		me.callParent();
	},
	
    oper : Ext.create('system.ewechat.autoreply.biz.AutoReplyOper'),
    
	createTable : function() {
	    var me = this;

		/**
		 * 查询文本框
		 */
		var queryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入规则名',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});
		
		me.wechat =  Ext.create("system.widget.FsdComboBoxData" , {
			name : 'wechat',
			fieldLabel : '选择微信号',
	    	labelAlign:'right',
	    	labelWidth:80,
			width : 350,
			zBaseUrl : 'e_wechat/load_data.do',
			zValueField : 'id',
		    zDisplayField : 'name'
		});

		// 创建表格
		me.grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_e_autoreply', // 显示列
			model : 'model_e_autoreply',
			baseUrl : 'e_autoreply/load_pagedata.do',
			border : false,
			disabled : true,
			zAutoLoad : false,
			tbar : [{
				xtype : "fsdbutton",
				text : "添加",
				iconCls : 'page_addIcon',
				popedomCode : 'wxzdhfbj',
				handler : function() {
					me.oper.add(me.grid, me.wxid);
				}
			}, {
				xtype : "fsdbutton",
				text : "修改",
				iconCls : 'page_editIcon',
				popedomCode : 'wxzdhfbj',
				handler : function() {
					me.oper.editor(me.grid, me.wxid);
				}
			}, {
				xtype : "fsdbutton",
				text : "删除",
				iconCls : 'page_deleteIcon',
				popedomCode : 'wxzdhfsc',
				handler : function() {
				    me.oper.del(me.grid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(me.grid);
				}
			}, '-', {
				xtype : "fsdbutton",
				text : "启用",
				iconCls : 'tickIcon',
				popedomCode : 'wxzdhfbj',
				handler : function(button) {
				    me.oper.isEnabled(me.grid, true);
				}
			}, {
				xtype : "fsdbutton",
				text : "停用",
				iconCls : 'crossIcon',
				popedomCode : 'wxzdhfbj',
				handler : function(button) {
				    me.oper.isEnabled(me.grid, false);
				}
			}, '-', {
				text : '排序',
				iconCls : 'up',
				handler : function() {
					var name =  queryText.getValue();
					var pram = {name : name, wxid : me.wxid};
					me.oper.util.sortOperate(
							grid, 
							'column_name',
							'model_sortoperate',
							grid.baseUrl,
							'e_autoreply/sort_AutoReply_qx_wxzdhfbj.do',
							function(){return pram;},
				        	function(){});
				}
			}, '->', queryText, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					me.grid.getStore().reload();
				}
			}]			
		});
		
		me.grid.getStore().on('beforeload', function(s) {
			var name =  queryText.getValue();
			var pram = {name : name, wxid : me.wxid};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});

		return me.grid;
	}
});