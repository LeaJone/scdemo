﻿/**
 * 自助回复列表添加界面
 * @author lw
 */
Ext.define('system.ewechat.autoreply.biz.AutoReplyListEdit', {
	extend : 'Ext.window.Window',
	
	grid : null,
	isUpdate : false,
	oper : Ext.create('system.ewechat.autoreply.biz.AutoReplyOper'),
	
	setNew : function(){
	    var me = this;
		me.idNo.setValue(Ext.id());
		me.isUpdate = false;
	},
	setUpdate : function(obj){
	    var me = this;
		me.isUpdate = true;
		me.form.getForm().setValues(obj);
	},

	initComponent:function(){
	    var me = this;
	    
		me.idNo = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '编号',
	    	name : 'id'
	    });
	    
	    var title = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '标题',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'title',
	    	maxLength: 50,
	    	allowBlank: false,
	    	blankText: '请输入标题',
	    	maxLengthText: '标题不能超过50个字符'
	    });
	    
	    var description = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '描述',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'description',
	    	rows : 4
	    });
	    
	    var imageurl = Ext.create('system.widget.FsdTextFileManage', {
	        width : 340,
	    	zName : 'imageurl',
	    	zFieldLabel : '图片地址',
	    	zLabelWidth : 80,
	    	zIsShowButton : true, 
	    	zIsUpButton : true,
	    	zFileType : 'image',
	    	zIsReadOnly : false,
	    	zFileAutoName : false,
	    	zFileUpPath : me.oper.util.getImagePath(),
	    	zManageType : 'manage'
	    });
	    
	    var linkurl = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '链接地址',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'linkurl',
	    	allowBlank: false,
	    	maxLength: 200,
	    	blankText: '请输入链接地址',
	    	maxLengthText: '链接地址不能超过200个字符'
	    });
	    
	    var sort = new Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '排序编号',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'sort',
	    	maxLength: 3,
	    	minValue: 1,
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank: false,
	    	blankText: '请输入排序编号',
	    	maxLengthText: '排序编号不能超过3个字符'
	    });
	    
	    var remark = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'remark',
	    	maxLength: 50,
	    	maxLengthText: '备注不能超过50个字符'
	    });
	    
	    me.form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [me.idNo, title, description, imageurl, linkurl, sort, remark]
        });
	        
	    Ext.apply(this,{
	        width:400,
	        height:310,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        items :[me.form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
			    	if (me.form.form.isValid()) {
				    	if (me.isUpdate){
						    var models = me.grid.getStore().getRange();
						    for ( var i = 0; i < models.length; i++) {
						    	if (models[i].data.id == me.idNo.getValue()){
						    		models[i].set('title', me.form.getForm().getValues().title);
						    		models[i].set('description', me.form.getForm().getValues().description);
						    		models[i].set('imageurl', me.form.getForm().getValues().imageurl);
						    		models[i].set('linkurl', me.form.getForm().getValues().linkurl);
						    		models[i].set('sort', me.form.getForm().getValues().sort);
						    		models[i].set('remark', me.form.getForm().getValues().remark);
						    		break;
						    	}
						    }
				    	}else{
				    		me.grid.getStore().add(me.form.getForm().getValues());
				    	}
				    	me.close();
			    	}
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});