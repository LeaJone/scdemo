﻿/**
 * 自动回复编辑界面
 * @author lw
 */
Ext.define('system.ewechat.autoreply.biz.AutoReplyEdit', {
	extend : 'Ext.window.Window',
	requires : ['system.widget.FsdTreeComboBox',
	             'system.ewechat.autoreply.model.Extjs_Column_E_autoreplykey', 
	             'system.ewechat.autoreply.model.Extjs_Model_E_autoreplykey',
	             'system.ewechat.autoreply.model.Extjs_Column_E_autoreplylist',
	             'system.ewechat.autoreply.model.Extjs_Model_E_autoreplylist',
	             'system.ewechat.autoreply.model.Extjs_Column_E_systemcode',
	             'system.ewechat.autoreply.model.Extjs_Model_E_systemcode'],
	
	grid : null,
	wxid : null,
	
	oper : Ext.create('system.ewechat.autoreply.biz.AutoReplyOper'),
	
	setUpdate : function(keyobjs, lstobjs){
	    var me = this;
		me.messagetype.setReadOnly(true);
		me.setMessageType(me.messagetype.getValue());
		me.setReplyType(me.replytype.getValue());
		
		me.gridKey.getStore().add(keyobjs);
		me.gridList.getStore().add(lstobjs);
	},
	
	setMessageType : function(code, name){
	    var me = this;
		switch (code) {
		case 'e_wxxxxt':
			me.gridKey.setDisabled(true);
			me.gridKey.getStore().removeAll();
			break;
		case 'e_wxxxcd':
			me.menuid.setDisabled(false);
			me.menuid.allowBlank = false;
			me.menuid.setValue(null);
			me.gridKey.setDisabled(true);
			me.gridKey.getStore().removeAll();
			break;
		case 'e_wxxxgj':
			me.menuid.setDisabled(true);
			me.menuid.allowBlank = true;
			me.menuid.setValue(null);
			me.gridKey.setDisabled(false);
			me.gridKey.getStore().removeAll();
			break;
		}
	},
	
	setReplyType : function(code , name){
	    var me = this;
		switch (code) {
		case 'e_wxhfnr':
			me.pList.setDisabled(true);
			me.gridList.getStore().removeAll();
			me.pContent.setDisabled(false);
			me.content.allowBlank = false;
			me.tabPanle.setActiveTab(me.pContent);
			break;
		case 'e_wxhflb':
			me.pContent.setDisabled(true);
			me.content.allowBlank = true;
			me.content.setValue('');
			me.pList.setDisabled(false);
			me.tabPanle.setActiveTab(me.pList);
			break;
		}
    },
	
	initComponent:function(){
	    var me = this;
	    me.oper.form = me;

	    var wechatid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '微信编号',
	    	name: 'wechatid',
	    	value: me.wxid
	    });
	    
	    var name = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '规则名',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'name',
	    	maxLength: 25,
	    	allowBlank: false,
	    	blankText: '请输入规则名',
	    	maxLengthText: '规则名不能超过25个字符'
	    });

	    me.messagetype = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'消息类型',
	        labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
    	    name : 'messagetype',//提交到后台的参数名
	        key : 'e_wxxxlx',//参数
    	    hiddenName : 'messagetypename',//提交隐藏域Name
    	    zCallback : function(code, name){
    	    	me.setMessageType(code, name);
    	    }
	    });
	    var messagetypename = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '消息类型名称',
	    	name: 'messagetypename'
	    });
	    
	    var menuname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '菜单名称',
	    	name: 'menuname'
	    });
	    me.menuid = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '菜单事件',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	rootText : '微信菜单',
	    	rootId : me.wxid,
	        name : 'menuid',
	        baseUrl:'e_menu/load_AsyncMenuTree.do',//访问路劲
	        allowBlank: true,
			disabled : true,
	        blankText: '请选择所属菜单',
	        hiddenName : 'menuname',//隐藏域Name
	        zCallback : function(obj){
	        	me.oper.getMenuCode(obj.id, me.gridKey);
    	    }
	    });

	    me.replytype = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'回复类型',
	        labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
    	    name : 'replytype',//提交到后台的参数名
	        key : 'e_wxhflx',//参数
    	    hiddenName : 'replytypename',//提交隐藏域Name
            zCallback : function(code , name){
            	me.setReplyType(code, name);
    	    }
	    });
	    var replytypename = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '回复类型名称',
	    	name: 'replytypename'
	    });
	    
	    me.content = Ext.create('system.widget.FsdTextArea',{
	        width : 480,
	    	name: 'content',
	    	rows : 8,
	    });
	    
	    var sort = new Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '排序编号',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'sort',
	    	maxLength: 3,
	    	minValue: 1,
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank: false,
	    	blankText: '请输入排序编号',
	    	maxLengthText: '排序编号不能超过3个字符'
	    });

	    var status = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'状态',
	        labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
    	    name : 'status',//提交到后台的参数名
	        key : 'e_wxhfzt',//参数
    	    hiddenName : 'statusname'//提交隐藏域Name
	    });
	    var statusname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '状态名',
	    	name: 'statusname'
	    });
	   
	    var remark = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '描述',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 700,
	    	name: 'remark',	    	
	    	maxLength: 200,
	    	allowBlank: true,
	    	blankText: '请输入描述',
	    	maxLengthText: '描述不能超过200个字符'
	    });
	    
	    // 创建表格
		me.gridKey = Ext.create("system.widget.FsdGridPanel", {
			column : 'column_e_autoreplykey', // 显示列
			model: 'model_e_autoreplykey',
			height : 150,
			disabled : true,
			zAutoLoad : false,
			tbar : [{
				xtype : "fsdbutton",
				text : "添加",
				iconCls : 'page_addIcon',
				handler : function(button) {
					var win = Ext.create('system.ewechat.autoreply.biz.AutoReplyKeyEdit', {
				    	title : '自动回复关键字添加',
				        grid : me.gridKey,
				        modal : true
				    });
					win.setNew();
			        win.show();
				}
			}, {
				xtype : "fsdbutton",
				text : "修改",
				iconCls : 'page_editIcon',
				handler : function(button) {
					var data = me.gridKey.getSelectionModel().getSelection();
			        if (data.length == 0) {
			            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
			        }else if(data.length > 1){
			            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
			        }else{
						var win = Ext.create('system.ewechat.autoreply.biz.AutoReplyKeyEdit', {
					    	title : '自动回复关键字修改',
					        grid : me.gridKey,
					        modal : true
					    });
						win.setUpdate(data[0].data);
				        win.show();
			        }
				}
			}, {
				xtype : "fsdbutton",
				text : "删除",
				iconCls : 'page_deleteIcon',
				handler : function(button) {
					var data = me.gridKey.getSelectionModel().getSelection();
			        if (data.length == 0) {
			            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
			        }else{
			        	me.gridKey.getStore().remove(data);
			        }
				}
			}]			
		});

	    // 创建表格
		me.gridList = Ext.create("system.widget.FsdGridPanel", {
			column : 'column_e_autoreplylist', // 显示列
			model: 'model_e_autoreplylist',
			zAutoLoad : false,
			tbar : [{
				xtype : "fsdbutton",
				text : "添加",
				iconCls : 'page_addIcon',
				handler : function(button) {
					var win = Ext.create('system.ewechat.autoreply.biz.AutoReplyListEdit', {
				    	title : '自动回复列表添加',
				        grid : me.gridList,
				        modal : true
				    });
					win.setNew();
			        win.show();
				}
			}, {
				xtype : "fsdbutton",
				text : "修改",
				iconCls : 'page_editIcon',
				handler : function(button) {
					var data = me.gridList.getSelectionModel().getSelection();
			        if (data.length == 0) {
			            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
			        }else if(data.length > 1){
			            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
			        }else{
						var win = Ext.create('system.ewechat.autoreply.biz.AutoReplyListEdit', {
					    	title : '自动回复列表修改',
					        grid : me.gridList,
					        modal : true
					    });
						win.setUpdate(data[0].data);
				        win.show();
			        }
				}
			}, {
				xtype : "fsdbutton",
				text : "删除",
				iconCls : 'page_deleteIcon',
				handler : function(button) {
					var data = me.gridList.getSelectionModel().getSelection();
			        if (data.length == 0) {
			            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
			        }else{
			        	me.gridList.getStore().remove(data);
			        }
				}
			}]			
		});

	    var strCode = new Ext.create('system.widget.FsdTextField',{
			emptyText : '系统代码',
	        width : 200,
	    	name: 'strCode'
	    });
		// 系统代码功能
		var gridCode = Ext.create("system.widget.FsdGridPanel", {
			column : 'column_e_systemcode', // 显示列
			model : 'model_e_systemcode',
			baseUrl : 'e_systemcode/load_data.do',
			selType : 'rowmodel',
	        width : 200,
	        multiSelect : false,
			header : false,
			region : 'east', // 设置方位
			collapsible : true,//折叠
			split : true,
			tbar : [strCode]			
		});
		gridCode.on('itemclick', function(obj, record, item, index, e, eOpts) {
			if(record != null && record.data != null && record.data.code != null){
				strCode.setValue(record.data.code);
			}
		});
		
		me.pContent = Ext.create("Ext.panel.Panel", {
        	title: '回复内容',
			disabled : true,
        	items : [me.content]
        });
		me.pList = Ext.create("Ext.panel.Panel", {
        	title: '回复列表',
			disabled : true,
        	items : [me.gridList]
        });
		me.tabPanle = Ext.create("Ext.tab.Panel", {
            border : true,
			frame:true,
			split: true,
			region: 'center',
            items : [me.pContent, me.pList]
		});
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [{
                name: "id",
                xtype: "hidden"
            }, wechatid, messagetypename, menuname, replytypename, statusname, {
                layout : 'column',
                border : false,
    	        width : 720,
                items:[{
                    columnWidth : 0.5,
                    border:false,
                    items:[name, me.replytype, sort]
                }, {
                    columnWidth : 0.5,
                    border:false,
                    items:[me.messagetype, me.menuid, status]
                }]
			}, remark, {
				xtype : 'fieldset',
                title : '关键字管理',
                border : true,
    	        width : 700,
                items : [ me.gridKey ]
			}, {
				border : 0,
				layout : 'border',
				header : false,
    	        width : 700,
    			height : 180,
				items : [ gridCode, me.tabPanle ]
			}]
        });
	    
	    me.on('boxready' , function(obj, width, height, eOpts){
	    	me.menuid.setText(menuname.getValue());
	    });
	        
	    Ext.apply(this,{
	        width:755,
	        height:565,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        items :[form],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'wdbj',
	        	name : 'btnsave',
			    text : '　保存　',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.formSubmit(form, me.grid, me.gridKey, me.gridList);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});