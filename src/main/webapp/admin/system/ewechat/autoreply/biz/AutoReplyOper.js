/**
 * 自动回复管理界面操作类
 * @author lw
 */
Ext.define('system.ewechat.autoreply.biz.AutoReplyOper', {
	
	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 获取菜单对象
     */
	getMenuCode : function(menuid, gridKey){
		var me = this;
        var pram = {id : menuid};
        var param = {jsonData : Ext.encode(pram)};
        me.util.ExtAjaxRequest('e_menu/load_MenuByid.do' , param ,
        function(response, options, respText){
        	gridKey.getStore().removeAll();
        	gridKey.getStore().add({
        		id : '',
        		keyword : respText.data.code,
        		ismatch : 'true',
        		remark : '菜单事件'
        	});
        }, null, me.form);
	},
	
	/**
     * 添加
     */
	add : function(grid, wxid){
	    var win = Ext.create('system.ewechat.autoreply.biz.AutoReplyEdit', {
	    	title : '自动回复添加',
	        grid : grid,
	        wxid : wxid,
	        modal : true
	    });
        win.show();
	},
	
	/**
     * 表单提交
     */
     formSubmit : function(formpanel, grid, gridKey, gridList){
        var me = this;
        if (formpanel.form.isValid()) {
        	var keyArray = new Array();
        	for ( var i = 0; i < gridKey.getStore().getRange().length; i++) {
        		keyArray.push(gridKey.getStore().getRange()[i].data);
			}
        	var lstArray = new Array();
        	for ( var i = 0; i < gridList.getStore().getRange().length; i++) {
        		lstArray.push(gridList.getStore().getRange()[i].data);
			}
    		var objMap = {};
    		objMap['keyobjs'] = keyArray;
    		objMap['lstobjs'] = lstArray;
            me.util.ExtFormSubmit('e_autoreply/save_AutoReply_qx_wxzdhfbj.do' , formpanel.form , '正在提交数据,请稍候.....',
			function(form, action){
            	formpanel.up('window').close();
			    grid.getStore().reload();
				Ext.MessageBox.alert('提示', '保存成功！');
			}, null,
			{jsonData : Ext.encode(objMap)});
        }
     },
	
	/**
     * 修改
     */
	editor : function(grid, wxid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{
        	// 先得到主键
            var id = data[0].data.id;
            var win = Ext.create('system.ewechat.autoreply.biz.AutoReplyEdit', {
    	    	title : '自动回复修改',
    	        grid : grid,
    	        wxid : wxid,
    	        modal : true
    	    });
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'e_autoreply/load_AutoReplyByid.do' , param , null , 
	        function(response, options, respText, data){
//	        	model.loadObject(data.data);
	        	win.setUpdate(data['keyobjs'], data['lstobjs']);
	            win.show();
	        });
        }
	},
 	
 	/**
     * 查看
     */
	view : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if(data.length > 0){
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.ewechat.autoreply.biz.AutoReplyEdit');
            win.setTitle('自动回复查看');
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'e_autoreply/load_AutoReplyByid.do' , param , null , 
	        function(response, options, respText, data){
	        	win.setUpdate(data['keyobjs'], data['lstobjs']);
	            win.show();
	        });
        }
	},
	
	/**
     * 删除
     */
	del : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要删除的项目！');
        }else {
        	Ext.MessageBox.show({
        		title : '询问',
        		msg : '您确定要删除吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                   	 var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
            			});
                        var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('e_autoreply/del_AutoReply_qx_wxzdhfsc.do' , param ,
                        function(response, options){
	                       	grid.getStore().reload();
                        }, null, me.form);
                    }
                }
            });
        }
	},
	
	/**
     * 是否启用停用
     */
	isEnabled : function(grid, isQT){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
		        msg : '您确定要修改自动回复状态吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
					    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
			            });
             			var pram = {ids : dir, isenabled : isQT};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('e_autoreply/enabled_AutoReply_qx_wxzdhfbj.do' , param ,
                        function(response, options){
            				Ext.MessageBox.alert('提示', '自动回复状态修改成功！');
                        	grid.getStore().reload(); 
                        });
                    }
                }
            });
        }
	}
});