﻿/**
 * 自助回复关键字添加界面
 * @author lw
 */
Ext.define('system.ewechat.autoreply.biz.AutoReplyKeyEdit', {
	extend : 'Ext.window.Window',
	
	grid : null,
	isUpdate : false,
	
	setNew : function(){
	    var me = this;
		me.idNo.setValue(Ext.id());
		me.isUpdate = false;
	},
	setUpdate : function(obj){
	    var me = this;
		me.isUpdate = true;
		me.form.getForm().setValues(obj);
	},

	initComponent:function(){
	    var me = this;
	    
		me.idNo = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '编号',
	    	name : 'id'
	    });
	    
	    var keyword = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '关键字',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'keyword',
	    	maxLength: 25,
	    	allowBlank: false,
	    	blankText: '请输入关键字',
	    	maxLengthText: '关键字不能超过25个字符'
	    });
	    
	    var ismatch = new Ext.create('system.widget.FsdCheckbox',{
	    	fieldLabel: '是否全匹配',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'ismatch',
	    	inputValue : 'true'
	    });
	    
	    var remark = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'remark',
	    	maxLength: 50,
	    	maxLengthText: '备注不能超过50个字符'
	    });
	    
	    me.form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [me.idNo, keyword, ismatch, remark]
        });
	        
	    Ext.apply(this,{
	        width:400,
	        height:175,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        items :[me.form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
			    	if (me.form.form.isValid()) {
				    	if (me.isUpdate){
						    var models = me.grid.getStore().getRange();
						    for ( var i = 0; i < models.length; i++) {
						    	if (models[i].data.id == me.idNo.getValue()){
						    		models[i].set('keyword', me.form.getForm().getValues().keyword);
						    		models[i].set('ismatch', me.form.getForm().getValues().ismatch);
						    		models[i].set('remark', me.form.getForm().getValues().remark);
						    		break;
						    	}
						    }
				    	}else{
				    		me.grid.getStore().add(me.form.getForm().getValues());
				    	}
				    	me.close();
			    	}
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});