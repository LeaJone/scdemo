﻿/**
 *f_flowobject Model
 *@author CodeSystem
 *文件名     Extjs_Model_F_flowobject
 *Model名    model_f_flowobject
 */

Ext.define('model_f_flowobject', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'projectname',
            type : 'string'
        }
        ,
        {
            name : 'nodename',
            type : 'string'
        }
        ,
        {
            name : 'recordid',
            type : 'string'
        }
        ,
        {
            name : 'dataobjectid',
            type : 'string'
        }
        ,
        {
            name : 'datatypename',
            type : 'string'
        }
        ,
        {
            name : 'code',
            type : 'string'
        }
        ,
        {
            name : 'branchname',
            type : 'string'
        }
        ,
        {
            name : 'employeename',
            type : 'string'
        }
        ,
        {
            name : 'datadepict',
            type : 'string'
        }
        ,
        {
            name : 'applydate',
            type : 'string'
        }
        ,
        {
            name : 'flowstatusname',
            type : 'string'
        }
        ,
        {
            name : 'windowname',
            type : 'string'
        }
        ,
        {
            name : 'windowpath',
            type : 'string'
        }
    ]
});
