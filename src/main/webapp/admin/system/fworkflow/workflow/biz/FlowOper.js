/**
 * 角色管理界面操作类
 * @author lw
 */
Ext.define('system.fworkflow.workflow.biz.FlowOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 显示处理界面
     */
	dealtShow : function(grid, type, title){
		var me = this;
	    var win = Ext.create('system.fworkflow.workflow.biz.FlowDealt', {
	    	title : title + ' — 意见处理',
	    	grid : grid,
	    	type : type,
	    	win : me.form,
	    	modal : true
	    });
        win.show();
	},
	
	/**
     * 表单提交
     */
	dealtSubmit : function(pram, grid, dealtWin, chooseWin){
        var me = this;
        var param = {jsonData : Ext.encode(pram)};
        var win = dealtWin;
        if (chooseWin != null){
        	win = chooseWin;
        }
        me.util.ExtAjaxRequest('f_flowrecord/save_DealtResults.do' , param ,
        function(response, options, respText){
        	if (grid != null)
        		grid.getStore().reload();
		    if (chooseWin != null){
		    	chooseWin.close();
		    }
		    dealtWin.close();
		    dealtWin.win.close();
			Ext.MessageBox.alert('提示', '保存成功！');
        }, null, win);
     },

 	/**
      * 显示节点条件选择界面
      */
 	dealtChooseShow : function(dealtWin){
 		var me = this;
 	    var win = Ext.create('system.fworkflow.workflow.biz.FlowDealtChoose', {
 	    	title : '选择下一审批人员',
 	    	win : dealtWin,
 	    	modal : true
 	    });
         win.show();
 	},

 	/**
      * 显示移交选择界面
      */
 	dealtDevolveShow : function(dealtWin){
 		var me = this;
 	    var win = Ext.create('system.fworkflow.workflow.biz.FlowDealtDevolve', {
 	    	title : '选择转交审批人员',
 	    	win : dealtWin,
 	    	modal : true
 	    });
         win.show();
 	}
});