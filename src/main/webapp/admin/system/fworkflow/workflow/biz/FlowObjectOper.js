/**
 * 角色管理界面操作类
 * @author lw
 */
Ext.define('system.fworkflow.workflow.biz.FlowObjectOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 审批操作
     */
	view : function(grid){
		var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要操作的记录！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个记录！');
        }else{
    	    var win = Ext.create('system.fworkflow.workflow.biz.FlowOperate', {
    	    	data : data[0].data
    	    });
            win.showWin();
        }
	},
	
	/**
     * 提取操作
     */
	extract : function(grid){
		var me = this;
		var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要操作的记录！');
        }else{
        	var dir = new Array();　
            Ext.Array.each(data, function(items) {
                var id = items.data.recordid;
                dir.push(id);
            });
 			var pram = {ids : dir};
            var param = {jsonData : Ext.encode(pram)};
            me.util.ExtAjaxRequest('f_flowrecord/extract_Record.do' , param ,
            function(response, options){
				Ext.MessageBox.alert('提示', '提取操作成功！');
            	grid.getStore().reload(); 
            });
        }
        
	},
	
	/**
     * 释放操作
     */
	release : function(grid){
		var me = this;
		var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要操作的记录！');
        }else{
        	var dir = new Array();　
            Ext.Array.each(data, function(items) {
                var id = items.data.recordid;
                dir.push(id);
            });
 			var pram = {ids : dir};
            var param = {jsonData : Ext.encode(pram)};
            me.util.ExtAjaxRequest('f_flowrecord/release_Record.do' , param ,
            function(response, options){
				Ext.MessageBox.alert('提示', '提取操作成功！');
            	grid.getStore().reload(); 
            });
        }
	},
	
	/**
     * 审批操作
     */
	pending : function(grid){
		var me = this;
		var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要操作的记录！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个记录！');
        }else{
            var pram = {id : data[0].data.recordid};
            var param = {jsonData : Ext.encode(pram)};
            me.util.ExtAjaxRequest('f_flowrecord/load_RecordDealtByid.do' , param ,
            function(response, options, respText){
        	    var win = Ext.create('system.fworkflow.workflow.biz.FlowOperate', {
        	    	grid : grid,
        	    	data : data[0].data,
        	    	record : respText.record,
        	    	dealt : respText.dealt,
        	    	isPending : true
        	    });
                win.showWin();
            }, null, me.form);
        }
	}
});