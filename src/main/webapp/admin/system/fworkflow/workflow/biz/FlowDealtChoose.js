﻿/**
 * 工作流选择处理界面
 * @author lw
 */
Ext.define('system.fworkflow.workflow.biz.FlowDealtChoose', {
	extend : 'Ext.window.Window',
	requires : ['system.fworkflow.workflow.model.Extjs_Column_F_flownodeterm',
	            'system.fworkflow.workflow.model.Extjs_Model_F_flownodeterm'],
	win : null,//处理窗体
	record : null,//流程记录对象
	oper : Ext.create('system.fworkflow.workflow.biz.FlowOper'),
	
	createList : function() {
	    var me = this;

		// 创建表格
	    me.gridList = Ext.create("system.widget.FsdGridPanel", {
			column : 'column_f_flownodeterm', // 显示列
			model: 'model_f_flownodeterm',
			baseUrl : 'f_flownodeterm/load_ObjectListByNodeID.do',
			border : false,
			multiSelect : true,
		});
	    me.gridList.getStore().on('beforeload', function(s) {
	    	var pram = {nodeid : me.record.nextid};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});

		return me.gridList;
	},	
	
	initComponent:function(){
	    var me = this;
	    me.record = me.win.record;
	    
	    Ext.apply(this,{
	        width:400,
	        height:300,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	    	layout : 'fit',
	        items :[ me.createList() ],
	        buttons : [{
				xtype: 'label',
				text: '下一审批为选择审批，请选择审批人员。'
			}, {
			    text : '提交',
			    iconCls : 'acceptIcon',
			    handler : function() {
			    	var data = me.gridList.getSelectionModel().getSelection();
			        if (data.length == 0) {
			            Ext.MessageBox.alert('提示', '未选择审批人员记录！');
			            return;
			        }
			    	var nextdealtidlist = new Array();
			    	for ( var i = 0; i < data.length; i++) {
			    		nextdealtidlist.push(data[i].data.id);
					}
			        var pram = {results : me.win.type, dealtid : me.win.dealt.id, depict : me.win.depictTxt, 
	        		nextdealtidlist : nextdealtidlist};
			        me.oper.dealtSubmit(pram, me.win.grid, me.win, me);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});