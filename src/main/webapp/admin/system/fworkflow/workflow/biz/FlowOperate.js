﻿/**
 * 流程处理界面界面
 * @author lw
 */
Ext.define('system.fworkflow.workflow.biz.FlowOperate', {
	extend : 'Ext.window.Window',

	grid : null,
	oper : Ext.create('system.fworkflow.workflow.biz.FlowOper'),
	isPending : false,//是否审批
	isReturn : false,//是否退回
	isDeliver : false,//是否转交机制
	isRefusal : false,//是否拒绝机制
	data : null,//流程对象
	record : null,//流程记录对象
	dealt : null,//处理记录对象
	win : null,//嵌套数据窗体
	
	initComponent:function(){
	    var me = this;
	    me.oper.form = me;
	    
	    if (me.record != null){
	    	if (me.record.isreturn == 'true')
	    		me.isReturn = true;
	    	if (me.record.isdeliver == 'true')
	    		me.isDeliver = true;
	    	if (me.record.isrefusal == 'true')
	    		me.isRefusal = true;
	    }
	    //加载操作按钮
	    var buttonItem = new Array();
	    if (me.isPending){
		    buttonItem.push({
				xtype : "button",
			    text : '同意',
			    iconCls : 'tickIcon',
			    handler : function() {
			       me.oper.dealtShow(me.grid, 'f_fdty', '同意');
	            }
		    });
	    }
	    if (me.isReturn){
		    buttonItem.push({
				xtype : "button",
			    text : '退回',
			    iconCls : 'arrow_redoIcon',
			    handler : function() {
			    	me.oper.dealtShow(me.grid, 'f_fdth', '退回');
	            }
		    });
	    }
	    if (me.isDeliver){
		    buttonItem.push({
				xtype : "button",
			    text : '转交',
			    iconCls : 'tbar_synchronizeIcon',
			    handler : function() {
			    	me.oper.dealtShow(me.grid, 'f_fdzj', '转交');
	            }
		    });
	    }
	    if (me.isRefusal){
		    buttonItem.push({
				xtype : "button",
			    text : '拒绝',
			    iconCls : 'crossIcon',
			    handler : function() {
			    	me.oper.dealtShow(me.grid, 'f_fdjj', '拒绝');
	            }
		    });
	    }
	    buttonItem.push({
			xtype : "button",
		    text : '关闭',
		    iconCls : 'cancel',
		    handler : function() {
		       me.close();
            }
	    });
	    
	    //加载数据窗体
		me.win = Ext.create(me.data.windowpath, {
			constrain : true,
			header : false,
			isShowFlowData : true
		});
		me.win.loadFormData(me.data.dataobjectid);

		//配置操作窗体
		Ext.apply(this,{
	        width : me.win.width + 15,
	        height : me.win.height + 65,
	        title : '审批—' + me.data.datatypename,
	        modal : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[ me.win ],
	        buttons : buttonItem
	    });
	    this.callParent(arguments);
	},
	
	showWin : function(){
		var me = this;
		this.show();
		me.win.show();
	}
});