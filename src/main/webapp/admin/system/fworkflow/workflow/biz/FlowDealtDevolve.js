﻿/**
 * 工作流转交选择界面
 * @author lw
 */
Ext.define('system.fworkflow.workflow.biz.FlowDealtDevolve', {
	extend : 'Ext.window.Window',
	requires : ['system.admin.user.model.Extjs_A_employee'],
	
	win : null,//处理窗体
	oper : Ext.create('system.fworkflow.workflow.biz.FlowOper'),
	
	initComponent:function(){
	    var me = this;
	        
	    Ext.apply(this,{
	        width:600,
	        height:400,
	        resizable:false,// 改变大小
	    	layout : 'fit',
	        items :[ me.createContent() ],
	        buttons : [{
				xtype: 'label',
				text: '当前审批进行转交，请选择审批人员。'
			}, {
			    text : '提交',
			    iconCls : 'acceptIcon',
			    handler : function() {
			    	var data = me.usergrid.getSelectionModel().getSelection();
			        if (data.length == 0) {
			            Ext.MessageBox.alert('提示', '未选择审批人员记录！');
			            return;
			        }else if(data.length > 1){
			            Ext.MessageBox.alert('提示', '转交每次只能选择一人！');
			            return;
			        }
			        var obj = data[0].data;
			        var pram = {results : me.win.type, dealtid : me.win.dealt.id, depict : me.win.depictTxt, 
		        		zjbranchid : obj.branchid, zjbranchname : obj.branchname,
		        		zjemployeeid : obj.id, zjemployeename : obj.realname};
			        me.oper.dealtSubmit(pram, me.grid, me);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    
	    this.callParent(arguments);
	},
	
	createContent : function(){
		var me = this;
		var queryid = me.oper.util.getParams("company").id;

		/**
		 * 左边的树面板
		 */
		var treepanel = Ext.create("system.widget.FsdTreePanel", {
			icon : 'resources/admin/icon/chart_organisation.png',
			baseUrl : 'A_Branch/load_AsyncBranchTree.do',
			title: '组织机构',
			rootText : me.oper.util.getParams("company").name,
			rootId : me.oper.util.getParams("company").id,
			region : "west", // 设置方位
			collapsible : true,//折叠
			width: 182,
    		minWidth : 150,
			split: true
		});
		
		treepanel.on('itemclick', function(view,record,item,index,e){
			queryid = record.raw.id;
			me.usergrid.getStore().reload();
		});
		
		// 创建表格
		me.usergrid = Ext.create("system.widget.FsdGridPanel", {
			column : 'column_a_employee_role', // 显示列
			model: 'model_a_employee',
			baseUrl : 'A_Employee/load_datanotrolepage.do',
			border : false,
			tbar : [new Ext.form.TextField( {
				id : 'flowEmplRoleName',
				name : 'queryParam',
				emptyText : '请输入人员名称',
				enableKeyEvents : true,
				listeners : {
					specialkey : function(field, e) {
						if (e.getKey() == Ext.EventObject.ENTER) {
							me.usergrid.getStore().reload();
						}
					}
				},
				width : 130
			}), {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.usergrid.getStore().reload();
				}
			}]
		});
		me.usergrid.getStore().on('beforeload', function(s) {
			var name =  Ext.getCmp('flowEmplRoleName').getValue();
			var pram = {fid : queryid , name : name};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		var userpanel = Ext.create("Ext.panel.Panel", {
			title: "人员信息",
			region : "center", // 设置方位
			split: true,
			width: 350,
			layout: 'fit',
			items:[me.usergrid]
		});

		var page1_jExtPanel1_obj = new Ext.panel.Panel({
			bodyPadding: 3,
			header : false,
			border : 0,
			split: true,
			layout: 'border',
			items: [treepanel, userpanel]
		});
		return page1_jExtPanel1_obj;
	}


});