﻿/**
 *F_FLOWNODE Model
 *@author CodeSystem
 *文件名     Extjs_Model_F_flownode
 *Model名    model_f_flownode
 */

Ext.define('model_f_flownode', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'nodetype',
            type : 'string'
        }
        ,
        {
            name : 'nodetypename',
            type : 'string'
        }
        ,
        {
            name : 'name',
            type : 'string'
        }
        ,
        {
            name : 'previousname',
            type : 'string'
        }
        ,
        {
            name : 'nextname',
            type : 'string'
        }
        ,
        {
            name : 'falsename',
            type : 'string'
        }
    ]
});
