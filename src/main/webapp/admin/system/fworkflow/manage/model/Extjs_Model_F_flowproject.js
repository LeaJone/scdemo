﻿/**
 *F_FLOWPROJECT Model
 *@author CodeSystem
 *文件名     Extjs_Model_F_flowproject
 *Model名    model_f_flowproject
 */

Ext.define('model_f_flowproject', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'code',
            type : 'string'
        }
        ,
        {
            name : 'name',
            type : 'string'
        }
        ,
        {
            name : 'describe',
            type : 'string'
        }
        ,
        {
            name : 'operatename',
            type : 'string'
        }
        ,
        {
            name : 'entityname',
            type : 'string'
        }
        ,
        {
            name : 'windowname',
            type : 'string'
        }
        ,
        {
            name : 'statusname',
            type : 'string'
        }
        ,
        {
            name : 'acceptname',
            type : 'string'
        }
    ]
});
