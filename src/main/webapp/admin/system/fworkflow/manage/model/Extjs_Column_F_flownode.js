﻿/**
 *F_FLOWNODE Column
 *@author CodeSystem
 *文件名     Extjs_Column_F_flownode
 *Columns名  column_f_flownode
 *xtype: 'textfield', 'numbercolumn', 'booleancolumn', 'datecolumn'   format : '0.00'
 *align: 'left', 'center', 'right'
 */

Ext.define('column_f_flownode', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            align : 'center',
            width : 40
        }
        ,
        {
            header : '名称',
            dataIndex : 'name',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '类型',
            dataIndex : 'nodetypename',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '上一节点',
            dataIndex : 'previousname',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '下一节点',
            dataIndex : 'nextname',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : 'False节点',
            dataIndex : 'falsename',
            sortable : true,
            align : 'left',
            flex : 1
        }
    ]
});
