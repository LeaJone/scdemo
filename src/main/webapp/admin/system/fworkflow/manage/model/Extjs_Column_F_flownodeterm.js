﻿/**
 *F_FlowNodeTerm Column
 *@author CodeSystem
 *文件名     Extjs_Column_F_flownodeterm
 *Columns名  column_f_flownodeterm
 *xtype: 'textfield', 'numbercolumn', 'booleancolumn', 'datecolumn'   format : '0.00'
 *align: 'left', 'center', 'right'
 */

Ext.define('column_f_flownodeterm', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            align : 'center',
            width : 40
        }
        ,
        {
            header : '部门',
            dataIndex : 'operatorname',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '人员名称',
            dataIndex : 'valuedepict',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '顺序号',
            dataIndex : 'sort',
            sortable : true,
            align : 'left',
            flex : 1
        }
    ]
});
