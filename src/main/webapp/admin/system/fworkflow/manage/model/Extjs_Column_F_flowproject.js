﻿/**
 *F_FLOWPROJECT Column
 *@author CodeSystem
 *文件名     Extjs_Column_F_flowproject
 *Columns名  column_f_flowproject
 *xtype: 'textfield', 'numbercolumn', 'booleancolumn', 'datecolumn'   format : '0.00'
 *align: 'left', 'center', 'right'
 */

Ext.define('column_f_flowproject', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            align : 'center',
            width : 40
        }
        ,
        {
            header : '流程编码',
            dataIndex : 'code',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '流程名称',
            dataIndex : 'name',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '描述',
            dataIndex : 'describe',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '操作类',
            dataIndex : 'operatename',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '实体类',
            dataIndex : 'entityname',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '展示窗体',
            dataIndex : 'windowname',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '项目状态',
            dataIndex : 'statusname',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '受理状态',
            dataIndex : 'acceptname',
            sortable : true,
            align : 'left',
            flex : 1
        }
    ]
});
