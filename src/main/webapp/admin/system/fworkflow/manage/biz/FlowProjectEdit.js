﻿/**
 * 流程项目编辑界面
 * @author lw
 */
Ext.define('system.fworkflow.manage.biz.FlowProjectEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	typeobj : null,
	isEdit : false,
	oper : Ext.create('system.fworkflow.manage.biz.FlowProjectOper'),
	initComponent:function(){
	    var me = this;

	    var typeid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '类型ID',
	    	name: 'typeid',
	    	value : me.typeobj != null ? me.typeobj.id : '',
	    });
	    var typename = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '类型名称',
	    	name: 'typename',
	    	value : me.typeobj != null ? me.typeobj.name : '',
	    });
	    
	    var code = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '流程编码',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 370,
	    	name: 'code',
	    	maxLength: 50,
	    	allowBlank: false,
	    	readOnly : true
	    });
	    
	    var code1 = null;
	    var code2 = null;
	    if (!me.isEdit){
	    	var codename = '编码前缀';
		    switch (me.typeobj.typecode) {
			case 'f_lcywzd':
				code2 = Ext.create('system.widget.FsdComboBoxZD',{
		            fieldLabel:'类型字典',
			        labelAlign:'right',
			    	labelWidth:90,
			        width : 370,
			        key : me.typeobj.dictionarykey,//参数
			        allowBlank: false,
			        zCallback: function(vcode, vname){ 
					    code.setValue(code1.getValue() + vcode);
		            }
			    });
				break;
			case 'f_lcywdx':
				code2 = Ext.create('system.widget.FsdTreeComboBox',{
			    	fieldLabel : '类型对象',
			    	labelAlign:'right',
			    	labelWidth:90,
			    	rootText : me.oper.util.getParams("company").name,
			    	rootId : me.typeobj.structurecode,
			        width : 370,
			        baseUrl: me.typeobj.structureurl,//访问路劲	        
			        allowBlank: false,
			        rootVisible : false,
			        zCallback: function(obj){ 
				    	code.setValue(code1.getValue() + obj.id);
		            }
			    });
				break;
			case 'f_lcywbm':
				codename = '编码输入';
				break;
			}
	    	code1 = Ext.create('system.widget.FsdTextField',{
		    	fieldLabel: codename,
		    	labelAlign:'right',
		    	labelWidth:90,
		        width : 370,
		    	maxLength: 20,
		    	allowBlank: false,
		    	zOnChange: function(obj, newValue, oldValue, eOpts ){ 
		    		if (code2.getValue() != null)
		    			code.setValue(newValue + code2.getValue());
		    		else
		    			code.setValue(newValue);
	            }
		    });
	    }
	    
	    var name = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '流程名称',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 370,
	    	name: 'name',
	    	maxLength: 25,
	    	allowBlank: false
	    });
	    
	    var describe = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '描述',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 370,
	    	name: 'describe',
	    	maxLength: 250
	    });
	    
	    var operatename = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '操作类名称',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 370,
	    	name: 'operatename',
	    	maxLength: 25,
	    	allowBlank: false
	    });
	    
	    var operatepath = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '操作类全称',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 370,
	    	name: 'operatepath',
	    	maxLength: 100,
	    	allowBlank: false
	    });
	    
	    var entityname = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '实体类名称',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 370,
	    	name: 'entityname',
	    	maxLength: 25
	    });
	    
	    var entitypath = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '实体类全称',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 370,
	    	name: 'entitypath',
	    	maxLength: 100
	    });
	    
	    var windowname = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '展示窗体名称',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 370,
	    	name: 'windowname',
	    	maxLength: 25,
	    	allowBlank: false
	    });
	    
	    var windowpath = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '展示窗体全称',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 370,
	    	name: 'windowpath',
	    	maxLength: 100,
	    	allowBlank: false
	    });
	    
	    var sort = Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '顺序号',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 370,
	    	name: 'sort',
	    	maxLength: 3,
	    	minValue: 0,
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank: false
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [{
                name: "id",
                xtype: "hidden"
            } , typeid, typename, code, code1, code2, name, describe, operatename, operatepath, 
            entityname, entitypath, windowname, windowpath, sort]
        });
	    
	    Ext.apply(this,{
	        width:430,
	        height:480,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'lcxmbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
			    	if (code1 != null && code2 != null)
			    		code.setValue(code1.getValue() + code2.getValue());
			    	else if (code1 != null)
			    		code.setValue(code1.getValue());
    				me.oper.formSubmitXM(form, me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});