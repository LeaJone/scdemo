﻿/**
 * 流程节点条件编辑界面 人员审批
 * @author lw
 */
Ext.define('system.fworkflow.manage.biz.FlowNodeTermEditEmpl', {
	extend : 'Ext.window.Window',
	requires : [ 'system.abasis.employee.model.Extjs_A_employee'],
	grid : null,
	nodeobj : null,
	oper : Ext.create('system.fworkflow.manage.biz.FlowProjectOper'),
	initComponent:function(){
	    var me = this;
	    
		var idNo = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '编号',
	    	name : 'id'
	    });

	    var nodeid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '项目ID',
	    	name: 'nodeid',
	    	value : me.nodeobj != null ? me.nodeobj.id : '',
	    });
	    var nodename = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '项目名称',
	    	name: 'nodename',
	    	value : me.nodeobj != null ? me.nodeobj.name : '',
	    });
	    
	    var valuedepict = Ext.create('system.widget.FsdTextGrid', {
	    	zFieldLabel : '人员姓名', 
	    	zLabelWidth : 60,
		    width : 350,
		    zName : 'valuedepict',
		    zColumn : 'column_a_employee',
		    zModel : 'model_a_employee',
		    zBaseUrl : 'A_Employee/load_data.do',
		    zAllowBlank : false,
		    zIsText1 : true,
		    zTxtLabel1 : '人员姓名',
		    zWinWidth : 700,
		    zFunChoose : function(data){
		    	if (idNo.getValue() == data.id){
		            Ext.MessageBox.alert('提示', '不得选择当前节点！');
		    		return false;
		    	}
		    	valuedepict.setValue(data.realname);
		    	valueinfo.setValue(data.id);
		    	operator.setValue(data.branchid);
		    	operatorname.setValue(data.branchname);
		    },
		    zFunQuery : function(txt1, txt2, txt3, txt4){
		    	return {fid : me.oper.util.getParams("company").id, name : txt1};
		    }
	    });
	    var valueinfo = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '人员ID',
	    	name: 'valueinfo'
	    });
	    var operator = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '部门ID',
	    	name: 'operator'
	    });
	    var operatorname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '部门名称',
	    	name: 'operatorname'
	    });
	    
	    var sort = new Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '顺序号',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 350,
	    	name: 'sort',
	    	maxLength: 3,
	    	minValue: 0,
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank: false
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [idNo, nodeid, nodename, valueinfo, valuedepict, sort, operator, operatorname]
        });
	    
	    Ext.apply(this,{
	        width:410,
	        height:150,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'lcxmbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.formSubmitTJ(form, me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});