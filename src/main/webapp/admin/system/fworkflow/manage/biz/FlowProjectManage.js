﻿/**

 * 流程项目管理界面
 * @author lw
 */

Ext.define('system.fworkflow.manage.biz.FlowProjectManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.admin.tools.model.Extjs_Column_SortOperate',
		         'system.admin.tools.model.Extjs_Model_SortOperate',
		         'system.fworkflow.manage.model.Extjs_Column_F_flowproject',
	             'system.fworkflow.manage.model.Extjs_Model_F_flowproject',
		         'system.fworkflow.manage.model.Extjs_Column_F_flownode',
	             'system.fworkflow.manage.model.Extjs_Model_F_flownode',
	             'system.fworkflow.manage.model.Extjs_Column_F_flownodeterm',
	             'system.fworkflow.manage.model.Extjs_Model_F_flownodeterm'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.fworkflow.manage.biz.FlowProjectOper'),
	
	createContent : function(){
		var me = this;
		
		var projectobj = null;//项目
		var nodeobj = null;//节点
		var typeobj = null;
		
		/**
		 * 左边的树面板
		 */
		var treepanel = Ext.create("system.widget.FsdTreePanel", {
			icon : 'resources/admin/icon/chart_organisation.png',
			baseUrl : 'f_flowtype/load_treedata.do',
			title: '流程类型',
			rootText : '流程类型',
			rootId : 'FSDFLOWTYPE',
			region : 'west', // 设置方位
			collapsible : true,//折叠
			width: '15%',
			split: true
		});
		treepanel.on('itemclick', function(view,record,item,index,e){
			projectobj = null;
			nodeobj = null;//节点ID
			typeobj = record.raw.obj;
			xmgrid.getStore().loadPage(1);
			jdgrid.getStore().removeAll();
			tjgrid.getStore().removeAll();
		});
		
		var xmQueryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入流程项目名称',
			enableKeyEvents : true,
			width : 120,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						xmgrid.getStore().loadPage(1);
					}
				}
			}
		});
		
		// 创建流程项目
		var xmgrid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_f_flowproject', // 显示列
			model: 'model_f_flowproject',
			baseUrl : 'f_flowproject/load_pagedata.do',
			tbar : {
				enableOverflow: true,//控件过多，出现下拉按钮
				defaults: {
			        height : 53
				},
				items: [{
			        xtype: 'buttongroup',
			        columns: 3,
			        items: [{
				xtype : "button",
				text : "添加",
				iconCls : 'page_addIcon',
				popedomCode : 'lcxmbj',
				handler : function(button) {
					me.oper.addXM(xmgrid, typeobj);
				}
			}, {
				xtype : "button",
				text : "修改",
				iconCls : 'page_editIcon',
				popedomCode : 'lcxmbj',
				handler : function(button) {
					me.oper.editorXM(xmgrid);
				}
			}, {
				xtype : "button",
				text : "删除",
				iconCls : 'page_deleteIcon',
				popedomCode : 'lcxmsc',
				handler : function(button) {
				    me.oper.delXM(xmgrid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.viewXM(xmgrid);
				}
			}, {
				text : '排序',
				iconCls : 'up',
				popedomCode : 'lcxmbj',
				handler : function() {
					if (typeobj == null || typeobj.id == 'FSDFLOWTYPE'){
						Ext.MessageBox.alert('提示', '请先选择具体流程类型！');
						return;
					}
					var name =  xmQueryText.getValue();
					var typeid = typeobj.id;
					var pram = {typeid : typeid, name : name};
					me.oper.util.sortOperate(
							xmgrid, 
							'column_name', 
							'model_sortoperate',
							xmgrid.baseUrl,
							'f_flowproject/sort_flowproject_qx_lcxmbj.do',
							function(){return pram;},
				        	function(){treepanel.getStore().load();});
				}
			}]}, {
		        xtype: 'buttongroup',
		        columns: 1,
		        items: [{
				xtype : "fsdbutton",
				popedomCode : 'lcxmbj',
				text : "项目启用",
				iconCls : 'tickIcon',
				handler : function(button) {
				    me.oper.isEnabledXM(xmgrid, true, 'lcxm');
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'lcxmbj',
				text : "项目停用",
				iconCls : 'crossIcon',
				handler : function(button) {
				    me.oper.isEnabledXM(xmgrid, false, 'lcxm');
				}
			}]}, {
		        xtype: 'buttongroup',
		        columns: 1,
		        items: [{
				xtype : "fsdbutton",
				popedomCode : 'lcxmbj',
				text : "受理启用",
				iconCls : 'tickIcon',
				handler : function(button) {
				    me.oper.isEnabledXM(xmgrid, true, 'lcsl');
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'lcxmbj',
				text : "受理停用",
				iconCls : 'crossIcon',
				handler : function(button) {
				    me.oper.isEnabledXM(xmgrid, false, 'lcsl');
				}
			}]}, {
		        xtype: 'buttongroup',
		        columns: 1,
		        items: [xmQueryText , {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					xmgrid.getStore().loadPage(1);
				}
			}]}, {
		        xtype: 'buttongroup',
		        columns: 1,
		        items: [{
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
	            rowspan: 2,
	            iconAlign: 'top',
				handler : function() {
					xmgrid.getStore().reload();
				}
			}]}]}			
		});
		xmgrid.getStore().on('beforeload', function(s) {
			var name =  xmQueryText.getValue();
			var typeid = '';
			if (typeobj != null)
				typeid = typeobj.id;
			var pram = {typeid : typeid, name : name};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		xmgrid.on('selectionchange', function(obj, selected, eOpts){
			if (selected.length != 1){
				projectobj = null;
				nodeobj = null;
				jdgrid.getStore().removeAll();
				tjgrid.getStore().removeAll();
			}else{
				projectobj = selected[0].data;
				nodeobj = null;//节点ID
				jdgrid.getStore().loadPage(1);
				tjgrid.getStore().removeAll();
			}
		});
		
		
		// 创建流程节点
		var jdgrid = Ext.create("system.widget.FsdGridPanel", {
			column : 'column_f_flownode', // 显示列
			model: 'model_f_flownode',
			baseUrl : 'f_flownode/load_databyprojectid.do',
			zAutoLoad : false,
			tbar : [{
				xtype : "button",
				text : "添加",
				iconCls : 'page_addIcon',
				popedomCode : 'lcxmbj',
				handler : function(button) {
					me.oper.addJD(jdgrid, projectobj);
				}
			}, {
				xtype : "button",
				text : "修改",
				iconCls : 'page_editIcon',
				popedomCode : 'lcxmbj',
				handler : function(button) {
					me.oper.editorJD(jdgrid);
				}
			}, {
				xtype : "button",
				text : "删除",
				iconCls : 'page_deleteIcon',
				popedomCode : 'lcxmsc',
				handler : function(button) {
				    me.oper.delJD(jdgrid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.viewJD(jdgrid);
				}
			}, '->', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					jdgrid.getStore().reload();
				}
			}]			
		});
		jdgrid.getStore().on('beforeload', function(s) {
			var pram = {projectid : projectobj.id};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		jdgrid.on('selectionchange', function(obj, selected, eOpts){
			if (selected.length != 1){
				nodeobj = null;
				tjgrid.getStore().removeAll();
			}else{
				nodeobj = selected[0].data;
				tjgrid.getStore().loadPage(1);
			}
		});
		
		// 创建流程节点条件
		var tjgrid = Ext.create("system.widget.FsdGridPanel", {
			column : 'column_f_flownodeterm', // 显示列
			model: 'model_f_flownodeterm',
			baseUrl : 'f_flownodeterm/load_ObjectListByNodeID.do',
			zAutoLoad : false,
			tbar : [{
				xtype : "button",
				text : "添加",
				iconCls : 'page_addIcon',
				popedomCode : 'lcxmbj',
				handler : function(button) {
					me.oper.addTJ(tjgrid, nodeobj);
				}
			}, {
				xtype : "button",
				text : "修改",
				iconCls : 'page_editIcon',
				popedomCode : 'lcxmbj',
				handler : function(button) {
					me.oper.editorTJ(tjgrid);
				}
			}, {
				xtype : "button",
				text : "删除",
				iconCls : 'page_deleteIcon',
				popedomCode : 'lcxmsc',
				handler : function(button) {
				    me.oper.delTJ(tjgrid);
				}
			}, {
				text : '排序',
				iconCls : 'up',
				handler : function() {
					var pram = {nodeid : nodeobj.id};
					me.oper.util.sortOperate(
							tjgrid, 
							tjgrid.column, 
							tjgrid.model,
							tjgrid.baseUrl,
							'f_flownodeterm/sort_flownodeterm_qx_lcxmbj.do',
							function(){return pram;},
				        	function(){});
				}
			}, '->', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					tjgrid.getStore().reload();
				}
			}]			
		});
		tjgrid.getStore().on('beforeload', function(s) {
			var pram = {nodeid : nodeobj.id};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		var xmpanel = Ext.create("Ext.panel.Panel", {
			layout:'fit',
			frame:true,
			split: true,
			region : 'center',
			title: '流程项目',
			width: '45%',
			items: [xmgrid]
		});
		
		var jdpanel = Ext.create("Ext.panel.Panel", {
			border : 0,
			layout: 'fit',
			region: 'north',
			split: true,
			frame:true,
			title: '项目节点',
			height: '50%',
			items: [jdgrid]
		});
		
		var tjpanel = Ext.create("Ext.panel.Panel", {
			border : 0,
			layout: 'fit',
			region: 'center',
			split: true,
			frame:true,
			title: '节点条件',
			height: '50%',
			items: [tjgrid]
		});
		
		var all1panel = Ext.create("Ext.panel.Panel", {
			layout: 'border',
			padding : 0,
			border : false,
			frame:true,
			split: true,
			region: 'east',
			width: '40%',
			items: [jdpanel, tjpanel]
		});
		
		var allPanel = Ext.create('Ext.panel.Panel', {
			bodyPadding: 3,
			header : false,
			border : 0,
			split: true,
			layout: 'border',
			items: [all1panel,xmpanel,treepanel]
		});
		
		return allPanel;
	}
});