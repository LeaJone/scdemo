/**
 * 流程项目操作类
 * @author lw
 */

Ext.define('system.fworkflow.manage.biz.FlowProjectOper', {

	form : null,
	util : Ext.create('system.util.util'),

	//=====================++流程项目++====================
	/**
     * 添加
     */
	addXM : function(grid, typeobj){
		if (typeobj == null || typeobj.id == 'FSDFLOWTYPE'){
			Ext.MessageBox.alert('提示', '请先选择具体流程类型！');
			return;
		}
	    var win = Ext.create('system.fworkflow.manage.biz.FlowProjectEdit', {
	    	typeobj : typeobj,
	    	isEdit : false
	    });
        win.setTitle('流程项目添加');
        win.grid = grid;
        win.modal = true;
        win.show();
	},
	
	/**
     * 表单提交
     */
     formSubmitXM : function(formpanel, grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('f_flowproject/save_flowproject_qx_lcxmbj.do' , formpanel.form , '正在提交数据,请稍候.....',
			function(form, action){
			    formpanel.up('window').close();
			    grid.getStore().reload();
				Ext.MessageBox.alert('提示', '保存成功！');
			});
        }
     },
     
     /**
      * 删除
      */
     delXM : function(grid){
    	 var me = this;
 		 // 获取选中的行
 	     var data = grid.getSelectionModel().getSelection();
         if (data.length == 0) {
             Ext.MessageBox.alert('提示', '请先选中要删除的项目！');
         }else {
        	 Ext.MessageBox.show({
        		 title : '询问',
        		 msg : '您确定要删除吗?',
 			     width : 250,
 				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                     if (btn == 'yes') {
                    	 var dir = new Array();　
                         Ext.Array.each(data, function(items) {
                             var id = items.data.id;
                             dir.push(id);
             			});
             			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('f_flowproject/del_flowproject_qx_lcxmsc.do' , param ,
                        function(response, options){
                        	grid.getStore().reload(); 
                        }, null, me.form);
                     }
                 }
             });
         }
 	},
 	
 	/**
     * 修改
     */
	editorXM : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{

            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            
            var win = Ext.create('system.fworkflow.manage.biz.FlowProjectEdit', {
    	    	isEdit : true
    	    });
            win.setTitle('流程项目修改');
            win.modal = true;
            win.grid = grid;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm(), 'f_flowproject/load_flowprojectbyid.do', param, null, 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},
 	
 	/**
     * 查看
     */
	viewXM : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if(data.length > 0){
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.fworkflow.manage.biz.FlowProjectEdit', {
    	    	isEdit : true
    	    });
            win.setTitle('流程项目查看');
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm(), 'f_flowproject/load_flowprojectbyid.do', param, null, 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},
	
	/**
     * 是否启用停用
     */
	isEnabledXM : function(grid, isQT, type){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
		        msg : '您确定要修改项目状态吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
					    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
			            });
             			var pram = {ids : dir, isenabled : isQT, type : type};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('f_flowproject/enabled_flowproject_qx_lcxmbj.do', param,
                        function(response, options){
            				Ext.MessageBox.alert('提示', '项目状态修改成功！');
                        	grid.getStore().reload(); 
                        });
                    }
                }
            });
        }
	},
	
	//=====================++流程节点++====================
	/**
     * 添加
     */
	addJD : function(grid, projectobj){
		if (projectobj == null){
			Ext.MessageBox.alert('提示', '请先选择具体流程项目！');
			return;
		}
	    var win = Ext.create('system.fworkflow.manage.biz.FlowNodeEdit', {
	    	projectobj : projectobj
	    });
        win.setTitle('流程节点添加');
        win.grid = grid;
        win.modal = true;
        win.show();
	},
	
	/**
     * 表单提交
     */
     formSubmitJD : function(formpanel, grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('f_flownode/save_flownode_qx_lcxmbj.do' , formpanel.form , '正在提交数据,请稍候.....',
			function(form, action){
			    formpanel.up('window').close();
			    grid.getStore().reload();
				Ext.MessageBox.alert('提示', '保存成功！');
			});
        }
     },
     
     /**
      * 删除
      */
     delJD : function(grid){
    	 var me = this;
 		 // 获取选中的行
 	     var data = grid.getSelectionModel().getSelection();
         if (data.length == 0) {
             Ext.MessageBox.alert('提示', '请先选中要删除的节点！');
         }else {
        	 Ext.MessageBox.show({
        		 title : '询问',
        		 msg : '您确定要删除吗?',
 			     width : 250,
 				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                     if (btn == 'yes') {
                    	 var dir = new Array();　
                         Ext.Array.each(data, function(items) {
                             var id = items.data.id;
                             dir.push(id);
             			});
             			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('f_flownode/del_flownode_qx_lcxmsc.do' , param ,
                        function(response, options){
                        	grid.getStore().reload(); 
                        }, null, me.form);
                     }
                 }
             });
         }
 	},
 	
 	/**
     * 修改
     */
	editorJD : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的节点！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个节点！');
        }else{

            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            
            var win = Ext.create('system.fworkflow.manage.biz.FlowNodeEdit');
            win.setTitle('流程节点修改');
            win.modal = true;
            win.grid = grid;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm(), 'f_flownode/load_flownodebyid.do', param, null, 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},
 	
 	/**
     * 查看
     */
	viewJD : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if(data.length > 0){
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.fworkflow.manage.biz.FlowNodeEdit');
            win.setTitle('流程节点查看');
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm(), 'f_flownode/load_flownodebyid.do', param, null, 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},
	
	//=====================++流程节点条件++====================
	/**
     * 添加
     */
	addTJ : function(grid, nodeobj){
		if (nodeobj == null){
			Ext.MessageBox.alert('提示', '请先选择具体流程节点！');
			return;
		}
	    var win = Ext.create('system.fworkflow.manage.biz.FlowNodeTermEditEmpl', {
	    	nodeobj : nodeobj
	    });
        win.setTitle('流程节点条件添加');
        win.grid = grid;
        win.modal = true;
        win.show();
	},
	
	/**
     * 表单提交
     */
     formSubmitTJ : function(formpanel, grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('f_flownodeterm/save_flownodeterm_qx_lcxmbj.do' , formpanel.form , '正在提交数据,请稍候.....',
			function(form, action){
			    formpanel.up('window').close();
			    grid.getStore().reload();
				Ext.MessageBox.alert('提示', '保存成功！');
			});
        }
     },
     
     /**
      * 删除
      */
     delTJ : function(grid){
    	 var me = this;
 		 // 获取选中的行
 	     var data = grid.getSelectionModel().getSelection();
         if (data.length == 0) {
             Ext.MessageBox.alert('提示', '请先选中要删除的节点条件！');
         }else {
        	 Ext.MessageBox.show({
        		 title : '询问',
        		 msg : '您确定要删除吗?',
 			     width : 250,
 				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                     if (btn == 'yes') {
                    	 var dir = new Array();　
                         Ext.Array.each(data, function(items) {
                             var id = items.data.id;
                             dir.push(id);
             			});
             			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('f_flownodeterm/del_flownodeterm_qx_lcxmsc.do' , param ,
                        function(response, options){
                        	grid.getStore().reload(); 
                        }, null, me.form);
                     }
                 }
             });
         }
 	},
 	
 	/**
     * 修改
     */
	editorTJ : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的节点条件！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个节点条件！');
        }else{

            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            
            var win = Ext.create('system.fworkflow.manage.biz.FlowNodeTermEditEmpl');
            win.setTitle('流程节点条件修改');
            win.modal = true;
            win.grid = grid;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm(), 'f_flownodeterm/load_flownodetermbyid.do', param, null, 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},
 	
 	/**
     * 查看
     */
	viewTJ : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if(data.length > 0){
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.fworkflow.manage.biz.FlowNodeTermEditEmpl');
            win.setTitle('流程节点条件查看');
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm(), 'f_flownodeterm/load_flownodetermbyid.do', param, null, 
	        function(response, options, respText){
	            win.show();
	        });
        }
	}
});