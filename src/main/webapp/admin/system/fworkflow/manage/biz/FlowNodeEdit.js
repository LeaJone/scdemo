﻿/**
 * 流程节点编辑界面
 * @author lw
 */
Ext.define('system.fworkflow.manage.biz.FlowNodeEdit', {
	extend : 'Ext.window.Window',
	requires : [ 'system.fworkflow.manage.model.Extjs_Column_F_flownode',
	             'system.fworkflow.manage.model.Extjs_Model_F_flownode'],
	grid : null,
	projectobj : null,
	oper : Ext.create('system.fworkflow.manage.biz.FlowProjectOper'),
	initComponent:function(){
	    var me = this;
	    
		var idNo = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '编号',
	    	name : 'id'
	    });

	    var projectid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '项目ID',
	    	name: 'projectid',
	    	value : me.projectobj != null ? me.projectobj.id : '',
	    });
	    var projectname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '项目名称',
	    	name: 'projectname',
	    	value : me.projectobj != null ? me.projectobj.name : '',
	    });
	    

		var nodetype = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'节点类型',
	        labelAlign:'right',
	    	labelWidth : 110,
		    width : 350,
	    	name: 'nodetype',
	        key : 'f_fnjdlx',//参数
	        allowBlank: false,
    	    hiddenName : 'nodetypename'//提交隐藏域Name
	    });
	    var nodetypename = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '节点类型名',
	    	name: 'nodetypename'
	    });
	    
	    var name = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '名称',
	    	labelAlign:'right',
	    	labelWidth : 110,
		    width : 350,
	    	name: 'name',
	    	maxLength: 50,
	    	allowBlank: false
	    });
	    
	    var previousname = Ext.create('system.widget.FsdTextGrid', {
	    	zFieldLabel : '上一节点', 
	    	zLabelWidth : 110,
		    width : 350,
		    zName : 'previousname',
		    zColumn : 'column_f_flownode',
		    zModel : 'model_f_flownode',
		    zBaseUrl : 'f_flownode/load_databyprojectid.do',
		    zAllowBlank : false,
			zIsPage : false,
		    zFunChoose : function(data){
		    	if (idNo.getValue() == data.id){
		            Ext.MessageBox.alert('提示', '不得选择当前节点！');
		    		return false;
		    	}
		    	previousname.setValue(data.name);
		    	previousid.setValue(data.id);
		    },
		    zFunQuery : function(txt1, txt2, txt3, txt4){
		    	return {projectid : projectid.getValue(), isproject : 'true'};
		    }
	    });
	    var previousid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '上一节点ID',
	    	name: 'previousid'
	    });
	    
	    var nextname = Ext.create('system.widget.FsdTextGrid', {
	    	zFieldLabel : '下一节点(True)', 
	    	zLabelWidth : 110,
		    width : 350,
		    zName : 'nextname',
		    zColumn : 'column_f_flownode',
		    zModel : 'model_f_flownode',
		    zBaseUrl : 'f_flownode/load_databyprojectid.do',
			zIsPage : false,
		    zFunChoose : function(data){
		    	if (idNo.getValue() == data.id){
		            Ext.MessageBox.alert('提示', '不得选择当前节点！');
		    		return false;
		    	}
		    	nextname.setValue(data.name);
		    	nextid.setValue(data.id);
		    	nexttype.setValue(data.nodetype);
		    	nexttypename.setValue(data.nodetypename);
		    },
		    zFunQuery : function(txt1, txt2, txt3, txt4){
		    	return {projectid : projectid.getValue()};
		    }
	    });
	    var nextid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '下一节点ID(True)',
	    	name: 'nextid'
	    });
	    var nexttype = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '下一节点类型',
	    	name: 'nexttype'
	    });
	    var nexttypename = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '下一节点类型名',
	    	name: 'nexttypename'
	    });
	    
	    var falsename = Ext.create('system.widget.FsdTextGrid', {
	    	zFieldLabel : '下一节点(False)', 
	    	zLabelWidth : 110,
		    width : 350,
		    zName : 'falsename',
		    zColumn : 'column_f_flownode',
		    zModel : 'model_f_flownode',
		    zBaseUrl : 'f_flownode/load_databyprojectid.do',
			zIsPage : false,
		    zFunChoose : function(data){
		    	if (idNo.getValue() == data.id){
		            Ext.MessageBox.alert('提示', '不得选择当前节点！');
		    		return false;
		    	}
		    	falsename.setValue(data.name);
		    	falseid.setValue(data.id);
		    },
		    zFunQuery : function(txt1, txt2, txt3, txt4){
		    	return {projectid : projectid.getValue()};
		    }
	    });
	    var falseid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '下一节点ID(False)',
	    	name: 'falseid'
	    });
	    
	    var checkboxgroup = new Ext.form.CheckboxGroup({
		    fieldLabel: '节点机制',
		    labelAlign:'right',
	    	labelWidth : 110,
		    width : 350,
		    columns : 2,
		    items: [
		        {
			    name : 'isdeliver',    
			    boxLabel: '是否转交机制',
			    inputValue: 'true'
			    }, 
			    {
				name : 'isextract',
				boxLabel: '是否提取机制',
				inputValue: 'true'
				}
			    , 
				{
		    	name : 'isrefusal',
			    boxLabel: '是否拒绝机制',
			    inputValue: 'true'
			    }, 
			    {
			    name : 'isreturn',  
			    boxLabel: '是否退回机制',
			    inputValue: 'true'
			    }
			    ]
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [idNo, projectid, projectname, nodetypename, previousid, 
			         nextid, nexttype, nexttypename, falseid, 
			         nodetype, name, previousname, nextname, falsename, checkboxgroup]
        });
	    
	    Ext.apply(this,{
	        width:430,
	        height:280,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'lcxmbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.formSubmitJD(form, me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});