﻿/**
 *a_employee Model
 *@author CodeSystem
 *文件名     Extjs_Column_A_memberSelect
 *Columns名  column_a_member_select
 */

Ext.define('column_a_member_select', {
    columns: [
		{
            xtype : 'rownumberer',
            text : 'NO',
            sortable : false,
            align : 'center',
            width : 30
        }
        ,
        {
			header : '编号',
            sortable : false,
			dataIndex : 'code'
		}
        ,
        {
            header : '姓名',
            sortable : false,
            dataIndex : 'realname'
        }
        ,
        {
            header : '助记符',
            sortable : false,
            dataIndex : 'symbol'
        }
        ,
        {
            header : '性别',
            sortable : false,
            dataIndex : 'sexname'
        }
    ]
});
