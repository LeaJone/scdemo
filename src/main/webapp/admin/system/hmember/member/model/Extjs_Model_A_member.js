﻿/**
 *a_employee Model
 *@author CodeSystem
 *文件名     Extjs_Model_A_member
 *Model名    model_a_member
 */

Ext.define('model_a_member', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'companyname',
            type : 'string'
        }
        ,
        {
            name : 'code',
            type : 'string'
        }
        ,
        {
            name : 'loginname',
            type : 'string'
        }
        ,
        {
            name : 'nickname',
            type : 'string'
        }
        ,
        {
            name : 'realname',
            type : 'string'
        }
        ,
        {
            name : 'symbol',
            type : 'string'
        }
        ,
        {
            name : 'sex',
            type : 'string'
        }
        ,
        {
            name : 'sexname',
            type : 'string'
        }
        ,
        {
            name : 'status',
            type : 'string'
        }
        ,
        {
            name : 'statusname',
            type : 'string'
        }
        ,
        {
            name : 'type',
            type : 'string'
        }
        ,
        {
            name : 'typename',
            type : 'string'
        }
        ,
        {
            name : 'iswechatbind',
            type : 'string'
        }
        ,
        {
            name : 'iswechatpush',
            type : 'string'
        }
    ]
});