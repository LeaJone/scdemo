﻿/**
 *a_employee Model
 *@author CodeSystem
 *文件名     Extjs_Column_A_member
 *Columns名  column_a_member
 */

Ext.define('column_a_member', {
    columns: [
		{
            xtype : 'rownumberer',
            text : 'NO',
            sortable : false,
            align : 'center',
            width : 30
        }
        ,
        {
			header : '编号',
            sortable : false,
			dataIndex : 'code',
			align : 'center'
		}
        ,
        {
            header : '备注名称',
            sortable : false,
            dataIndex : 'nickname',
            align : 'center'
        }
        ,
        {
            header : '真实姓名',
            sortable : false,
            dataIndex : 'realname',
            align : 'center'
        }
        ,
        {
            header : '助记符',
            sortable : false,
            dataIndex : 'symbol',
            align : 'center'
        }
        ,
        {
            header : '性别',
            sortable : false,
            dataIndex : 'sexname',
            align : 'center'
        }
        ,
        {
            header : '类型',
            sortable : false,
            dataIndex : 'typename',
            align : 'center'
        }
        ,
        {
            header : '状态',
            sortable : false,
            dataIndex : 'status',
            align : 'center',
            renderer :function(value, metaData, record ){
				if(value == 'ryztqy'){
					value="<font style='color:green;'>启用</font>";
				}else{
					value="<font style='color:red;'>停用</font>";
				}
				return value;
			}
        }
        ,
        {
            header : '微信绑定',
            sortable : false,
            dataIndex : 'iswechatbind',
            align : 'center',
			renderer :function(value, metaData, record){
				if(value == 'true'){
					value="<font style='color:green;'>已绑定</font>";
				}else{
					value="<font style='color:red;'>未绑定</font>";
				}
				return value;
			}
        }
        ,
        {
            header : '是否推送',
            sortable : false,
            dataIndex : 'iswechatpush',
            align : 'center',
			renderer :function(value, metaData, record){
				if(value == 'true'){
					value="<font style='color:green;'>是</font>";
				}else{
					value="<font style='color:red;'>否</font>";
				}
				return value;
			}
        }
    ]
});