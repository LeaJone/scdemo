﻿/**
 *a_employee Model
 *@author CodeSystem
 *文件名     Extjs_Column_A_memberRecycle
 *Columns名  column_a_memberrecycle
 */

Ext.define('column_a_memberrecycle', {
    columns: [
		{
            xtype : 'rownumberer',
            text : 'NO',
            sortable : false,
            align : 'center',
            width : 30
        }
		,
        {
            header : '单位',
            dataIndex : 'companyname'
        }
        ,
        {
			header : '编号',
            sortable : false,
			dataIndex : 'code'
		}
        ,
        {
            header : '备注名称',
            sortable : false,
            dataIndex : 'nickname'
        }
        ,
        {
            header : '真实姓名',
            sortable : false,
            dataIndex : 'realname'
        }
        ,
        {
            header : '助记符',
            sortable : false,
            dataIndex : 'symbol'
        }
        ,
        {
            header : '性别',
            sortable : false,
            dataIndex : 'sexname'
        }
        ,
        {
            header : '类型',
            sortable : false,
            dataIndex : 'typename'
        }
        ,
        {
            header : '状态',
            sortable : false,
            dataIndex : 'statusname'
        }
        ,
        {
            header : '微信绑定',
            sortable : false,
            dataIndex : 'iswechatbind',
			renderer :function(value ,metaData ,record ){
				if(value == 'true'){
					value="<font style='color:green;'>已绑定</font>";
				}else{
					value="未绑定";
				}
				return value;
			}
        }
        ,
        {
            header : '是否推送',
            sortable : false,
            dataIndex : 'iswechatpush',
			renderer :function(value ,metaData ,record ){
				if(value == 'true'){
					value="<font style='color:green;'>是</font>";
				}else{
					value="否";
				}
				return value;
			}
        }
    ]
});