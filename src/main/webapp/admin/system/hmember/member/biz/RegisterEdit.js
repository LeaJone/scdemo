﻿/**
 * 会员注册界面
 * @author lw
 */
Ext.define('system.hmember.member.biz.RegisterEdit', {
	extend : 'Ext.window.Window',
	grid:null,
	oper : Ext.create('system.hmember.member.biz.MemberOper'),
	initComponent:function(){
	    var me = this;
	    
	    me.idNo = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '编号',
	    	name : 'id'
	    });

	    var idcard = Ext.create('system.widget.FsdTextButton',{
	        zName: 'idcard',
	    	zFieldLabel : '身份证号',
	    	zLabelAlign : 'right',
	    	zLabelWidth : 90,
	        width : 340,
	        zMaxLength: 18,
	    	zAllowBlank: true,
	        zIsButton1 : true,
	        zVtype : 'idCard',
	    	zButton1Text : '加载',
	    	zButton1Callback : function(){
	    		if(idcard.zIsValid()){
	    			mobile.zSetValue('');
	    			code.zSetValue('');
	    			me.oper.loadRY(idcard, mobile, code, form);
	        	}
	    	}
	    });

	    var mobile = Ext.create('system.widget.FsdTextButton',{
	        zName: 'mobile',
	    	zFieldLabel : '注册电话',
	    	zLabelAlign : 'right',
	    	zLabelWidth : 90,
	        width : 340,
	        zMaxLength: 11,
	        zMinLength: 11,
	    	zAllowBlank: true,
	        zIsButton1 : true,
	    	zButton1Text : '加载',
	    	zButton1Callback : function(){
	    		if(mobile.zIsValid()){
	    			idcard.zSetValue('');
	    			code.zSetValue('');
	    			me.oper.loadRY(idcard, mobile, code, form);
	        	}
	    	}
	    });

	    var code = Ext.create('system.widget.FsdTextButton',{
	        zName: 'code',
	    	zFieldLabel : '会员编码',
	    	zLabelAlign : 'right',
	    	zLabelWidth : 90,
	        width : 340,
	        zMaxLength: 20,
	    	zAllowBlank: true,
	        zIsButton1 : true,
	    	zButton1Text : '加载',
	    	zButton1Callback : function(){
	    		if(code.zIsValid()){
	    			idcard.zSetValue('');
	    			mobile.zSetValue('');
	    			me.oper.loadRY(idcard, mobile, code, form);
	        	}
	    	}
	    });
	    
	    var nickname = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '备注姓名',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 340,
	    	name: 'nickname'
	    });
	    
	    var realname = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '姓名',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 340,
	    	name: 'realname'
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [me.idNo, idcard, mobile, code, nickname, nickname, realname]
        });
	    
	    Ext.apply(this,{
	        width:400,
	        height:230,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
			    text : '注册',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.logonSubmit(me.idNo.getValue(), me.grid, me);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    }),
	    this.callParent(arguments);
	}
});