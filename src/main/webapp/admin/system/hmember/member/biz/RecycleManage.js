﻿/**

 * 用户删除管理界面
 * @author lw
 */

Ext.define('system.hmember.member.biz.RecycleManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.hmember.member.model.Extjs_Column_A_memberRecycle',
	             'system.hmember.member.model.Extjs_Model_A_member'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.hmember.member.biz.MemberOper'),
	
	createContent : function(){
		var me = this;
		
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_a_memberrecycle', // 显示列
			model: 'model_a_member',
			baseUrl : 'h_member/load_recycledata.do',
			tbar : [{
				xtype : "fsdbutton",
				text : "还原",
				iconCls : 'arrow_redoIcon',
				handler : function(button) {
					me.oper.recycle(grid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid);
				}
			}, '->', new Ext.form.TextField( {
				id : 'queryMemberRecycle',
				emptyText : '请输入人员名称',
				enableKeyEvents : true,
				listeners : {
					specialkey : function(field, e) {
						if (e.getKey() == Ext.EventObject.ENTER) {
							grid.getStore().loadPage(1);
						}
					}
				},
				width : 130
			}), {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  Ext.getCmp('queryMemberRecycle').getValue();
			var pram = {name : name};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		return grid;
	}
});