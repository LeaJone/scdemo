﻿/**
 * 会员管理界面
 * @author lw
 */

Ext.define('system.hmember.member.biz.MemberManageAll', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.hmember.member.model.Extjs_Column_A_member',
	             'system.hmember.member.model.Extjs_Model_A_member' ],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.hmember.member.biz.MemberOper'),
	
	createContent : function(){
		var me = this;
		
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_a_member', // 显示列
			model: 'model_a_member',
			baseUrl : 'h_member/load_databyall.do',
			tbar : [{
				xtype : "fsdbutton",
				popedomCode : 'hyglqbbj',
				text : "添加",
				iconCls : 'page_addIcon',
				handler : function(button) {
					me.oper.add(grid, true);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'hyglqbbj',
				text : "修改",
				iconCls : 'page_editIcon',
				handler : function(button) {
					me.oper.editor(grid, true);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'hyglqbsc',
				text : "删除",
				iconCls : 'page_deleteIcon',
				handler : function(button) {
				    me.oper.del(grid, true);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'hyglqbbj',
				text : "密码重置",
				iconCls : 'keyIcon',
				handler : function(button) {
				    me.oper.resetPassword(grid, true);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'hyzcglzc',
				text : "会员注册",
				iconCls : 'page_addIcon',
				handler : function(button) {
					me.oper.logongrid(grid);
				}
			}, '-', {
				xtype : "fsdbutton",
				popedomCode : 'hyglqbbj',
				text : "启用",
				iconCls : 'tickIcon',
				handler : function(button) {
				    me.oper.isEnabled(grid, true, true);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'hyglqbbj',
				text : "停用",
				iconCls : 'crossIcon',
				handler : function(button) {
				    me.oper.isEnabled(grid, false, true);
				}
			}, '-', {
				xtype : "fsdbutton",
				popedomCode : 'rybj',
				text : "开启推送",
				iconCls : 'tickIcon',
				handler : function(button) {
				    me.oper.isWeChatPush(grid, 'true', true);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'rybj',
				text : "关闭推送",
				iconCls : 'crossIcon',
				handler : function(button) {
				    me.oper.isWeChatPush(grid, 'false', true);
				}
			}, '-', {
				xtype : "fsdbutton",
				popedomCode : 'hyglqbbj',
				text : "扫描绑定",
				iconCls : 'scanTwoIcon',
				handler : function(button) {
				    me.oper.userWeChat(grid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'hyglqbbj',
				text : "查看微信",
				iconCls : 'wechatIcon',
				handler : function(button) {
				    me.oper.userView(grid);
				}
			}, '->', Ext.create('Ext.form.TextField',{
				id : 'queryNameAll',
				name : 'queryParam',
				emptyText : '请输入会员姓名',
				enableKeyEvents : true,
				listeners : {
					specialkey : function(field, e) {
						if (e.getKey() == Ext.EventObject.ENTER) {
							grid.getStore().loadPage(1);
						}
					}
				},
				width : 130
			}), {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  Ext.getCmp('queryNameAll').getValue();
			var pram = {name : name};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		return grid;
	}
});