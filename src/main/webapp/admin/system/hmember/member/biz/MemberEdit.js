﻿/**
 * 会员添加界面
 * @author lw
 */
Ext.define('system.hmember.member.biz.MemberEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	isAll : false,
	oper : Ext.create('system.hmember.member.biz.MemberOper'),
	initComponent:function(){
	    var me = this;
	   
	    var code = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '编码',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 340,
	    	name: 'code',
	    	maxLength: 20,
	    	allowBlank: true
	    }); 
	   
	    var loginname = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '登录名',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 340,
	    	name: 'loginname',
	    	maxLength: 20,
	    	allowBlank: true
	    });
	    
	    var regtime = Ext.create('system.widget.FsdDateField',{
	    	fieldLabel: '注册时间',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 340,
	    	name: 'regtime',
	    	allowBlank: false
	    });
	    
	    var nickname = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注姓名',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 340,
	    	name: 'nickname',
	    	maxLength: 20,
	    	allowBlank: false
	    });
	    
	    var realname = Ext.create('system.widget.FsdTextFieldZJF',{
	    	fieldLabel: '姓名',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 340,
	    	name: 'realname',
	    	fname : 'symbol',
	    	maxLength: 25,
	    	allowBlank: false
	    });
	    
	    var symbol = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '助记符',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 340,
	    	name: 'symbol',
	    	maxLength: 20,
	    	allowBlank: false
	    });
	    
	    var idcard = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '身份证号',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 340,
	    	name: 'idcard',
	    	maxLength: 18,
	        vtype : 'idCard',
	    	allowBlank: true
	    });
	    
	    var birthday = Ext.create('system.widget.FsdDateField',{
	    	fieldLabel: '出生日期',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 340,
	    	name: 'birthday',
	    	allowBlank: true
	    });

	    var birthyear = Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '出生年',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 340,
	    	name: 'birthyear',
	    	maxLength: 4,
	    	minLength: 4,
	    	minValue: 1900,
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank: true
	    });
	    
	    var birthmonth = Ext.create('system.widget.FsdComboBoxNumber',{
	    	fieldLabel: '出生月',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 340,
	    	name: 'birthmonth',
	    	allowBlank: true,
	    	zNumSort : 'asc',//asc为正序，desc为倒叙
	        zNumBegin : 1,//起始数
	        zNumEnd : 12,//结束数
	        zNumUnit : ' 月',//显示内容单位
	        zNumIsPlace : true,//是否启用补位占位
	        zNumPlace : '0',//补位占位符
	        zNumLength : 2,//数字长度
	    	allowBlank: false
	    });

	    var birthdays = Ext.create('system.widget.FsdComboBoxNumber',{
	    	fieldLabel: '出生日',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 340,
	    	name: 'birthdays',
	    	allowBlank: true,
	    	zNumSort : 'asc',//asc为正序，desc为倒叙
	        zNumBegin : 1,//起始数
	        zNumEnd : 31,//结束数
	        zNumUnit : ' 日',//显示内容单位
	        zNumIsPlace : true,//是否启用补位占位
	        zNumPlace : '0',//补位占位符
	        zNumLength : 2,//数字长度
	    	allowBlank: false
	    });
	    
	    var xb = Ext.create('system.widget.FsdComboBoxZD',{
	    	fieldLabel: '性别',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 340,
	    	name: 'sex',
	        key : 'xb',//参数
	        allowBlank: false,
	        zDefaultCode : 'xbwz',
    	    hiddenName : 'sexname'//提交隐藏域Name
	    });
	    var xbcode = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '性别名',
	    	name: 'sexname'
	    });
	    
	    var email = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '电子邮箱',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 340,
	    	name: 'email',
			vtype : 'email',
	    	maxLength: 100
	    });
	    
	    var qq = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: 'QQ号',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 340,
	    	name: 'qq',
	    	maxLength: 20
	    });

	    var mobile = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '注册电话',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 340,
	    	name: 'mobile',
	    	allowBlank: false,
	    	maxLength: 11,
	    	minLength: 11
	    });
	    
	    var mobile1 = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '联系电话2',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 340,
	    	name: 'mobile1',
	    	allowBlank: true,
	    	maxLength: 11,
	    	minLength: 11
	    });
	    
	    var mobile2 = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '联系电话3',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 340,
	    	name: 'mobile2',
	    	allowBlank: true,
	    	maxLength: 11,
	    	minLength: 11
	    });

	    var ryzt = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'人员状态',
	        labelAlign:'right',
	    	labelWidth:90,
	        width : 340,
    	    name : 'status',//提交到后台的参数名
	        key : 'ryzt',//参数
	        allowBlank: false,
	        zDefaultCode : 'ryztqy',
    	    hiddenName : 'statusname'//提交隐藏域Name
	    });
	    var ryztcode = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '人员状态名',
	    	name: 'statusname'
	    });

	    var workaddress = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '工作单位',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 680,
	    	name: 'workaddress',
	    	maxLength: 100,
	    	colspan : 2
	    });

	    var homeaddress = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '家庭地址',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 680,
	    	name: 'homeaddress',
	    	maxLength: 100,
	    	colspan : 2
	    });
	    
	    var beizhu = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 680,
	    	name: 'remark',
	    	maxLength: 50,
	    	colspan : 2
	    });
	    
	    me.on('show' , function(){
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			layout: { 
		        type: 'table', 
		        columns: 2, //每行有几列 
		    },
			items : [{
                name: "id",
                xtype: "hidden"
            } , 
            xbcode, ryztcode, 
            code, loginname, 
            nickname, regtime, 
            realname, idcard, 
            symbol, xb, 
            email, qq, 
            birthday, mobile, 
            birthyear, mobile1, 
            birthmonth, mobile2, 
            birthdays, ryzt, 
            workaddress, homeaddress, beizhu]
        });
	        
	    Ext.apply(this,{
	        width:750,
	        height:420,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.formSubmit(form, me.grid, me.isAll);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    }),
	    this.callParent(arguments);
	}
});