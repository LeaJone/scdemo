﻿/**
 * 会员注册界面
 * @author lw
 */

Ext.define('system.hmember.member.biz.RegisterManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.hmember.member.model.Extjs_Column_A_member',
	             'system.hmember.member.model.Extjs_Model_A_member' ],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.hmember.member.biz.MemberOper'),
	
	createContent : function(){
		var me = this;
		
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_a_member', // 显示列
			model: 'model_a_member',
			baseUrl : 'h_member/load_databyregister.do',
			tbar : [{
				xtype : "fsdbutton",
				popedomCode : 'hyzcglzc',
				text : "会员注册",
				iconCls : 'page_addIcon',
				handler : function(button) {
					me.oper.logon(grid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'hyzcglzx',
				text : "注销会员",
				iconCls : 'page_deleteIcon',
				handler : function(button) {
				    me.oper.logout(grid);
				}
			}, '-', {
				xtype : "fsdbutton",
				popedomCode : 'hyglbj',
				text : "会员信息修改",
				iconCls : 'page_editIcon',
				handler : function(button) {
					me.oper.editor(grid);
				}
			}, {
				text : '会员信息查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid);
				}
			}, '-', {
				xtype : "fsdbutton",
				popedomCode : 'hyglbj',
				text : "扫描绑定",
				iconCls : 'scanTwoIcon',
				handler : function(button) {
				    me.oper.userWeChat(grid);
				}
			}, '->', new Ext.form.TextField( {
				id : 'queryZCName',
				name : 'queryParam',
				emptyText : '请输入会员姓名',
				enableKeyEvents : true,
				listeners : {
					specialkey : function(field, e) {
						if (e.getKey() == Ext.EventObject.ENTER) {
							grid.getStore().loadPage(1);
						}
					}
				},
				width : 130
			}), {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  Ext.getCmp('queryZCName').getValue();
			var pram = {name : name};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		return grid;
	}
});