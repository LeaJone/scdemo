/**
 * 会员管理界面操作类
 * @author lw
 */
Ext.define('system.hmember.member.biz.MemberOper', {

	form : null,
	util : Ext.create('system.util.util'),

	//============================  会员管理  ============================
	/**
     * 添加
     */
	add : function(grid, isAll){
    	var title = '会员添加';
    	if (isAll){
    		title = '会员添加(全部)';
    	}
	    var win = Ext.create('system.hmember.member.biz.MemberEdit');
        win.setTitle(title);
        win.grid = grid;
        win.isAll = isAll;
        win.modal = true;
        win.show();
	},
	
	/**
     * 表单提交
     */
     formSubmit : function(formpanel, grid, isAll){
        var me = this;
        if (formpanel.form.isValid()) {
        	var url = 'h_member/save_Member_qx_hyglbj.do';
        	if (isAll){
            	url = 'h_member/save_Member_qx_hyglqbbj.do';
        	}
            me.util.ExtFormSubmit(url, formpanel.form , '正在提交数据,请稍候.....',
			function(form, action){
			    formpanel.up('window').close();
			    grid.getStore().reload();
				Ext.MessageBox.alert('提示', '保存成功！');
			});
        }
     },
	
	/**
     * 修改
     */
	editor : function(grid, isAll){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{
        	var title = '会员修改';
        	if (isAll){
        		title = '会员修改(全部)';
        	}
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.hmember.member.biz.MemberEdit');
            win.setTitle(title);
            win.modal = true;
            win.grid = grid;
            win.isAll = isAll;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'A_Employee/load_EmployeeByid.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},
 	
 	/**
     * 查看
     */
	view : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if(data.length > 0){
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.hmember.member.biz.MemberEdit');
            win.setTitle('会员查看');
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'A_Employee/load_EmployeeByid.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},
	
	/**
     * 删除
     */
	del : function(grid, isAll){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
		        msg : '您确定要删除吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                    	var url = 'A_Employee/del_Member_qx_hyglsc.do';
                    	if (isAll){
                        	url = 'A_Employee/del_Member_qx_hyglqbsc.do';
                    	}
					    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
			            });
             			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest(url, param,
                        function(response, options){
            				Ext.MessageBox.alert('提示', '删除成功！');
                        	grid.getStore().reload(); 
                        }, null, me.form);
                    }
                }
            });
        }
	},

	/**
     * 重置密码
     */
	resetPassword : function(grid, isAll){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
		        msg : '您确定要重置密码吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                    	var url = 'A_Employee/pwd_Member_qx_hyglbj.do';
                    	if (isAll){
                        	url = 'A_Employee/pwd_Member_qx_hyglqbbj.do';
                    	}
					    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
			            });
             			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest(url, param ,
                        function(response, options){
            				Ext.MessageBox.alert('提示', '密码重置成功！');
                        });
                    }
                }
            });
        }
	},

	/**
     * 是否启用停用
     */
	isEnabled : function(grid, isQT, isAll){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
		        msg : '您确定要修改人员状态吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                    	var url = 'A_Employee/enabled_Member_qx_hyglbj.do';
                    	if (isAll){
                        	url = 'A_Employee/enabled_Member_qx_hyglqbbj.do';
                    	}
					    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
			            });
             			var pram = {ids : dir, isenabled : isQT};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest(url, param ,
                        function(response, options){
            				Ext.MessageBox.alert('提示', '人员状态修改成功！');
                        	grid.getStore().reload(); 
                        });
                    }
                }
            });
        }
	},
 	
 	/**
      * 微信绑定二维码
      */
     userWeChat : function(grid){
 		var me = this;
 		// 获取选中的行
 	    var data = grid.getSelectionModel().getSelection();
         if (data.length == 0) {
             Ext.MessageBox.alert('提示', '请先选中要绑定的人员！');
         }else if(data.length > 1){
             Ext.MessageBox.alert('提示', '每次只能操作一个人员！');
         }else{
             // 先得到主键
             var item = data[0].data;
             //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
             var pram = {id : item.id};
             var param = {jsonData : Ext.encode(pram)};
             me.util.ExtAjaxRequest("e_userinfo/get_weichatQRcode.do", param,
             function(response, options, respText){
            	 var win = Ext.create('system.abasis.employee.biz.UserWeChat');
                 win.setTitle('微信绑定');
                 win.modal = true;
            	 win.setImageUrl(respText.data);
            	 win.grid = grid;
            	 win.show();
             }, null, me.form);
         }
 	},
 	
 	/**
     * 微信查看
     */
 	userView : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
	    if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要查看的人员！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个人员！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.ewechat.userinfo.biz.UserInfoBind');
            win.setTitle('微信用户查看');
            win.grid = grid;
            win.modal = true;
            var pram = {employeeid : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'e_userinfo/load_e_userinfobyemployeeid.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},

	/**
     * 是否开启关闭推送
     */
	isWeChatPush : function(grid, isQT, isAll){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
		        msg : '您确定要修改微信推送状态吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                    	var url = 'A_Employee/ispush_Member_qx_hyglbj.do';
                    	if (isAll){
                        	url = 'A_Employee/ispush_Member_qx_hyglqbbj.do';
                    	}
					    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
			            });
             			var pram = {ids : dir, iswechatpush : isQT};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest(url, param,
                        function(response, options){
            				Ext.MessageBox.alert('提示', '微信推送状态修改成功！');
                        	grid.getStore().reload(); 
                        });
                    }
                }
            });
        }
	},

	/**
     * 还原用户
     */
	recycle : function(grid){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
		        msg : '您确定要还原人员信息吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
					    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
			            });
             			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('A_Employee/recycle_Employee.do' , param ,
                        function(response, options){
            				Ext.MessageBox.alert('提示', '人员信息还原成功！');
                        	grid.getStore().reload(); 
                        });
                    }
                }
            });
        }
	},
	
	//充值提醒测试
	cztest : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
	    if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else{
        	var dir = new Array();　
            Ext.Array.each(data, function(items) {
                var id = items.data.id;
                dir.push(id);
            });
 			var pram = {ids : dir};
            var param = {jsonData : Ext.encode(pram)};
            me.util.ExtAjaxRequest("h_member/test_sendMessageRecharge.do", param,
            function(response, options, respText){
            	
            }, null, me.form);
            
        }
	},
	
	//消费提醒测试
	xftest : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
	    if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else{
        	var dir = new Array();　
            Ext.Array.each(data, function(items) {
                var id = items.data.id;
                dir.push(id);
            });
 			var pram = {ids : dir};
            var param = {jsonData : Ext.encode(pram)};
            me.util.ExtAjaxRequest("h_member/test_sendMessagePay.do", param,
            function(response, options, respText){
            	
            }, null, me.form);
            
        }
	},
	
	//消费提醒测试
	yytest : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
	    if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else{
        	var dir = new Array();　
            Ext.Array.each(data, function(items) {
                var id = items.data.id;
                dir.push(id);
            });
 			var pram = {ids : dir};
            var param = {jsonData : Ext.encode(pram)};
            me.util.ExtAjaxRequest("h_member/test_sendMessageReserve.do", param,
            function(response, options, respText){
            	
            }, null, me.form);
            
        }
	},
	

	//============================  注册管理  ============================
	/**
     * 注册
     */
	logon : function(grid){
	    var win = Ext.create('system.hmember.member.biz.RegisterEdit');
        win.setTitle('会员注册');
        win.grid = grid;
        win.modal = true;
        win.show();
	},
	
	/**
     * 注册提交
     */
	logonSubmit : function(id, grid, win){
		var me = this;
		if(id == null || id == ''){
			Ext.MessageBox.alert('提示', '未找到注册人员ID信息，请重新加载查询！');
			return;
		}
		var pram = {id : id};
		var param = {jsonData : Ext.encode(pram)};
		me.util.ExtAjaxRequest('h_relation/save_qx_hyzcglzc.do' , param ,
			function(response, options){
			grid.getStore().reload();
			Ext.MessageBox.alert('提示', '注册成功！');
			win.close();
		}, null, me.form);
     },
 	
 	/**
      * 注册提交
      */
 	logongrid : function(grid){
 		var me = this;
 		var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else {
        	var dir = new Array();　
            Ext.Array.each(data, function(items) {
                var id = items.data.id;
                dir.push(id);
            });
 			var pram = {ids : dir};
            var param = {jsonData : Ext.encode(pram)};
            me.util.ExtAjaxRequest('h_relation/savelist_qx_hyzcglzc.do', param,
            function(response, options){
            	grid.getStore().reload(); 
     			Ext.MessageBox.alert('提示', '注册成功！');
            });
        }
    },
	
	/**
     * 注销
     */
	logout : function(grid){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
		        msg : '您确定要注销会员吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
					    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
			            });
             			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('h_relation/del_qx_hyzcglzx.do', param,
                        function(response, options){
            				Ext.MessageBox.alert('提示', '注销成功！');
                        	grid.getStore().reload(); 
                        }, null, me.form);
                    }
                }
            });
        }
	},
 	
 	/**
     * 加载人员信息
     */
	loadRY : function(idcard, mobile, code, form){
		var me = this;
		var idcardO = idcard.zGetValue();
		var mobileO = mobile.zGetValue();
		var codeO = code.zGetValue();
		var pram = {idcard : idcardO, mobile : mobileO, code : codeO};
        var param = {jsonData : Ext.encode(pram)};
        me.util.formLoad(form.getForm() , 'A_Employee/load_EmployeeByZC.do' , param , null , 
        function(response, options, respText){
        },
        function(response, options){
        	form.getForm().reset();
			idcard.zSetValue(idcardO);
			mobile.zSetValue(mobileO);
			code.zSetValue(codeO);
        });
	}
});