﻿/**
 * 推送设置添加界面
 *
 * @author liahnlin
 */
Ext.define('system.z_newsclient.pushSettings.biz.PushSettingsEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.z_newsclient.pushSettings.biz.PushSettingsOper'),
	initComponent:function(){
	    var me = this;
	   
	    var appname = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '应用名称',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 400,
	    	name: 'appname',
	    	maxLength: 50,
	    	allowBlank: false
	    });

        var appid = Ext.create('system.widget.FsdTextField',{
            fieldLabel: 'AppID',
            labelAlign:'right',
            labelWidth:80,
            width : 400,
            name: 'appid',
            maxLength: 50,
            allowBlank: false
        });

        var appkey = Ext.create('system.widget.FsdTextField',{
            fieldLabel: 'AppKey',
            labelAlign:'right',
            labelWidth:80,
            width : 400,
            name: 'appkey',
            maxLength: 50,
            allowBlank: false
        });

        var appsecret = Ext.create('system.widget.FsdTextField',{
            fieldLabel: 'AppSecret',
            labelAlign:'right',
            labelWidth:80,
            width : 400,
            name: 'appsecret',
            maxLength: 50,
            allowBlank: false
        });

        var mastersecret = Ext.create('system.widget.FsdTextField',{
            fieldLabel: 'MasterSecret',
            labelAlign:'right',
            labelWidth:80,
            width : 400,
            name: 'mastersecret',
            maxLength: 50,
            allowBlank: false
        });
	        
	    Ext.apply(this,{
            layout : 'fit',
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[{
				xtype : 'form',
				id : 'PushAddForm',
				defaultType : 'textfield',
				header : false,// 是否显示标题栏
				border : 0,
				padding : '20 20 10 20',
				items : [{
                    name: "id",
                    xtype: "hidden"
                } , appname, appid , appkey , appsecret, mastersecret ]
	        }],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'tsszbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.formSubmit(Ext.getCmp('PushAddForm') , me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});