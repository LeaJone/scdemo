﻿/**
 * 发布推送通知添加界面
 *
 * @author liahnlin
 */
Ext.define('system.z_newsclient.pushSettings.biz.PushPublishEdit', {
	extend : 'Ext.window.Window',
    requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*' ,
                'system.bcms.article.model.Extjs_Column_B_article',
                'system.bcms.article.model.Extjs_Model_B_article'],
	grid : null,
	oper : Ext.create('system.z_newsclient.pushSettings.biz.PushSettingsOper'),
	initComponent:function(){
	    var me = this;

        me.settingsid = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '应用ID',
            name: 'settingsid'
        });

        var title = Ext.create('system.widget.FsdTextField',{
            fieldLabel: '推送标题',
            labelAlign:'right',
            labelWidth:80,
            width : 400,
            name: 'title',
            maxLength: 50,
            allowBlank: false
        });

        var text = Ext.create('system.widget.FsdTextArea',{
            fieldLabel: '推送内容',
            labelAlign:'right',
            labelWidth:80,
            width : 400,
            name: 'text',
            maxLength: 100,
            allowBlank: false
        });

        var logourl = Ext.create('system.widget.FsdTextFileManage', {
            width : 400,
            zName : 'logourl',
            zFieldLabel : '推送图标',
            zLabelWidth : 80,
            zIsShowButton : true,
            zIsUpButton : true,
            zFileUpPath : me.oper.util.getImagePath()
        });

        var articleid = Ext.create('system.widget.FsdTextGrid', {
            zFieldLabel : '推送文章',
            zLabelWidth : 80,
            width : 400,
            zName : 'articleid',
            zColumn : 'column_n_article', // 显示列
            zModel : 'model_n_article',
            zBaseUrl : 'B_Article/load_Article.do',
            zAllowBlank : false,
            zGridType : 'page',
            zGridWidth : 500,//弹出表格宽度
            zGridHeight : 550,//弹出表格高度
            zIsText1 : true,
            zTxtLabel1 : '文章标题',
            zFunChoose : function(data){
                articleid.setValue(data.id);
            },
            zFunQuery : function(txt1, txt2, txt3, txt4){
                return {title : txt1};
            }
        });
	        
	    Ext.apply(this,{
            layout : 'fit',
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[{
				xtype : 'form',
				id : 'PushPublishForm',
				defaultType : 'textfield',
				header : false,// 是否显示标题栏
				border : 0,
				padding : '20 20 10 20',
				items : [{
                    name: "id",
                    xtype: "hidden"
                } , me.settingsid, title, text, logourl, articleid ]
	        }],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'tsfbbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.publishFormSubmit(Ext.getCmp('PushPublishForm') , me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});