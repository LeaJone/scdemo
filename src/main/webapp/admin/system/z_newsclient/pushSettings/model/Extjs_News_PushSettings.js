﻿Ext.define('model_news_pushSettings', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'appid',
            type : 'string'
        }
        ,
        {
            name : 'appname',
            type : 'string'
        }
        ,
        {
            name : 'appkey',
            type : 'string'
        }
        ,
        {
            name : 'appsecret',
            type : 'string'
        }
        ,
        {
            name : 'mastersecret',
            type : 'string'
        }
    ]
});

Ext.define('column_news_pushSettings', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            align : 'center',
            width : 50
        }
        ,
        {
            header : '应用名称',
            dataIndex : 'appname',
            align : 'center',
            flex : 1
        }
        ,
        {
            header : 'AppID',
            dataIndex : 'appid',
            align : 'center',
            flex : 1
        }
        ,
        {
            header : 'AppSecret',
            dataIndex : 'appsecret',
            align : 'center',
            flex : 1
        }
        ,
        {
            header : 'AppKey',
            dataIndex : 'appkey',
            align : 'center',
            flex : 1
        }
        ,
        {
            header : 'MasterSecret',
            dataIndex : 'mastersecret',
            align : 'center',
            flex : 1
        }
    ]
});