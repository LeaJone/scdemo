/**
 * 版本管理界面操作类
 *
 * @author lihanlin
 */
Ext.define('system.z_newsclient.version.biz.VersionOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 添加
     */
	add : function(grid){
	    var win = Ext.create('system.z_newsclient.version.biz.VersionEdit');
        win.setTitle('版本添加');
        win.grid = grid;
        win.modal = true;
        win.show();
	},
	
	/**
     * 修改
     */
	editor : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的数据！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一条数据！');
        }else{
        	// 先得到主键
            var id = data[0].data.id;
            var win = Ext.create('system.z_newsclient.version.biz.VersionEdit');
            win.setTitle('版本修改');
            win.grid = grid;
            win.modal = true;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'n_newsversion/load_newsversionById.do' , param , null ,
	        function(response, options, respText){
	            win.show();
	        });
        }
	},
 	
 	/**
     * 查看
     */
	view : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if(data.length > 0){
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.z_newsclient.version.biz.VersionEdit');
            win.setTitle('版本查看');
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'n_newsversion/load_newsversionById.do' , param , null ,
	        function(response, options, respText){
	            win.show();
	        });
        }
	},
	
	/**
     * 删除
     */
	del : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要删除的数据！');
        }else {
        	Ext.MessageBox.show({
        		title : '询问',
        		msg : '您确定要删除吗？',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                   	 var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
            			});
                        var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('n_newsversion/del_newsversion_qx_versionsc.do' , param ,
                        function(response, options){
	                       	grid.getStore().reload();
                        }, null, me.form);
                    }
                }
            });
        }
	},
	
	/**
     * 表单提交
     */
     formSubmit : function(formpanel , grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('n_newsversion/save_newsversion_qx_versionbj.do' , formpanel.form , '正在提交数据,请稍候.....',
			function(form, action){
			    formpanel.up('window').close();
			    grid.getStore().reload();
				Ext.MessageBox.alert('提示', '保存成功！');
			});
        }
     }
});