﻿/**
 * 版本添加界面
 *
 * @author liahnlin
 */
Ext.define('system.z_newsclient.version.biz.VersionEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.z_newsclient.version.biz.VersionOper'),
	initComponent:function(){
	    var me = this;
	   
	    var version = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '版本号',
	    	labelAlign:'right',
	    	labelWidth:100,
	        width : 400,
	    	name: 'version',
	    	maxLength: 50,
	    	allowBlank: false
	    });

        var iosurl = Ext.create('system.widget.FsdTextFileManage', {
            width : 400,
            zName : 'iosurl',
            zFieldLabel : 'ios下载路径',
            zLabelWidth : 100,
            zIsUpButton : true,
            zFileType : 'file',
            zFileAutoName : false,
            zFileUpPath : me.oper.util.getFilePath(),
            zManageType : 'manage'
        });

        var androidurl = Ext.create('system.widget.FsdTextFileManage', {
            width : 400,
            zName : 'androidurl',
            zFieldLabel : 'android下载路径',
            zLabelWidth : 100,
            zIsUpButton : true,
            zFileType : 'file',
            zFileAutoName : false,
            zFileUpPath : me.oper.util.getFilePath(),
            zManageType : 'manage'
        });
	    
	    var remark = new Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:100,
	        width : 400,
	    	name: 'remark',
	    	maxLength: 500
	    });
	        
	    Ext.apply(this,{
            layout : 'fit',
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[{
				xtype : 'form',
				id : 'VersionAddForm',
				defaultType : 'textfield',
				header : false,// 是否显示标题栏
				border : 0,
				padding : '20 20 10 20',
				items : [{
                    name: "id",
                    xtype: "hidden"
                } , version, iosurl , androidurl , remark ]
	        }],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'versionbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.formSubmit(Ext.getCmp('VersionAddForm') , me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});