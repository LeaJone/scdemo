﻿Ext.define('model_news_version', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'version',
            type : 'string'
        }
        ,
        {
            name : 'iosurl',
            type : 'string'
        }
        ,
        {
            name : 'androidurl',
            type : 'string'
        }
        ,
        {
            name : 'pubdate',
            type : 'string'
        }
    ]
});

Ext.define('column_news_version', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            align : 'center',
            width : 50
        }
        ,
        {
            header : '版本号',
            dataIndex : 'version',
            align : 'center',
            flex : 1
        }
        ,
        {
            header : 'ios系统下载链接',
            dataIndex : 'iosurl',
            align : 'center',
            flex : 2
        }
        ,
        {
            header : 'android系统下载链接',
            dataIndex : 'androidurl',
            align : 'center',
            flex : 2
        }
        ,
        {
            header : '发布时间',
            dataIndex : 'pubdate',
            align : 'center',
            flex : 1,
            renderer :function(value, metaData, record){
                return system.UtilStatic.formatDateTime(value, true);
            }
        }
    ]
});

/**
 *sys_popedomgroup Columns
 *@author CodeSystem
 */
Ext.define('column_sys_popedomuser', {
    columns: [
        {
            header : '权限组名',
            dataIndex : 'name',
            flex : 3
        }
        ,
        {
            header : '序号',
            dataIndex : 'sort',
            flex : 1
        }
    ]
});