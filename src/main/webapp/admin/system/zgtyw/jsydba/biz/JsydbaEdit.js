﻿/**
 * 建设用地备案编辑界面
 * @author lumingbao
 */
Ext.define('system.zgtyw.jsydba.biz.JsydbaEdit', {
	extend : 'Ext.window.Window',
	requires : ['system.widget.FsdTreeComboBox'],
	grid : null,
	oper : Ext.create('system.zgtyw.jsydba.biz.JsydbaOper'),
	initComponent:function(){
	    var me = this;	    
	   
	    var xmmc = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '项目名称',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'xmmc',
	    	maxLength: 20,
	    	allowBlank: false,
	    	blankText: '请输入项目名称',
	    	maxLengthText: '项目名称不能超过20个字符'
	    });
	    
	    var sqdw = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '申请单位',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'sqdw',
	    	maxLength: 20,
	    	allowBlank: false,
	    	blankText: '请输入申请单位',
	    	maxLengthText: '申请单位不能超过20个字符'
	    });
	    
	    var ydqw = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '用地区位',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'ydqw',
	    	maxLength: 20,
	    	allowBlank: false,
	    	blankText: '请输入用地区位',
	    	maxLengthText: '用地区位不能超过20个字符'
	    });
	    
	    var xmlx = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '项目类型',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'xmlx',
	    	maxLength: 20,
	    	allowBlank: false,
	    	blankText: '请输入项目类型',
	    	maxLengthText: '项目类型不能超过20个字符'
	    });
	    
	    var pzmj = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '批准面积',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'sqmj',
	    	maxLength: 20,
	    	allowBlank: false,
	    	blankText: '请输入批准面积',
	    	maxLengthText: '批准面积不能超过20个字符'
	    });
	    
	    var pzwh = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '批准文号',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'pzwh',
	    	maxLength: 20,
	    	allowBlank: false,
	    	blankText: '请输入批准文号',
	    	maxLengthText: '批准文号不能超过20个字符'
	    });
	    
	    var pzsj = Ext.create('system.widget.FsdDateField',{
	    	fieldLabel: '批准时间',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'pzsj',
	    	allowBlank: false
	    });
	     
	    var beizhu = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'remark',
	    	maxLength: 50,
	    	maxLengthText: '备注不能超过50个字符'
	    });
	    
	    var formpanel = new Ext.form.Panel({
	    	defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [{
	                    name: "id",
	                    xtype: "hidden"
	                } , 
	                xmmc, sqdw, ydqw, xmlx, pzmj, pzwh, pzsj, beizhu]
	    });
	    Ext.apply(this,{
	        width:400,
	        height:320,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[formpanel],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    zIsSpace : true,
			    handler : function() {
    				me.oper.formSubmit(formpanel , me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});