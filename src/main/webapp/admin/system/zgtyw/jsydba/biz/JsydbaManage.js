﻿/**
 * 建设用地备案管理界面
 * @author lumingbao
 */
Ext.define('system.zgtyw.jsydba.biz.JsydbaManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*', 
	             'system.zgtyw.jsydba.model.Extjs_Model_Z_jsydba',
	             'system.zgtyw.jsydba.model.Extjs_Column_Z_jsydba'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.zgtyw.jsydba.biz.JsydbaOper'),
    
	createTable : function() {
	    var me = this;
	    
	    var queryText = Ext.create("Ext.form.TextField", {
	    	name : 'queryParam',
			emptyText : '请输入项目名称',
			width : 130,
			enableKeyEvents : true,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
	    });
	    
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_jsydba', // 显示列
			model : 'model_z_jsydba',
			baseUrl : 'z_jsydba/load_pagedata.do',
			pageSize : 30,
			border : false,
			tbar : [{
				xtype : "fsdbutton",
				popedomCode : 'jsydspdr',
				text : "导入",
				iconCls : 'office_excelIcon',
				handler : function() {
					me.oper.import(grid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
				text : "添加",
				iconCls : 'page_addIcon',
				handler : function() {
					me.oper.add(grid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
				text : "修改",
				iconCls : 'page_editIcon',
				handler : function() {
					me.oper.editor(grid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'dwsc',
				text : "删除",
				iconCls : 'page_deleteIcon',
				handler : function() {
				    me.oper.del(grid);
				}
			}, '->', queryText, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var xmmc =  queryText.getValue();
			var pram = {xmmc : xmmc};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});

		return grid;
	}
});