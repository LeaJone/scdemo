﻿/**
 *Z_JSYDBA Model
 *@author CodeSystem
 *文件名     Extjs_Model_Z_jsydba
 *Model名    model_z_jsydba
 */

Ext.define('model_z_jsydba', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'xmmc',
            type : 'string'
        }
        ,
        {
            name : 'sqdw',
            type : 'string'
        }
        ,
        {
            name : 'ydqw',
            type : 'string'
        }
        ,
        {
            name : 'xmlx',
            type : 'string'
        }
        ,
        {
            name : 'sqmj',
            type : 'string'
        }
        ,
        {
            name : 'pzsj',
            type : 'string'
        }
        ,
        {
            name : 'pzwh',
            type : 'string'
        }
    ]
});
