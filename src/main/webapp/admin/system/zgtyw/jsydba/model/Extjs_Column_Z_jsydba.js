﻿/**
 *Z_JSYDBA Column
 *@author CodeSystem
 *文件名     Extjs_Column_Z_jsydba
 *Columns名  column_z_jsydba
 *xtype: 'textfield', 'numbercolumn', 'booleancolumn', 'datecolumn'   format : '0.00'
 *align: 'left', 'center', 'right'
 */

Ext.define('column_z_jsydba', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            align : 'center',
            width : 30
        }
        ,
        {
            header : '项目名称',
            dataIndex : 'xmmc',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '申请单位',
            dataIndex : 'sqdw',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '用地区位',
            dataIndex : 'ydqw',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '项目类型',
            dataIndex : 'xmlx',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '申请面积',
            dataIndex : 'sqmj',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '批准时间',
            dataIndex : 'pzsj',
            sortable : true,
            align : 'left',
            flex : 1,
			renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value);
			}
        }
        ,
        {
            header : '批准文号',
            dataIndex : 'pzwh',
            sortable : true,
            align : 'left',
            flex : 1
        }
    ]
});
