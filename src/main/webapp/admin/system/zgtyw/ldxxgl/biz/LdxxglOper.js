/**
 * 领导信息管理界面操作类
 * @author lmb
 */
Ext.define('system.zgtyw.ldxxgl.biz.LdxxglOper', {
	
	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 添加
     */
	add : function(grid){
	    var me = this;
		var model = Ext.create("system.zgtyw.ldxxgl.biz.LdxxEdit");
		var tabID = 'ldxxtj';
		var myDate = new Date();
		tabID += myDate.getTime();
		model.grid = grid;
		me.util.addTab(tabID , "领导添加" , model , "resources/admin/icon/vcard_add.png");
	},
     
     /**
      * 表单提交
      */
     formSubmit : function(formpanel, grid, treesubject, treesubjectxg){
         var me = this;
         if (formpanel.form.form.isValid()) {
         	var checkedNodes = treesubject.getChecked();
     		var sids = [];
     		for(var i=0 ; i < checkedNodes.length ; i++){
     			sids.push(checkedNodes[i].internalId)
     		}
     		checkedNodes = treesubjectxg.getChecked();
     		var sxgids = [];
     		for(var i=0 ; i < checkedNodes.length ; i++){
     			sxgids.push(checkedNodes[i].internalId)
     		}
     		var idMap = {};
     		idMap[system.UtilStatic.articleRelateLM] = sids;
     		idMap[system.UtilStatic.articleRelateXG] = sxgids;
            me.util.ExtFormSubmit('z_ldxxgl/save_Lsxx.do' , formpanel.form.form , '正在提交数据,请稍候.....',
 			function(form, action, respText){
             	if(grid != null){
             		grid.getStore().reload();//刷新管理界面的表格
             	}
             	formpanel.idNo.setValue(action.result.id);
 				Ext.MessageBox.alert('提示', '保存成功！');
 			}, null,
 			{jsonData : Ext.encode(idMap)});
         }
      },
	
	/**
     * 修改
     */
    editor : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{
        	// 先得到主键
            var id = data[0].data.id;
    		var model = me.util.getTab("ldxxxg");
    		if (model == null){
    			model = Ext.create("system.zgtyw.ldxxgl.biz.LdxxEdit");
    			model.grid = grid;
    		}
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(model.form, 'z_ldxxgl/saveLoad_ldxxbyid.do', param, null, 
            function(response, options, respText, data){
            	model.loadObject(data[system.UtilStatic.articleRelateLM], data[system.UtilStatic.articleRelateXG]);
            	me.util.addTab("ldxxxg" , "领导修改" , model , "resources/admin/icon/vcard_edit.png");
            });
        }
	},
 	
 	/**
     * 查看
     */
	view : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要查看的记录！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个记录！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            var model = Ext.create("system.zgtyw.ldxxgl.biz.LdxxEdit");
            model.viewObject();
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(model.form, 'z_ldxxgl/saveLoad_ldxxbyid.do', param, null, 
            function(response, options, respText, data){
            	model.loadObject(data[system.UtilStatic.articleRelateLM], data[system.UtilStatic.articleRelateXG]);
            	me.util.addTab(id + "view", "领导查看", model, "resources/admin/icon/vcard_edit.png");
            });
        }
	},
	
	/**
     * 删除
     */
	del : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要删除的项目！');
        }else {
        	Ext.MessageBox.show({
        		title : '询问',
        		msg : '您确定要删除吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                   	 var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
            			});
                        var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('z_ldxxgl/del_ldxx.do' , param ,
                        function(response, options){
	                       	grid.getStore().reload();
                        }, null, me.form);
                    }
                }
            });
        }
	},
	
	/**
     * 是否启用停用
     */
	isEnabled : function(grid, isQT){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
		        msg : '您确定要修改吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
					    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
			            });
             			var pram = {ids : dir, isenabled : isQT};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('z_ldxxgl/enabled_ldxx.do' , param ,
                        function(response, options){
            				Ext.MessageBox.alert('提示', '状态修改成功！');
                        	grid.getStore().reload(); 
                        });
                    }
                }
            });
        }
	}
});