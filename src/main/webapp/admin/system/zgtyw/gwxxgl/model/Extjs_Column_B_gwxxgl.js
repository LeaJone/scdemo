﻿/**
 *b_gwxxgl Column
 *@author CodeSystem
 *文件名     Extjs_Column_B_gwxxgl
 *Columns名  column_b_gwxxgl
 */

Ext.define('column_b_gwxxgl', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            width : 30
        }
        ,
        {
            header : '标题',
            dataIndex : 'title',
            sortable : true,
            flex : 8
        }
        ,
        {
            header : '所属栏目',
            dataIndex : 'subjectname',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '所属机构',
            dataIndex : 'branchname',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '类型',
            dataIndex : 'typename',
            sortable : true,
            flex : 1
        }
//        ,
//        {
//            header : '取图',
//            dataIndex : 'imageurl1',
//            sortable : true,
//            flex : 1,
//			renderer :function(value ,metaData ,record ){
//				if(value != ''){
//					value="<font style='color:blue;'>是</font>";
//				}else{
//					value='';
//				}
//				return value;
//			}
//        }
        ,
        {
            header : '审核',
            dataIndex : 'auditing',
            sortable : true,
            flex : 1,
			renderer :function(value ,metaData ,record ){
				if(value == 'true'){
					value="<font style='color:green;'>审</font>";
				}else{
					value="<font style='color:red;'>停</font>";
				}
				return value;
			}
        }
        ,
        {
            header : '置顶',
            dataIndex : 'firstly',
            sortable : true,
            flex : 1,
			renderer :function(value ,metaData ,record ){
				if(value == 'true'){
					value="<font style='color:blue;'>是</font>";
				}else{
					value="<font style='color:gray;'>否</font>";
				}
				return value;
			}
        }
        ,
        {
            header : '幻灯',
            dataIndex : 'slide',
            sortable : true,
            flex : 1,
			renderer :function(value ,metaData ,record ){
				if(value == 'true'){
					value="<font style='color:blue;'>是</font>";
				}else{
					value="<font style='color:gray;'>否</font>";
				}
				return value;
			}
        }
        ,
        {
            header : '外链接',
            dataIndex : 'isaurl',
            sortable : true,
            flex : 1,
			renderer :function(value ,metaData ,record ){
				if(value == 'true'){
					value="<font style='color:blue;'>是</font>";
				}else{
					value="<font style='color:gray;'>否</font>";
				}
				return value;
			}
        }
//        ,
//        {
//            header : '手机',
//            dataIndex : 'ismobile',
//            sortable : true,
//            flex : 1,
//			renderer :function(value ,metaData ,record ){
//				if(value == 'true'){
//					value="<font style='color:blue;'>是</font>";
//				}else{
//					value="<font style='color:gray;'>否</font>";
//				}
//				return value;
//			}
//        }
//        ,
//        {
//            header : '手机栏目',
//            dataIndex : 'msubjectname',
//            sortable : true,
//            flex : 2
//        }
//        ,
//        {
//            header : '操作人',
//            dataIndex : 'addemployeename',
//            sortable : true,
//            flex : 2
//        }
        ,
        {
            header : '新增时间',
            dataIndex : 'adddate',
            sortable : true,
            flex : 4,
			renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value);
			}
        }
    ]
});
