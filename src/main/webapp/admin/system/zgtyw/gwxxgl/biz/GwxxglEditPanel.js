﻿/**
 * 公文文章编辑界面
 * @author 
 */
Ext.define('system.zgtyw.gwxxgl.biz.GwxxglEditPanel', {
	extend : 'Ext.panel.Panel',
	oper : Ext.create('system.zgtyw.gwxxgl.biz.GwxxglOper'),
	
	grid : null,//管理表格
	formWin : null,//编辑窗体
	
	lstLM: null,
	lstXG:null,
	
	loadObject : function(LM, XG){
		var me = this;
		me.lstLM = LM;
		me.lstXG = XG;
		me.subjectid.setText(me.subjectname.getValue());
		me.branchid.setText(me.branchname.getValue());
		me.treesubject.getStore().reload();
    	me.treesubjectxg.getStore().reload();
	},
	
	viewObject : function(){
		var me = this;
		me.down('button[name=btnsave]').setVisible(false);
		me.down('button[name=btnnew]').setVisible(false);
	},
	
	initComponent : function() {
		var me = this;

		me.idNo = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '编号',
	    	name : 'id'
	    });
		
		me.type = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '文章类型',
	    	value : 'wzgw',
	    	name : 'type'
	    });
		
		me.typename = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '文章类型名称',
	    	value : '公文',
	    	name : 'typename'
	    });
		
		me.branchname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属机构名称',
	    	name: 'branchname'
	    });
		me.branchid = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属机构',
	    	labelAlign:'right',
	    	labelWidth:80,
	    	rootText : me.oper.util.getParams("company").name,
	    	rootId : me.oper.util.getParams("company").id,
			rootVisible: false,
	        width : 450,
	        name : 'branchid',
	        baseUrl:'A_Branch/load_AsyncBranchTree.do',//访问路劲
	        allowBlank: false,
	        hiddenName : 'branchname'//隐藏域Name
	    });
		
		me.subjectname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属栏目名称',
	    	name: 'subjectname'
	    });
	    me.subjectid = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属栏目',
	    	labelAlign:'right',
	    	labelWidth:80,
	    	rootText : '主栏目',
	    	rootId : 'FSDMAIN',
			rootVisible: false,
	        width : 450,
	        name : 'subjectid',
	        baseUrl:'B_Subject/load_AsyncSubjectTreeByPopdom.do',//访问路劲
	        allowBlank: false,
	        hiddenName : 'subjectname'//隐藏域Name
	    });
	    
//	    me.showtype = Ext.create('system.widget.FsdComboBoxZD',{
//            fieldLabel:'显示类型',
//	        labelAlign:'right',
//	    	labelWidth:80,
//	        width : 450,
//    	    name : 'showtype',//提交到后台的参数名
//	        key : 'wzxslx',//参数
//	        allowBlank: false,
//    	    hiddenName : 'showtypename',//提交隐藏域Name
//    	    zDefaultCode : 'wzxspt'//默认值 wzxspt，wzxsht
//	    });  
	    me.showtypename = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '显示类型名称',
	    	name: 'showtypename'
	    });
	    
	    me.titlecolor = Ext.create('system.widget.FsdTextColor', {
	        width : 450,
	    	zName : 'titlecolor', 
	    	zFieldLabel : '标题颜色', 
	    	zLabelWidth : 80
	    });

	    
	    me.checkboxgroup = new Ext.form.CheckboxGroup({
		    fieldLabel: '文章属性',
		    labelAlign:'right',
	    	labelWidth:80,
		    width: 450,
		    items: [
		        {
			    name : 'firstly',    
			    boxLabel: '置顶',
			    inputValue: 'true'
			    }, 
			    {
				name : 'slide',
				boxLabel: '幻灯',
				inputValue: 'true'
				}
//			    , 
//				{
//		    	name : 'recommend',
//			    boxLabel: '推荐',
//			    inputValue: 'true'
//			    }, 
//			    {
//			    name : 'video',  
//			    boxLabel: '视频',
//			    inputValue: 'true'
//			    }, 
//			    {
//			    name : 'roll',
//				boxLabel: '滚动',
//				inputValue: 'true'
//			    }, 
//			    {
//			    name : 'headline',
//				boxLabel: '头条',
//				inputValue: 'true'
//				}, 
//				{
//				name : 'hot',
//				boxLabel: '热门',
//				inputValue: 'true'
//				}, 
//				{
//				name : 'comments',
//				boxLabel: '评论',
//				inputValue: 'true'
//				}
			    ]
	    });

	    me.aurl = Ext.create('system.widget.FsdTextCheckbox',{
	    	zNameCheckbox : 'isaurl',
	    	zFieldLabel : '外链接',
	    	zLabelAlign : 'right',
	    	zLabelWidth : 80,
	    	zInputValue : 'true',
	    	zNameText : 'aurl',
	        width : 450,
	        zMaxLength : 200
	    });
	    
	    me.author = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '作者',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 450,
	    	name: 'author',
	    	maxLength: 50,
	    	maxLengthText: '作者不能超过50个字符'
	    });
	    
	    me.source = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '来源',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 450,
	    	name: 'source',
	    	value : me.oper.util.getParams("company").name,
	    	maxLength: 50,
	    	allowBlank: false
	    });
	    
	    me.overdue = Ext.create('system.widget.FsdDateField',{
	    	fieldLabel: '过期时间',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 450,
	    	name: 'overdue'
	    });
	    
	    me.adddate = Ext.create('system.widget.FsdDateTime', {
	    	fieldLabel: '新增时间',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 450,
	    	name: 'adddate',
	    });
	    
	    me.title = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '标题',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 900,
	    	name: 'title',
	    	maxLength: 100,
	    	allowBlank: false
	    });
	    
	    me.subtitle = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '副标题',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 900,
	    	name: 'subtitle',
	    	maxLength: 100
	    });
	    
	    me.showtitle = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '显示标题',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 900,
	    	name: 'showtitle',
	    	maxLength: 100,
	    	allowBlank: false
	    });
	    
	    me.abstracts = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '简介',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 900,
	    	name: 'abstracts',
	    	maxLength: 250
	    });
	    
	    me.content = Ext.create('system.widget.FsdUeditor',{
	    	fieldLabel: '内容',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 920,
	        height : 500,
	    	name: 'content'
	    });
	    
	    me.imageurl1 = Ext.create('system.widget.FsdTextFileManage', {
	        width : 450,
	    	zName : 'imageurl1',
	    	zFieldLabel : '标题图片',
	    	zLabelWidth : 80,
	    	zIsShowButton : true,
	    	zIsGetButton : true,
	    	zIsUpButton : true,
	    	zIsReadOnly : false,
	    	zContentObj : me.content,
	    	zFileUpPath : me.oper.util.getImagePath()
	    });
	    
	    me.remark = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 450,
	    	name: 'remark',
	    	maxLength: 50
	    });
	    
	    me.mc = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '名称',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 439,
	    	name: 'mc',
	    	maxLength: 50
	    });
	    
	    me.syh = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '索引号',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 439,
	    	name: 'syh',
	    	maxLength: 50
	    });
	    
	    me.wh = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '文号',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 439,
	    	name: 'wh',
	    	maxLength: 50
	    });
	    
	    me.fwsj = Ext.create('system.widget.FsdDateTime', {
	    	fieldLabel: '发文时间',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 439,
	    	name: 'fwsj',
	    });
	    
	    me.fwjg = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '发文机构',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 439,
	    	name: 'fwjg',
	    	maxLength: 50
	    });
	    
	    me.ztfl = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '主题分类',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 439,
	    	name: 'ztfl',
	    	maxLength: 20
	    });
	    
	    me.zpfl = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '组配分类',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 439,
	    	name: 'zpfl',
	    	maxLength: 20
	    });
	    
	    me.tzfl = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '体裁分类',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 439,
	    	name: 'tzfl',
	    	maxLength: 20
	    });
	    
	    me.gkxs = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '公开形式',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 439,
	    	name: 'gkxs',
	    	maxLength: 20
	    });
	    
	    me.gkfs = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '公开方式',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 439,
	    	name: 'gkfs',
	    	maxLength: 20
	    });
	    
	    me.gkfw = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '公开范围',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 439,
	    	name: 'gkfw',
	    	maxLength: 20
	    });
	    
	    me.yxq = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '有效期',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 439,
	    	name: 'yxq',
	    	maxLength: 20
	    });
	    
	    /**
		 * 栏目面板
		 */
		me.treesubject = Ext.create("system.widget.FsdTreePanel", {
			title: "所属栏目",
		    columnWidth : 0.5,
    		minWidth : 350,
    		border : true,
            rootVisible : false,//不显示根节点
            baseUrl :'B_Subject/get_QxSubjectCheck.do',
	        zTreeRoot : 'children',//一次性加载
            zTreeParams : {id : system.UtilStatic.subjectZTCode},
            tbar : [{
					xtype : "button",
					text : "展开",
					iconCls : 'expand-all',
					handler : function() {
						me.treesubject.expandAll();
					}
				},{
					xtype : "button",
					text : "收缩",
					iconCls : 'collapse-all',
					handler : function() {
						me.treesubject.collapseAll();
					}
				}]
    	});
	    
	    /**
		 * 相关栏目面板
		 */
		me.treesubjectxg = Ext.create("system.widget.FsdTreePanel", {
			title: "相关栏目",
		    columnWidth : 0.5,
    		minWidth : 350,
    		border : true,
            rootVisible : false,//不显示根节点
            baseUrl :'B_Subject/get_QxSubjectCheck.do',
	        zTreeRoot : 'children',//一次性加载
            zTreeParams : {id : system.UtilStatic.subjectXGCode},
            margin:'0 0 0 10',
            tbar : [{
					xtype : "button",
					text : "展开",
					iconCls : 'expand-all',
					handler : function() {
						me.treesubjectxg.expandAll();
					}
				},{
					xtype : "button",
					text : "收缩",
					iconCls : 'collapse-all',
					handler : function() {
						me.treesubjectxg.collapseAll();
					}
				}]
    	});
		
	    me.form = Ext.create('Ext.form.Panel',{
				border : false,
//				trackResetOnLoad : true,
				padding : '10 0 30 20',
				items : [me.idNo, me.type, me.typename, 
				         me.branchname, me.showtypename, me.subjectname,
                {
                    layout : 'column',
                    border : false,
        	        width : 900,
                    items:[{
                        columnWidth : 0.5,
                        border:false,
                        items:[me.branchid, me.source, me.author]
                    }, {
                        columnWidth : 0.5,
                        border:false,
                        items:[me.subjectid, me.titlecolor, me.showtype]
                    }]
    			}, 
    			me.title, me.subtitle, me.showtitle, me.abstracts, me.content,
    			{
                    layout : 'column',
    				padding : '25 0 0 0',
                    border : false,
        	        width : 900,
                    items:[{
                        columnWidth : 0.5,
                        border : false,
                        items : [me.imageurl1, me.aurl, me.voiceurl, me.remark]
                    }, {
                        columnWidth : 0.5,
                        border : false,
                        items : [me.videourl, me.overdue, me.checkboxgroup, me.adddate]
                    }]
    			},{
    				xtype : 'fieldset',
	                title : '公文属性',
                    layout : 'column',
                    border : true,
        	        width : 900,
	                items :[{
                        columnWidth : 0.5,
                        border : false,
                        items : [me.mc, me.wh, me.fwjg, me.zpfl, me.gkxs, me.gkfw]
                    }, {
                        columnWidth : 0.5,
                        border : false,
                        items : [me.syh, me.fwsj, me.ztfl, me.tzfl, me.gkfs, me.yxq]
                    }]
    			},{
					xtype : 'fieldset',
	                title : '关联信息',
                    layout : 'column',
                    border : true,
        	        width : 900,
	                items : [ me.treesubject, me.treesubjectxg ]
				}]
	    });
	    
	    
	    me.treesubject.getStore().on('load', function(s) {
	    	me.treesubject.setChosseValues(me.lstLM);
		});
	    
	    me.treesubjectxg.getStore().on('load', function(s) {
	    	me.treesubjectxg.setChosseValues(me.lstXG);
		});
	    
	    me.on('boxready' , function(obj, width, height, eOpts){
        	me.subjectid.setText(me.subjectname.getValue());
        	me.branchid.setText(me.branchname.getValue());
	    });
	    
		Ext.apply(me, {
        	title: '内容信息发布',
			autoScroll : true,
	        tbar : [{
				xtype : "fsdbutton",
				popedomCode : 'wzbj',
				text : "内容提交",
	        	name : 'btnsave',
				iconCls : 'acceptIcon',
				handler : function(button) {
					me.oper.submitGwxxgl(me, me.grid, me.treesubject, me.treesubjectxg);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'gwxxtj',
				text : "新增一条",
	        	name : 'btnnew',
				iconCls : 'tbar_synchronizeIcon',
				handler : function(button) {
					me.oper.addText(me.grid);
				}
			}],
	        items :[me.form]
	    });
	    
		me.callParent(arguments);
	},
	
	
	setArticleType : function(type){
		var me = this;
		switch (type) {
		case system.UtilStatic.articleTypeWZ :
			me.videourl.setVisible(false);
			me.typename.setValue('文章');
			break;
		}
		me.type.setValue(type);
	}
});