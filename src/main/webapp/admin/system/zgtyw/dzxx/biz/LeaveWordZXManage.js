﻿/**
 * 在线咨询管理界面
 * @author lw
 */
Ext.define('system.zgtyw.dzxx.biz.LeaveWordZXManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.zgtyw.dzxx.model.Extjs_Column_B_leaveword', 
	             'system.zgtyw.dzxx.model.Extjs_Model_B_leaveword'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.zgtyw.dzxx.biz.LeaveWordOper'),
    
	createTable : function() {
	    var me = this;
	    
	    var isreply = '';
		
		var queryid = 'lybzx';

		/**
		 * 查询文本框
		 */
		var queryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入标题',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});

		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_b_leaveword', // 显示列
			model : 'model_b_leaveword',
			baseUrl : 'b_leaveword/load_pagedatabyemployee.do',
			border : false,
			tbar : [{
				xtype : "fsdbutton",
				popedomCode : 'zxzxhf',
				text : "回复",
				iconCls : 'page_editIcon',
				handler : function() {
					me.oper.editor(grid, queryid);
				}
			}, {
				xtype : "fsdbutton",
				text : "删除",
				iconCls : 'page_deleteIcon',
				popedomCode : 'zxzxsc',
				handler : function(button) {
				    me.oper.delZX(grid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid, queryid);
				}
			}, '-', {
				xtype : "fsdbutton",
				popedomCode : 'zxzxsh',
				text : "审核通过",
				iconCls : 'tickIcon',
				handler : function(button) {
				    me.oper.auditZX(grid, true);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'zxzxsh',
				text : "审核折回",
				iconCls : 'crossIcon',
				handler : function(button) {
				    me.oper.auditZX(grid, false);
				}
			}, '-', {
				xtype : "fsdbutton",
				popedomCode : 'zxzxxd',
				text : "选登信息",
				iconCls : 'tickIcon',
				handler : function(button) {
				    me.oper.chooseZX(grid, true);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'zxzxxd',
				text : "取消选登",
				iconCls : 'crossIcon',
				handler : function(button) {
				    me.oper.chooseZX(grid, false);
				}
			}, '-', {
				xtype : "fsdbutton",
				popedomCode : 'gkyjhf',
				text : "设置敏感",
				iconCls : 'tickIcon',
				handler : function(button) {
				    me.oper.flageObject(grid, true);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'gkyjhf',
				text : "取消敏感",
				iconCls : 'crossIcon',
				handler : function(button) {
				    me.oper.flageObject(grid, false);
				}
			}, '-', {
				xtype : "fsdbutton",
				popedomCode : 'dzxxbgr',
				text : "回复人变更",
				iconCls : 'arrow_redoIcon',
				handler : function(button) {
				    me.oper.updateHFR(grid);
				}
			}, '-', {
				xtype : "fsdbutton",
				text : "查看已回复",
				iconCls : 'magnifierIcon',
				handler : function(button) {
					isreply = 'true';
					grid.getStore().loadPage(1);
				}
			}, {
				xtype : "fsdbutton",
				text : "查看未回复",
				iconCls : 'magnifierIcon',
				handler : function(button) {
					isreply = 'false';
					grid.getStore().loadPage(1);
				}
			}, {
				xtype : "fsdbutton",
				text : "查看全部",
				iconCls : 'magnifierIcon',
				handler : function(button) {
					isreply = '';
					grid.getStore().loadPage(1);
				}
			}, '->', queryText, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  queryText.getValue();
			var pram = {fid : queryid, name : name, glqx : 'zxzxcx', isreply : isreply};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		grid.on('cellclick', function(thisObj, td, cellIndex, record, tr, rowIndex, e, eOpts) {
			if (cellIndex == 7){
				var value = record.get('path');
				alert(value);
			}
		});
		return grid;
	}
});