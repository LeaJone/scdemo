﻿/**
 * 编辑界面
 * @author lw
 */
Ext.define('system.zgtyw.dzxx.biz.LeaveWordEdit', {
	extend : 'system.widget.FsdWindowFlow',
	requires : ['system.widget.FsdTreeComboBox'],
	grid : null,
	oper : Ext.create('system.zgtyw.dzxx.biz.LeaveWordOper'),
	type : '',
	initComponent:function(){
	    var me = this;

	    var name = new Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '姓名',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'name'
	    });
	    
	    var idcard = new Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '身份证号',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'idcard'
	    });
	    
	    var workunit = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '工作单位',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'workunit'
	    });
	    
	    var email = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '电子邮箱',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'email'
	    });
	    
	    var telphone = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '联系电话',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'telphone'
	    });
	    
	    var postalcode = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '邮政编码',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'postalcode'
	    });
	    
	    var address = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '联系地址',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'address'
	    });
	    
	    var accessories = Ext.create('system.widget.FsdTextFileManage', {
	        width : 340,
	    	zName : 'accessories',
	    	zFieldLabel : '附件',
	    	zLabelWidth : 60,
	    	zIsDownButton : true
	    });
	    
	    var qureycode = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '查询码',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'qureycode'
	    });
	    
	    var title = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '主题',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 690,
	    	name: 'title'
	    });
	    
	    var content = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '内容',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 690,
	        height : 80,
	    	name: 'content'
	    });
	    
	    

	    
	    var atitle = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '回复主题',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 690,
	    	name: 'atitle',
	    	maxLength: 50,
	    	allowBlank: me.isShowFlowData
	    });

	    var acontent = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '回复内容',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 690,
	        height : 80,
	    	name: 'acontent',
	    	maxLength: 3000,
	    	allowBlank: me.isShowFlowData
	    });
	    
	    me.lypanle = Ext.create('Ext.form.FieldSet',{
            title : '留言信息',
            items:[{
                layout : 'column',
                border : false,
                items:[{
                    columnWidth : 0.5,
                    border:false,
                    items:[name, idcard, workunit, email, qureycode]
                }, {
                    columnWidth : 0.5,
                    border:false,
                    items:[telphone, postalcode, address, accessories]
                }]
			}
			, title, content]
	    });
	    
	    me.form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			header : false,// 是否显示标题栏
            border : false,
			padding : '20 20 0 20',
			items : [
				{
					name: "id",
					xtype: "hidden"
				}, 
				me.lypanle,
				{
					xtype : 'fieldset',
	                title : '回复信息',
	                items : [ atitle, acontent ]
				}]
        });
	    
	    var buttonItem = [{
			xtype : "fsdbutton",
        	name : 'btnsave',
		    text : '保存',
		    zIsSpace : true,
		    iconCls : 'acceptIcon',
		    handler : function() {
		    	switch (me.type) {
				case 'lybmy':
    				me.oper.submitMY(me.form, me.grid);
					break;
				case 'lybzx':
    				me.oper.submitZX(me.form, me.grid);
					break;
				case 'lybxf':
    				me.oper.submitXF(me.form, me.grid);
					break;
				case 'lybxc':
    				me.oper.submitXC(me.form, me.grid);
					break;
				case 'lybtd':
    				me.oper.submitTD(me.form, me.grid);
					break;
				case 'lybkc':
    				me.oper.submitKC(me.form, me.grid);
					break;
				case 'lybld':
    				me.oper.submitLD(me.form, me.grid);
					break;
				case 'lybgkyj':
    				me.oper.submitGK(me.form, me.grid);
					break;
				case 'lybqzyj':
    				me.oper.submitQZ(me.form, me.grid);
					break;
				case 'lybsl':
    				me.oper.submitSL(me.form, me.grid);
					break;
				default:
					break;
				}
		    }
	    }, {
		    text : '关闭',
		    iconCls : 'deleteIcon',
		    handler : function() {
		       me.close();
            }
	    }];

	    //是否是流程数据
	    var intHeight = 510;
	    if (me.isShowFlowData){
	    	buttonItem = null;
	    	intHeight = intHeight - 55;
	    }
	    
	    Ext.apply(this,{
	        width:770,
	        height:intHeight,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[me.form],
	        buttons : buttonItem
	    });
	    this.callParent(arguments);
	},
	
	loadFormData : function(id, url, success, failure){
		var me = this;
		if (url == null){
			url = 'b_leaveword/load_LeaveWordByid.do';
		}
		var pram = {id : id};
        var param = {jsonData : Ext.encode(pram)};
        me.oper.util.formLoad(me.form.getForm(), url, param, null, success);
	}
});