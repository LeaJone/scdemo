/**
 * 留言板界面操作类
 * @author lw
 */
Ext.define('system.zgtyw.dzxx.biz.LeaveWordOper', {

	requires : [ 'system.abasis.employee.model.Extjs_A_employee'],
	form : null,
	util : Ext.create('system.util.util'),
    

	submitMY : function(formpanel, grid){
		this.formSubmit(formpanel, grid, 'z_leaveword/save_dzxx_qx_myzjhf.do');
	},
	submitZX : function(formpanel, grid){
		this.formSubmit(formpanel, grid, 'z_leaveword/save_dzxx_qx_zxzxhf.do');
	},
	submitXF : function(formpanel, grid){
		this.formSubmit(formpanel, grid, 'z_leaveword/save_dzxx_qx_xfxxhf.do');
	},
	submitXC : function(formpanel, grid){
		this.formSubmit(formpanel, grid, 'z_leaveword/save_dzxx_qx_xcxxhf.do');
	},
	submitTD : function(formpanel, grid){
		this.formSubmit(formpanel, grid, 'z_leaveword/save_dzxx_qx_tdwfjbhf.do');
	},
	submitKC : function(formpanel, grid){
		this.formSubmit(formpanel, grid, 'z_leaveword/save_dzxx_qx_kcwfjbhf.do');
	},
	submitLD : function(formpanel, grid){
		this.formSubmit(formpanel, grid, 'z_leaveword/save_dzxx_qx_ldxxhf.do');
	},
	submitGK : function(formpanel, grid){
		this.formSubmit(formpanel, grid, 'z_leaveword/save_dzxx_qx_gkyjhf.do');
	},
	submitQZ : function(formpanel, grid){
		this.formSubmit(formpanel, grid, 'z_leaveword/save_dzxx_qx_qzyjxhf.do');
	},
	submitSL : function(formpanel, grid){
		this.formSubmit(formpanel, grid, 'z_leaveword/save_dzxx_qx_slwthf.do');
	},
	/**
     * 表单提交
     */
    formSubmit : function(formpanel , grid, url){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit(url, formpanel.form, '正在提交数据,请稍候.....',
			function(form, action){
			    formpanel.up('window').close();
			    grid.getStore().reload();
				Ext.MessageBox.alert('提示', '保存成功！');
			});
        }
    },
	
	/**
     * 修改
     */
	editor : function(grid, type){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{
        	// 先得到主键
            var id = data[0].data.id;
            var win = Ext.create('system.zgtyw.dzxx.biz.LeaveWordEdit');
            win.setTitle('回复留言');
            win.grid = grid;
            win.type = type;
            win.modal = true;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'b_leaveword/load_LeaveWordByid.do' , param , null , 
	        function(response, options, respText, obj){
	            win.setTitle(obj.typename);
	            win.show();
	        });
        }
	},
	
 	/**
     * 查看
     */
	view : function(grid, type){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if(data.length > 0){
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.zgtyw.dzxx.biz.LeaveWordEdit');
            win.setTitle('信息查看');
            win.type = type;
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'b_leaveword/load_LeaveWordByid.do' , param , null , 
	        function(response, options, respText, obj){
	            win.setTitle(obj.typename);
	            win.show();
	        });
        }
	},

	delMY : function(grid){
 		this.del(grid, 'z_leaveword/del_dzxx_qx_myzjsc.do');
 	},
 	delZX : function(grid){
 		this.del(grid, 'z_leaveword/del_dzxx_qx_zxzxsc.do');
 	},
 	delXF : function(grid){
 		this.del(grid, 'z_leaveword/del_dzxx_qx_xfxxsc.do');
 	},
 	delXC : function(grid){
 		this.del(grid, 'z_leaveword/del_dzxx_qx_xcxxsc.do');
 	},
 	delTD : function(grid){
 		this.del(grid, 'z_leaveword/del_dzxx_qx_tdwfjbsc.do');
 	},
 	delKC : function(grid){
 		this.del(grid, 'z_leaveword/del_dzxx_qx_kcwfjbsc.do');
 	},
 	delLD : function(grid){
 		this.del(grid, 'z_leaveword/del_dzxx_qx_ldxxsc.do');
 	},
 	delGK : function(grid){
 		this.del(grid, 'z_leaveword/del_dzxx_qx_gkyjsc.do');
 	},
 	delQZ : function(grid){
 		this.del(grid, 'z_leaveword/del_dzxx_qx_qzyjxsc.do');
 	},
 	delSL : function(grid){
 		this.del(grid, 'z_leaveword/del_dzxx_qx_slwtsc.do');
 	},
	/**
     * 删除
     */
	del : function(grid, url){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要删除的项目！');
        }else {
        	Ext.MessageBox.show({
        		title : '询问',
        		msg : '您确定要删除吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                   	 var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
            			});
                        var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest(url, param,
                        function(response, options){
	                       	grid.getStore().reload();
                        }, null, me.form);
                    }
                }
            });
        }
	},
 	
	auditMY : function(grid, isQT){
 		this.audit(grid, isQT, 'z_leaveword/audit_dzxx_qx_myzjsh.do');
 	},
 	auditZX : function(grid, isQT){
 		this.audit(grid, isQT, 'z_leaveword/audit_dzxx_qx_zxzxsh.do');
 	},
 	auditXF : function(grid, isQT){
 		this.audit(grid, isQT, 'z_leaveword/audit_dzxx_qx_xfxxsh.do');
 	},
 	auditXC : function(grid, isQT){
 		this.audit(grid, isQT, 'z_leaveword/audit_dzxx_qx_xcxxsh.do');
 	},
 	auditTD : function(grid, isQT){
 		this.audit(grid, isQT, 'z_leaveword/audit_dzxx_qx_tdwfjbsh.do');
 	},
 	auditKC : function(grid, isQT){
 		this.audit(grid, isQT, 'z_leaveword/audit_dzxx_qx_kcwfjbsh.do');
 	},
 	auditLD : function(grid, isQT){
 		this.audit(grid, isQT, 'z_leaveword/audit_dzxx_qx_ldxxsh.do');
 	},
 	auditGK : function(grid, isQT){
 		this.audit(grid, isQT, 'z_leaveword/audit_dzxx_qx_gkyjsh.do');
 	},
 	auditQZ : function(grid, isQT){
 		this.audit(grid, isQT, 'z_leaveword/audit_dzxx_qx_qzyjxsh.do');
 	},
 	auditSL : function(grid, isQT){
 		this.audit(grid, isQT, 'z_leaveword/audit_dzxx_qx_slwtsh.do');
 	},
	/**
     * 是否审核
     */
 	audit : function(grid, isQT, url){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
		        msg : '您确定要修改信息状态吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
					    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
			            });
             			var pram = {ids : dir, status : isQT};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest(url, param,
                        function(response, options){
                        	grid.getStore().reload(); 
                        });
                    }
                }
            });
        }
	},
 	
	chooseMY : function(grid, isQT){
 		this.audit(grid, isQT, 'z_leaveword/choose_dzxx_qx_myzjxd.do');
 	},
 	chooseZX : function(grid, isQT){
 		this.audit(grid, isQT, 'z_leaveword/choose_dzxx_qx_zxzxxd.do');
 	},
 	chooseXF : function(grid, isQT){
 		this.audit(grid, isQT, 'z_leaveword/choose_dzxx_qx_xfxxxd.do');
 	},
 	chooseXC : function(grid, isQT){
 		this.audit(grid, isQT, 'z_leaveword/choose_dzxx_qx_xcxxxd.do');
 	},
 	chooseTD : function(grid, isQT){
 		this.audit(grid, isQT, 'z_leaveword/choose_dzxx_qx_tdwfjbxd.do');
 	},
 	chooseKC : function(grid, isQT){
 		this.audit(grid, isQT, 'z_leaveword/choose_dzxx_qx_kcwfjbxd.do');
 	},
 	chooseLD : function(grid, isQT){
 		this.audit(grid, isQT, 'z_leaveword/choose_dzxx_qx_ldxxxd.do');
 	},
 	chooseGK : function(grid, isQT){
 		this.audit(grid, isQT, 'z_leaveword/choose_dzxx_qx_gkyjxd.do');
 	},
 	chooseQZ : function(grid, isQT){
 		this.audit(grid, isQT, 'z_leaveword/choose_dzxx_qx_qzyjxxd.do');
 	},
 	chooseSL : function(grid, isQT){
 		this.audit(grid, isQT, 'z_leaveword/choose_dzxx_qx_slwtxd.do');
 	},
	/**
     * 是否审核
     */
 	choose : function(grid, isQT, url){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
		        msg : '您确定要修改信息状态吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
					    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
			            });
             			var pram = {ids : dir, status : isQT};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest(url, param,
                        function(response, options){
                        	grid.getStore().reload(); 
                        });
                    }
                }
            });
        }
	},
	
	/**
     * 是否敏感标志
     */
 	flageObject : function(grid, isQT){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
		        msg : '您确定要敏感标志状态吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
					    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
			            });
             			var pram = {ids : dir, flage : isQT};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('z_leaveword/audit_LeaveWordMgbz.do', param,
                        function(response, options){
                        	grid.getStore().reload(); 
                        });
                    }
                }
            });
        }
	},
	
	/**
     * 变更回复人
     */
 	updateHFR : function(grid){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
		        msg : '您确定要变更回复人员吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                    	var winChoose = Ext.create('system.admin.tools.biz.ChooseOperate', {
                			column : 'column_a_employee_role',
                			model : 'model_a_employee',
                			baseUrl : 'A_Employee/load_dataall.do',
                      		gridType : 'list',
                			isText1 : true,
                      		isText2 : true,
                      		txtLabel1 : '部门名称',
                      		txtLabel2 : '身份证号码',
                        	funQuery : function(txt1, txt2, txt3, txt4){
                        		return {branchname : txt1, realname : txt2, status : 'ryztqy'};
                        	},
                        	funChoose : function(model, funClose){
        					    var dir = new Array();　
                                Ext.Array.each(data, function(items) {
                                    var id = items.data.id;
                                    dir.push(id);
        			            });
                     			var pram = {ids : dir, empid : model.id};
                                var param = {jsonData : Ext.encode(pram)};
                                me.util.ExtAjaxRequest('z_leaveword/hfrbg_dzxx_qx_dzxxbgr.do', param,
                                function(response, options){
                                	Ext.MessageBox.alert('提示', '回复人员变更成功！');
                                	grid.getStore().reload(); 
                                	funClose();
                                }, null, winChoose);
                        	}
                        });
                        winChoose.setTitle('人员证书查询');
                        winChoose.modal = true;
                        winChoose.show();
                    }
                }
            });
        }
	}
	
});