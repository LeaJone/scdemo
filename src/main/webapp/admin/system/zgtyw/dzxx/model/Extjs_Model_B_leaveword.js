﻿/**
 *b_leaveword Model
 *@author CodeSystem
 *文件名     Extjs_Model_B_leaveword
 *Model名    model_b_leaveword
 */

Ext.define('model_b_leaveword', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'type',
            type : 'string'
        }
        ,
        {
            name : 'typename',
            type : 'string'
        }
        ,
        {
            name : 'name',
            type : 'string'
        }
        ,
        {
            name : 'idcard',
            type : 'string'
        }
        ,
        {
            name : 'title',
            type : 'string'
        }
        ,
        {
            name : 'accessories',
            type : 'string'
        }
        ,
        {
            name : 'submitdate',
            type : 'string'
        }
        ,
        {
            name : 'qureycode',
            type : 'string'
        }
        ,
        {
            name : 'auditing',
            type : 'string'
        }
        ,
        {
            name : 'selected',
            type : 'string'
        }
        ,
        {
            name : 'isflage',
            type : 'string'
        }
        ,
        {
            name : 'isreply',
            type : 'string'
        }
        ,
        {
            name : 'adddate',
            type : 'string'
        }
        ,
        {
            name : 'abranchid',
            type : 'string'
        }
        ,
        {
            name : 'abranchname',
            type : 'string'
        }
        ,
        {
            name : 'addemployeeid',
            type : 'string'
        }
        ,
        {
            name : 'addemployeename',
            type : 'string'
        }
        ,
        {
            name : 'flowstatus',
            type : 'string'
        }
        ,
        {
            name : 'flowstatusname',
            type : 'string'
        }
    ]
});
