﻿/**
 * 发布统计-单位部门管理界面
 * @author 
 */

Ext.define('system.zgtyw.fbtj.biz.dwBmManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*', 
	             'system.widget.FsdButton',
	             'system.zgtyw.fbtj.model.Extjs_Column_B_dwBm', 
	             'system.zgtyw.fbtj.model.Extjs_Model_B_dwBm'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.zgtyw.fbtj.biz.dwBmOper'),
    
	createTable : function() {
	    var me = this;
		var arrayYear = new Array();
	    var myDate = new Date();
	    var num = myDate.getFullYear();
	    for ( var i = num; i >= 2006; i--) {
	    	arrayYear. push([String(i), String(i) + '年']);
		}
	    var year = Ext.create('system.widget.FsdComboBox',{
            fieldLabel:'年份',
	        labelAlign:'right',
	    	labelWidth:60,
	        width : 150,
    	    name : 'auditing',//提交到后台的参数名
            store : arrayYear
	    });
	    
	  
		var branchid = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属机构',
	    	labelAlign:'right',
	        name : 'branchid',
	    	labelWidth:80,
	    	rootText : me.oper.util.getParams("company").name,
	    	rootId : me.oper.util.getParams("company").id,
			//rootVisible: false,
	        width : 250,
	        baseUrl:'A_Branch/load_AsyncBranchTree.do',//访问路劲
	        allowBlank: false,
	    });

		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPanel", {
			column : 'column_b_dwBm', // 显示列
			model : 'model_b_dwBm',
			baseUrl : 'Z_fbtj/stat_data.do',
			border : false,
			zAutoLoad : false,
			tbar : [ year, branchid, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, {
				text : '生成月统计',
				iconCls : 'articlewz',
				handler : function() {
					var win = Ext.create('system.zgtyw.fbtj.biz.fbtjscWin');
			        win.setTitle('生成月统计');
			        win.modal = true;
			        win.show();
				}
			}, '->', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}, {
				xtype : "fsdbutton",
				text : "下载",
				iconCls : 'articlewj',
				popedomCode : 'bmxz',
				handler : function(button) {
					if(branchid.getValue() == null || ""==branchid.getValue()){
						Ext.MessageBox.alert('提示', '请选择单位或部门！');
					}else{
						me.oper.download(year.getValue(), branchid.getValue(),grid,me);
					}
				}
			}]	
		});
		grid.getStore().on('beforeload', function(s) {
			var y =  year.getValue();
			var sub =  branchid.getValue();
			var pram = {year : y, branchid : sub};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		year.select(String(num));
		branchid.setValue(me.oper.util.getParams("company").id);

		return grid;
	}
});