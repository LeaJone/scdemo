﻿/**
 * 发布统计生成文章界面
 * @author lw
 */
Ext.define('system.zgtyw.fbtj.biz.fbtjscWin', {
	extend : 'Ext.window.Window',
	util : Ext.create('system.util.util'),
	initComponent:function(){
	    var me = this;
	    
	    var arrayYear = new Array();
		var arrayMonth = new Array();
	    var myDate = new Date();
	    var yearNum = myDate.getFullYear();
	    var monthNum = myDate.getMonth()+1;
	    for ( var i = yearNum; i >= 2016; i--) {
	    	arrayYear. push([String(i), String(i) + '年']);
		}
	    for ( var j = 1; j <= 12; j++) {
	    	if(String(j).length == 1){
	    		arrayMonth. push(["0" + String(j), String(j) + '月']);
	    	}else{
	    		arrayMonth. push([String(j), String(j) + '月']);
	    	}
		}
	    var startYear = Ext.create('system.widget.FsdComboBox',{
            fieldLabel:'选择年份',
	        labelAlign:'right',
	    	labelWidth:60,
	        width : 200,
    	    name : 'year',//提交到后台的参数名
            store : arrayYear
	    });
	    
	    var startMonth = Ext.create('system.widget.FsdComboBox',{
            fieldLabel:'选择年份',
	        labelAlign:'right',
	    	labelWidth:60,
	        width : 200,
    	    name : 'month',//提交到后台的参数名
            store : arrayMonth
	    });
	    
		startYear.select(String(yearNum));
		if(String(monthNum).length == 1){
			startMonth.select("0" + String(monthNum));
		}else{
			startMonth.select(String(monthNum));
		}
	    
	    var formpanel = new Ext.form.Panel({
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 45',
			items : [startYear, startMonth, 
			         {
				xtype : "fsdbutton",
				margin : '15 0 0 0',
			    text : '生成厅机关',
		        width : 200,
			    handler : function() {
			        var pram = {year : startYear.getValue(), month : startMonth.getValue()};
	                var param = {jsonData : Ext.encode(pram)};
	                me.util.ExtAjaxRequest('Z_fbtj/save_ScWzTjg.do', param,
	                function(response, options){
	    				Ext.MessageBox.alert('提示', '生成厅机关文章成功！');
	                }, null, me);
			    }
		    }, {
				xtype : "fsdbutton",
				margin : '10 0 0 0',
			    text : '生成事业单位',
		        width : 200,
			    handler : function() {
			        var pram = {year : startYear.getValue(), month : startMonth.getValue()};
	                var param = {jsonData : Ext.encode(pram)};
	                me.util.ExtAjaxRequest('Z_fbtj/save_ScWzSydw.do', param,
	                function(response, options){
	    				Ext.MessageBox.alert('提示', '生成事业单位文章成功！');
	                }, null, me);
			    }
		    }
		    ]
	    });
	    Ext.apply(this,{
	        width:300,
	        height:200,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[formpanel]
	    });
	    this.callParent(arguments);
	}
});