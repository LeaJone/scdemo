﻿/**
 * 发布登记-单位部门管理界面
 * @author 
 */

Ext.define('system.zgtyw.fbtj.biz.FbDwBmManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*', 
	             'system.widget.FsdButton',
	             'system.zgtyw.fbtj.model.Extjs_Column_B_FbDwBm', 
	             'system.zgtyw.fbtj.model.Extjs_Model_B_FbDwBm'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.zgtyw.fbtj.biz.FbDwBmOper'),
    
    createTable : function() {
	    var me = this;
		var arrayYear = new Array();
		var arrayMonth = new Array();
	    var myDate = new Date();
	    var yearNum = myDate.getFullYear();
	    var monthNum = myDate.getMonth()+1;
	    for ( var i = yearNum; i >= 2006; i--) {
	    	arrayYear. push([String(i), String(i) + '年']);
		}
	    for ( var j = 1; j <= 12; j++) {
	    	if(String(j).length == 1){
	    		arrayMonth. push(["0" + String(j), String(j) + '月']);
	    	}else{
	    		arrayMonth. push([String(j), String(j) + '月']);
	    	}
		}
	    var startYear = Ext.create('system.widget.FsdComboBox',{
            fieldLabel:'起始年月',
	        labelAlign:'right',
	    	labelWidth:60,
	        width : 150,
    	    name : 'startYear',//提交到后台的参数名
            store : arrayYear
	    });
	    
	    var startMonth = Ext.create('system.widget.FsdComboBox',{
	        labelAlign:'right',
	    	labelWidth:60,
	        width : 150,
    	    name : 'startMonth',//提交到后台的参数名
            store : arrayMonth
	    });
	    
	    var endYear = Ext.create('system.widget.FsdComboBox',{
            fieldLabel:'终止年月',
	        labelAlign:'right',
	    	labelWidth:60,
	        width : 150,
    	    name : 'endYear',//提交到后台的参数名
            store : arrayYear
	    });
	    
	    var endMonth = Ext.create('system.widget.FsdComboBox',{
	        labelAlign:'right',
	    	labelWidth:60,
	        width : 150,
    	    name : 'endMonth',//提交到后台的参数名
            store : arrayMonth
	    });
	    
	    var branchid = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '部门',
	    	labelAlign:'right',
	        name : 'branchid',
	    	labelWidth:80,
	    	rootText : me.oper.util.getParams("company").name,
	    	rootId : me.oper.util.getParams("company").id,
			//rootVisible: false,
	        width : 250,
	        baseUrl:'A_Branch/load_AsyncBranchTree.do',//访问路劲
	        allowBlank: false,
	    });


	    
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPanel", {
			column : 'column_b_FbDwBm', // 显示列
			model : 'model_b_FbDwBm',
			baseUrl : 'Z_fbtj/stat_dataFb.do',
			border : false,
			zAutoLoad : false,
			tbar : [ startYear, startMonth, endYear, endMonth, branchid,{
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, '->', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}, {
				xtype : "fsdbutton",
				text : "下载",
				iconCls : 'articlewj',
				popedomCode : 'bmxz',
				handler : function(button) {
					if(branchid.getValue() == null || ""==branchid.getValue()){
					Ext.MessageBox.alert('提示', '请选择单位或部门！');
					}else{
						me.oper.download(startYear.getValue(), startMonth.getValue(), endYear.getValue(), endMonth.getValue(), branchid.getValue(), grid);
					}
				 }
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var sy = startYear.getValue();
			var sm = startMonth.getValue();
			var ey = endYear.getValue();
			var em = endMonth.getValue();
			var sub =  branchid.getValue();
			var pram = {startYear : sy, startMonth : sm, endYear : ey, endMonth : em, branchid : sub};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		startYear.select(String(yearNum));
		if(String(monthNum).length == 1){
			startMonth.select("0" + String(monthNum));
		}else{
			startMonth.select(String(monthNum));
		}
		endYear.select(String(yearNum));
		if(String(monthNum).length == 1){
			endMonth.select("0" + String(monthNum));
		}else{
			endMonth.select(String(monthNum));
		}
		
		branchid.setValue(me.oper.util.getParams("company").id);

		return grid;
	}
});