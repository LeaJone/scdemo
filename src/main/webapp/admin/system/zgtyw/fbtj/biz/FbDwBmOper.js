/**
 * 发布登记-单位部门操作类
 * @author lw
 */

Ext.define('system.zgtyw.fbtj.biz.FbDwBmOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	
	/**
     * 下载
     */
	 download : function(startYear, startMonth, endYear, endMonth, branchid, grid){
    	 var me = this;
    	//获取选中的行
  		var data = grid.getSelectionModel().getSelection();
    	 Ext.MessageBox.show({
    		 title : '询问',
    		 msg : '您确定要下载吗?',
		     width : 250,
			 buttons : Ext.MessageBox.YESNO,
             icon : Ext.MessageBox.QUESTION,
             fn : function(btn) {
            	 var dir = new Array();　
                 Ext.Array.each(data, function(items) {
                         var id = items.data.id;
                         dir.push(id);
         		});
                 if (btn == 'yes') {
                	 var pram = {startYear : startYear, startMonth : startMonth, endYear : endYear, endMonth : endMonth, branchid : branchid, ids : dir};
                     var param = {jsonData : Ext.encode(pram)};
                    me.util.ExtAjaxRequest('Z_fbtj/downloadFb.do' , param ,
                    function(response, options){
                    	var respText = Ext.JSON.decode(response.responseText);
                    	window.open(respText.data.httpUrl);
                    }, null, me.form);
                 }
             }
         });
     }
});