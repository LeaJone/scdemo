/**
 * 发布统计-作者操作类
 * @author lw
 */

Ext.define('system.zgtyw.fbtj.biz.zzOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	
	/**
     * 下载
     */
	download : function(year, Author, grid){
   	 var me = this;
 	//获取选中的行
	var data = grid.getSelectionModel().getSelection();
   	 Ext.MessageBox.show({
   		 title : '询问',
   		 msg : '您确定要下载吗?',
		     width : 250,
			 buttons : Ext.MessageBox.YESNO,
            icon : Ext.MessageBox.QUESTION,
            fn : function(btn) {
            	var nir = new Array();
            	var bir = new Array();
                Ext.Array.each(data, function(items) {
                        var names = items.data.name;
                        var branchs = items.data.BranchID;
                        nir.push(names);
                        bir.push(branchs);
        		});
                if (btn == 'yes') {
               	 var pram = {year : year, Author : Author, nir : nir, bir : bir};
                    var param = {jsonData : Ext.encode(pram)};
                   me.util.ExtAjaxRequest('Z_fbtj/downloadAuthor.do' , param ,
                   function(response, options){
                   	var respText = Ext.JSON.decode(response.responseText);
                   	window.open(respText.data.httpUrl);
                   }, null, me.form);
                }
            }
        });
    }
});