/**
 * 发布统计-单位部门操作类
 * @author lw
 */

Ext.define('system.zgtyw.fbtj.biz.dwBmOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
	 * 下载
	 */
	 download : function(year, branch, grid, form){
    	 var me = this;
    	 me.form = form;
    	//获取选中的行
 		var data = grid.getSelectionModel().getSelection();
 		Ext.MessageBox.show({
    		 title : '询问',
    		 msg : '您确定要下载吗?',
		     width : 250,
			 buttons : Ext.MessageBox.YESNO,
             icon : Ext.MessageBox.QUESTION,
             fn : function(btn) {
                 if (btn == 'yes') {
                	 var dir = new Array();　
                     Ext.Array.each(data, function(items) {
                             var id = items.data.id;
                             dir.push(id);
             		});
                	var pram = {ids : dir, year : year, branch : branch};
                    var param = {jsonData : Ext.encode(pram)};
                    me.util.ExtAjaxRequest('Z_fbtj/download.do' , param ,
                    function(response, options){
                    	var respText = Ext.JSON.decode(response.responseText);
                    	window.open(respText.data.httpUrl);
                    }, null, me.form);
                 }
             }
         });
     }
});