﻿/**
 * 发布统计-作者管理界面
 * @author 
 */

Ext.define('system.zgtyw.fbtj.biz.zzManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*', 
	             'system.widget.FsdButton',
	             'system.zgtyw.fbtj.model.Extjs_Column_B_zz', 
	             'system.zgtyw.fbtj.model.Extjs_Model_B_zz'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.zgtyw.fbtj.biz.zzOper'),
    
	createTable : function() {
	    var me = this;

		/**
		 * 查询文本框
		 */
		var Author =  Ext.create("Ext.form.TextField" , {
			fieldLabel:'作者',
			labelAlign:'right',
			labelWidth:50,
	        width : 200,
			name : 'Author',
			allowBlank: false,
			blankText: '请输入作者名称',
			emptyText : '请输入作者名称',
			enableKeyEvents : true,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});
		
		var arrayYear = new Array();
	    var myDate = new Date();
	    var num = myDate.getFullYear();
	    for ( var i = num; i >= 2006; i--) {
	    	arrayYear. push([String(i), String(i) + '年']);
		}
	    var year = Ext.create('system.widget.FsdComboBox',{
            fieldLabel:'年份',
	        labelAlign:'right',
	    	labelWidth:60,
	        width : 150,
    	    name : 'year',//提交到后台的参数名
            store : arrayYear
	    });

		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPanel", {
			column : 'column_b_zz', // 显示列
			model : 'model_b_zz',
			baseUrl : 'Z_fbtj/stat_dataAuthor.do',
			border : false,
			zAutoLoad : false,
			tbar : [ year, Author, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			},'->', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}, {
				xtype : "fsdbutton",
				text : "" +
						"下载",
				iconCls : 'articlewj',
				popedomCode : 'lmxz',
				handler : function(button) {
					me.oper.download(year.getValue(),Author.getValue(),grid);
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var y =  year.getValue();
			var sub =  Author.getValue();
			var pram = {year : y, Author : sub};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		year.select(String(num));

		return grid;
	}
});