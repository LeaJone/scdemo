﻿/**
 *b_FbDwBm Column
 *@author CodeSystem
 *文件名     Extjs_Column_B_FbDwBm
 *Columns名  column_b_FbDwBm
 */

Ext.define('column_b_FbDwBm', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            width : 30
        }
        ,
        {
            header : '标题',
            dataIndex : 'title',
            sortable : true,
            flex : 8
        }
        ,
        {
            header : 'ID号',
            dataIndex : 'id',
            sortable : true,
            flex : 2
        }

        ,
        {
            header : '信息类别',
            dataIndex : 'typename',
            sortable : true,
            flex : 2
        }

        ,
        {
            header : '字数',
            dataIndex : 'wordnum',
            sortable : true,
            flex : 2
        }

        ,
        {
            header : '照片',
            dataIndex : 'imagenum',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '放置栏目',
            dataIndex : 'subjectname',
            sortable : true,
            flex : 2
        }
//        ,
//        {
//            header : '取图',
//            dataIndex : 'imageurl1',
//            sortable : true,
//            flex : 1,
//			renderer :function(value ,metaData ,record ){
//				if(value != ''){
//					value="<font style='color:blue;'>是</font>";
//				}else{
//					value='';
//				}
//				return value;
//			}
//        }
//        ,
//        {
//            header : '手机',
//            dataIndex : 'ismobile',
//            sortable : true,
//            flex : 1,
//			renderer :function(value ,metaData ,record ){
//				if(value == 'true'){
//					value="<font style='color:blue;'>是</font>";
//				}else{
//					value="<font style='color:gray;'>否</font>";
//				}
//				return value;
//			}
//        }
//        ,
//        {
//            header : '手机栏目',
//            dataIndex : 'msubjectname',
//            sortable : true,
//            flex : 2
//        }
//        ,
//        {
//            header : '操作人',
//            dataIndex : 'addemployeename',
//            sortable : true,
//            flex : 2
//        }
        ,
        {
            header : '提交日期',
            dataIndex : 'adddate',
            sortable : true,
            flex : 4,
			renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value);
			}
        }
        ,
        {
            header : '提交人',
            dataIndex : 'addemployeename',
            sortable : true,
            flex : 2
        }
    ]
});
