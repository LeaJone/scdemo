﻿/**
 *b_FbDwBm Model
 *@author CodeSystem
 *文件名     Extjs_Model_B_FbDwBm
 *Model名    model_b_FbDwBm
 */

Ext.define('model_b_FbDwBm', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'rownumberer',
            type : 'string'
        }
        ,
        {
            name : 'title',
            type : 'string'
        }
        ,
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'typename',
            type : 'string'
        }

        ,
        {
            name : 'wordnum',
            type : 'string'
        }

        ,
        {
            name : 'imagenum',
            type : 'string'
        }
        ,
        {
            name : 'subjectname',
            type : 'string'
        }
        ,
        {
            name : 'adddate',
            type : 'string'
        }
        ,
        {
            name : 'addemployeename',
            type : 'string'
        }
    ]
});
