﻿/**
 *b_zz Column
 *@author CodeSystem
 *文件名     Extjs_Column_B_zz
 *Columns名  column_b_zz
 */

Ext.define('column_b_zz', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            width : 30
        }
        ,
        {
            header : '单位',
            dataIndex : 'BranchName',
            sortable : true,
            flex : 6
        }
        ,
        {
            header : '姓名',
            dataIndex : 'name',
            sortable : true,
            flex : 8
        }
        ,
        {
            header : '一月',
            dataIndex : 'January',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '二月',
            dataIndex : 'February',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '三月',
            dataIndex : 'March',
            sortable : true,
            flex : 2
        }
        ,
        {
        	header : '四月',
            dataIndex : 'April',
            sortable : true,
            flex : 2
        }
        ,
        {
        	header : '五月',
            dataIndex : 'May',
            sortable : true,
            flex : 2
        }
        ,
        {
        	header : '六月',
            dataIndex : 'June',
            sortable : true,
            flex : 2
        }
        ,
        {
        	header : '七月',
            dataIndex : 'July',
            sortable : true,
            flex : 2
        }
        ,
        {
        	header : '八月',
            dataIndex : 'August',
            sortable : true,
            flex : 2
        }
        ,
        {
        	header : '九月',
            dataIndex : 'September',
            sortable : true,
            flex : 2
        }
        ,
        {
        	header : '十月',
            dataIndex : 'October',
            sortable : true,
            flex : 2
        } 
        ,
        {
        	header : '十一月',
            dataIndex : 'November',
            sortable : true,
            flex : 2
        } 
        ,
        {
        	header : '十二月',
            dataIndex : 'December',
            sortable : true,
            flex : 2
        } 
        ,
        {
        	header : '年累计',
            dataIndex : 'nlj',
            sortable : true,
            flex : 2
        } 
    ]
});
