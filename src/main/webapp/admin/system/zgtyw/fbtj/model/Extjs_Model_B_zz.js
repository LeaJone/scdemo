﻿/**
 *b_zz Model
 *@author CodeSystem
 *文件名     Extjs_Model_B_zz
 *Model名    model_b_zz
 */

Ext.define('model_b_zz', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'rownumberer',
            type : 'string'
        }
        ,
        {
            name : 'BranchName',
            type : 'string'
        }
        ,
        {
            name : 'BranchID',
            type : 'string'
        }
        ,
        {
            name : 'name',
            type : 'string'
        }
        ,
        {
            name : 'January',
            type : 'string'
        }
        ,
        {
            name : 'February',
            type : 'string'
        }
        ,
        {
            name : 'March',
            type : 'string'
        }
        ,
        {
            name : 'April',
            type : 'string'
        }
        ,
        {
            name : 'May',
            type : 'string'
        }
        ,
        {
            name : 'June',
            type : 'string'
        }
        ,
        {
            name : 'July',
            type : 'string'
        }
        ,
        {
            name : 'August',
            type : 'string'
        }
        ,
        {
            name : 'September',
            type : 'string'
        }
        ,
        {
            name : 'October',
            type : 'string'
        }
        ,
        {
            name : 'November',
            type : 'string'
        }
        ,
        {
            name : 'December',
            type : 'string'
        }
        ,
        {
            name : 'nlj',
            type : 'string'
        }
        
    ]
});
