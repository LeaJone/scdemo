﻿/**
 * 建设用地备案编辑界面
 * @author lumingbao
 */
Ext.define('system.zgtyw.ckqdyba.biz.CkqdybaEdit', {
	extend : 'Ext.window.Window',
	requires : ['system.widget.FsdTreeComboBox'],
	grid : null,
	oper : Ext.create('system.zgtyw.ckqdyba.biz.CkqdybaOper'),
	initComponent:function(){
	    var me = this;	    
	   
	    var gwwh = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '公告文号',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 385,
	    	name: 'gwwh',
	    	maxLength: 25,
	    	allowBlank: false
	    });
	    
	    var ksmc = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '矿山名称',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 385,
	    	name: 'ksmc',
	    	maxLength: 100,
	    	allowBlank: false
	    });
	    
	    var xkzh = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '许可证号',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 385,
	    	name: 'xkzh',
	    	maxLength: 50,
	    	allowBlank: false
	    });
	    
	    var yxqx = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '有效期限',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 385,
	    	name: 'yxqx',
	    	maxLength: 25,
	    	allowBlank: false
	    });
	    
	    var dyr = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '抵押人',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 385,
	    	name: 'dyr',
	    	maxLength: 50,
	    	allowBlank: false
	    });
	    
	    var dyqr = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '抵押权人',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 385,
	    	name: 'dyqr',
	    	maxLength: 50,
	    	allowBlank: false
	    });
	    
	    var dyqx = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '抵押期限',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 385,
	    	name: 'dyqx',
	    	maxLength: 25,
	    	allowBlank: false
	    });
	     
	    var baqx = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备案期限',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 385,
	    	name: 'baqx',
	    	allowBlank: false,
	    	maxLength: 25
	    });
	    
	    var ggrq = Ext.create('system.widget.FsdDateField',{
	    	fieldLabel: '公告日期',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 385,
	    	name: 'ggrq',
	    	allowBlank: false
	    });
	    
	    var jczt = Ext.create('system.widget.FsdComboBox',{
	    	fieldLabel:'解除状态',
	        labelAlign:'right',
	    	labelWidth:60,
	        width : 385,
    	    name : 'jczt',//提交到后台的参数名
    	    allowBlank: false,
            store : [
                    ['已解除', '已解除'],
                    ['未解除', '未解除']
                ]
	    });  
	    
	    var remark = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 385,
	    	name: 'remark',
	    	maxLength: 50
	    });
	    
	    var formpanel = new Ext.form.Panel({
	    	defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [{
	                    name: "id",
	                    xtype: "hidden"
	                } , 
	                gwwh, ksmc, xkzh, yxqx, dyr, dyqr, dyqx, baqx, ggrq, jczt, remark]
	    });
	    //将后面的参数属性全部拷贝给第一个的参数，下面的例子是将一个form的表单及其属性 赋给this(即window对象)
	    Ext.apply(this,{
	        width:450,
	        height:390,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[formpanel],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    zIsSpace : true,
			    handler : function() {
    				me.oper.formSubmit(formpanel , me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});