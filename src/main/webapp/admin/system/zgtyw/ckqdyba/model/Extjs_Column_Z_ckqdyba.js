﻿/**
 *Z_CKQDYBA Column
 *@author CodeSystem
 *文件名     Extjs_Column_Z_ckqdyba
 *Columns名  column_z_ckqdyba
 *xtype: 'textfield', 'numbercolumn', 'booleancolumn', 'datecolumn'   format : '0.00'
 *align: 'left', 'center', 'right'
 */

Ext.define('column_z_ckqdyba', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            align : 'center',
            width : 30
        }
        ,
        {
            header : '公告文号',
            dataIndex : 'gwwh',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '矿山名称',
            dataIndex : 'ksmc',
            sortable : true,
            align : 'left',
            flex : 3
        }
        ,
        {
            header : '采矿许可证号',
            dataIndex : 'xkzh',
            sortable : true,
            align : 'left',
            flex : 2
        }
        ,
        {
            header : '抵押人',
            dataIndex : 'dyr',
            sortable : true,
            align : 'left',
            flex : 4
        }
        ,
        {
            header : '公告日期',
            dataIndex : 'ggrq',
            sortable : true,
            align : 'left',
            flex : 2,
			renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value);
			}
        }
        ,
        {
            header : '解除状态',
            dataIndex : 'jczt',
            sortable : true,
            align : 'left',
            flex : 1
        }
    ]
});
