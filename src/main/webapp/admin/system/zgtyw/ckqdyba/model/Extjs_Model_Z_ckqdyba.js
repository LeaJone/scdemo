﻿/**
 *@author CodeSystem
 *文件名     Extjs_Model_Z_ckqdyba
 *Model名    model_z_ckqdyba
 */

Ext.define('model_z_ckqdyba', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'gwwh',
            type : 'string'
        }
        ,
        {
            name : 'ksmc',
            type : 'string'
        }
        ,
        {
            name : 'xkzh',
            type : 'string'
        }
        ,
        {
            name : 'yxqx',
            type : 'string'
        }
        ,
        {
            name : 'dyr',
            type : 'string'
        }
        ,
        {
            name : 'dyqr',
            type : 'string'
        }
        ,
        {
            name : 'dyqx',
            type : 'string'
        }
        ,
        {
            name : 'baqx',
            type : 'string'
        }
        ,
        {
            name : 'ggrq',
            type : 'string'
        }
        ,
        {
            name : 'jczt',
            type : 'string'
        }
    ]
});
