﻿/**
 *Z_YSQGK Model
 *@author CodeSystem
 *文件名     Extjs_Model_Z_ysqgkjs
 *Model名    model_z_ysqgkjs
 */

Ext.define('model_z_ysqgk', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'xm',
            type : 'string'
        }
        ,
        {
            name : 'sqsj',
            type : 'string'
        }
        ,
        {
            name : 'sxxxnrms',
            type : 'string'
        }
        ,
        {
            name : 'qureycode',
            type : 'string'
        }
        ,
        {
            name : 'auditing',
            type : 'string'
        }
        ,
        {
            name : 'adddate',
            type : 'string'
        }
        ,
        {
            name : 'addemployeename',
            type : 'string'
        }
        ,
        {
            name : 'flowstatus',
            type : 'string'
        }
        ,
        {
            name : 'flowstatusname',
            type : 'string'
        }
    ]
});
