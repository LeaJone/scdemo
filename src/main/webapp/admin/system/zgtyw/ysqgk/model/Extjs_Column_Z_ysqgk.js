﻿/**
 *Z_YSQGK Column
 *@author CodeSystem
 *文件名     Extjs_Column_Z_ysqgkjs
 *Columns名  column_z_ysqgkjs
 *xtype: 'textfield', 'numbercolumn', 'booleancolumn', 'datecolumn'   format : '0.00'
 *align: 'left', 'center', 'right'
 */

Ext.define('column_z_ysqgk', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            align : 'center',
            width : 30
        }
        ,
        {
            header : '编号',
            dataIndex : 'id',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '查询码',
            dataIndex : 'qureycode',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '姓名',
            dataIndex : 'xm',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '所需信息内容描述',
            dataIndex : 'sxxxnrms',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '申请时间',
            dataIndex : 'sqsj',
            sortable : true,
            align : 'left',
            flex : 1,
			renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value);
			}
        }
        ,
        {
            header : '审核',
            dataIndex : 'auditing',
            sortable : true,
            align : 'left',
            flex : 1,
            renderer :function(value, metadata, record, rowIndex, columnIndex, store){
            	if(value == 'true'){
            		return "<font color='green'>已审核</font>";
            	}else{
            		return "<font color='red'>未审核</font>";
            	}
  			}
        }
        ,
        {
            header : '回复人',
            dataIndex : 'addemployeename',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '回复时间',
            dataIndex : 'adddate',
            sortable : true,
            align : 'left',
            flex : 1,
			renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value);
			}
        }
        ,
        {
            header : '流程',
            dataIndex : 'flowstatusname',
            sortable : true,
            flex : 1
        }
//        ,
//        {
//        	text: "回复",
//        	width : 80,
//        	align : 'center',
//	        renderer :function(value, metadata, record, rowIndex, columnIndex, store){
//	        	return "<a style='text-decoration:none;' href='#' onClick=\"Ysqgkhf('"+store.getAt(rowIndex).get('id')+"');\">回复</a>";
//  			}
//        }
    ]
});
