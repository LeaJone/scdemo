﻿/**
 * 依申请公开回复界面
 * @author lumingbao
 */
Ext.define('system.zgtyw.ysqgk.biz.YsqgkHf', {
	extend : 'system.widget.FsdWindowFlow',
	requires : ['system.widget.FsdTreeComboBox'],
	grid : null,
	oper : Ext.create('system.zgtyw.ysqgk.biz.YsqgkOper'),
	initComponent:function(){
	    var me = this;
	    
	    var id = Ext.create('Ext.form.field.Hidden',{
	    	name: 'id'
	    });
	    
	    var xm = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '姓名',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'xm'
	    });
	    
	    var gzdw = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '工作单位',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'gzdw'
	    });
	    
	    var zjmc = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '证件名称',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'zjmc'
	    });
	    
	    var zjhm = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '证件号码',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'zjhm'
	    });
	    
	    var lxdh = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '联系电话',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'lxdh'
	    });
	    
	    var yzbm = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '邮政编码',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'yzbm'
	    });
	    
	    var dzyx1 = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '电子邮箱',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'dzyx1'
	    });
	    
	    var txdz = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '通信地址',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 890,
	    	name: 'txdz',
	    	colspan : 2
	    });
	    
	    var mc = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '名称',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'mc'
	    });
	    
	    var zzjgdm = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '组织机构代码',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'zzjgdm'
	    });
	    
	    var zzjgdm = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '组织机构代码',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'zzjgdm'
	    });
	    
	    var yyzzxx = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '营业执照信息',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 890,
	    	name: 'yyzzxx',
	    	colspan : 2
	    });
	    
	    var frdb = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '法人代表',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'frdb'
	    });
	    
	    var lxrxm = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '联系人姓名',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'lxrxm'
	    });
	    
	    var lxrdh = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '联系人电话',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'lxrdh'
	    });
	    
	    var cz = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '传真',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'cz'
	    });
	    
	    var lxdz = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '联系地址',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 890,
	    	name: 'lxdz',
	    	colspan : 2
	    });
	    
	    var dzyx2 = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '电子邮箱',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 890,
	    	name: 'dzyx2',
	    	colspan : 2
	    });
	    
	    var sqrqm = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '申请人签名',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 890,
	    	name: 'sqrqm',
	    	colspan : 2
	    });
	    
	    var sxxxnrms = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '所需信息内容描述',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 890,
	    	name: 'sxxxnrms',
	    	colspan : 2
	    });
	    
	    var sxxxyt = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '所需信息的用途',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 890,
	    	name: 'sxxxyt',
	    	colspan : 2
	    });
	  
	    var sxxxsqh = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '所需信息的索取号',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'sxxxsqh'
	    });
	    
	    var qtts = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '其他提示',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'qtts'
	    });
	    
	    var sfsqjmfy = new Ext.form.RadioGroup({
	    	fieldLabel: '是否申请减免费用',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	items: [{
	    		name: 'sfsqjmfy',
	    		inputValue: '0',
	    		boxLabel: '申请，请提供相关证明。',
	    		checked: true
	    	}, {
	    		name: 'sfsqjmfy',
	    		inputValue: '1',
	    		boxLabel: '不'
	    	}]
	    });
	    
	    var sxxxzdgyfs = new Ext.form.CheckboxGroup({
	    	fieldLabel: '所需信息提供方式',
	    	labelAlign:'right',
	    	labelWidth:110,
	    	width : 445,
	    	name : 'xxtgfs',
	    	items: [{
	    		name: 'sxxxzdgyfs',
	    		boxLabel: '纸面',
	    		inputValue: '纸面'
	    	}, {
	    		name: 'sxxxzdgyfs',
	    		boxLabel: '电子邮件',
	    		inputValue: '电子邮件'
	    	}, {
	    		name: 'sxxxzdgyfs',
	    		boxLabel: '光盘',
	    		inputValue: '光盘'
	    	}, {
	    		name: 'sxxxzdgyfs',
	    		boxLabel: '磁盘',
	    		inputValue: '磁盘'
	    	}]
	    });
	    
	    var hqxxfs = new Ext.form.CheckboxGroup({
	    	fieldLabel: '获取信息的方式',
	    	labelAlign:'right',
	    	labelWidth:110,
	    	width : 890,
	    	colspan : 2,
	    	name : 'xxhqfs',
	    	items: [{
	    		name: 'hqxxfs',
	    		boxLabel: '邮寄',
	    		inputValue: '邮寄'
	    	}, {
	    		name: 'hqxxfs',
	    		boxLabel: '快递',
	    		inputValue: '快递'
	    	}, {
	    		name: 'hqxxfs',
	    		boxLabel: '电子邮件',
	    		inputValue: '电子邮件'
	    	}, {
	    		name: 'hqxxfs',
	    		boxLabel: '传真',
	    		inputValue: '传真'
	    	}, {
	    		name: 'hqxxfs',
	    		boxLabel: '自行领取/当场阅读/抄录',
	    		inputValue: '自行领取/当场阅读/抄录'
	    	}]
	    });
	    
	    var qtfs = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '信息其他提供方式',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 890,
	    	name: 'qtfs',
	    	maxLength: 50,
	    	colspan : 2
	    });
	    
	    var atitle = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '回复标题',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 890,
	    	name: 'atitle',
	    	maxLength: 50,
	    	allowBlank: me.isShowFlowData,
	    	colspan : 2
	    });
	    
	    var acontent = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '回复内容',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 890,
	    	name: 'acontent',
	    	maxLength: 1000,
	    	allowBlank: me.isShowFlowData,
	    	colspan : 2
	    });
	    
	    var addemployeename = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '回复人',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'addemployeename'
	    });
	    
	    var adddate = Ext.create('system.widget.FsdDateTimeLabel',{
	    	fieldLabel: '回复时间',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'adddate'
	    });
	    
	    me.formpanel = new Ext.form.Panel({
			layout: 'absolute',
			border : 0,
			autoScroll:true,
			items: [{
					xtype: 'fieldset',
					x: 4,
					y: 4,
					width: 930,
					height: 130,
					title: '申请人信息-公民',
					layout: {
				        type: 'table', 
				        columns: 2, //每行有几列 
				        //默认样式 
				        tableAttrs: {
				            style: "width:100;height:40;" 
				        }
				    },
					items: [id, xm, gzdw, zjmc, zjhm, lxdh, yzbm, dzyx1, txdz]
				},{
					xtype: 'fieldset',
					x: 4,
					y: 140,
					width: 930,
					height: 215,
					title: '申请人信息-企业',
					layout: {
				        type: 'table', 
				        columns: 2, //每行有几列 
				        //默认样式 
				        tableAttrs: {
				            style: "width:100;height:40;" 
				        }
				    },
					items: [mc, zzjgdm, zzjgdm, yyzzxx, frdb, lxrxm, lxrdh, cz, lxdz, dzyx2, sqrqm]
				},{
					xtype: 'fieldset',
					x: 4,
					y: 361,
					width: 930,
					height: 310,
					title: '所需信息情况',
					layout: {
				        type: 'table', 
				        columns: 2, //每行有几列 
				        //默认样式 
				        tableAttrs: {
				            style: "width:100;height:40;" 
				        }
				    },
					items: [sxxxnrms, sxxxyt, sxxxsqh, qtts, sfsqjmfy, sxxxzdgyfs, hqxxfs, qtfs]
				},
				{
					xtype: 'fieldset',
					x: 4,
					y: 677,
					width: 930,
					height: 160,
					title: '回复',
					layout: {
				        type: 'table',
				        columns: 2, //每行有几列 
				        //默认样式 
				        tableAttrs: {
				            style: "width:100;height:40;" 
				        }
				    },
					items: [atitle, acontent, addemployeename, adddate]
				}
			]
		});
	    

	    var buttonItem = [{
			xtype : "fsdbutton",
			popedomCode : 'ysqgkhf',
        	name : 'btnsave',
		    text : '回复',
		    iconCls : 'acceptIcon',
		    zIsSpace : true,
		    handler : function() {
				me.oper.huifuSubmit(me.formpanel);
		    }
	    }, {
	    	xtype : "fsdbutton",
		    text : '关闭',
		    iconCls : 'deleteIcon',
		    zIsSpace : true,
		    handler : function() {
		       me.close();
            }
	    }];
	        
	  //是否是流程数据
	    var intHeight = 610;
	    if (me.isShowFlowData){
	    	buttonItem = null;
	    	intHeight = intHeight - 55;
	    }
	    
	    Ext.apply(this,{
	        width:970,
	        height:intHeight,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        layout : 'fit',
	        items :[me.formpanel],
	        buttons : buttonItem
	    });
	    this.callParent(arguments);
	},
	
	loadFormData : function(id, url, success, failure){
		var me = this;
		if (url == null){
			url = 'z_ysqgk/load_ysqgkbyid.do';
		}
		var pram = {id : id};
        var param = {jsonData : Ext.encode(pram)};
        me.oper.util.formLoad(me.formpanel.getForm(), url, param, null, success);
	}
});