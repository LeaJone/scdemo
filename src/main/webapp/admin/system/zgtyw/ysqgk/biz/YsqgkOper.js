/**
 * 依申请公开界面操作类
 * @author lumingbao
 */

Ext.define('system.zgtyw.ysqgk.biz.YsqgkOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 添加
     */
	add : function(grid){
	    var win = Ext.create('system.zgtyw.ysqgk.biz.YsqgkEdit');
        win.setTitle('添加');
        win.grid = grid;
        win.modal = true;
        win.show();
	},
	
	/**
     * 表单提交
     */
     formSubmit : function(formpanel , grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_ysqgk/save_ysqgk_qx_ysqgkbj.do' , formpanel.form , '正在提交数据,请稍候.....',
			function(form, action){
			    formpanel.up('window').close();
			    grid.getStore().reload();
				Ext.MessageBox.alert('提示', '保存成功！');
			});
        }
     },
     
     /**
      * 提交回复
      */
      huifuSubmit : function(formpanel){
         var me = this;
         if (formpanel.form.isValid()) {
             me.util.ExtFormSubmit('z_ysqgk/huifu_ysqgk_qx_ysqgkhf.do' , formpanel.form , '正在提交数据,请稍候.....',
 			function(form, action){
 			    formpanel.up('window').close();
 				Ext.MessageBox.alert('提示', '保存成功！');
 			});
         }
      },
     
     /**
      * 删除
      */
     del : function(grid){
    	 var me = this;
 		 // 获取选中的行
 	     var data = grid.getSelectionModel().getSelection();
         if (data.length == 0) {
             Ext.MessageBox.alert('提示', '请先选中要删除的项目！');
         }else {
        	 Ext.MessageBox.show({
        		 title : '询问',
        		 msg : '您确定要删除吗?',
 			     width : 250,
 				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                     if (btn == 'yes') {
                    	 var dir = new Array();　
                         Ext.Array.each(data, function(items) {
                             var id = items.data.id;
                             dir.push(id);
             			});
             			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('z_ysqgk/del_ysqgk_qx_ysqgksc.do' , param ,
                        function(response, options){
            				Ext.MessageBox.alert('提示', '删除成功！');
                        	grid.getStore().reload();
                        }, null, me.form);
                     }
                 }
             });
         }
 	},
 	
 	/**
     * 修改
     */
	editor : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.zgtyw.ysqgk.biz.YsqgkEdit');
            win.setTitle('修改');
            win.modal = true;
            win.grid = grid;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'z_ysqgk/load_ysqgkbyid.do' , param , null , 
	        function(response, options, respText){
	        	if(respText.data.sxxxzdgyfs != null && respText.data.sxxxzdgyfs != ""){
	        		var fxk1 = win.down('form').getForm().findField('xxtgfs');
	        		var fxk1store = respText.data.sxxxzdgyfs.split(",");
	        		for(var i=0;i<fxk1.items.length;i++){
		        		for(var j=0;j<fxk1store.length;j++){
		        			 if(fxk1.items.get(i).inputValue == fxk1store[j]){
		        				 fxk1.items.get(i).setValue(true);
	                         }
		        		}
		        	}
	        	}
	        	
	        	if(respText.data.hqxxfs != null && respText.data.hqxxfs != ""){
	        		var fxk2 = win.down('form').getForm().findField('xxhqfs');
	        		var fxk2store = respText.data.hqxxfs.split(",");
	        		for(var i=0;i<fxk2.items.length;i++){
		        		for(var j=0;j<fxk2store.length;j++){
		        			if(fxk2.items.get(i).inputValue == fxk2store[j]){
		        				 fxk2.items.get(i).setValue(true);
	                        }
		        		}
		        	}
	        	}
	            win.show();
	        });
        }
	},
 	
 	/**
     * 查看
     */
	view : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
	    if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要查看的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.zgtyw.ysqgk.biz.YsqgkHf');
            win.setTitle('查看');
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'z_ysqgk/load_ysqgkbyid.do' , param , null , 
	        function(response, options, respText){
	        	if(respText.data.sxxxzdgyfs != null && respText.data.sxxxzdgyfs != ""){
	        		var fxk1 = win.down('form').getForm().findField('xxtgfs');
	        		var fxk1store = respText.data.sxxxzdgyfs.split(",");
	        		for(var i=0;i<fxk1.items.length;i++){
		        		for(var j=0;j<fxk1store.length;j++){
		        			 if(fxk1.items.get(i).inputValue == fxk1store[j]){
		        				 fxk1.items.get(i).setValue(true);
	                         }
		        		}
		        	}
	        	}
	        	
	        	if(respText.data.hqxxfs != null && respText.data.hqxxfs != ""){
	        		var fxk2 = win.down('form').getForm().findField('xxhqfs');
	        		var fxk2store = respText.data.hqxxfs.split(",");
	        		for(var i=0;i<fxk2.items.length;i++){
		        		for(var j=0;j<fxk2store.length;j++){
		        			if(fxk2.items.get(i).inputValue == fxk2store[j]){
		        				 fxk2.items.get(i).setValue(true);
	                        }
		        		}
		        	}
	        	}
	            win.show();
	        });
        }
	},
	
	/**
     * 是否审核
     */
	isEnabled : function(grid, isQT){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
		        msg : '您确定要修改审核状态吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
					    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
			            });
             			var pram = {ids : dir, isenabled : isQT};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('z_ysqgk/enabled_ysqgk_qx_ysqgksh.do' , param ,
                        function(response, options){
            				Ext.MessageBox.alert('提示', '审核状态修改成功！');
                        	grid.getStore().reload(); 
                        });
                    }
                }
            });
        }
	},
	
	/**
     * 回复
     */
	Huifu : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
	    if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要回复的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
	        //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
	        var win = Ext.create('system.zgtyw.ysqgk.biz.YsqgkHf');
	        win.setTitle('回复');
	        win.modal = true;
	        var pram = {id : id};
	        var param = {jsonData : Ext.encode(pram)};
		    me.util.formLoad(win.down('form').getForm() , 'z_ysqgk/load_ysqgkbyid.do' , param , null , 
		    function(response, options, respText){
		    	if(respText.data.sxxxzdgyfs != null && respText.data.sxxxzdgyfs != ""){
	        		var fxk1 = win.down('form').getForm().findField('xxtgfs');
	        		var fxk1store = respText.data.sxxxzdgyfs.split(",");
	        		for(var i=0;i<fxk1.items.length;i++){
		        		for(var j=0;j<fxk1store.length;j++){
		        			 if(fxk1.items.get(i).inputValue == fxk1store[j]){
		        				 fxk1.items.get(i).setValue(true);
	                         }
		        		}
		        	}
	        	}
	        	
	        	if(respText.data.hqxxfs != null && respText.data.hqxxfs != ""){
	        		var fxk2 = win.down('form').getForm().findField('xxhqfs');
	        		var fxk2store = respText.data.hqxxfs.split(",");
	        		for(var i=0;i<fxk2.items.length;i++){
		        		for(var j=0;j<fxk2store.length;j++){
		        			if(fxk2.items.get(i).inputValue == fxk2store[j]){
		        				 fxk2.items.get(i).setValue(true);
	                        }
		        		}
		        	}
	        	}
		    	win.show();
		    });
        }
	}
});