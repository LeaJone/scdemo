﻿/**
 * 依申请公开管理界面
 * @author lumingbao
 */
Ext.define('system.zgtyw.ysqgk.biz.YsqgkManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*', 
	             'system.zgtyw.ysqgk.model.Extjs_Model_Z_ysqgk',
	             'system.zgtyw.ysqgk.model.Extjs_Column_Z_ysqgk'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
//		window.Ysqgkhf = function(id){
//			me.oper.Huifu(id);
//		};
	},
	
    oper : Ext.create('system.zgtyw.ysqgk.biz.YsqgkOper'),
    
	createTable : function() {
	    var me = this;
	    
	    var queryText = Ext.create("Ext.form.TextField", {
	    	name : 'queryParam',
			emptyText : '请输入查询码',
			width : 130,
			enableKeyEvents : true,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
	    });
		
		var btnAdd = Ext.create("system.widget.FsdButton", {
			text : "添加",
			iconCls : 'page_addIcon',
			handler : function() {
				me.oper.add(grid);
			}
		});
		var btnDel = Ext.create("system.widget.FsdButton", {
			text : "删除",
			iconCls : 'page_deleteIcon',
			handler : function() {
			    me.oper.del(grid);
			}
		});
	    
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_ysqgk', // 显示列
			model : 'model_z_ysqgk',
			baseUrl : 'z_ysqgk/load_pagedatabyemployee.do',
			pageSize : 30,
			border : false,
			tbar : [{
				xtype : "fsdbutton",
				popedomCode : 'ysqgkhf',
				text : "回复",
				iconCls : 'page_editIcon',
				handler : function() {
					me.oper.Huifu(grid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid);
				}
			}, '-', {
				xtype : "fsdbutton",
				popedomCode : 'ysqgksh',
				text : "审核通过",
				iconCls : 'tickIcon',
				handler : function(button) {
				    me.oper.isEnabled(grid, true);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'ysqgksh',
				text : "取消审核",
				iconCls : 'crossIcon',
				handler : function(button) {
				    me.oper.isEnabled(grid, false);
				}
			}, '->', queryText, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var querycode =  queryText.getValue();
			var pram = {querycode : querycode, glqx : 'ysqgkcx'};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});

		return grid;
	}
});