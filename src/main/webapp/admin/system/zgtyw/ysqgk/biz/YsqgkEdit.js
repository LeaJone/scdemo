﻿/**
 * 依申请公开编辑界面
 * @author lumingbao
 */
Ext.define('system.zgtyw.ysqgk.biz.YsqgkEdit', {
	extend : 'Ext.window.Window',
	requires : ['system.widget.FsdTreeComboBox'],
	grid : null,
	oper : Ext.create('system.zgtyw.ysqgk.biz.YsqgkOper'),
	initComponent:function(){
	    var me = this;
	    
	    var id = Ext.create('Ext.form.field.Hidden',{
	    	name: 'id'
	    });
	    
	    var xm = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '姓名',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'xm',
	    	maxLength: 50
	    });
	    
	    var gzdw = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '工作单位',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'gzdw',
	    	maxLength: 50
	    });
	    
	    var zjmc = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '证件名称',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'zjmc',
	    	maxLength: 50
	    });
	    
	    var zjhm = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '证件号码',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'zjhm',
	    	maxLength: 50
	    });
	    
	    var lxdh = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '联系电话',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'lxdh',
	    	maxLength: 50
	    });
	    
	    var yzbm = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '邮政编码',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'yzbm',
	    	maxLength: 50
	    });
	    
	    var dzyx1 = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '电子邮箱',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'dzyx1',
	    	maxLength: 50
	    });
	    
	    var txdz = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '通信地址',
	    	labelAlign:'right',
	    	labelWidth:110,
	    	width : 445,
	    	name: 'txdz',
	    	maxLength: 50
	    });
	    
	    var mc = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '名称',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'mc',
	    	maxLength: 50
	    });
	    
	    var zzjgdm = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '组织机构代码',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'zzjgdm',
	    	maxLength: 50
	    });
	    
	    var zzjgdm = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '组织机构代码',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'zzjgdm',
	    	maxLength: 50
	    });
	    
	    var yyzzxx = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '营业执照信息',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 890,
	    	name: 'yyzzxx',
	    	maxLength: 50,
	    	colspan : 2
	    });
	    
	    var frdb = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '法人代表',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'frdb',
	    	maxLength: 50
	    });
	    
	    var lxrxm = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '联系人姓名',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'lxrxm',
	    	maxLength: 50
	    });
	    
	    var lxrdh = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '联系人电话',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'lxrdh',
	    	maxLength: 50
	    });
	    
	    var cz = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '传真',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'cz',
	    	maxLength: 50
	    });
	    
	    var lxdz = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '联系地址',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 890,
	    	name: 'lxdz',
	    	maxLength: 50,
	    	colspan : 2
	    });
	    
	    var dzyx2 = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '电子邮箱',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 890,
	    	name: 'dzyx2',
	    	maxLength: 50,
	    	colspan : 2
	    });
	    
	    var sqrqm = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '申请人签名',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 890,
	    	name: 'sqrqm',
	    	maxLength: 50,
	    	colspan : 2
	    });
	    
	    var sxxxnrms = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '所需信息内容描述',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'sxxxnrms',
	    	maxLength: 50
	    });
	    
	    var sxxxyt = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '所需信息的用途',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'sxxxyt',
	    	maxLength: 50
	    });
	    
	    var sxxxsqh = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '所需信息的索取号',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'sxxxsqh',
	    	maxLength: 50
	    });
	    
	    var qtts = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '其他提示',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'qtts',
	    	maxLength: 50
	    });
	    
	    var sfsqjmfy = new Ext.form.RadioGroup({
	    	fieldLabel: '是否申请减免费用',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	items: [{
	    		name: 'sfsqjmfy',
	    		inputValue: '申请，请提供相关证明',
	    		boxLabel: '申请，请提供相关证明。',
	    		checked: true
	    	}, {
	    		name: 'sfsqjmfy',
	    		inputValue: '不',
	    		boxLabel: '不'
	    	}]
	    });
	    
	    var sxxxzdgyfs = new Ext.form.CheckboxGroup({
	    	fieldLabel: '所需信息提供方式',
	    	labelAlign:'right',
	    	labelWidth:110,
	    	width : 445,
	    	name : 'xxtgfs',
	    	items: [{
	    		name: 'sxxxzdgyfs',
	    		boxLabel: '纸面',
	    		inputValue: '纸面'
	    	}, {
	    		name: 'sxxxzdgyfs',
	    		boxLabel: '电子邮件',
	    		inputValue: '电子邮件'
	    	}, {
	    		name: 'sxxxzdgyfs',
	    		boxLabel: '光盘',
	    		inputValue: '光盘'
	    	}, {
	    		name: 'sxxxzdgyfs',
	    		boxLabel: '磁盘',
	    		inputValue: '磁盘'
	    	}]
	    });
	    
	    var hqxxfs = new Ext.form.CheckboxGroup({
	    	fieldLabel: '获取信息的方式',
	    	labelAlign:'right',
	    	labelWidth:110,
	    	width : 890,
	    	colspan : 2,
	    	name : 'xxhqfs',
	    	items: [{
	    		name: 'hqxxfs',
	    		boxLabel: '邮寄',
	    		inputValue: '邮寄'
	    	}, {
	    		name: 'hqxxfs',
	    		boxLabel: '快递',
	    		inputValue: '快递'
	    	}, {
	    		name: 'hqxxfs',
	    		boxLabel: '电子邮件',
	    		inputValue: '电子邮件'
	    	}, {
	    		name: 'hqxxfs',
	    		boxLabel: '传真',
	    		inputValue: '传真'
	    	}, {
	    		name: 'hqxxfs',
	    		boxLabel: '自行领取/当场阅读/抄录',
	    		inputValue: '自行领取/当场阅读/抄录'
	    	}]
	    });
	    
	    var qtfs = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '信息其他提供方式',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 890,
	    	name: 'qtfs',
	    	maxLength: 50,
	    	colspan : 2
	    });
	    
	    var atitle = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '回复标题',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 890,
	    	name: 'atitle',
	    	maxLength: 50,
	    	colspan : 2
	    });
	    
	    var acontent = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '回复内容',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 890,
	    	name: 'acontent',
	    	maxLength: 50,
	    	colspan : 2
	    });
	    
	    var addemployeename = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '回复人',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'addemployeename',
	    	maxLength: 50,
	    	readOnly: true
	    });
	    
	    var adddate = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '回复时间',
	    	labelAlign:'right',
	    	labelWidth:110,
	        width : 445,
	    	name: 'adddate',
	    	maxLength: 50,
	    	readOnly: true
	    });
	    
	    var formpanel = new Ext.form.Panel({
			layout: 'absolute',
			border : 0,
			autoScroll:true,
			items: [{
					xtype: 'fieldset',
					x: 4,
					y: 4,
					width: 930,
					height: 130,
					title: '申请人信息-公民',
					layout: {
				        type: 'table', 
				        columns: 2, //每行有几列 
				        //默认样式 
				        tableAttrs: {
				            style: "width:100;height:40;" 
				        }
				    },
					items: [id, xm, gzdw, zjmc, zjhm, lxdh, yzbm, dzyx1, txdz]
				},{
					xtype: 'fieldset',
					x: 4,
					y: 140,
					width: 930,
					height: 215,
					title: '申请人信息-企业',
					layout: {
				        type: 'table', 
				        columns: 2, //每行有几列 
				        //默认样式 
				        tableAttrs: {
				            style: "width:100;height:40;" 
				        }
				    },
					items: [mc, zzjgdm, zzjgdm, yyzzxx, frdb, lxrxm, lxrdh, cz, lxdz, dzyx2, sqrqm]
				},{
					xtype: 'fieldset',
					x: 4,
					y: 361,
					width: 930,
					height: 160,
					title: '所需信息情况',
					layout: {
				        type: 'table', 
				        columns: 2, //每行有几列 
				        //默认样式 
				        tableAttrs: {
				            style: "width:100;height:40;" 
				        }
				    },
					items: [sxxxnrms, sxxxyt, sxxxsqh, qtts, sfsqjmfy, sxxxzdgyfs, hqxxfs, qtfs]
				},
				{
					xtype: 'fieldset',
					x: 4,
					y: 530,
					width: 930,
					height: 160,
					title: '回复',
					layout: {
				        type: 'table',
				        columns: 2, //每行有几列 
				        //默认样式 
				        tableAttrs: {
				            style: "width:100;height:40;" 
				        }
				    },
					items: [atitle, acontent, addemployeename, adddate]
				}
			]
		});
	        
	    Ext.apply(this,{
	        width:970,
	        height:610,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        layout : 'fit',
	        items :[formpanel],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    zIsSpace : true,
			    handler : function() {
    				me.oper.formSubmit(formpanel , me.grid);
			    }
		    }, {
		    	xtype : "fsdbutton",
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});