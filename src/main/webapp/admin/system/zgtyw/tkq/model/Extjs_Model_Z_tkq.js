﻿/**
 *Z_tkq Model
 *@author CodeSystem
 *文件名     Extjs_Model_Z_tkq
 *Model名    model_z_tkq
 */

Ext.define('model_z_tkq', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'sqr',
            type : 'string'
        }
        ,
        {
            name : 'xmmc',
            type : 'string'
        }
        ,
        {
            name : 'kcdw',
            type : 'string'
        }
    ]
});
