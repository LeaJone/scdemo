﻿/**
 *Z_tkq Column
 *@author CodeSystem
 *文件名     Extjs_Column_Z_tkq
 *Columns名  column_z_tkq
 *xtype: 'textfield', 'numbercolumn', 'booleancolumn', 'datecolumn'   format : '0.00'
 *align: 'left', 'center', 'right'
 */

Ext.define('column_z_tkq', {
    columns: [
		{
		    xtype : 'rownumberer',
			text : 'NO',
			sortable : true,
			align : 'center',
		    width : 30
		}
		,
        {
            header : '申请人',
            dataIndex : 'sqr',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '项目名称',
            dataIndex : 'xmmc',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '勘察单位',
            dataIndex : 'kcdw',
            sortable : true,
            align : 'left',
            flex : 1
        }
    ]
});
