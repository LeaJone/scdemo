﻿/**
 * 采矿权编辑界面
 * @author lumingbao
 */
Ext.define('system.zgtyw.tkq.biz.TkqEdit', {
	extend : 'Ext.window.Window',
	requires : ['system.widget.FsdTreeComboBox'],
	grid : null,
	oper : Ext.create('system.zgtyw.tkq.biz.TkqOper'),
	initComponent:function(){
	    var me = this;	    
	   
	    var xkzh = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '许可证号',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'xkzh',
	    	maxLength: 50,
	    	allowBlank: false
	    });
	    
	    var xmmc = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '项目名称',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'xmmc',
	    	maxLength: 20,
	    	allowBlank: false
	    });
	    
	    var sqr = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '申请人',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'sqr',
	    	maxLength: 50,
	    	allowBlank: false
	    });
	    
	    var kcdw = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '勘察单位',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'kcdw',
	    	maxLength: 20,
	    	allowBlank: false
	    });
	    
	    var kckzbm = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '勘察矿种',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'kckzbm',
	    	maxLength: 50,
	    	allowBlank: false
	    });

	    var yxqq = Ext.create('system.widget.FsdDateField',{
	    	fieldLabel: '有效期起',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'yxqq',
	    	allowBlank: false
	    });
	    
	    var yxqz = Ext.create('system.widget.FsdDateField',{
	    	fieldLabel: '有效期止',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'yxqz',
	    	allowBlank: false
	    });
	    
	    var zmj = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '总面积',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'zmj',
	    	maxLength: 20,
	    	allowBlank: false
	    });
	    
	    var djq = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '东经起',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'djq',
	    	maxLength: 20,
	    	allowBlank: false
	    });
	    
	    var djz = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '东经止',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'djz',
	    	maxLength: 20,
	    	allowBlank: false
	    });
	    
	    var bwq = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '北纬起',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'bwq',
	    	maxLength: 20,
	    	allowBlank: false
	    });
	    
	    var bwz = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '北纬止',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'bwz',
	    	maxLength: 20,
	    	allowBlank: false
	    });
	     
	    var beizhu = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'remark',
	    	maxLength: 50,
	    	maxLengthText: '备注不能超过50个字符'
	    });
	    
	    var formpanel = new Ext.form.Panel({
	    	defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '10 0 0 10',
			items : [{
	                    name: "id",
	                    xtype: "hidden"
	                } , 
	                xkzh,xmmc, sqr, kcdw, kckzbm, yxqq, yxqz, zmj, djq,djz,bwq,bwz]
	    });
	    Ext.apply(this,{
	        width:400,
	        height:400,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[formpanel],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    zIsSpace : true,
			    handler : function() {
    				me.oper.formSubmit(formpanel , me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});