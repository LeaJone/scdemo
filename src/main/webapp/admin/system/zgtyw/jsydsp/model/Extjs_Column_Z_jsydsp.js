﻿/**
 *Z_JSYDSP Column
 *@author CodeSystem
 *文件名     Extjs_Column_Z_jsydsp
 *Columns名  column_z_jsydsp
 *xtype: 'textfield', 'numbercolumn', 'booleancolumn', 'datecolumn'   format : '0.00'
 *align: 'left', 'center', 'right'
 */

Ext.define('column_z_jsydsp', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            align : 'center',
            width : 30
        }
        ,
        {
            header : '项目名称',
            dataIndex : 'xmmc',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '项目位置',
            dataIndex : 'xmwz',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '批准时间',
            dataIndex : 'pzsj',
            sortable : true,
            align : 'left',
            flex : 1,
			renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value);
			}
        }
        ,
        {
            header : '批准面积',
            dataIndex : 'pzmj',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '批准文号',
            dataIndex : 'pzwh',
            sortable : true,
            align : 'left',
            flex : 1
        }
    ]
});
