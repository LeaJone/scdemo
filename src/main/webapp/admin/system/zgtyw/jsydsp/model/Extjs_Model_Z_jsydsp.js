﻿/**
 *Z_JSYDSP Model
 *@author CodeSystem
 *文件名     Extjs_Model_Z_jsydsp
 *Model名    model_z_jsydsp
 */

Ext.define('model_z_jsydsp', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'pzwh',
            type : 'string'
        }
        ,
        {
            name : 'xmmc',
            type : 'string'
        }
        ,
        {
            name : 'xmwz',
            type : 'string'
        }
        ,
        {
            name : 'pzsj',
            type : 'string'
        }
        ,
        {
            name : 'pzmj',
            type : 'string'
        }
    ]
});
