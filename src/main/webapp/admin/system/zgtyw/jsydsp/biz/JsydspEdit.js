﻿/**
 * 建设用地审批编辑界面
 * @author lumingbao
 */
Ext.define('system.zgtyw.jsydsp.biz.JsydspEdit', {
	extend : 'Ext.window.Window',
	requires : ['system.widget.FsdTreeComboBox'],
	grid : null,
	oper : Ext.create('system.zgtyw.jsydsp.biz.JsydspOper'),
	initComponent:function(){
	    var me = this;
	    
	    var id = Ext.create('Ext.form.field.Hidden',{
	    	name: 'id'
	    });
	    
	    var xmmc = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '项目名称',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 550,
	    	name: 'xmmc',
	    	maxLength: 50,
	    	allowBlank: false,
	    	blankText: '请输入项目名称',
	    	maxLengthText: '项目名称不能超过50个字符',
	    	colspan : 2
	    });
	    
	    var pzwh = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '批准文号',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 275,
	    	name: 'pzwh',
	    	maxLength: 50,
	    	allowBlank: false,
	    	blankText: '请输入批准文号',
	    	maxLengthText: '批准文号不能超过50个字符'
	    });
	    
	    var xmwz = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '项目位置',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 275,
	    	name: 'xmwz',
	    	maxLength: 50,
	    	allowBlank: false,
	    	blankText: '请输入项目位置',
	    	maxLengthText: '项目位置不能超过50个字符'
	    });
	   
	    var pzsj = Ext.create('system.widget.FsdDateField',{
	    	fieldLabel: '批准时间',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 275,
	    	name: 'pzsj',
	    	allowBlank: false
	    });
	    
	    var sxh = Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '顺序号',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 275,
	    	name: 'sort',
	    	maxLength: 3,
	    	minValue: 0,
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank: false,
	    	blankText: '请输入顺序号',
	    	maxLengthText: '顺序号不能超过3个字符'
	    });
	    
	    var wlyd = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '未利用地',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 275,
	    	name: 'wlyd',
	    	maxLength: 50,
	    	allowBlank: false,
	    	blankText: '请输入未利用地',
	    	maxLengthText: '未利用地不能超过50个字符'
	    });
	    
	    var jsyd = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '建设用地',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 275,
	    	name: 'jsyd',
	    	maxLength: 50,
	    	allowBlank: false,
	    	blankText: '请输入建设用地',
	    	maxLengthText: '建设用地不能超过50个字符'
	    });

	    var pzmj = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '批准总面积',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 550,
	    	name: 'pzmj',
	    	maxLength: 50,
	    	allowBlank: false,
	    	blankText: '请输入批准总面积',
	    	maxLengthText: '批准总面积不能超过50个字符',
	    	colspan : 2
	    });
	    
	    var yd = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '园地',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 275,
	    	name: 'yd',
	    	maxLength: 50,
	    	allowBlank: false,
	    	blankText: '请输入园地',
	    	maxLengthText: '园地不能超过50个字符'
	    });
	    
	    var gd = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '耕地',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 275,
	    	name: 'gd',
	    	maxLength: 50,
	    	allowBlank: false,
	    	blankText: '请输入耕地',
	    	maxLengthText: '耕地不能超过50个字符'
	    });
	    
	    var xj = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '小计',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 550,
	    	name: 'xj',
	    	maxLength: 50,
	    	allowBlank: false,
	    	blankText: '请输入小计',
	    	maxLengthText: '小计不能超过50个字符',
	    	colspan : 2
	    });
	    
	    var zzyd = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '住宅用地',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 275,
	    	name: 'zzyd',
	    	maxLength: 50,
	    	allowBlank: false,
	    	blankText: '请输入住宅用地',
	    	maxLengthText: '住宅用地不能超过50个字符'
	    });
	    
	    var sfyd = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '商服用地',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 275,
	    	name: 'sfyd',
	    	maxLength: 50,
	    	allowBlank: false,
	    	blankText: '请输入商服用地',
	    	maxLengthText: '商服用地不能超过50个字符'
	    });
	    
	    var gkccyd = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '工矿仓储用地',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 275,
	    	name: 'gkccyd',
	    	maxLength: 50,
	    	allowBlank: false,
	    	blankText: '请输入工矿仓储用地',
	    	maxLengthText: '工矿仓储用地不能超过50个字符'
	    });
	    
	    var ggglfwyd = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '公共管理与公共服务用地',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 275,
	    	name: 'ggglfwyd',
	    	maxLength: 50,
	    	allowBlank: false,
	    	blankText: '请输入公共管理与公共服务用地',
	    	maxLengthText: '公共管理与公共服务用地不能超过50个字符'
	    });
	    
	    var jtysyd = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '交通运输用地',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 275,
	    	name: 'jtysyd',
	    	maxLength: 50,
	    	allowBlank: false,
	    	blankText: '请输入交通运输用地',
	    	maxLengthText: '交通运输用地不能超过50个字符'
	    });
	    
	    var slss = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '水利设施',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 275,
	    	name: 'slss',
	    	maxLength: 50,
	    	allowBlank: false,
	    	blankText: '请输入水利设施',
	    	maxLengthText: '水利设施不能超过50个字符'
	    });
	    
	    var nyyd = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '能源用地',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 275,
	    	name: 'nyyd',
	    	maxLength: 50,
	    	allowBlank: false,
	    	blankText: '请输入能源用地',
	    	maxLengthText: '能源用地不能超过50个字符'
	    });
	    
	    var qt = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '其他',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 275,
	    	name: 'qt',
	    	maxLength: 50,
	    	allowBlank: false,
	    	blankText: '请输入其他',
	    	maxLengthText: '其他不能超过50个字符'
	    });
	    
	    var formpanel = new Ext.form.Panel({
			layout: 'absolute',
			border : 0,
			items: [
				{
					xtype: 'fieldset',
					x: 4,
					y: 4,
					width: 580,
					height: 155,
					title: '基本',
					layout: {
				        type: 'table', 
				        columns: 2, //每行有几列 
				        //默认样式 
				        tableAttrs: {
				            style: "width:100;height:40;" 
				        }
				    },
					items: [id, xmmc, pzwh, xmwz, pzsj, sxh, wlyd, jsyd, pzmj]
				},
				{
					xtype: 'fieldset',
					x: 4,
					y: 162,
					width: 580,
					height: 80,
					title: '农用地',
					layout: {
				        type: 'table', 
				        columns: 2, //每行有几列 
				        //默认样式 
				        tableAttrs: {
				            style: "width:100;height:40;" 
				        }
				    },
					items: [yd, gd, xj]
				},
				{
					xtype: 'fieldset',
					x: 4,
					y: 245,
					width: 580,
					height: 155,
					title: '用途',
					layout: {
				        type: 'table', 
				        columns: 2, //每行有几列 
				        //默认样式 
				        tableAttrs: {
				            style: "width:100;height:40;" 
				        }
				    },
					items: [zzyd, sfyd, gkccyd, ggglfwyd, jtysyd, slss, nyyd, qt]
				}
			]
		});
	        
	    Ext.apply(this,{
	        width:600,
	        height:465,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[formpanel],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    zIsSpace : true,
			    handler : function() {
    				me.oper.formSubmit(formpanel , me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});