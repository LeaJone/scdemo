﻿/**
 *Z_CKQ Model
 *@author CodeSystem
 *文件名     Extjs_Model_Z_ckq
 *Model名    model_z_ckq
 */

Ext.define('model_z_ckq', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'xkzh',
            type : 'string'
        }
        ,
        {
            name : 'sqr',
            type : 'string'
        }
        ,
        {
            name : 'ksmc',
            type : 'string'
        }
        ,
        {
            name : 'kczkz',
            type : 'string'
        }
        ,
        {
            name : 'gmdw',
            type : 'string'
        }
        ,
        {
            name : 'kcfs',
            type : 'string'
        }
        ,
        {
            name : 'kqmj',
            type : 'string'
        }
        ,
        {
            name : 'yxqq',
            type : 'string'
        }
        ,
        {
            name : 'yxqz',
            type : 'string'
        }
        ,
        {
            name : 'szxzqmc',
            type : 'string'
        }
    ]
});
