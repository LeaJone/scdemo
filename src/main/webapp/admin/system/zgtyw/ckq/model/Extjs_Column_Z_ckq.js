﻿/**
 *Z_CKQ Column
 *@author CodeSystem
 *文件名     Extjs_Column_Z_ckq
 *Columns名  column_z_ckq
 *xtype: 'textfield', 'numbercolumn', 'booleancolumn', 'datecolumn'   format : '0.00'
 *align: 'left', 'center', 'right'
 */

Ext.define('column_z_ckq', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            align : 'center',
            width : 30
        }
        ,
        {
            header : '许可证号',
            dataIndex : 'xkzh',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '申请人',
            dataIndex : 'sqr',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '规模单位',
            dataIndex : 'gmdw',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '矿山名称',
            dataIndex : 'ksmc',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '开采主矿种',
            dataIndex : 'kczkz',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '开采方式',
            dataIndex : 'kcfs',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '有效期起',
            dataIndex : 'yxqq',
            sortable : true,
            align : 'left',
            flex : 1,
			renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value);
			}
        }
        ,
        {
            header : '有效期止',
            dataIndex : 'yxqz',
            sortable : true,
            align : 'left',
            flex : 1,
			renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value);
			}
        }
        ,
        {
            header : '矿区面积',
            dataIndex : 'kqmj',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '所在行政区',
            dataIndex : 'szxzqmc',
            sortable : true,
            align : 'left',
            flex : 1
        }
    ]
});
