﻿/**
 * 采矿权管理界面
 * @author lumingbao
 */
Ext.define('system.zgtyw.ckq.biz.CkqManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*', 
	             'system.zgtyw.ckq.model.Extjs_Model_Z_ckq',
	             'system.zgtyw.ckq.model.Extjs_Column_Z_ckq'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.zgtyw.ckq.biz.CkqOper'),
    
	createTable : function() {
	    var me = this;
	    
	    var queryText = Ext.create("Ext.form.TextField", {
	    	name : 'queryParam',
			emptyText : '请输入许可证号码',
			width : 130,
			enableKeyEvents : true,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
	    });
	    
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_ckq', // 显示列
			model : 'model_z_ckq',
			baseUrl : 'z_ckq/load_pagedata.do',
			pageSize : 30,
			border : false,
			tbar : [{
				xtype : "fsdbutton",
				popedomCode : 'jsydspdr',
				text : "导入",
				iconCls : 'office_excelIcon',
				handler : function() {
					me.oper.import(grid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
				text : "添加",
				iconCls : 'page_addIcon',
				handler : function() {
					me.oper.add(grid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
				text : "修改",
				iconCls : 'page_editIcon',
				handler : function() {
					me.oper.editor(grid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'dwsc',
				text : "删除",
				iconCls : 'page_deleteIcon',
				handler : function() {
				    me.oper.del(grid);
				}
			}, '->', queryText, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var xkzh =  queryText.getValue();
			var pram = {xkzh : xkzh};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});

		return grid;
	}
});