﻿/**
 * 采矿权编辑界面
 * @author lumingbao
 */
Ext.define('system.zgtyw.ckq.biz.CkqEdit', {
	extend : 'Ext.window.Window',
	requires : ['system.widget.FsdTreeComboBox'],
	grid : null,
	oper : Ext.create('system.zgtyw.ckq.biz.CkqOper'),
	initComponent:function(){
	    var me = this;	    
	   
	    var xkzh = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '许可证号',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'xkzh',
	    	maxLength: 50,
	    	allowBlank: false
	    });
	    
	    var sqr = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '申请人',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'sqr',
	    	maxLength: 20,
	    	allowBlank: false
	    });
	    
	    var gmdw = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '规模单位',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'gmdw',
	    	maxLength: 20,
	    	allowBlank: false
	    });
	    
	    var ksmc = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '矿山名称',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'ksmc',
	    	maxLength: 50,
	    	allowBlank: false
	    });
	    
	    var kczkz = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '开采矿种',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'kczkz',
	    	maxLength: 20,
	    	allowBlank: false
	    });
	    
	    var kcfs = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '开采方式',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'kcfs',
	    	maxLength: 20,
	    	allowBlank: false
	    });
	    
	    var yxqq = Ext.create('system.widget.FsdDateField',{
	    	fieldLabel: '有效期起',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'yxqq',
	    	allowBlank: false
	    });
	    
	    var yxqz = Ext.create('system.widget.FsdDateField',{
	    	fieldLabel: '有效期止',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'yxqz',
	    	allowBlank: false
	    });
	    
	    var kqmj = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '矿区面积',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'kqmj',
	    	maxLength: 20,
	    	allowBlank: false
	    });
	    
	    var szxzqmc = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '所在行政区',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'szxzqmc',
	    	maxLength: 20,
	    	allowBlank: false
	    });
	     
	    var beizhu = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'remark',
	    	maxLength: 50,
	    	maxLengthText: '备注不能超过50个字符'
	    });
	    
	    var formpanel = new Ext.form.Panel({
	    	defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '10 0 0 10',
			items : [{
	                    name: "id",
	                    xtype: "hidden"
	                } , 
	                xkzh, sqr, gmdw, ksmc, kczkz, kcfs, yxqq, yxqz, kqmj, szxzqmc]
	    });
	    Ext.apply(this,{
	        width:400,
	        height:350,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[formpanel],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    zIsSpace : true,
			    handler : function() {
    				me.oper.formSubmit(formpanel , me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});