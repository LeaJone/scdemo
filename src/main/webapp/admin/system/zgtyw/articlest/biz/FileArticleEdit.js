﻿/**
 * 下载中心文章编辑界面
 * @author lw
 */
Ext.define('system.zgtyw.articlest.biz.FileArticleEdit', {
	extend : 'system.widget.FsdFormPanelTab',
	oper : Ext.create('system.bcms.download.biz.DownloadOper'),
	grid : null,
	
	setControl : function(grid){
		var me = this;
		me.grid = grid;
	    me.formArticle.grid = me.grid;
	    me.formArticleWap.grid = me.grid;
	},
	
	loadObject : function(obj){
		var me = this;
		var pram = {id : obj.id};
        var param = {jsonData : Ext.encode(pram)};
        me.oper.util.formLoad(me.form.getForm() , 'b_download/load_ObjectByArticleId.do' , param , null , 
        function(response, options, respText){
    		me.idNo.setValue(obj.id);
        });
	},
	
	loadObjectID : function(id){
		var me = this;
		me.idNo.setValue(id);
		me.formArticleWap.idNo.setValue(id);
	},
	
	viewObject : function(){
		var me = this;
		me.formArticle.viewObject();
		me.formArticleWap.viewObject();
		me.down('button[name=btnsavefile]').setVisible(false);
	},
	
	setNew : function(){
		var me = this;
	    me.formArticleWap.setNew();
	    var obj = {
				id : null,
				articleid : null,
				type : 'xzwjwz',
				typename : '文章类型',
				name : null,
				path : null,
				sizes : null,
				language : null,
				version : null,
				classified : null,
				updated : null,
				adaptive : null,
				remark : null
			};
		me.form.getForm().setValues(obj);
	},
	
	initComponent : function() {
		var me = this;
		me.oper.form = me;
		
	    me.formArticle = Ext.create('system.zgtyw.articlest.biz.ArticleEditPanel');
	    me.formArticle.setArticleType(system.UtilStatic.articleTypeWJ);
	    me.formArticle.formWin = me;
	    me.formArticle.oper.form = me;

	    me.formArticleWap = Ext.create('system.bcms.article.biz.ArticleWapEditPanel');
	    me.formArticleWap.formWin = me;
	    me.formArticleWap.oper.form = me;

		me.idNo = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '文章编号',
	    	name : 'articleid',
	        listeners:{
	    		change : function(obj, newValue, oldValue, eOpts){
	    			if (newValue != null && newValue != ''){
	    				me.formArticleWap.setDisabled(false);
	    				formFile.setDisabled(false);
	    			}else{
	    				me.formArticleWap.setDisabled(true);
	    				formFile.setDisabled(true);
	    			}
    			}
    		}
	    });
		
		var type = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '文件类型',
	    	name : 'type',
	    	value : 'xzwjwz'
	    });
		var typename = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '文件类型名称',
	    	name : 'typename',
	    	value : '文章类型'
	    });
		
		var name = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '文件名',
	    	labelAlign:'right',
	    	labelWidth : 80,
	        width : 450,
	    	name: 'name',
	    	maxLength: 50,
	    	allowBlank: false,
	    	blankText: '请输入文件名',
	    	maxLengthText: '文件名不能超过50个字符'
	    });

	    var path = Ext.create('system.widget.FsdTextFileManage', {
	        width : 450,
	    	zName : 'path',
	    	zFieldLabel : '文件路径',
	    	zLabelWidth : 80,
	    	zIsUpButton : true,
	    	zAllowBlank : false,
	    	zBlankText : '请选择输入文件路径',
	    	zFileType : 'file',
	    	zFileAutoName : false,
	    	zFileUpPath : me.oper.util.getFilePath(),
	    	zManageType : 'manage'
	    });

	    var sizes = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '大小',
	    	labelAlign:'right',
	    	labelWidth : 80,
	        width : 450,
	    	name: 'sizes',
	    	maxLength: 50,
	    	maxLengthText: '文件大小不能超过50个字符'
	    });
	    
	    var language = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '语言',
	    	labelAlign:'right',
	    	labelWidth : 80,
	        width : 450,
	    	name: 'language',
	    	maxLength: 50,
	    	maxLengthText: '语言不能超过50个字符'
	    });
	    
	    var version = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '版本',
	    	labelAlign:'right',
	    	labelWidth : 80,
	        width : 450,
	    	name: 'version',
	    	maxLength: 50,
	    	maxLengthText: '版本不能超过50个字符'
	    });
	    
	    var classified = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '类型',
	    	labelAlign:'right',
	    	labelWidth : 80,
	        width : 450,
	    	name: 'classified',
	    	maxLength: 50,
	    	maxLengthText: '类型不能超过50个字符'
	    });
	    
	    var updated = Ext.create('system.widget.FsdDateField',{
	    	fieldLabel: '更新时间',
	    	labelAlign:'right',
	    	labelWidth : 80,
	        width : 450,
	    	name: 'updated',
	    	allowBlank: false,
	    	blankText: '请输入更新时间'
	    });
	    
	    var adaptive = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '适合系统',
	    	labelAlign:'right',
	    	labelWidth : 80,
	        width : 450,
	    	name: 'adaptive',
	    	maxLength: 50,
	    	maxLengthText: '适合系统不能超过50个字符'
	    });
	    
	    var remark = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth : 80,
	        width : 900,
	        height : 50,
	    	name: 'remark',
	    	maxLength: 100,
	    	maxLengthText: '描述不能超过100个字符'
	    });
	    
	    me.form = Ext.create('Ext.form.Panel',{
				border : false,
				padding : '10 0 30 20',
				items : [{
                    name: "id",
                    xtype: "hidden"
                }, me.idNo, type, typename, 
                {
                    layout : 'column',
                    border : false,
        	        width : 900,
                    items:[{
                        columnWidth : 0.5,
                        border:false,
                        items:[name, updated, sizes, language]
                    }, {
                        columnWidth : 0.5,
                        border:false,
                        items:[path, version, classified, adaptive]
                    }]
    			}, 
    			remark]
	    });
	    
	    var formFile = Ext.create('Ext.panel.Panel',{
        	title: '文件信息发布',
			autoScroll : true,
			disabled : true,
	        tbar : [{
				xtype : "fsdbutton",
				popedomCode : 'nrxzbj',
				text : "提交文件信息",
	        	name : 'btnsavefile',
				iconCls : 'acceptIcon',
				handler : function(button) {
					me.oper.formSubmit(me.form, me.grid, false);
				}
			}],
			items : [ me.form ]
	    });
	    
		Ext.apply(me,{
	        items : [ 
	                 me.formArticle,
	                 //me.formArticleWap,
	                 formFile
	                  ]
	    });
		this.callParent(arguments);
	}
});