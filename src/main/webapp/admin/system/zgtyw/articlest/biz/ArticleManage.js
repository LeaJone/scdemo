﻿/**
 * 文本文章管理界面
 * @author lumingbao
 */

Ext.define('system.zgtyw.articlest.biz.ArticleManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*' , 
	             'system.bcms.article.model.Extjs_Column_B_article',
	             'system.bcms.article.model.Extjs_Model_B_article'],
	header : false,
	border : 0,
	layout : 'fit',
	
	oper : Ext.create('system.bcms.article.biz.ArticleOper'),
	operst : Ext.create('system.zgtyw.articlest.biz.ArticleOper'),
	queryObj : null,
	
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.operst.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	createContent : function(){
		var me = this;
		
		var queryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入文章标题',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});
		
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_b_article', // 显示列
			model: 'model_b_article',
			baseUrl : 'B_Article/load_Article.do',
			border : false,
			tbar : {
				enableOverflow: true,//控件过多，出现下拉按钮
				defaults: {
			        height : 53
				},
				items: [{
			        xtype: 'buttongroup',
			        columns: 2,
			        items: [{
					xtype : "fsdbutton",
					text : "添加文章",
					iconCls : 'articlewz',
					popedomCode : 'nrwzbj',
					handler : function(button) {
						me.operst.addText(grid);
					}
				}, {
					xtype : "fsdbutton",
					text : "添加组图",
					iconCls : 'articlezt',
					popedomCode : 'nrztbj',
					handler : function(button) {
						me.operst.addImage(grid);
					}
				}, {
					xtype : "fsdbutton",
					text : "添加视频",
					iconCls : 'articlesp',
					popedomCode : 'nrspbj',
					handler : function(button) {
						me.operst.addVideo(grid);
					}
				}, {
					xtype : "fsdbutton",
					text : "添加下载",
					iconCls : 'articlewj',
					popedomCode : 'nrxzbj',
					handler : function(button) {
						me.operst.addFile(grid);
					}
				}]}, {
			        xtype: 'buttongroup',
			        columns: 2,
			        items: [{
					xtype : "fsdbutton",
					text : "修改",
					iconCls : 'page_editIcon',
					popedomCode : 'wzbj',
					handler : function(button) {
						me.operst.editor(grid);
					}
				}, {
					text : '查看',
					iconCls : 'magnifierIcon',
					handler : function() {
						me.operst.view(grid);
					}
				}, {
					xtype : "fsdbutton",
					text : "删除",
					iconCls : 'page_deleteIcon',
					popedomCode : 'wzsc',
					handler : function(button) {
					    me.oper.del(grid);
					}
				}]}, {
			        xtype: 'buttongroup',
			        columns: 2,
			        items: [{
					xtype : "fsdbutton",
					text : "审核通过",
					iconCls : 'tickIcon',
					popedomCode : 'wzsh',
					handler : function(button) {
						me.oper.auditing(grid , 'true');
					}
				}, {
					xtype : "fsdbutton",
					text : "取消审核",
					iconCls : 'crossIcon',
					popedomCode : 'wzsh',
					handler : function(button) {
						me.oper.auditing(grid , 'false');
					}
				}, {
					xtype : "fsdbutton",
					text : "文章置顶",
					iconCls : 'tickIcon',
					popedomCode : 'wzsh',
					handler : function(button) {
						me.oper.firstly(grid , 'true');
					}
				}, {
					xtype : "fsdbutton",
					text : "取消置顶",
					iconCls : 'crossIcon',
					popedomCode : 'wzsh',
					handler : function(button) {
						me.oper.firstly(grid , 'false');
					}
				}]}, {
			        xtype: 'buttongroup',
			        columns: 1,
			        items: [{
						xtype : "fsdbutton",
						text : "更改文章类型",
						iconCls : 'tickIcon',
						popedomCode : 'wzbj',
						handler : function(button) {
							me.oper.updateType(grid);
						}
					}, {
						xtype : "fsdbutton",
						text : "更改所属栏目",
						iconCls : 'crossIcon',
						popedomCode : 'wzbj',
						handler : function(button) {
							me.oper.updateSubject(grid);
						}
				}]}, {
			        xtype: 'buttongroup',
			        columns: 2,
			        items: [queryText , {
					text : '查询',
					iconCls : 'magnifierIcon',
					handler : function() {
						me.queryObj = {title : queryText.getValue()};
						grid.getStore().loadPage(1);
					}
				}, {
					text : '高级查询',
					iconCls : 'magnifierIcon',
					handler : function() {
						me.oper.query(me, grid);
					}
				}]}
				, {
			        xtype: 'buttongroup',
			        columns: 1,
			        items: [{
					text : '刷新',
					iconCls : 'tbar_synchronizeIcon',
		            rowspan: 2,
		            iconAlign: 'top',
					handler : function() {
						grid.getStore().reload();
					}
				}]}
			]}			
		});
		
		grid.getStore().on('beforeload', function(s) {
			if (me.queryObj == null){
				me.queryObj = {};
			}
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(me.queryObj)});
		});
		
		return grid;
	}
});