﻿/**
 * 视频文章编辑界面
 * @author lw
 */
Ext.define('system.zgtyw.articlest.biz.VideoArticleEdit', {
	extend : 'system.widget.FsdFormPanelTab',
	oper : Ext.create('system.bcms.article.biz.ArticleOper'),
	grid : null,
	
	setControl : function(grid){
		var me = this;
		me.grid = grid;
	    me.formArticle.grid = me.grid;
	    me.formArticleWap.grid = me.grid;
	},
	
	loadObject : function(obj){
		var me = this;
		me.idNo.setValue(obj.id);
	},
	
	loadObjectID : function(id){
		var me = this;
		me.idNo.setValue(id);
		me.formArticleWap.idNo.setValue(id);
	},
	
	viewObject : function(){
		var me = this;
		me.formArticle.viewObject();
		me.formArticleWap.viewObject();
	},
	
	setNew : function(){
		var me = this;
	    me.formArticleWap.setNew();
	},
	
	initComponent : function() {
		var me = this;
		me.oper.form = me;
		
	    me.formArticle = Ext.create('system.zgtyw.articlest.biz.ArticleEditPanel');
	    me.formArticle.setArticleType(system.UtilStatic.articleTypeSP);
	    me.formArticle.formWin = me;
	    me.formArticle.oper.form = me;

	    me.formArticleWap = Ext.create('system.bcms.article.biz.ArticleWapEditPanel');
	    me.formArticleWap.formWin = me;
	    me.formArticleWap.oper.form = me;

		me.idNo = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '编号',
	    	name : 'id',
	        listeners:{
	    		change : function(obj, newValue, oldValue, eOpts){
	    			if (newValue != null && newValue != ''){
	    				me.formArticleWap.setDisabled(false);
	    			}else{
	    				me.formArticleWap.setDisabled(true);
	    			}
    			}
    		}
	    });
	    
		Ext.apply(me, {
	        items : [ 
	                 me.formArticle
	                 //,
	                 //me.formArticleWap
	                  ]
	    });
		
		me.callParent(arguments);
	}
});