/**
 * 文本文章操作类
 * @author lw
 */

Ext.define('system.zgtyw.articlest.biz.ArticleOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 添加文本文章
     */
	addText : function(grid){
		var me = this;
		var model = Ext.create("system.zgtyw.articlest.biz.TextArticleEdit");
		model.setControl(grid);
		me.util.addTab("nrwzbj" , "添加文章" , model , "resources/admin/icon/vcard_add.png");
	},
	/**
     * 添加组图文章
     */
	addImage : function(grid){
		var me = this;
		var model = Ext.create("system.zgtyw.articlest.biz.ImageArticleEdit");
		model.setControl(grid);
		me.util.addTab("nrztbj" , "添加组图" , model , "resources/admin/icon/photo.png");
	},
	/**
     * 添加视频
     */
	addVideo : function(grid){
		var me = this;
		var model = Ext.create("system.zgtyw.articlest.biz.VideoArticleEdit");
		model.setControl(grid);
		me.util.addTab("nrspbj" , "添加视频" , model , "resources/admin/icon/film.png");
	},
	/**
     * 添加下载文件
     */
	addFile : function(grid){
		var me = this;
		var model = Ext.create("system.zgtyw.articlest.biz.FileArticleEdit");
		model.setControl(grid);
		me.util.addTab("nrxzbj" , "添加下载文件" , model , "resources/admin/icon/drive_web.png");
	},
	
	/**
     * 表单提交
     */
     submitArticle : function(formpanel, grid, treesubject, treesubjectxg, formWin){
        var me = this;
        if (formpanel.form.form.isValid()) {
        	var checkedNodes = treesubject.getChecked();
    		var sids = [];
    		for(var i=0 ; i < checkedNodes.length ; i++){
    			sids.push(checkedNodes[i].internalId)
    		}
    		checkedNodes = treesubjectxg.getChecked();
    		var sxgids = [];
    		for(var i=0 ; i < checkedNodes.length ; i++){
    			sxgids.push(checkedNodes[i].internalId)
    		}
    		var idMap = {};
    		idMap[system.UtilStatic.articleRelateLM] = sids;
    		idMap[system.UtilStatic.articleRelateXG] = sxgids;
            me.util.ExtFormSubmit('B_Article/save_Article.do' , formpanel.form.form , '正在提交数据,请稍候.....',
			function(form, action, respText){
            	if(grid != null){
            		grid.getStore().reload();//刷新管理界面的表格
            	}
            	formpanel.idNo.setValue(action.result.id);
            	formWin.loadObjectID(action.result.id);
				Ext.MessageBox.alert('提示', '保存成功！');
			},
			function (form, action){
				Ext.MessageBox.alert('异常', action.result.msg);
			},
			{jsonData : Ext.encode(idMap)});
        }
     },
 	
 	/**
     * 修改
     */
	editor : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的记录！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个记录！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            var type = data[0].data.type;
            switch (type) {
    		case system.UtilStatic.articleTypeWZ :
    			me.editorText(grid, id);
    			break;
    		case system.UtilStatic.articleTypeZT :
    			me.editorImage(grid, id);
    			break;
    		case system.UtilStatic.articleTypeSP :
    			me.editorVideo(grid, id);
    			break;
    		case system.UtilStatic.articleTypeWJ :
    			me.editorFile(grid, id);
    			break;
    		}
        }
	},
 	/**
     * 修改
     */
	editorText : function(grid, id){
		var me = this;
		var model = me.util.getTab("editnrwzbj");
		if (model == null){
			model = Ext.create("system.zgtyw.articlest.biz.TextArticleEdit");
			model.setControl(grid);
		}
        var pram = {id : id};
        var param = {jsonData : Ext.encode(pram)};
        me.util.formLoad(model.formArticle.form, 'B_Article/load_ArticleByid.do', param, null, 
        function(response, options, respText, data){
        	model.formArticleWap.loadObject(data.data, data[system.UtilStatic.articleRelateWAP], data[system.UtilStatic.articleRelateWAPXG]);
        	model.formArticle.loadObject(data[system.UtilStatic.articleRelateLM], data[system.UtilStatic.articleRelateXG]);
        	model.loadObject(data.data);
        	me.util.addTab("editnrwzbj" , "修改文章" , model , "resources/admin/icon/vcard_edit.png");
        });
	},
 	/**
     * 修改
     */
	editorImage : function(grid, id){
		var me = this;
		var model = me.util.getTab("editnrztbj");
		if (model == null){
			model = Ext.create("system.zgtyw.articlest.biz.ImageArticleEdit");
			model.setControl(grid);
		}
        var pram = {id : id};
        var param = {jsonData : Ext.encode(pram)};
        me.util.formLoad(model.formArticle.form, 'B_Article/load_ArticleByid.do', param, null, 
        function(response, options, respText, data){
        	model.formArticleWap.loadObject(data.data, data[system.UtilStatic.articleRelateWAP], data[system.UtilStatic.articleRelateWAPXG]);
        	model.formArticle.loadObject(data[system.UtilStatic.articleRelateLM], data[system.UtilStatic.articleRelateXG]);
        	model.loadObject(data.data);
        	me.util.addTab("editnrztbj" , "修改组图" , model , "resources/admin/icon/photo.png");
        });
	},
 	/**
     * 修改
     */
	editorVideo : function(grid, id){
		var me = this;
		var model = me.util.getTab("editnrspbj");
		if (model == null){
			model = Ext.create("system.zgtyw.articlest.biz.VideoArticleEdit");
			model.setControl(grid);
		}
        var pram = {id : id};
        var param = {jsonData : Ext.encode(pram)};
        me.util.formLoad(model.formArticle.form, 'B_Article/load_ArticleByid.do', param, null, 
        function(response, options, respText, data){
        	model.formArticleWap.loadObject(data.data, data[system.UtilStatic.articleRelateWAP], data[system.UtilStatic.articleRelateWAPXG]);
        	model.formArticle.loadObject(data[system.UtilStatic.articleRelateLM], data[system.UtilStatic.articleRelateXG]);
        	model.loadObject(data.data);
        	me.util.addTab("editnrspbj" , "修改视频" , model , "resources/admin/icon/film.png");
        });
	},
 	/**
     * 修改
     */
	editorFile : function(grid, id){
		var me = this;
		var model = me.util.getTab("editnrxzbj");
		if (model == null){
			model = Ext.create("system.zgtyw.articlest.biz.FileArticleEdit");
			model.setControl(grid);
		}
        var pram = {id : id};
        var param = {jsonData : Ext.encode(pram)};
        me.util.formLoad(model.formArticle.form, 'B_Article/load_ArticleByid.do', param, null, 
        function(response, options, respText, data){
        	model.formArticleWap.loadObject(data.data, data[system.UtilStatic.articleRelateWAP], data[system.UtilStatic.articleRelateWAPXG]);
        	model.formArticle.loadObject(data[system.UtilStatic.articleRelateLM], data[system.UtilStatic.articleRelateXG]);
        	model.loadObject(data.data);
        	me.util.addTab("editnrxzbj" , "修改下载" , model , "resources/admin/icon/drive_web.png");
        });
	},
 	
 	/**
     * 查看
     */
	view : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要查看的记录！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个记录！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            var type = data[0].data.type;
            switch (type) {
    		case system.UtilStatic.articleTypeWZ :
    			me.viewText(id);
    			break;
    		case system.UtilStatic.articleTypeZT :
    			me.viewImage(id);
    			break;
    		case system.UtilStatic.articleTypeSP :
    			me.viewVideo(id);
    			break;
    		case system.UtilStatic.articleTypeWJ :
    			me.viewFile(id);
    			break;
    		}
        }
	},
 	/**
     * 查看
     */
	viewText : function(id){
		var me = this;
        var model = Ext.create("system.zgtyw.articlest.biz.TextArticleEdit");
        model.viewObject();
        var pram = {id : id};
        var param = {jsonData : Ext.encode(pram)};
        me.util.formLoad(model.formArticle.form, 'B_Article/load_ArticleByid.do', param, null, 
        function(response, options, respText, data){
        	model.formArticleWap.loadObject(data.data, data[system.UtilStatic.articleRelateWAP], data[system.UtilStatic.articleRelateWAPXG]);
        	model.formArticle.loadObject(data[system.UtilStatic.articleRelateLM], data[system.UtilStatic.articleRelateXG]);
        	model.loadObject(data.data);
            me.util.addTab(id + "view", "查看文章", model, "resources/admin/icon/vcard_edit.png");
        });
	},
 	/**
     * 查看
     */
	viewImage : function(id){
		var me = this;
		var model = Ext.create("system.zgtyw.articlest.biz.ImageArticleEdit");
        model.viewObject();
        var pram = {id : id};
        var param = {jsonData : Ext.encode(pram)};
        me.util.formLoad(model.formArticle.form, 'B_Article/load_ArticleByid.do', param, null, 
        function(response, options, respText, data){
        	model.formArticleWap.loadObject(data.data, data[system.UtilStatic.articleRelateWAP], data[system.UtilStatic.articleRelateWAPXG]);
        	model.formArticle.loadObject(data[system.UtilStatic.articleRelateLM], data[system.UtilStatic.articleRelateXG]);
        	model.loadObject(data.data);
        	me.util.addTab(id + "view", "查看组图" , model , "resources/admin/icon/photo.png");
        });
	},
 	/**
     * 查看
     */
	viewVideo : function(grid, id){
		var me = this;
		var model = Ext.create("system.zgtyw.articlest.biz.VideoArticleEdit");
        model.viewObject();
        var pram = {id : id};
        var param = {jsonData : Ext.encode(pram)};
        me.util.formLoad(model.formArticle.form, 'B_Article/load_ArticleByid.do', param, null, 
        function(response, options, respText, data){
        	model.formArticleWap.loadObject(data.data, data[system.UtilStatic.articleRelateWAP], data[system.UtilStatic.articleRelateWAPXG]);
        	model.formArticle.loadObject(data[system.UtilStatic.articleRelateLM], data[system.UtilStatic.articleRelateXG]);
        	model.loadObject(data.data);
        	me.util.addTab(id + "view", "查看视频" , model , "resources/admin/icon/film.png");
        });
	},
 	/**
     * 查看
     */
	viewFile : function(grid, id){
		var me = this;
		var model = Ext.create("system.zgtyw.articlest.biz.FileArticleEdit");
        model.viewObject();
        var pram = {id : id};
        var param = {jsonData : Ext.encode(pram)};
        me.util.formLoad(model.formArticle.form, 'B_Article/load_ArticleByid.do', param, null, 
        function(response, options, respText, data){
        	model.formArticleWap.loadObject(data.data, data[system.UtilStatic.articleRelateWAP], data[system.UtilStatic.articleRelateWAPXG]);
        	model.formArticle.loadObject(data[system.UtilStatic.articleRelateLM], data[system.UtilStatic.articleRelateXG]);
        	model.loadObject(data.data);
        	me.util.addTab(id + "view", "查看下载" , model , "resources/admin/icon/drive_web.png");
        });
	}
});