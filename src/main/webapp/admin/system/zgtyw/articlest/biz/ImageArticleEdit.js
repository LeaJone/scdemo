﻿/**
 * 组图文章编辑界面
 * @author lw
 */
Ext.define('system.zgtyw.articlest.biz.ImageArticleEdit', {
	extend : 'system.widget.FsdFormPanelTab',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'system.widget.FsdButton', 'Ext.data.*', 
	             'system.bcms.article.model.Extjs_Column_B_articleimage',
	             'system.bcms.article.model.Extjs_Model_B_articleimage'],
	oper : Ext.create('system.bcms.article.biz.ArticleOper'),
	operimg : Ext.create('system.bcms.article.biz.ImageArticleOper'),
	grid : null,
	
	setControl : function(grid){
		var me = this;
		me.grid = grid;
	    me.formArticle.grid = me.grid;
	    me.formArticleWap.grid = me.grid;
	},
	
	loadObject : function(obj){
		var me = this;
		me.idNo.setValue(obj.id);
		me.gridImage.getStore().removeAll();
		me.gridImage.getStore().load();
	},
	
	loadObjectID : function(id){
		var me = this;
		me.idNo.setValue(id);
		me.formArticleWap.idNo.setValue(id);
	},
	
	viewObject : function(){
		var me = this;
		me.formArticle.viewObject();
		me.formArticleWap.viewObject();
		me.down('button[name=btnimgnew]').setVisible(false);
		me.down('button[name=btnimgupdate]').setVisible(false);
		me.down('button[name=btnimgdelete]').setVisible(false);
		me.down('button[name=btnimgqy]').setVisible(false);
		me.down('button[name=btnimgty]').setVisible(false);
	},
	
	setNew : function(){
		var me = this;
	    me.formArticleWap.setNew();
		me.gridImage.getStore().removeAll();
	},
	
	initComponent : function() {
		var me = this;
		me.oper.form = me;
		
	    me.formArticle = Ext.create('system.zgtyw.articlest.biz.ArticleEditPanel');
	    me.formArticle.setArticleType(system.UtilStatic.articleTypeZT);
	    me.formArticle.formWin = me;
	    me.formArticle.oper.form = me;

	    me.formArticleWap = Ext.create('system.bcms.article.biz.ArticleWapEditPanel');
	    me.formArticleWap.formWin = me;
	    me.formArticleWap.oper.form = me;

		me.idNo = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '编号',
	    	name : 'id',
	        listeners:{
	    		change : function(obj, newValue, oldValue, eOpts){
	    			if (newValue != null && newValue != ''){
	    				me.formArticleWap.setDisabled(false);
	    				formText.setDisabled(false);
	    			}else{
	    				me.formArticleWap.setDisabled(true);
	    				formText.setDisabled(true);
	    			}
    			}
    		}
	    });
		
		//右边文章表格
		me.gridImage = Ext.create("system.widget.FsdGridPanel", {
			column : 'column_b_articleimage', // 显示列
			model: 'model_b_articleimage',
			baseUrl : 'b_articleimage/load_DataByArticleId.do',
			border : false,
			zAutoLoad : false,
			tbar : [{
				xtype : "fsdbutton",
				text : "添加",
				name : 'btnimgnew',
				iconCls : 'page_addIcon',
				popedomCode : 'nrztbj',
				handler : function(button) {
					me.operimg.add(me.gridImage , me.idNo.getValue());
				}
			}, {
				xtype : "fsdbutton",
				text : "修改",
				name : 'btnimgupdate',
				iconCls : 'page_editIcon',
				popedomCode : 'nrztbj',
				handler : function(button) {
					me.operimg.editor(me.gridImage);
				}
			}, {
				xtype : "fsdbutton",
				text : "删除",
				name : 'btnimgdelete',
				iconCls : 'page_deleteIcon',
				popedomCode : 'nrztbj',
				handler : function(button) {
				    me.operimg.del(me.gridImage);
				}
			}, {
				xtype : "fsdbutton",
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.operimg.view(me.gridImage);
				}
			}, '-', {
				xtype : "fsdbutton",
				popedomCode : 'nrztbj',
				text : "启用",
				name : 'btnimgqy',
				iconCls : 'page_deleteIcon',
				handler : function(button) {
				    me.operimg.isEnabled(me.gridImage, true);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'nrztbj',
				text : "停用",
				name : 'btnimgty',
				iconCls : 'page_deleteIcon',
				handler : function(button) {
				    me.operimg.isEnabled(me.gridImage, false);
				}
			}, '->', {
				xtype : "fsdbutton",
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					me.gridImage.getStore().reload();
				}
			}]
		});
		me.gridImage.getStore().on('beforeload', function(s) {
			var pram = {fid : me.idNo.getValue()};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
	    
	    var formText = Ext.create('Ext.panel.Panel',{
        	title: '组图信息管理',
        	layout : 'fit',
			disabled : true,
			items : [me.gridImage]
	    });
	    
	    me.on('afterrender', function(obj, eOpts){
	    });
	    
	    
		Ext.apply(me, {
	        items : [ 
	                 me.formArticle,
	                 //me.formArticleWap,
	                 formText
	                  ]
	    });
		
		me.callParent(arguments);
	}
});