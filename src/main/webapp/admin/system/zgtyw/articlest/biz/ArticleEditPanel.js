﻿/**
 * 文本文章编辑界面
 * @author lw
 */
Ext.define('system.zgtyw.articlest.biz.ArticleEditPanel', {
	extend : 'Ext.panel.Panel',
	oper : Ext.create('system.bcms.article.biz.ArticleOper'),
	operst : Ext.create('system.zgtyw.articlest.biz.ArticleOper'),
	
	grid : null,//管理表格
	formWin : null,//编辑窗体
	
	loadObject : function(lstLM, lstXG){
		var me = this;
		me.treesubject.setChosseValues(lstLM);
		me.treesubjectxg.setChosseValues(lstXG);
		me.subjectid.setText(me.subjectname.getValue());
		me.branchid.setText(me.branchname.getValue());
		
		//所属单位控制代码
		
	},
	
	viewObject : function(){
		var me = this;
		me.down('button[name=btnsave]').setVisible(false);
		me.down('button[name=btnnew]').setVisible(false);
	},
	
	initComponent : function() {
		var me = this;

		me.idNo = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '编号',
	    	name : 'id'
	    });
		
		me.type = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '文章类型',
	    	value : 'wzwz',
	    	name : 'type'
	    });
		
		me.typename = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '文章类型名称',
	    	value : '文章',
	    	name : 'typename'
	    });
		
		me.branchname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属机构名称',
	    	name: 'branchname'
	    });
		me.branchid = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属机构',
	    	labelAlign:'right',
	    	labelWidth:80,
	    	rootText : me.oper.util.getParams("company").name,
	    	rootId : 'e66c35349d2040e5bd411c7f18b5eb25',
			rootVisible: false,
	        width : 450,
	        name : 'branchid',
	        baseUrl:'A_Branch/load_AsyncBranchTree.do',//访问路劲
	        allowBlank: false,
	        hiddenName : 'branchname'//隐藏域Name
	    });
		
		me.subjectname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属栏目名称',
	    	name: 'subjectname'
	    });
	    me.subjectid = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属栏目',
	    	labelAlign:'right',
	    	labelWidth:80,
	    	rootText : '主栏目',
	    	rootId : 'FSDMAIN',
			rootVisible: false,
	        width : 450,
	        name : 'subjectid',
	        baseUrl:'B_Subject/load_AsyncSubjectTreeByPopdom.do',//访问路劲
	        allowBlank: false,
	        hiddenName : 'subjectname'//隐藏域Name
	    });
	    
	    me.showtype = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'显示类型',
	        labelAlign:'right',
	    	labelWidth:80,
	        width : 450,
    	    name : 'showtype',//提交到后台的参数名
	        key : 'wzxslx',//参数
	        allowBlank: false,
    	    hiddenName : 'showtypename',//提交隐藏域Name
    	    zDefaultCode : 'wzxspt'//默认值 wzxspt，wzxsht
	    });  
	    me.showtypename = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '显示类型名称',
	    	name: 'showtypename'
	    });
	    
	    me.titlecolor = Ext.create('system.widget.FsdTextColor', {
	        width : 450,
	    	zName : 'titlecolor', 
	    	zFieldLabel : '标题颜色', 
	    	zLabelWidth : 80,
	    	zAllowBlank : false
	    });

	    
	    me.checkboxgroup = new Ext.form.CheckboxGroup({
		    fieldLabel: '文章属性',
		    labelAlign:'right',
	    	labelWidth:80,
		    width: 450,
		    items: [
		        {
			    name : 'firstly',    
			    boxLabel: '置顶',
			    inputValue: 'true'
			    }, 
			    {
				name : 'slide',
				boxLabel: '幻灯',
				inputValue: 'true'
				}
//			    , 
//				{
//		    	name : 'recommend',
//			    boxLabel: '推荐',
//			    inputValue: 'true'
//			    }, 
//			    {
//			    name : 'video',  
//			    boxLabel: '视频',
//			    inputValue: 'true'
//			    }, 
//			    {
//			    name : 'roll',
//				boxLabel: '滚动',
//				inputValue: 'true'
//			    }, 
//			    {
//			    name : 'headline',
//				boxLabel: '头条',
//				inputValue: 'true'
//				}, 
//				{
//				name : 'hot',
//				boxLabel: '热门',
//				inputValue: 'true'
//				}, 
//				{
//				name : 'comments',
//				boxLabel: '评论',
//				inputValue: 'true'
//				}
			    ]
	    });

	    me.aurl = Ext.create('system.widget.FsdTextCheckbox',{
	    	zNameCheckbox : 'isaurl',
	    	zFieldLabel : '外链接',
	    	zLabelAlign : 'right',
	    	zLabelWidth : 80,
	    	zInputValue : 'true',
	    	zNameText : 'aurl',
	        width : 450,
	        zMaxLength : 200
	    });
	    
	    me.author = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '作者',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 450,
	    	name: 'author',
	    	maxLength: 50
	    });
	    
	    me.source = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '来源',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 450,
	    	name: 'source',
	    	value : me.oper.util.getParams("company").name,
	    	maxLength: 50,
	    	allowBlank: false
	    });
	    
	    me.overdue = Ext.create('system.widget.FsdDateField',{
	    	fieldLabel: '过期时间',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 450,
	    	name: 'overdue'
	    });
	    
	    me.videourl = Ext.create('system.widget.FsdTextFileManage', {
	        width : 450,
	    	zName : 'videourl',
	    	zFieldLabel : '内容视频',
	    	zLabelWidth : 80,
	    	zIsShowButton : true,
	    	zIsUpButton : true,
	    	zFileType : 'video',
	    	zFileAutoName : false,
	    	zFileUpPath : me.oper.util.getVideoPath(),
	    	zManageType : 'manage'
	    });
	    
	    me.voiceurl = Ext.create('system.widget.FsdTextFileManage', {
	        width : 450,
	    	zName : 'voiceurl',
	    	zFieldLabel : '内容语音',
	    	zLabelWidth : 80,
	    	zIsShowButton : true,
	    	zIsUpButton : true,
	    	zFileType : 'voice',
	    	zFileAutoName : false,
	    	zFileUpPath : me.oper.util.getVoicePath(),
	    	zManageType : 'manage'
	    });
	    
	    me.adddate = Ext.create('system.widget.FsdDateTime', {
	    	fieldLabel: '新增时间',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 450,
	    	name: 'adddate',
	    });
	    
	    me.title = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '标题',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 900,
	    	name: 'title',
	    	maxLength: 50,
	    	allowBlank: false
	    });
	    
	    me.subtitle = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '副标题',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 900,
	    	name: 'subtitle',
	    	maxLength: 100
	    });
	    
	    me.showtitle = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '显示标题',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 900,
	    	name: 'showtitle',
	    	maxLength: 100,
	    	allowBlank: false
	    });
	    
	    me.abstracts = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '简介',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 900,
	    	name: 'abstracts',
	    	maxLength: 250
	    });
	    
	    me.content = Ext.create('system.widget.FsdUeditor',{
	    	fieldLabel: '内容',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 920,
	        height : 500,
	    	name: 'content'
	    });
	    
	    me.imageurl1 = Ext.create('system.widget.FsdTextFileManage', {
	        width : 450,
	    	zName : 'imageurl1',
	    	zFieldLabel : '标题图片',
	    	zLabelWidth : 80,
	    	zIsShowButton : true,
	    	zIsGetButton : true,
	    	zIsUpButton : true,
	    	zContentObj : me.content,
	    	zFileUpPath : me.oper.util.getImagePath()
	    });
	    
	    me.remark = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 450,
	    	name: 'remark',
	    	maxLength: 50
	    });
	    
	    /**
		 * 栏目面板
		 */
		me.treesubject = Ext.create("system.widget.FsdTreePanel", {
			title: "所属栏目",
		    columnWidth : 0.5,
    		minWidth : 350,
    		border : true,
            rootVisible : false,//不显示根节点
            tbar : [{
					xtype : "button",
					text : "展开",
					iconCls : 'expand-all',
					handler : function() {
						me.treesubject.expandAll();
					}
				},{
					xtype : "button",
					text : "收缩",
					iconCls : 'collapse-all',
					handler : function() {
						me.treesubject.collapseAll();
					}
				}]
    	});
	    
	    /**
		 * 相关栏目面板
		 */
		me.treesubjectxg = Ext.create("system.widget.FsdTreePanel", {
			title: "相关栏目",
		    columnWidth : 0.5,
    		minWidth : 350,
    		border : true,
            rootVisible : false,//不显示根节点
            tbar : [{
					xtype : "button",
					text : "展开",
					iconCls : 'expand-all',
					handler : function() {
						me.treesubjectxg.expandAll();
					}
				},{
					xtype : "button",
					text : "收缩",
					iconCls : 'collapse-all',
					handler : function() {
						me.treesubjectxg.collapseAll();
					}
				}]
    	});
	    
	    
	    me.form = Ext.create('Ext.form.Panel',{
				border : false,
//				trackResetOnLoad : true,
				padding : '10 0 30 20',
				items : [me.idNo, me.type, me.typename, 
				         me.branchname, me.showtypename, me.subjectname,
                {
                    layout : 'column',
                    border : false,
        	        width : 900,
                    items:[{
                        columnWidth : 0.5,
                        border:false,
                        items:[me.branchid, me.source, me.author]
                    }, {
                        columnWidth : 0.5,
                        border:false,
                        items:[me.subjectid, me.titlecolor, me.showtype]
                    }]
    			}, 
    			me.title, me.subtitle, me.showtitle, me.abstracts, me.content,
    			{
                    layout : 'column',
    				padding : '25 0 0 0',
                    border : false,
        	        width : 900,
                    items:[{
                        columnWidth : 0.5,
                        border : false,
                        items : [me.imageurl1, me.aurl, me.voiceurl, me.remark]
                    }, {
                        columnWidth : 0.5,
                        border : false,
                        items : [me.videourl, me.overdue, me.checkboxgroup, me.adddate]
                    }]
    			},{
					xtype : 'fieldset',
	                title : '关联信息',
                    layout : 'column',
                    border : true,
        	        width : 900,
	                items : [ me.treesubject, me.treesubjectxg ]
				}]
	    });
	    
	    me.on('afterrender', function(obj, eOpts){
			me.oper.LoadSubjectTree(me.treesubject);
			me.oper.LoadSubjectXGTree(me.treesubjectxg);
	    });
	    
	    me.on('boxready' , function(obj, width, height, eOpts){
        	me.subjectid.setText(me.subjectname.getValue());
        	me.branchid.setText(me.branchname.getValue());
	    });
	    
		Ext.apply(me, {
        	title: '内容信息发布',
			autoScroll : true,
	        tbar : [{
				xtype : "fsdbutton",
				popedomCode : 'wzbj',
				text : "内容提交",
	        	name : 'btnsave',
				iconCls : 'acceptIcon',
				handler : function(button) {
					me.operst.submitArticle(me, me.grid, me.treesubject, me.treesubjectxg, me.formWin);
				}
			}, {
				xtype : "fsdbutton",
				zPopedomCodeArray :['nrwzbj', 'nrztbj', 'nrspbj', 'nrxzbj'],
				text : "新增内容",
	        	name : 'btnnew',
				iconCls : 'tbar_synchronizeIcon',
				handler : function(button) {
					var obj = {
						id : null,
						title : null,
						content : null,
						abstracts : null,
						firstly : null,
						slide : null,
						isaurl : null,
						aurl : null,
						author : null,
						overdue : null,
						imageurl1 : null,
						videourl : null,
						voiceurl : null,
						remark : null
					};
					me.form.getForm().setValues(obj);
					me.treesubject.setChosseValues(null);
					me.treesubjectxg.setChosseValues(null);
					me.formWin.setNew();
				}
			}],
	        items :[me.form]
	    });
		me.callParent(arguments);
	},
	
	
	setArticleType : function(type){
		var me = this;
		switch (type) {
		case system.UtilStatic.articleTypeWZ :
			me.videourl.setVisible(false);
			me.typename.setValue('文章');
			break;
		case system.UtilStatic.articleTypeZT :
			me.videourl.setVisible(false);
			me.showtype.setVisible(false);
			me.overdue.setVisible(false);
			me.typename.setValue('组图');
			break;
		case system.UtilStatic.articleTypeSP :
			me.showtype.setVisible(false);
			me.overdue.setVisible(false);
			me.videourl.zSetAllowBlank(false);
			me.typename.setValue('视频');
			break;
		case system.UtilStatic.articleTypeWJ :
			me.videourl.setVisible(false);
			me.showtype.setVisible(false);
			me.overdue.setVisible(false);
			me.typename.setValue('文件');
			break;
		}
		me.type.setValue(type);
	}
});