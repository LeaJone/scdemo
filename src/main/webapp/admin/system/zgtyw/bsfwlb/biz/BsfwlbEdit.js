﻿/**
 * 办事服务类别编辑界面
 * @author lumingbao
 */
Ext.define('system.zgtyw.bsfwlb.biz.BsfwlbEdit', {
	extend : 'Ext.window.Window',
	requires : ['system.widget.FsdTreeComboBox'],
	grid : null,
	oper : Ext.create('system.zgtyw.bsfwlb.biz.BsfwlbOper'),
	initComponent:function(){
	    var me = this;	    
	   
	    var gjid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '根级ID',
	    	name: 'baseid',
	    	value: 'FSDQDFLB'
	    });
	    
	    var sslmname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属栏目名称',
	    	name: 'parentname'
	    });
	    var sslm = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属栏目',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 360,
	    	rootText : '清单类别',
	    	rootId : 'FSDQDFLB',
	        name : 'parentid',
	        baseUrl:'z_qdflb/load_AsyncQdflbTree.do',//访问路劲
	        allowBlank: false,
	        blankText: '请选择所属栏目',
	        hiddenName : 'parentname'//隐藏域Name
	    });
	    
	    var title = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '标题',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 360,
	    	name: 'title',
	    	maxLength: 20,
	    	allowBlank: false
	    });
	    
	    var sort = Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '序号',
	    	labelAlign:'right',
	    	labelWidth:60,
	    	width : 360,
	    	name: 'sort',
	    	maxLength: 3,
	    	minValue: 1,
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank: false,
	    	blankText: '请输入序号',
	    	maxLengthText: '序号不能超过3个字符'
	    });
	    
	    var imageurl1 = Ext.create('system.widget.FsdTextFileManage', {
	    	width : 360,
	    	zName : 'imageurl1',
	    	zFieldLabel : '标题图片',
	    	zLabelWidth : 60,
	    	zIsShowButton : true,
	    	zIsGetButton : false,
	    	zIsUpButton : true,
	    	zContentObj : me.content,
	    	zFileUpPath : me.oper.util.getImagePath()
	    });
	    
	    var aurl = Ext.create('system.widget.FsdTextCheckbox',{
	    	zNameCheckbox : 'isaurl',
	    	zFieldLabel : '外链接',
	    	zLabelAlign : 'right',
	    	labelWidth:60,
	    	zInputValue : 'true',
	    	zNameText : 'aurl',
	    	width : 360,
	        zMaxLength : 200
	    });
	    
	    var beizhu = new Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 360,
	    	name: 'remark',
	    	maxLength: 50,
	    	maxLengthText: '备注不能超过50个字符'
	    });
	    
	    me.on('show' , function(){
	    	sslm.setText(sslmname.getValue());
	    });
	    
	    var formpanel = new Ext.form.Panel({
	    	defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '10 0 0 10',
			items : [{
	                    name: "id",
	                    xtype: "hidden"
	                } , gjid, sslmname, sslm, title, sort, imageurl1, aurl, beizhu]
	    });
	    Ext.apply(this,{
	        width:400,
	        height:310,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[formpanel],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    zIsSpace : true,
			    handler : function() {
    				me.oper.formSubmit(formpanel , me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});