﻿/**
 *Z_QDFLB Column
 *@author CodeSystem
 *文件名     Extjs_Column_Z_qdflb
 *Columns名  column_z_qdflb
 *xtype: 'textfield', 'numbercolumn', 'booleancolumn', 'datecolumn'   format : '0.00'
 *align: 'left', 'center', 'right'
 */

Ext.define('column_z_qdflb', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            align : 'center',
            width : 30
        }
        ,
        {
            header : '根栏目ID',
            dataIndex : 'baseid',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '标题',
            dataIndex : 'title',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '序号',
            dataIndex : 'sort',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '是否外链接',
            dataIndex : 'isaurl',
            sortable : true,
            align : 'left',
            flex : 1,
            renderer :function(value, metadata, record, rowIndex, columnIndex, store){
            	if(value == 'true'){
            		return "<font>是</font>";
            	}else{
            		return "<font>否</font>";
            	}
  			}
        }
        ,
        {
            header : '新增时间',
            dataIndex : 'adddate',
            sortable : true,
            align : 'left',
            flex : 1,
			renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value);
			}
        }
        ,
        {
            header : '新增人名称',
            dataIndex : 'addemployeename',
            sortable : true,
            align : 'left',
            flex : 1
        }
    ]
});
