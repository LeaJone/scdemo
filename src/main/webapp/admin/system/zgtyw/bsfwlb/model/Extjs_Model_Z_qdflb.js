﻿/**
 *Z_QDFLB Model
 *@author CodeSystem
 *文件名     Extjs_Model_Z_qdflb
 *Model名    model_z_qdflb
 */

Ext.define('model_z_qdflb', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'baseid',
            type : 'string'
        }
        ,
        {
            name : 'title',
            type : 'string'
        }
        ,
        {
            name : 'sort',
            type : 'double'
        }
        ,
        {
            name : 'isaurl',
            type : 'string'
        }
        ,
        {
            name : 'adddate',
            type : 'string'
        }
        ,
        {
            name : 'addemployeename',
            type : 'string'
        }
    ]
});
