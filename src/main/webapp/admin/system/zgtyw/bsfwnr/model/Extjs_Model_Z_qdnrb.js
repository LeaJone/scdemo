﻿/**
 *Z_QDNRB Model
 *@author CodeSystem
 *文件名     Extjs_Model_Z_qdnrb
 *Model名    model_z_qdnrb
 */

Ext.define('model_z_qdnrb', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'bt',
            type : 'string'
        }
        ,
        {
            name : 'lbmc',
            type : 'string'
        }
        ,
        {
            name : 'cbjg',
            type : 'string'
        }
        ,
        {
            name : 'ssyj',
            type : 'string'
        }
        ,
        {
            name : 'sort',
            type : 'double'
        }
        ,
        {
            name : 'isaurl',
            type : 'string'
        }
        ,
        {
            name : 'isaurlstuff',
            type : 'string'
        }
        ,
        {
            name : 'isaurlguides',
            type : 'string'
        }
        ,
        {
            name : 'auditing',
            type : 'string'
        }
        ,
        {
            name : 'adddate',
            type : 'string'
        }
        ,
        {
            name : 'addemployeename',
            type : 'string'
        }
    ]
});
