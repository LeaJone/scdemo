﻿/**
 *Z_QDNRB Column
 *@author CodeSystem
 *文件名     Extjs_Column_Z_qdnrb
 *Columns名  column_z_qdnrb
 *xtype: 'textfield', 'numbercolumn', 'booleancolumn', 'datecolumn'   format : '0.00'
 *align: 'left', 'center', 'right'
 */

Ext.define('column_z_qdnrb', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            align : 'center',
            width : 30
        }
        ,
        {
            header : '标题',
            dataIndex : 'bt',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '类别名称',
            dataIndex : 'lbmc',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '承办机关',
            dataIndex : 'cbjg',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '实施依据',
            dataIndex : 'ssyj',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '序号',
            dataIndex : 'sort',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '外链接',
            dataIndex : 'isaurl',
            sortable : true,
            flex : 1,
			renderer :function(value ,metaData ,record ){
				if(value == 'true'){
					value="<font style='color:blue;'>是</font>";
				}else{
					value="<font style='color:gray;'>否</font>";
				}
				return value;
			}
        }
        ,
        {
            header : '资料链接',
            dataIndex : 'isaurlstuff',
            sortable : true,
            flex : 1,
			renderer :function(value ,metaData ,record ){
				if(value == 'true'){
					value="<font style='color:blue;'>是</font>";
				}else{
					value="<font style='color:gray;'>否</font>";
				}
				return value;
			}
        }
        ,
        {
            header : '指南链接',
            dataIndex : 'isaurlguides',
            sortable : true,
            flex : 1,
			renderer :function(value ,metaData ,record ){
				if(value == 'true'){
					value="<font style='color:blue;'>是</font>";
				}else{
					value="<font style='color:gray;'>否</font>";
				}
				return value;
			}
        }
        ,
        {
            header : '审核',
            dataIndex : 'auditing',
            sortable : true,
            align : 'left',
            flex : 1,
            renderer :function(value, metadata, record, rowIndex, columnIndex, store){
            	if(value == 'false'){
            		return "<font color='red'>未审核</font>";
            	}else if(value == 'true'){
            		return "<font color='green'>已审核</font>";
            	}else{
            		return "<font color='red'>未知</font>";
            	}
  			}
        }
        ,
        {
            header : '新增时间',
            dataIndex : 'adddate',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '新增人名称',
            dataIndex : 'addemployeename',
            sortable : true,
            align : 'left',
            flex : 1
        }
    ]
});
