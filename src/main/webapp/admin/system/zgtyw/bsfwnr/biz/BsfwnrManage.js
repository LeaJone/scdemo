﻿/**
 * 办事服务内容管理界面
 * @author lumingbao
 */
Ext.define('system.zgtyw.bsfwnr.biz.BsfwnrManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*', 
	             'system.zgtyw.bsfwnr.model.Extjs_Model_Z_qdnrb',
	             'system.zgtyw.bsfwnr.model.Extjs_Column_Z_qdnrb'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.zgtyw.bsfwnr.biz.BsfwnrOper'),
    
	createTable : function() {
	    var me = this;
	    
	    var queryText = Ext.create("Ext.form.TextField", {
	    	name : 'queryParam',
			emptyText : '请输入标题内容',
			width : 130,
			enableKeyEvents : true,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
	    });
	    
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_qdnrb', // 显示列
			model : 'model_z_qdnrb',
			baseUrl : 'z_qdnrb/load_pagedata.do',
			pageSize : 30,
			border : false,
			tbar : [{
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
				text : "添加",
				iconCls : 'page_addIcon',
				handler : function() {
					me.oper.add(grid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
				text : "修改",
				iconCls : 'page_editIcon',
				handler : function() {
					me.oper.editor(grid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'dwsc',
				text : "删除",
				iconCls : 'page_deleteIcon',
				handler : function() {
				    me.oper.del(grid);
				}
			}, '-', {
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
				text : "审核通过",
				iconCls : 'page_deleteIcon',
				handler : function(button) {
				    me.oper.isEnabled(grid, true);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
				text : "取消审核",
				iconCls : 'page_deleteIcon',
				handler : function(button) {
				    me.oper.isEnabled(grid, false);
				}
			}, '->', queryText, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var title =  queryText.getValue();
			var pram = {title : title};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});

		return grid;
	}
});