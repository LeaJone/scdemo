﻿/**
 *办事服务内容编辑界面
 * @author lumingbao
 */
Ext.define('system.zgtyw.bsfwnr.biz.BsfwnrEdit', {
	extend : 'Ext.window.Window',
	requires : ['system.widget.FsdTreeComboBox'],
	grid : null,
	oper : Ext.create('system.zgtyw.bsfwnr.biz.BsfwnrOper'),
	initComponent:function(){
	    var me = this;	    
	   
	    var lbmc = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属类别名称',
	    	name: 'lbmc'
	    });
	    var lbid = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属类别',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	rootText : '清单类别',
	    	rootId : 'FSDQDFLB',
	        name : 'lbid',
	        baseUrl:'z_qdflb/load_AsyncQdflbTree.do',//访问路径
	        allowBlank: false,
	        blankText: '请选择所属类别',
	        hiddenName : 'lbmc'//隐藏域Name
	    });
	    
//	    var parentid = Ext.create('system.widget.FsdTextField',{
//	    	fieldLabel: '上级内容',
//	    	labelAlign:'right',
//	    	labelWidth:80,
//	        width : 340,
//	    	name: 'parentid',
//	    	maxLength: 32,
//	    	allowBlank: false
//	    });
	    
	    var bt = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '标题',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'bt',
	    	maxLength: 400,
	    	allowBlank: false
	    });
	    
	    var cbjg = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '承办机关',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'cbjg',
	    	maxLength: 100,
	    });
	    
	    var ssyj = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '实施依据',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'ssyj',
	    	maxLength: 1000,
	    });
	    
	    var sszt = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '实施主体',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'sszt',
	    	maxLength: 100,
	    });
	    
	    var zrsx = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '责任事项',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'zrsx',
	    	maxLength: 2000,
	    });
	    
	    var zzqk = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '追责情形',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'zzqk',
	    	maxLength: 2000,
	    });
	    
	    var aurl = Ext.create('system.widget.FsdTextCheckbox',{
	    	zNameCheckbox : 'isaurl',
	    	zFieldLabel : '外链接',
	    	zLabelAlign : 'right',
	    	zLabelWidth : 80,
	    	zInputValue : 'true',
	    	zNameText : 'aurl',
	        width : 340,
	        zMaxLength : 100
	    });
	    
	    var aurlstuff = Ext.create('system.widget.FsdTextCheckbox',{
	    	zNameCheckbox : 'isaurlstuff',
	    	zFieldLabel : '资料链接',
	    	zLabelAlign : 'right',
	    	zLabelWidth : 80,
	    	zInputValue : 'true',
	    	zNameText : 'aurlstuff',
	        width : 340,
	        zMaxLength : 100
	    });
	    
	    var aurlguides = Ext.create('system.widget.FsdTextCheckbox',{
	    	zNameCheckbox : 'isaurlguides',
	    	zFieldLabel : '指南链接',
	    	zLabelAlign : 'right',
	    	zLabelWidth : 80,
	    	zInputValue : 'true',
	    	zNameText : 'aurlguides',
	        width : 340,
	        zMaxLength : 100
	    });
	    
	    var bz = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'bz',
	    	maxLength: 500,
	    });
	    
	    var sort = Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '序号',
	    	labelAlign:'right',
	    	labelWidth:80,
	    	width : 340,
	    	name: 'sort',
	    	maxLength: 3,
	    	minValue: 1,
	    	allowDecimals : false,//是否允许输入小数
	    	blankText: '请输入序号',
	    	maxLengthText: '序号不能超过3个字符'
	    });
	    
	    me.on('show' , function(){
	    	lbid.setText(lbmc.getValue());
	    });
	    
	    var formpanel = new Ext.form.Panel({
	    	defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '10 0 0 10',
			items : [{
	                    name: "id",
	                    xtype: "hidden"
	                } , 
	                lbid, lbmc, bt, cbjg, ssyj, sszt, zrsx, zzqk, aurl, aurlstuff, aurlguides, sort, bz ]
	    });
	    Ext.apply(this,{
	        width:400,
	        height:410,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[formpanel],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    zIsSpace : true,
			    handler : function() {
    				me.oper.formSubmit(formpanel , me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});