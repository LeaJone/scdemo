﻿/**
 * 电子期刊信息编辑界面
 * @author lw
 */
Ext.define('system.zgtyw.dzqkgl.biz.DzqkEdit', {
	extend : 'Ext.panel.Panel',
	
	articleOper : Ext.create('system.bcms.article.biz.ArticleOper'),
	oper : Ext.create('system.zgtyw.dzqkgl.biz.DzqkglOper'),
	
	grid : null,//管理表格
	
	lstLM: null,
	lstXG:null,
	
	loadObject : function(LM, XG){
		var me = this;
		me.lstLM = LM;
		me.lstXG = XG;
		me.subjectid.setText(me.subjectname.getValue());
		me.branchid.setText(me.branchname.getValue());
		me.treesubject.getStore().reload();
    	me.treesubjectxg.getStore().reload();
	},
	
	viewObject : function(){
		var me = this;
		me.down('button[name=btnsave]').setVisible(false);
		me.down('button[name=btnnew]').setVisible(false);
	},
	
	initComponent : function() {
		var me = this;

		me.idNo = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '编号',
	    	name : 'id'
	    });
		
		me.type = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '文章类型',
	    	value : 'wzld',
	    	name : 'type'
	    });
		
		me.typename = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '文章类型名称',
	    	value : '领导',
	    	name : 'typename'
	    });
		
		me.branchname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属机构名称',
	    	name: 'branchname'
	    });
		me.branchid = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属机构',
	    	labelAlign:'right',
	    	labelWidth:80,
	    	rootText : me.oper.util.getParams("company").name,
	    	rootId : me.oper.util.getParams("company").id,
			rootVisible: false,
	        width : 450,
	        name : 'branchid',
	        baseUrl:'A_Branch/load_AsyncBranchTree.do',//访问路劲
	        allowBlank: false,
	        hiddenName : 'branchname'//隐藏域Name
	    });
		
		me.subjectname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属栏目名称',
	    	name: 'subjectname'
	    });
	    me.subjectid = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属栏目',
	    	labelAlign:'right',
	    	labelWidth:80,
	    	rootText : '主栏目',
	    	rootId : 'FSDMAIN',
			rootVisible: false,
	        width : 450,
	        name : 'subjectid',
	        baseUrl:'B_Subject/load_AsyncSubjectTreeByPopdom.do',//访问路劲
	        allowBlank: false,
	        hiddenName : 'subjectname'//隐藏域Name
	    });
	    
	    me.titlecolor = Ext.create('system.widget.FsdTextColor', {
	        width : 450,
	    	zName : 'titlecolor', 
	    	zFieldLabel : '标题颜色', 
	    	zLabelWidth : 80
	    });

	    
	    me.checkboxgroup = new Ext.form.CheckboxGroup({
		    fieldLabel: '文章属性',
		    labelAlign:'right',
	    	labelWidth:80,
		    width: 450,
		    items: [
		        {
			    name : 'firstly',    
			    boxLabel: '置顶',
			    inputValue: 'true'
			    }, 
			    {
				name : 'slide',
				boxLabel: '幻灯',
				inputValue: 'true'
				}
			    ]
	    });

	    me.aurl = Ext.create('system.widget.FsdTextCheckbox',{
	    	zNameCheckbox : 'isaurl',
	    	zFieldLabel : '外链接',
	    	zLabelAlign : 'right',
	    	zLabelWidth : 80,
	    	zInputValue : 'true',
	    	zNameText : 'aurl',
	        width : 450,
	        zMaxLength : 200
	    });
	    
	    me.source = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '来源',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 450,
	    	name: 'source',
	    	value : me.oper.util.getParams("company").name,
	    	maxLength: 50,
	    	allowBlank: false
	    });
	    
	    me.adddate = Ext.create('system.widget.FsdDateTime', {
	    	fieldLabel: '新增时间',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 450,
	    	name: 'adddate',
	    });
	    
	    me.title = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '标题',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 920,
	    	name: 'title',
	    	maxLength: 50,
	    	allowBlank: false,
	    	colspan : 2
	    });
	    
	    me.subtitle = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '副标题',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 450,
	    	name: 'subtitle',
	    	maxLength: 50
	    });
	    
	    me.showtitle = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '显示标题',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 450,
	    	name: 'showtitle',
	    	allowBlank: false,
	    	maxLength: 50
	    });
	    
	    me.abstracts = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '简介',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 920,
	    	name: 'abstracts',
	    	maxLength: 250,
	    	colspan : 2
	    });
	    
	    me.imageurl1 = Ext.create('system.widget.FsdTextFileManage', {
	        width : 450,
	    	zName : 'imageurl1',
	    	zFieldLabel : '标题图片',
	    	zLabelWidth : 80,
	    	zIsShowButton : true,
	    	zIsGetButton : true,
	    	zIsUpButton : true,
	    	zIsReadOnly : false,
	    	zContentObj : me.content,
	    	zFileUpPath : me.oper.util.getImagePath()
	    });
	    
	    me.remark = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 450,
	    	name: 'remark',
	    	maxLength: 100,
	    	colspan : 2
	    });
	    
	    /**
		 * 栏目面板
		 */
		me.treesubject = Ext.create("system.widget.FsdTreePanel", {
			title: "所属栏目",
		    columnWidth : 0.5,
    		minWidth : 350,
    		border : true,
            rootVisible : false,//不显示根节点
            baseUrl :'B_Subject/get_QxSubjectCheck.do',
	        zTreeRoot : 'children',//一次性加载
            zTreeParams : {id : system.UtilStatic.subjectZTCode},
            tbar : [{
					xtype : "button",
					text : "展开",
					iconCls : 'expand-all',
					handler : function() {
						me.treesubject.expandAll();
					}
				},{
					xtype : "button",
					text : "收缩",
					iconCls : 'collapse-all',
					handler : function() {
						me.treesubject.collapseAll();
					}
				}]
    	});
	    
	    /**
		 * 相关栏目面板
		 */
		me.treesubjectxg = Ext.create("system.widget.FsdTreePanel", {
			title: "相关栏目",
		    columnWidth : 0.5,
    		minWidth : 350,
    		border : true,
            rootVisible : false,//不显示根节点
            baseUrl :'B_Subject/get_QxSubjectCheck.do',
	        zTreeRoot : 'children',//一次性加载
            zTreeParams : {id : system.UtilStatic.subjectXGCode},
            margin:'0 0 0 10',
            tbar : [{
					xtype : "button",
					text : "展开",
					iconCls : 'expand-all',
					handler : function() {
						me.treesubjectxg.expandAll();
					}
				},{
					xtype : "button",
					text : "收缩",
					iconCls : 'collapse-all',
					handler : function() {
						me.treesubjectxg.collapseAll();
					}
				}]
    	});
	    
	    
	    me.form = Ext.create('Ext.form.Panel',{
				border : false,
				padding : '10 0 30 20',
				layout: {
			        type: 'table', 
			        columns: 2 //每行有几列
			    },
				items : [me.idNo, me.type, me.typename, 
				         me.branchid, me.branchname, me.subjectid, me.subjectname, me.source, me.titlecolor, 
				         me.title, me.showtitle, me.subtitle, me.abstracts, 
				         me.imageurl1, me.aurl, me.checkboxgroup, me.adddate, me.remark,
    			{
					xtype : 'fieldset',
	                title : '关联信息',
                    layout : 'column',
                    border : true,
        	        width : 900,
        	    	colspan : 2,
	                items : [ me.treesubject, me.treesubjectxg ]
				}]
	    });
	    
	    me.treesubject.getStore().on('load', function(s) {
	    	me.treesubject.setChosseValues(me.lstLM);
		});
	    
	    me.treesubjectxg.getStore().on('load', function(s) {
	    	me.treesubjectxg.setChosseValues(me.lstXG);
		});
	    
	    me.on('boxready' , function(obj, width, height, eOpts){
        	me.subjectid.setText(me.subjectname.getValue());
        	me.branchid.setText(me.branchname.getValue());
	    });
	    
		Ext.apply(me, {
        	title: '电子期刊发布',
			autoScroll : true,
	        tbar : [{
				xtype : "fsdbutton",
				text : "内容提交",
	        	name : 'btnsave',
				iconCls : 'acceptIcon',
				handler : function(button) {
					me.oper.formSubmit(me, me.grid, me.treesubject, me.treesubjectxg);
				}
	        }, {
					xtype : "fsdbutton",
					text : "新增一条",
		        	name : 'btnnew',
					iconCls : 'tbar_synchronizeIcon',
					handler : function(button) {
						me.oper.add(me.grid);
					}
			}],
	        items :[me.form]
	    });
		me.callParent(arguments);
	}
});