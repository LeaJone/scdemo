﻿/**
 *Ldxxgl Column
 *@author CodeSystem
 *文件名     Extjs_Column_Ldxxgl
 *Columns名  column_dzqkgl
 *xtype: 'textfield', 'numbercolumn', 'booleancolumn', 'datecolumn'   format : '0.00'
 *align: 'left', 'center', 'right'
 */

Ext.define('column_dzqkgl', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            align : 'center',
            width : 30
        }
        ,
        {
            header : '标题',
            dataIndex : 'title',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '显示标题',
            dataIndex : 'showtitle',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '所属栏目',
            dataIndex : 'subjectname',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '所属部门',
            dataIndex : 'branchname',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '审核',
            dataIndex : 'auditing',
            sortable : true,
            align : 'center',
            flex : 1,
			renderer :function(value, metaData, record){
				if(value == 'true'){
					value="<font style='color:green;'>审</font>";
				}else{
					value="<font style='color:red;'>停</font>";
				}
				return value;
			}
        }
        ,
        {
            header : '置顶',
            dataIndex : 'firstly',
            sortable : true,
            align : 'center',
            flex : 1,
			renderer :function(value, metaData, record){
				if(value == 'true'){
					value="<font style='color:blue;'>是</font>";
				}else{
					value="<font style='color:gray;'>否</font>";
				}
				return value;
			}
        }
        ,
        {
            header : '操作人',
            dataIndex : 'addemployeename',
            sortable : true,
            align : 'center',
            flex : 1
        }
        ,{
            header : '新增时间',
            dataIndex : 'adddate',
            sortable : true,
            align : 'left',
            flex : 1,
			renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value);
			}
        }
    ]
});
