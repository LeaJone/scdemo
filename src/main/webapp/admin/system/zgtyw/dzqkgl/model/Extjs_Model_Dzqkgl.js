﻿/**
 *Ldxxgl Model
 *@author CodeSystem
 *文件名     Extjs_Model_Ldxxgl
 *Model名    model_dzqkgl
 */

Ext.define('model_dzqkgl', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'branchname',
            type : 'string'
        }
        ,
        {
            name : 'adddate',
            type : 'string'
        }
        ,
        {
            name : 'addemployeename',
            type : 'string'
        }
        ,
        {
            name : 'showtitle',
            type : 'string'
        }
        ,
        {
            name : 'subjectname',
            type : 'string'
        }
        ,
        {
            name : 'title',
            type : 'string'
        }
        ,
        {
            name : 'subtitle',
            type : 'string'
        }
        ,
        {
            name : 'firstly',
            type : 'string'
        }
        ,
        {
            name : 'auditing',
            type : 'string'
        }
    ]
});
