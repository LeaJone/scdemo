/**
 *b_zwftgl Model
 *@author CodeSystem
 *文件名     Extjs_Model_B_zwftgl
 *Model名    model_b_zwftgl
 */
Ext.define('model_b_zwftgl', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'subjectid',
            type : 'string'
        }
        ,
        {
            name : 'subjectname',
            type : 'string'
        }
        ,
        {
            name : 'type',
            type : 'string'
        }
        ,
        {
            name : 'typename',
            type : 'string'
        }
        ,
        {
            name : 'title',
            type : 'string'
        }
        ,
        {
            name : 'firstly',
            type : 'string'
        }
        ,
        {
            name : 'aurl',
            type : 'string'
        }
        ,
        {
            name : 'isaurl',
            type : 'string'
        }
        ,
        {
            name : 'showtype',
            type : 'string'
        }
        ,
        {
            name : 'showtypename',
            type : 'string'
        }
        ,
        {
            name : 'imageurl1',
            type : 'string'
        }
        ,
        {
            name : 'auditing',
            type : 'string'
        }
        ,
        {
            name : 'firstly',
            type : 'string'
        }
        ,
        {
            name : 'slide',
            type : 'string'
        }
        ,
        {
            name : 'ismobile',
            type : 'string'
        }
        ,
        {
            name : 'msubjectid',
            type : 'string'
        }
        ,
        {
            name : 'msubjectname',
            type : 'string'
        }
        ,
        {
            name : 'branchid',
            type : 'string'
        }
        ,
        {
            name : 'branchname',
            type : 'string'
        }
        ,
        {
            name : 'adddate',
            type : 'string'
        }
        ,
        {
            name : 'addemployeeid',
            type : 'string'
        }
        ,
        {
            name : 'addemployeename',
            type : 'string'
        }
    ]
});
