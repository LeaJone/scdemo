/**
 * 政务访谈管理页面
 * @author 
 */

Ext.define('system.zgtyw.zwftgl.biz.ZwftglManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*' , 
	             'system.zgtyw.zwftgl.model.Extjs_Column_B_zwftgl',
	             'system.zgtyw.zwftgl.model.Extjs_Model_B_zwftgl'],
	header : false,
	border : 0,
	layout : 'fit',
	
	oper : Ext.create('system.zgtyw.zwftgl.biz.ZwftglOper'),
	queryObj : null,
	
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	createContent : function(){
		var me = this;
		
		var queryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入访谈标题',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});
		
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_b_zwftgl', // 显示列
			model: 'model_b_zwftgl',
			baseUrl : 'z_zwftgl/load_pagedata.do',
			border : false,
			tbar : [{
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
				text : "添加",
				iconCls : 'page_addIcon',
				handler : function() {
					me.oper.addText(grid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
				text : "修改",
				iconCls : 'page_editIcon',
				handler : function() {
					me.oper.editor(grid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'dwsc',
				text : "删除",
				iconCls : 'page_deleteIcon',
				handler : function() {
				    me.oper.del(grid);
				}
			}, '-', {
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
				text : "审核通过",
				iconCls : 'tickIcon',
				handler : function(button) {
				    me.oper.isEnabled(grid, true);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
				text : "取消审核",
				iconCls : 'crossIcon',
				handler : function(button) {
				    me.oper.isEnabled(grid, false);
				}
			}, '-', {
				xtype : "fsdbutton",
				text : "文章置顶",
				iconCls : 'tickIcon',
				popedomCode : 'wzzd',
				handler : function(button) {
					me.oper.firstly(grid , 'true');
				}
			}, {
				xtype : "fsdbutton",
				text : "取消置顶",
				iconCls : 'crossIcon',
				popedomCode : 'wzzd',
				handler : function(button) {
					me.oper.firstly(grid , 'false');
				}
			}, '-', {
				xtype : "fsdbutton",
				text : "图片幻灯",
				iconCls : 'tickIcon',
				popedomCode : 'wzhd',
				handler : function(button) {
					me.oper.slide(grid , 'true');
				}
			}, {
				xtype : "fsdbutton",
				text : "取消幻灯",
				iconCls : 'crossIcon',
				popedomCode : 'wzhd',
				handler : function(button) {
					me.oper.slide(grid , 'false');
				}
			}, '->', queryText, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]		
		});
		
		grid.getStore().on('beforeload', function(s) {
			var title =  queryText.getValue();
			var pram = {title : title};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		return grid;
	}
});