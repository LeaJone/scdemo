/**
 * 政务访谈操作类
 * @author 
 */
Ext.define('system.zgtyw.zwftgl.biz.ZwftglOper', {
	
	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 添加访谈
     */
	addText : function(grid){
		var me = this;
		var model = Ext.create("system.zgtyw.zwftgl.biz.ZwftglEditPanel");
		var tabID = 'zwfttj';
		var myDate = new Date();
		tabID += myDate.getTime();
		model.grid = grid;
		me.util.addTab(tabID , "添加访谈" , model , "resources/admin/icon/vcard_add.png");
	},
	
	/**
     * 表单提交
     */
	submitZwftgl : function(formpanel, grid, treesubject, treesubjectxg){
        var me = this;
        if (formpanel.form.form.isValid()) {
        	var checkedNodes = treesubject.getChecked();
    		var sids = [];
    		for(var i=0 ; i < checkedNodes.length ; i++){
    			sids.push(checkedNodes[i].internalId)
    		}
    		checkedNodes = treesubjectxg.getChecked();
    		var sxgids = [];
    		for(var i=0 ; i < checkedNodes.length ; i++){
    			sxgids.push(checkedNodes[i].internalId)
    		}
    		var idMap = {};
    		idMap[system.UtilStatic.articleRelateLM] = sids;
    		idMap[system.UtilStatic.articleRelateXG] = sxgids;
           me.util.ExtFormSubmit('z_zwftgl/save_z_zwftgl_qx_z_zwftglbj.do' , formpanel.form.form , '正在提交数据,请稍候.....',
			function(form, action, respText){
            	if(grid != null){
            		grid.getStore().reload();//刷新管理界面的表格
            	}
            	formpanel.idNo.setValue(action.result.id);
				Ext.MessageBox.alert('提示', '保存成功！');
			}, null,
			{jsonData : Ext.encode(idMap)});
        }
     },
     
     /**
      * 删除
      */
     del : function(grid){
    	 var me = this;
 		 // 获取选中的行
 	     var data = grid.getSelectionModel().getSelection();
         if (data.length == 0) {
             Ext.MessageBox.alert('提示', '请先选中要删除的记录！');
         }else {
        	 Ext.MessageBox.show({
        		 title : '询问',
        		 msg : '您确定要删除吗?',
 			     width : 250,
 				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                     if (btn == 'yes') {
                    	 var dir = new Array();　
                         Ext.Array.each(data, function(items) {
                             var id = items.data.id;
                             dir.push(id);
             			});
             			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('B_Article/del_Article_qx_wzsc.do' , param ,
                        function(response, options){
                        	grid.getStore().reload();
                        }, null, me.form);
                     }
                 }
             });
         }
 	},
 	
 	/**
     * 修改
     */
	editor : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的记录！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个记录！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
    		var model = me.util.getTab("zwftbj");
    		if (model == null){
    			model = Ext.create("system.zgtyw.zwftgl.biz.ZwftglEditPanel");
    			model.grid = grid;
    		}
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(model.form, 'z_zwftgl/load_z_zwftglbyid.do', param, null, 
            function(response, options, respText, data){
            	model.loadObject(data[system.UtilStatic.articleRelateLM], data[system.UtilStatic.articleRelateXG]);
            	me.util.addTab("zwftbj" , "修改访谈" , model , "resources/admin/icon/vcard_edit.png");
            });
        }
	},
	
	/**
     * 查看
     */
	view : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要查看的记录！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个记录！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            var model = Ext.create("system.zgtyw.zwftgl.biz.ZwftglEditPanel");
            model.viewObject();
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(model.form, 'z_zwftgl/load_z_zwftglbyid.do', param, null, 
            function(response, options, respText, data){
            	model.loadObject(data[system.UtilStatic.articleRelateLM], data[system.UtilStatic.articleRelateXG]);
            	me.util.addTab(id + "view", "查看访谈", model, "resources/admin/icon/vcard_edit.png");
            });
        }
	},
	
	/**
	 * 加载栏目
	 */
	LoadSubjectTree : function(treepanel){
	    var me = this;
	    treepanel.getRootNode().removeAll();
	    me.util.ExtAjaxRequest('B_Subject/get_QxSubjectCheck.do', {jsonData : Ext.encode({id : system.UtilStatic.subjectZTCode})},
	    function(response, options, respText){
	    	if(respText.data != ""){
	    		treepanel.getRootNode().appendChild(respText.data);
	            treepanel.expandAll();
	            treepanel.checkChosseValues();
	    	}
	    }, null, me.form);
	},
	
	/**
	 * 加载相关栏目
	 */
	LoadSubjectXGTree : function(treepanel){
	    var me = this;
	    treepanel.getRootNode().removeAll();
	    me.util.ExtAjaxRequest('B_Subject/get_QxSubjectCheck.do', {jsonData : Ext.encode({id : system.UtilStatic.subjectXGCode})},
	    function(response, options, respText){
	    	if(respText.data != ""){
	    		treepanel.getRootNode().appendChild(respText.data);
	            treepanel.expandAll();
	            treepanel.checkChosseValues();
	    	}
	    }, null, me.form);
	},
	
	/**
     * 是否审核
     */
	isEnabled : function(grid, isQT){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
		        msg : '您确定要修改审核状态吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
					    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
			            });
             			var pram = {ids : dir, isenabled : isQT};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('z_zwftgl/enabled_qdnrb_qx.do' , param ,
                        function(response, options){
            				Ext.MessageBox.alert('提示', '审核状态修改成功！');
                        	grid.getStore().reload(); 
                        });
                    }
                }
            });
        }
	},
	
	/**
     * 置顶
     */
	firstly : function(grid , status){
 		var me = this;
		 // 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的记录！');
        }else {
       	 Ext.MessageBox.show({
       		 title : '询问',
       		 msg : '您确定要执行该操作吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes'){
	                   	var dir = new Array();　
	                    Ext.Array.each(data, function(items) {
	                            var id = items.data.id;
	                            dir.push(id);
	            		});
	                    var pram = {ids : dir , status : status};
	                    var param = {jsonData : Ext.encode(pram)};
	                    me.util.ExtAjaxRequest('B_Article/firstly_Article_qx_wzbj.do' , param ,
	                    function(response, options){
	                    	grid.getStore().reload();
	                    });
                    }
                }
            });
        }
	},
	
	/**
     * 幻灯
     */
	slide : function(grid , status){
 		var me = this;
		 // 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的记录！');
        }else {
       	 Ext.MessageBox.show({
       		 title : '询问',
       		 msg : '您确定要执行该操作吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes'){
	                   	var dir = new Array();　
	                    Ext.Array.each(data, function(items) {
	                            var id = items.data.id;
	                            dir.push(id);
	            		});
	                    var pram = {ids : dir , status : status};
	                    var param = {jsonData : Ext.encode(pram)};
	                    me.util.ExtAjaxRequest('B_Article/slide_Article_qx_wzbj.do' , param ,
	                    function(response, options){
	                    	grid.getStore().reload();
	                    });
                    }
                }
            });
        }
	}
});