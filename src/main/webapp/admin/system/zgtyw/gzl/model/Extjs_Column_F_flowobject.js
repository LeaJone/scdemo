﻿/**
 *f_flowobject Column
 *@author CodeSystem
 *文件名     Extjs_Column_F_flowobject
 *Columns名  column_f_flowobject
 *xtype: 'textfield', 'numbercolumn', 'booleancolumn', 'datecolumn'   format : '0.00'
 *align: 'left', 'center', 'right'
 */

Ext.define('column_f_flowobject', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            align : 'center',
            width : 30
        }
        ,
        {
            header : '流程名称',
            dataIndex : 'projectname',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '审批名称',
            dataIndex : 'nodename',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '类型',
            dataIndex : 'datatypename',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '编码',
            dataIndex : 'code',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '部门',
            dataIndex : 'branchname',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '提交人',
            dataIndex : 'employeename',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '信息描述',
            dataIndex : 'datadepict',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '提交时间',
            dataIndex : 'applydate',
            sortable : true,
            align : 'left',
            flex : 1,
			renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value);
			}
        }
        ,
        {
            header : '流程状态',
            dataIndex : 'flowstatusname',
            sortable : true,
            align : 'left',
            flex : 1
        }
    ]
});
