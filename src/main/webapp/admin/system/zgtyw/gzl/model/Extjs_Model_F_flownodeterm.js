﻿/**
 *f_flownodeterm Model
 *@author CodeSystem
 *文件名     Extjs_Model_F_flownodeterm
 *Model名    model_f_flownodeterm
 */

Ext.define('model_f_flownodeterm', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'operatorname',
            type : 'string'
        }
        ,
        {
            name : 'valuedepict',
            type : 'string'
        }
    ]
});
