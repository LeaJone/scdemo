﻿/**
 * 本人审批过的管理
 * @author lw
 */
Ext.define('system.zgtyw.gzl.biz.FlowObjectPassed', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.zgtyw.gzl.model.Extjs_Column_F_flowobject', 
	             'system.zgtyw.gzl.model.Extjs_Model_F_flowobject'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.zgtyw.gzl.biz.FlowObjectOper'),
    
	createTable : function() {
	    var me = this;

		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_f_flowobject',
			model : 'model_f_flowobject',
			baseUrl : 'f_flowobject/load_pagePassed.do',
			border : false,
			tbar : [{
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid);
				}
			}, '->', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
//			var name =  queryText.getValue();
//			var pram = {employeename : name};
//	        var params = s.getProxy().extraParams;  
//	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});

		return grid;
	}
});