﻿/**
 * 流程对象管理
 * @author lw
 */
Ext.define('system.zgtyw.gzl.biz.FlowObjectManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.zgtyw.gzl.model.Extjs_Column_F_flowobject', 
	             'system.zgtyw.gzl.model.Extjs_Model_F_flowobject'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.zgtyw.gzl.biz.FlowObjectOper'),
    
	createTable : function() {
	    var me = this;

		/**
		 * 查询文本框
		 */
		var queryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '提交人姓名',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});
		
		var changingImage = Ext.create('Ext.Img', {
		    src: 'http://www.gsdlr.gov.cn:8306/uploadfiles/FSDCOMPANY/system/tzxx.png',
			handler : function() {
				me.oper.view(grid);
			}
		});

		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_f_flowobject', // 显示列
			model : 'model_f_flowobject',
			baseUrl : 'f_flowobject/load_pageManage.do',
			border : false,
			tbar : [{
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid);
				}
			}, '->', queryText, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  queryText.getValue();
			var pram = {employeename : name};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});

		return grid;
	}
});