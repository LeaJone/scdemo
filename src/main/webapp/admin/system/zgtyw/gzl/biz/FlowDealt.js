﻿/**
 * 工作流处理界面
 * @author lw
 */
Ext.define('system.zgtyw.gzl.biz.FlowDealt', {
	extend : 'Ext.window.Window',
	grid : null,//管理表格
	win : null,//记录窗体
	type : null,//提交类型
	record : null,//流程记录对象
	dealt : null,//处理记录对象
	oper : Ext.create('system.zgtyw.gzl.biz.FlowOper'),
	
	
	initComponent:function(){
	    var me = this;
	    me.record = me.win.record;
	    me. = me.win.dealt;

	    var depict = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel : '处理意见',
	    	labelAlign :'right',
	    	labelWidth : 70,
	        width : 340,
	        height : 80,
	    	name: 'depict'
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [depict]
        });
	    
	    Ext.apply(this,{
	        width:400,
	        height:185,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[ form ],
	        buttons : [{
			    text : '提交',
			    iconCls : 'acceptIcon',
			    handler : function() {
			    	me.depictTxt = depict.getValue();
			    	switch (me.type) {
						case 'f_fdty'://同意
							//选择审批节点
							if (me.record.nexttype == 'f_fnxzp'
								|| me.record.nexttype == 'f_fnzxy'
								|| me.record.nexttype == 'f_fnzxd'){
						        me.oper.dealtChooseShow(me);
							}else{
						        var pram = {results : me.type, dealtid : me.dealt.id, depict : me.depictTxt};
						        me.oper.dealtSubmit(pram, me.grid, me);
							}
							break;
						case 'f_fdjj'://拒绝
						case 'f_fdth'://退回
					        var pram = {results : me.type, dealtid : me.dealt.id, depict : me.depictTxt};
					        me.oper.dealtSubmit(pram, me.grid, me);
							break;
						case 'f_fdzj'://转交
					        me.oper.dealtDevolveShow(me);
							break;
						default:
							Ext.MessageBox.alert('提示', '未知的处理类型！');
							return;
					}
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});