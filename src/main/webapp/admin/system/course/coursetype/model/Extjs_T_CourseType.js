﻿/**
 * t_coursetype Model
 * @author CodeSystem
 * 文件名     Extjs_T_CourseType
 * Model名    model_t_coursetype
 * Columns名  column_t_coursetype
 */
Ext.define('model_t_coursetype', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'f_title',
            type : 'string'
        }
        ,
        {
            name : 'f_state',
            type : 'string'
        }
        ,
        {
            name : 'f_order',
            type : 'Long'
        }
        ,
        {
            name : 'f_imageurl',
            type : 'string'
        }
    ]
});


/**
 * t_coursetype Columns
 */
Ext.define('column_t_coursetype', {
    columns: [
    	{
    		xtype : 'rownumberer',
    		text : 'NO',
    		sortable : true,
    		align : 'center',
    		flex : 1
    	}
    	,
    	{
            header : '名称',
            dataIndex : 'f_title',
            align : 'center',
            flex : 2
        }
        ,
        {
            header : '图片地址',
            dataIndex : 'f_imageurl',
            sortable : true,
            align : 'center',
            flex : 2
        }
        ,
        {
            header : '状态',
            dataIndex : 'f_state',
            align : 'center',
            flex : 1,
        	renderer : function(value, metaData, record) {
    			if (value == "ztqy") {
    				value = "<font style='color:green;'>启用</font>";
    			} else {
    				value = "<font style='color:red;'>停用</font>";
    			}
    			return value;
    		}
        }
        ,
        {
            header : '顺序号',
            dataIndex : 'f_order',
            sortable : true,
            align : 'center',
            flex : 1
        }
    ]
});