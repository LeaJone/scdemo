﻿/**
 * 课程分类添加界面
 */
Ext.define('system.course.coursetype.biz.CourseTypeEdit', {
	extend : 'Ext.window.Window',
	requires : [ 'system.course.coursetype.model.Extjs_T_CourseType'],
	grid : null,
	treepanel : null,
	oper : Ext.create('system.course.coursetype.biz.CourseTypeOper'),
	initComponent:function(){
	    var me = this;
	     
	    var f_parentid = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属分类',
	    	labelAlign : 'right',
	    	labelWidth : 120,
	    	rootText : '课程分类',
	    	rootId : 'FSDCOURSETYPE',
	        width : 360,
	        name : 'f_parentid',
	        baseUrl : 't_coursetype/load_AsyncAreaTree.do',//访问路劲	        
	        allowBlank : false,
	    });
	  
	    var f_title = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '当前分类名称',
	    	labelAlign:'right',
	    	labelWidth:120,
	        width : 360,
	    	name: 'f_title',
	    	maxLength: 200,
	    	allowBlank: false,
	    	maxLengthText : '课程分类名称不能超过200个字符'
	    });
	     
	    var f_imageurl = Ext.create('system.widget.FsdTextFileManage', {
	        width : 360,
	    	zName : 'f_imageurl',
	    	zFieldLabel : '课程预览图',
	    	zLabelWidth : 120,
	    	zIsShowButton : true, 
	    	zIsUpButton : true,
	    	zFileType : 'image',
	    	zFileAutoName : false,
	    	zFileUpPath : me.oper.util.getSystemPath(),
	    	zManageType : 'upload'
	    });
	    
	    var f_order = Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '排序编号',
	    	labelAlign:'right',
	    	labelWidth:120,
	        width : 360,
	    	name: 'f_order',
	    	maxLength: 3,
	    	minValue: 0,
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank: false
	    });
	        
	    
	    Ext.apply(this,{
	        width:450,
	        height:200,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[{
				xtype : 'form',
				id : 'coursetypeAddForm',
				defaultType : 'textfield',
				header : false,// 是否显示标题栏
				border : 0,
				padding : '20 0 0 20',
				items : [{
                    name: "id",
                    xtype: "hidden"
                } ,f_parentid, f_title, f_imageurl, f_order ]
	        }],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'jgbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.formSubmit(Ext.getCmp('coursetypeAddForm') , me.grid , me.treepanel);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});