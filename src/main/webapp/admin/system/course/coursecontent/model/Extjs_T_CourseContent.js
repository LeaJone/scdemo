﻿/**
 * t_coursecontent  Model
 * @author  CodeSystem
 * 文件名     Extjs_T_CourseContent
 * Model名    model_t_coursecontent
 * Columns名  column_t_coursecontent
 */
Ext.define('model_t_coursecontent', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'f_title',
            type : 'string'
        }
        ,
        {
            name : 'f_fileurl',
            type : 'string'
        }
        ,
        {
            name : 'f_videourl',
            type : 'string'
        }
        ,
        {
            name : 'f_accessoryurl',
            type : 'string'
        }
        ,
        {
            name : 'f_state',
            type : 'string'
        }
        ,
        {
            name : 'f_order',
            type : 'Long'
        }
    ]
});


/**
 *t_coursecontent Columns
 *@author CodeSystem
 */
Ext.define('column_t_coursecontent', {
    columns: [
    	{
    		xtype : 'rownumberer',
    		text : 'NO',
    		sortable : true,
    		align : 'center',
    		flex : 0.3
    	}
    	,
        {
            header : '课时名称',
            dataIndex : 'f_title',
            align : 'center',
    		flex : 1.5
        }
        ,
        {
            header : '文件地址',
            dataIndex : 'f_fileurl',
            align : 'center',
    		flex : 1
        }
        ,
        {
            header : '视频地址',
            dataIndex : 'f_videourl',
            align : 'center',
    		flex : 1
        }
        ,
        {
            header : '文件地址',
            dataIndex : 'f_accessoryurl',
            align : 'center',
    		flex : 1
        }
        ,
        {
            header : '状态',
            dataIndex : 'f_state',
            align : 'center',
            flex : 0.3,
        	renderer : function(value, metaData, record) {
    			if (value == "ztqy") {
    				value = "<font style='color:green;'>启用</font>";
    			} else {
    				value = "<font style='color:red;'>停用</font>";
    			}
    			return value;
    		}
        }
        ,
        {
            header : '排序编号',
            dataIndex : 'f_order',
            sortable : true ,
            align : 'center',
            flex : 0.4
        }
    ]
});

/**
 * t_coursecontent  Model
 * @author  CodeSystem
 * 文件名     Extjs_T_CourseContent
 * Model名    model_t_coursecontent
 * Columns名  column_t_coursecontent
 */
Ext.define('model_t_coursecontent2', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'f_title',
            type : 'string'
        }
        ,
        {
            name : 't_courseinfoname',
            type : 'string'
        }
        ,
        {
            name : 'f_state',
            type : 'string'
        }
        ,
        {
            name : 'f_order',
            type : 'Long'
        }
    ]
});


/**
 *t_coursecontent Columns
 *@author CodeSystem
 */
Ext.define('column_t_coursecontent2', {
    columns: [
        {
            header : '课时名称',
            dataIndex : 'f_title',
            align : 'center',
    		flex : 1
        }
        ,
        {
            header : '课程名称',
            dataIndex : 't_courseinfoname',
            align : 'center',
    		flex : 1
        }
        ,
        {
            header : '状态',
            dataIndex : 'f_state',
            align : 'center',
            flex : 0.5,
        	renderer : function(value, metaData, record) {
    			if (value == "ztqy") {
    				value = "<font style='color:green;'>启用</font>";
    			} else {
    				value = "<font style='color:red;'>停用</font>";
    			}
    			return value;
    		}
        }
        ,
        {
            header : '排序编号',
            dataIndex : 'f_order',
            sortable : true ,
            align : 'center',
            flex : 0.5
        }
    ]
});



/**
 * t_coursecontent  Model
 * @author  CodeSystem
 * 文件名     Extjs_T_CourseContent
 * Model名    model_t_coursecontent1
 * Columns名  column_t_coursecontent1
 */
Ext.define('model_t_coursecontent1', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'f_title',
            type : 'string'
        }
    ]
});


/**
 *t_coursecontent Columns
 *@author CodeSystem
 */
Ext.define('column_t_coursecontent1', {
    columns: [
        {
            header : '课时名称',
            dataIndex : 'f_title',
            align : 'center',
    		flex : 1
        }
      
    ]
});



