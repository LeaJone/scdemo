﻿/**
 * 课程编辑界面
 */
Ext.define('system.course.coursecontent.biz.CourseContentEdit', {
	extend : 'Ext.window.Window',
	requires : ['system.course.course.model.Extjs_T_Course',
		        'system.course.coursecontent.model.Extjs_T_CourseContent'],
	grid : null,
	oper : Ext.create('system.course.coursecontent.biz.CourseContentOper'),
	initComponent:function(){
	    var me = this;	  
	      
	    var f_title = new Ext.create('system.widget.FsdTextFieldZJF',{
	    	fieldLabel : '课时名称',
	    	labelAlign :'right',
	    	labelWidth : 80,
	        width : 440,
	    	name : 'f_title',
	    	maxLength : 200,
	    	allowBlank : false,
	    	blankText : '请输入课时名称',
	    	maxLengthText : '课时名称不能超过200个字符'
	    });
	    
	    me.t_courseinfoname = Ext.create('system.widget.FsdTextGrid', {
	    	zFieldLabel : '所属课程', 
	    	zLabelWidth : 80,
	    	width : 440,
		    zName : 't_courseinfoname',
		    zColumn : 'column_t_course1', // 显示列
		    zModel : 'model_t_course1',
		    zBaseUrl : 't_course/load_pagedata.do',
		    zGridType : 'list',
			zGridWidth : 700,//弹出表格宽度
			zGridHeight : 600,//弹出表格高度
		    zIsText1 : true,
		    zTxtLabel1 : '课程名称',
		    zAllowBlank : false,
		    zFunChoose : function(data){
		    	me.t_courseinfoid.setValue(data.id);
		    	me.t_courseinfoname.setValue(data.f_title);
		    },
		    zFunQuery : function(txt1, txt2, txt3, txt4){
		    	return {name : txt1};
		    }
	    });
	    me.t_courseinfoid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属课程',
	    	name: 't_courseinfoid'
	    });
	    
	    var f_fileurl = Ext.create('system.widget.FsdTextFileManage', {
	    	width : 440,
	    	zName : 'f_fileurl',
	    	zFieldLabel : '文件地址',
	    	zLabelWidth : 80,
	    	zIsShowButton : true, 
	    	zIsUpButton : true,
	    	zFileType : 'image',
	    	zFileAutoName : false,
	    	zFileUpPath : me.oper.util.getSystemPath(),
	    	zManageType : 'upload'
	    });
	    
	    var f_videourl = Ext.create('system.widget.FsdTextFileManage', {
	    	width : 440,
	    	zName : 'f_videourl',
	    	zFieldLabel : '视频地址',
	    	zLabelWidth : 80,
	    	zIsShowButton : true, 
	    	zIsUpButton : true,
	    	zFileType : 'image',
	    	zFileAutoName : false,
	    	zFileUpPath : me.oper.util.getSystemPath(),
	    	zManageType : 'upload'
	    });
	    
	    var f_accessoryurl = Ext.create('system.widget.FsdTextFileManage', {
	    	width : 440,
	    	zName : 'f_accessoryurl',
	    	zFieldLabel : '附件地址',
	    	zLabelWidth : 80,
	    	zIsShowButton : true, 
	    	zIsUpButton : true,
	    	zFileType : 'image',
	    	zFileAutoName : false,
	    	zFileUpPath : me.oper.util.getSystemPath(),
	    	zManageType : 'upload'
	    });
	    
	    var f_order = new Ext.create('system.widget.FsdNumber',{
	    	fieldLabel : '排序编号',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 440,
	    	name : 'f_order',
	    	maxLength : 3,
	    	minValue : 0,
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank : false,
	    	blankText : '请输入排序编号',
	    	maxLengthText : '排序编号不能超过3个字符'
	    });
	    
	    var f_content = Ext.create('system.widget.FsdUeditor',{
	    	fieldLabel : '课时内容',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 920,
	        height : 380,
	    	name : 'f_content',
	    	padding : '0 0 20 0'
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '0 20 0 20',
			items : [{
                name: "id",
                xtype: "hidden"
            }
			, 
            {
                layout : 'column',
                border : false,
				padding : '10 0 0 0',
    	        width : 900,
                items:[{
                    columnWidth : 0.5,
                    border:false,
                    items:[ f_title, f_fileurl, f_accessoryurl]
                }, {
                    columnWidth : 0.5,
                    border:false,
                    items:[ me.t_courseinfoname, me.t_courseinfoid, f_videourl, f_order]
                }]
			}, f_content]
        });
	    
	    Ext.apply(this,{
	        width:1000,
	        height:600,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items : [{
				xtype : 'form',
				id : 'coursecontentAddForm',
				defaultType : 'textfield',
				header : false,// 是否显示标题栏
				border : 0,
				padding : '20 0 0 20',
				items : [form]
	        }],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.formSubmit(Ext.getCmp('coursecontentAddForm') , me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});