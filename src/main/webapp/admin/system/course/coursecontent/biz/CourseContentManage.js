﻿/**
 * 课时管理界面
 */
Ext.define('system.course.coursecontent.biz.CourseContentManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : ['system.course.course.model.Extjs_T_Course', 
		        'system.course.coursecontent.model.Extjs_T_CourseContent'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.course.coursecontent.biz.CourseContentOper'),
    
	createTable : function() {
	    var me = this;
	    var queryid = me.oper.util.getParams("company").id;
        var queryid1 = me.oper.util.getParams("company").id;

        /**
         * 左边的课程分类树面板
         */
        var treepanel = Ext.create("system.widget.FsdTreePanel", {
            icon : 'resources/admin/icon/chart_organisation.png',
            baseUrl : 't_coursetype/load_AsyncAreaTree.do',
            title : '课程分类',
            rootText : '课程分类',
            rootId : 'FSDCOURSETYPE' ,
            region : 'west', // 设置方位
            collapsible : true,//折叠
            region: 'west',
            width: 250,
            split: true
        });

        treepanel.on('itemclick', function(view,record,item,index,e){
            queryid = record.raw.id;
            zgrid.getStore().reload();
        });
	  
	    /**
	     * 中间的课程信息
	     */
	    var zgrid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_t_course1', // 显示列
			model: 'model_t_course1',
			baseUrl : 't_course/load_pagedata.do',
			selType : 'rowmodel',
			tbar : [
			    new Ext.form.TextField( {
				id : 'zzquerycourse1Name',
				name : 'queryParam',
				emptyText : '请输入课程名称',
				enableKeyEvents : true,
				listeners : {
					specialkey : function(field, e) {
						if (e.getKey() == Ext.EventObject.ENTER) {
							zgrid.getStore().reload();
						}
					}
				},
				width : 130
			}), {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					zgrid.getStore().reload();
				}
			}]
			
		});
	    zgrid.getStore().on('beforeload', function(s) {
			var name =  Ext.getCmp('zzquerycourse1Name').getValue();
            var pram = {fid : queryid ,name : name};
            var params = s.getProxy().extraParams;
            Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
        zgrid.on('itemclick', function(view,record,item,index,e){
	    	me.t_courseinfoid = record.raw.id;
            queryid1 = me.t_courseinfoid;
            ksgrid.getStore().reload();
		});
	    
		// 右边的课时信息
		var ksgrid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_t_coursecontent', // 显示列
			model : 'model_t_coursecontent',
			baseUrl : 't_coursecontent/load_pagedata.do',
			tbar : [{
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
				text : "添加",
				iconCls : 'page_addIcon',
				handler : function() {
					me.oper.add(ksgrid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
				text : "修改",
				iconCls : 'page_editIcon',
				handler : function() {
					me.oper.editor(ksgrid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'dwsc',
				text : "删除",
				iconCls : 'page_deleteIcon',
				handler : function() {
				    me.oper.del(ksgrid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(ksgrid);
				}
			}, '-', {
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
				text : "启用",
				iconCls : 'tickIcon',
				handler : function(button) {
				    me.oper.isEnabled(ksgrid, true);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
				text : "停用",
				iconCls : 'crossIcon',
				handler : function(button) {
				    me.oper.isEnabled(ksgrid, false);
				}
			}, '->', new Ext.form.TextField( {
				id : 'querycoursecontentName',
				name : 'queryParam',
				emptyText : '请输入课时名称',
				enableKeyEvents : true,
				listeners : {
					specialkey : function(field, e) {
						if (e.getKey() == Ext.EventObject.ENTER) {
                            ksgrid.getStore().reload();
						}
					}
				},
				width : 130
			}), {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
                    ksgrid.getStore().reload();
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
                    ksgrid.getStore().reload();
				}
			}]			
		});
        ksgrid.getStore().on('beforeload', function(s) {
			var name =  Ext.getCmp('querycoursecontentName').getValue();
			var pram = {fid : queryid1 ,name : name};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		var centerpanel = Ext.create("Ext.panel.Panel", {
			frame:true,
			split: true,
            region: 'center',
			layout: 'fit',
			width: 800,
			title: '课程信息',
			items: [zgrid]
		});

        var eastpanel = Ext.create("Ext.panel.Panel", {
            frame:true,
            split: true,
            region: 'east',
            layout: 'fit',
            width: 800,
            title: '课时信息',
            items: [ksgrid]
        });

		var page1_jExtPanel1_obj = new Ext.panel.Panel({
			bodyPadding: 3,
			header : false,
			border : 0,
			split: true,
			layout: 'border',
			items: [treepanel, centerpanel, eastpanel]
		});
		
		return page1_jExtPanel1_obj;
	}
});