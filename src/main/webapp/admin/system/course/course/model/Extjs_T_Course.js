﻿/**
 * t_course Model
 * @author CodeSystem
 * 文件名     Extjs_T_Course
 * Model名    model_t_course
 * Columns名  column_t_course
 */

Ext.define('model_t_course', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'f_title',
            type : 'string'
        }
        ,
        {
            name : 'f_imageurl',
            type : 'string'
        }
        ,
        {
            name : 'f_recommend',
            type : 'Long'
        }
        ,
        {
            name : 'f_required',
            type : 'Long'
        }
        ,
        {
            name : 'f_expirationtime',
            type : 'string'
        }
        ,
        {
            name : 'f_score',
            type : 'string'
        }
        ,
        {
            name : 'f_flow',
            type : 'Long'
        }
        ,
        {
            name : 'f_state',
            type : 'string'
        }
        ,
        {
            name : 'f_order',
            type : 'Long'
        }
    ]
});


/**
 *a_company Columns
 *@author CodeSystem
 */
Ext.define('column_t_course', {
    columns: [
    	{
    		xtype : 'rownumberer',
    		text : 'NO',
    		sortable : true,
    		align : 'center',
    		flex : 0.3
    	}
    	,
        {
            header : '课程名称',
            dataIndex : 'f_title',
            align : 'center',
    		flex : 1.2
        }
        ,
        {
            header : '图片地址',
            dataIndex : 'f_imageurl',
            align : 'center',
    		flex : 1
        }
        ,
        {
            header : '是否推荐',
            dataIndex : 'f_recommend',
            align : 'center',
    		flex : 0.5,
    		renderer : function(value, metaData, record) {
    			if (value == "0") {
    				value = "<font style='color:green;'>推荐</font>";
    			} else {
    				value = "<font style='color:red;'>不推荐</font>";
    			}
    			return value;
    		}
        }
        ,
        {
            header : '是否必修',
            dataIndex : 'f_required',
            align : 'center',
    		flex : 0.5,
    		renderer : function(value, metaData, record) {
    			if (value == "0") {
    				value = "<font style='color:green;'>必修</font>";
    			} else {
    				value = "<font style='color:red;'>选修</font>";
    			}
    			return value;
    		}
        }
        ,
        {
            header : '到期时间',
            dataIndex : 'f_expirationtime',
            align : 'center',
    		flex : 1,
    		renderer :function(value, metaData, record){
    			return system.UtilStatic.formatDateTime(value);
    		}
        }
        ,
        {
            header : '课程分数',
            dataIndex : 'f_score',
            align : 'center',
    		flex : 0.5
        }
        ,
        {
            header : '课程流程',
            dataIndex : 'f_flow',
            align : 'center',
    		flex : 0.5,
    		renderer : function(value, metaData, record) {
    			if (value == "jjsx") {
    				value = "<font style='color:red;'>即将上线</font>";
    			} else if (value == "ydq") {
    				value = "<font style='color:red;'>已经到期</font>";
    			} else {
    				value = "<font style='color:green;'>正在学习中</font>";
    			}
    			return value;
    		}
        }
        ,
        {
            header : '状态',
            dataIndex : 'f_state',
            align : 'center',
            flex : 0.5,
        	renderer : function(value, metaData, record) {
    			if (value == "ztqy") {
    				value = "<font style='color:green;'>启用</font>";
    			} else {
    				value = "<font style='color:red;'>停用</font>";
    			}
    			return value;
    		}
        }
        ,
        {
            header : '排序编号',
            dataIndex : 'f_order',
            sortable : true ,
            align : 'center',
            flex : 0.5
        }
    ]
});


Ext.define('model_t_course1', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'f_title',
            type : 'string'
        }
        ,
        {
            name : 'f_coursetypename',
            type : 'string'
        }
        ,
        {
            name : 'f_state',
            type : 'string'
        }
        ,
        {
            name : 'f_order',
            type : 'Long'
        }
    ]
});


/**
 *a_company Columns
 *@author CodeSystem
 */
Ext.define('column_t_course1', {
    columns: [
        {
            header : '课程名称',
            dataIndex : 'f_title',
            align : 'center',
    		flex : 1
        }
        ,
        {
            header : '课程分类',
            dataIndex : 'f_coursetypename',
            align : 'center',
    		flex : 1 ,
    		renderer : function(value, metaData, record) {
                 
    			return value;
    		}
        }
        ,
        {
            header : '状态',
            dataIndex : 'f_state',
            align : 'center',
            flex : 0.5,
        	renderer : function(value, metaData, record) {
    			if (value == "ztqy") {
    				value = "<font style='color:green;'>启用</font>";
    			} else {
    				value = "<font style='color:red;'>停用</font>";
    			}
    			return value;
    		}
        }
        ,
        {
            header : '排序编号',
            dataIndex : 'f_order',
            sortable : true ,
            align : 'center',
            flex : 0.5
        }
    ]
});

Ext.define('model_t_course2', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'f_title',
            type : 'string'
        }
        ,
        {
            name : 'f_state',
            type : 'string'
        }
        ,
        {
            name : 'f_order',
            type : 'Long'
        }
    ]
});


/**
 *a_company Columns
 *@author CodeSystem
 */
Ext.define('column_t_course2', {
    columns: [
        {
            header : '课程名称',
            dataIndex : 'f_title',
            align : 'center',
    		flex : 1.9
        }
        ,
        {
            header : '状态',
            dataIndex : 'f_state',
            align : 'center',
            flex : 0.3,
        	renderer : function(value, metaData, record) {
    			if (value == "ztqy") {
    				value = "<font style='color:green;'>启用</font>";
    			} else {
    				value = "<font style='color:red;'>停用</font>";
    			}
    			return value;
    		}
        }
        ,
        {
            header : '排序编号',
            dataIndex : 'f_order',
            sortable : true ,
            align : 'center',
            flex : 0.8
        }
    ]
});
