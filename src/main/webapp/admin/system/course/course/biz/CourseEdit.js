﻿/**
 * 课程编辑界面
 */
Ext.define('system.course.course.biz.CourseEdit', {
	extend : 'Ext.window.Window',
	requires : ['system.course.course.model.Extjs_T_Course'],
	grid : null,
	oper : Ext.create('system.course.course.biz.CourseOper'),
	initComponent:function(){
	    var me = this;	  
	       
	    var f_coursetypeid = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属分类',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	    	rootText : '课程分类',
	    	rootId : 'FSDCOURSETYPE',
	        width : 340,
	        name : 'f_coursetypeid',
	        baseUrl : 't_coursetype/load_AsyncAreaTree.do',//访问路劲	        
	        allowBlank : false,
	        hiddenName : 'f_coursetypename'//隐藏域Name
	    });
	    
	    var f_coursetypename = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属分类名称',
	    	name: 'f_coursetypename'
	    });
	    
	    
	    var f_title = new Ext.create('system.widget.FsdTextFieldZJF',{
	    	fieldLabel : '课程名称',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 340,
	    	name : 'f_title',
	    	maxLength : 200,
	    	allowBlank : false,
	    	blankText : '请输入课程名称',
	    	maxLengthText : '课程名称不能超过200个字符'
	    });
	   
	    var f_imageurl = Ext.create('system.widget.FsdTextFileManage', {
	        width : 340,
	    	zName : 'f_imageurl',
	    	zFieldLabel : '课程预览图',
	    	zLabelWidth : 80,
	    	zIsShowButton : true, 
	    	zIsUpButton : true,
	    	zFileType : 'image',
	    	zFileAutoName : false,
	    	zFileUpPath : me.oper.util.getSystemPath(),
	    	zManageType : 'upload'
	    });
	         
	    var f_abstract = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel : '课程简介',
	    	labelAlign :'right',
	    	labelWidth : 80,
	        width : 340,
	        allowBlank : false,
	    	name : 'f_abstract',
	    	maxLength : 500,
	    	blankText : '请输入课程名称',
	    	maxLengthText : '课程名称不能超过500个字符'
	    });
	       
	    var f_required = Ext.create('system.widget.FsdComboBox',{
            fieldLabel : '是否必修',
	        labelAlign : 'right',
	    	labelWidth : 80,
	        width : 340,
	        allowBlank : false,
    	    name : 'f_required',//提交到后台的参数名
            store : [
                    [0, '是'],
                    [1, '否']
                ]
	    });
	    
	    var f_recommend = Ext.create('system.widget.FsdComboBox',{
            fieldLabel : '是否推荐',
	        labelAlign : 'right',
	    	labelWidth : 80,
	        width : 340,
	        allowBlank : false,
    	    name : 'f_recommend',//提交到后台的参数名
            store : [
                    [0, '是'],
                    [1, '否']
                ]
	    });
	    
	    var f_flow = Ext.create('system.widget.FsdComboBox',{
            fieldLabel : '课程流程',
	        labelAlign : 'right',
	    	labelWidth : 80,
	        width : 340,
	        allowBlank : false,
    	    name : 'f_flow',//提交到后台的参数名
            store : [
                    ['jjsx', '即将上线'],
                    ['zzxxz', '正在学习中'],
                    ['ydq', '已到期']
                ]
	    });
	    
	    var f_order = new Ext.create('system.widget.FsdNumber',{
	    	fieldLabel : '排序编号',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 340,
	    	name : 'f_order',
	    	maxLength : 3,
	    	minValue : 0,
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank : false,
	    	blankText : '请输入排序编号',
	    	maxLengthText : '排序编号不能超过3个字符'
	    });
	    
	    var f_score = new Ext.create('system.widget.FsdNumber',{
	    	fieldLabel : '课程分数',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 340,
	    	name: 'f_score',
	    	maxLength : 3,
	    	minValue : 0,
	    	allowDecimals : true,//是否允许输入小数
	    	allowBlank : false,
	    	blankText : '请输入课程分数',
	    	maxLengthText : '课程分数不能超过3个字符'
	    });
	    
	    var f_expirationtime = Ext.create('system.widget.FsdDateTime',{
	    	fieldLabel : '失效时间',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 340,
	    	name: 'f_expirationtime'
	    });

        var f_islink = new Ext.create('system.widget.FsdCheckbox',{
            fieldLabel: '是否外连接',
            labelAlign:'right',
            labelWidth:80,
            width : 360,
            name: 'f_islink',
            inputValue : 'true'
        });
        f_islink.on('change', function (obj, newValue, oldValue, eOpts) {
            f_link.allowBlank = !newValue;
        });
        var f_link = new Ext.create('system.widget.FsdTextField',{
            fieldLabel: '外连接',
            labelAlign:'right',
            labelWidth:80,
            width : 360,
            name: 'f_link',
            maxLength: 200,
            allowBlank: true
        });
	        
	    Ext.apply(this,{
	        width:400,
	        height:500,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[{
				xtype : 'form',
				id : 'courseAddForm',
				defaultType : 'textfield',
				header : false,// 是否显示标题栏
				border : 0,
				padding : '20 0 0 20',
				items : [{
		                    name: "id",
		                    xtype: "hidden"
		                } , 
		                f_coursetypeid, f_coursetypename, f_title, f_imageurl, f_abstract, f_required, f_recommend, f_score, f_order,
		                f_flow, f_expirationtime, f_islink, f_link]
	        }],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.formSubmit(Ext.getCmp('courseAddForm') , me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});