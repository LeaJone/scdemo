﻿/**
 * 课程信息管理界面
 */
Ext.define('system.course.course.biz.CourseManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.course.course.model.Extjs_T_Course'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.course.course.biz.CourseOper'),
    
	createTable : function() {
	    var me = this;
	    var queryid = "FSDCOURSETYPE";
	    
	    /**
		 * 左边的树面板
		 */
		var treepanel = Ext.create("system.widget.FsdTreePanel", {
			icon : 'resources/admin/icon/chart_organisation.png',
			baseUrl : 't_coursetype/load_AsyncAreaTree.do',
			title: '课程分类',
			rootText : '课程分类',
			rootId : 'FSDCOURSETYPE',
			region : 'west', // 设置方位
			collapsible : true,//折叠
			width: 250,
			split: true
		});
		
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_t_course', // 显示列
			model : 'model_t_course',
			baseUrl : 't_course/load_pagedata.do',
			tbar : [{
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
				text : "添加",
				iconCls : 'page_addIcon',
				handler : function() {
					me.oper.add(grid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
				text : "修改",
				iconCls : 'page_editIcon',
				handler : function() {
					me.oper.editor(grid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'dwsc',
				text : "删除",
				iconCls : 'page_deleteIcon',
				handler : function() {
				    me.oper.del(grid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid);
				}
			}, '-', {
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
				text : "启用",
				iconCls : 'tickIcon',
				handler : function(button) {
				    me.oper.isEnabled(grid, true);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
				text : "停用",
				iconCls : 'crossIcon',
				handler : function(button) {
				    me.oper.isEnabled(grid, false);
				}
			}, '->', new Ext.form.TextField( {
				id : 'querycourseName',
				name : 'queryParam',
				emptyText : '请输入课程名称',
				enableKeyEvents : true,
				listeners : {
					specialkey : function(field, e) {
						if (e.getKey() == Ext.EventObject.ENTER) {
							grid.getStore().reload();
						}
					}
				},
				width : 130
			}), {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  Ext.getCmp('querycourseName').getValue();
			var pram = {fid : queryid ,name : name};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		var contentpanel = Ext.create("Ext.panel.Panel", {
			frame:true,
			split: true,
			region: 'center',
			layout: 'fit',
			title: '课程信息管理',
			items: [grid]
		});

		var page1_jExtPanel1_obj = new Ext.panel.Panel({
			bodyPadding: 3,
			header : false,
			border : 0,
			split: true,
			layout: 'border',
			items: [treepanel,contentpanel]
		});
		
		treepanel.on('itemclick', function(view,record,item,index,e){
			queryid = record.raw.id;
			grid.getStore().reload();
		});
		return page1_jExtPanel1_obj;
	}
});