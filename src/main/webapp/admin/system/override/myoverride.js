﻿/**
 * 定义重写一些类或方法的公共行为
 */
Ext.onReady(function() {
	
	/**
	 * 重写Ext.Ajax的意思在于：添加拦截器，对后台异常、session超时信息在前台进行统一的处理。
	 */
	Ext.define('Ext.Ajax',{
		extend: 'Ext.data.Connection',
		
		singleton: true,
	    
		autoAbort: false,
		//是否允许在请求前拦截
		enableBeforeInterceptor:false,
	    
		interceptors:Ext.create('Ext.util.MixedCollection'),
		
		//执行拦截操作
		invokeInterceptor:function(options,response,mode){
			var me = this;
			
			this.interceptors.each(function(interceptor){
				//判断拦截器类型
				if(interceptor.mode == mode || 
					interceptor.mode == Xzr.AbstractInterceptor.AROUND ){
					//执行拦截操作，如果没有通过拦截器则返回false
					if(interceptor.handler(options,response) === false){
						return false;
					};
				}
			});
			
			return true;

		},
		//通过listener实现对Ajax访问的拦截
		listeners :{
			//拦截器处理，如果没有通过拦截器，则取消请求
			beforerequest:function( conn, options, eOpts ){
				if(this.enableBeforeInterceptor){
					return this.invokeInterceptor(options,null,'before');
				}
				return true;
			},
			//请求完成后对数据验证，无法中断后续的操作，具体请研究ExtJs源码。
			requestcomplete:function(conn, response, options, eOpts){
				return this.invokeInterceptor(options,response,'after');
			}
		},
		
		/**
		 * 添加拦截器
		 * @param {String} interceptorId
		 * @param {Xzr.web.AbstractInterceptor} interceptor
		 */
		addInterceptor:function(interceptor){
			if(!interceptor)return;
			
			if(Ext.isString(interceptor)){
				interceptor = Ext.create(interceptor);
			}
			
			this.interceptors.add(interceptor.getId(),interceptor);
		}
	});
	
	/**
	 * 重写该方法的意思在于：当表单提交含有waitMsg信息并且本次请求出现异常，异常无法弹出的问题。
	 */
	Ext.form.Basic.prototype.afterAction = function(action, success) {
		var me = this;
        if (action.waitMsg) {
            var messageBox = Ext.MessageBox,
                waitMsgTarget = me.waitMsgTarget;
            if (waitMsgTarget === true) {
                me.owner.el.unmask();
            } else if (waitMsgTarget) {
                waitMsgTarget.unmask();
            } else {
            	if(action.result!= null){
            		if(!action.result.isException && !action.result.isSessionOut && !action.result.isNoAuthority){
            			messageBox.hide();
            		}
            	}
            }
        }
        // Restore setting of any floating ancestor which was manipulated in beforeAction
        if (me.floatingAncestor) {
            me.floatingAncestor.preventFocusOnActivate = me.savePreventFocusOnActivate;
        }
        if (success) {
            if (action.reset) {
                me.reset();
            }
            Ext.callback(action.success, action.scope || action, [me, action]);
            me.fireEvent('actioncomplete', me, action);
        } else {
            Ext.callback(action.failure, action.scope || action, [me, action]);
            me.fireEvent('actionfailed', me, action);
        }
	}
});
