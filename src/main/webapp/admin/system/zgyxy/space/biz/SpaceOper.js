/**
 * 空间管理界面操作类
 *
 */
Ext.define('system.zgyxy.space.biz.SpaceOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 添加
     */
	add : function(grid){
	    var win = Ext.create('system.zgyxy.space.biz.SpaceEdit');
        win.setTitle('空间添加');
        win.modal = true;
        win.grid = grid;
        win.show();
	},
	
	/**
     * 修改
     */
	editor : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的空间！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能修改一个空间！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.zgyxy.space.biz.SpaceEdit');
            win.setTitle('空间修改');
            win.modal = true;
            win.grid = grid;
            win.fid = id;
            win.honor.setDisabled(false);
            win.honorGrid.getStore().reload();
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'z_space/load_z_spacebyid.do' , param , null ,
	        function(){
	            win.show();
	        });
        }
	},
 	
 	/**
     * 查看
     */
	view : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
	    if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要查看的空间！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能查看一个空间！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.zgyxy.space.biz.SpaceEdit');
            win.setTitle('基地查看');
            win.modal = true;
            win.fid = id;
            win.honor.setDisabled(false);
            win.honorGrid.getStore().reload();
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'z_space/load_z_spacebyid.do' , param , null ,
	        function(){
	            win.show();
	        });
        }
	},
	
	/**
     * 删除
     */
	del : function(grid){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
	    if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要删除的空间！');
        }else{
            Ext.MessageBox.show({
                title : '提示',
		        msg : '您确定要删除所选空间吗？',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
					    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
			            });
             			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('z_space/del_z_space_qx_z_spacesc.do' , param ,
                        function(){
            				Ext.MessageBox.alert('提示', '空间删除成功！');
                        	grid.getStore().reload();
                        }, null, me.form);
                    }
                }
            });
        }
	},

	/**
     * 表单提交
     */
     formSubmit : function(formpanel, grid, honor){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_space/save_z_space_qx_z_spacebj.do' , formpanel.form , '正在提交数据,请稍候.....',
			function(form, action, respText){
                formpanel.up('window').fid = respText.data.id;
                formpanel.up('window').f_id.setValue(respText.data.id);
                honor.setDisabled(false);
				Ext.MessageBox.alert('提示', '保存成功！');
                grid.getStore().reload();
            });
        }
     }
});