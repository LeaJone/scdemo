﻿/**
 * 空间管理界面
 * @author lumingbao
 */
Ext.define('system.zgyxy.space.biz.SpaceManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*',
		'system.zgyxy.space.model.Extjs_Column_Space',
		'system.zgyxy.space.model.Extjs_Model_Space'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.zgyxy.space.biz.SpaceOper'),
	
	createContent : function(){
		var me = this;

		/**
		 * 查询文本框
		 */
		var queryText =  Ext.create("Ext.form.TextField", {
			name : 'queryTeamParam',
			emptyText : '请输入空间名称',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});

		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_space', // 显示列
			model: 'model_z_space',
			baseUrl : 'z_space/load_pagedata.do',
			tbar : [{
				xtype : "fsdbutton",
				popedomCode : 'rybj',
				text : "添加",
				iconCls : 'page_addIcon',
				handler : function() {
					me.oper.add(grid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'rybj',
				text : "修改",
				iconCls : 'page_editIcon',
				handler : function() {
					me.oper.editor(grid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'rysc',
				text : "删除",
				iconCls : 'page_deleteIcon',
				handler : function() {
				    me.oper.del(grid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid);
				}
			},  '->', queryText, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
					treepanel.getStore.reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  queryText.getValue();
			var pram = {name : name};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params, {jsonData : Ext.encode(pram)});
		});

		return grid;
	}
});