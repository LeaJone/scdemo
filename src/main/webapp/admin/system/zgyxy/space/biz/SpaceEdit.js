﻿/**
 * 基地添加界面
 * @author lumingbao
 */
Ext.define('system.zgyxy.space.biz.SpaceEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	fid:'',
	oper : Ext.create('system.zgyxy.space.biz.SpaceOper'),
	honorOper : Ext.create('system.zgyxy.honor.biz.HonorOper'),
	requires : [ 'system.zgyxy.honor.model.Extjs_Model_Honor',
		'system.zgyxy.honor.model.Extjs_Column_Honor'],
	initComponent:function(){
		var me = this;

		me.f_id = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
			value: ''
		});

		var f_name = new Ext.create('system.widget.FsdTextField',{
			fieldLabel: '空间名称',
			labelAlign:'right',
			labelWidth:100,
			width : 650,
			name: 'f_name',
			maxLength: 25,
			allowBlank: false
		});

		var f_contact = new Ext.create('system.widget.FsdTextField',{
			fieldLabel: 'VR地址',
			labelAlign:'right',
			labelWidth:100,
			width : 650,
			name: 'f_vrurl',
			maxLength: 200
		});

		var f_ds = new Ext.create('system.widget.FsdTextField',{
			fieldLabel: '双创导师',
			labelAlign:'right',
			labelWidth:100,
			width : 650,
			name: 'f_ds',
			maxLength: 25
		});

		var f_rs = new Ext.create('system.widget.FsdTextField',{
			fieldLabel: '双创人数',
			labelAlign:'right',
			labelWidth:100,
			width : 650,
			name: 'f_rs',
			maxLength: 25
		});

		var f_rzqy = new Ext.create('system.widget.FsdTextField',{
			fieldLabel: '入驻企业',
			labelAlign:'right',
			labelWidth:100,
			width : 650,
			name: 'f_rzqy',
			maxLength: 25
		});

		var f_zxwcxjj = new Ext.create('system.widget.FsdTextField',{
			fieldLabel: '中小微创新基金',
			labelAlign:'right',
			labelWidth:100,
			width : 650,
			name: 'f_zxwcxjj',
			maxLength: 25
		});

		var f_scjj = new Ext.create('system.widget.FsdTextField',{
			fieldLabel: '双创基金',
			labelAlign:'right',
			labelWidth:100,
			width : 650,
			name: 'f_scjj',
			maxLength: 25
		});

		var f_schd = new Ext.create('system.widget.FsdTextField',{
			fieldLabel: '双创活动',
			labelAlign:'right',
			labelWidth:100,
			width : 650,
			name: 'f_schd',
			maxLength: 25
		});

		var f_cyjs = new Ext.create('system.widget.FsdTextField',{
			fieldLabel: '创业竞赛',
			labelAlign:'right',
			labelWidth:100,
			width : 650,
			name: 'f_cyjs',
			maxLength: 25
		});

		var f_zscq = new Ext.create('system.widget.FsdTextField',{
			fieldLabel: '知识产权',
			labelAlign:'right',
			labelWidth:100,
			width : 650,
			name: 'f_zscq',
			maxLength: 25
		});
		var f_abstract = Ext.create('system.widget.FsdUeditor',{
			fieldLabel: '空间简介',
			labelAlign:'right',
			labelWidth:100,
			width : 650,
			height : 420,
			name: 'f_abstract',
			allowBlank: false
		});

		var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '15 10 5 10',
			items : [me.f_id,
				f_name,
				f_contact,
				f_ds,
				f_rs,
				f_rzqy,
				f_zxwcxjj,
				f_scjj,
				f_schd,
				f_cyjs,
				f_zscq,
				f_abstract]
		});

		// 创建表格
		me.honorGrid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_honor', // 显示列
			model: 'model_z_honor',
			baseUrl : 'z_honor/load_pagedata.do',
			border: 0,
			zAutoLoad: false,
			tbar : [{
				xtype : "fsdbutton",
				popedomCode : 'rybj',
				text : "添加",
				iconCls : 'page_addIcon',
				handler : function() {
					me.honorOper.add(me.honorGrid, me.fid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'rybj',
				text : "修改",
				iconCls : 'page_editIcon',
				handler : function() {
					me.honorOper.editor(me.honorGrid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.honorOper.view(me.honorGrid);
				}
			},{
				xtype : "fsdbutton",
				popedomCode : 'rysc',
				text : "删除",
				iconCls : 'page_deleteIcon',
				handler : function() {
					me.honorOper.del(me.honorGrid);
				}
			},   '->', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					me.honorGrid.getStore().reload();
				}
			}]
		});
		me.honorGrid.getStore().on('beforeload', function(s) {
			var pram = {fid : me.fid};
			var params = s.getProxy().extraParams;
			Ext.apply(params,{jsonData : Ext.encode(pram)});
		});

		me.honor = Ext.create('Ext.panel.Panel',{
			title: '空间荣誉',
			layout : 'fit',
			disabled : true,
			items : [me.honorGrid]
		});

		var tabpanel = Ext.create('Ext.tab.Panel', {
			border: 0,
			enableTabScroll: true,
			deferredRender: false,
			defaults: { autoScroll: true },
			items: [{
				border: 0,
				title: '基本信息',
				items: [form]
			}, me.honor],
			listeners:{
				'tabchange':function (t, n) {

				}
			}
		});

		Ext.apply(this,{
			layout : 'fit',
			width:700,
			height:530,
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			items :[tabpanel],
			buttons : [{
				name : 'btnsave',
				text : '保存',
				iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
				handler : function() {
					me.oper.formSubmit(form, me.grid, me.honor);
				}
			}, {
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	}
});