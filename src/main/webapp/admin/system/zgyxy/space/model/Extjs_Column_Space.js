﻿Ext.define('column_z_space', {
    columns: [
        {
            xtype: 'rownumberer',
            text: '',
            sortable: true,
            width: 40,
            align: 'left'
        }
        ,
        {
            header: '空间名称',
            dataIndex: 'f_name',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '添加时间',
            dataIndex: 'f_adddate',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                return system.UtilStatic.formatDateTime(value);
            }
        }
    ]
});
