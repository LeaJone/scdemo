﻿/**
 * 专家选择
 */
Ext.define('system.zgyxy.user.biz.ZjChoose', {
	extend : 'Ext.window.Window',
	initComponent:function(){
	    var me = this;

	    me.form = Ext.create('system.zgyxy.user.biz.ZjChooseForm');

	    Ext.apply(this,{
            width : 800,
            height : 500,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
			layout : 'fit',
	        items :[me.form],
	        buttons : [{
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	},

	setCallbackFun:function(callbackFun){
		this.form.callbackFun  =  callbackFun;
	}
});