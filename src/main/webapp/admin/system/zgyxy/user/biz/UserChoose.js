﻿/**
 * 用户选择
 */
Ext.define('system.zgyxy.user.biz.UserChoose', {
	extend : 'Ext.window.Window',
	initComponent:function(){
	    var me = this;

	    me.form = Ext.create('system.zgyxy.user.biz.UserChooseForm');

	    Ext.apply(this,{
            width : 800,
            height : 600,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[me.form],
	        buttons : [{
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	},

	setCallbackFun:function(callbackFun){
		this.form.callbackFun  =  callbackFun;
	}
});