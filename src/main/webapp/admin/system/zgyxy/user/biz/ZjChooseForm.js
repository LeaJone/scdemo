﻿/**
 * 专家选择表单
 */
Ext.define('system.zgyxy.user.biz.ZjChooseForm', {
	extend : 'system.widget.FsdFormPanel',
	requires : ['system.abasis.employee.model.Extjs_A_employee'],
	header : false,
	win : null,
	callbackFun :  null,
	grid : null,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.zgyxy.team.biz.TeamOper'),
	
	createContent : function(){
		var me = this;

		/**
		 * 查询文本框
		 */
		var queryText =  Ext.create("Ext.form.TextField", {
			name : 'queryParam',
			emptyText : '请输入专家名称',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});

		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_a_employee_zj', // 显示列
			model: 'model_a_employee',
			baseUrl : 'A_Employee/load_zhuanjia.do',
			tbar : [{
				xtype : "fsdbutton",
				text : "选择专家",
				iconCls : 'page_addIcon',
				handler : function() {
					var data = grid.getSelectionModel().getSelection();
					if (data.length == 0) {
						Ext.MessageBox.alert('提示', '请至少选择一位专家！');
					}else{
						var dir = new Array();
						Ext.Array.each(data, function(items) {
							var id = items.data.id;
							dir.push(id);
						});
						me.callbackFun(dir);
					}
				}
				}, '->', queryText, {
					text : '查询',
					iconCls : 'magnifierIcon',
					handler : function() {
						grid.getStore().loadPage(1);
					}
				}, {
					text : '刷新',
					iconCls : 'tbar_synchronizeIcon',
					handler : function() {
						grid.getStore().reload();
					}
			}]
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  queryText.getValue();
			var pram = {name : name};
			var params = s.getProxy().extraParams;
			Ext.apply(params, {jsonData : Ext.encode(pram)});
		});
		return grid;
	}
});