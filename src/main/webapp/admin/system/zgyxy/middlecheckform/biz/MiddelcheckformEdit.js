﻿/**
 * 中期检查表编辑界面
 *
 * @author lhl
 */
Ext.define('system.zgyxy.middlecheckform.biz.MiddelcheckformEdit', {
    extend : 'Ext.panel.Panel',
    grid : null,
    projectid : '',
    projectname: '',
    oper : Ext.create('system.zgyxy.middlecheckform.biz.MiddlecheckformOper'),

    initComponent : function() {
        var me = this;

        var f_projectid = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '编号',
            name : 'f_projectid'
        });

        var f_projectname = Ext.create('system.widget.FsdTextField',{
            fieldLabel: '项目名称',
            labelAlign:'right',
            labelWidth:120,
            width : 940,
            name: 'f_projectname',
            readOnly: true
        });

        var f_projectprogress = Ext.create('system.widget.FsdUeditor',{
            fieldLabel: '项目进展情况',
            labelAlign: 'right',
            labelWidth: 120,
            width: 920,
            height: 300,
            name: 'f_projectprogress',
            padding: '0 0 30 0'
        });

        var f_projectphaseresult = Ext.create('system.widget.FsdUeditor',{
            fieldLabel: '项目阶段性成果',
            labelAlign:'right',
            labelWidth:120,
            width : 920,
            height : 300,
            name: 'f_projectphaseresult',
            padding: '0 0 30 0'
        });

        var f_mainproblem = Ext.create('system.widget.FsdUeditor',{
            fieldLabel: '主要问题及措施',
            labelAlign:'right',
            labelWidth:120,
            width : 920,
            height : 300,
            name: 'f_mainproblem',
            padding: '0 0 30 0'
        });

        var f_maintask = Ext.create('system.widget.FsdUeditor',{
            fieldLabel: '主要任务及安排',
            labelAlign:'right',
            labelWidth:120,
            width : 920,
            height : 300,
            name: 'f_maintask',
            padding: '0 0 30 0'
        });

        var f_projectfunds = Ext.create('system.widget.FsdUeditor',{
            fieldLabel: '项目经费使用情况',
            labelAlign:'right',
            labelWidth:120,
            width : 920,
            height : 300,
            name: 'f_projectfunds',
            padding: '0 0 30 0'
        });

        var f_accessory = Ext.create('system.widget.FsdTextFileManage', {
            width : 940,
            zName : 'f_accessory',
            zFieldLabel : '附件',
            zLabelWidth : 120,
            zIsUpButton : true,
            zIsShowButton : true,
            zFileUpPath : me.oper.util.getFilePath()
        });

        me.form = Ext.create('Ext.form.Panel',{
            border : false,
            padding : '10 0 30 10',
            items : [ f_projectid, f_projectname, f_projectprogress, f_projectphaseresult, f_mainproblem, f_maintask, f_projectfunds, f_accessory ]
        });

        me.on('boxready', function(){
            f_projectid.setValue(me.projectid);
            f_projectname.setValue(me.projectname);
        });

        Ext.apply(me, {
            title: '提交中期检查表',
            autoScroll : true,
            tbar : [{
                xtype : "fsdbutton",
                popedomCode : 'middelcheckbj',
                text : "提 交",
                iconCls : 'acceptIcon',
                handler : function() {
                    me.oper.formSubmit(me, me.grid);
                }
            }],
            items :[me.form]
        });
        me.callParent(arguments);
    }

});