﻿/**
 * 中期检查表查看界面
 *
 * @author lhl
 */
Ext.define('system.zgyxy.middlecheckform.biz.MiddelcheckformView', {
    extend : 'Ext.panel.Panel',
    grid : null,
    projectname: '',
    oper : Ext.create('system.zgyxy.middlecheckform.biz.MiddlecheckformOper'),

    initComponent : function() {
        var me = this;

        var f_projectid = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '编号',
            name : 'f_projectid'
        });

        var f_teacheraudit = new Ext.form.RadioGroup({
            fieldLabel: '指导老师审核状态',
            labelWidth:113,
            labelAlign:'right',
            width : 350,
            items: [{
                name: 'f_teacheraudit',
                inputValue: 'true',
                boxLabel: '已审核'
            }, {
                name: 'f_teacheraudit',
                inputValue: 'false',
                boxLabel: '未审核'
            }]
        });

        var f_teacherauditcon = Ext.create('system.widget.FsdTextArea',{
            fieldLabel: '指导老师审核意见',
            labelAlign:'right',
            labelWidth:120,
            anchor : '95%',
            grow : true,
            name: 'f_teacherauditcon'
        });

        var f_expertaudit = new Ext.form.RadioGroup({
            fieldLabel: '院系专家审核状态',
            labelWidth:113,
            labelAlign:'right',
            width : 350,
            items: [{
                name: 'f_expertaudit',
                inputValue: 'true',
                boxLabel: '已审核'
            }, {
                name: 'f_expertaudit',
                inputValue: 'false',
                boxLabel: '未审核'
            }]
        });

        var f_expertauditcon = Ext.create('system.widget.FsdTextArea',{
            fieldLabel: '院系专家审核意见',
            labelAlign:'right',
            labelWidth:120,
            anchor : '95%',
            grow : true,
            name: 'f_expertauditcon'
        });

        var f_branchaudit = new Ext.form.RadioGroup({
            fieldLabel: '校级专家审核状态',
            labelWidth:113,
            labelAlign:'right',
            width : 350,
            items: [{
                name: 'f_branchaudit',
                inputValue: 'true',
                boxLabel: '已审核'
            }, {
                name: 'f_branchaudit',
                inputValue: 'false',
                boxLabel: '未审核'
            }]
        });

        var f_branchauditcon = Ext.create('system.widget.FsdTextArea',{
            fieldLabel: '校级专家审核意见',
            labelAlign:'right',
            labelWidth:120,
            anchor : '95%',
            grow : true,
            name: 'f_branchauditcon'
        });

        var f_projectname = Ext.create('system.widget.FsdTextField',{
            fieldLabel: '项目名称',
            labelAlign:'right',
            labelWidth:120,
            anchor : '95%',
            name: 'f_projectname'
        });

        var f_projectprogress = Ext.create('system.widget.FsdTextArea',{
            fieldLabel: '项目进展情况',
            labelAlign: 'right',
            labelWidth: 120,
            anchor : '95%',
            grow : true,
            name: 'f_projectprogress'
        });

        var f_projectphaseresult = Ext.create('system.widget.FsdTextArea',{
            fieldLabel: '项目阶段性成果',
            labelAlign:'right',
            labelWidth:120,
            anchor : '95%',
            grow : true,
            name: 'f_projectphaseresult'
        });

        var f_mainproblem = Ext.create('system.widget.FsdTextArea',{
            fieldLabel: '主要问题及措施',
            labelAlign:'right',
            labelWidth:120,
            anchor : '95%',
            grow : true,
            name: 'f_mainproblem'
        });

        var f_maintask = Ext.create('system.widget.FsdTextArea',{
            fieldLabel: '主要任务及安排',
            labelAlign:'right',
            labelWidth:120,
            anchor : '95%',
            grow : true,
            name: 'f_maintask'
        });

        var f_projectfunds = Ext.create('system.widget.FsdTextArea',{
            fieldLabel: '项目经费使用情况',
            labelAlign:'right',
            labelWidth:120,
            anchor : '95%',
            grow : true,
            name: 'f_projectfunds'
        });

        var f_accessory = Ext.create('system.widget.FsdTextFileManage', {
            width : 650,
            zName : 'f_accessory',
            zFieldLabel : '附件',
            zLabelWidth : 120,
            zIsUpButton : true,
            zIsDownButton : true,
            zFileUpPath : me.oper.util.getFilePath()
        });

        me.form = Ext.create('Ext.form.Panel',{
            border : false,
            padding : '20 0 30 30',
            items : [ f_projectname, f_teacheraudit, f_teacherauditcon, f_expertaudit, f_expertauditcon, f_branchaudit, f_branchauditcon, f_projectid, f_projectprogress, f_projectphaseresult, f_mainproblem, f_maintask, f_projectfunds, f_accessory ]
        });

        me.on('boxready', function(){
            f_projectname.setValue(me.projectname);
        });

        Ext.apply(me, {
            title: '提交中期检查表',
            autoScroll : true,
            items :[me.form]
        });
        me.callParent(arguments);
    }

});