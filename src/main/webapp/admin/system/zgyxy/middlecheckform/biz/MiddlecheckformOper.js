/**
 * 中期检查表操作类
 */

Ext.define('system.zgyxy.middlecheckform.biz.MiddlecheckformOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 提交中期检查表
     */
	add : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要提交的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能选择一个项目提交！');
        }else{
            var isFlag = data[0].data.f_issubzqjc;
            if(isFlag == "false"){
                var model = Ext.create("system.zgyxy.middlecheckform.biz.MiddelcheckformEdit");
                model.grid = grid;
                model.projectid = data[0].data.id;
                model.projectname = data[0].data.f_name;
                me.util.addTab('middelcheckbj', "提交中期检查表", model, "fa-table");
            }else{
                Ext.MessageBox.alert('提示', '你已经提交过了，请不要重复提交！');
            }
        }
	},
	
	/**
     * 保存中期检查表
     */
    formSubmit : function(formpanel , grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('f_middlecheckform/save_middlecheckform.do', formpanel.form, '正在保存中,请稍候.....',
			function(){
                me.util.closeTab();
			    grid.getStore().reload();
				Ext.MessageBox.alert('提示', '保存成功！');
			});
        }
     },
	
	/**
	 * 查看中期检查表
	 */
	view : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要查看的数据！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能查看一条数据！');
        }else{
            var isFlag = data[0].data.f_issubzqjc;
            if(isFlag == "false"){
                Ext.MessageBox.alert('提示', '该项目没有提交中期检查表！');
            }else{
                // 先得到主键
                var id = data[0].data.id;
                //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
                var model = Ext.create('system.zgyxy.middlecheckform.biz.MiddelcheckformView');
                model.grid = grid;
                model.projectname = data[0].data.f_name;
                var pram = {id : id};
                var param = {jsonData : Ext.encode(pram)};
                me.util.formLoad(model.down('form').getForm() , 'f_middlecheckform/load_MiddlecheckformById.do', param, null,
                    function(){
                        me.util.addTab('middlecheckbj', "查看项目中期检查表", model, "fa-table");
                    });
            }
        }
	}

});