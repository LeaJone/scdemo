﻿/**
 * 中期检查表管理界面
 */

Ext.define('system.zgyxy.middlecheckform.biz.MiddlecheckformManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.zgyxy.middlecheckform.model.Extjs_Column_F_middlecheckform',
	             'system.zgyxy.middlecheckform.model.Extjs_Model_F_middlecheckform'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.zgyxy.middlecheckform.biz.MiddlecheckformOper'),
	
	createContent : function(){
		var me = this;
		
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_f_middlecheckform', // 显示列
			model: 'model_f_middlecheckform',
			baseUrl : 'f_middlecheckform/load_ProjectPageData.do',
			border : false,
			tbar : [{
				xtype : "fsdbutton",
				popedomCode : 'middlecheckbj',
				text : "提交中期检查表",
				iconCls : 'tickIcon',
				handler : function() {
				    me.oper.add(grid);
				}
			}, {
				xtype : "fsdbutton",
				text : "查看",
				iconCls : 'magnifierIcon',
				handler : function() {
				    me.oper.view(grid);
				}
			}, '->', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]
		});
		
		return grid;
	}
});