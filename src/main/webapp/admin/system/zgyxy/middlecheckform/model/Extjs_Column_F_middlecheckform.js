﻿Ext.define('column_f_middlecheckform', {
    columns: [
        {
            xtype : 'rownumberer',
            text : '',
            sortable : true,
            width: 40,
            align: 'left'
        }
        ,
        {
            header: '项目名称',
            dataIndex: 'f_name',
            sortable: true,
            align: 'center',
            flex: 2
        }
        ,
        {
            header: '项目类型',
            dataIndex: 'f_typename',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '指导老师',
            dataIndex: 'f_teachername',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '发布时间',
            dataIndex: 'f_adddate',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                return system.UtilStatic.formatDateTime(value);
            }
        }
        ,
        {
            header : '项目流程状态',
            dataIndex : 'f_statusname',
            sortable : true,
            align: 'center',
            flex : 1
        }
        ,
        {
            header: '是否提交',
            dataIndex: 'f_issubzqjc',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                if(value == "true"){
                    value = "<font style='color:green;'>已提交</font>";
                }else{
                    value = "<font style='color:red;'>未提交</font>";
                }
                return value;
            }
        }
    ]
});