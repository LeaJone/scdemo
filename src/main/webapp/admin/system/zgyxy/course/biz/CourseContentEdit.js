﻿/**
 * 课程编辑界面
 */
Ext.define('system.zgyxy.course.biz.CourseContentEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.zgyxy.course.biz.CourseContentOper'),
	initComponent:function(){
		var me = this;	 
		
		var f_typeid = Ext.create('Ext.form.field.Hidden',{
			name: 'f_typeid',
			value: 'xskc'
		});
		
		var f_typename = Ext.create('Ext.form.field.Hidden',{
			name: 'f_typename',
			value: '线上课程'
		});
	    
	    var f_name = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel : '课程名称',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 750,
	    	name : 'f_name',
	    	maxLength : 50,
	    	allowBlank : false,
	    	blankText : '请输入课程名称',
	    	maxLengthText : '课程名称不能超过50个字符'
	    });
	    var f_teacher = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel : '开课教师',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 750,
	    	name : 'f_teacher',
	    	maxLength : 30,
	    	allowBlank : false,
	    	blankText : '请输入开课教师姓名',
	    	maxLengthText : '开课教师姓名不能超过30个字符'
	    });
	    
	    var f_period = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel : '学时',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 750,
	    	name : 'f_period',
	    	maxLength : 10,
	    	allowBlank : false,
	    	blankText : '请输入课程学时',
	    	maxLengthText : '课程学时不能超过10个字符'
	    });
	    
	    var f_score = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel : '学分',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 750,
	    	name : 'f_score',
	    	maxLength : 10,
	    	allowBlank : false,
	    	blankText : '请输入课程学分',
	    	maxLengthText : '课程学分不能超过10个字符'
	    });
	    
	    var f_count = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel : '学生人数',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 750,
	    	name : 'f_count',
	    	maxLength : 10,
	    	allowBlank : false,
	    	blankText : '请输入上课人数',
	    	maxLengthText : '上课人数不能超过10个字符'
	    });
	    
	    var f_collegename = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel : '开课学院',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 750,
	    	name : 'f_collegename',
	    	maxLength : 50,
	    	allowBlank : true,
	    	blankText : '请输入开课学院',
	    	maxLengthText : '开课学院不能超过50个字符'
	    });
	    
	    var f_site = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel : '授课地点',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 750,
	    	name : 'f_site',
	    	maxLength : 50,
	    	allowBlank : true,
	    	blankText : '请输入授课地点',
	    	maxLengthText : '授课地点不能超过50个字符'
	    });
	    
	    var f_propertyid = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'课程类型',
            labelAlign:'right',
            labelWidth:80,
            width : 750,
            name : 'f_propertyid',//提交到后台的参数名
            key : 'sckclx',//参数
            allowBlank: false,
            hiddenName : 'f_propertyname'//提交隐藏域Name
        });
        var f_propertyname = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '课程类型',
            name: 'f_propertyname'
        });
        
	    var f_wayid = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'授课方式',
            labelAlign:'right',
            labelWidth:80,
            width : 750,
            name : 'f_wayid',//提交到后台的参数名
            key : 'scskfs',//参数
            allowBlank: false,
            hiddenName : 'f_wayname'//提交隐藏域Name
        });
        var f_wayname = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '授课方式',
            name: 'f_wayname'
        });
	    
	    var f_platformcode = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'开课平台',
            labelAlign:'right',
            labelWidth:80,
            width : 750,
            name : 'f_platformcode',//提交到后台的参数名
            key : 'kkpt',//参数
            allowBlank: false,
            hiddenName : 'f_platform'//提交隐藏域Name
        });
        var f_platform = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '开课平台',
            name: 'f_platform'
        });
        
        
	    var f_url = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel : '选课网址',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 750,
	    	name : 'f_url',
	    	maxLength : 200,
	    	allowBlank : false,
	    	blankText : '请输入课程学分',
	    	maxLengthText : '课程学分不能超过200个字符'
	    });
	    
	    var f_sort = new Ext.create('system.widget.FsdNumber',{
			fieldLabel: '排序编号',
			labelAlign:'right',
			labelWidth:80,
			width : 750,
			name: 'f_sort',
			maxLength: 4,
			minValue: 0,
			allowDecimals : false,//是否允许输入小数
			allowBlank: false
		});
	    
	    var f_abstract = Ext.create('system.widget.FsdUeditor',{
			fieldLabel: '课程简介',
			labelAlign:'right',
			labelWidth:80,
			width : 750,
			height : 420,
			name: 'f_abstract',
			allowBlank: false
		});

		var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '15 10 5 10',
			items : [{
				name: "id",
				xtype: "hidden"
			} ,
				f_typename,f_typeid,f_name,f_teacher,f_period,f_score,f_count,f_collegename,f_site,f_propertyid,f_propertyname,f_wayid,f_wayname,f_platformcode,f_platform,f_url,f_sort,f_abstract]
		});

	    Ext.apply(this,{
	        width:800,
	        height:550,
	        autoScroll:true,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.formSubmit(form , me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});