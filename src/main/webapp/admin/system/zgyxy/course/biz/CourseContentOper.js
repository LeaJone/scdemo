/**
 * 课时管理界面操作类
 */

Ext.define('system.zgyxy.course.biz.CourseContentOper', {

	form : null,
	util : Ext.create('system.util.util'),

	/**
	 * 添加
	 */
	add : function(grid, type){
		var win;
		if(type == 'xskc'){
			win = Ext.create('system.zgyxy.course.biz.CourseContentEdit');
		}else if(type == 'xxkc'){
			win = Ext.create('system.zgyxy.course.biz.CourseUnderEdit');
		}else{
			Ext.MessageBox.alert('提示', '请选择课程类型！');
		}

		win.setTitle('课程添加');
		win.grid = grid;
		win.modal = true;
		win.show();
	},

	/**
	 * 表单提交
	 */
	formSubmit : function(formpanel , grid){
		var me = this;
		if (formpanel.form.isValid()) {
			me.util.ExtFormSubmit('z_course/save_z_course_qx_z_coursebj.do' , formpanel.form , '正在提交数据,请稍候.....',
					function(form, action){
				formpanel.up('window').close();
				grid.getStore().reload();
				Ext.MessageBox.alert('提示', '保存成功！');
			},
			function (form, action){
				Ext.MessageBox.alert('异常', action.result.msg);
			});
		}
	},
	/**
	 * 拒绝理由提交
	 */
	reasonSubmit: function(formpanel , grid){
		var me = this;
		if (formpanel.form.isValid()) {
			me.util.ExtFormSubmit('z_course/save_reason.do' , formpanel.form , '正在提交数据,请稍候.....',
					function(form, action){
				formpanel.up('window').close();
				grid.getStore().reload();
				Ext.MessageBox.alert('提示', '保存成功！');
			},
			function (form, action){
				Ext.MessageBox.alert('异常', action.result.msg);
			});
		}
	},

	/**
	 * 删除
	 */
	del : function(grid){
		var me = this;
		// 获取选中的行
		var data = grid.getSelectionModel().getSelection();
		if (data.length == 0) {
			Ext.MessageBox.alert('提示', '请先选中要删除的项目！');
		}else {
			Ext.MessageBox.show({
				title : '询问',
				msg : '您确定要删除吗?',
				width : 250,
				buttons : Ext.MessageBox.YESNO,
				icon : Ext.MessageBox.QUESTION,
				fn : function(btn) {
					if (btn == 'yes') {
						var dir = new Array();　
						Ext.Array.each(data, function(items) {
							var id = items.data.id;
							dir.push(id);
						});
						var pram = {ids : dir};
						var param = {jsonData : Ext.encode(pram)};
						me.util.ExtAjaxRequest('z_course/del_z_course_qx_z_coursesc.do' , param ,
								function(response, options){
							Ext.MessageBox.alert('提示', '删除成功！');
							grid.getStore().reload();
						}, null, me.form);
					}
				}
			});
		}
	},

	/**
	 * 修改
	 */
	editor : function(grid,type){
		var me = this;
		// 获取选中的行
		var data = grid.getSelectionModel().getSelection();
		if (data.length == 0) {
			Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
		}else if(data.length > 1){
			Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
		}else{
			// 先得到主键
			var id = data[0].data.id;
			//用的到的主键到后台查询，查询成功以后将相应的值放入文本框
			var win;
			if(type == 'xskc'){
				win = Ext.create('system.zgyxy.course.biz.CourseContentEdit');
			}else if(type == 'xxkc'){
				win = Ext.create('system.zgyxy.course.biz.CourseUnderEdit');
			}else{
				Ext.MessageBox.alert('提示', '请选择课程类型！');
			}
			win.setTitle('课程信息修改');
			win.modal = true;
			win.grid = grid;
			var pram = {id : id};
			var param = {jsonData : Ext.encode(pram)};
			me.util.formLoad(win.down('form').getForm() , 'z_course/load_z_coursebyid.do' , param , null , 
					function(response, options, respText){
				win.show();
			});
		}
	},

	/**
	 * 查看
	 */
	view : function(grid,type){
		var me = this;
		// 获取选中的行
		var data = grid.getSelectionModel().getSelection();
		if (data.length == 0) {
			Ext.MessageBox.alert('提示', '请先选中要查看的项目！');
		}else if(data.length > 1){
			Ext.MessageBox.alert('提示', '每次只能查看一个项目！');
		}else{
			// 先得到主键
			var id = data[0].data.id;
			//用的到的主键到后台查询，查询成功以后将相应的值放入文本框
			var win;
			if(type == 'xskc'){
				win = Ext.create('system.zgyxy.course.biz.CourseContentEdit');
			}else if(type == 'xxkc'){
				win = Ext.create('system.zgyxy.course.biz.CourseUnderEdit');
			}          
			win.setTitle('课时查看');
			win.modal = true;
			win.down('button[name=btnsave]').setVisible(false);
			var pram = {id : id};
			var param = {jsonData : Ext.encode(pram)};
			me.util.formLoad(win.down('form').getForm() , 'z_course/load_z_coursebyid.do' , param , null , 
					function(response, options, respText){
				win.show();
			});
		}
	},

	/**
	 * 是否启用停用
	 */
	isEnabled : function(grid,treepanel,isQT){
		var me = this;
		// 获取选中的行
		var data = grid.getSelectionModel().getSelection();
		if (data.length == 0) {
			Ext.MessageBox.alert('提示', '请选择至少一条数据！');
		}else {
			Ext.MessageBox.show({
				title : '提示',
				msg : '您确定要修改课程状态吗?',
				width : 250,
				buttons : Ext.MessageBox.YESNO,
				icon : Ext.MessageBox.QUESTION,
				fn : function(btn) {
					if (btn == 'yes') {
						var dir = new Array();　
						Ext.Array.each(data, function(items) {
							var id = items.data.id;
							dir.push(id);
						});
						var pram = {ids : dir, isenabled : isQT};
						var param = {jsonData : Ext.encode(pram)};
						me.util.ExtAjaxRequest('z_course/enabled_course_qx_coursebj.do' , param ,
								function(response, options){
							Ext.MessageBox.alert('提示', '课程状态修改成功！');
							grid.getStore().reload(); 
						});
					}
				}
			});
		}
	},
	/**
	 * 是否通过审核
	 */
	isCheck : function(grid,treepanel,isQT){
		var me = this;
		// 获取选中的行
		var data = grid.getSelectionModel().getSelection();
		if (data.length == 0) {
			Ext.MessageBox.alert('提示', '请先选中要审核的项目！');
		}else if(data.length > 1){
			Ext.MessageBox.alert('提示', '每次只能审核一个项目！');
		}else{
			// 先得到主键
			var mainid = data[0].data.id;
			//用的到的主键到后台查询，查询成功以后将相应的值放入文本框
			Ext.MessageBox.show({
				title : '提示',
				msg : '您确定要修改审核状态吗?',
				width : 250,
				buttons : Ext.MessageBox.YESNO,
				icon : Ext.MessageBox.QUESTION,
				fn : function(btn) {
					if (btn == 'yes') {
						if(!isQT){
							var win = Ext.create('system.zgyxy.course.biz.ReasonEdit');
							win.setTitle('拒绝原因');
							win.modal = true;
							win.grid = grid;
							win.setFid(mainid);
							win.show();
							
						}else{
							var dir = new Array();　
							Ext.Array.each(data, function(items) {
								var id = items.data.id;
								dir.push(id);
							});
							var pram = {ids : dir};
							var param = {jsonData : Ext.encode(pram)};
							me.util.ExtAjaxRequest('z_course/checked_course_qx_coursebj.do' , param ,
									function(response, options){
								Ext.MessageBox.alert('提示', '审核状态修改成功！');
								grid.getStore().reload(); 
							});
						}
						
					}
				}
			});
		}
	},

	import : function(grid, type){
		var win = Ext.create('system.zgyxy.course.biz.CourseImport');
		win.setTitle('课程导入');
		win.type = type;
		win.grid = grid;
		win.modal = true;
		win.show();
	},

	/**
	 * 路演导入表单提交
	 * @param formpanel
	 * @param grid
	 */
	ImportFormSubmit : function(formpanel, grid){
		var me = this;
		if (formpanel.form.isValid()) {
			me.util.ExtFormSubmit('z_course/import_course.do' , formpanel.form , '正在提交数据,请稍候.....',
				function(form, action, respText){
					grid.getStore().reload();
					Ext.MessageBox.alert('提示', '保存成功！');
					formpanel.up('window').close();
				}, null);
		}
	}
});