﻿/**
 * 项目分组导入界面
 * @author lumingbao
 */
Ext.define(	'system.zgyxy.course.biz.CourseImport', {
	extend : 'Ext.window.Window',
	grid : null,
	type: '',
	oper : Ext.create('system.zgyxy.course.biz.CourseContentOper'),
	initComponent:function(){
		var me = this;

		var f_url = Ext.create('system.widget.FsdTextFileManage', {
			width : 450,
			zName : 'f_url',
			zFieldLabel : 'Excel文件',
			zLabelWidth : 90,
			zIsShowButton : false,
			zIsGetButton : false,
			zIsUpButton : true,
			zIsReadOnly : true,
			zFileType: 'file',
			zAllowBlank: false,
			zFileUpPath : me.oper.util.getFilePath()
		});

		var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 20 15 5',
			items : [
				f_url
			]
		});

		Ext.apply(this,{
			layout : 'fit',
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			items :[form],
			buttons : [{
				name : 'btnsave',
				text : '导入',
				iconCls : 'office_excelIcon',
				popedomCode : 'lmbj',
				handler : function() {
					me.oper.ImportFormSubmit(form, me.grid);
				}
			}, {
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	}
});