﻿
/**
 *a_company Columns
 *@author CodeSystem
 */
Ext.define('column_t_course', {
    columns: [
    	{
    		xtype : 'rownumberer',
    		text : 'NO',
    		sortable : true,
    		align : 'center',
    		flex : 0.3
    	}
    	
        ,
        {
            header : '课程名称',
            dataIndex : 'f_name',
            align : 'center',
    		flex : 1.2
        }
        ,
        {
            header : '开课老师',
            dataIndex : 'f_teacher',
            align : 'center',
    		flex : 1
        }
        ,
        {
            header : '状态',
            dataIndex : 'f_state',
            align : 'center',
            flex : 1,
        	renderer : function(value, metaData, record) {
    			if (value == "ztqy") {
    				value = "<font style='color:green;'>启用</font>";
    			} else {
    				value = "<font style='color:red;'>停用</font>";
    			}
    			return value;
    		}
        }
//        ,
//    	{
//            header : '审核状态',
//            dataIndex : 'f_statusid',
//            align : 'center',
//            flex : 1,
//        	renderer : function(value, metaData, record) {
//    			if (value == "jjsh") {
//    				value = "<font style='color:red;'>拒绝审核</font>";
//    			} else if(value == "tgsh"){
//    				value = "<font style='color:green;'>通过审核</font>";
//    			} else{
//    				value = "<font style='color:blank;'>未审核</font>";
//    			}
//    			return value;
//    		}
//        }
        ,
        {
            header : '学分',
            dataIndex : 'f_score',
            align : 'center',
    		flex : 1
        }
    ]
});
