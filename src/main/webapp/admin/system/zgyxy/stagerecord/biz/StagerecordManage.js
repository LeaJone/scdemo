﻿/**
 * 阶段性成果管理界面
 *
 * @author lhl
 */
Ext.define('system.zgyxy.stagerecord.biz.StagerecordManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.zgyxy.stagerecord.model.Extjs_Column_Stagerecord',
	             'system.zgyxy.stagerecord.model.Extjs_Model_Stagerecord'],
	header : false,
	border : 0,
    grid : null,
    projectid : '',
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.zgyxy.stagerecord.biz.StagerecordOper'),
    
	createTable : function() {
	    var me = this;
	    
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_stagerecord',
			model : 'model_z_stagerecord',
			baseUrl : 'z_stagerecord/load_pagedata.do',
			border : false,
			tbar : [{
                xtype : "fsdbutton",
                text : "编辑",
                iconCls : 'page_editIcon',
                popedomCode : 'stagerecordbj',
                handler : function() {
                    me.oper.editor(grid);
                }
            }, {
                xtype : "fsdbutton",
                text : "查看",
                iconCls : 'magnifierIcon',
                handler : function() {
                    me.oper.view(grid);
                }
            }, {
                xtype : "fsdbutton",
                text : "删除",
                iconCls : 'page_deleteIcon',
                popedomCode : 'stagerecordsc',
                handler : function() {
                    me.oper.del(grid);
                }
            }, '-', {
                xtype : "fsdbutton",
                text : "返回上一层",
                iconCls : 'arrow_redoIcon',
                handler : function() {
                    me.oper.goback(me.grid);
                }
            },'->', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]
		});
        grid.getStore().on('beforeload', function(s) {
            var pram = {fid : me.projectid};
            var params = s.getProxy().extraParams;
            Ext.apply(params,{jsonData : Ext.encode(pram)});
        });
		return grid;
	}
});