﻿/**
 * 阶段性成果管理界面
 *
 * @author lhl
 */
Ext.define('system.zgyxy.stagerecord.biz.StagerecordProjectManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.zgyxy.stagerecord.model.Extjs_Column_Stagerecord_Project',
	             'system.zgyxy.stagerecord.model.Extjs_Model_Stagerecord_Project'],
	header : false,
	border : 0,
	subnum : '4',
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.zgyxy.stagerecord.biz.StagerecordOper'),
    
	createTable : function() {
	    var me = this;
	    
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_stagerecord_project',
			model : 'model_z_stagerecord_project',
			baseUrl : 'z_stagerecord/load_projectpagedata.do',
			border : false,
			tbar : [{
                xtype : "fsdbutton",
                text : "提交阶段成果报告",
                iconCls : 'tickIcon',
                popedomCode : 'stagerecordbj',
                handler : function() {
                    me.oper.save(grid);
                }
            }, '-', {
                xtype : "fsdbutton",
                text : "查看阶段成果记录",
                iconCls : 'magnifierIcon',
                handler : function() {
                    me.oper.viewRecord(grid);
                }
            }, '->', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]
		});

		return grid;
	}
});