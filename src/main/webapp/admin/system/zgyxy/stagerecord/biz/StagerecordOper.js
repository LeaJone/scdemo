/**
 * 阶段性成果操作类
 *
 * @author lhl
 */
Ext.define('system.zgyxy.stagerecord.biz.StagerecordOper', {

	form : null,
	util : Ext.create('system.util.util'),

    /**
     * 阶段性成果窗体
     */
    save : function(grid){
        var me = this;
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择要汇报的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '只能选择一个项目！');
        }else{
            var f_leaderid = data[0].data.f_leaderid;
            var loginemp = me.util.getParams("user");
            if(f_leaderid != loginemp.id){
                var win = Ext.create('system.zgyxy.stagerecord.biz.StagerecordAdd');
                win.setTitle('阶段性成果报告');
                win.modal = true;
                win.grid = grid;
                win.code = data[0].data.id;
                win.name = data[0].data.f_name;
                win.show();
            }else{
                Ext.MessageBox.alert('提示', '你不是该项目的管理员，无法提交阶段性成果报告！');
            }
        }
    },

    /**
     * 提交阶段性成果信息
     */
    formSubmit : function(formpanel, grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_stagerecord/save_stagerecord_qx_stagerecordbj.do', formpanel.form, '正在提交数据,请稍候.....', function(){
                    formpanel.form.up('window').close();
                    grid.getStore().reload();
                    Ext.MessageBox.alert('提示', '提交成功！');
                },
                function (form, action){
                    Ext.MessageBox.alert('异常', action.result.message);
                });
        }
    },

    /**
     * 查看阶段性成果Grid
     */
    viewRecord : function(grid){
        var me = this;
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择查看的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '只能查看一个项目！');
        }else{
            var id = data[0].data.id;
            var model = Ext.create("system.zgyxy.stagerecord.biz.StagerecordManage");
            model.grid = grid;
            model.projectid = id;
            me.util.addTab("stagerecordgrid", "阶段性成果-详细记录", model, "fa-shield");
        }
    },

    /**
     * 编辑
     */
    editor : function(grid){
        var me = this;
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择要编辑的阶段性成果报告！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一条数据！');
        }else{
            var id = data[0].data.id;
            var win = Ext.create('system.zgyxy.stagerecord.biz.StagerecordEdit');
            win.setTitle('阶段性成果编辑');
            win.modal = true;
            win.grid = grid;
            win.code = data[0].data.id;
            win.name = data[0].data.f_name;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(win.down('form').getForm(), 'z_stagerecord/load_z_stagerecordbyid.do', param, null, function(){
                win.show();
            });
        }
    },

    /**
     * 查看
     */
    view : function(grid){
        var me = this;
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择要查看的记录！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一条数据！');
        }else{
            var id = data[0].data.id;
            var win = Ext.create('system.zgyxy.stagerecord.biz.StagerecordEdit');
            win.setTitle('阶段性成果查看');
            win.modal = true;
            win.grid = grid;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(win.down('form').getForm(), 'z_stagerecord/load_z_stagerecordbyid.do', param, null, function(){
                win.show();
            });
        }
    },

    /**
     * 删除
     */
    del : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要删除的记录！');
        }else{
            Ext.MessageBox.show({
                title : '提示',
                msg : '你确定要删除所选记录吗?',
                width : 250,
                buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                        var dir = new Array();
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
                        });
                        var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('z_stagerecord/del_stagerecord_qx_stagerecordsc.do', param,
                            function(){
                                Ext.MessageBox.alert('提示', '删除成功！');
                                grid.getStore().reload();
                            }, null, me.form);
                    }
                }
            });
        }
    },

    /**
     * 返回上一层
     */
    goback : function(grid){
        var me = this;
        me.util.closeTab();
        var model = Ext.create("system.zgyxy.stagerecord.biz.StagerecordProjectManage");
        me.util.addTab("stagerecord", "阶段性成果", model, "fa-imdb");
        grid.getStore().reload();
    }

});