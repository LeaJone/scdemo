﻿Ext.define('model_z_stagerecord_project', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'id',
            type: 'string'
        }
        ,
        {
            name: 'f_code',
            type: 'string'
        }
        ,
        {
            name: 'f_name',
            type: 'string'
        }
        ,
        {
            name: 'f_typename',
            type: 'string'
        }
        ,
        {
            name: 'f_typeid1',
            type: 'string'
        }
        ,
        {
            name: 'f_typename1',
            type: 'string'
        }
        ,
        {
            name: 'f_typeid2',
            type: 'string'
        }
        ,
        {
            name: 'f_typename2',
            type: 'string'
        }
        ,
        {
            name: 'f_typeid3',
            type: 'string'
        }
        ,
        {
            name: 'f_typename3',
            type: 'string'
        }
        ,
        {
            name: 'f_teachername',
            type: 'string'
        }
        ,
        {
            name: 'f_leaderid',
            type: 'string'
        }
        ,
        {
            name: 'f_leadername',
            type: 'string'
        }
        ,
        {
            name: 'f_statusname',
            type: 'string'
        }
        ,
        {
            name: 'f_adddate',
            type: 'string'
        }
        ,
        {
            name: 'f_subnum',
            type: 'string'
        }
    ]
});
