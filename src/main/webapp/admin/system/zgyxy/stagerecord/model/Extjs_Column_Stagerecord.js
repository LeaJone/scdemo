﻿Ext.define('column_z_stagerecord', {
    columns: [
        {
            xtype: 'rownumberer',
            text: '',
            sortable: true,
            width: 40,
            align: 'left'
        }
        ,
        {
            header: '项目名称',
            dataIndex: 'f_name',
            sortable: true,
            align: 'center',
            flex: 3
        }
        ,
        {
            header: '提交时间',
            dataIndex: 'f_subtime',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                return system.UtilStatic.formatDateTime(value);
            }
        }
        ,
        {
            header: '提交人',
            dataIndex: 'f_user',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '附件',
            dataIndex: 'f_file',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                if(value != "" && value != null){
                    value = "<font style='color:blue;'>有</font>"
                }else{
                    value = ""
                }
                return value;
            }
        }

    ]
});
