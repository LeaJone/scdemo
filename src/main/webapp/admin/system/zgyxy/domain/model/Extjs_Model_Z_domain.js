﻿Ext.define('model_z_domain', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'subjectid',
            type : 'string'
        }
        ,
        {
            name : 'subjectname',
            type : 'string'
        }
        ,
        {
            name : 'parentid',
            type : 'string'
        }
        ,
        {
            name : 'parentname',
            type : 'string'
        }
        ,
        {
            name : 'code',
            type : 'string'
        }
        ,
        {
            name : 'title',
            type : 'string'
        }
        ,
        {
            name : 'imageurl1',
            type : 'string'
        }
        ,
        {
            name : 'sort',
            type : 'int'
        }
        ,
        {
            name : 'statusname',
            type : 'string'
        }
        ,
        {
            name : 'column1',
            type : 'string'
        }
        ,
        {
            name : 'column3',
            type : 'string'
        }
        ,
        {
            name : 'isaurl',
            type : 'string'
        }
        ,
        {
            name : 'ispublish',
            type : 'string'
        }
        ,
        {
            name : 'issingle',
            type : 'string'
        }
        ,
        {
            name : 'describes',
            type : 'string'
        }
        ,
        {
        	name : 'levelpath',
            type : 'string'
        }
    ]
});
