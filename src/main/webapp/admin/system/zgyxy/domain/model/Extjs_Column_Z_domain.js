﻿Ext.define('column_z_domain', {
    columns: [
          {
              xtype : 'rownumberer',
              text : 'NO',
              sortable : true,
              width : 40
          }
        ,
        {
            header : '学科代码',
            dataIndex : 'code',
            sortable : true,
            flex : 1
        }
          ,
        {
            header : '学科名称',
            dataIndex : 'title',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '所属分类',
            dataIndex : 'parentname',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '顺序号',
            dataIndex : 'sort',
            sortable : true,
            align : 'center',
            flex : 1
        }
        ,
        {
            header : '状态',
            dataIndex : 'statusname',
            sortable : true,
            align : 'center',
            flex : 1
        }
        ,
        {
            header : '描述',
            dataIndex : 'describes',
            sortable : true,
            flex : 2
        }
    ]
});



Ext.define('column_z_domainxg', {
    columns: [
        {
            header : '标题',
            dataIndex : 'title',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '所属栏目',
            dataIndex : 'parentname',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '顺序号',
            dataIndex : 'sort',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '备注',
            dataIndex : 'remark',
            sortable : true,
            flex : 2
        }
    ]
});



Ext.define('column_b_domainzw', {
    columns: [
        {
            header : '标题',
            dataIndex : 'title',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '对应栏目',
            dataIndex : 'subjectname',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '所属栏目',
            dataIndex : 'parentname',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '顺序号',
            dataIndex : 'sort',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '外链接',
            dataIndex : 'isaurl',
            sortable : true,
            flex : 1,
			renderer :function(value ,metaData ,record ){
				if(value == 'true'){
					//metaData.style='color:blue;text-decoration:underline;cursor:pointer;';下划线
					value="<font style='color:blue;'>启用</font>";
				}else{
					value="<font style='color:red;'></font>";
				}
				return value;
			}
        }
        ,
        {
            header : '备注',
            dataIndex : 'remark',
            sortable : true,
            flex : 1
        }
    ]
});


Ext.define('column_z_domainxz', {
    columns: [
          {
              xtype : 'rownumberer',
              text : 'NO',
              sortable : true,
              width : 40
          }
        ,
        {
            header : '学科代码',
            dataIndex : 'code',
            sortable : true,
            width : '20%'
        }
          ,
          {
              header : '学科名称',
              dataIndex : 'levelpath',
              sortable : true,
              width : '35%',
			  renderer :function(value ,metaData ,record){
				  var num = Number(value);
				  var title = '';
				  for ( var int = 1; int < num; int++) {
					  if (int == num - 1){
						  title += '　　|-';
					  }else{
						  title += '　　';
					  }
				  }
				  return title + record.data.title;
			  }
          }
          ,
          {
              header : '结构',
              dataIndex : 'column3',
              sortable : true,
              width : '65%'
          }
    ]
});
