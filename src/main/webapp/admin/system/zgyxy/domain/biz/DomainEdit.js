﻿/**
 * 栏目添加界面
 */
Ext.define('system.zgyxy.domain.biz.DomainEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	treepanel : null,
	oper : Ext.create('system.zgyxy.domain.biz.DomainOper'),
	initComponent:function(){
	    var me = this;
	    
	    var gjid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '根级ID',
	    	name: 'baseid',
	    	value: 'FSDMAIN'
	    });
	    
	    var sslmname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属一级学科名称',
	    	name: 'parentname'
	    });
	    var sslm = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属领域',
	    	labelAlign:'right',
	    	labelWidth:80,
	    	rootText : '一级学科',
	    	rootId : 'FSDMAIN',
	        width : 360,
	        name : 'parentid',
	        baseUrl:'z_domain/load_AsyncDomainTreeQuery.do',//访问路劲
	        allowBlank: false,
	        hiddenName : 'parentname'//隐藏域Name
	    });

		var xkdm = new Ext.create('system.widget.FsdTextField',{
			fieldLabel: '名称代码',
			labelAlign:'right',
			labelWidth:80,
			width : 360,
			name: 'code',
			maxLength: 25,
			allowBlank: false
		});
	    
	    var lmmc = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '学科名称',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'title',
	    	maxLength: 25,
	    	allowBlank: false
	    });
	    
	    var pxbh = new Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '排序编号',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'sort',
	    	maxLength: 4,
	    	minValue: 0,
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank: false
	    });
	    
	    var describes = new Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '学科描述',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'describes',
	    	maxLength: 250
	    });
	    
	    me.on('show' , function(){
	    	sslm.setText(sslmname.getValue());
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '15 15 10 5',
			items : [{
                name: "id",
                xtype: "hidden"
            } , gjid, sslmname, sslm, xkdm, lmmc, , pxbh, describes]
        });
	    
	    Ext.apply(this,{
            layout : 'fit',
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
				popedomCode : 'domainbj',
			    handler : function() {
    				me.oper.submitMain(form, me.grid , me.treepanel);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});