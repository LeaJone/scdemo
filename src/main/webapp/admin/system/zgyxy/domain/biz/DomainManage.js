﻿Ext.define('system.zgyxy.domain.biz.DomainManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.zgyxy.domain.model.Extjs_Column_Z_domain',
	             'system.zgyxy.domain.model.Extjs_Model_Z_domain',
	             'system.admin.tools.model.Extjs_Column_SortOperate',
		         'system.admin.tools.model.Extjs_Model_SortOperate'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.zgyxy.domain.biz.DomainOper'),
	
	createContent : function(){
		var me = this;
		
		var queryid = 'FSDMAIN';
		
		/**
		 * 左边的树面板
		 */
		var treepanel = Ext.create("system.widget.FsdTreePanel", {
			icon : 'resources/admin/icon/chart_organisation.png',
			baseUrl : 'z_domain/load_AsyncDomainTreeQuery.do',
			title: '领域树菜单',
			rootText : '学科结构',
			rootId : 'FSDMAIN',
			region : 'west', // 设置方位
			collapsible : true,//折叠
			width: 300,
			split: true
		});
		
		
		var queryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入领域名称',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});
		
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_domain', // 显示列
			model: 'model_z_domain',
			baseUrl : 'z_domain/load_data.do',
			tbar : [{
				xtype : "button",
				text : "添加",
				iconCls : 'page_addIcon',
				popedomCode : 'domainbj',
				handler : function() {
					me.oper.addMain(grid , treepanel);
				}
			}, {
				xtype : "button",
				text : "修改",
				iconCls : 'page_editIcon',
				popedomCode : 'domainbj',
				handler : function() {
					me.oper.editorMain(grid , treepanel);
				}
			}, {
				xtype : "button",
				text : "删除",
				iconCls : 'page_deleteIcon',
				popedomCode : 'domainsc',
				handler : function() {
				    me.oper.delMain(grid , treepanel);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.viewMain(grid);
				}
			}, '-', {
				xtype : "fsdbutton",
				popedomCode : 'domainbj',
				text : "启用",
				iconCls : 'tickIcon',
				handler : function() {
				    me.oper.isEnabledMain(grid, true);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'domainbj',
				text : "停用",
				iconCls : 'crossIcon',
				handler : function() {
				    me.oper.isEnabledMain(grid, false);
				}
			}, '->', queryText , {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  queryText.getValue();
			var pram = {fid : queryid , name : name};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		var contentpanel = Ext.create("Ext.panel.Panel", {
			frame:true,
			split: true,
			region: 'center',
			layout: 'fit',
			title: '领域信息',
			items: [grid]
		});

		var page1_jExtPanel1_obj = new Ext.panel.Panel({
			bodyPadding: 3,
			header : false,
			border : 0,
			split: true,
			layout: 'border',
			items: [treepanel, contentpanel]
		});
		
		treepanel.on('itemclick', function(view,record){
			queryid = record.raw.id;
			grid.getStore().loadPage(1);
		});
		return page1_jExtPanel1_obj;
	}
});