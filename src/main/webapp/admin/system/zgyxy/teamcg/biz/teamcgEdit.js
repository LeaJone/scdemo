/**
 * 团队成果编辑界面
 */
Ext.define('system.zgyxy.teamcg.biz.teamcgEdit', {
	extend : 'Ext.panel.Panel',
	teamid : '',
	teamname : '',
	type : '',
	oper : Ext.create('system.zgyxy.teamcg.biz.teamcgOper'),
	initComponent:function(){
	    var me = this;

        var id = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '编号',
            name : 'id'
        });
	    
	    var teamid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '团队编号',
	    	name: 'teamid',
			value: me.teamid
	    });

        var teamname = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '团队名称',
            name: 'teamname',
			value: me.teamname
        });

        var type = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '类型',
            name: 'type',
			value: me.type
        });

        var content = Ext.create('system.widget.FsdTextArea',{
            fieldLabel: '内容',
            labelAlign:'right',
            labelWidth:60,
            width : 800,
            height : 300,
            name: 'content'
        });

        var file = Ext.create('system.widget.FsdTextFileManage', {
            width : 800,
            zName : 'file',
            zFieldLabel : '附件',
            zLabelWidth : 60,
            zIsUpButton : true,
            zIsShowButton : true,
            zIsDownButton : true,
            zFileUpPath : me.oper.util.getFilePath()
        });

        me.form = Ext.create('Ext.form.Panel',{
            height : 360,
            border : false,
            padding : '15 15 10 5',
            items : [ id, teamid, teamname, type, content, file ]
        });
	    
	    Ext.apply(me,{
	        layout: 'fit',
            tbar : [{
                xtype : "fsdbutton",
                popedomCode : 'teamcgbj',
                text : "保 存",
                iconCls : 'acceptIcon',
                handler : function() {
                    me.oper.formSubmit(me);
                }
            }],
            items :[me.form]
	    });
	    me.callParent(arguments);
	}
});