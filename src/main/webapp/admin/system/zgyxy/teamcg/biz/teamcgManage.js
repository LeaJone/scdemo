﻿/**
 * 团队成果管理界面
 */
Ext.define('system.zgyxy.teamcg.biz.teamcgManage', {
	extend : 'Ext.window.Window',
    border : false,
    teamid : '',
    teamname : '',
	oper : Ext.create('system.zgyxy.teamcg.biz.teamcgOper'),

    initComponent : function(){
		var me = this;

        var teamidtemp = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '团队编号'
        });

        var teamnametemp = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '团队名称'
        });

        me.on('boxready', function(){
            teamidtemp.setValue(me.teamid);
            teamnametemp.setValue(me.teamname);
        });

        var teamcgid = me.oper.util.getParams("teamcg_id");
        var teamcgname = me.oper.util.getParams("teamcg_mc");

	    //团队成果
	    var teamcgform = Ext.create("system.zgyxy.teamcg.biz.teamcgEdit", {
            teamid : teamcgid,
            teamname : teamcgname,
            type : '成果'
		});
        me.on('boxready', function() {
            var pram = {
                teamid : teamidtemp.getValue(),
                type : "成果"
            };
            var param = {
                jsonData : Ext.encode(pram)
            };
            me.oper.util.formLoad(teamcgform.form.getForm(), 'z_teamcg/load_z_teamcgbyid.do', param, null, function(response, options, respText) {});
        });

        //企业论文
        var teamlwform = Ext.create("system.zgyxy.teamcg.biz.teamcgEdit", {
            teamid : teamcgid,
            teamname : teamcgname,
            type : '论文'
        });
        me.on('boxready', function() {
            var pram = {
                teamid : teamidtemp.getValue(),
                type : "论文"
            };
            var param = {
                jsonData : Ext.encode(pram)
            };
            me.oper.util.formLoad(teamlwform.form.getForm(), 'z_teamcg/load_z_teamcgbyid.do', param, null, function(response, options, respText) {});
        });

        //企业专利
        var teamzlform = Ext.create("system.zgyxy.teamcg.biz.teamcgEdit", {
            teamid : teamcgid,
            teamname : teamcgname,
            type : '专利'
        });
        me.on('boxready', function() {
            var pram = {
                teamid : teamidtemp.getValue(),
                type : "专利"
            };
            var param = {
                jsonData : Ext.encode(pram)
            };
            me.oper.util.formLoad(teamzlform.form.getForm(), 'z_teamcg/load_z_teamcgbyid.do', param, null, function(response, options, respText) {});
        });

        //企业著作权
        var teamzzqform = Ext.create("system.zgyxy.teamcg.biz.teamcgEdit", {
            teamid : teamcgid,
            teamname : teamcgname,
            type : '著作权'
        });
        me.on('boxready', function() {
            var pram = {
                teamid : teamidtemp.getValue(),
                type : "著作权"
            };
            var param = {
                jsonData : Ext.encode(pram)
            };
            me.oper.util.formLoad(teamzzqform.form.getForm(), 'z_teamcg/load_z_teamcgbyid.do', param, null, function(response, options, respText) {});
        });

        // 项目
        var teamxmform = Ext.create("system.zgyxy.teamcg.biz.teamcgEdit", {
            teamid : teamcgid,
            teamname : teamcgname,
            type : '项目'
        });
        me.on('boxready', function() {
            var pram = {
                teamid : teamidtemp.getValue(),
                type : "项目"
            };
            var param = {
                jsonData : Ext.encode(pram)
            };
            me.oper.util.formLoad(teamxmform.form.getForm(), 'z_teamcg/load_z_teamcgbyid.do', param, null, function(response, options, respText) {});
        });

        //获奖
        var teamhjform = Ext.create("system.zgyxy.teamcg.biz.teamcgEdit", {
            teamid : teamcgid,
            teamname : teamcgname,
            type : '获奖'
        });
        me.on('boxready', function() {
            var pram = {
                teamid : teamidtemp.getValue(),
                type : "获奖"
            };
            var param = {
                jsonData : Ext.encode(pram)
            };
            me.oper.util.formLoad(teamhjform.form.getForm(), 'z_teamcg/load_z_teamcgbyid.do', param, null, function(response, options, respText) {});
        });

	    /**
	     * 右下方的大选项卡控件
	     */
	    var tabPanel = new Ext.tab.Panel({
			layout : 'fit',
			activeTab: 0,
			split: true,//拖动
			deferredRender: true,//是否在显示每个标签的时候再渲染标签中的内容.默认true
			items: [
				{
					xtype: 'panel',
					layout: 'fit',
					title: '成果',
					items: [
                        teamcgform
					]},{
						xtype: 'panel',
						layout: 'fit',
						title: '论文',
						items: [
                            teamlwform
					]},{
						xtype: 'panel',
						layout: 'fit',
						title: '专利',
						items: [
                            teamzlform
					]},{
						xtype: 'panel',
						layout: 'fit',
						title: '著作权',
						items: [
                            teamzzqform
                    ]},{
                        xtype: 'panel',
                        layout: 'fit',
                        title: '项目',
                        items: [
                            teamxmform
                    ]},{
                        xtype: 'panel',
                        layout: 'fit',
                        title: '获奖',
                        items: [
                            teamhjform
                    ]}
			]
		});

        var buttonItem = [{
            text : '关闭',
            iconCls : 'deleteIcon',
            handler : function() {
                me.close();
            }
        }];
        Ext.apply(this,{
            layout : 'fit',
            resizable:false,// 改变大小
            bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
            items :[tabPanel],
            buttons : buttonItem
        });
        this.callParent(arguments);
	}
});