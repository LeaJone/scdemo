/**
 * 团队成果管理界面操作类
 *
 * @author lihanlin
 */

Ext.define('system.zgyxy.teamcg.biz.teamcgOper', {

	form : null,
	util : Ext.create('system.util.util'),
      
	/**
     * 表单提交
     */
     formSubmit : function(formpanel){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_teamcg/save_teamcg_qx_teamcgbj.do', formpanel.form, '正在提交数据,请稍候.....',
			function(){
				Ext.MessageBox.alert('提示', '保存成功！');
			});
        }
     }

});