/**
 * 模板操作类
 * @author lmb
 */
Ext.define('system.zgyxy.qyrzjl.biz.QyrzjlOper', {

	form : null,
	util : Ext.create('system.util.util'),

    /**
     * 查看
     */
    view : function(grid){
        var me = this;
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择要查看的记录！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一条数据！');
        }else{
            var id = data[0].data.id;
            var win = Ext.create('system.zgyxy.qyrzjl.biz.QyrzjlEdit');
            win.setTitle('企业入驻记录查看');
            win.modal = true;
            win.grid = grid;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(win.down('form').getForm(), 'z_qyrzjl/load_qyrzjl.do', param, null, function(){
                win.show();
            });
        }
    }

});