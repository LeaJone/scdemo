﻿/**
 * 企业入驻记录管理界面
 *
 * @author lhl
 */
Ext.define('system.zgyxy.qyrzjl.biz.QyrzjlManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.zgyxy.qyrzjl.model.Extjs_Column_Qyrzjl',
	             'system.zgyxy.qyrzjl.model.Extjs_Model_Qyrzjl'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.zgyxy.qyrzjl.biz.QyrzjlOper'),
    
	createTable : function() {
	    var me = this;

        /**
         * 查询文本框
         */
        var queryText =  Ext.create("Ext.form.TextField" , {
            name : 'queryParam',
            emptyText : '请输入企业名称',
            enableKeyEvents : true,
            width : 130,
            listeners : {
                specialkey : function(field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        grid.getStore().reload();
                    }
                }
            }
        });
	    
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_qyrzjl',
			model : 'model_z_qyrzjl',
			baseUrl : 'z_qyrzjl/load_pagedata.do',
			border : false,
			tbar : [{
                text : '查看详情',
                iconCls : 'magnifierIcon',
                handler : function() {
                    me.oper.view(grid);
                }
            }, '->', queryText, {
                text: '记录查询',
                iconCls: 'magnifierIcon',
                handler: function () {
                    grid.getStore().reload();
                }
            }, {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]
		});
		grid.getStore().on('beforeload', function(s) {
            var text =  queryText.getValue();
            var pram = {qymc : text};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		return grid;
	}
});