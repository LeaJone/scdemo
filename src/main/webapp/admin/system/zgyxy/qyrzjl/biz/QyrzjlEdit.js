/**
 * 企业记录编辑界面
 *
 * @author lhl
 */
Ext.define('system.zgyxy.qyrzjl.biz.QyrzjlEdit',{
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.zgyxy.qyrzjl.biz.QyrzjlOper'),
    initComponent:function(){
        var me = this;

        var qymc = new Ext.create('system.widget.FsdTextField',{
            fieldLabel: '企业名称',
            labelAlign:'right',
            labelWidth:80,
            width : 340,
            name: 'qymc'
        });

        var type = Ext.create('system.widget.FsdTextField',{
            fieldLabel:'操作类型',
            labelAlign:'right',
            labelWidth:80,
            width : 340,
            name : 'type'
        });

        var time = Ext.create('system.widget.FsdDateTime',{
            fieldLabel: '操作时间',
            labelAlign:'right',
            labelWidth:80,
            width : 340,
            name: 'time'
        });

        var remark = Ext.create('system.widget.FsdTextArea',{
            fieldLabel: '操作详情',
            labelAlign:'right',
            labelWidth:80,
            width : 340,
            name: 'remark'
        });

        var form = Ext.create('Ext.form.Panel',{
            xtype : 'form',
            header : false,
            border : false,
            padding : '20 20 15 20',
            items : [
                {
                    name: "id",
                    xtype: "hidden"
                }, qymc, type, time, remark ]
        });

        var buttonItem = [{
            text : '关闭',
            iconCls : 'deleteIcon',
            handler : function() {
                me.close();
            }
        }];
        Ext.apply(this,{
            layout : 'fit',
            bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
            resizable:false,// 改变大小
            items :[form],
            buttons : buttonItem
        });
        this.callParent(arguments);
    }
});