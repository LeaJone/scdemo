﻿Ext.define('column_z_qyrzjl', {
    columns: [
        {
            xtype: 'rownumberer',
            text: '',
            sortable: true,
            width: 30,
            align: 'left'
        }
        ,
        {
            header: '企业名称',
            dataIndex: 'qymc',
            sortable: true,
            align: 'center',
            flex: 1

        }
        ,
        {
            header: '操作类型',
            dataIndex: 'type',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '操作时间',
            dataIndex: 'time',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                return system.UtilStatic.formatDateTime(value);
            }
        }
        ,
        {
            header: '备注',
            dataIndex: 'remark',
            sortable: true,
            align: 'center',
            flex: 3
        }
    ]
});
