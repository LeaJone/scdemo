﻿Ext.define('model_z_qyrzjl', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'id',
            type: 'string'
        }
        ,
        {
            name: 'qymc',
            type: 'string'
        }
        ,
        {
            name: 'time',
            type: 'string'
        }
        ,
        {
            name: 'type',
            type: 'string'
        }
        ,
        {
            name: 'remark',
            type: 'string'
        }
    ]
});
