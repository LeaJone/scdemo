/**
 * 周进展记录操作类
 *
 * @author lhl
 */
Ext.define('system.zgyxy.weekrecord.biz.WeekrecordOper', {

	form : null,
	util : Ext.create('system.util.util'),

    /**
     * 周进展窗体
     */
    save : function(grid){
        var me = this;
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择要汇报的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '只能选择一个项目！');
        }else{
            var f_leaderid = data[0].data.f_leaderid;
            var loginemp = me.util.getParams("user");
            if(f_leaderid != loginemp.id){
                var win = Ext.create('system.zgyxy.weekrecord.biz.WeekrecordAdd');
                win.setTitle('项目周进展报告');
                win.modal = true;
                win.grid = grid;
                win.code = data[0].data.id;
                win.name = data[0].data.f_name;
                win.show();
            }else{
                Ext.MessageBox.alert('提示', '你不是该项目的管理员，无法提交周进展报告！');
            }
        }
    },

    /**
     * 提交周进展信息
     */
    formSubmit : function(formpanel, grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_weekrecord/save_weekrecord_qx_weekrecordbj.do', formpanel.form, '正在提交数据,请稍候.....', function(){
                    formpanel.form.up('window').close();
                    grid.getStore().reload();
                    Ext.MessageBox.alert('提示', '提交成功！');
                },
                function (form, action){
                    Ext.MessageBox.alert('异常', action.result.message);
                });
        }
    },

    /**
     * 查看周进展记录Grid
     */
    viewRecord : function(grid){
        var me = this;
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择查看的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '只能查看一个项目！');
        }else{
            var id = data[0].data.id;
            var model = Ext.create("system.zgyxy.weekrecord.biz.WeekrecordManage");
            model.grid = grid;
            model.projectid = id;
            me.util.addTab("weekrecordgrid", "周进展记录-详细记录", model, "fa-ravelry");
        }
    },

    /**
     * 编辑
     */
    editor : function(grid){
        var me = this;
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择要编辑的周进展报告！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一条数据！');
        }else{
            var id = data[0].data.id;
            var win = Ext.create('system.zgyxy.weekrecord.biz.WeekrecordEdit');
            win.setTitle('周进展报告编辑');
            win.modal = true;
            win.grid = grid;
            win.code = data[0].data.id;
            win.name = data[0].data.f_name;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(win.down('form').getForm(), 'z_weekrecord/load_z_weekrecordbyid.do', param, null, function(){
                win.show();
            });
        }
    },

    /**
     * 查看
     */
    view : function(grid){
        var me = this;
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择要查看的记录！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一条数据！');
        }else{
            var id = data[0].data.id;
            var win = Ext.create('system.zgyxy.weekrecord.biz.WeekrecordEdit');
            win.setTitle('周进展记录查看');
            win.modal = true;
            win.grid = grid;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(win.down('form').getForm(), 'z_weekrecord/load_z_weekrecordbyid.do', param, null, function(){
                win.show();
            });
        }
    },

    /**
     * 删除
     */
    del : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要删除的记录！');
        }else{
            Ext.MessageBox.show({
                title : '提示',
                msg : '你确定要删除所选记录吗?',
                width : 250,
                buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                        var dir = new Array();
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
                        });
                        var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('z_weekrecord/del_weekrecord_qx_weekrecordsc.do', param,
                            function(){
                                Ext.MessageBox.alert('提示', '删除成功！');
                                grid.getStore().reload();
                            }, null, me.form);
                    }
                }
            });
        }
    },

    /**
     * 返回上一层
     */
    goback : function(grid){
        var me = this;
        var model = Ext.create("system.zgyxy.weekrecord.biz.WeekrecordProjectManage");
        me.util.closeTab();
        me.util.addTab("weekrecord", "周进展记录", model, "fa-microchip");
        grid.getStore().reload();
    }

});