﻿/**
 * 周进展记录管理界面
 *
 * @author lhl
 */
Ext.define('system.zgyxy.weekrecord.biz.WeekrecordProjectManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.zgyxy.weekrecord.model.Extjs_Column_Weekrecord_Project',
	             'system.zgyxy.weekrecord.model.Extjs_Model_Weekrecord_Project'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.zgyxy.weekrecord.biz.WeekrecordOper'),
    
	createTable : function() {
	    var me = this;
	    
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_weekrecord_project',
			model : 'model_z_weekrecord_project',
			baseUrl : 'z_weekrecord/load_projectpagedata.do',
			border : false,
			tbar : [{
                xtype : "fsdbutton",
                text : "提交周进展报告",
                iconCls : 'tickIcon',
                popedomCode : 'weekrecordbj',
                handler : function() {
                    me.oper.save(grid);
                }
            }, '-', {
                xtype : "fsdbutton",
                text : "查看周进展记录",
                iconCls : 'magnifierIcon',
                handler : function() {
                    me.oper.viewRecord(grid);
                }
            }, '->', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]
		});

		return grid;
	}
});