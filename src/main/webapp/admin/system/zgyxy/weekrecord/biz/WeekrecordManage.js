﻿/**
 * 周进展记录管理界面
 *
 * @author lhl
 */
Ext.define('system.zgyxy.weekrecord.biz.WeekrecordManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.zgyxy.weekrecord.model.Extjs_Column_Weekrecord',
	             'system.zgyxy.weekrecord.model.Extjs_Model_Weekrecord'],
	header : false,
    grid : null,
	border : 0,
    projectid : '',
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.zgyxy.weekrecord.biz.WeekrecordOper'),
    
	createTable : function() {
	    var me = this;
	    
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_weekrecord',
			model : 'model_z_weekrecord',
			baseUrl : 'z_weekrecord/load_pagedata.do',
			border : false,
			tbar : [{
                xtype : "fsdbutton",
                text : "编辑",
                iconCls : 'page_editIcon',
                popedomCode : 'weekrecordbj',
                handler : function() {
                    me.oper.editor(grid);
                }
            }, {
                xtype : "fsdbutton",
                text : "查看",
                iconCls : 'magnifierIcon',
                handler : function() {
                    me.oper.view(grid);
                }
            }, {
                xtype : "fsdbutton",
                text : "删除",
                iconCls : 'page_deleteIcon',
                popedomCode : 'weekrecordsc',
                handler : function() {
                    me.oper.del(grid);
                }
            }, '-', {
                xtype : "fsdbutton",
                text : "返回上一层",
                iconCls : 'arrow_redoIcon',
                handler : function() {
                    me.oper.goback(me.grid);
                }
            },'->', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]
		});
        grid.getStore().on('beforeload', function(s) {
            var pram = {fid : me.projectid};
            var params = s.getProxy().extraParams;
            Ext.apply(params,{jsonData : Ext.encode(pram)});
        });
		return grid;
	}
});