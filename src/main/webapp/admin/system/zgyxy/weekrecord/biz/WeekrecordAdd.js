/**
 * 周进展记录编辑界面
 *
 * @author lhl
 */
Ext.define('system.zgyxy.weekrecord.biz.WeekrecordAdd',{
	extend : 'Ext.window.Window',
	grid : null,
    code : '',
    name : '',
	oper : Ext.create('system.zgyxy.weekrecord.biz.WeekrecordOper'),

    initComponent:function(){
        var me = this;

        var f_code = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '项目编号',
            name: 'f_code'
        });

        var f_name = new Ext.create('system.widget.FsdTextField',{
            fieldLabel: '项目名称',
            labelAlign:'right',
            labelWidth:80,
            width : 660,
            name: 'f_name',
            readOnly: true
        });

        var f_content = Ext.create('system.widget.FsdUeditor',{
            fieldLabel: '详细内容',
            labelAlign:'right',
            labelWidth:80,
            width : 680,
            height : 300,
            name: 'f_content',
            allowBlank: false,
            padding: '0 0 45 0'
        });

        var f_file = Ext.create('system.widget.FsdTextFileManage', {
            width : 660,
            zName : 'f_file',
            zFieldLabel : '附件',
            zLabelWidth : 80,
            zIsUpButton : true,
            zIsDownButton : true,
            zIsShowButton : true,
            zFileUpPath : me.oper.util.getFilePath()
        });

        var panel = Ext.create('Ext.form.FieldSet',{
            title : '项目周进展信息',
            items:[{
                border : false,
                items:[f_code, f_name, f_content, f_file]
            }]
        });

        me.on('boxready', function(){
            f_code.setValue(me.code);
            f_name.setValue(me.name);
        });

        me.form = Ext.create('Ext.form.Panel',{
            xtype : 'form',
            header : false,
            border : false,
            padding : '15 20 15 20',
            items : [
                {
                    name: "id",
                    xtype: "hidden"
                }, panel ]
        });

        var buttonItem = [{
            xtype : "fsdbutton",
            popedomCode : 'weekrecordbj',
            name : 'btnsave',
            text : '提交',
            zIsSpace : true,
            iconCls : 'acceptIcon',
            handler : function() {
            	me.oper.formSubmit(me, me.grid);
            }
        }, {
            text : '关闭',
            iconCls : 'deleteIcon',
            handler : function() {
                me.close();
            }
        }];
        Ext.apply(this,{
            layout : 'fit',
            bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
            resizable:false,// 改变大小
            items :[me.form],
            buttons : buttonItem
        });
        this.callParent(arguments);
    }
});