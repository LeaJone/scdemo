﻿Ext.define('model_z_weekrecord', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'id',
            type: 'string'
        }
        ,
        {
            name: 'f_code',
            type: 'string'
        }
        ,
        {
            name: 'f_name',
            type: 'string'
        }
        ,
        {
            name: 'f_subtime',
            type: 'string'
        }
        ,
        {
            name: 'f_user',
            type: 'string'
        }
        ,
        {
            name: 'f_content',
            type: 'string'
        }
        ,
        {
            name: 'f_file',
            type: 'string'
        }
        ,
        {
            name: 'f_delete',
            type: 'string'
        }
    ]
});
