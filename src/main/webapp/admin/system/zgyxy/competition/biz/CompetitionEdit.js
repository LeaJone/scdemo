﻿/**
 * 竞赛添加界面
 * @author lumingbao
 */
Ext.define('system.zgyxy.competition.biz.CompetitionEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.zgyxy.competition.biz.CompetitionOper'),
	initComponent:function(){
		var me = this;

		var f_name = new Ext.create('system.widget.FsdTextField',{
			fieldLabel: '竞赛名称',
			labelAlign:'right',
			labelWidth:90,
			width : 360,
			name: 'f_name',
			maxLength: 25,
			allowBlank: false
		});

		var f_typeid = Ext.create('system.widget.FsdComboBoxZD',{
			fieldLabel:'竞赛类型',
			labelAlign:'right',
			labelWidth:90,
			width : 360,
			name : 'f_typeid',//提交到后台的参数名
			key : 'jslx',//参数
			allowBlank: false,
			hiddenName : 'f_typename'//提交隐藏域Name
		});
		var f_typename = Ext.create('Ext.form.field.Hidden',{
			fieldLabel: '竞赛类型名称',
			name: 'f_typename'
		});

		var f_levelid = Ext.create('system.widget.FsdComboBoxZD',{
			fieldLabel:'竞赛级别',
			labelAlign:'right',
			labelWidth:90,
			width : 360,
			name : 'f_levelid',//提交到后台的参数名
			key : 'jsjb',//参数
			allowBlank: false,
			hiddenName : 'f_levelname'//提交隐藏域Name
		});
		var f_levelname = Ext.create('Ext.form.field.Hidden',{
			fieldLabel: '竞赛级别名称',
			name: 'f_levelname'
		});

		var f_sponsor = new Ext.create('system.widget.FsdTextField',{
			fieldLabel: '主办方',
			labelAlign:'right',
			labelWidth:90,
			width : 360,
			name: 'f_sponsor',
			maxLength: 25,
			allowBlank: false
		});

		var f_imgurl = Ext.create('system.widget.FsdTextFileManage', {
			width : 360,
			zName : 'f_imgurl',
			zFieldLabel : '竞赛导图',
			zLabelWidth : 90,
			zIsShowButton : true,
			zIsUpButton : true,
			zIsReadOnly : false,
			zContentObj : me.content,
			zFileUpPath : me.oper.util.getImagePath(),
			zAllowBlank: false
		});

		var f_starttime = Ext.create('system.widget.FsdDateField',{
			fieldLabel: '开始时间',
			labelAlign:'right',
			labelWidth:90,
			width : 360,
			name: 'f_starttime',
			allowBlank: false
		});

		var f_stoptime = Ext.create('system.widget.FsdDateField',{
			fieldLabel: '结束时间',
			labelAlign:'right',
			labelWidth:90,
			width : 360,
			name: 'f_stoptime',
			allowBlank: false
		});

		var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '15 10 5 10',
			items : [{
				name: "id",
				xtype: "hidden"
			} , f_name,
				f_typeid,
				f_typename,
				f_levelid,
				f_levelname,
				f_sponsor,
				f_imgurl,
				f_starttime,
				f_stoptime]
		});

		Ext.apply(this,{
			layout : 'fit',
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			items :[form],
			buttons : [{
				name : 'btnsave',
				text : '保存',
				iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
				handler : function() {
					me.oper.formSubmit(form, me.grid);
				}
			}, {
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	}
});