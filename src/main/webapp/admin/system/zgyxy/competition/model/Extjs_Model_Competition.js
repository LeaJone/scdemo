﻿Ext.define('model_z_competition', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'id',
            type: 'string'
        }
        ,
        {
            name: 'f_name',
            type: 'string'
        }
        ,
        {
            name: 'f_typeid',
            type: 'string'
        }
        ,
        {
            name: 'f_typename',
            type: 'string'
        }
        ,
        {
            name: 'f_starttime',
            type: 'string'
        }
        ,
        {
            name: 'f_stoptime',
            type: 'string'
        }
        ,
        {
            name: 'f_levelid',
            type: 'string'
        }
        ,
        {
            name: 'f_levelname',
            type: 'string'
        }
        ,
        {
            name: 'f_sponsor',
            type: 'string'
        }
        ,
        {
            name: 'f_adddate',
            type: 'string'
        }
    ]
});
