﻿Ext.define('column_z_service', {
    columns: [
        {
            xtype: 'rownumberer',
            text: '',
            sortable: true,
            width: 40,
            align: 'left'
        }
        ,
        {
            header: '服务类型',
            dataIndex: 'f_typename',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '联系人',
            dataIndex: 'f_contacts',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '联系电话',
            dataIndex: 'f_phone',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '是否回复',
            dataIndex: 'f_isreply',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                var str = "";
                if(value == null || value == "" || value == "false"){
                    str = "<font color='red'>否</font>";
                }else{
                    str = "<font color='green'>是</font>";
                }
                return str;
            }
        }
        ,
        {
            header: '提交时间',
            dataIndex: 'f_adddate',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                return system.UtilStatic.formatDateTime(value);
            }
        }
    ]
});
