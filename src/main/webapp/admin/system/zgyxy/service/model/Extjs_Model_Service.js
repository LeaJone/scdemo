﻿Ext.define('model_z_service', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'id',
            type: 'string'
        }
        ,
        {
            name: 'f_typeid',
            type: 'string'
        }
        ,
        {
            name: 'f_typename',
            type: 'string'
        }
        ,
        {
            name: 'f_contacts',
            type: 'string'
        }
        ,
        {
            name: 'f_phone',
            type: 'string'
        }
        ,
        {
            name: 'f_describe',
            type: 'string'
        }
        ,
        {
            name: 'f_stateid',
            type: 'string'
        }
        ,
        {
            name: 'f_statename',
            type: 'string'
        }
        ,
        {
            name: 'f_isreply',
            type: 'string'
        }
        ,
        {
            name: 'f_adddate',
            type: 'string'
        }
    ]
});
