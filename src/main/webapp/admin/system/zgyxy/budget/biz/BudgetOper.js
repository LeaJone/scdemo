/**
 * 经费预算管理界面操作类
 *
 */
Ext.define('system.zgyxy.budget.biz.BudgetOper', {

    form : null,
    util : Ext.create('system.util.util'),

    /**
     * 添加
     */
    add : function(grid, fid){
        var win = Ext.create('system.zgyxy.budget.biz.BudgetEdit');
        win.setTitle('预算添加');
        win.modal = true;
        win.grid = grid;
        win.setFid(fid);
        win.show();
    },

    /**
     * 修改
     */
    editor : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的预算信息！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能修改一条预算信息！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.zgyxy.budget.biz.BudgetEdit');
            win.setTitle('预算修改');
            win.modal = true;
            win.grid = grid;
            win.setFid(id)
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(win.down('form').getForm() , 'z_budget/load_z_budgetbyid.do' , param , null ,
                function(){
                    win.show();
                });
        }
    },

    /**
     * 查看
     */
    view : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要查看的预算信息！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能查看一条预算信息！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.zgyxy.budget.biz.BudgetEdit');
            win.setTitle('预算查看');
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(win.down('form').getForm() , 'z_budget/load_z_budgetbyid.do' , param , null ,
                function(){
                    win.show();
                });
        }
    },

    /**
     * 删除
     */
    del : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要删除的预算信息！');
        }else{
            Ext.MessageBox.show({
                title : '提示',
                msg : '您确定要删除所选的预算信息吗？',
                width : 250,
                buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                        var dir = new Array();
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
                        });
                        var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('z_budget/del_z_budget_qx_z_budgetsc.do' , param ,
                            function(){
                                Ext.MessageBox.alert('提示', '预算删除成功！');
                                grid.getStore().reload();
                            }, null, me.form);
                    }
                }
            });
        }
    },

    /**
     * 表单提交
     */
    formSubmit : function(formpanel, grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_budget/save_z_budget_qx_z_budgetbj.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    formpanel.up('window').close();
                    Ext.MessageBox.alert('提示', '保存成功！');
                    grid.getStore().reload();
                });
        }
    }
});