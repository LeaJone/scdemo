﻿/**
 * 经费预算添加界面
 * @author lumingbao
 */
Ext.define(	'system.zgyxy.budget.biz.BudgetEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.zgyxy.budget.biz.BudgetOper'),
	initComponent:function(){
		var me = this;

		me.f_fid = Ext.create('Ext.form.field.Hidden',{
			name:'f_fid'
		});

		var f_name = new Ext.create('system.widget.FsdTextField',{
			fieldLabel: '开支科目',
			labelAlign:'right',
			labelWidth:90,
			width : 360,
			name: 'f_name',
			maxLength: 25,
			allowBlank: false
		});

		var f_money = Ext.create('system.widget.FsdNumber',{
			fieldLabel: '预算金额',
			labelAlign:'right',
			labelWidth:90,
			width : 360,
			name: 'f_money',
			maxLength: 10,
			minValue: 0,
			allowBlank: false,
			allowDecimals : false//是否允许输入小数
		});

		var f_use = new Ext.create('system.widget.FsdTextField',{
			fieldLabel: '主要用途',
			labelAlign:'right',
			labelWidth:90,
			width : 360,
			name: 'f_use',
			maxLength: 25,
			allowBlank: false
		});

		var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '15 10 5 10',
			items : [{
				name: "id",
				xtype: "hidden"
			} , me.f_fid,
				f_name,
				f_money,
				f_use]
		});

		Ext.apply(this,{
			layout : 'fit',
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			items :[form],
			buttons : [{
				name : 'btnsave',
				text : '保存',
				iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
				handler : function() {
					me.oper.formSubmit(form, me.grid);
				}
			}, {
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	},

	/**
	 * 设置父级ID
	 * @param tutorid
	 */
	setFid:function (fid) {
		this.f_fid.setValue(fid);
	}
});