﻿Ext.define('model_z_budget', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'id',
            type: 'string'
        }
        ,
        {
            name: 'f_fid',
            type: 'string'
        }
        ,
        {
            name: 'f_name',
            type: 'string'
        }
        ,
        {
            name: 'f_money',
            type: 'string'
        }
        ,
        {
            name: 'f_gist',
            type: 'string'
        }
        ,
        {
            name: 'f_use',
            type: 'string'
        }
        ,
        {
            name: 'f_remark',
            type: 'string'
        }
        ,
        {
            name: 'f_adddate',
            type: 'string'
        }
    ]
});
