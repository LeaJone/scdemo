﻿Ext.define('column_z_budget', {
    columns: [
        {
            xtype: 'rownumberer',
            text: '',
            sortable: true,
            width: 40,
            align: 'left'
        }
        ,
        {
            header: '开支科目',
            dataIndex: 'f_name',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '预算经费（元）',
            dataIndex: 'f_money',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '主要用途',
            dataIndex: 'f_use',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '添加时间',
            dataIndex: 'f_adddate',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                return system.UtilStatic.formatDateTime(value);
            }
        }
    ]
});
