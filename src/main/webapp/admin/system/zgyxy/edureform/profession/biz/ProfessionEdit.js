﻿/**
 * 课程编辑界面
 */
Ext.define('system.zgyxy.edureform.profession.biz.ProfessionEdit', {
	extend : 'Ext.window.Window',
	requires : [ 'system.zgyxy.edureform.profession.model.Extjs_T_Column',
        		'system.zgyxy.edureform.profession.model.Extjs_T_Model'],
	grid : null,
	oper : Ext.create('system.zgyxy.edureform.profession.biz.ProfessionOper'),
	initComponent:function(){
		var me = this;	 
	    
	    var f_name = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel : '专业名称',
	    	labelAlign : 'right',
	    	labelWidth : 100,
	        width : 340,
	    	name : 'f_name',
	    	maxLength : 200,
	    	allowBlank : false,
	    	blankText : '请输入专业名称',
	    	maxLengthText : '专业名称不能超过200个字符'
	    });
	    
        var f_teamname = Ext.create('system.widget.FsdTextGrid', {
            zFieldLabel : '项目团队',
            zLabelWidth : 100,
            width : 340,
            zName : 'f_teamname',
            zColumn : 'column_project_team', // 显示列
            zModel : 'model_z_team',
            zBaseUrl : 'z_team/load_pagedata.do',
            zAllowBlank : true,
            zGridType : 'page',
            zGridWidth : 700,//弹出表格宽度
            zGridHeight : 600,//弹出表格高度
            zIsText1 : true,
            zTxtLabel1 : '团队名称',
            zFunChoose : function(data){
                f_teamid.setValue(data.id);
                f_teamname.setValue(data.f_name);
                f_leadername.zTeamId = data.id;
            },
            zFunQuery : function(txt1){
                return {name : txt1};
            }
        });
        
        var f_teamid = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '项目团队编码',
            name: 'f_teamid'
        });
        
        var f_leadername = Ext.create('system.widget.FsdTextGridById', {
            zFieldLabel : '项目负责人',
            zLabelWidth : 100,
            width : 340,
            zName : 'f_principalname',
            zColumn : 'column_project_member', // 显示列
            zModel : 'model_z_teamanduser',
            zBaseUrl : 'z_teamanduser/load_teamuserbyid.do',
            zAllowBlank : true,
            zGridType : 'list',
            zGridWidth : 700,//弹出表格宽度
            zGridHeight : 500,//弹出表格高度
            zIsText1 : true,
            zTxtLabel1 : '项目负责人姓名',
            zFunChoose : function(data){
                f_leadername.setValue(data.username);
            },
            zFunQuery : function(txt1){
                return {id : f_leadername.zTeamId, txt : txt1};
            }
        });

	    var f_basename = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel : '依托基地',
	    	labelAlign : 'right',
	    	labelWidth : 100,
	        width : 340,
	    	name : 'f_basename',
	    	maxLength : 200,
	    	allowBlank : true,
	    	blankText : '请输入依托基地',
	    	maxLengthText : '依托基地不能超过200个字符'
	    });
	    me.on('boxready', function(){
            f_leadername.zTeamId = f_teamid.getValue();
        });
        
	    var f_money = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel : '经费预算',
	    	labelAlign : 'right',
	    	labelWidth : 100,
	        width : 340,
	    	name : 'f_money',
	    	maxLength : 200,
	    	allowBlank : true,
	    	blankText : '请输入经费预算',
	    	maxLengthText : '经费预算不能超过200个字符'
	    });
	    
	    Ext.apply(this,{
	        width:400,
	        height:280,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[{
				xtype : 'form',
				id : 'courseAddForm',
				defaultType : 'textfield',
				header : false,// 是否显示标题栏
				border : 0,
				padding : '20 0 0 20',
				items : [{
		                    name: "id",
		                    xtype: "hidden"
		                } , 
		                f_name,f_teamname,f_teamid,f_leadername,f_basename,f_money]
	        }],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.formSubmit(Ext.getCmp('courseAddForm') , me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});