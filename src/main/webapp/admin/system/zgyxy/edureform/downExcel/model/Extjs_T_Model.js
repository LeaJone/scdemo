﻿/**
 * t_course Model
 * @author CodeSystem
 * 文件名     Extjs_T_Course
 * Model名    model_t_course
 * Columns名  column_t_course
 */

Ext.define('model_t_course', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'f_name',
            type : 'string'
        }
        ,
        {
            name : 'f_principalname',
            type : 'string'
        }
        ,
        {
            name : 'f_typename',
            type : 'string'
        }
        ,
        {
            name : 'f_teamname',
            type : 'string'
        }
        ,
        {
            name : 'f_pdfurl',
            type : 'string'
        }
    ]
});

/**
 *团队
 */

Ext.define('model_z_team', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'f_name',
            type : 'string'
        }
        ,
        {
            name : 'f_abstract',
            type : 'string'
        }
        ,
        {
            name : 'f_leaderno',
            type : 'string'
        }
        ,
        {
            name : 'f_leaderid',
            type : 'string'
        }
        ,
        {
            name : 'f_leadername',
            type : 'string'
        }
        ,
        {
            name : 'f_creatorid',
            type : 'string'
        }
        ,
        {
            name : 'f_creatorname',
            type : 'string'
        }
        ,
        {
            name : 'f_adddate',
            type : 'string'
        }
        ,
        {
            name : 'f_photo1',
            type : 'string'
        }
        ,
        {
            name : 'f_photo2',
            type : 'string'
        }  
        ,
        {
            name : 'f_leaderphone',
            type : 'string'
        } 
    ]
});

/**
 *团队成员
 */

Ext.define('model_z_teamanduser', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'f_teamid',
            type : 'string'
        }
        ,
        {
            name : 'f_userid',
            type : 'string'
        }
        ,
        {
            name : 'username',
            type : 'string'
        }
        ,
        {
            name : 'usersex',
            type : 'string'
        }
        ,
        {
            name : 'useracademy',
            type : 'string'
        }
        ,
        {
            name : 'userbranch',
            type : 'string'
        }
        ,
        {
            name : 'usernumber',
            type : 'string'
        }
    ]
});
