﻿
/**
 *promote Columns
 *@author CodeSystem
 */
Ext.define('column_t_course', {
    columns: [
    	{
    		xtype : 'rownumberer',
    		text : 'NO',
    		sortable : true,
    		align : 'center',
    		flex : 0.3
    	}
    	,
        {
            header : '申请单位',
            dataIndex : 'f_company',
            align : 'center',
    		flex : 1.2
        }
        ,
        {
            header : '负责人姓名',
            dataIndex : 'f_principalname',
            align : 'center',
    		flex : 1
        }
        ,
        {
            header : '主要成员',
            dataIndex : 'f_member',
            align : 'center',
    		flex : 1
        }
        ,
        {
            header : '经费预算',
            dataIndex : 'f_money',
            align : 'center',
    		flex : 1
        }
        ,
        {
            header: '操作',
            dataIndex: 'f_pdfurl',
            sortable: true,
            align: 'center',
            flex: 0.5,
            renderer :function(value ,metaData){
                if(value != null && value != ""){
                    value = "<a href='#' style='color:blue;'>下载申报书</a>";
                }else{
                    value = "<a href='#' style='color:red;'>生成申报书</a>";
                }
                return value;
            }
        }
    ]
});

