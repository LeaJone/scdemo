/**
 * 就业创业能力提升机构管理界面操作类
 */

Ext.define('system.zgyxy.edureform.promote.biz.PromoteOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 添加
     */
	add : function(grid){
		var win = Ext.create('system.zgyxy.edureform.promote.biz.PromoteEdit');
        win.setTitle('单位信息');
        win.grid = grid;
        win.modal = true;
        win.show();
	},
	
	/**
     * 表单提交
     */
     formSubmit : function(formpanel , grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_edupromote/save_z_edupromote_qx_z_edupromotebj.do' , formpanel.form , '正在提交数据,请稍候.....',
			function(form, action){
			    formpanel.up('window').close();
			    grid.getStore().reload();
				Ext.MessageBox.alert('提示', '保存成功！');
			},
			function (form, action){
				Ext.MessageBox.alert('异常', action.result.msg);
			});
        }
     },
     
     /**
      * 上传申请文件
      */
     uploadSubmit : function(formpanel , grid){
         var me = this;
         if (formpanel.form.isValid()) {
             me.util.ExtFormSubmit('z_edupromote/edupromote_fileupload.do' , formpanel.form , '正在提交数据,请稍候.....',
 			function(form, action){
 			    formpanel.up('window').close();
 			    grid.getStore().reload();
 				Ext.MessageBox.alert('提示', '保存成功！');
 			},
 			function (form, action){
 				Ext.MessageBox.alert('异常', action.result.msg);
 			});
         }
      },
     
     /**
      * 删除
      */
     del : function(grid){
    	 var me = this;
 		 // 获取选中的行
 	     var data = grid.getSelectionModel().getSelection();
         if (data.length == 0) {
             Ext.MessageBox.alert('提示', '请先选中要删除的项目！');
         }else {
        	 Ext.MessageBox.show({
        		 title : '询问',
        		 msg : '您确定要删除吗?',
 			     width : 250,
 				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                     if (btn == 'yes') {
                    	 var dir = new Array();　
                         Ext.Array.each(data, function(items) {
                             var id = items.data.id;
                             dir.push(id);
             			});
             			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('z_edupromote/del_z_edupromote_qx_z_edupromotesc.do' , param ,
                        function(response, options){
            				Ext.MessageBox.alert('提示', '删除成功！');
                        	grid.getStore().reload();
                        }, null, me.form);
                     }
                 }
             });
         }
 	},
 	
 	/**
     * 修改
     */
	editor : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.zgyxy.edureform.promote.biz.PromoteEdit');
            win.setTitle('单位信息修改');
            win.modal = true;
            win.grid = grid;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'z_edupromote/load_z_edupromotebyid.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},
 	
 	/**
     * 查看
     */
	view : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
	    if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要查看的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能查看一个项目！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.zgyxy.edureform.promote.biz.PromoteEdit');
    		win.setTitle('单位信息查看');
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'z_edupromote/load_z_edupromotebyid.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},
	
	/**
     * PDF文件下载
     */
    download : function(f_pdf){
        var me = this;
        if(f_pdf != "" && f_pdf != null){
            Ext.MessageBox.show({
                title : '询问',
                msg : '你确定要下载word文档吗？',
                width : 250,
                buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                        window.open('Common/download.do?path=' + f_pdf);
                    }
                }
            });
        }
    },
    
    /**
     * word文件上传
     */
    upload : function(grid){
    	var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
	    if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要上传的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能上传一个项目！');
        }else{
        	var id = data[0].data.id;
            var win = Ext.create('system.zgyxy.edureform.promote.biz.PromoteFileEdit');
            win.setTitle('指定文件');
            win.grid = grid;
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(true);
            win.setFid(id);
            win.show();
        }
    }
});