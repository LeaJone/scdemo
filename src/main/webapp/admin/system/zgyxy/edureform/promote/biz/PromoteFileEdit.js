﻿/**
 * 课程编辑界面
 */
Ext.define('system.zgyxy.edureform.promote.biz.PromoteFileEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.zgyxy.edureform.promote.biz.PromoteOper'),
	initComponent:function(){
		var me = this;	 
		
		me.f_fid = Ext.create('Ext.form.field.Hidden',{
			name:'id'
		});
		
		 var f_pdfurl = Ext.create('system.widget.FsdTextFileManage', {
		        width : 330,
		    	zName : 'f_pdfurl',
		    	zFieldLabel : '文件路径',
		    	zLabelWidth : 80,
		    	zIsUpButton : true,
		    	zAllowBlank : false,
		    	zBlankText : '请选择输入文件路径',
		    	zFileType : 'file',
		    	zFileAutoName : false,
		    	zFileUpPath : me.oper.util.getFilePath(),
		    	zManageType : 'manage'
		    });
		
		 me.form = Ext.create('Ext.form.Panel',{
				xtype : 'form',
				defaultType : 'textfield',
				header : false,// 是否显示标题栏
				border : 0,
				padding : '15 10 5 10',
				items : [me.f_fid, 
	                f_pdfurl]
			});
		 
	    Ext.apply(this,{
	        width:400,
	        height:150,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[me.form],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.uploadSubmit(me.form , me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	},
	/**
	 * 获取主键
	 */
	setFid:function(fid){
		this.f_fid.setValue(fid);
	}
});