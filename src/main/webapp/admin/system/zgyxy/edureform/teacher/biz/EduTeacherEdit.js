﻿/**
 * 课程编辑界面
 */
Ext.define('system.zgyxy.edureform.teacher.biz.EduTeacherEdit', {
	extend : 'Ext.window.Window',
	requires : [ 'system.zgyxy.edureform.teacher.model.Extjs_T_Column',
        		'system.zgyxy.edureform.teacher.model.Extjs_T_Model'],
	grid : null,
	oper : Ext.create('system.zgyxy.edureform.teacher.biz.EduTeacherOper'),
	initComponent:function(){
		var me = this;	 
	    
	    var f_course = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel : '主讲课程',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 340,
	    	name : 'f_course',
	    	maxLength : 200,
	    	allowBlank : false,
	    	blankText : '请输入主讲课程',
	    	maxLengthText : '主讲课程名称不能超过200个字符'
	    });
	    
	    var f_basename = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel : '基地名称',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 340,
	    	name : 'f_basename',
	    	maxLength : 200,
	    	allowBlank : true,
	    	blankText : '请输入基地名称',
	    	maxLengthText : '基地名称不能超过200个字符'
	    });
	    
	    var f_imgurl = Ext.create('system.widget.FsdTextFileManage', {
			width : 340,
			zName : 'f_imgurl',
			zFieldLabel : '工作照片',
			zLabelWidth : 80,
			zIsShowButton : true,
			zIsUpButton : true,
			zIsReadOnly : false,
			zContentObj : me.content,
			zFileUpPath : me.oper.util.getImagePath()
		});
	    
	    var f_tips = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel : '名师心得',
	    	labelAlign :'right',
	    	labelWidth : 80,
	        width : 340,
	        allowBlank : true,
	    	name : 'f_tips',
	    	maxLength : 500,
	    	blankText : '请输入名师心得',
	    	maxLengthText : '名师心得不能超过500个字符'
	    });
	    
	    var f_abstract = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel : '个人简介',
	    	labelAlign :'right',
	    	labelWidth : 80,
	        width : 340,
	        allowBlank : true,
	    	name : 'f_abstract',
	    	maxLength : 500,
	    	blankText : '请输入个人简介',
	    	maxLengthText : '个人简介不能超过500个字符'
	    });
	    
	    Ext.apply(this,{
	        width:400,
	        height:350,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[{
				xtype : 'form',
				id : 'courseAddForm',
				defaultType : 'textfield',
				header : false,// 是否显示标题栏
				border : 0,
				padding : '20 0 0 20',
				items : [{
		                    name: "id",
		                    xtype: "hidden"
		                } , 
		                f_course,f_basename,f_imgurl,f_tips,f_abstract]
	        }],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.formSubmit(Ext.getCmp('courseAddForm') , me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});