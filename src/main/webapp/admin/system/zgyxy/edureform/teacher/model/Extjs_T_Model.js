﻿/**
 * t_course Model
 * @author CodeSystem
 * 文件名     Extjs_T_Course
 * Model名    model_t_course
 * Columns名  column_t_course
 */

Ext.define('model_t_course', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'f_candidatename',
            type : 'string'
        }
        ,
        {
            name : 'f_course',
            type : 'string'
        }
        ,
        {
            name : 'f_basename',
            type : 'string'
        }
        ,
        {
            name : 'f_pdfurl',
            type : 'string'
        }
    ]
});

