﻿/**
 * 课程编辑界面
 */
Ext.define('system.zgyxy.edureform.project.biz.ProjectEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.zgyxy.edureform.project.biz.ProjectOper'),
	initComponent:function(){
		var me = this;	 
	    
	    var f_name = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel : '项目名称',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 340,
	    	name : 'f_name',
	    	maxLength : 200,
	    	allowBlank : false,
	    	blankText : '请输入项目名称',
	    	maxLengthText : '项目名称名称不能超过200个字符'
	    });
	    
	    var f_principalname = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel : '负责人',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 340,
	    	name : 'f_principalname',
	    	maxLength : 200,
	    	allowBlank : false,
	    	blankText : '负责人姓名名称',
	    	maxLengthText : '负责人姓名不能超过200个字符'
	    });
	    
	    var f_money = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel : '经费预算',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 340,
	    	name : 'f_money',
	    	maxLength : 200,
	    	allowBlank : true,
	    	blankText : '请输入经费预算',
	    	maxLengthText : '经费预算不能超过200个字符'
	    });
	    
	    var f_basename = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel : '基地名称',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 340,
	    	name : 'f_basename',
	    	maxLength : 200,
	    	allowBlank : true,
	    	blankText : '请输入基地名称',
	    	maxLengthText : '基地名称不能超过200个字符'
	    });
	    
	    var f_member = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel : '项目成员',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 340,
	    	name : 'f_member',
	    	maxLength : 200,
	    	allowBlank : true,
	    	blankText : '请输入项目成员',
	    	maxLengthText : '项目成员不能超过200个字符'
	    });
	    
	    Ext.apply(this,{
	        width:400,
	        height:260,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[{
				xtype : 'form',
				id : 'courseAddForm',
				defaultType : 'textfield',
				header : false,// 是否显示标题栏
				border : 0,
				padding : '20 0 0 20',
				items : [{
		                    name: "id",
		                    xtype: "hidden"
		                } , 
		                f_name,f_principalname,f_money,f_basename,f_member]
	        }],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.formSubmit(Ext.getCmp('courseAddForm') , me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});