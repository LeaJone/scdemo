﻿/**
 * 项目信息管理界面
 */
Ext.define('system.zgyxy.edureform.project.biz.ProjectManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.zgyxy.edureform.project.model.Extjs_T_Column',
		         'system.zgyxy.edureform.project.model.Extjs_T_Model'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.zgyxy.edureform.project.biz.ProjectOper'),
    
	createTable : function() {
	    var me = this;
	    
	    /**
		 * 查询文本框
		 */
		var queryText =  Ext.create("Ext.form.TextField", {
			name : 'queryParam',
			emptyText : '请输入项目名称',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().reload();
					}
				}
			}
		});
		
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_t_course', // 显示列
			model : 'model_t_course',
			baseUrl : 'z_eduproject/load_pagedata.do',
			zAutoLoad: true,
			tbar : [
				{
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
				text : "教改项目申请",
				iconCls : 'page_addIcon',
				handler : function() {
					me.oper.add(grid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
				text : "修改",
				iconCls : 'page_editIcon',
				handler : function() {
					me.oper.editor(grid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'dwsc',
				text : "删除",
				iconCls : 'page_deleteIcon',
				handler : function() {
				    me.oper.del(grid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid);
				}
			},{
				text : '上传申报书',
				iconCls : 'up',
				handler : function() {
					me.oper.upload(grid);
				}
			
			},'->', queryText, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  queryText.getValue();
			var pram = {name : name};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});

		});
		
		grid.on('cellclick', function(thisObj, td, cellIndex, record) {
			if (cellIndex == 6){
				me.oper.download(record.get('f_pdfurl'));
			}
		});
		
		var contentpanel = Ext.create("Ext.panel.Panel", {
			frame:true,
			split: true,
			region: 'center',
			layout: 'fit',
			title: '项目信息管理',
			items: [grid]
		});
		
		return contentpanel;
	}
});