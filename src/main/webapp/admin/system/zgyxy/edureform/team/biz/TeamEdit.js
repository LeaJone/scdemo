﻿/**
 * 团队编辑界面
 */
Ext.define('system.zgyxy.edureform.team.biz.TeamEdit', {
	extend : 'Ext.window.Window',
	requires : [ 'system.zgyxy.edureform.team.model.Extjs_T_Column',
		'system.zgyxy.edureform.team.model.Extjs_T_Model'],
	grid : null,
	oper : Ext.create('system.zgyxy.edureform.team.biz.TeamOper'),
	initComponent:function(){
		var me = this;	 
	    
	    var f_teamname = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel : '团队名称',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 340,
	    	name : 'f_teamname',
	    	maxLength : 200,
	    	allowBlank : false,
	    	blankText : '请输入团队名称',
	    	maxLengthText : '团队名称不能超过200个字符'
	    });
	    
	    var f_principalname = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel : '负责人',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 340,
	    	name : 'f_principalname',
	    	maxLength : 200,
	    	allowBlank : false,
	    	blankText : '请输入负责人姓名',
	    	maxLengthText : '负责人姓名不能超过200个字符'
	    });
	    
	    var f_basename = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel : '依托基地',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 340,
	    	name : 'f_basename',
	    	maxLength : 200,
	    	allowBlank : true,
	    	blankText : '请输入依托基地',
	    	maxLengthText : '依托基地不能超过200个字符'
	    });
	    
	    var f_member = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel : '团队成员',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 340,
	    	name : 'f_member',
	    	maxLength : 200,
	    	allowBlank : true,
	    	blankText : '请输入团队成员',
	    	maxLengthText : '团队成员不能超过200个字符'
	    });
	    
	    Ext.apply(this,{
	        width:400,
	        height:240,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[{
				xtype : 'form',
				id : 'courseAddForm',
				defaultType : 'textfield',
				header : false,// 是否显示标题栏
				border : 0,
				padding : '20 0 0 20',
				items : [{
		                    name: "id",
		                    xtype: "hidden"
		                } , 
		                f_teamname,f_principalname,f_basename,f_member]
	        }],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.formSubmit(Ext.getCmp('courseAddForm') , me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});