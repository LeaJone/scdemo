﻿
/**
 *Team Columns
 *@author CodeSystem
 */
Ext.define('column_t_course', {
    columns: [
    	{
    		xtype : 'rownumberer',
    		text : 'NO',
    		sortable : true,
    		align : 'center',
    		flex : 0.3
    	}
    	,
        {
            header : '团队名称',
            dataIndex : 'f_teamname',
            align : 'center',
    		flex : 1.2
        }
        ,
        {
            header : '负责人姓名',
            dataIndex : 'f_principalname',
            align : 'center',
    		flex : 1
        }
        ,
        {
            header : '团队成员',
            dataIndex : 'f_member',
            align : 'center',
    		flex : 1
        }
        ,
        {
            header: '操作',
            dataIndex: 'f_pdfurl',
            sortable: true,
            align: 'center',
            flex: 0.5,
            renderer :function(value ,metaData){
                if(value != null && value != ""){
                    value = "<a href='#' style='color:blue;'>下载申报书</a>";
                }else{
                    value = "<a href='#' style='color:red;'>生成申报书</a>";
                }
                return value;
            }
        }
    ]
});

/**
 * 项目管理选择团队面板
 */
Ext.define('column_project_member', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            align : 'center',
            flex : 0.5
        }
        ,
        {
            header : '成员名称',
            dataIndex : 'username',
            align : 'center',
            flex : 1
        }
        ,
        {
            header : '性别',
            dataIndex : 'usersex',
            align : 'center',
            flex : 0.5
        }
        ,
        {
            header : '院系',
            dataIndex : 'useracademy',
            align : 'center',
            flex : 1.5
        }
        ,
        {
            header : '专业',
            dataIndex : 'userbranch',
            align : 'center',
            flex : 1.5
        }
    ]
});

/**
 * 项目管理选择团队面板
 */
Ext.define('column_project_team', {
    columns: [
        {
            header : '团队名称',
            dataIndex : 'f_name',
            align : 'center',
            flex : 2
        }
        ,
        {
            header : '团队负责人',
            dataIndex : 'f_leadername',
            align : 'center',
            flex : 1
        }
        ,
        {
            header : '团队成立时间',
            dataIndex : 'f_adddate',
            align : 'center',
            flex : 1,
            renderer :function(value){
                return system.UtilStatic.formatDateTime(value, false);
            }
        }

    ]
});