﻿/**
 * 导师添加界面
 * @author lumingbao
 */
Ext.define('system.zgyxy.tutor.biz.TutorEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	fid:'',
	oper : Ext.create('system.zgyxy.tutor.biz.TutorOper'),
	honorOper : Ext.create('system.zgyxy.honor.biz.HonorOper'),
	trainOper : Ext.create('system.zgyxy.tutor.biz.TrainOper'),
	requires : [ 'system.zgyxy.honor.model.Extjs_Model_Honor',
		'system.zgyxy.honor.model.Extjs_Column_Honor',
		'system.zgyxy.tutor.model.Extjs_Column_TutorTrain',
		'system.zgyxy.tutor.model.Extjs_Model_TutorTrain'],
	initComponent:function(){
		var me = this;

		me.f_id = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
			value: ''
		});

		var f_name = new Ext.create('system.widget.FsdTextField',{
			fieldLabel: '姓名',
			labelAlign:'right',
			labelWidth:90,
			width : 800,
			name: 'f_name',
			maxLength: 25,
			allowBlank: false
		});

		var f_sexid = Ext.create('system.widget.FsdComboBoxZD',{
			fieldLabel:'性别',
			labelAlign:'right',
			labelWidth:90,
			width : 800,
			name : 'f_sexid',//提交到后台的参数名
			key : 'xb',//参数
			allowBlank: false,
			hiddenName : 'f_sexname'//提交隐藏域Name
		});
			var f_sexname = Ext.create('Ext.form.field.Hidden',{
			fieldLabel: '性别名称',
			name: 'f_sexname'
		});

		var f_degree = new Ext.create('system.widget.FsdTextField',{
			fieldLabel: '学位',
			labelAlign:'right',
			labelWidth:90,
			width : 800,
			name: 'f_degree',
			maxLength: 25
		});

		var f_professionalTitle = new Ext.create('system.widget.FsdTextField',{
			fieldLabel: '职称',
			labelAlign:'right',
			labelWidth:90,
			width : 800,
			name: 'f_professionalTitle',
			maxLength: 25
		});

		var f_remark = new Ext.create('system.widget.FsdTextField',{
			fieldLabel: '备注',
			labelAlign:'right',
			labelWidth:90,
			width : 800,
			name: 'f_remark',
			maxLength: 25
		});

		var f_typecode = Ext.create('system.widget.FsdComboBoxZD',{
			fieldLabel:'导师类型',
			labelAlign:'right',
			labelWidth:90,
			width : 800,
			name : 'f_typecode',//提交到后台的参数名
			key : 'dslx',//参数
			allowBlank: false,
			hiddenName : 'f_typename'//提交隐藏域Name
		});
		var f_typename = Ext.create('Ext.form.field.Hidden',{
			fieldLabel: '导师类型名称',
			name: 'f_typename'
		});

		var f_workunit = new Ext.create('system.widget.FsdTextField',{
			fieldLabel: '工作单位',
			labelAlign:'right',
			labelWidth:90,
			width : 800,
			name: 'f_workunit',
			maxLength: 25
		});

		var f_courses = new Ext.create('system.widget.FsdTextField',{
			fieldLabel: '主讲课程',
			labelAlign:'right',
			labelWidth:90,
			width : 800,
			name: 'f_courses',
			maxLength: 25
		});

		var f_direction = new Ext.create('system.widget.FsdTextField',{
			fieldLabel: '研究方向',
			labelAlign:'right',
			labelWidth:90,
			width : 800,
			name: 'f_direction',
			maxLength: 25
		});

		var f_imgurl = Ext.create('system.widget.FsdTextFileManage', {
			width : 800,
			zName : 'f_imgurl',
			zFieldLabel : '工作照片',
			zLabelWidth : 90,
			zIsShowButton : true,
			zIsUpButton : true,
			zIsReadOnly : false,
			zContentObj : me.content,
			zFileUpPath : me.oper.util.getImagePath()
		});

		var f_isfriend = Ext.create('system.widget.FsdComboBoxZD',{
			fieldLabel:'是否校友',
			labelAlign:'right',
			labelWidth:90,
			width : 800,
			name : 'f_isfriend',//提交到后台的参数名
			key : 'sf',//参数
			allowBlank: false
		});

		var f_sort = new Ext.create('system.widget.FsdNumber',{
			fieldLabel: '排序编号',
			labelAlign:'right',
			labelWidth:90,
			width : 800,
			name: 'f_sort',
			maxLength: 4,
			minValue: 0,
			allowDecimals : false,//是否允许输入小数
			allowBlank: false
		});

		var f_resume = Ext.create('system.widget.FsdUeditor',{
			fieldLabel: '个人简历',
			labelAlign:'right',
			labelWidth:90,
			width : 800,
			height : 500,
			name: 'f_resume'
		});

		var f_practice = Ext.create('system.widget.FsdUeditor',{
			fieldLabel: '双创实践',
			labelAlign:'right',
			labelWidth:90,
			width : 800,
			height : 500,
			name: 'f_practice'
		});

		var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			autoScroll: true,
			border : 0,
			padding : '15 10 5 10',
			items : [me.f_id ,
				f_name,
				f_sexid,
				f_sexname,
				f_degree,
				f_professionalTitle,
				f_remark,
				f_typecode,
				f_typename,
				f_workunit,
				f_courses,
				f_direction,
				f_imgurl,
				f_isfriend,
				f_sort,
				f_resume,
				f_practice]
		});


		// 创建荣誉表格
		me.honorGrid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_honor', // 显示列
			model: 'model_z_honor',
			baseUrl : 'z_honor/load_pagedata.do',
			border: 0,
			zAutoLoad: false,
			tbar : [{
				xtype : "fsdbutton",
				popedomCode : 'rybj',
				text : "添加",
				iconCls : 'page_addIcon',
				handler : function() {
					me.honorOper.add(me.honorGrid, me.fid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'rybj',
				text : "修改",
				iconCls : 'page_editIcon',
				handler : function() {
					me.honorOper.editor(me.honorGrid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.honorOper.view(me.honorGrid);
				}
			},{
				xtype : "fsdbutton",
				popedomCode : 'rysc',
				text : "删除",
				iconCls : 'page_deleteIcon',
				handler : function() {
					me.honorOper.del(me.honorGrid);
				}
			},   '->', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					me.honorGrid.getStore().reload();
				}
			}]
		});
		me.honorGrid.getStore().on('beforeload', function(s) {
			var pram = {fid : me.fid};
			var params = s.getProxy().extraParams;
			Ext.apply(params,{jsonData : Ext.encode(pram)});
		});

		me.honor = Ext.create('Ext.panel.Panel',{
			title: ' 导师荣誉',
			layout : 'fit',
			disabled : true,
			items : [me.honorGrid]
		});

		// 创建培训表格
		me.trainGrid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_tutortrain', // 显示列
			model: 'model_z_tutortrain',
			baseUrl : 'z_tutortrain/load_pagedata.do',
			border: 0,
			zAutoLoad: false,
			tbar : [{
				xtype : "fsdbutton",
				popedomCode : 'rybj',
				text : "添加",
				iconCls : 'page_addIcon',
				handler : function() {
					me.trainOper.add(me.trainGrid, me.fid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'rybj',
				text : "修改",
				iconCls : 'page_editIcon',
				handler : function() {
					me.trainOper.editor(me.trainGrid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.trainOper.view(me.trainGrid);
				}
			},{
				xtype : "fsdbutton",
				popedomCode : 'rysc',
				text : "删除",
				iconCls : 'page_deleteIcon',
				handler : function() {
					me.trainOper.del(me.trainGrid);
				}
			},   '->', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					me.trainGrid.getStore().reload();
				}
			}]
		});
		me.trainGrid.getStore().on('beforeload', function(s) {
			var pram = {tutorid : me.fid};
			var params = s.getProxy().extraParams;
			Ext.apply(params,{jsonData : Ext.encode(pram)});
		});

		me.train = Ext.create('Ext.panel.Panel',{
			title: ' 导师培训',
			layout : 'fit',
			disabled : true,
			items : [me.trainGrid]
		});

		var tabpanel = Ext.create('Ext.tab.Panel', {
			border: 0,
			enableTabScroll: true,
			deferredRender: false,
			defaults: { autoScroll: true },
			items: [{
				border: 0,
				title: '基本信息',
				items: [form]
			}, me.honor, me.train],
			listeners:{
				'tabchange':function (t, n) {

				}
			}
		});

		Ext.apply(this,{
			layout : 'fit',
			width:860,
			height:700,
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			items :[form],
			buttons : [{
				name : 'btnsave',
				text : '保存',
				iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
				handler : function() {
					me.oper.formSubmit(form, me.grid, me.honor, me.train);
				}
			}, {
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	}
});