/**
 * 导师管理界面操作类
 *
 */
Ext.define('system.zgyxy.tutor.biz.TutorOper', {

    form : null,
    util : Ext.create('system.util.util'),

    /**
     * 添加
     */
    add : function(grid){
        var win = Ext.create('system.zgyxy.tutor.biz.TutorEdit');
        win.setTitle('导师添加');
        win.modal = true;
        win.grid = grid;
        win.fid = "";
        win.show();
    },

    /**
     * 修改
     */
    editor : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的导师！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能修改一个导师！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            var name = data[0].data.f_name;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.zgyxy.tutor.biz.TutorEdit');
            win.setTitle(name + '-导师修改');
            win.modal = true;
            win.grid = grid;
            win.fid = id;
            win.honor.setDisabled(false);
            win.train.setDisabled(false);
            win.honorGrid.getStore().reload();
            win.trainGrid.getStore().reload();
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(win.down('form').getForm() , 'z_tutor/load_z_tutorbyid.do' , param , null ,
                function(){
                    win.show();
                });
        }
    },

    /**
     * 查看
     */
    view : function(grid, zt){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要查看的导师！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能查看一个导师！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.zgyxy.tutor.biz.TutorEdit');
            win.setTitle('导师查看');
            win.modal = true;
            win.fid = id;
            win.honor.setDisabled(false);
            win.train.setDisabled(false);
            win.honorGrid.getStore().reload();
            win.trainGrid.getStore().reload();
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(win.down('form').getForm() , 'z_tutor/load_z_tutorbyid.do' , param , null ,
                function(){
                    win.show();
                });
        }
    },

    /**
     * 删除
     */
    del : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要删除的导师！');
        }else{
            Ext.MessageBox.show({
                title : '提示',
                msg : '您确定要删除所选导师吗？',
                width : 250,
                buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                        var dir = new Array();
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
                        });
                        var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('z_tutor/del_z_tutor_qx_z_tutorsc.do' , param ,
                            function(){
                                Ext.MessageBox.alert('提示', '导师删除成功！');
                                grid.getStore().reload();
                            }, null, me.form);
                    }
                }
            });
        }
    },

    /**
     * 表单提交
     */
    formSubmit : function(formpanel, grid, honor, train){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_tutor/save_z_tutor_qx_z_tutorbj.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    formpanel.up('window').fid = respText.data.id;
                    formpanel.up('window').f_id.setValue(respText.data.id);
                    honor.setDisabled(false);
                    train.setDisabled(false);
                    Ext.MessageBox.alert('提示', '保存成功！');
                    grid.getStore().reload();
                });
        }
    },
    /**
	 * 拒绝理由提交
	 */
	reasonSubmit: function(formpanel , grid){
		var me = this;
		if (formpanel.form.isValid()) {
			me.util.ExtFormSubmit('z_tutor/save_reason.do' , formpanel.form , '正在提交数据,请稍候.....',
					function(form, action){
				formpanel.up('window').close();
				grid.getStore().reload();
				Ext.MessageBox.alert('提示', '保存成功！');
			},
			function (form, action){
				Ext.MessageBox.alert('异常', action.result.msg);
			});
		}
	},
    /**
	 * 是否通过审核
	 */
	isCheck : function(grid,treepanel,isQT){
		var me = this;
		// 获取选中的行
		var data = grid.getSelectionModel().getSelection();
		if (data.length == 0) {
			Ext.MessageBox.alert('提示', '请先选中要审核的项目！');
		}else{
			// 先得到主键
			var mainid = data[0].data.id;
			//用的到的主键到后台查询，查询成功以后将相应的值放入文本框
			Ext.MessageBox.show({
				title : '提示',
				msg : '您确定要修改审核状态吗?',
				width : 250,
				buttons : Ext.MessageBox.YESNO,
				icon : Ext.MessageBox.QUESTION,
				fn : function(btn) {
					if (btn == 'yes') {
						var dir = new Array();　
						Ext.Array.each(data, function(items) {
							var id = items.data.id;
							dir.push(id);
						});
						var pram = {ids : dir, state : isQT};
						var param = {jsonData : Ext.encode(pram)};
						me.util.ExtAjaxRequest('z_tutor/checked_tutor_bj.do' , param ,
								function(response, options){
							Ext.MessageBox.alert('提示', '状态修改成功！');
							grid.getStore().reload(); 
						});
					}
				}
			});
		}
	}
});