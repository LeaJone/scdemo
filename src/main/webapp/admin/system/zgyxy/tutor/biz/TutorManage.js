﻿/**
 * 导师管理界面
 *
 */
Ext.define('system.zgyxy.tutor.biz.TutorManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*',
		'system.zgyxy.tutor.model.Extjs_Column_Tutor',
		'system.zgyxy.tutor.model.Extjs_Model_Tutor'],
	header : false,
	border : 0,
	layout : 'fit',
	type:'',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},

	oper : Ext.create('system.zgyxy.tutor.biz.TutorOper'),

	createContent : function(){
		var me = this;

		/**
		 * 左边的树面板
		 */
		var store = Ext.create('Ext.data.TreeStore', {
			root: {
				expanded: true,
				children: [ {
					id:'xnds',
					text:'校内导师',
					expanded: true
				},{
					id:'xwds',
					text:'校外导师',
					expanded: true
				},{
					id:'xy',
					text:'校友',
					expanded: true}

				]
			}
		});

		var treepanel = Ext.create('Ext.tree.Panel', {
			collapsible : true,//折叠
			title: '导师类型',
			width: 150,
			rootVisible: false, //是否隐藏根节点
			region: 'west',
			store: store
		});

		treepanel.on('itemclick', function(view,record,item,index,e){
			me.type =  record.raw.id;
			grid.getStore().reload();
		});

		/**
		 * 查询文本框
		 */
		var queryText =  Ext.create("Ext.form.TextField", {
			name : 'queryTeamParam',
			emptyText : '请输入导师名称',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});

		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_tutor', // 显示列
			model: 'model_z_tutor',
			baseUrl : 'z_tutor/load_pagedata.do',
			tbar : [{
				xtype : "fsdbutton",
				popedomCode : 'rybj',
				text : "添加",
				iconCls : 'page_addIcon',
				handler : function() {
					me.oper.add(grid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'rybj',
				text : "修改",
				iconCls : 'page_editIcon',
				handler : function() {
					me.oper.editor(grid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'rysc',
				text : "删除",
				iconCls : 'page_deleteIcon',
				handler : function() {
					me.oper.del(grid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid, 'ck');
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'jgbj',
				text : "启用",
				iconCls : 'tickIcon',
				handler : function(button) {
				    me.oper.isCheck(grid, treepanel, true);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'jgbj',
				text : "停用",
				iconCls : 'crossIcon',
				handler : function(button) {
				    me.oper.isCheck(grid, treepanel,false);
				}
			},  '->', queryText, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  queryText.getValue();
			var pram = {name : name, type: me.type};
			var params = s.getProxy().extraParams;
			Ext.apply(params, {jsonData : Ext.encode(pram)});
		});

		var contentpanel = Ext.create("Ext.panel.Panel", {
			frame:true,
			split: true,
			region: 'center',
			layout: 'fit',
			title: '导师信息',
			items: [grid]
		});

		var page1_jExtPanel1_obj = new Ext.panel.Panel({
			bodyPadding: 3,
			header : false,
			border : 0,
			split: true,
			layout: 'border',
			items: [treepanel,contentpanel]
		});

		return page1_jExtPanel1_obj;
	}
});