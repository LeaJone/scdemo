﻿/**
 * 课程编辑界面
 */
Ext.define('system.zgyxy.tutor.biz.ReasonEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.zgyxy.tutor.biz.TutorOper'),
	initComponent:function(){
		var me = this;	 
        
		me.f_fid = Ext.create('Ext.form.field.Hidden',{
			name:'id'
		});
		
		var f_reason = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel : '请输入原因',
	    	labelAlign :'right',
	    	labelWidth : 80,
	        width : 340,
	        allowBlank : false,
	    	name : 'f_reason',
	    	maxLength : 200,
	    	blankText : '请输入拒绝原因',
	    	maxLengthText : '拒绝原因不能超过200个字符'
	    });
	    
		var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '15 10 5 10',
			items : [me.f_fid,f_reason]
		});
		
	    Ext.apply(this,{
	        width:400,
	        height:190,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.reasonSubmit(form , me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	},
	setFid:function(fid){
		this.f_fid.setValue(fid);
	}
});