﻿Ext.define('model_z_tutor', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'id',
            type: 'string'
        }
        ,
        {
            name: 'f_baseid',
            type: 'string'
        }
        ,
        {
            name: 'f_name',
            type: 'string'
        }
        ,
        {
            name: 'f_age',
            type: 'string'
        }
        ,
        {
            name: 'f_sexid',
            type: 'string'
        }
        ,
        {
            name: 'f_sexname',
            type: 'string'
        }
        ,
        {
            name: 'f_degree',
            type: 'string'
        }
        ,
        {
            name: 'f_professionalTitle',
            type: 'string'
        }
        ,
        {
            name: 'f_educationid',
            type: 'string'
        }
        ,
        {
            name: 'f_educationname',
            type: 'string'
        }
        ,
        {
            name: 'f_specialityid',
            type: 'string'
        }
        ,
        {
            name: 'f_specialityname',
            type: 'string'
        }
        ,
        {
            name: 'f_courses',
            type: 'string'
        }
        ,
        {
            name: 'f_direction',
            type: 'string'
        }
        ,
        {
            name: 'f_adddate',
            type: 'string'
        }
        ,
        {
            name: 'f_statusid',
            type: 'string'
        }
        ,
        {
            name: 'f_reason',
            type: 'string'
        }
    ]
});
