﻿Ext.define('column_z_tutor', {
    columns: [
        {
            xtype: 'rownumberer',
            text: '',
            sortable: true,
            width: 40,
            align: 'left'
        }
        ,
        {
            header: '姓名',
            dataIndex: 'f_name',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '性别',
            dataIndex: 'f_sexname',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '学位',
            dataIndex: 'f_degree',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '职称',
            dataIndex: 'f_professionalTitle',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '主讲课程',
            dataIndex: 'f_courses',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '研究方向',
            dataIndex: 'f_direction',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '添加时间',
            dataIndex: 'f_adddate',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                return system.UtilStatic.formatDateTime(value);
            }
        }
        ,
    	{
            header : '状态',
            dataIndex : 'f_statusid',
            align : 'center',
            flex : 1,
        	renderer : function(value, metaData, record) {
    			if (value == "ty") {
    				value = "<font style='color:red;'>停用</font>";
    			} else if(value == "qy"){
    				value = "<font style='color:green;'>启用</font>";
    			} else{
    				value = "<font style='color:blank;'>未审核</font>";
    			}
    			return value;
    		}
        }
    ]
});
