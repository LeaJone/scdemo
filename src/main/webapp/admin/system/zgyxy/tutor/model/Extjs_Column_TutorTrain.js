﻿Ext.define('column_z_tutortrain', {
    columns: [
        {
            xtype: 'rownumberer',
            text: '',
            sortable: true,
            width: 40,
            align: 'left'
        }
        ,
        {
            header: '参加时间',
            dataIndex: 'f_starttime',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                return system.UtilStatic.formatDateTime(value);
            }
        }
        ,
        {
            header: '时长',
            dataIndex: 'f_days',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '培训地点',
            dataIndex: 'f_address',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                return value;
            }
        }
        ,
        {
            header: '主办方',
            dataIndex: 'f_sponsor',
            sortable: true,
            align: 'center',
            flex: 1
        }
    ]
});
