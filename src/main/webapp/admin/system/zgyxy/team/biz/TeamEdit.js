﻿/**
 * 团队添加界面
 */
Ext.define('system.zgyxy.team.biz.TeamEdit', {
	extend : 'Ext.window.Window',
	requires : ['system.abasis.employee.model.Extjs_A_employee',
				'system.zgyxy.team.model.Extjs_Z_teamanduser'],
	grid : null,
	oper : Ext.create('system.zgyxy.team.biz.TeamOper'),
	initComponent:function(){
	    var me = this;
	    var tdmc = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '团队名称',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 700,
	        allowBlank: false,
	    	name: 'f_name',
	    	maxLength: 200
	    });

		var f_leadername = Ext.create('system.widget.FsdTextField',{
			fieldLabel: '负责人姓名',
			labelAlign:'right',
			labelWidth:80,
			width : 700,
			allowBlank: false,
			name: 'f_leadername',
			maxLength: 200
		});
	    
		var photo1 = Ext.create('system.widget.FsdTextFileManage', {
	        width : 700,
	    	zName : 'f_photo1',
	    	zFieldLabel : '团队风采1',
	    	zLabelWidth : 80,
	    	zIsShowButton : true,
	    	zIsGetButton : false,
	    	zIsUpButton : true,
	    	zIsReadOnly : true,
	    	zContentObj : me.content,
	    	zFileUpPath : me.oper.util.getImagePath()
		});
		
		var photo2 = Ext.create('system.widget.FsdTextFileManage', {
	        width : 700,
	    	zName : 'f_photo2',
	    	zFieldLabel : '团队风采2',
	    	zLabelWidth : 80,
	    	zIsShowButton : true,
	    	zIsGetButton : false,
	    	zIsUpButton : true,
	    	zIsReadOnly : true,
	    	zContentObj : me.content,
	    	zFileUpPath : me.oper.util.getImagePath()
		});
		
		var tdjj = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '团队简介',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 700,
	    	name: 'f_abstract',
	    	maxLength: 500
	    });

		var f_member = Ext.create('system.widget.FsdTextArea',{
			fieldLabel: '团队成员',
			labelAlign:'right',
			labelWidth:80,
			width : 700,
			name: 'f_member',
			maxLength: 500
		});

		var f_projects = Ext.create('system.widget.FsdUeditor',{
			fieldLabel: '团队项目',
			labelAlign:'right',
			labelWidth:80,
			width : 700,
			height : 420,
			name: 'f_projects',
			allowBlank: false
		});

	    var form = Ext.create('Ext.form.Panel',{
	    	border : false,
			padding : '5 0 0 5',
			items : [{
				name: "id",
				xtype: "hidden"
			}, tdmc, f_leadername, photo1, photo2, tdjj, f_member, f_projects]
        });
	        
	    Ext.apply(this,{
	    	width:730,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
				xtype : "fsdbutton",
				name : 'btnsave',
				text : '保存',
				iconCls : 'acceptIcon',
				handler : function() {
					me.oper.formSubmit(form , me.grid);
				}
			}, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});