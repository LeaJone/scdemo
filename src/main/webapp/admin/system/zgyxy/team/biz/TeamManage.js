﻿/**
 * 团队管理界面
 * 
 */

Ext.define('system.zgyxy.team.biz.TeamManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*', 'system.zgyxy.team.model.Extjs_Z_team'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.zgyxy.team.biz.TeamOper'),
	
	createContent : function(){
		var me = this;
		
		/**
		 * 查询文本框
		 */
		var queryText =  Ext.create("Ext.form.TextField", {
			name : 'queryTeamParam',
			emptyText : '请输入团队名称',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});

		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_team', // 显示列
			model: 'model_z_team',
			baseUrl : 'z_team/load_pagedata.do',
			tbar : [{
				xtype : "fsdbutton",
				text : "创建团队",
				iconCls : 'page_addIcon',
				handler : function() {
					me.oper.add(grid);
				}
			}, {
				xtype : "fsdbutton",
				text : "修改",
				iconCls : 'page_editIcon',
				handler : function() {
					me.oper.editor(grid);
				}
			}, {
				xtype : "fsdbutton",
				text : "删除",
				iconCls : 'page_deleteIcon',
				handler : function() {
				    me.oper.del(grid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid, 'ck');
				}
			}, '-', {
                text : '团队成果',
                iconCls : 'tagIcon',
                popedomCode : 'teamcgbj',
                handler : function() {
                    me.oper.Teamcg(grid);
                }
            }, '->', queryText, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  queryText.getValue();
			var pram = {name : name};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params, {jsonData : Ext.encode(pram)});
		});
		
		return grid;
	}
});