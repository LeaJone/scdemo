/**
 * 团队管理界面操作类
 *
 */
Ext.define('system.zgyxy.team.biz.TeamOper', {

	form : null,
	util : Ext.create('system.util.util'),

	/**
     * 添加
     */
	add : function(grid){
	    var win = Ext.create('system.zgyxy.team.biz.TeamEdit');
        win.setTitle('团队添加');
        win.modal = true;
        win.grid = grid;
        win.show();
	},
	
	/**
     * 修改
     */
	editor : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的团队！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能修改一个团队！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.zgyxy.team.biz.TeamEdit');
            win.setTitle('团队修改');
            win.modal = true;
            win.grid = grid;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'z_team/load_z_teambyid.do' , param , null , 
	        function(){
	            win.show();
	        });
        }
	},
 	
 	/**
     * 查看
     */
	view : function(grid, zt){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
	    if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要查看的团队！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能查看一支团队！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.zgyxy.team.biz.TeamEdit');
            win.setTitle('团队查看');
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            win.down('button[name=btnxz]').setVisible(false);
            win.idvalueaa.setValue(id);
            win.zt.setValue(zt);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'z_team/load_z_teambyid.do' , param , null , 
	        function(){
	            win.show();
	        });
        }
	},
	
	/**
     * 删除
     */
	del : function(grid){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
	    if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要删除的团队！');
        }else{
            Ext.MessageBox.show({
                title : '提示',
		        msg : '您确定要删除所选团队吗？',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
					    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
			            });
             			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('z_team/del_z_team_qx_z_teamsc.do' , param ,
                        function(){
            				Ext.MessageBox.alert('提示', '团队删除成功！');
                        	grid.getStore().reload(); 
                        }, null, me.form);
                    }
                }
            });
        }
	},

	/**
     * 表单提交
     */
     formSubmit : function(formpanel, grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_team/save_z_team_qx_z_teambj.do' , formpanel.form , '正在提交数据,请稍候.....',
			function(form, action){
                formpanel.up('window').close();
				Ext.MessageBox.alert('提示', '保存成功！');
                grid.getStore().reload();
            });
        }
     },
     
     /**
      * 团队成员添加
      */
     addUser : function(teamid, grid){
         var me = this;
    	 if (teamid == ''){
    		 Ext.MessageBox.alert('提示', '请先填写并保存团队信息！');
    	 }else{
             me.util.setParams("tdid", teamid);
    		 var win = Ext.create('system.zgyxy.team.biz.TeamOtherEdit');
             win.form.grid = grid;
             win.setTitle('团队成员添加');
             win.modal = true;
             win.show();
    	 }
 	},
 	
 	/**
     * 表单提交(团队成员)
     */
     formSubmitOther : function(tdid, id, grid){
        var me = this;
        var pram = {id : id, tdid : tdid};
        var param = {jsonData : Ext.encode(pram)};
        me.util.ExtAjaxRequest('z_teamanduser/save_z_teamanduser_qx_z_teamanduserbj.do', param,
            function(){
                Ext.MessageBox.alert('提示', '保存成功！');
                grid.up('window').idvalueaa.setValue(tdid);
                grid.getStore().reload();
            }, null, me.form);
     },
     
     /**
      * 删除团队成员
      */
     deletedid : function(id, grid, zt){
    	if(zt == 'ck' ){
    		Ext.MessageBox.alert('提示', '当前状态仅支持查看操作！');
    	} else {
    		var me = this;
     		var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
     		me.util.ExtAjaxRequest('z_teamanduser/del_z_teamanduser_qx_z_teamandusersc.do', param, 
     		    function(){
                    Ext.MessageBox.alert('提示', '删除成功！');
     			    grid.getStore().reload();
     	 	    }, null, me.form);
    	}
 	},

    /**
     * 企业成果
     */
    Teamcg : function(grid){
        var me = this;
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择要编辑的团队成果！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一条数据！');
        }else{
            var teamid = data[0].data.id;
            var teamname = data[0].data.f_name;
            me.util.setParams("teamcg_id", teamid);
            me.util.setParams("teamcg_mc", teamname);
            var win = Ext.create('system.zgyxy.teamcg.biz.teamcgManage');
            win.setTitle('团队成果编辑');
            win.modal = true;
            win.teamid = teamid;
            win.teamname = teamname;
            win.show();
        }
    }

});