﻿/**
 * 团队添加界面
 */
Ext.define('system.zgyxy.team.biz.TeamOtherEdit', {
	extend : 'Ext.window.Window',
	oper : Ext.create('system.zgyxy.team.biz.TeamOper'),
	initComponent:function(){
	    var me = this;
	    
	    me.form = Ext.create('system.zgyxy.team.biz.UserManage',{
	    	win : me
		});
	        
	    Ext.apply(this,{
            width : 800,
            height : 600,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[me.form],
	        buttons : [{
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}

});