﻿/**
 * 用户选择面板
 *
 * @author lihanlin
 */

Ext.define('system.zgyxy.team.biz.UserManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : ['system.abasis.employee.model.Extjs_A_employee'],
	header : false,
	win : null,
	grid : null,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.zgyxy.team.biz.TeamOper'),
	
	createContent : function(){
		var me = this;
		
		var queryid = me.oper.util.getParams("company").id;
        var tdid = me.oper.util.getParams("tdid");

		/**
		 * 左边的树面板
		 */
		var treepanel = Ext.create("system.widget.FsdTreePanel", {
			icon : 'resources/admin/icon/chart_organisation.png',
			baseUrl : 'A_Branch/load_AsyncBranchTreeByPopdom.do',
			title: '院系结构',
			rootText : me.oper.util.getParams("company").name,
			rootId : me.oper.util.getParams("company").id,
			region : "west", // 设置方位
			collapsible : true,//折叠
			width: 240,
            height : 528,
            border : 1
		});
		
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
            pageSize : 15,
			column : 'column_a_employeeemail', // 显示列
			model: 'model_a_employee',
			baseUrl : 'A_Employee/load_teamuserdata.do',
			tbar : {
				items : [ new Ext.form.TextField( {
				id : 'queryTeamUserName',
				name : 'queryTeamUserParam',
				emptyText : '请输入姓名查询',
				enableKeyEvents : true,
				listeners : {
					specialkey : function(field, e) {
						if (e.getKey() == Ext.EventObject.ENTER) {
							grid.getStore().reload();
						}
					}
				},
				width : 130
			}), {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}, '->', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]},
            listeners : {
                itemdblclick : function(obj, record){
                	me.oper.formSubmitOther(tdid, record.data.id, me.grid);
                	me.win.hide();
                }
            }
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  Ext.getCmp('queryTeamUserName').getValue();
			var pram = {fid : queryid , name : name};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		var contentpanel = Ext.create("Ext.panel.Panel", {
			width : 550,
            height : 528,
            border : 1,
			region: 'center',
			title: '友情提示：双击选中人员，将其添加到团队中！',
			autoScroll : true,
			items: [grid]
		});
		
		var page1_jExtPanel1_obj = new Ext.panel.Panel({
			layout : 'hbox',
			header : false,
			border : 0,
			items: [treepanel,contentpanel]
		});
		
		treepanel.on('itemclick', function(view,record){
			queryid = record.raw.id;
			grid.getStore().reload();
		});

		
		return page1_jExtPanel1_obj;
	}
});