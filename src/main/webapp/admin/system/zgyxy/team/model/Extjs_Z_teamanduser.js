﻿/**
 *团队成员
 */

Ext.define('model_z_teamanduser', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'f_teamid',
            type : 'string'
        }
        ,
        {
            name : 'f_userid',
            type : 'string'
        }
        ,
        {
            name : 'username',
            type : 'string'
        }
        ,
        {
            name : 'usersex',
            type : 'string'
        }
        ,
        {
            name : 'useracademy',
            type : 'string'
        }
        ,
        {
            name : 'userbranch',
            type : 'string'
        }
        ,
        {
            name : 'usernumber',
            type : 'string'
        }
    ]
});


/**
 *团队
 */
Ext.define('column_z_teamanduser', {
    columns: [ 
    	{
    		xtype : 'rownumberer',
    		text : 'NO',
    		sortable : true,
    		align : 'center',
    		flex : 1
        }
    	,
		{
			header : '成员名称',
			dataIndex : 'username',
			align : 'center',
			flex : 2
		}
		,
        {
            header : '性别',
            dataIndex : 'usersex',
            align : 'center',
            flex : 2
        }
		,
		{
            header : '院系',
            dataIndex : 'useracademy',
            align : 'center',
            flex : 2
        }
		,
		{
            header : '专业',
            dataIndex : 'userbranch',
            align : 'center',
            flex : 2
        }
        ,
        {
            header : '操作',
            dataIndex : 'f_userid',
            sortable : true,
            align : 'center',
            flex : 1,
  			renderer :function(value ,metaData ,record ){
  				metaData.style = 'color:blue;cursor:pointer;';
  				value = "<font style='color:blue;'>删除</font>";
  				return value;
  			}
        }
    ]
});

/**
 * 项目管理选择团队面板
 */
Ext.define('column_project_member', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            align : 'center',
            flex : 0.5
        }
        ,
        {
            header : '成员名称',
            dataIndex : 'username',
            align : 'center',
            flex : 1
        }
        ,
        {
            header : '性别',
            dataIndex : 'usersex',
            align : 'center',
            flex : 0.5
        }
        ,
        {
            header : '院系',
            dataIndex : 'useracademy',
            align : 'center',
            flex : 1.5
        }
        ,
        {
            header : '专业',
            dataIndex : 'userbranch',
            align : 'center',
            flex : 1.5
        }
    ]
});

