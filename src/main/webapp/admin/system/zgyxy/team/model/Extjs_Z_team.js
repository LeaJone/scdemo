﻿/**
 *团队
 */

Ext.define('model_z_team', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'f_name',
            type : 'string'
        }
        ,
        {
            name : 'f_abstract',
            type : 'string'
        }
        ,
        {
            name : 'f_leaderno',
            type : 'string'
        }
        ,
        {
            name : 'f_leaderid',
            type : 'string'
        }
        ,
        {
            name : 'f_leadername',
            type : 'string'
        }
        ,
        {
            name : 'f_creatorid',
            type : 'string'
        }
        ,
        {
            name : 'f_creatorname',
            type : 'string'
        }
        ,
        {
            name : 'f_adddate',
            type : 'string'
        }
        ,
        {
            name : 'f_photo1',
            type : 'string'
        }
        ,
        {
            name : 'f_photo2',
            type : 'string'
        }  
        ,
        {
            name : 'f_leaderphone',
            type : 'string'
        } 
    ]
});


/**
 *团队
 */
Ext.define('column_z_team', {
    columns: [ 
    	{
    		xtype : 'rownumberer',
    		text : 'NO',
    		sortable : true,
    		align : 'center',
    		flex : 1
        }
    	,
		{
			header : '团队名称',
			dataIndex : 'f_name',
			align : 'center',
			flex : 4
		}
		,
        {
            header : '负责人学号',
            dataIndex : 'f_leaderno',
            align : 'center',
            flex : 2
        }
		,
		{
            header : '负责人姓名',
            dataIndex : 'f_leadername',
            align : 'center',
            flex : 2
        }
		,
		{
            header : '负责人电话',
            dataIndex : 'f_leaderphone',
            align : 'center',
            flex : 2
        }
        ,
        {
            header : '创建时间',
            dataIndex : 'f_adddate',
            align : 'center',
            flex : 2,
            renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value);
			}
        }
    ]
});

Ext.define('column_z_teamxz', {
    columns: [ 
    	{
    		xtype : 'rownumberer',
    		text : 'NO',
    		sortable : true,
    		align : 'center',
    		flex : 1
        }
    	,
		{
			header : '团队名称',
			dataIndex : 'f_name',
			align : 'center',
			flex : 4
		}
		,
		{
            header : '负责人姓名',
            dataIndex : 'f_leadername',
            align : 'center',
            flex : 2
        }
	    ,
        {
            header : '创建时间',
            dataIndex : 'f_adddate',
            align : 'center',
            flex : 2,
            renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value);
			}
        }
        
    ]
});

/**
 * 项目管理选择团队面板
 */
Ext.define('column_project_team', {
    columns: [
        {
            header : '团队名称',
            dataIndex : 'f_name',
            align : 'center',
            flex : 2
        }
        ,
        {
            header : '团队负责人',
            dataIndex : 'f_leadername',
            align : 'center',
            flex : 1
        }
        ,
        {
            header : '团队成立时间',
            dataIndex : 'f_adddate',
            align : 'center',
            flex : 1,
            renderer :function(value){
                return system.UtilStatic.formatDateTime(value, false);
            }
        }

    ]
});