﻿Ext.define('model_z_qyrz', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'id',
            type: 'string'
        }
        ,
        {
            name: 'qymc',
            type: 'string'
        }
        ,
        {
            name: 'qyxz_name',
            type: 'string'
        }
        ,
        {
            name: 'frdb',
            type: 'string'
        }
        ,
        {
            name: 'zcsj',
            type: 'string'
        }
        ,
        {
            name: 'zczb',
            type: 'string'
        }
        ,
        {
            name: 'yyzz',
            type: 'string'
        }
        ,
        {
            name: 'subtime',
            type: 'string'
        }
        ,
        {
            name: 'status',
            type: 'string'
        }
    ]
});
