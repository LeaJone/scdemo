﻿Ext.define('column_z_qyrz', {
    columns: [
        {
            xtype: 'rownumberer',
            text: '',
            sortable: true,
            width: 30,
            align: 'left'
        }
        ,
        {
            header: '企业名称',
            dataIndex: 'qymc',
            sortable: true,
            align: 'center',
            flex: 1

        }
        ,
        {
            header: '企业性质',
            dataIndex: 'qyxz_name',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '法人代表',
            dataIndex: 'frdb',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '注册时间',
            dataIndex: 'zcsj',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                return system.UtilStatic.formatDateTime(value);
            }
        }
        ,
        {
            header: '注册资本',
            dataIndex: 'zczb',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '是否上传营业执照',
            dataIndex: 'yyzz',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                if (value != null && value != "") {
                    value = "<font style='color:green;'>已上传</font>";
                } else {
                    value = "<font style='color:red;'>未上传</font>";
                }
                return value;
            }
        }
        ,
        {
            header: '申请时间',
            dataIndex: 'subtime',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                return system.UtilStatic.formatDateTime(value);
            }
        }
        ,
        {
            header: '审核状态',
            dataIndex: 'status',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                if (value == 0) {
                    value = "<font style='color:black;'>入驻待审核</font>";
                } else if (value == 1) {
                    value = "<font style='color:green;'>入驻审核通过</font>";
                } else if (value == -1) {
                    value = "<font style='color:red;'>入驻审核驳回</font>";
                }else{
                    value = "<font style='color:blue;'>毕业待审核</font>";
                }
                return value;
            }
        }
    ]
});
