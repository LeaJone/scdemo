/**
 * 模板操作类
 * @author lmb
 */
Ext.define('system.zgyxy.qyrz.biz.QyrzOper', {

	form : null,
	util : Ext.create('system.util.util'),

    /**
     * 申请审核界面
     */
    auditSqWin : function(grid){
        var me = this;
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择要审核的企业信息！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一条数据！');
        }else{
            var status = data[0].data.status;
            if(status == "0"){
                var id = data[0].data.id;
                var win = Ext.create('system.zgyxy.qyrz.biz.QyrzEdit');
                win.setTitle('企业入驻审核');
                win.modal = true;
                win.grid = grid;
                win.belongsid = id;
                win.down('button[name=btnsave]').setVisible(false);
                var pram = {id : id};
                var param = {jsonData : Ext.encode(pram)};
                me.util.formLoad(win.down('form').getForm(), 'z_qyrz/load_qyrzbyid.do', param, null, function(){
                    win.show();
                });
            }else{
                Ext.MessageBox.alert('提示', '该信息不是【入驻待审核】状态，无法执行该操作！');
            }
        }
    },

    /**
     * 毕业审核界面
     */
    auditClWin : function(grid){
        var me = this;
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择要审核的企业信息！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一条数据！');
        }else{
            var status = data[0].data.status;
            if(status == "10086"){
                var id = data[0].data.id;
                var win = Ext.create('system.zgyxy.qyrz.biz.QyclAuditEdit');
                win.setTitle('企业毕业审核');
                win.modal = true;
                win.grid = grid;
                win.belongsid = id;
                var pram = {id : id};
                var param = {jsonData : Ext.encode(pram)};
                me.util.formLoad(win.down('form').getForm(), 'z_qyrzjl/load_qyrzjlbyid.do', param, null, function(){
                    win.show();
                });
            }else{
                Ext.MessageBox.alert('提示', '该信息不是【毕业待审核】状态，无法执行该操作！');
            }
        }
    },

    /**
     * 审核驳回界面
     */
    auditFalseWin : function(winForm, grid, id, type){
        var win = Ext.create('system.zgyxy.qyrz.biz.QyrzAuditFalse');
        win.setTitle('企业入驻审核 - 驳回');
        win.grid = grid;
        win.parentWin = winForm;
        win.belongsid = id;
        win.type = type;
        win.modal = true;
        win.show();
    },

    /**
     * 审核通过
     */
    audit : function(winForm, parentWin, grid, id, type, reason){
        var me = this;
        Ext.MessageBox.show({
            title : '询问',
            msg : '您确定要执行该操作吗？',
            width : 250,
            buttons : Ext.MessageBox.YESNO,
            icon : Ext.MessageBox.QUESTION,
            fn : function(btn) {
                if (btn == 'yes') {
                    var param = {
                        jsonData : Ext.encode({ id : id, type : type, reason : reason})
                    };
                    me.util.ExtAjaxRequest('z_qyrz/audit_qyrz_qx_qyrzsh.do', param, function(){
                        winForm.close();
                        if(parentWin != null){
                            parentWin.close();
                        }
                        grid.getStore().reload();
                        Ext.MessageBox.alert('提示', '已将审核结果通过邮件告知该企业！');
                    }, null, winForm);
                }
            }
        });
    },

    /**
     * 编辑
     */
    editorWin : function(grid){
        var me = this;
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择要编辑的企业信息！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一条数据！');
        }else{
            var id = data[0].data.id;
            var win = Ext.create('system.zgyxy.qyrz.biz.QyrzEdit');
            win.setTitle('企业入驻编辑');
            win.modal = true;
            win.grid = grid;
            win.belongsid = id;
            win.down('button[name=btnshtg]').setVisible(false);
            win.down('button[name=btnshbh]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(win.down('form').getForm(), 'z_qyrz/load_qyrzbyid.do', param, null, function(){
                win.show();
            });
        }
    },

    /**
     * 表单提交
     */
    formSubmit : function(formpanel , grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_qyrz/save_qyrz_qx_qyrzbj.do', formpanel.form, '正在提交数据,请稍候.....', function(){
                formpanel.up('window').close();
                grid.getStore().reload();
                Ext.MessageBox.alert('提示', '更改成功，请等待管理员审核！');
            },
            function (form, action){
                Ext.MessageBox.alert('异常', action.result.message);
            });
        }
    },

    /**
     * 毕业
     */
    goback : function(grid){
        var me = this;
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择要操作的数据！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一条数据！');
        }else{
            var status = data[0].data.status;
            if(status == "1"){
                var qyid = data[0].data.id;
                var qymc = data[0].data.qymc;
                var win = Ext.create('system.zgyxy.qyrz.biz.QyclEdit');
                win.setTitle('企业毕业');
                win.grid = grid;
                win.qyid.setValue(qyid);
                win.qymc.setValue(qymc);
                win.modal = true;
                win.show();
            }else{
                Ext.MessageBox.alert('提示', '该企业不是入驻状态，无法执行毕业操作！');
            }
        }
    },

    /**
     * 提交毕业申请
     */
    subCheLi : function(formpanel , grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_qyrzjl/save_qyrzjl_qx_qyrzjlbj.do', formpanel.form, '正在提交数据,请稍候.....', function(){
                formpanel.up('window').close();
                grid.getStore().reload();
                Ext.MessageBox.alert('提示', '提交成功，请耐心等待管理员审核！');
            });
        }
    },

    subToCheLi : function(win, grid){
        var me = this;
        var data = grid.getSelectionModel().getSelection();
        var id = data[0].data.id;
        var pram = {id : id};
        var param = {jsonData : Ext.encode(pram)};
        me.util.ExtAjaxRequest('z_qyrz/del_qyrz_qx_qyrzsc.do', param, function(){
            Ext.MessageBox.alert('提示', '操作成功，已注销企业登录账号！');
            win.close();
            grid.getStore().reload();
        }, null, me.form);
    },

    /**
     * 查看
     */
    viewWin : function(grid){
        var me = this;
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择要查看的企业信息！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一条数据！');
        }else{
            var id = data[0].data.id;
            var win = Ext.create('system.zgyxy.qyrz.biz.QyrzEdit');
            win.setTitle('企业入驻信息查看');
            win.modal = true;
            win.grid = grid;
            win.belongsid = id;
            win.down('button[name=btnshtg]').setVisible(false);
            win.down('button[name=btnshbh]').setVisible(false);
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(win.down('form').getForm(), 'z_qyrz/load_qyrzbyid.do', param, null, function(){
                win.show();
            });
        }
    },

    /**
     * 企业成果
     */
    Qycg : function(grid){
        var me = this;
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择要编辑的项目成果！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一条数据！');
        }else{
            var qyid = data[0].data.id;
            var qymc = data[0].data.qymc;
            me.util.setParams("qycg_id", qyid);
            me.util.setParams("qycg_mc", qymc);
            var win = Ext.create('system.zgyxy.qycg.biz.qycgManage');
            win.setTitle('项目成果编辑');
            win.modal = true;
            win.qyid = qyid;
            win.qymc = qymc;
            win.show();
        }
    }

});