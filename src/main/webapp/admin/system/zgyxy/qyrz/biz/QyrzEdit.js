/**
 * 企业入驻审核界面
 *
 * @author lhl
 */
Ext.define('system.zgyxy.qyrz.biz.QyrzEdit',{
	extend : 'Ext.window.Window',
	grid : null,
    belongsid : '',
	oper : Ext.create('system.zgyxy.qyrz.biz.QyrzOper'),
    initComponent:function(){
        var me = this;

        var qymc = new Ext.create('system.widget.FsdTextField',{
            fieldLabel: '企业名称',
            labelAlign:'right',
            labelWidth:80,
            width : 340,
            name: 'qymc',
            maxLength: 50,
            allowBlank: false
        });

        var qyxz_code = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'企业性质',
            labelAlign:'right',
            labelWidth:80,
            width : 340,
            name : 'qyxz_code',//提交到后台的参数名
            key : 'qyxz',
            allowBlank: false,
            hiddenName : 'qyxz_name'//提交隐藏域Name
        });
        var qyxz_name = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '企业性质名称',
            name: 'qyxz_name'
        });

        var qyyx = new Ext.create('system.widget.FsdTextField',{
            fieldLabel: '企业邮箱',
            labelAlign:'right',
            labelWidth:80,
            width : 340,
            name: 'qyyx',
            maxLength: 100,
            allowBlank: false
        });

        var frdb = new Ext.create('system.widget.FsdTextField',{
            fieldLabel: '法人代表',
            labelAlign:'right',
            labelWidth:80,
            width : 340,
            name: 'frdb',
            maxLength: 50,
            allowBlank: false
        });

        var frsfzh = new Ext.create('system.widget.FsdTextField',{
            fieldLabel: '法人身份证',
            labelAlign:'right',
            labelWidth:80,
            width : 340,
            name: 'frsfzh',
            maxLength: 18,
            allowBlank: false
        });

        var frlxdh = new Ext.create('system.widget.FsdTextField',{
            fieldLabel: '法人电话',
            labelAlign:'right',
            labelWidth:80,
            width : 340,
            name: 'frlxdh',
            maxLength: 18,
            allowBlank: false
        });

        var jyfw = Ext.create('system.widget.FsdTextArea',{
            fieldLabel: '经营范围',
            labelAlign:'right',
            labelWidth:80,
            width : 680,
            height : 100,
            name: 'jyfw',
            maxLength: 500,
            allowBlank: false
        });

        var zycp = Ext.create('system.widget.FsdTextArea',{
            fieldLabel: '主要产品',
            labelAlign:'right',
            labelWidth:80,
            width : 680,
            height : 100,
            name: 'zycp',
            maxLength: 500,
            allowBlank: false
        });

        var zcsj = Ext.create('system.widget.FsdDateTime',{
            fieldLabel: '注册时间',
            labelAlign:'right',
            labelWidth:80,
            width : 340,
            name: 'zcsj',
            allowBlank: false
        });

        var zcdd = new Ext.create('system.widget.FsdTextField',{
            fieldLabel: '注册地点',
            labelAlign:'right',
            labelWidth:80,
            width : 340,
            name: 'zcdd',
            maxLength: 200,
            allowBlank: false
        });

        var zczb = new Ext.create('system.widget.FsdTextField',{
            fieldLabel: '注册资本',
            labelAlign:'right',
            labelWidth:80,
            width : 340,
            name: 'zczb',
            maxLength: 50,
            allowBlank: false
        });

        var yyzz = Ext.create('system.widget.FsdTextFileManage', {
            width : 340,
            zName : 'yyzz',
            zFieldLabel : '营业执照',
            zLabelWidth : 80,
            zIsUpButton : true,
            zIsDownButton : true,
            zIsShowButton : true,
            zFileUpPath : me.oper.util.getImagePath(),
            zAllowBlank: false
        });

        var panel = Ext.create('Ext.form.FieldSet',{
            title : '企业入驻申请信息',
            items:[{
                layout : 'column',
                border : false,
                items:[{
                    columnWidth : 0.5,
                    border:false,
                    items:[qymc, qyyx, frsfzh, zcsj, zczb ]
                }, {
                    columnWidth : 0.5,
                    border:false,
                    items:[qyxz_code, qyxz_name, frdb, frlxdh, zcdd, yyzz ]
                }]
            }, jyfw, zycp ]
        });

        me.form = Ext.create('Ext.form.Panel',{
            xtype : 'form',
            header : false,
            border : false,
            padding : '20 20 15 20',
            items : [
                {
                    name: "id",
                    xtype: "hidden"
                }, panel ]
        });

        var buttonItem = [{
            xtype : "fsdbutton",
            popedomCode : 'qyrzsh',
            name : 'btnshtg',
            text : '审核通过',
            zIsSpace : true,
            iconCls : 'acceptIcon',
            handler : function() {
            	me.oper.audit(me, null, me.grid, me.belongsid, "1", "");
            }
        }, {
            xtype : "fsdbutton",
            popedomCode : 'qyrzsh',
            name : 'btnshbh',
            text : '审核驳回',
            zIsSpace : true,
            iconCls : 'tbar_synchronizeIcon',
            handler : function() {
                me.oper.auditFalseWin(me, me.grid, me.belongsid, "-1");
            }
        }, {
            xtype : "fsdbutton",
            popedomCode : 'qyrzbj',
            name : 'btnsave',
            text : '更 改',
            iconCls : 'acceptIcon',
            handler : function() {
                me.oper.formSubmit(me.form , me.grid);
            }
        }, {
            text : '关闭',
            iconCls : 'deleteIcon',
            handler : function() {
                me.close();
            }
        }];
        Ext.apply(this,{
            layout : 'fit',
            bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
            resizable:false,// 改变大小
            items :[me.form],
            buttons : buttonItem
        });
        this.callParent(arguments);
    }
});