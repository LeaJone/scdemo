﻿/**
 * 企业入驻管理界面
 * @author lhl
 */
Ext.define('system.zgyxy.qyrz.biz.QyrzManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.zgyxy.qyrz.model.Extjs_Column_Qyrz',
	             'system.zgyxy.qyrz.model.Extjs_Model_Qyrz'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.zgyxy.qyrz.biz.QyrzOper'),
    
	createTable : function() {
	    var me = this;

        /**
         * 查询文本框
         */
        var queryText =  Ext.create("Ext.form.TextField" , {
            name : 'queryParam',
            emptyText : '请输入企业名称',
            enableKeyEvents : true,
            width : 130,
            listeners : {
                specialkey : function(field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        grid.getStore().reload();
                    }
                }
            }
        });
	    
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_qyrz',
			model : 'model_z_qyrz',
			baseUrl : 'z_qyrz/load_pagedata.do',
			border : false,
			tbar : [{
                xtype : "fsdbutton",
                text : "入驻审核",
                iconCls : 'tickIcon',
                popedomCode : 'qyrzsh',
                handler : function() {
                    me.oper.auditSqWin(grid);
                }
            }, {
                xtype : "fsdbutton",
                text : "毕业审核",
                iconCls : 'tickIcon',
                popedomCode : 'qyrzsh',
                handler : function() {
                    me.oper.auditClWin(grid);
                }
            }, '-', {
                xtype : "fsdbutton",
                text : "企业信息编辑",
                iconCls : 'page_editIcon',
                popedomCode : 'qyrzbj',
                handler : function() {
                    me.oper.editorWin(grid);
                }
            }, {
                xtype : "fsdbutton",
                text : "申请毕业",
                iconCls : 'page_deleteIcon',
                popedomCode : 'qyrzsc',
                handler : function() {
                    me.oper.goback(grid);
                }
            }, {
                text : '信息查看',
                iconCls : 'magnifierIcon',
                handler : function() {
                    me.oper.viewWin(grid);
                }
            }, {
                text : '项目成果',
                iconCls : 'tagIcon',
                popedomCode : 'qycgbj',
                handler : function() {
                    me.oper.Qycg(grid);
                }
            }, '->', queryText, {
                text: '企业查询',
                iconCls: 'magnifierIcon',
                handler: function () {
                    grid.getStore().reload();
                }
            }, {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]
		});
		grid.getStore().on('beforeload', function(s) {
            var text =  queryText.getValue();
            var pram = {qymc : text};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		return grid;
	}
});