/**
 * 企业入驻审批驳回界面
 *
 * @author lhl
 */
Ext.define('system.zgyxy.qyrz.biz.QyrzAuditFalse',{
	extend : 'Ext.window.Window',
	grid : null,
    parentWin : null,
    belongsid : '',
    type : '',
	oper : Ext.create('system.zgyxy.qyrz.biz.QyrzOper'),
    initComponent:function(){
        var me = this;

        var reason = Ext.create('system.widget.FsdTextArea',{
            fieldLabel: '驳回原因',
            labelAlign:'right',
            labelWidth:80,
            width : 690,
            height : 150,
            name: 'reason'
        });

        var form = Ext.create('Ext.form.Panel',{
            xtype : 'form',
            defaultType : 'textfield',
            header : false,// 是否显示标题栏
            border : 0,
            padding : '15 15 10 0',
            items : [reason]
        });

        Ext.apply(this,{
            layout : 'fit',
            bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
            resizable:false,// 改变大小
            items :[form],
            buttons : [{
                text : '确定',
                iconCls : 'acceptIcon',
                handler : function() {
                    me.oper.audit(me, me.parentWin, me.grid, me.belongsid, me.type, reason.getValue());
                }
            }, {
                text : '关闭',
                iconCls : 'deleteIcon',
                handler : function() {
                    me.close();
                }
            }]
        });
        this.callParent(arguments);
    }
});