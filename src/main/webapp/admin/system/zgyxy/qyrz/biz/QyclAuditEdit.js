﻿/**
 * 企业毕业审核界面
 *
 * @author lhl
 */
Ext.define('system.zgyxy.qyrz.biz.QyclAuditEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.zgyxy.qyrz.biz.QyrzOper'),
	initComponent:function(){
	    var me = this;
	    
	    var qymc = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '企业名称',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'qymc'
	    });
	    
	    var remark = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '毕业原因',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'remark'
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '15 15 10 0',
			items : [{
                name: "id",
                xtype: "hidden"
            }, qymc, remark ]
        });
	    
	    Ext.apply(this,{
            layout : 'fit',
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
			    text : '同意毕业',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.subToCheLi(me, me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});