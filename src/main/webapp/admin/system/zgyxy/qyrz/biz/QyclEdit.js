/**
 * 企业毕业界面
 *
 * @author lhl
 */
Ext.define('system.zgyxy.qyrz.biz.QyclEdit',{
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.zgyxy.qyrz.biz.QyrzOper'),
    initComponent:function(){
        var me = this;

        me.qyid = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '企业编号',
            name: 'qyid'
        });

        me.qymc = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '企业名称',
            name: 'qymc'
        });

        var remark = Ext.create('system.widget.FsdTextArea',{
            fieldLabel: '毕业原因',
            labelAlign:'right',
            labelWidth:80,
            width : 690,
            height : 150,
            name: 'remark'
        });

        var form = Ext.create('Ext.form.Panel',{
            xtype : 'form',
            defaultType : 'textfield',
            header : false,// 是否显示标题栏
            border : 0,
            padding : '15 15 10 0',
            items : [{
                name: "id",
                xtype: "hidden"
            }, me.qyid, me.qymc, remark ]
        });

        Ext.apply(this,{
            layout : 'fit',
            bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
            resizable:false,// 改变大小
            items :[form],
            buttons : [{
                text : '确定',
                popedomCode : 'qyrzsc',
                iconCls : 'acceptIcon',
                handler : function() {
                    me.oper.subCheLi(form, me.grid);
                }
            }, {
                text : '关闭',
                iconCls : 'deleteIcon',
                handler : function() {
                    me.close();
                }
            }]
        });
        this.callParent(arguments);
    }
});