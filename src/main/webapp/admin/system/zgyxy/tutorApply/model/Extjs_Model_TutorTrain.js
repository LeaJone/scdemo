﻿Ext.define('model_z_tutortrain', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'id',
            type: 'string'
        }
        ,
        {
            name: 'f_starttime',
            type: 'string'
        }
        ,
        {
            name: 'f_days',
            type: 'string'
        }
        ,
        {
            name: 'f_address',
            type: 'string'
        }
        ,
        {
            name: 'f_sponsor',
            type: 'string'
        }
    ]
});
