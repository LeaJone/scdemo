﻿/**
 * 导师培训添加界面
 * @author lumingbao
 */
Ext.define('system.zgyxy.tutorApply.biz.TrainEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.zgyxy.tutorApply.biz.TrainOper'),
	initComponent:function(){
		var me = this;

		var id = Ext.create('Ext.form.field.Hidden',{
			fieldLabel: '编号',
			name : 'id'
		});

		me.f_tutorid = Ext.create('Ext.form.field.Hidden',{
			name:'f_tutorid'
		});

		var f_starttime = new Ext.create('system.widget.FsdDateField',{
			fieldLabel: '培训时间',
			labelAlign:'right',
			labelWidth:90,
			width : 360,
			format: 'Y年m月',
			altFormats : 'Ym',
			submitFormat : 'Ym',
			name: 'f_starttime',
			maxLength: 25,
			allowBlank: false
		});

		var f_days = new Ext.create('system.widget.FsdTextField',{
			fieldLabel: '培训时长',
			labelAlign:'right',
			labelWidth:90,
			width : 360,
			name: 'f_days',
			maxLength: 25,
			allowBlank: false
		});

		var f_address = new Ext.create('system.widget.FsdTextField',{
			fieldLabel: '培训地点',
			labelAlign:'right',
			labelWidth:90,
			width : 360,
			name: 'f_address',
			maxLength: 25,
			allowBlank: false
		});

		var f_sponsor = new Ext.create('system.widget.FsdTextField',{
			fieldLabel: '主办单位',
			labelAlign:'right',
			labelWidth:90,
			width : 360,
			name: 'f_sponsor',
			maxLength: 25,
			allowBlank: false
		});

		var f_content = new Ext.create('system.widget.FsdTextArea',{
			fieldLabel: '培训内容',
			labelAlign:'right',
			labelWidth:90,
			width : 360,
			name: 'f_content',
			maxLength: 1000,
			allowBlank: false
		});

		var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '15 10 5 10',
			items : [
				id,
				me.f_tutorid ,
				f_starttime,
				f_days,
				f_address,
				f_sponsor,
				f_content]
		});

		Ext.apply(this,{
			layout : 'fit',
			width:400,
			height:300,
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			items :[form],
			buttons : [{
				name : 'btnsave',
				text : '保存',
				iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
				handler : function() {
					me.oper.formSubmit(form, me.grid);
				}
			}, {
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	},

	setFid:function (fid) {
		this.f_tutorid.setValue(fid);
	}
});