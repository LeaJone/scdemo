/**
 * 项目报账操作类
 * @author lumingbao
 */
Ext.define('system.zgyxy.project.biz.money.ProjectMoneyOper', {

	util : Ext.create('system.util.util'),

    /**
     * 资料添加
     */
    add : function(grid, projectid){
        var win = Ext.create('system.zgyxy.project.biz.money.ProjectMoneyEdit');
        win.setTitle('项目报账');
        win.modal = true;
        win.grid = grid;
        win.setProjectId(projectid);
        win.show();
    },

    /**
     * 修改
     */
    editor : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目报账信息！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能修改一个项目报账信息！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.zgyxy.project.biz.money.ProjectMoneyEdit');
            win.setTitle('项目报账修改');
            win.modal = true;
            win.grid = grid;
            win.setProjectId(id)
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(win.down('form').getForm() , 'z_projectbill/load_z_projectbillbyid.do' , param , null ,
                function(){
                    win.show();
                });
        }
    },

    /**
     * 查看
     */
    view : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要查看的项目报账信息！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能查看一个项目报账信息！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.zgyxy.project.biz.money.ProjectMoneyEdit');
            win.setTitle('项目报账查看');
            win.modal = true;
            win.setProjectId(id);
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(win.down('form').getForm() , 'z_projectbill/load_z_projectbillbyid.do' , param , null ,
                function(){
                    win.show();
                });
        }
    },

    /**
     * 删除资料
     */
    del : function(grid){
        var me = this;
        // 获取选中的行s
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要删除的项目报账信息！');
        }else{
            Ext.MessageBox.show({
                title : '提示',
                msg : '您确定要删除所选的项目报账信息吗?',
                width : 250,
                buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                        var dir = new Array();
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
                        });
                        var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('z_projectbill/del_z_projectbill_qx_z_projectbillsc.do' , param ,
                            function(){
                                Ext.MessageBox.alert('提示', '删除成功！');
                                grid.getStore().reload();
                            }, null, me.form);
                    }
                }
            });
        }
    },

    /**
     * 表单提交
     * @param formpanel
     * @param grid
     */
    moneyFormSubmit : function(formpanel, grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_projectbill/save_z_projectbill_qx_z_projectbillbj.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    grid.getStore().reload();
                    Ext.MessageBox.alert('提示', '保存成功！');
                    formpanel.up('window').close();
                }, null);
        }
    },

    /**
     * 文件下载
     */
    download : function(f_pdf){
        var me = this;
        if(f_pdf != "" && f_pdf != null){
            Ext.MessageBox.show({
                title : '询问',
                msg : '你确定要下载吗？',
                width : 250,
                buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                        window.open('Common/download.do?path=' + f_pdf);
                    }
                }
            });
        }
    },

    /**
     * 是否启用停用
     */
    isEnabled : function(grid, isQT){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
                msg : '您确定要修改审核状态吗?',
                width : 250,
                buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                        var dir = new Array();
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
                        });
                        var pram = {ids : dir, isenabled : isQT};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('z_projectbill/enabled_bill.do' , param ,
                            function(response, options){
                                Ext.MessageBox.alert('提示', '报账审核状态修改成功！');
                                grid.getStore().reload();
                            });
                    }
                }
            });
        }
    }
});