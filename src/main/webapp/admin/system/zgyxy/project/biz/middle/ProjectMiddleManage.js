﻿/**
 * 项目中期检查管理界面
 * @author lumingbao
 */
Ext.define('system.zgyxy.project.biz.middle.ProjectMiddleManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.zgyxy.project.model.middle.Extjs_Column_ProjectMiddle',
	             'system.zgyxy.project.model.middle.Extjs_Model_ProjectMiddle'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.zgyxy.project.biz.middle.ProjectMiddleOper'),
    
	createTable : function() {
	    var me = this;

        /**
         * 查询文本框
         */
        var queryText =  Ext.create("Ext.form.TextField" , {
            name : 'queryParam',
            emptyText : '请输入项目名称',
            enableKeyEvents : true,
            width : 130,
            listeners : {
                specialkey : function(field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        grid.getStore().reload();
                    }
                }
            }
        });
	    
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_projectmiddle',
			model : 'model_z_projectmiddle',
			baseUrl : 'z_project/load_middlepagedata.do',
			border : false,
			tbar : [{
                xtype : "fsdbutton",
                text : "提交中期检查",
                iconCls : 'add',
                handler : function() {
                    me.oper.edit(grid);
                }
            },  '->', queryText, {
                text: '查询',
                iconCls: 'magnifierIcon',
                handler: function () {
                    grid.getStore().reload();
                }
            }, {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]
		});
		grid.getStore().on('beforeload', function(s) {
            var text =  queryText.getValue();
            var pram = {name : text};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		return grid;
	}
});