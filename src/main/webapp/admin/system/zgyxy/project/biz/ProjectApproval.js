﻿/**
 * 项目立项
 * @author lumingbao
 */
Ext.define(	'system.zgyxy.project.biz.ProjectApproval', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.zgyxy.project.biz.ProjectOper'),
	initComponent:function(){
		var me = this;

		me.projectids = Ext.create('Ext.form.field.Hidden',{
			name: 'projectids',
		});

		var typeid = Ext.create('system.widget.FsdComboBoxZD',{
			fieldLabel:'项目级别',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name : 'typeid',//提交到后台的参数名
			key : 'jsjb',//参数
			allowBlank: false,
			hiddenName : 'typename',//提交隐藏域Name
			zCallback:function (code, name) {
				if(code == "xj"){
					f_schooltypeid.setVisible(true);
				}else{
					f_schooltypeid.setVisible(false);
					f_schooltypeid.setValue("");
					f_schooltypename.setValue("");
				}
			}
		});
		var typename = Ext.create('Ext.form.field.Hidden',{
			fieldLabel: '项目级别名称',
			name: 'typename'
		});


		var f_schooltypeid = Ext.create('system.widget.FsdComboBoxZD',{
			fieldLabel:'项目类型',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name : 'f_schooltypeid',//提交到后台的参数名
			key : 'xjxmlx',//参数
			hiddenName : 'f_schooltypename'//提交隐藏域Name
		});
		var f_schooltypename = Ext.create('Ext.form.field.Hidden',{
			fieldLabel: '项目类型名称',
			name: 'f_schooltypename'
		});
		f_schooltypeid.setVisible(false);

		var money = Ext.create('system.widget.FsdTextField',{
			name: 'money',
			fieldLabel: '项目金额',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			maxLength: 500,
			allowBlank: false
		});
		var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 20 15 5',
			items : [
				me.projectids,
				typeid,
				typename,
				f_schooltypeid,
				f_schooltypename,
				money,
			]
		});

		Ext.apply(this,{
			layout : 'fit',
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			items :[form],
			buttons : [{
				name : 'btnsave',
				text : '提交',
				iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
				handler : function() {
					me.oper.projectApprovalSubmit(form, me.grid);
				}
			}, {
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	},

	/**
	 * 设置项目编码
	 * @param tutorid
	 */
	setProjectids : function (ids) {
		this.projectids.setValue(ids);
	}
});