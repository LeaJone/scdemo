﻿/**
 * 项目中期检查表编辑界面
 * @author lumingbao
 */
Ext.define(	'system.zgyxy.project.biz.middle.ProjectMiddleEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	height:600,
	width:850,
	requires : [ 'system.zgyxy.project.model.middle.Extjs_Column_ProjectMiddleNext',
		'system.zgyxy.project.model.middle.Extjs_Model_ProjectMiddleNext'],
	oper : Ext.create('system.zgyxy.project.biz.middle.ProjectMiddleOper'),
	nextOper : Ext.create('system.zgyxy.project.biz.middle.ProjectMiddleNextOper'),
	initComponent:function(){
		var me = this;

		me.f_id = Ext.create('Ext.form.field.Hidden',{
			name: 'id'
		});

		me.f_projectId = Ext.create('Ext.form.field.Hidden',{
			name: 'f_projectid'
		});

		var f_projectprogress = Ext.create('system.widget.FsdUeditor',{
			labelAlign:'right',
			labelWidth:1,
			width: 800,
			height : 400,
			style: {
				marginBottom: '25px'
			},
			name: 'f_projectprogress'
		});

		var f_mainproblem = Ext.create('system.widget.FsdUeditor',{
			labelAlign:'right',
			labelWidth:1,
			width: 800,
			height : 400,
			style: {
				marginBottom: '25px'
			},
			name: 'f_mainproblem'
		});

		var f_resourceuse = Ext.create('system.widget.FsdUeditor',{
			labelAlign:'right',
			labelWidth:1,
			width: 800,
			height : 400,
			style: {
				marginBottom: '25px'
			},
			name: 'f_resourceuse'
		});

		var f_existproblem = Ext.create('system.widget.FsdUeditor',{
			labelAlign:'right',
			labelWidth:1,
			width: 800,
			height : 400,
			style: {
				marginBottom: '25px'
			},
			name: 'f_existproblem'
		});

		var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			autoScroll : true,
			padding : '20 0 15 15',
			items : [
				me.f_id,
				me.f_projectId,
				f_projectprogress,
				f_mainproblem,
				f_resourceuse,
				f_existproblem
			]
		});

		// 创建表格
		me.nextGrid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_projectmiddlenext',
			model : 'model_z_projectmiddlenext',
			baseUrl : 'f_middlechecknext/load_pagedata.do',
			zAutoLoad: false,
			border : false,
			tbar : [{
				xtype : "fsdbutton",
				text : "添加",
				iconCls : 'add',
				popedomCode : 'projectbj',
				handler : function() {
					me.nextOper.add(me.nextGrid, me.f_id, me.f_projectId);
				}
			}, ,'-', {
				xtype : "fsdbutton",
				text : "编辑",
				iconCls : 'page_editIcon',
				popedomCode : 'projectbj',
				handler : function() {
					me.nextOper.editor(me.nextGrid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.nextOper.view(me.nextGrid);
				}
			}, {
				xtype : "fsdbutton",
				text : "删除",
				iconCls : 'page_deleteIcon',
				popedomCode : 'projectsc',
				handler : function() {
					me.nextOper.del(me.nextGrid);
				}
			},  '->', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					me.nextGrid.getStore().reload();
				}
			}]
		});
		me.nextGrid.getStore().on('beforeload', function(s) {
			var fid =  me.f_id.getValue();
			var pram = {fid : fid};
			var params = s.getProxy().extraParams;
			Ext.apply(params, {jsonData : Ext.encode(pram)});
		});

		var nextPanel = Ext.create('Ext.panel.Panel',{
			title: '项目下一阶段进度安排',
			layout : 'fit',
			items : [me.nextGrid]
		});


		var tabpanel = Ext.create('Ext.tab.Panel', {
			border: 0,
			enableTabScroll: true,
			deferredRender: false,
			defaults: { autoScroll: true },
			layout : 'fit',
			items: [{
				border: 0,
				title: '项目中期检查信息',
				items: [form]
			}, nextPanel]
		});


		Ext.apply(this,{
			layout : 'fit',
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			items :[tabpanel],
			buttons : [{
				name : 'btnsave',
				text : '保存',
				iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
				handler : function() {
					me.oper.formSubmit(form, me.grid);
				}
			}, {
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	},

	setProjectid:function (projectId) {
		this.f_projectId.setValue(projectId);
	}
});