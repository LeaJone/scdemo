Ext.define('system.zgyxy.project.biz.apply.ProjectApplyOper', {

	util : Ext.create('system.util.util'),

    /**
     * 项目成员添加
     */
    memberAdd : function(grid, projectid){
        var win = Ext.create('system.zgyxy.project.biz.apply.ProjectMemberAdd');
        win.setTitle('项目成员添加');
        win.modal = true;
        win.grid = grid;
        win.setProjectId(projectid);
        win.show();
    },

    /**
     * 删除
     */
    memberDel : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要删除的成员！');
        }else{
            Ext.MessageBox.show({
                title : '提示',
                msg : '您确定要删除所选成员吗?',
                width : 250,
                buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                        var dir = new Array();
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
                        });
                        var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('z_projectmember/del_z_projectmember_qx_z_projectmembersc.do' , param ,
                            function(){
                                Ext.MessageBox.alert('提示', '删除成功！');
                                grid.getStore().reload();
                            }, null, me.form);
                    }
                }
            });
        }
    },

    /**
     * 表单提交
     * @param formpanel
     * @param grid
     */
    memberFormSubmit : function(formpanel, grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_projectmember/save_z_projectmember_qx_z_projectmemberbj.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    grid.getStore().reload();
                    Ext.MessageBox.alert('提示', '保存成功！');
                    formpanel.up('window').close();
                }, null);
        }
    },

    /**
     * 评分表单提交
     * @param formpanel
     * @param grid
     */
    scoreFormSubmit : function(formpanel, fun){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_projectscore/save_z_projectscore_qx_z_projectscorebj.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    fun(form, action, respText);
                }, null);
        }
    },

    /**
     * 下载项目申报书
     * @param projectid
     */
    downloadProjectApplyBook : function (projectid) {
        var me = this;
        me.util.ExtAjaxRequest('z_project/download_projectApplyBook.do?projectid=' + projectid , null ,
            function(response, options, respText){
                var f_pdf = respText.data;
                location.href = "Common/download.do?path=" + f_pdf;
            }, null, me.form);
    },

    /**
     * 下载项目中期检查表
     * @param projectid
     */
    downloadProjectMiddleBook : function (projectid) {
        var me = this;
        me.util.ExtAjaxRequest('z_project/download_projectMiddleBook.do?projectid=' + projectid , null ,
            function(response, options, respText){
                var f_pdf = respText.data;
                location.href = "Common/download.do?path=" + f_pdf;
            }, null, me.form);
    },

    /**
     * 下载项目结项表
     * @param projectid
     */
    downloadProjectEndingBook : function (projectid) {
        var me = this;
        me.util.ExtAjaxRequest('z_project/download_projectEndingBook.do?projectid=' + projectid , null ,
            function(response, options, respText){
                var f_pdf = respText.data;
                location.href = "Common/download.do?path=" + f_pdf;
            }, null, me.form);
    },

    /**
     * 下载项目变更申请表
     * @param projectid
     */
    downloadProjectModifyBook : function (id, projectid) {
        var me = this;
        me.util.ExtAjaxRequest('z_project/download_projectModifyBook.do?id=' + id +"&projectid=" + projectid , null ,
            function(response, options, respText){
                var f_pdf = respText.data;
                location.href = "Common/download.do?path=" + f_pdf;
            }, null, me.form);
    }
});