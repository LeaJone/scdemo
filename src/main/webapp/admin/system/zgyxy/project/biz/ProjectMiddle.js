﻿/**
 * 项目中期检查结果
 * @author lumingbao
 */
Ext.define(	'system.zgyxy.project.biz.ProjectMiddle', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.zgyxy.project.biz.ProjectOper'),
	initComponent:function(){
		var me = this;

		me.projectids = Ext.create('Ext.form.field.Hidden',{
			name: 'projectids',
		});

		var typeid = Ext.create('system.widget.FsdComboBoxZD',{
			fieldLabel:'检查结果',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name : 'typeid',//提交到后台的参数名
			key : 'zqjcjg',//参数
			allowBlank: false,
			hiddenName : 'typename'//提交隐藏域Name
		});
		var typename = Ext.create('Ext.form.field.Hidden',{
			fieldLabel: '项目级别名称',
			name: 'typename'
		});
		var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 20 15 5',
			items : [
				me.projectids,
				typeid,
				typename
			]
		});

		Ext.apply(this,{
			layout : 'fit',
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			items :[form],
			buttons : [{
				name : 'btnsave',
				text : '提交',
				iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
				handler : function() {
					me.oper.projectMiddleSubmit(form, me.grid);
				}
			}, {
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	},

	/**
	 * 设置项目编码
	 * @param tutorid
	 */
	setProjectids : function (ids) {
		this.projectids.setValue(ids);
	}
});