/**
 * 项目管理操作类
 *
 * @author lumingbao
 */
Ext.define('system.zgyxy.project.biz.ProjectOper', {

	form : null,
	util : Ext.create('system.util.util'),

    /**
     * 项目申报
     */
    add : function(grid, type){
        var me = this;
        var model = Ext.create("system.zgyxy.project.biz.ProjectEdit");
        model.grid = grid;
        model.f_typeid.zDefaultCode = type;
        var tabID = 'projectbj';
        me.util.addTab(tabID, "项目申报", model, "fa-list-ul");
    },

    /**
     * 项目申报
     * @param grid
     * @param type
     */
    apply : function(grid, type){
        var win = Ext.create('system.zgyxy.project.biz.apply.ProjectApply');
        win.setTypeId(type);
        if("cxxlxm" == type){
            win.title = '创新训练项目申报 - 基本信息';
            win.setTypeName("创新训练项目");
        }else if("cyxlxm" == type){
            win.title = '创业训练项目申报 - 基本信息';
            win.setTypeName("创业训练项目");
        }else if("cysjxm" == type){
            win.title = '创业实践项目申报 - 基本信息';
            win.setTypeName("创业实践项目");
        }else{
            win.title = '项目申报 - 基本信息';
        }
        win.modal = true;
        win.grid = grid;
        win.show();
    },

    /**
     * 表单提交
     */
    formSubmit : function(formpanel, grid, type, panel, budget){
        var me = this;
        if (formpanel.form.isValid()) {
            var pram = {status : type};
            var param = {jsonData : Ext.encode(pram)};
            me.util.ExtFormSubmit('z_project/save_project_qx_projectbj.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    grid.getStore().reload();
                    Ext.MessageBox.alert('提示', '保存成功！');
                    panel.fid = respText.data.id;
                    budget.setDisabled(false);
                }, null, param);
        }
    },

    /**
     * 表单提交
     */
    uploadformSubmit : function(formpanel, grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_project/save_project_qx_projectbj.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    grid.getStore().reload();
                    Ext.MessageBox.alert('提示', '上传成功！');
                });
        }
    },

    /**
     * 修改
     */
    editor : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要编辑的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能编辑一个项目！');
        }else{
            // 先得到主键
            var status = data[0].data.f_statusname;
            if(status == "草稿"){
                var win = Ext.create('system.zgyxy.project.biz.apply.ProjectApply');
                var id = data[0].data.id;
                var pram = {id : id};
                var param = {jsonData : Ext.encode(pram)};
                me.util.formLoad(win.infoForm.getForm() , 'z_project/load_z_projectbyid.do' , param , null ,
                    function(form, action, respText, result){
                        var type = respText.data.f_typeid;
                        if("cxxlxm" == type){
                            win.title = '创新训练项目申报 - 基本信息';
                        }else if("cyxlxm" == type){
                            win.title = '创业训练项目申报 - 基本信息';
                        }else if("cysjxm" == type){
                            win.title = '创业实践项目申报 - 基本信息';
                        }else{
                            win.title = '项目申报 - 基本信息';
                        }
                        win.modal = true;
                        win.grid = grid;
                        win.show();
                    });
            }else{
                var id = data[0].data.id;
                me.util.ExtAjaxRequest('z_project/get_projectAuditStep.do?projectid=' + id , null ,
                    function(response, options, respText){
                        if(respText != null && respText.data != null && respText.data != "" && respText.data == "修改项目"){
                            var win = Ext.create('system.zgyxy.project.biz.apply.ProjectApplyUpdate');
                            var id = data[0].data.id;
                            var pram = {id : id};
                            var param = {jsonData : Ext.encode(pram)};
                            me.util.formLoad(win.infoForm.getForm() , 'z_project/load_z_projectbyid.do' , param , null ,
                                function(form, action, respText, result){
                                    var type = respText.data.f_typeid;
                                    if("cxxlxm" == type){
                                        win.title = '创新训练项目申报 - 基本信息';
                                    }else if("cyxlxm" == type){
                                        win.title = '创业训练项目申报 - 基本信息';
                                    }else if("cysjxm" == type){
                                        win.title = '创业实践项目申报 - 基本信息';
                                    }else{
                                        win.title = '项目申报 - 基本信息';
                                    }
                                    win.modal = true;
                                    win.grid = grid;
                                    win.show();
                                });
                        }else{
                            Ext.MessageBox.alert('提示', '已提交的项目不支持编辑操作！');
                        }
                    });
            }
        }
    },

    /**
     * 删除
     */
    del : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要删除的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能删除一个项目！');
        }else{
            // 先得到主键
            var status = data[0].data.f_statusname;
            if(status == "草稿"){
                Ext.MessageBox.show({
                    title : '提示',
                    msg : '您确定要删除所选项目吗?',
                    width : 250,
                    buttons : Ext.MessageBox.YESNO,
                    icon : Ext.MessageBox.QUESTION,
                    fn : function(btn) {
                        if (btn == 'yes') {
                            var dir = new Array();
                            Ext.Array.each(data, function(items) {
                                var id = items.data.id;
                                dir.push(id);
                            });
                            var pram = {ids : dir};
                            var param = {jsonData : Ext.encode(pram)};
                            me.util.ExtAjaxRequest('z_project/del_project_qx_projectsc.do' , param ,
                                function(){
                                    Ext.MessageBox.alert('提示', '删除成功！');
                                    grid.getStore().reload();
                                }, null, me.form);
                        }
                    }
                });
            }else{
                Ext.MessageBox.alert('提示', '已提交的项目不能删除！');
            }
        }
    },

    /**
     * 项目立项
     */
    projectApproval : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要立项的项目！');
        }else{
            // 先得到主键
            var dir = new Array();
            Ext.Array.each(data, function(items) {
                var id = items.data.id;
                dir.push(id);
            });
            var win = Ext.create('system.zgyxy.project.biz.ProjectApproval');
            win.setTitle("项目立项");
            win.modal = true;
            win.grid = grid;
            win.setProjectids(dir);
            win.show();
        }
    },

    /**
     * 中期检查结果
     */
    projectMiddle : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中项目！');
        }else{
            // 先得到主键
            var dir = new Array();
            Ext.Array.each(data, function(items) {
                var id = items.data.id;
                dir.push(id);
            });
            var win = Ext.create('system.zgyxy.project.biz.ProjectMiddle');
            win.setTitle("项目中期检查结果");
            win.modal = true;
            win.grid = grid;
            win.setProjectids(dir);
            win.show();
        }
    },

    /**
     * 项目结题结果
     */
    projectEnding : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中项目！');
        }else{
            // 先得到主键
            var dir = new Array();
            Ext.Array.each(data, function(items) {
                var id = items.data.id;
                dir.push(id);
            });
            var win = Ext.create('system.zgyxy.project.biz.ProjectEnding');
            win.setTitle("项目结题结果");
            win.modal = true;
            win.grid = grid;
            win.setProjectids(dir);
            win.show();
        }
    },

    /**
     * 查看
     */
    view : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要查看的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能查看一个项目！');
        }else{
            var win = Ext.create('system.zgyxy.project.biz.apply.ProjectApply');
            var id = data[0].data.id;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(win.infoForm.getForm() , 'z_project/load_z_projectbyid.do' , param , null ,
                function(form, action, respText, result){
                    var type = respText.data.f_typeid;
                    if("cxxlxm" == type){
                        win.title = '创新训练项目申报 - 基本信息';
                    }else if("cyxlxm" == type){
                        win.title = '创业训练项目申报 - 基本信息';
                    }else if("cysjxm" == type){
                        win.title = '创业实践项目申报 - 基本信息';
                    }else{
                        win.title = '项目申报 - 基本信息';
                    }
                    win.modal = true;
                    win.grid = grid;
                    win.show();
                });
        }
    },

    

    /**
     * 文件下载
     */
    download : function(f_pdf){
        var me = this;
        if(f_pdf != "" && f_pdf != null){
            Ext.MessageBox.show({
                title : '询问',
                msg : '你确定要下载申报书吗？',
                width : 250,
                buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                        window.open('Common/download.do?path=' + f_pdf);
                    }
                }
            });
        }
    },

    /**
     * 根据ID进行查看操作
     */
    viewById : function(id){
        var me = this;
        var pram = {id : id};
        var param = {jsonData : Ext.encode(pram)};
        var win = Ext.create('system.zgyxy.project.biz.apply.ProjectApply');
        me.util.formLoad(win.infoForm.getForm() , 'z_project/load_z_projectbyid.do' , param , null ,
            function(form, action, respText, result){
                var type = respText.data.f_typeid;
                if("cxxlxm" == type){
                    win.title = '创新训练项目申报 - 基本信息';
                }else if("cyxlxm" == type){
                    win.title = '创业训练项目申报 - 基本信息';
                }else if("cysjxm" == type){
                    win.title = '创业实践项目申报 - 基本信息';
                }else{
                    win.title = '项目申报 - 基本信息';
                }
                win.modal = true;
                win.show();
            });
    },

    /**
     * 提交项目基本信息
     */
    infoFormSubmit : function(formpanel, fun){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_project/save_project_info.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    fun(form, action, respText);
                });
        }
    },

    /**
     * 提交项目负责人信息
     */
    leaderFormSubmit : function(formpanel, fun){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_project/save_project_leader.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    fun(form, action, respText);
                });
        }
    },

    /**
     * 校内第一指导老师
     */
    teacherFormSubmit : function(formpanel, fun){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_project/save_project_teacher.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    fun(form, action, respText);
                });
        }
    },

    /**
     * 校内第二指导老师
     */
    teacherFormSubmit2 : function(formpanel, fun){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_project/save_project_teacher2.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    fun(form, action, respText);
                });
        }
    },

    /**
     * 校外第一指导老师
     */
    outteacherFormSubmit : function(formpanel, fun){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_project/save_project_outteacher.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    fun(form, action, respText);
                });
        }
    },

    /**
     * 校外第二指导老师
     */
    outteacherFormSubmit2 : function(formpanel, fun){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_project/save_project_outteacher2.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    fun(form, action, respText);
                });
        }
    },

    /**
     * 项目简介
     */
    abstractFormSubmit : function(formpanel, fun){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_project/save_project_abstract.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    fun(form, action, respText);
                });
        }
    },

    /**
     * 申请理由
     */
    gistFormSubmit : function(formpanel, fun){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_project/save_project_gist.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    fun(form, action, respText);
                });
        }
    },

    /**
     * 项目方案
     */
    schemeFormSubmit : function(formpanel, fun){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_project/save_project_scheme.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    fun(form, action, respText);
                });
        }
    },

    /**
     * 特色与创新点
     */
    featureFormSubmit : function(formpanel, fun){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_project/save_project_feature.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    fun(form, action, respText);
                });
        }
    },

    /**
     * 项目进度安排
     */
    scheduleFormSubmit : function(formpanel, fun){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_project/save_project_schedule.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    fun(form, action, respText);
                });
        }
    },

    /**
     * 提交项目
     */
    projectSubmit : function(projectid, type, fun){
        var me = this;
        var pram = {projectid : projectid, type : type};
        var param = {jsonData : Ext.encode(pram)};
        me.util.ExtAjaxRequest('z_project/save_project_submit.do' , param ,
            function(response, options, respText){
                fun(response, options, respText);
            }, null, me.form);
    },

    /**
     * 项目立项提交
     */
    projectApprovalSubmit : function(formpanel, grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_project/save_project_approval.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    formpanel.up('window').close();
                    Ext.MessageBox.alert('提示', '保存成功！');
                    grid.getStore().reload();
                });
        }
    },

    /**
     * 中期检查结果提交
     */
    projectMiddleSubmit : function(formpanel, grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_project/save_project_middle.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    formpanel.up('window').close();
                    Ext.MessageBox.alert('提示', '保存成功！');
                    grid.getStore().reload();
                });
        }
    },

    /**
     * 项目结项结果提交
     */
    projectEndingSubmit : function(formpanel, grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_project/save_project_ending.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    formpanel.up('window').close();
                    Ext.MessageBox.alert('提示', '保存成功！');
                    grid.getStore().reload();
                });
        }
    },

    /**
     * 路演录分
     */
    luyanlufen : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要录分的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能给一个项目录分！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.zgyxy.project.biz.luyan.ProjectLuyanEdit');
            win.setTitle('录入分数');
            win.modal = true;
            win.grid = grid;
            win.setProjectid(id);
            win.show();
        }
    },

    /**
     * 路演录分导入
     */
    luyanlufenImport : function(grid){
        var win = Ext.create('system.zgyxy.project.biz.luyan.ProjectLuyanImport');
        win.setTitle('路演分数导入');
        win.grid = grid;
        win.modal = true;
        win.show();
    },

    /**
     * 路演表单提交
     * @param formpanel
     * @param grid
     */
    luyanFormSubmit : function(formpanel, grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_project/update_luyanscore.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    grid.getStore().reload();
                    Ext.MessageBox.alert('提示', '保存成功！');
                    formpanel.up('window').close();
                }, null);
        }
    },

    /**
     * 路演导入表单提交
     * @param formpanel
     * @param grid
     */
    luyanImportFormSubmit : function(formpanel, grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_project/import_luyanscore.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    grid.getStore().reload();
                    Ext.MessageBox.alert('提示', '保存成功！');
                    formpanel.up('window').close();
                }, null);
        }
    },

    /**
     * 提交申报网站资料
     * @param grid
     */
    submitSiteInfo : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要提价资料的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{
            var id = data[0].data.id;
            var win = Ext.create('system.zgyxy.project.biz.site.ProjectApplySite');
            win.title = '提交省级项目申报网站资料 - 项目申报书';
            win.modal = true;
            win.grid = grid;
            win.setFid(id)
            me.util.formLoad(win.infoForm.getForm() , 'z_projectsite/load_z_projectsitebyfid.do?fid=' + id , null , null ,
                function(){
                });
            win.show();
        }
    },

    viewSite : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要查看申报网站的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{
            var id = data[0].data.id;
            var projectCode = data[0].data.f_code;
            me.util.ExtAjaxRequest('z_projectsite/load_z_projectsiteview.do?fid=' + id , null ,
                function(response, options, respText){
                    if(respText.data != null){
                        window.open("http://cxcy.lzit.edu.cn/apply-"+ projectCode +".htm");
                    }else{
                        Ext.MessageBox.alert('提示', '该项目还没有提交申报网站资料！');
                    }
                }, null, me.form);
        }

    }

});