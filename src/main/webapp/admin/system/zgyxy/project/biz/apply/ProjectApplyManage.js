﻿/**
 * 项目申报管理界面
 * @author lumingbao
 */
Ext.define('system.zgyxy.project.biz.apply.ProjectApplyManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.zgyxy.project.model.apply.Extjs_Column_ProjectApply',
	             'system.zgyxy.project.model.apply.Extjs_Model_ProjectApply'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.zgyxy.project.biz.ProjectOper'),
    
	createTable : function() {
	    var me = this;

        /**
         * 查询文本框
         */
        var queryText =  Ext.create("Ext.form.TextField" , {
            name : 'queryParam',
            emptyText : '请输入项目名称',
            enableKeyEvents : true,
            width : 130,
            listeners : {
                specialkey : function(field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        grid.getStore().reload();
                    }
                }
            }
        });
	    
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_projectapply',
			model : 'model_z_projectapply',
			baseUrl : 'z_project/load_applypagedata.do',
			border : false,
			tbar : [{
                xtype : "fsdbutton",
                text : "创新训练项目",
                iconCls : 'add',
                handler : function() {
                    me.oper.apply(grid, "cxxlxm");
                }
            }, {
                xtype : "fsdbutton",
                text : "创业训练项目",
                iconCls : 'add',
                handler : function() {
                    me.oper.apply(grid, "cyxlxm");
                }
            },{
                xtype : "fsdbutton",
                text : "创业实践项目",
                iconCls : 'add',
                handler : function() {
                    me.oper.apply(grid, "cysjxm");
                }
            },'-', {
                xtype : "fsdbutton",
                text : "修改项目",
                iconCls : 'page_editIcon',
                handler : function() {
                    me.oper.editor(grid);
                }
            }, {
                xtype : "fsdbutton",
                text : "删除项目",
                iconCls : 'page_deleteIcon',
                handler : function() {
                    me.oper.del(grid);
                }
            }, {
                xtype : "fsdbutton",
                text : "提交省级项目申报网站资料",
                iconCls : 'add',
                handler : function() {
                    me.oper.submitSiteInfo(grid);
                }
            }, {
                xtype : "fsdbutton",
                text : "浏览申报网站",
                iconCls : 'add',
                handler : function() {
                    me.oper.viewSite(grid);
                }
            },  '->', queryText, {
                text: '查询',
                iconCls: 'magnifierIcon',
                handler: function () {
                    grid.getStore().reload();
                }
            }, {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]
		});
		grid.getStore().on('beforeload', function(s) {
            var text =  queryText.getValue();
            var pram = {name : text};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		return grid;
	}
});