Ext.define('system.zgyxy.project.biz.group.ProjectGroupOper', {

	util : Ext.create('system.util.util'),

    /**
     * 分组添加
     */
    add : function(grid){
        var win = Ext.create('system.zgyxy.project.biz.group.ProjectGroupEdit');
        win.setTitle('添加项目分组');
        win.modal = true;
        win.grid = grid;
        win.show();
    },

    /**
     * 修改
     */
    editor : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目组！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能修改一个项目组！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.zgyxy.project.biz.group.ProjectGroupEdit');
            win.setTitle('项目分组修改');
            win.modal = true;
            win.grid = grid;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(win.down('form').getForm() , 'z_projectgroup/load_z_projectgroupbyid.do' , param , null ,
                function(){
                    win.show();
                });
        }
    },

    /**
     * 查看
     */
    view : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要查看的项目资料！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能查看一个项目资料！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.zgyxy.project.biz.group.ProjectGroupEdit');
            win.setTitle('项目分组查看');
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(win.down('form').getForm() , 'z_projectgroup/load_z_projectgroupbyid.do' , param , null ,
                function(){
                    win.show();
                });
        }
    },

    /**
     * 删除资料
     */
    del : function(grid){
        var me = this;
        // 获取选中的行s
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要删除的项目分组！');
        }else{
            Ext.MessageBox.show({
                title : '提示',
                msg : '您确定要删除所选的项目分组吗?',
                width : 250,
                buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                        var dir = new Array();
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
                        });
                        var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('z_projectgroup/del_z_projectgroup_qx_z_projectgroupsc.do' , param ,
                            function(){
                                Ext.MessageBox.alert('提示', '删除成功！');
                                grid.getStore().reload();
                            }, null, me.form);
                    }
                }
            });
        }
    },

    /**
     * 表单提交
     * @param formpanel
     * @param grid
     */
    groupFormSubmit : function(formpanel, grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_projectgroup/save_z_projectgroup_qx_z_projectgroupbj.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    grid.getStore().reload();
                    Ext.MessageBox.alert('提示', '保存成功！');
                    formpanel.up('window').close();
                }, null);
        }
    },

    /**
     * 项目分组
     * @param grid
     */
    projectGroup : function(grid){
        var me = this;
        // 获取选中的行s
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要进行分组的项目！');
        }else{
            var dir = new Array();
            Ext.Array.each(data, function(items) {
                var id = items.data.id;
                dir.push(id);
            });
            var win = Ext.create('system.zgyxy.project.biz.group.ProjectGroupChoose');
            win.setTitle('选择分组查看');
            win.projectids = dir;
            win.grid = grid;
            win.modal = true;
            win.show();
        }
    },

    /**
     * 提价项目分组
     * @param grid
     * @param groupgrid
     * @param projectids
     */
    projectGroupSubmit : function(grid, groupgrid, projectids){
        var me = this;
        var data = groupgrid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选择项目组！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '只能选择一个项目组！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            var pram = {groupid: id, projectids: projectids};
            var param = {jsonData : Ext.encode(pram)};
            me.util.ExtAjaxRequest('z_projectgroup/save_z_projectgroup.do' , param ,
                function(){
                    Ext.MessageBox.alert('提示', '分组成功！');
                    grid.getStore().reload();
                    groupgrid.up('window').close();
                }, null, null);
        }
    },

    /**
     * 项目分组导出
     * @param groupid
     */
    projectGroupReport: function (groupid) {
        if(groupid == null || groupid == "" || groupid == "wfz"){
            Ext.MessageBox.alert('提示', '请先从左侧选择项目分组！');
        }else{
            var util = Ext.create('system.util.util');
            var model = Ext.create("system.zgyxy.project.biz.group.ProjectGroupReport");
            model.setHtml("<iframe src='report_xmfz.htm?groupid="+ groupid +"' frameborder='0' width='100%' height='100%'></iframe>");
            util.addTab("xmfzreport" , "项目分组报表" , model , 'fa-legal');
        }
    }
});