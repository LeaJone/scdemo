﻿/**
 * 项目资料添加界面
 * @author lumingbao
 */
Ext.define(	'system.zgyxy.project.biz.material.ProjectMaterialEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.zgyxy.project.biz.material.ProjectMaterialOper'),
	initComponent:function(){
		var me = this;

		me.f_id = Ext.create('Ext.form.field.Hidden',{
			name: 'id'
		});

		me.f_projectid = Ext.create('Ext.form.field.Hidden',{
			name: 'f_projectid',
		});

		var f_url = Ext.create('system.widget.FsdTextFileManage', {
			width : 450,
			zName : 'f_url',
			zFieldLabel : '资料附件',
			zLabelWidth : 90,
			zIsShowButton : false,
			zIsGetButton : false,
			zIsUpButton : true,
			zIsReadOnly : true,
			zFileType: 'file',
			zAllowBlank: false,
			zFileUpPath : me.oper.util.getFilePath()
		});

		var f_describe = Ext.create('system.widget.FsdTextArea',{
			name : 'f_describe',
			fieldLabel : '资料描述',
			labelAlign :'right',
			labelWidth : 90,
			width : 450,
			allowBlank : false,
			maxLength : 500,
			blankText : '请输入资料描述',
			maxLengthText : '资料描述不能超过200个字符'
		});
		var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 20 15 5',
			items : [
				me.f_id,
				me.f_projectid,
				f_url,
				f_describe
			]
		});

		Ext.apply(this,{
			layout : 'fit',
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			items :[form],
			buttons : [{
				name : 'btnsave',
				text : '保存',
				iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
				handler : function() {
					me.oper.memberFormSubmit(form, me.grid);
				}
			}, {
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	},

	/**
	 * 设置父级ID
	 * @param tutorid
	 */
	setProjectId : function (projectid) {
		this.f_projectid.setValue(projectid);
	}
});