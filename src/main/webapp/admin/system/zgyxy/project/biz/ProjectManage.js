﻿/**
 * 项目管理界面
 * @author lumingbao
 */
Ext.define('system.zgyxy.project.biz.ProjectManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.zgyxy.project.model.Extjs_Column_Project',
	             'system.zgyxy.project.model.Extjs_Model_Project'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.zgyxy.project.biz.ProjectOper'),
    
	createTable : function() {
	    var me = this;

        /**
         * 查询文本框
         */
        var queryText =  Ext.create("Ext.form.TextField" , {
            name : 'queryParam',
            emptyText : '请输入项目名称',
            enableKeyEvents : true,
            width : 130,
            listeners : {
                specialkey : function(field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        grid.getStore().reload();
                    }
                }
            }
        });
	    
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_project',
			model : 'model_z_project',
			baseUrl : 'z_project/load_pagedata.do',
			border : false,
			tbar : [{
                xtype : "fsdbutton",
                text : "编辑",
                iconCls : 'page_editIcon',
                handler : function() {
                    me.oper.editor(grid);
                }
            }, {
                text : '查看',
                iconCls : 'magnifierIcon',
                handler : function() {
                    me.oper.view(grid);
                }
            }, {
                xtype : "fsdbutton",
                text : "删除",
                iconCls : 'page_deleteIcon',
                handler : function() {
                    me.oper.del(grid);
                }
            },'-',{
                xtype : "fsdbutton",
                text : "项目立项",
                iconCls : 'tickIcon',
                handler : function() {
                    me.oper.projectApproval(grid);
                }
            },{
                xtype : "fsdbutton",
                text : "提交中期检查结果",
                iconCls : 'tickIcon',
                handler : function() {
                    me.oper.projectMiddle(grid);
                }
            },{
                xtype : "fsdbutton",
                text : "提交项目结题结果",
                iconCls : 'tickIcon',
                handler : function() {
                    me.oper.projectEnding(grid);
                }
            },  '->', queryText, {
                text: '查询',
                iconCls: 'magnifierIcon',
                handler: function () {
                    grid.getStore().reload();
                }
            }, {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]
		});
		grid.getStore().on('beforeload', function(s) {
            var text =  queryText.getValue();
            var pram = {name : text};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		return grid;
	}
});