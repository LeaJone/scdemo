﻿/**
 * 项目报账加界面
 * @author lumingbao
 */
Ext.define(	'system.zgyxy.project.biz.money.ProjectMoneyEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.zgyxy.project.biz.money.ProjectMoneyOper'),
	initComponent:function(){
		var me = this;

		me.f_id = Ext.create('Ext.form.field.Hidden',{
			name: 'id'
		});

		me.f_projectid = Ext.create('Ext.form.field.Hidden',{
			name: 'f_projectid',
		});

		var f_level = Ext.create('system.widget.FsdComboBoxZD',{
			fieldLabel:'报销级别',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name : 'f_level',//提交到后台的参数名
			key : 'jsjb',//参数
			allowBlank: false
		});

		var f_name = Ext.create('system.widget.FsdTextField',{
			fieldLabel: '报账类别',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			emptyText:'如：业务费，办公费等',
			name: 'f_name',
			maxLength: 500,
			allowBlank: false
		});

		var f_money = Ext.create('system.widget.FsdTextField',{
			fieldLabel: '报账金额',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			emptyText:'单位：元',
			name: 'f_money',
			maxLength: 500,
			allowBlank: false
		});



		var f_detail = Ext.create('system.widget.FsdTextArea',{
			name : 'f_detail',
			fieldLabel : '费用明细',
			labelAlign :'right',
			labelWidth : 90,
			width : 450,
			allowBlank : false,
			maxLength : 500,
			emptyText:'请描述费用明细',
			blankText : '请输入费用明细',
			maxLengthText : '费用明细不能超过200个字符'
		});


		var f_url = Ext.create('system.widget.FsdTextFileManage', {
			width : 450,
			zName : 'f_url',
			zFieldLabel : '报账凭证',
			zLabelWidth : 90,
			zIsShowButton : false,
			zIsGetButton : false,
			zIsUpButton : true,
			zIsReadOnly : true,
			zAllowBlank: false,
			zFileUpPath : me.oper.util.getFilePath()
		});

		var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 20 15 5',
			items : [
				me.f_id,
				me.f_projectid,
				f_level,
				f_name,
				f_money,
				f_detail,
				f_url
			]
		});

		Ext.apply(this,{
			layout : 'fit',
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			items :[form],
			buttons : [{
				name : 'btnsave',
				text : '保存',
				iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
				handler : function() {
					me.oper.moneyFormSubmit(form, me.grid);
				}
			}, {
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	},

	/**
	 * 设置父级ID
	 * @param tutorid
	 */
	setProjectId : function (projectid) {
		this.f_projectid.setValue(projectid);
	}
});