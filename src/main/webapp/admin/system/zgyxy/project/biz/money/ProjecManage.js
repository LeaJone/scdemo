﻿/**
 * 项目申报管理界面
 * @author lumingbao
 */
Ext.define('system.zgyxy.project.biz.money.ProjecManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.zgyxy.project.model.money.Extjs_Column_ProjectMoney',
	             'system.zgyxy.project.model.money.Extjs_Model_ProjectMoney'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.zgyxy.project.biz.ProjectOper'),
    
	createTable : function() {
	    var me = this;

        /**
         * 查询文本框
         */
        var queryText =  Ext.create("Ext.form.TextField" , {
            name : 'queryParam',
            emptyText : '请输入项目名称',
            enableKeyEvents : true,
            width : 130,
            listeners : {
                specialkey : function(field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        grid.getStore().reload();
                    }
                }
            }
        });
	    
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_projectmoney',
			model : 'model_z_projectmoney',
			baseUrl : 'z_project/load_applypagedata.do',
			border : false,
			tbar : [{
                xtype : "fsdbutton",
                text : "项目报账",
                iconCls : 'page_editIcon',
                handler : function() {
					var me = this;
					// 获取选中的行
					var data = grid.getSelectionModel().getSelection();
					if (data.length == 0) {
						Ext.MessageBox.alert('提示', '请先选中要报账的项目！');
					}else if(data.length > 1){
						Ext.MessageBox.alert('提示', '每次只对一个项目进行报账！');
					}else{
						// 先得到主键
						var id = data[0].data.id;
						var moneyOper =  Ext.create('system.zgyxy.project.biz.money.ProjectMoneyOper');
						moneyOper.add(grid, id);
					}
                }
            },  '->', queryText, {
                text: '查询',
                iconCls: 'magnifierIcon',
                handler: function () {
                    grid.getStore().reload();
                }
            }, {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]
		});
		grid.getStore().on('beforeload', function(s) {
            var text =  queryText.getValue();
            var pram = {name : text};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		return grid;
	}
});