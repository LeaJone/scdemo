﻿/**
 * 荣誉添加界面
 * @author lumingbao
 */
Ext.define(	'system.zgyxy.project.biz.apply.ProjectMemberAdd', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.zgyxy.project.biz.apply.ProjectApplyOper'),
	initComponent:function(){
		var me = this;

		me.f_projectid = Ext.create('Ext.form.field.Hidden',{
			name: 'f_projectid',
		});

		var f_memberid = Ext.create('Ext.form.field.Hidden',{
			name: 'f_memberid',
		});
		var f_studentnumber = Ext.create('system.widget.FsdTextField',{
			name: 'f_studentnumber',
			fieldLabel: '学号',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			maxLength: 500,
			allowBlank: false,
			readOnly: true,
		});
		var f_membername = Ext.create('system.widget.FsdTextField',{
			name: 'f_membername',
			fieldLabel: '姓名',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			maxLength: 500,
			allowBlank: false,
			readOnly: true,
		});
		var f_memberclass = Ext.create('system.widget.FsdTextField',{
			name: 'f_memberclass',
			fieldLabel: '年级',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			maxLength: 500,
			allowBlank: false
		});
		var f_memberphone = Ext.create('system.widget.FsdTextField',{
			name: 'f_memberphone',
			fieldLabel: '电话',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			maxLength: 500,
			allowBlank: false
		});
		var f_membercollege = Ext.create('system.widget.FsdTextField',{
			name: 'f_membercollege',
			fieldLabel: '所在学院',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			maxLength: 500,
			allowBlank: false,
			readOnly: true,
		});
		var f_memberresponsibility = Ext.create('system.widget.FsdTextArea',{
			name : 'f_memberresponsibility',
			fieldLabel : '项目中的分工',
			labelAlign :'right',
			labelWidth : 90,
			width : 450,
			allowBlank : false,
			maxLength : 200,
			blankText : '请输入项目中的分工',
			maxLengthText : '课程简介不能超过200个字符'
		});
		var form = Ext.create('Ext.form.Panel',{
			tbar : [{
				xtype : "fsdbutton",
				text : "选择项目成员",
				iconCls : 'folder_userIcon',
				handler : function() {
					var win = Ext.create("system.zgyxy.user.biz.UserChoose");
					win.setTitle("选择项目成员")
					win.modal = true;
					win.setCallbackFun(function(obj, record){
						f_memberid.setValue(record.data.id);
						f_studentnumber.setValue(record.data.loginname);
						f_membername.setValue(record.data.realname);
						f_memberclass.setValue(record.data.registerclass);
						f_membercollege.setValue(record.data.branchname);
						f_memberphone.setValue(record.data.mobile);
						win.close();
					});
					win.show();
				}
			}],
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '0 20 15 5',
			items : [
				me.f_projectid,
				f_memberid,
				f_studentnumber,
				f_membername,
				f_memberclass,
				f_memberphone,
				f_membercollege,
				f_memberresponsibility
			]
		});

		Ext.apply(this,{
			layout : 'fit',
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			items :[form],
			buttons : [{
				name : 'btnsave',
				text : '保存',
				iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
				handler : function() {
					me.oper.memberFormSubmit(form, me.grid);
				}
			}, {
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	},

	/**
	 * 设置父级ID
	 * @param tutorid
	 */
	setProjectId : function (projectid) {
		this.f_projectid.setValue(projectid);
	}
});