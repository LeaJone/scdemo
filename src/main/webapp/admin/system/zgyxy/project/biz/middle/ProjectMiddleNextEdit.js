﻿/**
 * 项目中期检查下一阶段安排编辑界面
 * @author lumingbao
 */
Ext.define(	'system.zgyxy.project.biz.middle.ProjectMiddleNextEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.zgyxy.project.biz.middle.ProjectMiddleNextOper'),
	initComponent:function(){
		var me = this;

		me.f_fid = Ext.create('Ext.form.field.Hidden',{
			name:'f_fid'
		});

		me.f_projectid = Ext.create('Ext.form.field.Hidden',{
			name:'f_projectid'
		});

		var f_starttime = new Ext.create('system.widget.FsdDateField',{
			fieldLabel: '开始时间',
			labelAlign:'right',
			labelWidth:90,
			width : 360,
			name: 'f_starttime',
			maxLength: 25,
			allowBlank: false
		});

		var f_stoptime = new Ext.create('system.widget.FsdDateField',{
			fieldLabel: '截止时间',
			labelAlign:'right',
			labelWidth:90,
			width : 360,
			name: 'f_stoptime',
			maxLength: 25,
			allowBlank: false
		});

		var f_content = Ext.create('system.widget.FsdTextArea',{
			fieldLabel: '开展内容',
			labelAlign:'right',
			labelWidth:90,
			width : 360,
			height : 100,
			name: 'f_content'
		});

		var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '15 10 5 10',
			items : [{
				name: "id",
				xtype: "hidden"
			} , me.f_fid,
				me.f_projectid,
				f_starttime,
				f_stoptime,
				f_content]
		});

		Ext.apply(this,{
			layout : 'fit',
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			items :[form],
			buttons : [{
				name : 'btnsave',
				text : '保存',
				iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
				handler : function() {
					me.oper.formSubmit(form, me.grid);
				}
			}, {
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	},

	/**
	 * 设置父级ID
	 * @param tutorid
	 */
	setFid:function (fid) {
		this.f_fid.setValue(fid);
	},

	/**
	 * 设置项目ID
	 * @param projectid
	 */
	setProjectId : function (projectid) {
		this.f_projectid.setValue(projectid);
	}
});