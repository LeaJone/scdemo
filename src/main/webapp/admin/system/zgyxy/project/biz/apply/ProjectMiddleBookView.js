﻿/**
 * 项目中期检查表查看界面
 * @author lumingbao
 */
Ext.define(	'system.zgyxy.project.biz.apply.ProjectMiddleBookView', {
	extend : 'Ext.window.Window',
	oper : Ext.create('system.zgyxy.project.biz.apply.ProjectApplyOper'),
	projectid : '',
	initComponent:function(){
		var me = this;
		Ext.apply(this,{
			layout : 'fit',
			title: '查看项目中期检查表',
			width:950,
			height:600,
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			buttons : [{
				name : 'btnsave',
				text : '下载中期检查表',
				iconCls : 'downIcon',
				popedomCode : 'lmbj',
				handler : function() {
					me.oper.downloadProjectMiddleBook(me.projectid);
				}
			}, {
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	},

	/**
	 * 设置父级ID
	 * @param tutorid
	 */
	setProjectId : function (projectid) {
		this.projectid = projectid;
		this.html = "<iframe width='100%' height='100%' src='dcxm_zqjc-"+ projectid +".htm'></iframe>";
	}
});