﻿/**
 * 项目结项表编辑界面
 * @author lumingbao
 */
Ext.define(	'system.zgyxy.project.biz.ending.ProjectEndingEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	height:600,
	width:850,
	requires : [ 'system.zgyxy.project.model.middle.Extjs_Column_ProjectMiddleNext',
		'system.zgyxy.project.model.middle.Extjs_Model_ProjectMiddleNext'],
	oper : Ext.create('system.zgyxy.project.biz.ending.ProjectEndingOper'),
	initComponent:function(){
		var me = this;

		me.f_id = Ext.create('Ext.form.field.Hidden',{
			name: 'id'
		});

		me.f_projectId = Ext.create('Ext.form.field.Hidden',{
			name: 'f_projectid'
		});

		var f_executeid = Ext.create('system.widget.FsdComboBoxZD',{
			fieldLabel:'项目执行情况',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name : 'f_executeid',//提交到后台的参数名
			key : 'xmjxsfddmb',//参数
			allowBlank: false,
			hiddenName : 'f_executename'//提交隐藏域Name
		});
		var f_executename = Ext.create('Ext.form.field.Hidden',{
			fieldLabel: '项目执行情况名称',
			name: 'f_executename'
		});

		var f_background = Ext.create('system.widget.FsdUeditor',{
			labelAlign:'right',
			labelWidth:1,
			width: 800,
			height : 400,
			style: {
				marginBottom: '25px'
			},
			name: 'f_background'
		});

		var f_progressachievement = Ext.create('system.widget.FsdUeditor',{
			labelAlign:'right',
			labelWidth:1,
			width: 800,
			height : 400,
			style: {
				marginBottom: '25px'
			},
			name: 'f_progressachievement'
		});

		var f_achievement = Ext.create('system.widget.FsdUeditor',{
			labelAlign:'right',
			labelWidth:1,
			width: 800,
			height : 400,
			style: {
				marginBottom: '25px'
			},
			name: 'f_achievement'
		});

		var f_moneyuse = Ext.create('system.widget.FsdUeditor',{
			labelAlign:'right',
			labelWidth:1,
			width: 800,
			height : 400,
			style: {
				marginBottom: '25px'
			},
			name: 'f_moneyuse'
		});

		var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			autoScroll : true,
			padding : '20 0 15 15',
			items : [
				me.f_id,
				me.f_projectId,
				f_executeid,
				f_executename,
				f_background,
				f_progressachievement,
				f_achievement,
				f_moneyuse
			]
		});

		Ext.apply(this,{
			layout : 'fit',
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			items :[form],
			buttons : [{
				name : 'btnsave',
				text : '保存',
				iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
				handler : function() {
					me.oper.formSubmit(form, me.grid);
				}
			}, {
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	},

	setProjectid:function (projectId) {
		this.f_projectId.setValue(projectId);
	}
});