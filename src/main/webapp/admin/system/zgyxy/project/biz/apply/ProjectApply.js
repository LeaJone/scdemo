﻿/**
 * 项目申报
 * @author lumingbao
 */
Ext.define(	'system.zgyxy.project.biz.apply.ProjectApply', {
	extend : 'Ext.window.Window',
	grid : null,
	steps : '1',
	title :'',
	requires : [
		'system.zgyxy.domain.model.Extjs_Model_Z_domain',
		'system.zgyxy.domain.model.Extjs_Column_Z_domain',
		'system.zgyxy.budget.model.Extjs_Model_Budget',
		'system.zgyxy.budget.model.Extjs_Column_Budget',
		'system.zgyxy.project.model.apply.Extjs_Model_ProjectMember',
		'system.zgyxy.project.model.apply.Extjs_Column_ProjectMember'
	],
	oper : Ext.create('system.zgyxy.project.biz.ProjectOper'),
	applyOper : Ext.create('system.zgyxy.project.biz.apply.ProjectApplyOper'),
	budgetOper : Ext.create('system.zgyxy.budget.biz.BudgetOper'),
	initComponent:function(){
		var me = this;

		// 项目基本信息表单项开始
		me.infoprojectid = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
		});

		var f_name = Ext.create('system.widget.FsdTextField',{
			fieldLabel: '项目名称',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name: 'f_name',
			maxLength: 500,
			allowBlank: false
		});

		me.f_typeid = Ext.create('system.widget.FsdComboBoxZD',{
			fieldLabel:'项目类型',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name : 'f_typeid',//提交到后台的参数名
			key : 'projecttype',//参数
			allowBlank: false,
			hiddenName : 'f_typename'//提交隐藏域Name
		});
		me.f_typename = Ext.create('Ext.form.field.Hidden',{
			fieldLabel: '项目类型',
			name: 'f_typename'
		});
		me.f_typeid.on('select', function(combo, record, opts) {
			me.title = record[0].data.name + '申报 - 基本信息';
			me.setTitle(me.title);
		});

		var f_typename1 = Ext.create('system.widget.FsdTextGrid', {
			zFieldLabel : '一级学科',
			zLabelWidth : 90,
			width : 450,
			zName : 'f_typeid1',
			zColumn : 'column_z_domainxz', // 显示列
			zModel : 'model_z_domain',
			zBaseUrl : 'z_domain/load_AllDomainListByPopdom.do',
			zAllowBlank : false,
			zGridType : 'list',
			zGridWidth : 700,//弹出表格宽度
			zGridHeight : 600,//弹出表格高度
			zIsText1 : true,
			zTxtLabel1 : '领域名称',
			zFunChoose : function(data){
				f_typename1.setValue(data.code);
				f_typeid1.setValue(data.title);
			},
			zFunQuery : function(txt1){
				return {fid : 'FSDMAIN', txt : txt1};
			}
		});
		var f_typeid1 = Ext.create('Ext.form.field.Hidden',{
			fieldLabel: '学科领域编码',
			name: 'f_typename1'
		});

		me.infoForm = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '15 20 15 5',
			items : [
				me.infoprojectid,
				f_name,
				me.f_typeid,
				me.f_typename,
				f_typeid1,
				f_typename1
			]
		});
		// 项目基本信息表单项结束

		// 项目负责人信息表单项开始
		me.leaderprojectid = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
		});
		var leaderid = Ext.create('Ext.form.field.Hidden',{
			name: 'f_leaderid',
		});
		var f_studentnumber = Ext.create('system.widget.FsdTextField',{
			name: 'f_studentnumber',
			fieldLabel: '负责人学号',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			maxLength: 500,
			allowBlank: false,
			readOnly: true,
		});
		var f_leadername = Ext.create('system.widget.FsdTextField',{
			name: 'f_leadername',
			fieldLabel: '负责人姓名',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			maxLength: 500,
			allowBlank: false,
			readOnly: true,
		});
		var f_leaderclass = Ext.create('system.widget.FsdTextField',{
			name: 'f_leaderclass',
			fieldLabel: '负责人年级',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			maxLength: 500,
			allowBlank: false
		});
		var f_leadercollege = Ext.create('system.widget.FsdTextField',{
			name: 'f_leadercollege',
			fieldLabel: '所在学院',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			maxLength: 500,
			allowBlank: false,
			readOnly: true,
		});
		var f_leaderphone = Ext.create('system.widget.FsdTextField',{
			name: 'f_leaderphone',
			fieldLabel: '负责人电话',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			maxLength: 500,
			allowBlank: false
		});
		var f_leaderresponsibility = Ext.create('system.widget.FsdTextArea',{
			name : 'f_leaderresponsibility',
			fieldLabel : '项目中的分工',
			labelAlign :'right',
			labelWidth : 90,
			width : 450,
			allowBlank : false,
			maxLength : 200,
			blankText : '请输入项目中的分工',
			maxLengthText : '课程简介不能超过200个字符'
		});
		var leaderForm = Ext.create('Ext.form.Panel',{
			tbar : [{
				xtype : "fsdbutton",
				text : "选择项目负责人",
				iconCls : 'folder_userIcon',
				handler : function() {
					var win = Ext.create("system.zgyxy.user.biz.UserChoose");
					win.setTitle("选择项目负责人")
					win.modal = true;
					win.setCallbackFun(function(obj, record){
						leaderid.setValue(record.data.id);
						f_studentnumber.setValue(record.data.loginname);
						f_leadername.setValue(record.data.realname);
						f_leaderclass.setValue(record.data.registerclass);
						f_leadercollege.setValue(record.data.branchname);
						f_leaderphone.setValue(record.data.mobile);
						win.close();
					});
					win.show();
				}
			}],
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '0 20 15 5',
			items : [
				me.leaderprojectid,
				leaderid,
				f_studentnumber,
				f_leadername,
				f_leaderclass,
				f_leadercollege,
				f_leaderphone,
				f_leaderresponsibility
			]
		});
		// 项目负责人信息表单项结束

		//项目组成员开始
		// 创建表格
		var membergrid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_projectmember',
			model : 'model_z_projectmember',
			baseUrl : 'z_projectmember/load_pagedata.do',
			border : false,
			zAutoLoad : false,
			width: 800,
			height: 350,
			tbar : [{
				xtype : "fsdbutton",
				text : "添加成员",
				iconCls : 'add',
				handler : function() {
					me.applyOper.memberAdd(membergrid, me.infoprojectid.getValue());
				}
			}, ,'-', {
				xtype : "fsdbutton",
				text : "删除成员",
				iconCls : 'page_deleteIcon',
				handler : function() {
					me.applyOper.memberDel(membergrid);
				}
			},  '->', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					membergrid.getStore().reload();
				}
			}]
		});
		membergrid.getStore().on('beforeload', function(s) {
			var pram = {projectid : me.infoprojectid.getValue()};
			var params = s.getProxy().extraParams;
			Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		//项目组成员结束


		//校内指导老师开始
		me.teacherprojectid = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
		});
		var f_teacherid = Ext.create('Ext.form.field.Hidden',{
			name: 'f_teacherid',
		});
		var f_teachername = Ext.create('system.widget.FsdTextField',{
			name: 'f_teachername',
			fieldLabel: '第一指导老师姓名',
			labelAlign:'right',
			labelWidth:150,
			width : 450,
			maxLength: 500,
			allowBlank: false,
			readOnly: true
		});
		var f_teacherduty = Ext.create('system.widget.FsdTextField',{
			name: 'f_teacherduty',
			fieldLabel: '第一指导老师职务/职称',
			labelAlign:'right',
			labelWidth:150,
			width : 450,
			maxLength: 500,
			allowBlank: false
		});
		var f_teacherunit = Ext.create('system.widget.FsdTextField',{
			name: 'f_teacherunit',
			fieldLabel: '第一指导老师单位',
			labelAlign:'right',
			labelWidth:150,
			width : 450,
			maxLength: 500,
			allowBlank: false
		});
		var f_teacherphone = Ext.create('system.widget.FsdTextField',{
			name: 'f_teacherphone',
			fieldLabel: '第一指导老师电话',
			labelAlign:'right',
			labelWidth:150,
			width : 450,
			maxLength: 500,
			allowBlank: false
		});
		var f_teacheremail = Ext.create('system.widget.FsdTextField',{
			name: 'f_teacheremail',
			fieldLabel: '第一指导老师邮箱',
			labelAlign:'right',
			labelWidth:150,
			width : 450,
			maxLength: 500,
			allowBlank: false
		});

		var teacherForm = Ext.create('Ext.form.Panel',{
			tbar : [{
				xtype : "fsdbutton",
				text : "选择第一指导老师",
				iconCls : 'folder_userIcon',
				handler : function() {
					var win = Ext.create("system.zgyxy.user.biz.UserChoose");
					win.setTitle("选择第一指导老师");
					win.modal = true;
					win.setCallbackFun(function(obj, record){
						f_teacherid.setValue(record.data.id);
						f_teachername.setValue(record.data.realname);
						f_teacherduty.setValue();
						f_teacherunit.setValue("兰州工业学院");
						f_teacherphone.setValue(record.data.mobile);
						f_teacheremail.setValue(record.data.email);
						win.close();
					});
					win.show();
				}
			}],
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '0 20 15 5',
			items:[me.teacherprojectid, f_teacherid, f_teachername, f_teacherduty, f_teacherunit, f_teacherphone, f_teacheremail]
		});

		me.teacherprojectid2 = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
		});
		var f_teacherid2 = Ext.create('Ext.form.field.Hidden',{
			name: 'f_teacherid2',
		});
		var f_teachername2 = Ext.create('system.widget.FsdTextField',{
			name: 'f_teachername2',
			fieldLabel: '第二指导老师姓名',
			labelAlign:'right',
			labelWidth:150,
			width : 450,
			maxLength: 500,
			readOnly: true
		});
		var f_teacherduty2 = Ext.create('system.widget.FsdTextField',{
			name: 'f_teacherduty2',
			fieldLabel: '第二指导老师职务/职称',
			labelAlign:'right',
			labelWidth:150,
			width : 450,
			maxLength: 500
		});
		var f_teacherunit2 = Ext.create('system.widget.FsdTextField',{
			name: 'f_teacherunit2',
			fieldLabel: '第二指导老师单位',
			labelAlign:'right',
			labelWidth:150,
			width : 450,
			maxLength: 500
		});
		var f_teacherphone2 = Ext.create('system.widget.FsdTextField',{
			name: 'f_teacherphone2',
			fieldLabel: '第二指导老师电话',
			labelAlign:'right',
			labelWidth:150,
			width : 450,
			maxLength: 500
		});
		var f_teacheremail2 = Ext.create('system.widget.FsdTextField',{
			name: 'f_teacheremail2',
			fieldLabel: '第二指导老师邮箱',
			labelAlign:'right',
			labelWidth:150,
			width : 450,
			maxLength: 500
		});
		var teacherForm2 = Ext.create('Ext.form.Panel',{
			tbar : [{
				xtype : "fsdbutton",
				text : "选择第二指导老师",
				iconCls : 'folder_userIcon',
				handler : function() {
					var win = Ext.create("system.zgyxy.user.biz.UserChoose");
					win.setTitle("选择第二指导老师");
					win.modal = true;
					win.setCallbackFun(function(obj, record){
						f_teacherid2.setValue(record.data.id);
						f_teachername2.setValue(record.data.realname);
						f_teacherduty2.setValue();
						f_teacherunit2.setValue("兰州工业学院");
						f_teacherphone2.setValue(record.data.mobile);
						f_teacheremail2.setValue(record.data.email);
						win.close();
					});
					win.show();
				}
			}],
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '0 20 15 5',
			items:[me.teacherprojectid2, f_teacherid2, f_teachername2, f_teacherduty2, f_teacherunit2, f_teacherphone2, f_teacheremail2]
		});
		//校内指导老师结束

		//校外指导老师结束
		me.outteacherprojectid = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
		});
		var f_outteacherid = Ext.create('Ext.form.field.Hidden',{
			name: 'f_outteacherid',
		});
		var f_outteachername = Ext.create('system.widget.FsdTextField',{
			name: 'f_outteachername',
			fieldLabel: '第一校外指导老师姓名',
			labelAlign:'right',
			labelWidth:180,
			width : 450,
			maxLength: 500
		});
		var f_outteacherduty = Ext.create('system.widget.FsdTextField',{
			name: 'f_outteacherduty',
			fieldLabel: '第一校外指导老师职务/职称',
			labelAlign:'right',
			labelWidth:180,
			width : 450,
			maxLength: 500
		});
		var f_outteacherunit = Ext.create('system.widget.FsdTextField',{
			name: 'f_outteacherunit',
			fieldLabel: '第一校外指导老师单位',
			labelAlign:'right',
			labelWidth:180,
			width : 450,
			maxLength: 500
		});
		var f_outteacherphone = Ext.create('system.widget.FsdTextField',{
			name: 'f_outteacherphone',
			fieldLabel: '第一校外指导老师电话',
			labelAlign:'right',
			labelWidth:180,
			width : 450,
			maxLength: 500
		});
		var f_outteacheremail = Ext.create('system.widget.FsdTextField',{
			name: 'f_outteacheremail',
			fieldLabel: '第一校外指导老师邮箱',
			labelAlign:'right',
			labelWidth:180,
			width : 450,
			maxLength: 500
		});

		var outteacherForm = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '0 20 15 5',
			items:[me.outteacherprojectid, f_outteacherid, f_outteachername, f_outteacherduty, f_outteacherunit, f_outteacherphone, f_outteacheremail]
		});

		me.outteacherprojectid2 = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
		});
		var f_outteacherid2 = Ext.create('Ext.form.field.Hidden',{
			name: 'f_outteacherid2',
		});
		var f_outteachername2 = Ext.create('system.widget.FsdTextField',{
			name: 'f_outteachername2',
			fieldLabel: '第二校外指导老师姓名',
			labelAlign:'right',
			labelWidth:180,
			width : 450,
			maxLength: 500
		});
		var f_outteacherduty2 = Ext.create('system.widget.FsdTextField',{
			name: 'f_outteacherduty2',
			fieldLabel: '第二校外指导老师职务/职称',
			labelAlign:'right',
			labelWidth:180,
			width : 450,
			maxLength: 500
		});
		var f_outteacherunit2 = Ext.create('system.widget.FsdTextField',{
			name: 'f_outteacherunit2',
			fieldLabel: '第二校外指导老师单位',
			labelAlign:'right',
			labelWidth:180,
			width : 450,
			maxLength: 500
		});
		var f_outteacherphone2 = Ext.create('system.widget.FsdTextField',{
			name: 'f_outteacherphone2',
			fieldLabel: '第二校外指导老师电话',
			labelAlign:'right',
			labelWidth:180,
			width : 450,
			maxLength: 500
		});
		var f_outteacheremail2 = Ext.create('system.widget.FsdTextField',{
			name: 'f_outteacheremail2',
			fieldLabel: '第二校外指导老师邮箱',
			labelAlign:'right',
			labelWidth:180,
			width : 450,
			maxLength: 500
		});

		var outteacherForm2 = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '0 20 15 5',
			items:[me.outteacherprojectid2, f_outteacherid2, f_outteachername2, f_outteacherduty2, f_outteacherunit2, f_outteacherphone2, f_outteacheremail2]
		});
		//校外指导老师结束

		//项目简介开始
		me.abstractprojectid = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
		});
		var f_abstract = Ext.create('system.widget.FsdTextArea',{
			fieldLabel: '项目简介',
			labelAlign: 'right',
			labelWidth: 90,
			width: 600,
			height: 200,
			name: 'f_abstract',
			allowBlank: false,
			maxLength:500
		});
		var abstractForm = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '10 20 15 5',
			items:[me.abstractprojectid, f_abstract]
		});
		//项目简介结束

		//申请理由开始
		me.gistprojectid = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
		});
		var f_gist = Ext.create('system.widget.FsdUeditor',{
			fieldLabel: '申请理由',
			labelAlign:'right',
			labelWidth:90,
			width: 800,
			height : 400,
			name: 'f_gist'
		});
		var gistForm = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '10 20 15 5',
			items:[me.gistprojectid, f_gist]
		});
		//申请理由结束

		//项目方案开始
		me.schemeprojectid = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
		});
		var f_scheme = Ext.create('system.widget.FsdUeditor',{
			fieldLabel: '项目方案',
			labelAlign:'right',
			labelWidth:90,
			width: 800,
			height : 400,
			name: 'f_scheme'
		});
		var schemeForm = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '10 20 15 5',
			items:[me.schemeprojectid, f_scheme]
		});
		//项目方案结束

		//特色与创新点开始
		me.featureprojectid = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
		});
		var f_feature = Ext.create('system.widget.FsdUeditor',{
			fieldLabel: '特色与创新点',
			labelAlign:'right',
			labelWidth:90,
			width: 800,
			height : 400,
			name: 'f_feature'
		});
		var featureForm = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '10 20 15 5',
			items:[me.featureprojectid, f_feature]
		});
		//特色与创新点结束

		//项目进度安排开始
		me.scheduleprojectid = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
		});
		var f_schedule = Ext.create('system.widget.FsdUeditor',{
			fieldLabel: '项目进度安排',
			labelAlign:'right',
			labelWidth:90,
			width: 800,
			height : 400,
			name: 'f_schedule'
		});
		var scheduleForm = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '10 20 15 5',
			items:[me.scheduleprojectid, f_schedule]
		});
		//项目进度安排结束

		//经费预算开始
		var budgetgrid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_budget',
			model : 'model_z_budget',
			baseUrl : 'z_budget/load_pagedata.do',
			border : false,
			width: 800,
			height: 350,
			tbar : [{
				xtype : "fsdbutton",
				text : "添加",
				iconCls : 'add',
				handler : function() {
					me.budgetOper.add(budgetgrid, me.infoprojectid.getValue());
				}
			}, ,'-', {
				xtype : "fsdbutton",
				text : "编辑",
				iconCls : 'page_editIcon',
				handler : function() {
					me.budgetOper.editor(budgetgrid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.budgetOper.view(budgetgrid);
				}
			}, {
				xtype : "fsdbutton",
				text : "删除",
				iconCls : 'page_deleteIcon',
				handler : function() {
					me.budgetOper.del(budgetgrid);
				}
			},  '->', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					budgetgrid.getStore().reload();
				}
			}]
		});
		budgetgrid.getStore().on('beforeload', function(s) {
			var pram = {fid : me.infoprojectid.getValue()};
			var params = s.getProxy().extraParams;
			Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		//经费预算结束

		var previous = Ext.create('system.widget.FsdButton', {
			name : 'btnsave',
			text : '上一步',
			iconCls : 'previousIcon',
			handler : function() {
				if(me.steps == "2"){
					me.steps = "1";
					previous.setVisible(false);
					me.removeAll(false);
					me.add(me.infoForm);
					me.resetLocation(me);
					var titles = me.title.split("-");
					me.title = titles[0] + "- 基本信息";
					me.setTitle(me.title);
					var pram = {id : me.infoprojectid.getValue()};
					var param = {jsonData : Ext.encode(pram)};
					me.oper.util.formLoad(me.infoForm.getForm() , 'z_project/load_z_projectbyid.do', param, null,
						function(){

						});
				}else if(me.steps == "3"){
					me.steps = "2";
					me.removeAll(false);
					me.add(leaderForm);
					me.resetLocation(me);
					var titles = me.title.split("-");
					me.title = titles[0] + "- 项目负责人信息";
					me.setTitle(me.title);
					var pram = {id : me.leaderprojectid.getValue()};
					var param = {jsonData : Ext.encode(pram)};
					me.oper.util.formLoad(leaderForm.getForm() , 'z_project/load_z_projectbyid.do', param, null,
						function(){

						});
				}else if(me.steps == "4"){
					me.steps = "3";
					me.removeAll(false);
					me.add(membergrid);
					me.resetLocation(me);
					var titles = me.title.split("-");
					me.title = titles[0] + "- 项目成员信息";
					me.setTitle(me.title);
				}else if(me.steps == "5"){
					me.steps = "4";
					me.removeAll(false);
					me.add(teacherForm);
					me.resetLocation(me);
					var titles = me.title.split("-");
					me.title = titles[0] + "- 项目校内第一指导老师信息";
					me.setTitle(me.title);
					var pram = {id : me.teacherprojectid.getValue()};
					var param = {jsonData : Ext.encode(pram)};
					me.oper.util.formLoad(teacherForm.getForm() , 'z_project/load_z_projectbyid.do', param, null,
						function(){

						});
				}else if(me.steps == "6"){
					me.steps = "5";
					me.removeAll(false);
					me.add(teacherForm2);
					me.resetLocation(me);
					var titles = me.title.split("-");
					me.title = titles[0] + "- 项目校内第二指导老师信息";
					me.setTitle(me.title);
					var pram = {id : me.teacherprojectid2.getValue()};
					var param = {jsonData : Ext.encode(pram)};
					me.oper.util.formLoad(teacherForm2.getForm() , 'z_project/load_z_projectbyid.do', param, null,
						function(){

						});
				}else if(me.steps == "7"){
					me.steps = "6";
					me.removeAll(false);
					me.add(outteacherForm);
					me.resetLocation(me);
					var titles = me.title.split("-");
					me.title = titles[0] + "- 项目校外第一指导老师信息";
					me.setTitle(me.title);
					var pram = {id : me.outteacherprojectid.getValue()};
					var param = {jsonData : Ext.encode(pram)};
					me.oper.util.formLoad(outteacherForm.getForm() , 'z_project/load_z_projectbyid.do', param, null,
						function(){

						});
				}else if(me.steps == "8"){
					me.steps = "7";
					me.removeAll(false);
					me.add(outteacherForm2);
					me.resetLocation(me);
					var titles = me.title.split("-");
					me.title = titles[0] + "- 项目校外第二指导老师信息";
					me.setTitle(me.title);
					var pram = {id : me.outteacherprojectid.getValue()};
					var param = {jsonData : Ext.encode(pram)};
					me.oper.util.formLoad(outteacherForm2.getForm() , 'z_project/load_z_projectbyid.do', param, null,
						function(){

						});
				}else if(me.steps == "9"){
					me.steps = "8";
					me.removeAll(false);
					me.add(abstractForm);
					me.resetLocation(me);
					var titles = me.title.split("-");
					me.title = titles[0] + "- 项目简介";
					me.setTitle(me.title);
					var pram = {id : me.abstractprojectid.getValue()};
					var param = {jsonData : Ext.encode(pram)};
					me.oper.util.formLoad(abstractForm.getForm() , 'z_project/load_z_projectbyid.do', param, null,
						function(){

						});
				}else if(me.steps == "10"){
					me.steps = "9";
					me.removeAll(false);
					me.add(gistForm);
					me.resetLocation(me);
					var titles = me.title.split("-");
					me.title = titles[0] + "- 申请理由";
					me.setTitle(me.title);
					var pram = {id : me.gistprojectid.getValue()};
					var param = {jsonData : Ext.encode(pram)};
					me.oper.util.formLoad(gistForm.getForm() , 'z_project/load_z_projectbyid.do', param, null,
						function(){

						});
				}else if(me.steps == "11"){
					me.steps = "10";
					me.removeAll(false);
					me.add(schemeForm);
					me.resetLocation(me);
					var titles = me.title.split("-");
					me.title = titles[0] + "- 项目方案";
					me.setTitle(me.title);
					var pram = {id : me.schemeprojectid.getValue()};
					var param = {jsonData : Ext.encode(pram)};
					me.oper.util.formLoad(schemeForm.getForm() , 'z_project/load_z_projectbyid.do', param, null,
						function(){

						});
				}else if(me.steps == "12"){
					me.steps = "11";
					me.removeAll(false);
					me.add(featureForm);
					me.resetLocation(me);
					var titles = me.title.split("-");
					me.title = titles[0] + "- 特色与创新点";
					me.setTitle(me.title);
					var pram = {id : me.featureprojectid.getValue()};
					var param = {jsonData : Ext.encode(pram)};
					me.oper.util.formLoad(featureForm.getForm() , 'z_project/load_z_projectbyid.do', param, null,
						function(){

						});
				}else if(me.steps == "13"){
					me.steps = "12";
					me.removeAll(false);
					me.add(scheduleForm);
					me.resetLocation(me);
					var titles = me.title.split("-");
					me.title = titles[0] + "- 项目进度安排";
					me.setTitle(me.title);
					next.setText("下一步");
					draft.setVisible(false);
					var pram = {id : me.schemeprojectid.getValue()};
					var param = {jsonData : Ext.encode(pram)};
					me.oper.util.formLoad(scheduleForm.getForm() , 'z_project/load_z_projectbyid.do', param, null,
						function(){

						});
				}
			}
		});
		previous.setVisible(false);

		var next = Ext.create('system.widget.FsdButton', {
			name : 'btnsave',
			text : '下一步',
			iconCls : 'nextIcon',
			handler : function() {
				if(me.steps == "1"){
					me.oper.infoFormSubmit(me.infoForm,function(form, action, respText){
						me.infoprojectid.setValue(respText.data.id);
						me.leaderprojectid.setValue(respText.data.id);
						me.teacherprojectid.setValue(respText.data.id);
						me.teacherprojectid2.setValue(respText.data.id);
						me.outteacherprojectid.setValue(respText.data.id);
						me.outteacherprojectid2.setValue(respText.data.id);
						me.abstractprojectid.setValue(respText.data.id);
						me.gistprojectid.setValue(respText.data.id);
						me.schemeprojectid.setValue(respText.data.id);
						me.featureprojectid.setValue(respText.data.id);
						me.scheduleprojectid.setValue(respText.data.id);
						if(me.grid != null){
							me.grid.getStore().reload();
						}
						me.removeAll(false);

						var pram = {id : me.infoprojectid.getValue()};
						var param = {jsonData : Ext.encode(pram)};
						me.oper.util.formLoad(leaderForm.getForm() , 'z_project/load_z_projectbyid.do', param, null,
							function(){

							});
						me.add(leaderForm);
						me.resetLocation(me);
						me.steps = "2";
						var titles = me.title.split("-");
						me.title = titles[0] + "- 项目负责人信息";
						me.setTitle(me.title);
						previous.setVisible(true);
					});
				}else if(me.steps == "2"){
					me.oper.leaderFormSubmit(leaderForm,function(form, action, respText){
						if(me.grid != null){
							me.grid.getStore().reload();
						}
						me.removeAll(false);
						me.add(membergrid);
						membergrid.getStore().reload();
						me.resetLocation(me);
						me.steps = "3";
						var titles = me.title.split("-");
						me.title = titles[0] + "- 项目成员信息";
						me.setTitle(me.title);
						previous.setVisible(true);
					});
				}else if(me.steps == "3"){
					me.removeAll(false);
					var pram = {id : me.infoprojectid.getValue()};
					var param = {jsonData : Ext.encode(pram)};
					me.oper.util.formLoad(teacherForm.getForm() , 'z_project/load_z_projectbyid.do', param, null,
						function(){

						});
					me.add(teacherForm);
					me.resetLocation(me);
					me.steps = "4";
					var titles = me.title.split("-");
					me.title = titles[0] + "- 项目第一指导老师信息";
					me.setTitle(me.title);
					previous.setVisible(true);
				}else if(me.steps == "4"){
					me.oper.teacherFormSubmit(teacherForm,function(form, action, respText){
						me.removeAll(false);
						var pram = {id : me.infoprojectid.getValue()};
						var param = {jsonData : Ext.encode(pram)};
						me.oper.util.formLoad(teacherForm2.getForm() , 'z_project/load_z_projectbyid.do', param, null,
							function(){

							});
						me.add(teacherForm2);
						me.resetLocation(me);
						me.steps = "5";
						var titles = me.title.split("-");
						me.title = titles[0] + "- 项目第二指导老师信息";
						me.setTitle(me.title);
						previous.setVisible(true);
					});
				}else if(me.steps == "5"){
					me.oper.teacherFormSubmit2(teacherForm2,function(form, action, respText){
						me.removeAll(false);
						var pram = {id : me.infoprojectid.getValue()};
						var param = {jsonData : Ext.encode(pram)};
						me.oper.util.formLoad(outteacherForm.getForm() , 'z_project/load_z_projectbyid.do', param, null,
							function(){

							});
						me.add(outteacherForm);
						me.resetLocation(me);
						me.steps = "6";
						var titles = me.title.split("-");
						me.title = titles[0] + "- 项目第一校外指导老师信息";
						me.setTitle(me.title);
						previous.setVisible(true);
					});
				}else if(me.steps == "6"){
					me.oper.outteacherFormSubmit(outteacherForm,function(form, action, respText){
						me.removeAll(false);
						var pram = {id : me.infoprojectid.getValue()};
						var param = {jsonData : Ext.encode(pram)};
						me.oper.util.formLoad(outteacherForm2.getForm() , 'z_project/load_z_projectbyid.do', param, null,
							function(){

							});
						me.add(outteacherForm2);
						me.resetLocation(me);
						me.steps = "7";
						var titles = me.title.split("-");
						me.title = titles[0] + "- 项目第二校外指导老师信息";
						me.setTitle(me.title);
						previous.setVisible(true);
					});
				}else if(me.steps == "7"){
					me.oper.outteacherFormSubmit2(outteacherForm2,function(form, action, respText){
						me.removeAll(false);
						var pram = {id : me.infoprojectid.getValue()};
						var param = {jsonData : Ext.encode(pram)};
						me.oper.util.formLoad(abstractForm.getForm() , 'z_project/load_z_projectbyid.do', param, null,
							function(){

							});
						me.add(abstractForm);
						me.resetLocation(me);
						me.steps = "8";
						var titles = me.title.split("-");
						me.title = titles[0] + "- 项目简介";
						me.setTitle(me.title);
						previous.setVisible(true);
					});
				}else if(me.steps == "8"){
					me.oper.abstractFormSubmit(abstractForm,function(form, action, respText){
						me.removeAll(false);
						var pram = {id : me.infoprojectid.getValue()};
						var param = {jsonData : Ext.encode(pram)};
						me.oper.util.formLoad(gistForm.getForm() , 'z_project/load_z_projectbyid.do', param, null,
							function(){

							});
						me.add(gistForm);
						me.resetLocation(me);
						me.steps = "9";
						var titles = me.title.split("-");
						me.title = titles[0] + "- 申请理由";
						me.setTitle(me.title);
						previous.setVisible(true);
					});
				}else if(me.steps == "9"){
					me.oper.gistFormSubmit(gistForm,function(form, action, respText){
						me.removeAll(false);
						var pram = {id : me.infoprojectid.getValue()};
						var param = {jsonData : Ext.encode(pram)};
						me.oper.util.formLoad(schemeForm.getForm() , 'z_project/load_z_projectbyid.do', param, null,
							function(){

							});
						me.add(schemeForm);
						me.resetLocation(me);
						me.steps = "10";
						var titles = me.title.split("-");
						me.title = titles[0] + "- 项目方案";
						me.setTitle(me.title);
						previous.setVisible(true);
					});
				}else if(me.steps == "10"){
					me.oper.schemeFormSubmit(schemeForm,function(form, action, respText){
						me.removeAll(false);
						var pram = {id : me.infoprojectid.getValue()};
						var param = {jsonData : Ext.encode(pram)};
						me.oper.util.formLoad(featureForm.getForm() , 'z_project/load_z_projectbyid.do', param, null,
							function(){

							});
						me.add(featureForm);
						me.resetLocation(me);
						me.steps = "11";
						var titles = me.title.split("-");
						me.title = titles[0] + "- 特色与创新点";
						me.setTitle(me.title);
						previous.setVisible(true);
					});
				}else if(me.steps == "11"){
					me.oper.featureFormSubmit(featureForm,function(form, action, respText){
						me.removeAll(false);
						var pram = {id : me.infoprojectid.getValue()};
						var param = {jsonData : Ext.encode(pram)};
						me.oper.util.formLoad(scheduleForm.getForm() , 'z_project/load_z_projectbyid.do', param, null,
							function(){

							});
						me.add(scheduleForm);
						me.resetLocation(me);
						me.steps = "12";
						var titles = me.title.split("-");
						me.title = titles[0] + "- 项目进度安排";
						me.setTitle(me.title);
						previous.setVisible(true);
					});
				}else if(me.steps == "12"){
					me.oper.scheduleFormSubmit(scheduleForm,function(form, action, respText){
						me.removeAll(false);
						me.add(budgetgrid);
						budgetgrid.getStore().reload();
						me.resetLocation(me);
						me.steps = "13";
						var titles = me.title.split("-");
						me.title = titles[0] + "- 项目预算信息";
						me.setTitle(me.title);
						previous.setVisible(true);
						next.setText("提交项目");
						draft.setVisible(true);
					});
				}else if(me.steps == "13"){
					me.oper.projectSubmit(me.infoprojectid.getValue(), "tj", function(form, action, respText){
						Ext.MessageBox.alert('提示', '项目提交成功！');
						if(me.grid != null){
							me.grid.getStore().reload();
						}
						me.close();
					});
				}
			}
		});

		var draft = Ext.create('system.widget.FsdButton', {
			name : 'btnsave',
			text : '保存草稿',
			iconCls : 'nextIcon',
			handler : function() {
				me.oper.projectSubmit(me.infoprojectid.getValue(), "cg", function(form, action, respText){
					Ext.MessageBox.alert('提示', '项目提交成功！');
					me.close();
				});
			}
		});
		draft.setVisible(false);

		Ext.apply(this,{
			layout : 'fit',
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			items :[me.infoForm],
			buttons : [previous, draft, next, {
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	},

	/**
	 * 设置项目类型编码
	 * @param typeid
	 */
	setTypeId:function(typeid){
		this.f_typeid.setValue(typeid);
	},
	/**
	 * 设置项目类型名称
	 * @param typename
	 */
	setTypeName:function (typename) {
		this.f_typename.setValue(typename);
	},

	/**
	 * 重置窗口的位置
	 * @param window
	 */
	resetLocation: function (window) {
		var docHeight = document.body.clientHeight;
		var docWidth = document.body.clientWidth;
		var winHeight = window.getHeight();
		var winWidth = window.getWidth();

		var y = (docHeight - winHeight)/2;
		var x = (docWidth - winWidth)/2;
		window.setPosition(x, y);
	}
});