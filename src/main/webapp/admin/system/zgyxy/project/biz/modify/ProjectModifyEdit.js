﻿/**
 * 项目变更表编辑界面
 * @author lumingbao
 */
Ext.define(	'system.zgyxy.project.biz.modify.ProjectModifyEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	height:600,
	width:850,
	oper : Ext.create('system.zgyxy.project.biz.modify.ProjectModifyOper'),
	initComponent:function(){
		var me = this;

		me.f_id = Ext.create('Ext.form.field.Hidden',{
			name: 'id'
		});

		me.f_projectId = Ext.create('Ext.form.field.Hidden',{
			name: 'f_projectid'
		});

		var f_modifytypeid = Ext.create('system.widget.FsdComboBoxZD',{
			fieldLabel:'变更事项',
			labelAlign:'right',
			labelWidth:70,
			width : 450,
			name : 'f_modifytypeid',//提交到后台的参数名
			key : 'xmbgsx',//参数
			allowBlank: false,
			hiddenName : 'f_modifytypename'//提交隐藏域Name
		});
		var f_modifytypename = Ext.create('Ext.form.field.Hidden',{
			fieldLabel: '变更事项名称',
			name: 'f_modifytypename'
		});

		var f_modifyinfo = Ext.create('system.widget.FsdUeditor',{
			labelAlign:'right',
			labelWidth:1,
			width: 800,
			height : 400,
			style: {
				marginBottom: '25px'
			},
			name: 'f_modifyinfo'
		});

		var f_modifyreason = Ext.create('system.widget.FsdUeditor',{
			labelAlign:'right',
			labelWidth:1,
			width: 800,
			height : 400,
			style: {
				marginBottom: '25px'
			},
			name: 'f_modifyreason'
		});

		var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			autoScroll : true,
			padding : '20 0 15 15',
			items : [
				me.f_id,
				me.f_projectId,
				f_modifytypeid,
				f_modifytypename,
				f_modifyinfo,
				f_modifyreason
			]
		});

		Ext.apply(this,{
			layout : 'fit',
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			items :[form],
			buttons : [{
				name : 'btnsave',
				text : '保存',
				iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
				handler : function() {
					me.oper.formSubmit(form, me.grid);
				}
			}, {
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	},

	setProjectid:function (projectId) {
		this.f_projectId.setValue(projectId);
	}
});