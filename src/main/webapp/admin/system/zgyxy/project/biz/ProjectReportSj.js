﻿/**
 * 大创项目申报省级项目统计报表
 */
Ext.define('system.zgyxy.project.biz.ProjectReportSj', {
	extend : 'system.widget.FsdFormPanel',
	header : false,
	border : 0,
	layout : 'fit',
    initComponent : function() {
        var me = this;
        var f_starttime = Ext.create('system.widget.FsdDateField',{
            fieldLabel: '开始时间',
            labelAlign:'right',
            labelWidth:90,
            width : 360,
            name: 'f_starttime',
            allowBlank: false
        });

        var f_stoptime = Ext.create('system.widget.FsdDateField',{
            fieldLabel: '结束时间',
            labelAlign:'right',
            labelWidth:90,
            width : 360,
            name: 'f_stoptime',
            allowBlank: false
        });
        var con = 0;
        Date.prototype.Format = function (fmt) { //author: meizz
            var o = {
                "M+": this.getMonth() + 1, //月份
                "d+": this.getDate(), //日
                "h+": this.getHours(), //小时
                "m+": this.getMinutes(), //分
                "s+": this.getSeconds(), //秒
                "q+": Math.floor((this.getMonth() + 3) / 3), //季度
                "S": this.getMilliseconds() //毫秒
            };
            if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
            for (var k in o)
                if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
            return fmt;
        };
        me.tbar = [f_starttime, f_stoptime, {
            text: '查询',
            iconCls: 'magnifierIcon',
            handler: function () {
                if(con == 0){
                    var start = f_starttime.getValue().Format("yyyyMMdd") + "000000";
                    var end = f_stoptime.getValue().Format("yyyyMMdd") + "235959";
                    console.log(start)
                    console.log(end);
                    me.add(new Ext.panel.Panel({
                        layout:'fit',
                        html: "<iframe src='report_dcxmsj.htm?start="+ start +"&end="+ end +"' frameborder='0' width='100%' height='100%'></iframe>"
                    }));
                    f_starttime.setDisabled(true);
                    f_stoptime.setDisabled(true);
                    this.setText("重置");
                    con = 1;
                }else{
                    me.removeAll(true);
                    this.setText("查询");
                    f_starttime.setDisabled(false);
                    f_stoptime.setDisabled(false);
                    con = 0;
                }
            }
        }];
        me.callParent();
    }
});