﻿/**
 * 项目分组选择界面
 * @author lumingbao
 */
Ext.define(	'system.zgyxy.project.biz.group.ProjectGroupChoose', {
	extend : 'Ext.window.Window',
	grid : null,
	projectids: '',
	oper : Ext.create('system.zgyxy.project.biz.group.ProjectGroupOper'),
	requires : ['system.zgyxy.project.model.group.Extjs_Column_ProjectGroup',
		'system.zgyxy.project.model.group.Extjs_Model_ProjectGroup'],
	initComponent:function(){
		var me = this;

		var groupGrid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_projectgroup',
			model : 'model_z_projectgroup',
			baseUrl : 'z_projectgroup/load_pagedata1.do',
			selType : 'rowmodel', // 多选框选择模式
			border : false
		});

		Ext.apply(this,{
			layout : 'fit',
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			width : 500,
			height : 400,
			items :[groupGrid],
			buttons : [{
				name : 'btnsave',
				text : '保存',
				iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
				handler : function() {
					me.oper.projectGroupSubmit(me.grid, groupGrid, me.projectids);
				}
			}, {
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	}
});