﻿/**
 * 项目分组报表
 * @author lumingbao
 */
Ext.define('system.zgyxy.project.biz.group.ProjectGroupReport', {
	extend : 'system.widget.FsdFormPanel',
	header : false,
	border : 0,
	layout : 'fit',
	groupid:'',
	initComponent : function() {
		var me = this;
		me.callParent();
	},

	setHtml :function(html){
		var me = this;
		me.html = html;
	}
});