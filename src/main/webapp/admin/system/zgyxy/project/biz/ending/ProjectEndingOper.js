Ext.define('system.zgyxy.project.biz.ending.ProjectEndingOper', {

	util : Ext.create('system.util.util'),

    /**
     * 编辑中期检查表
     */
    edit : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选中一个项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.zgyxy.project.biz.ending.ProjectEndingEdit');
            win.setTitle('提交项目结项');
            win.modal = true;
            win.grid = grid;
            var pram = {projectid : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(win.down('form').getForm() , 'z_projectending/load_z_projectendingbyProjectid.do' , param , null ,
                function(form, action, respText, result){
                    win.show();
                });
        }
    },

    /**
     * 表单提交
     * @param formpanel
     * @param grid
     */
    formSubmit : function(formpanel, grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_projectending/save_z_projectending_qx_z_projectendingbj.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    grid.getStore().reload();
                    Ext.MessageBox.alert('提示', '保存成功！');
                }, null);
        }
    }
});