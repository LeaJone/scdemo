﻿/**
 * 项目管理编辑界面
 *
 * @author lhl
 */
Ext.define('system.zgyxy.project.biz.ProjectEdit', {
	extend : 'system.widget.FsdFormPanel',
    grid : null,
    typeid1 : '',
    typename1: '',
    fid:'',
	requires : [ 'system.zgyxy.team.model.Extjs_Z_team',
                 'system.zgyxy.team.model.Extjs_Z_teamanduser',
                 'system.zgyxy.domain.model.Extjs_Column_Z_domain',
                 'system.zgyxy.domain.model.Extjs_Model_Z_domain',
				 'system.abasis.employee.model.Extjs_A_employee',
                 'system.zgyxy.budget.model.Extjs_Model_Budget',
                 'system.zgyxy.budget.model.Extjs_Column_Budget'],

    oper : Ext.create('system.zgyxy.project.biz.ProjectOper'),
    budgetOper : Ext.create('system.zgyxy.budget.biz.BudgetOper'),
	
	initComponent : function() {
		var me = this;

        var id = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '编号',
            name : 'id'
        });

        var f_name = Ext.create('system.widget.FsdTextField',{
            fieldLabel: '项目名称',
            labelAlign:'right',
            labelWidth:90,
            width : 900,
            name: 'f_name',
            maxLength: 500,
            allowBlank: false
        });

        me.f_typeid = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'项目类型',
            labelAlign:'right',
            labelWidth:90,
            width : 450,
            name : 'f_typeid',//提交到后台的参数名
            key : 'projecttype',//参数
            allowBlank: false,
            hiddenName : 'f_typename'//提交隐藏域Name
        });
        var f_typename = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '项目类型',
            name: 'f_typename'
        });

        var f_typename1 = Ext.create('system.widget.FsdTextGrid', {
            zFieldLabel : '一级学科',
            zLabelWidth : 90,
            width : 450,
            zName : 'f_typename1',
            zColumn : 'column_z_domainxz', // 显示列
            zModel : 'model_z_domain',
            zBaseUrl : 'z_domain/load_AllDomainListByPopdom.do',
            zAllowBlank : false,
            zGridType : 'list',
            zGridWidth : 700,//弹出表格宽度
            zGridHeight : 600,//弹出表格高度
            zIsText1 : true,
            zTxtLabel1 : '领域名称',
            zFunChoose : function(data){
                f_typeid1.setValue(data.id);
                f_typename1.setValue(data.title);
            },
            zFunQuery : function(txt1){
                return {fid : 'FSDMAIN', txt : txt1};
            }
        });
        var f_typeid1 = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '学科领域编码',
            name: 'f_typeid1'
        });

        var f_teamname = Ext.create('system.widget.FsdTextGrid', {
            zFieldLabel : '项目团队',
            zLabelWidth : 90,
            width : 450,
            zName : 'f_teamname',
            zColumn : 'column_project_team', // 显示列
            zModel : 'model_z_team',
            zBaseUrl : 'z_team/load_pagedata.do',
            zAllowBlank : false,
            zGridType : 'page',
            zGridWidth : 700,//弹出表格宽度
            zGridHeight : 600,//弹出表格高度
            zIsText1 : true,
            zTxtLabel1 : '团队名称',
            zFunChoose : function(data){
                f_teamid.setValue(data.id);
                f_teamname.setValue(data.f_name);
                f_leadername.zTeamId = data.id;
            },
            zFunQuery : function(txt1){
                return {name : txt1};
            }
        });
        var f_teamid = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '项目团队编码',
            name: 'f_teamid'
        });

        var f_teachername = Ext.create('system.widget.FsdTextGrid', {
            zFieldLabel : '第一指导教师',
            zLabelWidth : 90,
            width : 450,
            zName : 'f_teachername',
            zColumn : 'column_project_employee', // 显示列
            zModel : 'model_a_employee',
            zBaseUrl : 'A_Employee/loadDataByType.do',
            zGridType : 'page',
            zGridWidth : 700,//弹出表格宽度
            zGridHeight : 600,//弹出表格高度
            zIsText1 : true,
            zTxtLabel1 : '校内指导教师姓名',
            zAllowBlank : false,
            zFunChoose : function(data){
                f_teacherid.setValue(data.id);
                f_teachername.setValue(data.realname);
            },
            zFunQuery : function(txt1){
                return {type : 'js', name : txt1};
            }
        });
        var f_teacherid = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '校内指导教师编码',
            name: 'f_teacherid'
        });

        var f_teachername2 = Ext.create('system.widget.FsdTextGrid', {
            zFieldLabel : '第二指导教师',
            zLabelWidth : 90,
            width : 450,
            zName : 'f_teachername2',
            zColumn : 'column_project_employee', // 显示列
            zModel : 'model_a_employee',
            zBaseUrl : 'A_Employee/loadDataByType.do',
            zGridType : 'page',
            zGridWidth : 700,//弹出表格宽度
            zGridHeight : 600,//弹出表格高度
            zIsText1 : true,
            zTxtLabel1 : '校外指导教师姓名',
            zFunChoose : function(data){
                f_teacherid2.setValue(data.id);
                f_teachername2.setValue(data.realname);
            },
            zFunQuery : function(txt1){
                return {type : 'js', name : txt1};
            }
        });
        var f_teacherid2 = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '第二指导教师编码',
            name: 'f_teacherid2'
        });

        var f_leadername = Ext.create('system.widget.FsdTextGridById', {
            zFieldLabel : '项目负责人',
            zLabelWidth : 90,
            width : 450,
            zName : 'f_leadername',
            zColumn : 'column_project_member', // 显示列
            zModel : 'model_z_teamanduser',
            zBaseUrl : 'z_teamanduser/load_teamuserbyid.do',
            zAllowBlank : false,
            zGridType : 'list',
            zGridWidth : 700,//弹出表格宽度
            zGridHeight : 500,//弹出表格高度
            zIsText1 : true,
            zTxtLabel1 : '项目负责人姓名',
            zFunChoose : function(data){
                f_leadername.setValue(data.username);
                f_leaderid.setValue(data.f_userid);
                f_studentnumber.setValue(data.usernumber);
            },
            zFunQuery : function(txt1){
                return {id : f_leadername.zTeamId, txt : txt1};
            }
        });
        var f_leaderid = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '项目负责人编码',
            name: 'f_leaderid'
        });
        var f_studentnumber = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '项目负责人学号',
            name: 'f_studentnumber'
        });
	    
	    var f_phone = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '联系电话',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 450,
	    	name: 'f_phone',
	    	maxLength: 20,
            vtype : 'cellphone',
            allowBlank: false
	    });

        var f_email = Ext.create('system.widget.FsdTextField',{
            fieldLabel: '电子邮件',
            labelAlign:'right',
            labelWidth:90,
            width : 450,
            name: 'f_email',
            maxLength: 50,
            vtype : 'email',
            allowBlank: false
        });

        var f_abstract = Ext.create('system.widget.FsdTextArea',{
            fieldLabel: '项目简介',
            labelAlign: 'right',
            labelWidth: 90,
            width: 900,
            height: 200,
            name: 'f_abstract',
            allowBlank: false,
            maxLength:200
        });

        var f_gist = Ext.create('system.widget.FsdUeditor',{
            fieldLabel: '申请理由',
            labelAlign:'right',
            labelWidth:90,
            width: 910,
            height : 500,
            style: {
                marginBottom: '25px'
            },
            name: 'f_gist'
        });

        var f_scheme = Ext.create('system.widget.FsdUeditor',{
            fieldLabel: '项目方案',
            labelAlign:'right',
            labelWidth:90,
            width: 910,
            height : 500,
            style: {
                marginBottom: '25px'
            },
            name: 'f_scheme'
        });

        var f_feature = Ext.create('system.widget.FsdUeditor',{
            fieldLabel: '特色与创新点',
            labelAlign:'right',
            labelWidth:90,
            width: 910,
            height : 500,
            style: {
                marginBottom: '25px'
            },
            name: 'f_feature'
        });

        var f_schedule = Ext.create('system.widget.FsdUeditor',{
            fieldLabel: '项目进度安排',
            labelAlign:'right',
            labelWidth:90,
            width: 910,
            height : 500,
            style: {
                marginBottom: '25px'
            },
            name: 'f_schedule'
        });

	    me.form = Ext.create('Ext.form.Panel',{
            tbar : [{
                xtype : "fsdbutton",
                text : "保存草稿",
                name : 'btnsave1',
                iconCls : 'drafts',
                handler : function() {
                    me.oper.formSubmit(me, me.grid, "projectstate-cg", me, me.budgetPanel);
                }
            }, {
                xtype : "fsdbutton",
                text : "提 交",
                name : 'btnsave2',
                iconCls : 'acceptIcon',
                handler : function() {
                    me.oper.formSubmit(me, me.grid, "projectstate-tjwsh", me, me.budgetPanel);
                }
            }],
			border : false,
            padding : '10 0 30 10',
			items : [id, f_typename, f_typeid1, f_teamid, f_teacherid, f_teacherid2, f_leaderid, f_studentnumber,f_name ,
                {
				layout : 'column',
				border : false,
				width : 900,
				items:[{
					columnWidth : 0.5,
					border:false,
					items:[me.f_typeid, f_teamname, f_teachername, f_phone]
				}, {
					columnWidth : 0.5,
					border:false,
					items:[f_typename1, f_leadername, f_teachername2, f_email]
				}]
			}, f_abstract, f_gist, f_scheme, f_feature, f_schedule]
	    });

        me.on('boxready', function(){
            f_leadername.zTeamId = f_teamid.getValue();
            f_typeid1.setValue(me.typeid1);
            f_typename1.setValue(me.typename1);
        });

        // 创建表格
        var budgetgrid = Ext.create("system.widget.FsdGridPagePanel", {
            column : 'column_z_budget',
            model : 'model_z_budget',
            baseUrl : 'z_budget/load_pagedata.do',
            border : false,
            tbar : [{
                xtype : "fsdbutton",
                text : "添加",
                iconCls : 'add',
                popedomCode : 'projectbj',
                handler : function() {
                    me.budgetOper.add(budgetgrid, me.fid);
                }
            }, ,'-', {
                xtype : "fsdbutton",
                text : "编辑",
                iconCls : 'page_editIcon',
                popedomCode : 'projectbj',
                handler : function() {
                    me.budgetOper.editor(budgetgrid);
                }
            }, {
                text : '查看',
                iconCls : 'magnifierIcon',
                handler : function() {
                    me.budgetOper.view(budgetgrid);
                }
            }, {
                xtype : "fsdbutton",
                text : "删除",
                iconCls : 'page_deleteIcon',
                popedomCode : 'projectsc',
                handler : function() {
                    me.budgetOper.del(budgetgrid);
                }
            },  '->', {
                text : '刷新',
                iconCls : 'tbar_synchronizeIcon',
                handler : function() {
                    budgetgrid.getStore().reload();
                }
            }]
        });

        me.budgetPanel = Ext.create('Ext.panel.Panel',{
            title: '预算信息',
            layout : 'fit',
            disabled : true,
            items : [budgetgrid]
        });

        var tabpanel = Ext.create('Ext.tab.Panel', {
            border: 0,
            enableTabScroll: true,
            deferredRender: false,
            defaults: { autoScroll: true },
            layout : 'fit',
            items: [{
                border: 0,
                title: '基本信息',
                items: [me.form]
            }, me.budgetPanel]
        });
	    
		Ext.apply(me, {
        	title: '项目申报',
			autoScroll : true,
	        items :[tabpanel]
	    });
		me.callParent(arguments);
	}

});