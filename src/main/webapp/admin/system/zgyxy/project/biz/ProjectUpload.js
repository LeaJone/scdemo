﻿/**
 * 项目申报书上传界面
 * @author lumingbao
 */
Ext.define(	'system.zgyxy.project.biz.ProjectUpload', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.zgyxy.project.biz.ProjectOper'),
	initComponent:function(){
		var me = this;

		me.f_fid = Ext.create('Ext.form.field.Hidden',{
			name:'f_fid'
		});

		var projectsbs = Ext.create('system.widget.FsdTextFileManage', {
			width : 400,
			zName : 'projectsbs',
			zFieldLabel : '申报书',
			zLabelWidth : 80,
			zIsShowButton : false,
			zIsGetButton : false,
			zIsUpButton : true,
			zIsReadOnly : true,
			zFileType: 'file',
			zFileUpPath : me.oper.util.getFilePath()
		});


		var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '15 10 5 10',
			items : [me.f_fid, projectsbs]
		});

		Ext.apply(this,{
			layout : 'fit',
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			items :[form],
			buttons : [{
				name : 'btnsave',
				text : '保存',
				iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
				handler : function() {
					me.oper.uploadformSubmit(form, me.grid);
				}
			}, {
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	},

	/**
	 * 设置父级ID
	 * @param tutorid
	 */
	setFid:function (fid) {
		this.f_fid.setValue(fid);
	}
});