﻿/**
 * 项目分组添加界面
 * @author lumingbao
 */
Ext.define(	'system.zgyxy.project.biz.luyan.ProjectLuyanEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.zgyxy.project.biz.ProjectOper'),
	initComponent:function(){
		var me = this;

		me.f_id = Ext.create('Ext.form.field.Hidden',{
			name: 'id'
		});

		me.projectid = Ext.create('Ext.form.field.Hidden',{
			name: 'projectid'
		});

		var f_score = Ext.create('system.widget.FsdTextField',{
			fieldLabel: '路演得分',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name: 'f_score',
			maxLength: 500,
			allowBlank: false
		});

		var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 20 15 5',
			items : [
				me.f_id,
				me.projectid,
				f_score
			]
		});

		Ext.apply(this,{
			layout : 'fit',
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			items :[form],
			buttons : [{
				name : 'btnsave',
				text : '保存',
				iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
				handler : function() {
					me.oper.luyanFormSubmit(form, me.grid);
				}
			}, {
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	},

	setProjectid : function (projectid) {
		this.projectid.setValue(projectid);
	}
});