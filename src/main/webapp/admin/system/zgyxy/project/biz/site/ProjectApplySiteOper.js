/**
 * 项目申报网站编辑操作类
 *
 * @author lumingbao
 */
Ext.define('system.zgyxy.project.biz.site.ProjectApplySiteOper', {

	form : null,
	util : Ext.create('system.util.util'),

    /**
     * 提交项目基本信息
     */
    infoFormSubmit : function(formpanel, fun){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_projectsite/save_project_info.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    fun(form, action, respText);
                });
        }
    },

    /**
     * 提交简介信息
     */
    abstractFormSubmit : function(formpanel, fun){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_projectsite/save_abstract_info.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    fun(form, action, respText);
                });
        }
    },

    /**
     * 提交团队风采信息
     */
    teammienFormSubmit : function(formpanel, fun){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_projectsite/save_teammien_info.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    fun(form, action, respText);
                });
        }
    },

    /**
     * 提交成果展示信息
     */
    achievementFormSubmit : function(formpanel, fun){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_projectsite/save_achievement_info.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    fun(form, action, respText);
                });
        }
    },

    /**
     * 提交指导老师信息
     */
    adviserFormSubmit : function(formpanel, fun){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_projectsite/save_adviser_info.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    fun(form, action, respText);
                });
        }
    },

    /**
     * 提交支撑材料信息
     */
    materialsFormSubmit : function(formpanel, fun){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_projectsite/save_materials_info.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    fun(form, action, respText);
                });
        }
    },

    /**
     * 提交项目
     */
    projectSubmit : function(projectid, type, fun){
        var me = this;
        var pram = {projectid : projectid, type : type};
        var param = {jsonData : Ext.encode(pram)};
        me.util.ExtAjaxRequest('z_xkjnjs/save_xkjnjs_submit.do' , param ,
            function(response, options, respText){
                fun(response, options, respText);
            }, null, me.form);
    },

    /**
     * 表单提交
     * @param formpanel
     * @param grid
     */
    teacherFormSubmit : function(formpanel, grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_xkjnjsteacher/save_z_xkjnjsteacher_qx_z_xkjnjsteacherbj.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    grid.getStore().reload();
                    Ext.MessageBox.alert('提示', '保存成功！');
                    formpanel.up('window').close();
                }, null);
        }
    },

    /**
     * 表单提交
     */
    formSubmit : function(formpanel, grid, type){
        var me = this;
        if (formpanel.form.isValid()) {
            var pram = {status : type};
            var param = {jsonData : Ext.encode(pram)};
            me.util.ExtFormSubmit('z_xkjnjs/save_z_xkjnjs_qx_z_xkjnjsbj.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(){
                    grid.getStore().reload();
                    Ext.MessageBox.alert('提示', '保存成功！');
                    me.util.closeTab();
                }, null, param);
        }
    },

    /**
     * 修改
     */
    editor : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要编辑的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能编辑一个项目！');
        }else{
            // 先得到主键
            var status = data[0].data.f_statusname;
            if(status == "草稿"){
                var win = Ext.create('system.zgyxy.xkjnjs.biz.apply.XkjnjsApply');
                win.title = '学科技能竞赛申报 - 基本信息';
                win.modal = true;
                win.grid = grid;

                var id = data[0].data.id;
                var pram = {id : id};
                var param = {jsonData : Ext.encode(pram)};
                me.util.formLoad(win.infoForm.getForm() , 'z_xkjnjs/load_z_xkjnjsbyid.do' , param , null ,
                    function(form, action, respText, result){
                        win.show();
                    });
            }else{
                var id = data[0].data.id;
                me.util.ExtAjaxRequest('z_xkjnjs/get_projectAuditStep.do?projectid=' + id , null ,
                    function(response, options, respText){
                        if(respText != null && respText.data != null && respText.data != "" && respText.data == "根据审核意见修改申请信息"){
                            var win = Ext.create('system.zgyxy.xkjnjs.biz.apply.XkjnjsApplyUpdate');
                            var id = data[0].data.id;
                            var pram = {id : id};
                            var param = {jsonData : Ext.encode(pram)};
                            me.util.formLoad(win.infoForm.getForm() , 'z_xkjnjs/load_z_xkjnjsbyid.do' , param , null ,
                                function(form, action, respText, result){
                                    var type = respText.data.f_typeid;
                                    if("cxxlxm" == type){
                                        win.title = '创新训练项目申报 - 基本信息';
                                    }else if("cyxlxm" == type){
                                        win.title = '创业训练项目申报 - 基本信息';
                                    }else if("cysjxm" == type){
                                        win.title = '创业实践项目申报 - 基本信息';
                                    }else{
                                        win.title = '项目申报 - 基本信息';
                                    }
                                    win.modal = true;
                                    win.grid = grid;
                                    win.show();
                                });
                        }else{
                            Ext.MessageBox.alert('提示', '已提交的项目不支持编辑操作！');
                        }
                    });
            }
        }
    },

    /**
     * 删除
     */
    del : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要删除的项目！');
        }else{
            // 先得到主键
            var status = data[0].data.f_statusname;
            if(status == "草稿"){
                Ext.MessageBox.show({
                    title : '提示',
                    msg : '您确定要删除所选项目吗?',
                    width : 250,
                    buttons : Ext.MessageBox.YESNO,
                    icon : Ext.MessageBox.QUESTION,
                    fn : function(btn) {
                        if (btn == 'yes') {
                            var dir = new Array();
                            Ext.Array.each(data, function(items) {
                                var id = items.data.id;
                                dir.push(id);
                            });
                            var pram = {ids : dir};
                            var param = {jsonData : Ext.encode(pram)};
                            me.util.ExtAjaxRequest('z_xkjnjs/del_z_xkjnjs_qx_z_xkjnjssc.do' , param ,
                                function(){
                                    Ext.MessageBox.alert('提示', '删除成功！');
                                    grid.getStore().reload();
                                }, null, me.form);
                        }
                    }
                });
            }else{
                Ext.MessageBox.alert('提示', '已提交的项目不能删除！');
            }
        }
    },

    /**
     * 查看
     */
    view : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要查看的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能查看一个项目！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var model = Ext.create("system.zgyxy.xkjnjs.biz.XkjnjsEdit");
            model.grid = grid;
            model.down('button[name=btnsave1]').setVisible(false);
            model.down('button[name=btnsave2]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(model.down('form').getForm() , 'z_xkjnjs/load_z_xkjnjsbyid.do', param, null,
                function(){
                    me.util.addTab('projectbj', "竞赛项目查看", model, "fa-list-ul");
                });
        }
    },

    /**
     * PDF文件下载
     */
    download : function(f_pdf){
        var me = this;
        if(f_pdf != "" && f_pdf != null){
            Ext.MessageBox.show({
                title : '询问',
                msg : '你确定要下载PDF吗？',
                width : 250,
                buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                        window.open('Common/download.do?path=' + f_pdf);
                    }
                }
            });
        }
    },

    /**
     * 根据ID进行查看操作
     */
    viewById : function(id){
        var me = this;
        var model = Ext.create("system.zgyxy.xkjnjs.biz.XkjnjsEdit");
        model.down('button[name=btnsave1]').setVisible(false);
        model.down('button[name=btnsave2]').setVisible(false);
        var pram = {id : id};
        var param = {jsonData : Ext.encode(pram)};
        me.util.formLoad(model.down('form').getForm() , 'z_xkjnjs/load_z_xkjnjsbyid.do', param, null,
            function(){
                me.util.addTab('projectbj', "竞赛项目查看", model, "fa-list-ul");
            });
    },

    /**
     * 下载项目申报书
     * @param projectid
     */
    downloadProjectApplyBook : function (projectid) {
        var me = this;
        me.util.ExtAjaxRequest('z_xkjnjs/download_projectApplyBook.do?projectid=' + projectid , null ,
            function(response, options, respText){
                var f_pdf = respText.data;
                location.href = "Common/download.do?path=" + f_pdf;
            }, null, me.form);
    },
});