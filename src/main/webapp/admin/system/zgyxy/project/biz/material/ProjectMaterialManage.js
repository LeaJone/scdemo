﻿/**
 * 项目资料管理界面
 * @author lumingbao
 */
Ext.define(	'system.zgyxy.project.biz.material.ProjectMaterialManage', {
	extend : 'Ext.window.Window',
	requires : [ 'system.zgyxy.project.model.material.Extjs_Column_ProjectMaterial',
		'system.zgyxy.project.model.material.Extjs_Model_ProjectMaterial'],
	oper : Ext.create('system.zgyxy.project.biz.material.ProjectMaterialOper'),
	projectid : '',
	initComponent:function(){
		var me = this;

		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_projectmaterial',
			model : 'model_z_projectmaterial',
			baseUrl : 'z_projectmaterial/load_pagedata.do',
			border : false,
			tbar : [{
				xtype : "fsdbutton",
				text : "上传项目资料",
				iconCls : 'up',
				handler : function() {
					me.oper.add(grid, me.projectid);
				}
			}, {
				xtype : "fsdbutton",
				text : "修改项目资料",
				iconCls : 'page_editIcon',
				handler : function() {
					me.oper.editor(grid);
				}
			}, {
				text : '查看项目资料',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid);
				}
			}, {
				xtype : "fsdbutton",
				text : "删除项目资料",
				iconCls : 'page_deleteIcon',
				handler : function() {
					me.oper.del(grid);
				}
			},  '->', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]
		});
		grid.getStore().on('beforeload', function(s) {
			var pram = {projectid : me.projectid};
			var params = s.getProxy().extraParams;
			Ext.apply(params,{jsonData : Ext.encode(pram)});
		});

		Ext.apply(this,{
			layout : 'fit',
			width: 700,
			height: 500,
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			items :[grid],
			buttons : [{
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	},

	/**
	 * 设置父级ID
	 * @param tutorid
	 */
	setProjectId : function (projectid) {
		this.projectid = projectid;
	}
});