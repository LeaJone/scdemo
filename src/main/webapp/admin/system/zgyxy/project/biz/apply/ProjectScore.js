﻿/**
 * 项目评分
 * @author lumingbao
 */
Ext.define(	'system.zgyxy.project.biz.apply.ProjectScore', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.zgyxy.project.biz.apply.ProjectApplyOper'),
	callbackFun :  null,
	initComponent:function(){
		var me = this;

		me.f_projectid = Ext.create('Ext.form.field.Hidden',{
			name: 'f_projectid',
		});

		var f_score = Ext.create('system.widget.FsdTextField',{
			name: 'f_score',
			fieldLabel: '得分',
			labelAlign:'right',
			labelWidth:50,
			width : 300,
			maxLength: 500,
			allowBlank: false
		});

		var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '10 20 15 5',
			items : [
				me.f_projectid,
				f_score
			]
		});

		Ext.apply(this,{
			layout : 'fit',
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			items :[form],
			buttons : [{
				name : 'btnsave',
				text : '保存',
				iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
				handler : function() {
					me.oper.scoreFormSubmit(form, me.callbackFun);
				}
			}, {
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	},

	/**
	 * 设置父级ID
	 * @param tutorid
	 */
	setProjectId : function (projectid) {
		this.f_projectid.setValue(projectid);
	},

	setCallbackFun:function(callbackFun){
		this.callbackFun  =  callbackFun;
	}
});