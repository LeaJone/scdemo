﻿/**
 * 项目分组管理界面
 * @author lumingbao
 */
Ext.define('system.zgyxy.project.biz.group.ProjectGroupManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.zgyxy.project.model.group.Extjs_Column_ProjectGroup',
	             'system.zgyxy.project.model.group.Extjs_Model_ProjectGroup',
                    'system.zgyxy.project.model.group.Extjs_Column_Group',
                    'system.zgyxy.project.model.group.Extjs_Model_Group'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.groupOper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    groupOper : Ext.create('system.zgyxy.project.biz.group.ProjectGroupOper'),
    
	createTable : function() {
	    var me = this;

	    var queryid = "";

        /**
         * 左边的表格
         */
        var groupGrid = Ext.create("system.widget.FsdGridPagePanel", {
            column : 'column_z_group',
            model : 'model_z_group',
            baseUrl : 'z_projectgroup/load_pagedata.do',
            selType : 'rowmodel', // 多选框选择模式
            border : false,
            tbar : [{
                xtype : "fsdbutton",
                text : "添加",
                iconCls : 'page_addIcon',
                handler : function() {
                    me.groupOper.add(groupGrid);
                }
                }, {
                    xtype : "fsdbutton",
                    text : "编辑",
                    iconCls : 'page_editIcon',
                    handler : function() {
                        me.groupOper.editor(groupGrid);
                    }
                }, {
                    text : '查看',
                    iconCls : 'magnifierIcon',
                    handler : function() {
                        me.groupOper.view(groupGrid);
                    }
                }, {
                    xtype : "fsdbutton",
                    text : "删除",
                    iconCls : 'page_deleteIcon',
                    handler : function() {
                        me.groupOper.del(groupGrid);
                    }
                },  '->', {
                    text : '刷新',
                    iconCls : 'tbar_synchronizeIcon',
                    handler : function() {
                        groupGrid.getStore().reload();
                    }
            }],
            listeners: { 'itemclick': function (view, record, item, index, e, opts) {
                   queryid = record.data.id;
                    grid.getStore().reload();
                }
            }
         });

        /**
         * 查询文本框
         */
        var queryText =  Ext.create("Ext.form.TextField" , {
            name : 'queryParam',
            emptyText : '请输入项目名称',
            enableKeyEvents : true,
            width : 130,
            listeners : {
                specialkey : function(field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        grid.getStore().reload();
                    }
                }
            }
        });
	    
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_projectgroup',
			model : 'model_z_projectgroup',
			baseUrl : 'z_project/load_pagedata.do',
			border : false,
			tbar : [{
                xtype : "fsdbutton",
                text : "项目分组",
                iconCls : 'page_editIcon',
                handler : function() {
                    me.groupOper.projectGroup(grid);
                }
            },{
                xtype : "fsdbutton",
                text : "导出",
                iconCls : 'page_editIcon',
                handler : function() {
                    me.groupOper.projectGroupReport(queryid);
                }
            },  '->', queryText, {
                text: '查询',
                iconCls: 'magnifierIcon',
                handler: function () {
                    grid.getStore().reload();
                }
            }, {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]
		});
		grid.getStore().on('beforeload', function(s) {
            var text =  queryText.getValue();
            var pram = {name : text, queryid: queryid};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});

        grid.on('cellclick', function(thisObj, td, cellIndex, record) {
            if (cellIndex == 9){
                me.oper.download(record.get('f_pdf'));
            }
        });

        var cgrouppanel = Ext.create("Ext.panel.Panel", {
            frame:true,
            split: true,
            region: 'west',
            layout: 'fit',
            title: '分组信息',
            width: 360,
            items: [groupGrid]
        });

        var contentpanel = Ext.create("Ext.panel.Panel", {
            frame:true,
            split: true,
            region: 'center',
            layout: 'fit',
            title: '项目信息',
            items: [grid]
        });

        var page1_jExtPanel1_obj = new Ext.panel.Panel({
            bodyPadding: 3,
            header : false,
            border : 0,
            split: true,
            layout: 'border',
            items: [cgrouppanel,contentpanel]
        });
		return page1_jExtPanel1_obj;
	}
});