﻿/**
 * 项目结题结果
 * @author lumingbao
 */
Ext.define(	'system.zgyxy.project.biz.ProjectEnding', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.zgyxy.project.biz.ProjectOper'),
	initComponent:function(){
		var me = this;

		me.projectids = Ext.create('Ext.form.field.Hidden',{
			name: 'projectids',
		});

		var ddmbid = Ext.create('system.widget.FsdComboBoxZD',{
			fieldLabel:'是否达到目标',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name : 'ddmbid',//提交到后台的参数名
			key : 'xmjxsfddmb',//参数
			allowBlank: false,
			hiddenName : 'ddmbname'//提交隐藏域Name
		});
		var ddmbname = Ext.create('Ext.form.field.Hidden',{
			fieldLabel: '是否达到目标名称',
			name: 'ddmbname'
		});


		var cjpdid = Ext.create('system.widget.FsdComboBoxZD',{
			fieldLabel:'成绩评定',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name : 'cjpdid',//提交到后台的参数名
			key : 'cjpd',//参数
			allowBlank: false,
			hiddenName : 'cjpdname'//提交隐藏域Name
		});
		var cjpdname = Ext.create('Ext.form.field.Hidden',{
			fieldLabel: '成绩评定名称',
			name: 'cjpdname'
		});

		var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 20 15 5',
			items : [
				me.projectids,
				ddmbid,
				ddmbname,
				cjpdid,
				cjpdname
			]
		});

		Ext.apply(this,{
			layout : 'fit',
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			items :[form],
			buttons : [{
				name : 'btnsave',
				text : '提交',
				iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
				handler : function() {
					me.oper.projectEndingSubmit(form, me.grid);
				}
			}, {
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	},

	/**
	 * 设置项目编码
	 * @param tutorid
	 */
	setProjectids : function (ids) {
		this.projectids.setValue(ids);
	}
});