Ext.define('system.zgyxy.project.biz.modify.ProjectModifyOper', {

	util : Ext.create('system.util.util'),

    /**
     * 编辑中期检查表
     */
    add : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选中一个项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.zgyxy.project.biz.modify.ProjectModifyEdit');
            win.setTitle('提交项目变更记录');
            win.modal = true;
            win.grid = grid;
            win.setProjectid(id);
            win.show();
        }
    },

    view : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选中一条变更记录！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一条变更记录！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.zgyxy.project.biz.modify.ProjectModifyEdit');
            win.setTitle('查看变更记录详情');
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(win.down('form').getForm() , 'z_projectmodify/load_z_projectmodifybyid.do' , param , null ,
                function(){
                    win.show();
                });
        }
    },

    /**
     * 表单提交
     * @param formpanel
     * @param grid
     */
    formSubmit : function(formpanel, grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_projectmodify/save_z_projectmodify_qx_z_projectmodifybj.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    grid.getStore().reload();
                    Ext.MessageBox.alert('提示', '保存成功！');
                }, null);
        }
    }
});