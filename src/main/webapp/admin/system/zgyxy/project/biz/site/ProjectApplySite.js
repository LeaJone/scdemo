﻿/**
 * 项目申报网站信息编辑
 * @author lumingbao
 */
Ext.define(	'system.zgyxy.project.biz.site.ProjectApplySite', {
	extend : 'Ext.window.Window',
	grid : null,
	steps : '1',
	title :'',
	oper : Ext.create('system.zgyxy.project.biz.site.ProjectApplySiteOper'),
	initComponent:function(){
		var me = this;

		// 项目基本信息表单项开始
		me.infoprojectid = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
		});

		me.f_fid = Ext.create('Ext.form.field.Hidden',{
			name: 'f_fid',
		});

		var f_applypdfurl = Ext.create('system.widget.FsdFile', {
			width : 500,
			zName : 'f_applypdfurl',
			zFieldLabel : '申报书',
			zLabelWidth : 90,
			zIsShowButton : true,
			zIsUpButton : true,
			zIsReadOnly : true,
			zFileType : 'file',
			zFileAutoName : true,
			zAllowBlank : false,
			zFileUpPath : me.oper.util.getFilePath(),
			zManageType : 'upload'
		});

		me.infoForm = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '15 20 15 5',
			items : [
				me.infoprojectid,
				me.f_fid,
				f_applypdfurl
			]
		});
		// 项目基本信息表单项结束

		// 项目简介信息表单项开始
		me.abstractprojectid = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
		});

		var f_videourl = Ext.create('system.widget.FsdTextFileManage', {
			width : 500,
			zName : 'f_videourl',
			zFieldLabel : '展示视频',
			zLabelWidth : 90,
			zIsShowButton : true,
			zIsUpButton : true,
			zIsReadOnly : true,
			zFileType : 'video',
			zFileAutoName : true,
			zFileUpPath : me.oper.util.getVideoPath(),
			zManageType : 'upload'
		});

		var f_abstract = Ext.create('system.widget.FsdUeditor',{
			fieldLabel: '项目简介',
			labelAlign:'right',
			labelWidth:90,
			width: 800,
			height : 400,
			name: 'f_abstract'
		});
		var abstractForm = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '10 20 15 5',
			items : [
				me.abstractprojectid,
				f_videourl,
				f_abstract
			]
		});
		// 项目简介信息表单项结束


		// 项目团队风采信息表单项开始
		me.teammienprojectid = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
		});

		var f_teammien = Ext.create('system.widget.FsdUeditor',{
			fieldLabel: '团队风采',
			labelAlign:'right',
			labelWidth:90,
			width: 800,
			height : 400,
			name: 'f_teammien'
		});
		var teammienForm = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '10 20 15 5',
			items : [
				me.teammienprojectid,
				f_teammien
			]
		});
		// 项目团队风采信息表单项结束

		// 项目成果展示信息表单项开始
		me.achievementrojectid = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
		});

		var f_achievement = Ext.create('system.widget.FsdUeditor',{
			fieldLabel: '成果展示',
			labelAlign:'right',
			labelWidth:90,
			width: 800,
			height : 400,
			name: 'f_achievement'
		});
		var achievementForm = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '10 20 15 5',
			items : [
				me.achievementrojectid,
				f_achievement
			]
		});
		// 项目成果展示信息表单项结束


		// 项目指导老师信息表单项开始
		me.adviserprojectid = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
		});

		var f_adviser = Ext.create('system.widget.FsdUeditor',{
			fieldLabel: '指导老师',
			labelAlign:'right',
			labelWidth:90,
			width: 800,
			height : 400,
			name: 'f_adviser'
		});
		var adviserForm = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '10 20 15 5',
			items : [
				me.adviserprojectid,
				f_adviser
			]
		});
		// 项目指导老师信息表单项结束


		// 项目支撑材料信息表单项开始
		me.materialsprojectid = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
		});

		var f_materials = Ext.create('system.widget.FsdUeditor',{
			fieldLabel: '支撑材料',
			labelAlign:'right',
			labelWidth:90,
			width: 800,
			height : 400,
			name: 'f_materials'
		});
		var materialsForm = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '10 20 15 5',
			items : [
				me.materialsprojectid,
				f_materials
			]
		});
		// 项目支撑材料信息表单项结束

		var previous = Ext.create('system.widget.FsdButton', {
			name : 'btnsave',
			text : '上一步',
			iconCls : 'previousIcon',
			handler : function() {
				if(me.steps == "2"){
					me.steps = "1";

					previous.setVisible(false);
					me.removeAll(false);

					me.add(me.infoForm);
					me.resetLocation(me);

					var titles = me.title.split("-");
					me.title = titles[0] + "- 项目申报书";
					me.setTitle(me.title);

					var pram = {id : me.infoprojectid.getValue()};
					var param = {jsonData : Ext.encode(pram)};
					me.oper.util.formLoad(me.infoForm.getForm() , 'z_projectsite/load_z_projectsitebyid.do', param, null,
						function(){
						});
				}else if(me.steps == "3"){
					me.steps = "2";

					me.removeAll(false);
					me.add(abstractForm);

					me.resetLocation(me);
					var titles = me.title.split("-");
					me.title = titles[0] + "- 项目简介";
					me.setTitle(me.title);

					var pram = {id : me.abstractprojectid.getValue()};
					var param = {jsonData : Ext.encode(pram)};
					me.oper.util.formLoad(abstractForm.getForm() , 'z_projectsite/load_z_projectsitebyid.do', param, null,
						function(){
						});
				}else if(me.steps == "4"){
					me.steps = "3";

					me.removeAll(false);
					me.add(teammienForm);

					me.resetLocation(me);

					var titles = me.title.split("-");
					me.title = titles[0] + "- 团队风采";
					me.setTitle(me.title);

					var pram = {id : me.teammienprojectid.getValue()};
					var param = {jsonData : Ext.encode(pram)};
					me.oper.util.formLoad(teammienForm.getForm() , 'z_projectsite/load_z_projectsitebyid.do', param, null,
						function(){
						});
				}else if(me.steps == "5"){
					me.steps = "4";

					me.removeAll(false);
					me.add(achievementForm);

					me.resetLocation(me);
					var titles = me.title.split("-");
					me.title = titles[0] + "- 成果展示";
					me.setTitle(me.title);

					var pram = {id : me.achievementrojectid.getValue()};
					var param = {jsonData : Ext.encode(pram)};
					me.oper.util.formLoad(achievementForm.getForm() , 'z_projectsite/load_z_projectsitebyid.do', param, null,
						function(){

						});
				}else if(me.steps == "6"){
					me.steps = "5";

					me.removeAll(false);
					me.add(adviserForm);

					me.resetLocation(me);
					var titles = me.title.split("-");
					me.title = titles[0] + "- 指导老师信息";
					me.setTitle(me.title);

					next.setText("下一步");

					var param = {jsonData : Ext.encode(pram)};
					me.oper.util.formLoad(adviserForm.getForm() , 'z_projectsite/load_z_projectsitebyid.do', param, null,
						function(){
						});
				}
			}
		});
		previous.setVisible(false);

		var next = Ext.create('system.widget.FsdButton', {
			name : 'btnsave',
			text : '下一步',
			iconCls : 'nextIcon',
			handler : function() {
				if(me.steps == "1"){
					me.oper.infoFormSubmit(me.infoForm,function(form, action, respText){
						me.infoprojectid.setValue(respText.data.id);
						me.abstractprojectid.setValue(respText.data.id);
						me.teammienprojectid.setValue(respText.data.id);
						me.achievementrojectid.setValue(respText.data.id);
						me.adviserprojectid.setValue(respText.data.id);
						me.materialsprojectid.setValue(respText.data.id);

						if(me.grid != null){
							me.grid.getStore().reload();
						}
						me.removeAll(false);

						var pram = {id : me.infoprojectid.getValue()};
						var param = {jsonData : Ext.encode(pram)};
						me.oper.util.formLoad(abstractForm.getForm() , 'z_projectsite/load_z_projectsitebyid.do', param, null,
							function(){

							});
						me.add(abstractForm);
						me.resetLocation(me);
						me.steps = "2";
						var titles = me.title.split("-");
						me.title = titles[0] + "- 项目简介";
						me.setTitle(me.title);
						previous.setVisible(true);
					});
				}else if(me.steps == "2"){
					me.oper.abstractFormSubmit(abstractForm,function(form, action, respText){
						if(me.grid != null){
							me.grid.getStore().reload();
						}
						me.removeAll(false);

						var pram = {id : me.infoprojectid.getValue()};
						var param = {jsonData : Ext.encode(pram)};
						me.oper.util.formLoad(teammienForm.getForm() , 'z_projectsite/load_z_projectsitebyid.do', param, null,
							function(){

							});
						me.add(teammienForm);
						me.resetLocation(me);
						me.steps = "3";
						var titles = me.title.split("-");
						me.title = titles[0] + "- 团队风采";
						me.setTitle(me.title);
						previous.setVisible(true);
					});
				}else if(me.steps == "3"){
					me.oper.teammienFormSubmit(teammienForm,function(form, action, respText){
						if(me.grid != null){
							me.grid.getStore().reload();
						}
						me.removeAll(false);

						var pram = {id : me.infoprojectid.getValue()};
						var param = {jsonData : Ext.encode(pram)};
						me.oper.util.formLoad(achievementForm.getForm() , 'z_projectsite/load_z_projectsitebyid.do', param, null,
							function(){

							});
						me.add(achievementForm);
						me.resetLocation(me);
						me.steps = "4";
						var titles = me.title.split("-");
						me.title = titles[0] + "- 成果展示";
						me.setTitle(me.title);
						previous.setVisible(true);
					});
				}else if(me.steps == "4"){
					me.oper.achievementFormSubmit(achievementForm,function(form, action, respText){
						me.removeAll(false);
						var pram = {id : me.infoprojectid.getValue()};
						var param = {jsonData : Ext.encode(pram)};
						me.oper.util.formLoad(adviserForm.getForm() , 'z_projectsite/load_z_projectsitebyid.do', param, null,
							function(){

							});
						me.add(adviserForm);
						me.resetLocation(me);
						me.steps = "5";
						var titles = me.title.split("-");
						me.title = titles[0] + "- 指导老师信息";
						me.setTitle(me.title);
						previous.setVisible(true);
					});
				}else if(me.steps == "5"){
					me.oper.adviserFormSubmit(adviserForm,function(form, action, respText){
						me.removeAll(false);
						var pram = {id : me.infoprojectid.getValue()};
						var param = {jsonData : Ext.encode(pram)};
						me.oper.util.formLoad(materialsForm.getForm() , 'z_projectsite/load_z_projectsitebyid.do', param, null,
							function(){

							});
						me.add(materialsForm);
						me.resetLocation(me);
						me.steps = "6";
						var titles = me.title.split("-");
						me.title = titles[0] + "- 支撑材料";
						me.setTitle(me.title);
						previous.setVisible(true);
					});
					next.setText("提交");
				}else if(me.steps == "6"){
					me.oper.materialsFormSubmit(materialsForm,function(form, action, respText){
						Ext.MessageBox.alert('提示', '项目提交成功！');
						if(me.grid != null){
							me.grid.getStore().reload();
						}
						me.close();
					});
				}
			}
		});

		Ext.apply(this,{
			layout : 'fit',
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			items :[me.infoForm],
			buttons : [previous, next, {
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	},

	/**
	 * 设置父级编码
	 * @param fid
	 */
	setFid: function(fid){
		this.f_fid.setValue(fid);
	},

	/**
	 * 重置窗口的位置
	 * @param window
	 */
	resetLocation: function (window) {
		var docHeight = document.body.clientHeight;
		var docWidth = document.body.clientWidth;
		var winHeight = window.getHeight();
		var winWidth = window.getWidth();

		var y = (docHeight - winHeight)/2;
		var x = (docWidth - winWidth)/2;
		window.setPosition(x, y);
	}
});