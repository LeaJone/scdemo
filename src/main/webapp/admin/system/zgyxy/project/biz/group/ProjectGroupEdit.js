﻿/**
 * 项目分组添加界面
 * @author lumingbao
 */
Ext.define(	'system.zgyxy.project.biz.group.ProjectGroupEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.zgyxy.project.biz.group.ProjectGroupOper'),
	initComponent:function(){
		var me = this;

		me.f_id = Ext.create('Ext.form.field.Hidden',{
			name: 'id'
		});

		var f_name = Ext.create('system.widget.FsdTextField',{
			fieldLabel: '分组名称',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name: 'f_name',
			maxLength: 500,
			allowBlank: false
		});

		var f_describe = Ext.create('system.widget.FsdTextArea',{
			name : 'f_describe',
			fieldLabel : '分组描述',
			labelAlign :'right',
			labelWidth : 90,
			width : 450,
			allowBlank : false,
			maxLength : 500,
			blankText : '请输入资料描述',
			maxLengthText : '资料描述不能超过200个字符'
		});
		var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 20 15 5',
			items : [
				me.f_id,
				f_name,
				f_describe
			]
		});

		Ext.apply(this,{
			layout : 'fit',
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			items :[form],
			buttons : [{
				name : 'btnsave',
				text : '保存',
				iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
				handler : function() {
					me.oper.groupFormSubmit(form, me.grid);
				}
			}, {
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	}
});