﻿/**
 * 项目变更管理界面
 * @author lumingbao
 */
Ext.define('system.zgyxy.project.biz.modify.ModifyManage', {
	extend : 'Ext.window.Window',
	requires : [ 'system.zgyxy.project.model.modify.Extjs_Column_Modify',
	             'system.zgyxy.project.model.modify.Extjs_Model_Modify'],
	oper : Ext.create('system.zgyxy.project.biz.modify.ProjectModifyOper'),
	height:400,
	width:700,
	project : '',
	initComponent : function() {
		var me = this;

		// 创建表格
		me.grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_modify',
			model : 'model_z_modify',
			baseUrl : 'z_projectmodify/load_pagedata.do',
			border : false,
			zAutoLoad : false,
			tbar : [{
				xtype : "fsdbutton",
				text : "详情",
				iconCls : 'add',
				handler : function() {
					me.oper.view(me.grid);
				}
			},  '->', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]
		});
		me.grid.getStore().on('beforeload', function(s) {
			var pram = {projectid : me.project};
			var params = s.getProxy().extraParams;
			Ext.apply(params,{jsonData : Ext.encode(pram)});
		});

		Ext.apply(this,{
			layout : 'fit',
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			items :[me.grid],
			buttons : [{
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	},
});