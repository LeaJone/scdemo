﻿/**
 * 报账管理界面
 * @author lumingbao
 */
Ext.define('system.zgyxy.project.biz.money.BillManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.zgyxy.project.model.money.Extjs_Column_Money',
		'system.zgyxy.project.model.money.Extjs_Model_Money'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},

    oper : Ext.create('system.zgyxy.project.biz.money.ProjectMoneyOper'),

	createTable : function() {
	    var me = this;

        /**
         * 查询文本框
         */
        var queryText =  Ext.create("Ext.form.TextField" , {
            name : 'queryParam',
            emptyText : '请输入项目名称',
            enableKeyEvents : true,
            width : 130,
            listeners : {
                specialkey : function(field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        grid.getStore().reload();
                    }
                }
            }
        });
	    
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_money',
			model : 'model_z_money',
			baseUrl : 'z_projectbill/load_pagedata1.do',
			border : false,
			tbar : [{
				text : '查看报账信息',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'rybj',
				text : "同意报账",
				iconCls : 'tickIcon',
				handler : function(button) {
					me.oper.isEnabled(grid, true);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'rybj',
				text : "拒绝报账",
				iconCls : 'crossIcon',
				handler : function(button) {
					me.oper.isEnabled(grid, false);
				}
			},  '->', queryText, {
                text: '查询',
                iconCls: 'magnifierIcon',
                handler: function () {
                    grid.getStore().reload();
                }
            }, {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]
		});
		grid.getStore().on('beforeload', function(s) {
            var text =  queryText.getValue();
            var pram = {name : text};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		return grid;
	}
});