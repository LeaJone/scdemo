﻿/**
 * 项目路演录分管理界面
 * @author lumingbao
 */
Ext.define('system.zgyxy.project.biz.luyan.ProjectLuyanManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.zgyxy.project.model.luyan.Extjs_Column_ProjectLuyan',
	             'system.zgyxy.project.model.luyan.Extjs_Model_ProjectLuyan'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.zgyxy.project.biz.ProjectOper'),
    
	createTable : function() {
	    var me = this;

        /**
         * 查询文本框
         */
        var queryText =  Ext.create("Ext.form.TextField" , {
            name : 'queryParam',
            emptyText : '请输入项目名称',
            enableKeyEvents : true,
            width : 130,
            listeners : {
                specialkey : function(field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        grid.getStore().reload();
                    }
                }
            }
        });
	    
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_projectluyan',
			model : 'model_z_projectluyan',
			baseUrl : 'z_project/load_pagedata.do',
			border : false,
			tbar : [{
                xtype : "fsdbutton",
                text : "路演录分",
                iconCls : 'tickIcon',
                handler : function() {
                    me.oper.luyanlufen(grid);
                }
            },{
				xtype : "fsdbutton",
				text : "批量导入",
				iconCls : 'office_excelIcon',
				handler : function() {
					me.oper.luyanlufenImport(grid);
				}
			},  '->', queryText, {
                text: '查询',
                iconCls: 'magnifierIcon',
                handler: function () {
                    grid.getStore().reload();
                }
            }, {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]
		});
		grid.getStore().on('beforeload', function(s) {
            var text =  queryText.getValue();
            var pram = {name : text};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		return grid;
	}
});