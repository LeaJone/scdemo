﻿Ext.define('column_z_modify', {
    columns: [
        {
            xtype: 'rownumberer',
            text: '',
            sortable: true,
            width: 40,
            align: 'left'
        }
        ,
        {
            header: '项目名称',
            dataIndex: 'f_projectname',
            sortable: true,
            align: 'center',
            flex: 2
        }
        ,
        {
            header: '变更事项',
            dataIndex: 'f_modifytypename',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '提交时间',
            dataIndex: 'f_adddate',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                return system.UtilStatic.formatDateTime(value);
            }
        },{
            xtype: 'gridcolumn',
            width: 120,
            dataIndex: 'operate',
            text: '操作项',
            align: 'center',
            renderer: function (value, metadata, record, rowIndex, columnIndex, store) {
                var id = metadata.record.id;

                Ext.defer(function () {
                    var buttonGroup = Ext.widget('buttongroup', {
                        renderTo: id,
                        style: 'margin:0',
                        ui: 'footer'
                    });

                    var viewApplyBookButton = Ext.create('Ext.button.Button', {
                        height: 19,
                        text: "<span style='color:#ffffff;font-weight: normal;line-height: 15px;'>变更申请表</span>",
                        style: 'background:#157fcc;border:none;margin-right:5px;',
                        iconCls: 'fa fa-edit gridMenuIcon',
                        handler: function () {
                            //oper.download(record.get('f_pdf'));
                            var win = Ext.create("system.zgyxy.project.biz.apply.ProjectModifyBookView");
                            win.modal = true;
                            win.setProjectId(record.get('id'), record.get('f_projectid'));
                            win.show();
                        }
                    });
                    buttonGroup.add(viewApplyBookButton);

                }, 50);
                return Ext.String.format('<div id="{0}"></div>', id);
            }
        }
    ]
});

Ext.define('column_z_projectprospectus', {
    columns: [
        {
            xtype: 'rownumberer',
            text: '',
            sortable: true,
            width: 40,
            align: 'left'
        }
        ,
        {
            header: '项目编号',
            dataIndex: 'f_code',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '项目名称',
            dataIndex: 'f_name',
            sortable: true,
            align: 'center',
            flex: 2
        }
        ,
        {
            header: '项目类型',
            dataIndex: 'f_typename',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '指导老师',
            dataIndex: 'f_teachername',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '项目负责人',
            dataIndex: 'f_leadername',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '发布时间',
            dataIndex: 'f_adddate',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                return system.UtilStatic.formatDateTime(value);
            }
        }
    ]
});
