﻿Ext.define('column_z_projectmiddlenext', {
    columns: [
        {
            xtype: 'rownumberer',
            text: '',
            sortable: true,
            width: 40,
            align: 'left'
        }
        ,
        {
            header: '项目名称',
            dataIndex: 'f_projectname',
            sortable: true,
            align: 'center',
            flex: 2
        }
        ,
        {
            header: '开始时间',
            dataIndex: 'f_starttime',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                return system.UtilStatic.formatDateTime(value);
            }
        }
        ,
        {
            header: '结束时间',
            dataIndex: 'f_stoptime',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                return system.UtilStatic.formatDateTime(value);
            }
        }
        ,
        {
            header: '提交时间',
            dataIndex: 'f_adddate',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                return system.UtilStatic.formatDateTime(value);
            }
        }
    ]
});
