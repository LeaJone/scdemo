﻿Ext.define('column_z_projectmember', {
    columns: [
        {
            xtype: 'rownumberer',
            text: '',
            sortable: true,
            width: 40,
            align: 'left'
        }
        ,
        {
            header: '学号',
            dataIndex: 'f_studentnumber',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '姓名',
            dataIndex: 'f_membername',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '所在学院',
            dataIndex: 'f_membercollege',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '年级',
            dataIndex: 'f_memberclass',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '联系电话',
            dataIndex: 'f_memberphone',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '项目中的分工',
            dataIndex: 'f_memberresponsibility',
            sortable: true,
            align: 'center',
            flex: 2
        }
    ]
});