﻿Ext.define('model_z_projectmaterial', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'id',
            type: 'string'
        }
        ,
        {
            name: 'f_projectid',
            type: 'string'
        }
        ,
        {
            name: 'f_projectname',
            type: 'string'
        }
        ,
        {
            name: 'f_url',
            type: 'string'
        }
        ,
        {
            name: 'f_describe',
            type: 'string'
        }
        ,
        {
            name: 'f_adddate',
            type: 'string'
        }
    ]
});
