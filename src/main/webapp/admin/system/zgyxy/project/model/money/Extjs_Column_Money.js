﻿Ext.define('column_z_money', {
    columns: [
        {
            xtype: 'rownumberer',
            text: '',
            sortable: true,
            width: 40,
            align: 'left'
        }
        ,
        {
            header: '项目名称',
            dataIndex: 'f_projectname',
            sortable: true,
            align: 'center',
            flex: 2
        }
        ,
        {
            header: '报账类别',
            dataIndex: 'f_name',
            sortable: true,
            align: 'center',
            flex: 2
        }
        ,
        {
            header: '报账金额',
            dataIndex: 'f_money',
            sortable: true,
            align: 'center',
            flex: 2
        }
        ,
        {
            header: '费用明细',
            dataIndex: 'f_detail',
            sortable: true,
            align: 'center',
            flex: 2
        }
        ,
        {
            header: '状态',
            dataIndex: 'f_state',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                if(value == "0"){
                    return "<span style='color: red;'>待审核</span>";
                }else{
                    return "<span style='color: green;'>已审核</span>";
                }
            }
        }, {
            xtype: 'gridcolumn',
            width: 150,
            dataIndex: 'operate',
            text: '操作项',
            align: 'center',
            renderer: function (value, metadata, record, rowIndex, columnIndex, store) {
                var oper = Ext.create('system.zgyxy.project.biz.money.ProjectMoneyOper');
                var id = metadata.record.id;
                Ext.defer(function () {
                    Ext.widget('buttongroup', {
                        renderTo: id,
                        style: 'margin:0',
                        ui: 'footer',
                        items: [{
                            xtype: 'button',
                            height: 19,
                            text: "<span style='color:#ffffff;font-weight: normal;line-height: 15px;'>下载报账凭据</span>",
                            style: 'background:#157fcc;border:none;margin-right:5px;',
                            iconCls: 'fa fa-edit gridMenuIcon',
                            handler: function () {
                                oper.download(record.get('f_url'));
                            }
                        }
                        ]
                    });
                }, 50);
                return Ext.String.format('<div id="{0}"></div>', id);
            }
        }
    ]
});