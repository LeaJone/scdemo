﻿Ext.define('column_z_project', {
    columns: [
        {
            xtype: 'rownumberer',
            text: '',
            sortable: true,
            width: 40,
            align: 'left'
        }
        ,
        {
            header: '项目编号',
            dataIndex: 'f_code',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '项目名称',
            dataIndex: 'f_name',
            sortable: true,
            align: 'center',
            flex: 2
        }
        ,
        {
            header: '项目类型',
            dataIndex: 'f_typename',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '指导老师',
            dataIndex: 'f_teachername',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '项目负责人',
            dataIndex: 'f_leadername',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '项目状态',
            dataIndex: 'f_statusname',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                if(value == "草稿"){
                    return "<span style='color: green;'>"+ value +"</span>";
                }else if(value == "提交待审核"){
                    return "<span style='color: dodgerblue;'>"+ value +"</span>";
                }else if(value == "项目终止"){
                    return "<span style='color: red;'>"+ value +"</span>";
                }else if(value == "提交已审核"){
                    return "<span style='color: blue;'>"+ value +"</span>";
                }
                return value;
            }
        }
        ,
        {
            header: '项目分组',
            dataIndex: 'f_classifyname',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                if(value == ""){
                    return "未分组";
                }
                return value;
            }
        }
        ,
        {
            xtype: 'gridcolumn',
            width: 100,
            dataIndex: 'operate',
            text: '项目级别',
            align: 'center',
            renderer: function (value, metadata, record, rowIndex, columnIndex, store) {
                var xj = record.get('f_isschool');
                var sj = record.get('f_isprovince');
                var gjj = record.get('f_iscountry');
                var xjtype = record.get('f_schooltypename');
                if(gjj != null && gjj == "true"){
                    return "<span style='color:green;'>国家级</span>";
                }
                if(sj != null && sj == "true"){
                    return "<span style='color:green;'>省级</span>";
                }
                if(xj != null && xj == "true"){
                    return "<span style='color:green;'>校级("+ xjtype +")</span>";
                }
            }
        }
        ,
        {
            xtype: 'gridcolumn',
            width: 100,
            dataIndex: 'f_ismiddletypename',
            text: '中期检查结果',
            align: 'center',
            renderer: function (value, metadata, record, rowIndex, columnIndex, store) {
                var ismiddle = record.get('f_ismiddle');
                if(ismiddle == "true"){
                    return value;
                }else{
                    return "未提交中期检查";
                }
            }
        }
        ,
        {
            xtype: 'gridcolumn',
            width: 100,
            dataIndex: 'f_isending',
            text: '结题结果',
            align: 'center',
            renderer: function (value, metadata, record, rowIndex, columnIndex, store) {
                if(value == "true"){
                    var f_endingmbname = record.get('f_endingmbname');
                    var f_endingcjpdname = record.get('f_endingcjpdname');
                    return f_endingmbname + "，" + f_endingcjpdname;
                }else{
                    return "项目未结题";
                }
            }
        }
        ,
        {
            header: '提交时间',
            dataIndex: 'f_adddate',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                return system.UtilStatic.formatDateTime(value);
            }
        },{
            xtype: 'gridcolumn',
            width: 490,
            dataIndex: 'operate',
            text: '操作项',
            align: 'center',
            renderer: function (value, metadata, record, rowIndex, columnIndex, store) {
                var oper = Ext.create('system.zgyxy.project.biz.ProjectOper');
                var id = metadata.record.id;
                var ismiddle = record.get("f_ismiddle");
                var isending = record.get("f_isending");
                Ext.defer(function () {
                    var buttonGroup = Ext.widget('buttongroup', {
                        renderTo: id,
                        style: 'margin:0',
                        ui: 'footer'
                    });

                    var viewApplyBookButton = Ext.create('Ext.button.Button', {
                        height: 19,
                        text: "<span style='color:#ffffff;font-weight: normal;line-height: 15px;'>申报书</span>",
                        style: 'background:#157fcc;border:none;margin-right:5px;',
                        iconCls: 'fa fa-edit gridMenuIcon',
                        handler: function () {
                            //oper.download(record.get('f_pdf'));
                            var win = Ext.create("system.zgyxy.project.biz.apply.ProjectApplyBookView");
                            win.setProjectId(record.get('id'));
                            win.show();
                        }
                    });
                    buttonGroup.add(viewApplyBookButton);

                    if(ismiddle != null && ismiddle == "true"){
                        var viewMiddleBookButton = Ext.create('Ext.button.Button', {
                            height: 19,
                            text: "<span style='color:#ffffff;font-weight: normal;line-height: 15px;'>中期检查表</span>",
                            style: 'background:#157fcc;border:none;margin-right:5px;',
                            iconCls: 'fa fa-edit gridMenuIcon',
                            handler: function () {
                                var win = Ext.create("system.zgyxy.project.biz.apply.ProjectMiddleBookView");
                                win.setProjectId(record.get('id'));
                                win.show();
                            }
                        });
                        buttonGroup.add(viewMiddleBookButton);
                    }

                    if(isending != null && isending == "true"){
                        var viewMiddleBookButton = Ext.create('Ext.button.Button', {
                            height: 19,
                            text: "<span style='color:#ffffff;font-weight: normal;line-height: 15px;'>项目结项表</span>",
                            style: 'background:#157fcc;border:none;margin-right:5px;',
                            iconCls: 'fa fa-edit gridMenuIcon',
                            handler: function () {
                                var win = Ext.create("system.zgyxy.project.biz.apply.ProjectEndingBookView");
                                win.setProjectId(record.get('id'));
                                win.show();
                            }
                        });
                        buttonGroup.add(viewMiddleBookButton);
                    }

                    var viewModifyButton = Ext.create('Ext.button.Button', {
                        height: 19,
                        text: "<span style='color:#ffffff;font-weight: normal;line-height: 15px;'>变更记录</span>",
                        style: 'background:#157fcc;border:none;margin-right:5px;',
                        iconCls: 'fa fa-edit gridMenuIcon',
                        handler: function () {
                            var win = Ext.create("system.zgyxy.project.biz.modify.ModifyManage");
                            win.setTitle('项目变更记录');
                            win.project = record.get('id');
                            win.show();
                            win.grid.getStore().reload();
                        }
                    });
                    buttonGroup.add(viewModifyButton);

                    var viewApplySourceButton = Ext.create('Ext.button.Button', {
                        height: 19,
                        text: "<span style='color:#ffffff;font-weight: normal;line-height: 15px;'>项目资料库</span>",
                        style: 'background:#157fcc;border:none;margin-right:5px;',
                        iconCls: 'fa fa-cloud-upload gridMenuIcon',
                        handler: function () {
                            var win = Ext.create("system.zgyxy.project.biz.material.ProjectMaterialManage");
                            win.setTitle('项目资料库');
                            win.modal = true;
                            win.setProjectId(record.get('id'));
                            win.show();
                        }
                    });
                    buttonGroup.add(viewApplySourceButton);
                }, 50);
                return Ext.String.format('<div id="{0}"></div>', id);
            }
        }
    ]
});

Ext.define('column_z_projectprospectus', {
    columns: [
        {
            xtype: 'rownumberer',
            text: '',
            sortable: true,
            width: 40,
            align: 'left'
        }
        ,
        {
            header: '项目编号',
            dataIndex: 'f_code',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '项目名称',
            dataIndex: 'f_name',
            sortable: true,
            align: 'center',
            flex: 2
        }
        ,
        {
            header: '项目类型',
            dataIndex: 'f_typename',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '指导老师',
            dataIndex: 'f_teachername',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '项目负责人',
            dataIndex: 'f_leadername',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '发布时间',
            dataIndex: 'f_adddate',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                return system.UtilStatic.formatDateTime(value);
            }
        }
    ]
});
