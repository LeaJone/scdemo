﻿Ext.define('model_z_group', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'id',
            type: 'string'
        }
        ,
        {
            name: 'f_name',
            type: 'string'
        }
        ,
        {
            name: 'f_describe',
            type: 'string'
        }
        ,
        {
            name: 'f_adddate',
            type: 'string'
        }
    ]
});
