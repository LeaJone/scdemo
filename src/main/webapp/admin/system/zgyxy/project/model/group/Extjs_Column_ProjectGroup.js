﻿Ext.define('column_z_projectgroup', {
    columns: [
        {
            xtype: 'rownumberer',
            text: '',
            sortable: true,
            width: 40,
            align: 'left'
        }
        ,
        {
            header: '项目编号',
            dataIndex: 'f_code',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '项目名称',
            dataIndex: 'f_name',
            sortable: true,
            align: 'center',
            flex: 2
        }
        ,
        {
            header: '项目类型',
            dataIndex: 'f_typename',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '指导老师',
            dataIndex: 'f_teachername',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '项目负责人',
            dataIndex: 'f_leadername',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '项目状态',
            dataIndex: 'f_statusname',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                if(value == "草稿"){
                    return "<span style='color: green;'>"+ value +"</span>";
                }else if(value == "提交待审核"){
                    return "<span style='color: dodgerblue;'>"+ value +"</span>";
                }else if(value == "项目终止"){
                    return "<span style='color: red;'>"+ value +"</span>";
                }else if(value == "提交已审核"){
                    return "<span style='color: blue;'>"+ value +"</span>";
                }
                return value;
            }
        }
        ,
        {
            header: '项目分组',
            dataIndex: 'f_classifyname',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                if(value == ""){
                    return "未分组";
                }
                return value;
            }
        }
        ,
        {
            xtype: 'gridcolumn',
            width: 100,
            dataIndex: 'operate',
            text: '项目级别',
            align: 'center',
            renderer: function (value, metadata, record, rowIndex, columnIndex, store) {
                var xj = record.get('f_isschool');
                var sj = record.get('f_isprovince');
                var gjj = record.get('f_iscountry');
                if(gjj != null && gjj == "true"){
                    return "<span style='color:green;'>国家级</span>";
                }
                if(sj != null && sj == "true"){
                    return "<span style='color:green;'>省级</span>";
                }
                if(gjj != null && gjj == "true"){
                    return "<span style='color:green;'>国家级</span>";
                }
            }
        }
        ,
        {
            header: '网评得分',
            dataIndex: 'f_netscore',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                if(value == null || value == ""){
                    return "暂无评分";
                }
            }
        }
        ,
        {
            header: '提交时间',
            dataIndex: 'f_adddate',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                return system.UtilStatic.formatDateTime(value);
            }
        }
    ]
});