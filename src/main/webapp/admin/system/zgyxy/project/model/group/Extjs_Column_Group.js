﻿Ext.define('column_z_group', {
    columns: [
        {
            xtype: 'rownumberer',
            text: '',
            sortable: true,
            width: 40,
            align: 'left'
        }
        ,
        {
            header: '分组名称',
            dataIndex: 'f_name',
            sortable: true,
            align: 'center',
            flex: 2
        }
    ]
});