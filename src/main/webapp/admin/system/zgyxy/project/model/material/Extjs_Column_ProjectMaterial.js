﻿Ext.define('column_z_projectmaterial', {
    columns: [
        {
            xtype: 'rownumberer',
            text: '',
            sortable: true,
            width: 40,
            align: 'left'
        }
        ,
        {
            header: '项目名称',
            dataIndex: 'f_projectname',
            sortable: true,
            align: 'center',
            flex: 2
        }
        ,
        {
            header: '资料描述',
            dataIndex: 'f_describe',
            sortable: true,
            align: 'center',
            flex: 2
        }, {
            xtype: 'gridcolumn',
            width: 150,
            dataIndex: 'operate',
            text: '操作项',
            align: 'center',
            renderer: function (value, metadata, record, rowIndex, columnIndex, store) {
                var oper = Ext.create('system.zgyxy.project.biz.material.ProjectMaterialOper');
                var id = metadata.record.id;
                Ext.defer(function () {
                    Ext.widget('buttongroup', {
                        renderTo: id,
                        style: 'margin:0',
                        ui: 'footer',
                        items: [{
                            xtype: 'button',
                            height: 19,
                            text: "<span style='color:#ffffff;font-weight: normal;line-height: 15px;'>下载资料附件</span>",
                            style: 'background:#157fcc;border:none;margin-right:5px;',
                            iconCls: 'fa fa-edit gridMenuIcon',
                            handler: function () {
                                oper.download(record.get('f_url'));
                            }
                        }
                        ]
                    });
                }, 50);
                return Ext.String.format('<div id="{0}"></div>', id);
            }
        }
    ]
});