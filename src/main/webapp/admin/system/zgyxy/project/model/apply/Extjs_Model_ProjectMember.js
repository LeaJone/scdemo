﻿Ext.define('model_z_projectmember', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'id',
            type: 'string'
        }
        ,
        {
            name: 'f_projectid',
            type: 'string'
        }
        ,
        {
            name: 'f_memberid',
            type: 'string'
        }
        ,
        {
            name: 'f_studentnumber',
            type: 'string'
        }
        ,
        {
            name: 'f_membername',
            type: 'string'
        }
        ,
        {
            name: 'f_memberphone',
            type: 'string'
        }
        ,
        {
            name: 'f_memberclass',
            type: 'string'
        }
        ,
        {
            name: 'f_membercollege',
            type: 'string'
        }
        ,
        {
            name: 'f_memberresponsibility',
            type: 'string'
        }
    ]
});
