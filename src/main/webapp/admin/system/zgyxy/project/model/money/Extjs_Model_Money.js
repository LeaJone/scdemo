﻿Ext.define('model_z_money', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'id',
            type: 'string'
        }
        ,
        {
            name: 'f_projectid',
            type: 'string'
        }
        ,
        {
            name: 'f_projectname',
            type: 'string'
        }
        ,
        {
            name: 'f_name',
            type: 'string'
        }
        ,
        {
            name: 'f_money',
            type: 'string'
        }
        ,
        {
            name: 'f_detail',
            type: 'string'
        }
        ,
        {
            name: 'f_url',
            type: 'string'
        }
        ,
        {
            name: 'f_state',
            type: 'string'
        }
        ,
        {
            name: 'f_adddate',
            type: 'string'
        }
    ]
});
