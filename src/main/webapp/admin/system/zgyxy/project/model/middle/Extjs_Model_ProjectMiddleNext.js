﻿Ext.define('model_z_projectmiddlenext', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'id',
            type: 'string'
        }
        ,
        {
            name: 'f_projectid',
            type: 'string'
        }
        ,
        {
            name: 'f_projectname',
            type: 'string'
        }
        ,
        {
            name: 'f_starttime',
            type: 'string'
        }
        ,
        {
            name: 'f_stoptime',
            type: 'string'
        }
        ,
        {
            name: 'f_content',
            type: 'string'
        }
        ,
        {
            name: 'f_adddate',
            type: 'string'
        }
    ]
});
