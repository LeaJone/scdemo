﻿Ext.define('model_z_modify', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'id',
            type: 'string'
        }
        ,
        {
            name: 'f_projectid',
            type: 'string'
        }
        ,
        {
            name: 'f_projectname',
            type: 'string'
        }
        ,
        {
            name: 'f_modifytypename',
            type: 'string'
        }
        ,
        {
            name: 'f_adddate',
            type: 'string'
        }
    ]
});
