﻿Ext.define('model_z_project', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'id',
            type: 'string'
        }
        ,
        {
            name: 'f_code',
            type: 'string'
        }
        ,
        {
            name: 'f_name',
            type: 'string'
        }
        ,
        {
            name: 'f_typename',
            type: 'string'
        }
        ,
        {
            name: 'f_typeid1',
            type: 'string'
        }
        ,
        {
            name: 'f_typename1',
            type: 'string'
        }
        ,
        {
            name: 'f_typeid2',
            type: 'string'
        }
        ,
        {
            name: 'f_typename2',
            type: 'string'
        }
        ,
        {
            name: 'f_typeid3',
            type: 'string'
        }
        ,
        {
            name: 'f_typename3',
            type: 'string'
        }
        ,
        {
            name: 'f_teachername',
            type: 'string'
        }
        ,
        {
            name: 'f_leadername',
            type: 'string'
        }
        ,
        {
            name: 'f_statusname',
            type: 'string'
        }
        ,
        {
            name: 'f_adddate',
            type: 'string'
        }
        ,
        {
            name: 'f_classifyid',
            type: 'string'
        }
        ,
        {
            name: 'f_classifyname',
            type: 'string'
        }
        ,
        {
            name: 'f_pdf',
            type: 'string'
        }
        ,
        {
            name: 'f_isschool',
            type: 'string'
        }
        ,
        {
            name: 'f_isprovince',
            type: 'string'
        }
        ,
        {
            name: 'f_iscountry',
            type: 'string'
        }
        ,
        {
            name: 'f_ismiddle',
            type: 'string'
        }
        ,
        {
            name: 'f_isending',
            type: 'string'
        }
        ,
        {
            name: 'f_ismiddletypename',
            type: 'string'
        }
        ,
        {
            name: 'f_endingmbname',
            type: 'string'
        }
        ,
        {
            name: 'f_endingcjpdname',
            type: 'string'
        }
        ,
        {
            name: 'f_schooltypeid',
            type: 'string'
        }
        ,
        {
            name: 'f_schooltypename',
            type: 'string'
        }
    ]
});
