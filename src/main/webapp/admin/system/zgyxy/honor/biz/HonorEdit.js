﻿/**
 * 荣誉添加界面
 * @author lumingbao
 */
Ext.define(	'system.zgyxy.honor.biz.HonorEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.zgyxy.honor.biz.HonorOper'),
	initComponent:function(){
		var me = this;

		me.f_fid = Ext.create('Ext.form.field.Hidden',{
			name:'f_fid'
		});

		var f_name = new Ext.create('system.widget.FsdTextField',{
			fieldLabel: '荣誉名称',
			labelAlign:'right',
			labelWidth:90,
			width : 360,
			name: 'f_name',
			maxLength: 25,
			allowBlank: false
		});

		var f_organization = new Ext.create('system.widget.FsdTextField',{
			fieldLabel: '颁发机构',
			labelAlign:'right',
			labelWidth:90,
			width : 360,
			name: 'f_organization',
			maxLength: 25
		});

		var f_getthetime = new Ext.create('system.widget.FsdDateField',{
			fieldLabel: '获得时间',
			labelAlign:'right',
			labelWidth:90,
			width : 360,
			format : 'Y年',
			altFormats : 'Y',
			submitFormat : 'Y',
			name: 'f_getthetime',
			maxLength: 25
		});

		var f_imageurl = Ext.create('system.widget.FsdTextFileManage', {
			width : 360,
			zName : 'f_imageurl',
			zFieldLabel : '照片',
			zLabelWidth : 90,
			zIsShowButton : true,
			zIsUpButton : true,
			zIsReadOnly : false,
			zFileUpPath : me.oper.util.getImagePath()
		});

		var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '15 10 5 10',
			items : [{
				name: "id",
				xtype: "hidden"
			} , me.f_fid,
				f_name,
				f_organization,
				f_getthetime,
				f_imageurl]
		});

		Ext.apply(this,{
			layout : 'fit',
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			items :[form],
			buttons : [{
				name : 'btnsave',
				text : '保存',
				iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
				handler : function() {
					me.oper.formSubmit(form, me.grid);
				}
			}, {
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	},

	/**
	 * 设置父级ID
	 * @param tutorid
	 */
	setFid:function (fid) {
		this.f_fid.setValue(fid);
	}
});