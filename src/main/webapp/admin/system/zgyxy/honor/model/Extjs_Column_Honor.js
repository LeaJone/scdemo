﻿Ext.define('column_z_honor', {
    columns: [
        {
            xtype: 'rownumberer',
            text: '',
            sortable: true,
            width: 40,
            align: 'left'
        }
        ,
        {
            header: '荣誉名称',
            dataIndex: 'f_name',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '颁发机构',
            dataIndex: 'f_organization',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '获得时间',
            dataIndex: 'f_getthetime',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                return value + "年";
            }
        }
    ]
});
