﻿/**
 * 课程编辑界面
 */
Ext.define('system.zgyxy.course_my.biz.CourseUnderEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.zgyxy.course_my.biz.CourseContentOper'),
	initComponent:function(){
		var me = this;	  
	   
		var f_typeid = Ext.create('Ext.form.field.Hidden',{
			name: 'f_typeid',
			value: 'xxkc'
		});
		
		var f_typename = Ext.create('Ext.form.field.Hidden',{
			name: 'f_typename',
			value: '线下课程'
		});
		
	    var f_name = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel : '课程名称',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 340,
	    	name : 'f_name',
	    	maxLength : 50,
	    	allowBlank : false,
	    	blankText : '请输入课程名称',
	    	maxLengthText : '课程名称不能超过50个字符'
	    });
	    var f_teacher = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel : '开课教师',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 340,
	    	name : 'f_teacher',
	    	maxLength : 30,
	    	allowBlank : false,
	    	blankText : '请输入开课教师名称',
	    	maxLengthText : '开课教师名称不能超过30个字符'
	    });
	    
	    var f_score = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel : '学分',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 340,
	    	name : 'f_score',
	    	maxLength : 10,
	    	allowBlank : false,
	    	blankText : '请输入课程学分',
	    	maxLengthText : '学分不能超过10个字符'
	    });
	    var f_period = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel : '学时',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 340,
	    	name : 'f_period',
	    	maxLength : 10,
	    	allowBlank : false,
	    	blankText : '请输入课程学时',
	    	maxLengthText : '学时不能超过10个字符'
	    });
		var f_xingqi = new Ext.create('system.widget.FsdTextField',{
			fieldLabel : '星期',
			labelAlign : 'right',
			labelWidth : 80,
			width : 340,
			name : 'f_xingqi',
			maxLength : 10,
			blankText : '请输入星期',
			maxLengthText : '星期不能超过10个字符'
		});
		var f_jieci = new Ext.create('system.widget.FsdTextField',{
			fieldLabel : '节次',
			labelAlign : 'right',
			labelWidth : 80,
			width : 340,
			name : 'f_jieci',
			maxLength : 10,
			blankText : '请输入节次',
			maxLengthText : '节次不能超过10个字符'
		});
		var f_semester = new Ext.create('system.widget.FsdTextField',{
			fieldLabel : '开课学期',
			labelAlign : 'right',
			labelWidth : 80,
			width : 340,
			name : 'f_semester',
			maxLength : 10,
			blankText : '请输入开课学期',
			maxLengthText : '开课学期不能超过10个字符'
		});
	    var f_count = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel : '学生人数',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 340,
	    	name : 'f_count',
	    	maxLength : 10,
	    	allowBlank : false,
	    	blankText : '请输入上课人数',
	    	maxLengthText : '上课人数不能超过10个字符'
	    });
	    
	    var f_collegename = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel : '开课学院',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 340,
	    	name : 'f_collegename',
	    	maxLength : 50,
	    	allowBlank : true,
	    	blankText : '请输入开课学院',
	    	maxLengthText : '开课学院不能超过50个字符'
	    });
	    
	    var f_site = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel : '授课地点',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 340,
	    	name : 'f_site',
	    	maxLength : 50,
	    	allowBlank : true,
	    	blankText : '请输入授课地点',
	    	maxLengthText : '授课地点不能超过50个字符'
	    });
	    
	    var f_propertyid = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'课程类型',
            labelAlign:'right',
            labelWidth:80,
            width : 340,
            name : 'f_propertyid',//提交到后台的参数名
            key : 'sckclx',//参数
            allowBlank: true,
            hiddenName : 'f_propertyname'//提交隐藏域Name
        });
        var f_propertyname = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '课程类型',
            name: 'f_propertyname'
        });
        
	    var f_wayid = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'授课方式',
            labelAlign:'right',
            labelWidth:80,
            width : 340,
            name : 'f_wayid',//提交到后台的参数名
            key : 'scskfs',//参数
            allowBlank: true,
            hiddenName : 'f_wayname'//提交隐藏域Name
        });
        var f_wayname = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '授课方式',
            name: 'f_wayname'
        });
	    
	    var f_abstract = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel : '课程简介',
	    	labelAlign :'right',
	    	labelWidth : 80,
	        width : 340,
	        allowBlank : true,
	    	name : 'f_abstract',
	    	maxLength : 500,
	    	blankText : '请输入课程简介',
	    	maxLengthText : '课程简介不能超过500个字符'
	    });
		var f_remark = Ext.create('system.widget.FsdTextArea',{
			fieldLabel : '备注',
			labelAlign :'right',
			labelWidth : 80,
			width : 340,
			allowBlank : true,
			name : 'f_remark',
			maxLength : 500,
			blankText : '请输入备注',
			maxLengthText : '备注不能超过500个字符'
		});

		var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '15 10 5 10',
			items : [{
				name: "id",
				xtype: "hidden"
			} ,
				f_typename,f_typeid,f_name,f_teacher,f_period,f_score,f_xingqi,f_jieci,f_semester,f_count,f_collegename,f_site,f_propertyid,f_propertyname,f_wayid,f_wayname,f_abstract,f_remark]
		});

	    Ext.apply(this,{
	        width:400,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
				xtype : "fsdbutton",
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.formSubmit(form , me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});