﻿/**
 * 课程信息管理界面
 */
Ext.define('system.zgyxy.course_my.biz.CourseManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.zgyxy.course_my.model.Extjs_T_Column',
		         'system.zgyxy.course_my.model.Extjs_T_Model'],
	header : false,
	border : 0,
	layout : 'fit',
	type :'',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.zgyxy.course_my.biz.CourseContentOper'),
    
	createTable : function() {
	    var me = this;
	    /**
		 * 左边的树面板
		 */
	    var store = Ext.create('Ext.data.TreeStore', {
			root: {
				expanded: true,
				children: [ {
					id:'xskc',
					text:'线上课程',
					expanded: true
					},{
						id:'xxkc',
						text:'线下课程',
						expanded: true}
				]
			}
		});
	    
	    var treepanel = Ext.create('Ext.tree.Panel', {
	    	collapsible : true,//折叠
			title: '双创课程',
			width: 180,
			height: 250,
			rootVisible: false, //是否隐藏根节点
			region: 'west',
			store: store
		});
		
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_t_course', // 显示列
			model : 'model_t_course',
			baseUrl : 'z_course/load_pagedata_my.do',
			zAutoLoad: false,
			tbar : [{
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
				text : "申报课程",
				iconCls : 'page_addIcon',
				handler : function() {
					me.oper.add(grid, me.type);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
				text : "修改",
				iconCls : 'page_editIcon',
				handler : function() {
					me.oper.editor(grid,me.type);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'dwsc',
				text : "删除",
				iconCls : 'page_deleteIcon',
				handler : function() {
				    me.oper.del(grid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid,me.type);
				}
			},'->', new Ext.form.TextField( {
				id : 'querycourseName',
				name : 'queryParam',
				emptyText : '请输入课程名称',
				enableKeyEvents : true,
				listeners : {
					specialkey : function(field, e) {
						if (e.getKey() == Ext.EventObject.ENTER) {
							grid.getStore().reload();
						}
					}
				},
				width : 130
			}), {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  Ext.getCmp('querycourseName').getValue();
			var pram = {type : me.type ,name : name};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		grid.on('cellclick', function(thisObj, td, cellIndex, record) {
			if (cellIndex == 7){
				me.oper.download(record.get('f_pdfurl'));
			}
		});
		
		var contentpanel = Ext.create("Ext.panel.Panel", {
			frame:true,
			split: true,
			region: 'center',
			layout: 'fit',
			title: '课程信息管理',
			items: [grid]
		});

		var page1_jExtPanel1_obj = new Ext.panel.Panel({
			bodyPadding: 3,
			header : false,
			border : 0,
			split: true,
			layout: 'border',
			items: [treepanel,contentpanel]
		});
		
		treepanel.on('itemclick', function(view,record,item,index,e){
			me.type =  record.raw.id;
			grid.getStore().reload();
		});
		return page1_jExtPanel1_obj;
	}
});