﻿
/**
 *a_company Columns
 *@author CodeSystem
 */
Ext.define('column_t_course', {
    columns: [
    	{
    		xtype : 'rownumberer',
    		text : 'NO',
    		sortable : true,
    		align : 'center',
    		flex : 0.3
    	}
    	,
        {
            header : '课程名称',
            dataIndex : 'f_name',
            align : 'center',
    		flex : 1.2
        }
        ,
        {
            header : '开课老师',
            dataIndex : 'f_teacher',
            align : 'center',
    		flex : 1
        }
        ,
        {
            header : '学分',
            dataIndex : 'f_score',
            align : 'center',
    		flex : 1
        }
        ,
    	{
            header : '审核状态',
            dataIndex : 'f_statusid',
            align : 'center',
            flex : 1,
        	renderer : function(value, metaData, record) {
    			if (value == "jjsh") {
    				value = "<font style='color:red;'>拒绝审核</font>";
    			} else if(value == "tgsh"){
    				value = "<font style='color:green;'>通过审核</font>";
    			} else{
    				value = "<font style='color:blank;'>提交未审核</font>";
    			}
    			return value;
    		}
        }
    	,
        {
            header : '审核意见',
            dataIndex : 'f_reason',
            align : 'center',
    		flex : 1.2
        }
        ,
        {
            header: '操作',
            dataIndex: 'f_pdfurl',
            sortable: true,
            align: 'center',
            flex: 0.5,
            renderer :function(value ,metaData){
                if(value != null && value != ""){
                    value = "<a href='#' style='color:blue;'>下载申报书</a>";
                }else{
                    value = "<a href='#' style='color:red;'>生成申报书</a>";
                }
                return value;
            }
        }
    ]
});
