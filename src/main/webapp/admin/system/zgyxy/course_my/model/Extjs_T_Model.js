﻿/**
 * t_course Model
 * @author CodeSystem
 * 文件名     Extjs_T_Course
 * Model名    model_t_course
 * Columns名  column_t_course
 */

Ext.define('model_t_course', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'f_name',
            type : 'string'
        }
        ,
        {
            name : 'f_teacher',
            type : 'string'
        }
        ,
        {
            name : 'f_score',
            type : 'string'
        }
        ,
        {
            name : 'f_statusid',
            type : 'string'
        }
        ,
        {
            name : 'f_reason',
            type : 'string'
        }
        ,
        {
            name : 'f_pdfurl',
            type : 'string'
        }
    ]
});


