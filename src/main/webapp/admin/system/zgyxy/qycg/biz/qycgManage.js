﻿/**
 * 企业成果管理界面
 */
Ext.define('system.zgyxy.qycg.biz.qycgManage', {
	extend : 'Ext.window.Window',
    border : false,
    qyid : '',
    qymc : '',
	oper : Ext.create('system.zgyxy.qycg.biz.qycgOper'),

    initComponent : function(){
		var me = this;

        var qyidtemp = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '企业编号'
        });

        var qymctemp = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '企业名称'
        });

        me.on('boxready', function(){
            qyidtemp.setValue(me.qyid);
            qymctemp.setValue(me.qymc);
        });

        var qycgid = me.oper.util.getParams("qycg_id");
        var qycgmc = me.oper.util.getParams("qycg_mc");

	    //企业成果
	    var qycgform = Ext.create("system.zgyxy.qycg.biz.qycgEdit", {
	        qyid : qycgid,
            qymc : qycgmc,
            type : '成果'
		});
        me.on('boxready', function() {
            var pram = {
                qyid : qyidtemp.getValue(),
                type : "成果"
            };
            var param = {
                jsonData : Ext.encode(pram)
            };
            me.oper.util.formLoad(qycgform.form.getForm(), 'z_qycg/load_z_qycgbyid.do', param, null, function(response, options, respText) {});
        });

        //企业论文
        var qylwform = Ext.create("system.zgyxy.qycg.biz.qycgEdit", {
            qyid : qycgid,
            qymc : qycgmc,
            type : '论文'
        });
        me.on('boxready', function() {
            var pram = {
                qyid : qyidtemp.getValue(),
                type : "论文"
            };
            var param = {
                jsonData : Ext.encode(pram)
            };
            me.oper.util.formLoad(qylwform.form.getForm(), 'z_qycg/load_z_qycgbyid.do', param, null, function(response, options, respText) {});
        });

        //企业专利
        var qyzlform = Ext.create("system.zgyxy.qycg.biz.qycgEdit", {
            qyid : qycgid,
            qymc : qycgmc,
            type : '专利'
        });
        me.on('boxready', function() {
            var pram = {
                qyid : qyidtemp.getValue(),
                type : "专利"
            };
            var param = {
                jsonData : Ext.encode(pram)
            };
            me.oper.util.formLoad(qyzlform.form.getForm(), 'z_qycg/load_z_qycgbyid.do', param, null, function(response, options, respText) {});
        });

        //企业著作权
        var qyzzqform = Ext.create("system.zgyxy.qycg.biz.qycgEdit", {
            qyid : qycgid,
            qymc : qycgmc,
            type : '著作权'
        });
        me.on('boxready', function() {
            var pram = {
                qyid : qyidtemp.getValue(),
                type : "著作权"
            };
            var param = {
                jsonData : Ext.encode(pram)
            };
            me.oper.util.formLoad(qyzzqform.form.getForm(), 'z_qycg/load_z_qycgbyid.do', param, null, function(response, options, respText) {});
        });

        //人才培养
        var qyrcpyform = Ext.create("system.zgyxy.qycg.biz.qycgEdit", {
            qyid : qycgid,
            qymc : qycgmc,
            type : '人才培养'
        });
        me.on('boxready', function() {
            var pram = {
                qyid : qyidtemp.getValue(),
                type : "人才培养"
            };
            var param = {
                jsonData : Ext.encode(pram)
            };
            me.oper.util.formLoad(qyrcpyform.form.getForm(), 'z_qycg/load_z_qycgbyid.do', param, null, function(response, options, respText) {});
        });

        // 项目
        var qyxmform = Ext.create("system.zgyxy.qycg.biz.qycgEdit", {
            qyid : qycgid,
            qymc : qycgmc,
            type : '项目'
        });
        me.on('boxready', function() {
            var pram = {
                qyid : qyidtemp.getValue(),
                type : "项目"
            };
            var param = {
                jsonData : Ext.encode(pram)
            };
            me.oper.util.formLoad(qyxmform.form.getForm(), 'z_qycg/load_z_qycgbyid.do', param, null, function(response, options, respText) {});
        });

        //获奖
        var qyhjform = Ext.create("system.zgyxy.qycg.biz.qycgEdit", {
            qyid : qycgid,
            qymc : qycgmc,
            type : '获奖'
        });
        me.on('boxready', function() {
            var pram = {
                qyid : qyidtemp.getValue(),
                type : "获奖"
            };
            var param = {
                jsonData : Ext.encode(pram)
            };
            me.oper.util.formLoad(qyhjform.form.getForm(), 'z_qycg/load_z_qycgbyid.do', param, null, function(response, options, respText) {});
        });

	    /**
	     * 右下方的大选项卡控件
	     */
	    var tabPanel = new Ext.tab.Panel({
			layout : 'fit',
			activeTab: 0,
			split: true,//拖动
			deferredRender: true,//是否在显示每个标签的时候再渲染标签中的内容.默认true
			items: [
				{
					xtype: 'panel',
					layout: 'fit',
					title: '成果',
					items: [
                        qycgform
					]},{
						xtype: 'panel',
						layout: 'fit',
						title: '论文',
						items: [
                            qylwform
					]},{
						xtype: 'panel',
						layout: 'fit',
						title: '专利',
						items: [
                            qyzlform
					]},{
						xtype: 'panel',
						layout: 'fit',
						title: '著作权',
						items: [
                            qyzzqform
                    ]},{
                        xtype: 'panel',
                        layout: 'fit',
                        title: '人才培养',
                        items: [
                            qyrcpyform
                    ]},{
                        xtype: 'panel',
                        layout: 'fit',
                        title: '项目',
                        items: [
                            qyxmform
                    ]},{
                        xtype: 'panel',
                        layout: 'fit',
                        title: '获奖',
                        items: [
                            qyhjform
                    ]}
			]
		});

        var buttonItem = [{
            text : '关闭',
            iconCls : 'deleteIcon',
            handler : function() {
                me.close();
            }
        }];
        Ext.apply(this,{
            layout : 'fit',
            resizable:false,// 改变大小
            bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
            items :[tabPanel],
            buttons : buttonItem
        });
        this.callParent(arguments);
	}
});