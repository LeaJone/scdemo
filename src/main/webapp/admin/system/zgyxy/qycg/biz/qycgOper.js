/**
 * 培训班管理界面操作类
 * @author lumingbao
 */

Ext.define('system.zgyxy.qycg.biz.qycgOper', {

	form : null,
	util : Ext.create('system.util.util'),
      
	/**
     * 表单提交
     */
     formSubmit : function(formpanel){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_qycg/save_qycg_qx_qycgbj.do', formpanel.form, '正在提交数据,请稍候.....',
			function(){
				Ext.MessageBox.alert('提示', '保存成功！');
			});
        }
     }

});