﻿/**
 * 竞赛添加界面
 * @author lumingbao
 */
Ext.define('system.zgyxy.competitiondeclare.biz.CompetitiondeclareEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.zgyxy.competitiondeclare.biz.CompetitiondeclareOper'),
	initComponent:function(){
		var me = this;

		var competition =  Ext.create("system.widget.FsdComboBoxData" , {
			name : 'f_fid',
			fieldLabel : '申报项目',
			labelAlign:'right',
			labelWidth:90,
			width : 360,
			zBaseUrl : 'z_competition/load_pagedata.do',
			zValueField : 'id',
			zDisplayField : 'f_name'
		});

		var f_name = new Ext.create('system.widget.FsdTextField',{
			fieldLabel: '项目名称',
			labelAlign:'right',
			labelWidth:90,
			width : 360,
			name: 'f_name',
			maxLength: 25,
			allowBlank: false
		});

		var f_sbs = Ext.create('system.widget.FsdTextFileManage', {
			width : 360,
			zName : 'f_sbs',
			zFieldLabel : '申报书',
			zLabelWidth : 90,
			zIsShowButton : true,
			zIsUpButton : true,
			zIsReadOnly : false,
			zContentObj : me.content,
			zFileUpPath : me.oper.util.getImagePath(),
			zAllowBlank: false
		});

		var f_zipurl = Ext.create('system.widget.FsdTextFileManage', {
			width : 360,
			zName : 'f_zipurl',
			zFieldLabel : '项目资料',
			zLabelWidth : 90,
			zIsShowButton : false,
			zIsUpButton : true,
			zIsReadOnly : false,
			zContentObj : me.content,
			zFileUpPath : me.oper.util.getFilePath(),
			zAllowBlank: false
		});

		var f_abstract = new Ext.create('system.widget.FsdTextArea',{
			fieldLabel: '项目简介',
			labelAlign:'right',
			labelWidth:90,
			width : 360,
			name: 'f_abstract',
			maxLength: 1000,
			allowBlank: false
		});

		var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '15 10 5 10',
			items : [{
				name: "id",
				xtype: "hidden"
			} ,
				competition,
				f_name,
				f_sbs,
				f_zipurl,
				f_abstract]
		});

		Ext.apply(this,{
			layout : 'fit',
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			items :[form],
			buttons : [{
				name : 'btnsave',
				text : '保存',
				iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
				handler : function() {
					me.oper.formSubmit(form, me.grid);
				}
			}, {
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	}
});