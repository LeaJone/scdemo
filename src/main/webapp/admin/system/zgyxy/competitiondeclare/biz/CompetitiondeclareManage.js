﻿/**
 * 竞赛管理界面
 *
 */
Ext.define('system.zgyxy.competitiondeclare.biz.CompetitiondeclareManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*',
		'system.zgyxy.competitiondeclare.model.Extjs_Column_Competitiondeclare',
		'system.zgyxy.competitiondeclare.model.Extjs_Model_Competitiondeclare'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},

	oper : Ext.create('system.zgyxy.competitiondeclare.biz.CompetitiondeclareOper'),

	createContent : function(){
		var me = this;

		/**
		 * 查询文本框
		 */
		var queryText =  Ext.create("Ext.form.TextField", {
			name : 'queryTeamParam',
			emptyText : '请输入导师名称',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});

		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_competitiondeclare', // 显示列
			model: 'model_z_competitiondeclare',
			baseUrl : 'z_competitiondeclare/load_pagedata.do',
			tbar : [{
				xtype : "fsdbutton",
				popedomCode : 'rybj',
				text : "申报",
				iconCls : 'page_addIcon',
				handler : function() {
					me.oper.add(grid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'rybj',
				text : "修改",
				iconCls : 'page_editIcon',
				handler : function() {
					me.oper.editor(grid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid, 'ck');
				}
			},{
				xtype : "fsdbutton",
				popedomCode : 'rysc',
				text : "撤回",
				iconCls : 'page_deleteIcon',
				handler : function() {
					me.oper.del(grid);
				}
			},   '->', queryText, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  queryText.getValue();
			var pram = {name : name};
			var params = s.getProxy().extraParams;
			Ext.apply(params, {jsonData : Ext.encode(pram)});
		});

		return grid;
	}
});