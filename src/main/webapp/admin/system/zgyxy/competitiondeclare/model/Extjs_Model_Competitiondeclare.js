﻿Ext.define('model_z_competitiondeclare', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'id',
            type: 'string'
        }
        ,
        {
            name: 'f_fid',
            type: 'string'
        }
        ,
        {
            name: 'f_fname',
            type: 'string'
        }
        ,
        {
            name: 'f_sbs',
            type: 'string'
        }
        ,
        {
            name: 'f_zipurl',
            type: 'string'
        }
        ,
        {
            name: 'f_name',
            type: 'string'
        }
        ,
        {
            name: 'f_abstract',
            type: 'string'
        }
        ,
        {
            name: 'f_adddate',
            type: 'string'
        }
    ]
});
