﻿Ext.define('column_z_competitiondeclare', {
    columns: [
        {
            xtype: 'rownumberer',
            text: '',
            sortable: true,
            width: 40,
            align: 'left'
        }
        ,
        {
            header: '项目名称',
            dataIndex: 'f_name',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '竞赛名称',
            dataIndex: 'f_fname',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '是否上传申报书',
            dataIndex: 'f_sbs',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                var str = "";
               if(value != ""){
                   str = "<font style='color:green;'>是</font>";
               }else{
                   str = "<font style='color:red;'>否</font>";
               }
               return str;
            }
        }
        ,
        {
            header: '是否上传项目资料',
            dataIndex: 'f_zipurl',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                var str = "";
                if(value != ""){
                    str = "<font style='color:green;'>是</font>";
                }else{
                    str = "<font style='color:red;'>否</font>";
                }
                return str;
            }
        }
        ,
        {
            header: '添加时间',
            dataIndex: 'f_adddate',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                return system.UtilStatic.formatDateTime(value);
            }
        }
    ]
});
