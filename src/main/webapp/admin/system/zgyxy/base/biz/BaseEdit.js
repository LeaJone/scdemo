﻿/**
 * 基地添加界面
 * @author lumingbao
 */
Ext.define('system.zgyxy.base.biz.BaseEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	fid:'',
	tree : null,
	oper : Ext.create('system.zgyxy.base.biz.BaseOper'),
	honorOper : Ext.create('system.zgyxy.honor.biz.HonorOper'),
	requires : [ 'system.zgyxy.honor.model.Extjs_Model_Honor',
		'system.zgyxy.honor.model.Extjs_Column_Honor'],
	initComponent:function(){
		var me = this;

		me.f_id = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
			value: ''
		});

		var f_name = new Ext.create('system.widget.FsdTextField',{
			fieldLabel: '基地名称',
			labelAlign:'right',
			labelWidth:90,
			width : 750,
			name: 'f_name',
			maxLength: 25,
			allowBlank: false
		});

		var f_fname = Ext.create('Ext.form.field.Hidden',{
			fieldLabel: '所属空间名称',
			name: 'f_fname'
		});
		var f_fid = Ext.create('system.widget.FsdTreeComboBox',{
			fieldLabel : '所属空间',
			labelAlign:'right',
			labelWidth:90,
			rootText : '根基地',
			rootId : 'FSDMAIN',
			width : 750,
			name : 'f_fid',
			baseUrl:'z_space/load_spacheTree.do',//访问路劲
			allowBlank: false,
			rootVisible : false,
			hiddenName : 'f_fname'//隐藏域Name
		});

		var f_collegeid = new system.widget.FsdTreeComboBox({
			fieldLabel : '所在院系',
			labelAlign:'right',
			labelWidth:90,
			rootText : me.oper.util.getParams("company").name,
			rootId : me.oper.util.getParams("company").id,
			rootVisible : false,//不显示根节点
			width : 750,
			name : 'f_collegeid',
			baseUrl:'A_Branch/load_AsyncBranchTreeByPopdom.do',//访问路劲
			allowBlank: false,
			hiddenName : 'f_collegename'
		});
		var f_collegename = Ext.create('Ext.form.field.Hidden',{
			fieldLabel: '所属院系',
			name: 'f_collegename'
		});

		var f_address = new Ext.create('system.widget.FsdTextField',{
			fieldLabel: '基地地址',
			labelAlign:'right',
			labelWidth:90,
			width : 750,
			name: 'f_address',
			maxLength: 25,
			allowBlank: false
		});

		var f_imgurl = Ext.create('system.widget.FsdTextFileManage', {
			width : 750,
			zName : 'f_imgurl',
			zFieldLabel : '基地照片',
			zLabelWidth : 90,
			zIsShowButton : true,
			zIsUpButton : true,
			zIsReadOnly : false,
			zFileUpPath : me.oper.util.getImagePath(),
			zAllowBlank: false
		});

		var f_abstract = Ext.create('system.widget.FsdUeditor',{
			fieldLabel: '基地简介',
			labelAlign:'right',
			labelWidth:90,
			width : 750,
			height : 420,
			name: 'f_abstract',
			allowBlank: false
		});

		var f_projects = Ext.create('system.widget.FsdUeditor',{
			fieldLabel: '入孵项目',
			labelAlign:'right',
			labelWidth:90,
			width : 750,
			height : 420,
			name: 'f_projects',
			allowBlank: false
		});

		var f_achievement = Ext.create('system.widget.FsdUeditor',{
			fieldLabel: '创新实践成果',
			labelAlign:'right',
			labelWidth:90,
			width : 750,
			height : 420,
			name: 'f_achievement',
			allowBlank: false
		});

		var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '15 10 5 10',
			autoScroll: true,
			items : [me.f_id,
				f_name,
				f_fid,
				f_fname,
				f_collegeid,
				f_collegename,
				f_address,
				f_imgurl,
				f_abstract,
				f_projects,
				f_achievement]
		});

		// 创建表格
		me.honorGrid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_honor', // 显示列
			model: 'model_z_honor',
			baseUrl : 'z_honor/load_pagedata.do',
			border: 0,
			zAutoLoad: false,
			tbar : [{
				xtype : "fsdbutton",
				popedomCode : 'rybj',
				text : "添加",
				iconCls : 'page_addIcon',
				handler : function() {
					me.honorOper.add(me.honorGrid, me.fid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'rybj',
				text : "修改",
				iconCls : 'page_editIcon',
				handler : function() {
					me.honorOper.editor(me.honorGrid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.honorOper.view(me.honorGrid);
				}
			},{
				xtype : "fsdbutton",
				popedomCode : 'rysc',
				text : "删除",
				iconCls : 'page_deleteIcon',
				handler : function() {
					me.honorOper.del(me.honorGrid);
				}
			},   '->', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					me.honorGrid.getStore().reload();
				}
			}]
		});
		me.honorGrid.getStore().on('beforeload', function(s) {
			var pram = {fid : me.fid};
			var params = s.getProxy().extraParams;
			Ext.apply(params,{jsonData : Ext.encode(pram)});
		});

		me.honor = Ext.create('Ext.panel.Panel',{
			title: '基地荣誉',
			layout : 'fit',
			disabled : true,
			items : [me.honorGrid]
		});

		var tabpanel = Ext.create('Ext.tab.Panel', {
			border: 0,
			enableTabScroll: true,
			deferredRender: false,
			defaults: { autoScroll: true },
			items: [{
				border: 0,
				title: '基本信息',
				items: [form]
			}, me.honor],
			listeners:{
				'tabchange':function (t, n) {

				}
			}
		});

		Ext.apply(this,{
			layout : 'fit',
			width:800,
			height:530,
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			items :[form],
			buttons : [{
				name : 'btnsave',
				text : '保存',
				iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
				handler : function() {
					me.oper.formSubmit(form, me.grid, me.honor);
				}
			}, {
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	}
});