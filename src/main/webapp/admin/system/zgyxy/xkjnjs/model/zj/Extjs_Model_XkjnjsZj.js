﻿Ext.define('model_z_xkjnjszj', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'id',
            type: 'string'
        }
        ,
        {
            name: 'f_gamename',
            type: 'string'
        }
        ,
        {
            name: 'f_leadername',
            type: 'string'
        }
        ,
        {
            name: 'f_gamelevelname',
            type: 'string'
        }
        ,
        {
            name: 'f_gametypename',
            type: 'string'
        }
        ,
        {
            name: 'f_hostunit',
            type: 'string'
        }
        ,
        {
            name: 'f_profession',
            type: 'string'
        }
        ,
        {
            name: 'f_gametime',
            type: 'string'
        }
        ,
        {
            name: 'f_pdfurl',
            type: 'string'
        }
        ,
        {
            name: 'f_statusid',
            type: 'string'
        }
        ,
        {
            name: 'f_statusname',
            type: 'string'
        }
        ,
        {
            name: 'f_issummarize',
            type: 'string'
        }
    ]
});
