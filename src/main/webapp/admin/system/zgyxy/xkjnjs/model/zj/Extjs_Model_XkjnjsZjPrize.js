﻿Ext.define('model_z_xkjnjszjprize', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'id',
            type: 'string'
        }
        ,
        {
            name: 'f_name',
            type: 'string'
        }
        ,
        {
            name: 'f_typename',
            type: 'string'
        }
        ,
        {
            name: 'f_levelname',
            type: 'string'
        }
        ,
        {
            name: 'f_teacher',
            type: 'string'
        }
        ,
        {
            name: 'f_student',
            type: 'string'
        }
    ]
});
