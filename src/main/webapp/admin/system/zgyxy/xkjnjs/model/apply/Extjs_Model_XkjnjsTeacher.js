﻿Ext.define('model_z_xkjnjsteacher', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'id',
            type: 'string'
        }
        ,
        {
            name: 'f_teacherid',
            type: 'string'
        }
        ,
        {
            name: 'f_teachername',
            type: 'string'
        }
        ,
        {
            name: 'f_teachersex',
            type: 'string'
        }
        ,
        {
            name: 'f_teachertitle',
            type: 'string'
        }
        ,
        {
            name: 'f_teacherphone',
            type: 'string'
        }
        ,
        {
            name: 'f_leaderresponsibility',
            type: 'string'
        }
    ]
});
