﻿Ext.define('column_z_xkjnjszjprize', {
    columns: [
        {
            xtype: 'rownumberer',
            text: '',
            sortable: true,
            width: 40,
            align: 'left'
        }
        ,
        {
            header: '作品名称',
            dataIndex: 'f_name',
            sortable: true,
            align: 'center',
            flex: 2
        }
        ,
        {
            header: '获奖级别',
            dataIndex: 'f_typename',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '奖励等级',
            dataIndex: 'f_levelname',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '指导老师',
            dataIndex: 'f_teacher',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '参赛学生',
            dataIndex: 'f_student',
            sortable: true,
            align: 'center',
            flex: 1
        }
    ]
});