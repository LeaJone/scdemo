﻿Ext.define('column_z_xkjnjsapply', {
    columns: [
        {
            xtype: 'rownumberer',
            text: '',
            sortable: true,
            width: 40,
            align: 'left'
        }
        ,
        {
            header: '竞赛名称',
            dataIndex: 'f_gamename',
            sortable: true,
            align: 'center',
            flex: 2
        }
        ,
        {
            header: '负责人',
            dataIndex: 'f_leadername',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                if(value == ""){
                    return "<span style='color:red;'>未填写</span>";
                }else{
                    return value;
                }
            }
        }
        ,
        {
            header: '竞赛级别',
            dataIndex: 'f_gamelevelname',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '竞赛类别',
            dataIndex: 'f_gametypename',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '主办单位',
            dataIndex: 'f_hostunit',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '参赛专业',
            dataIndex: 'f_profession',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '竞赛时间',
            dataIndex: 'f_gametime',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                return system.UtilStatic.formatDateTime(value);
            }
        }
        ,
        {
            header: '项目状态',
            dataIndex: 'f_statusname',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                if(value == "草稿"){
                    return "<span style='color: green;'>"+ value +"</span>";
                }else if(value == "待审核"){
                    return "<span style='color: dodgerblue;'>"+ value +"</span>";
                }else if(value == "项目终止"){
                    return "<span style='color: red;'>"+ value +"</span>";
                }else if(value == "已审核"){
                    return "<span style='color: blue;'>"+ value +"</span>";
                }
                return value;
            }
        }
        ,
        {
            header: '总结报告',
            dataIndex: 'f_issummarize',
            sortable: true,
            align: 'center',
            flex: 1,
            renderer: function (value) {
                if(value == null || value == ""){
                    return "<span style='color: dodgerblue;'>未提交</span>";
                }else if(value == "cg"){
                    return "<span style='color: green;'>草稿</span>";
                }else if(value == "tj"){
                    return "<span style='color: green;'>已提交</span>";
                }
                return value;
            }
        }
        ,
        {
            xtype: 'gridcolumn',
            width: 250,
            dataIndex: 'operate',
            text: '操作项',
            align: 'center',
            renderer: function (value, metadata, record, rowIndex, columnIndex, store) {
                var id = metadata.record.id;
                Ext.defer(function () {
                    var buttonGroup = Ext.widget('buttongroup', {
                        renderTo: id,
                        style: 'margin:0',
                        ui: 'footer'
                    });

                    var viewApplyBookButton = Ext.create('Ext.button.Button', {
                        height: 19,
                        text: "<span style='color:#ffffff;font-weight: normal;line-height: 15px;'>查看申报书</span>",
                        style: 'background:#157fcc;border:none;margin-right:5px;',
                        iconCls: 'fa fa-edit gridMenuIcon',
                        handler: function () {
                            var win = Ext.create("system.zgyxy.xkjnjs.biz.apply.XkjnjsApplyBookView");
                            win.setProjectId(record.get('id'));
                            win.show();
                        }
                    });
                    buttonGroup.add(viewApplyBookButton);

                    if(record.get('f_issummarize') == "tj"){
                        var viewZjBookButton = Ext.create('Ext.button.Button', {
                            height: 19,
                            text: "<span style='color:#ffffff;font-weight: normal;line-height: 15px;'>查看总结报告</span>",
                            style: 'background:#157fcc;border:none;margin-right:5px;',
                            iconCls: 'fa fa-edit gridMenuIcon',
                            handler: function () {
                                var win = Ext.create("system.zgyxy.xkjnjs.biz.zj.XkjnjsZjBookView");
                                win.setProjectId(record.get('id'));
                                win.show();
                            }
                        });
                        buttonGroup.add(viewZjBookButton);
                    }
                }, 50);
                return Ext.String.format('<div id="{0}"></div>', id);
            }
        }
    ]
});