﻿Ext.define('column_z_xkjnjsteacher', {
    columns: [
        {
            xtype: 'rownumberer',
            text: '',
            sortable: true,
            width: 40,
            align: 'left'
        }
        ,
        {
            header: '姓名',
            dataIndex: 'f_teachername',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '性别',
            dataIndex: 'f_teachersex',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '职称',
            dataIndex: 'f_teachertitle',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '电话',
            dataIndex: 'f_teacherphone',
            sortable: true,
            align: 'center',
            flex: 1
        }
        ,
        {
            header: '承担的工作',
            dataIndex: 'f_leaderresponsibility',
            sortable: true,
            align: 'center',
            flex: 2
        }
    ]
});