/**
 * 学科技能竞赛总结管理操作类
 *
 * @author lumingbao
 */
Ext.define('system.zgyxy.xkjnjs.biz.zj.XkjnjsZjOper', {

	form : null,
	util : Ext.create('system.util.util'),

    /**
     * 竞赛总结
     */
    add : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要提交总结报告的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{
            if(data[0].data.f_issummarize != null && data[0].data.f_issummarize != ""){
                // 先得到主键
                var id = data[0].data.id;
                var win = Ext.create('system.zgyxy.xkjnjs.biz.zj.XkjnjsZj');
                win.title = '学科技能竞赛总结报告 - 基本信息';
                win.modal = true;
                win.grid = grid;
                win.setFid(id);

                var pram = {fid : id};
                var param = {jsonData : Ext.encode(pram)};
                me.util.formLoad(win.infoForm.getForm() , 'z_xkjnjssummarize/load_z_xkjnjssummarizebyfid.do' , param , null ,
                    function(){
                        win.show();
                    });
            }else{
                // 先得到主键
                var id = data[0].data.id;
                var win = Ext.create('system.zgyxy.xkjnjs.biz.zj.XkjnjsZj');
                win.title = '学科技能竞赛总结报告 - 基本信息';
                win.modal = true;
                win.grid = grid;
                win.setFid(id);
                win.show();
            }
        }
    },

    /**
     * 提交项目基本信息
     */
    infoFormSubmit : function(formpanel, fun){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_xkjnjssummarize/save_xkjnjszjbg_info.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    fun(form, action, respText);
                });
        }
    },

    /**
     * 保存项目背景信息
     * @param formpanel
     * @param fun
     */
    backgroundFormSubmit : function(formpanel, fun){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_xkjnjssummarize/save_xkjnjssummarize_background.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    fun(form, action, respText);
                });
        }
    },

    /**
     * 保存项目实施过程信息
     * @param formpanel
     * @param fun
     */
    implementcourseFormSubmit : function(formpanel, fun){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_xkjnjssummarize/save_xkjnjssummarize_implementcourse.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    fun(form, action, respText);
                });
        }
    },

    /**
     * 保存项目竞赛过程信息
     * @param formpanel
     * @param fun
     */
    matchcourseFormSubmit : function(formpanel, fun){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_xkjnjssummarize/save_xkjnjssummarize_matchcourse.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    fun(form, action, respText);
                });
        }
    },

    /**
     * 保存项目竞赛成果信息
     * @param formpanel
     * @param fun
     */
    achievementFormSubmit : function(formpanel, fun){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_xkjnjssummarize/save_xkjnjssummarize_achievement.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    fun(form, action, respText);
                });
        }
    },

    /**
     * 保存项目不足与改进信息
     * @param formpanel
     * @param fun
     */
    summarizeFormSubmit : function(formpanel, fun){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_xkjnjssummarize/save_xkjnjssummarize_summarize.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    fun(form, action, respText);
                });
        }
    },

    /**
     * 保存项目中遇到的问题信息
     * @param formpanel
     * @param fun
     */
    questionFormSubmit : function(formpanel, fun){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_xkjnjssummarize/save_xkjnjssummarize_question.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    fun(form, action, respText);
                });
        }
    },

    /**
     * 保存项目创新信息
     * @param formpanel
     * @param fun
     */
    innovateFormSubmit : function(formpanel, type, fun){
        var me = this;
        var pram = {type : type};
        var param = {jsonData : Ext.encode(pram)};

        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_xkjnjssummarize/save_xkjnjssummarize_innovate.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    fun(form, action, respText);
                }, null, param);
        }
    },

    /**
     * 获奖信息添加
     */
    prizeAdd : function(grid, id){
        var win = Ext.create('system.zgyxy.xkjnjs.biz.zj.XkjnjsZjPrizeAdd');
        win.setTitle('获奖信息添加');
        win.modal = true;
        win.grid = grid;
        win.setFid(id)
        win.show();
    },

    /**
     * 获奖信息删除
     * @param grid
     */
    prizeDel : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要删除的获奖信息！');
        }else{
            Ext.MessageBox.show({
                title : '提示',
                msg : '您确定要删除所选获奖信息吗?',
                width : 250,
                buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                        var dir = new Array();
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
                        });
                        var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('z_xkjnjsprize/del_z_xkjnjsprize_qx_z_xkjnjsprizesc.do' , param ,
                            function(){
                                Ext.MessageBox.alert('提示', '删除成功！');
                                grid.getStore().reload();
                            }, null, me.form);
                    }
                }
            });
        }
    },

    /**
     * 获奖信息表单提交
     * @param formpanel
     * @param grid
     */
    prizeFormSubmit : function(formpanel, grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_xkjnjsprize/save_z_xkjnjsprize_qx_z_xkjnjsprizebj.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action, respText){
                    grid.getStore().reload();
                    Ext.MessageBox.alert('提示', '保存成功！');
                    formpanel.up('window').close();
                }, null);
        }
    },

    /**
     * 表单提交
     */
    formSubmit : function(formpanel, grid, type){
        var me = this;
        if (formpanel.form.isValid()) {
            var pram = {status : type};
            var param = {jsonData : Ext.encode(pram)};
            me.util.ExtFormSubmit('z_xkjnjs/save_z_xkjnjs_qx_z_xkjnjsbj.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(){
                    grid.getStore().reload();
                    Ext.MessageBox.alert('提示', '保存成功！');
                    me.util.closeTab();
                }, null, param);
        }
    },

    /**
     * 修改
     */
    editor : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要编辑的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能编辑一个项目！');
        }else{
            // 先得到主键
            var status = data[0].data.f_statusname;
            if(status == "草稿"){
                var id = data[0].data.id;
                var model = Ext.create("system.zgyxy.xkjnjs.biz.XkjnjsEdit");
                model.grid = grid;
                var pram = {id : id};
                var param = {jsonData : Ext.encode(pram)};
                me.util.formLoad(model.down('form').getForm() , 'z_xkjnjs/load_z_xkjnjsbyid.do' , param , null ,
                    function(){
                        me.util.addTab('projectbj', "竞赛项目编辑", model, "fa-list-ul");
                    });
            }else{
                Ext.MessageBox.alert('提示', '已提交的竞赛项目不支持编辑操作！');
            }
        }
    },

    /**
     * 删除
     */
    del : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要删除的项目！');
        }else{
            Ext.MessageBox.show({
                title : '提示',
                msg : '您确定要删除所选项目吗?',
                width : 250,
                buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                        var dir = new Array();
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
                        });
                        var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('z_xkjnjs/del_z_xkjnjs_qx_z_xkjnjssc.do' , param ,
                            function(){
                                Ext.MessageBox.alert('提示', '删除成功！');
                                grid.getStore().reload();
                            }, null, me.form);
                    }
                }
            });
        }
    },

    /**
     * 查看
     */
    view : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要查看的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能查看一个项目！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var model = Ext.create("system.zgyxy.xkjnjs.biz.XkjnjsEdit");
            model.grid = grid;
            model.down('button[name=btnsave1]').setVisible(false);
            model.down('button[name=btnsave2]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(model.down('form').getForm() , 'z_xkjnjs/load_z_xkjnjsbyid.do', param, null,
                function(){
                    me.util.addTab('projectbj', "竞赛项目查看", model, "fa-list-ul");
                });
        }
    },

    /**
     * 根据ID进行查看操作
     */
    viewById : function(id){
        var me = this;
        var model = Ext.create("system.zgyxy.xkjnjs.biz.XkjnjsEdit");
        model.down('button[name=btnsave1]').setVisible(false);
        model.down('button[name=btnsave2]').setVisible(false);
        var pram = {id : id};
        var param = {jsonData : Ext.encode(pram)};
        me.util.formLoad(model.down('form').getForm() , 'z_xkjnjs/load_z_xkjnjsbyid.do', param, null,
            function(){
                me.util.addTab('projectbj', "竞赛项目查看", model, "fa-list-ul");
            });
    },

    /**
     * 下载项目申报书
     * @param projectid
     */
    downloadProjectApplyBook : function (projectid) {
        var me = this;
        me.util.ExtAjaxRequest('z_xkjnjs/download_projectApplyBook.do?projectid=' + projectid , null ,
            function(response, options, respText){
                var f_pdf = respText.data;
                location.href = "Common/download.do?path=" + f_pdf;
            }, null, me.form);
    },
});