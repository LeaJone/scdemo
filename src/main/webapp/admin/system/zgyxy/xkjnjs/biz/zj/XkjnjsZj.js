﻿/**
 * 学科技能竞赛总结
 * @author lumingbao
 */
Ext.define(	'system.zgyxy.xkjnjs.biz.zj.XkjnjsZj', {
	extend : 'Ext.window.Window',
	grid : null,
	steps : '1',
	title :'',
	requires : [
		'system.zgyxy.xkjnjs.model.zj.Extjs_Model_XkjnjsZjPrize',
		'system.zgyxy.xkjnjs.model.zj.Extjs_Column_XkjnjsZjPrize'
	],
	oper : Ext.create('system.zgyxy.xkjnjs.biz.zj.XkjnjsZjOper'),
	initComponent:function(){
		var me = this;

		// 项目基本信息表单项开始
		me.infoprojectid = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
		});

		me.f_fid = Ext.create('Ext.form.field.Hidden',{
			name: 'f_fid',
		});

		var f_joingroup = Ext.create('system.widget.FsdNumber',{
			fieldLabel: '参赛组数',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name: 'f_joingroup',
			maxLength: 10,
			minValue: 0,
			allowBlank: false,
			allowDecimals : false//是否允许输入小数
		});

		var f_joinpeople = Ext.create('system.widget.FsdNumber',{
			fieldLabel: '参赛人数',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name: 'f_joinpeople',
			maxLength: 10,
			minValue: 0,
			allowBlank: false,
			allowDecimals : false//是否允许输入小数
		});

		var f_prizegroup = Ext.create('system.widget.FsdNumber',{
			fieldLabel: '获奖组数',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name: 'f_prizegroup',
			maxLength: 10,
			minValue: 0,
			allowBlank: false,
			allowDecimals : false//是否允许输入小数
		});

		var f_prizepeople = Ext.create('system.widget.FsdNumber',{
			fieldLabel: '获奖人数',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name: 'f_prizepeople',
			maxLength: 10,
			minValue: 0,
			allowBlank: false,
			allowDecimals : false//是否允许输入小数
		});

		var f_matchtime = Ext.create('system.widget.FsdDateField',{
			fieldLabel: '竞赛时间',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name: 'f_matchtime',
			allowBlank: false
		});

		var f_matchdays = Ext.create('system.widget.FsdNumber',{
			fieldLabel: '竞赛天数',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name: 'f_matchdays',
			maxLength: 10,
			minValue: 0,
			allowBlank: false,
			allowDecimals : false//是否允许输入小数
		});

		var f_money = Ext.create('system.widget.FsdNumber',{
			fieldLabel: '结算总金额',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name: 'f_money',
			maxLength: 10,
			minValue: 0,
			allowBlank: false,
			allowDecimals : false//是否允许输入小数
		});

		var moneydiv = new Ext.Container({
			fieldLabel: "测试",
			layout: "column",
			items: [f_money,{
				xtype: "displayfield",
				value: "万元"
			}]
		});

		me.infoForm = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '15 20 15 5',
			items : [
				me.infoprojectid,
				me.f_fid,
				f_joingroup,
				f_joinpeople,
				f_prizegroup,
				f_prizepeople,
				f_matchtime,
				f_matchdays,
				moneydiv
			]
		});
		// 项目基本信息表单项结束


		//获奖情况开始
		var prizegrid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_xkjnjszjprize',
			model : 'model_z_xkjnjszjprize',
			baseUrl : 'z_xkjnjsprize/load_pagedata.do',
			border : false,
			zAutoLoad : false,
			width: 800,
			height: 350,
			tbar : [{
				xtype : "fsdbutton",
				text : "添加获奖情况",
				iconCls : 'add',
				handler : function() {
					me.oper.prizeAdd(prizegrid, me.infoprojectid.getValue());
				}
			}, ,'-', {
				xtype : "fsdbutton",
				text : "删除获奖情况",
				iconCls : 'page_deleteIcon',
				handler : function() {
					me.oper.prizeDel(prizegrid);
				}
			},  '->', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					prizegrid.getStore().reload();
				}
			}]
		});
		prizegrid.getStore().on('beforeload', function(s) {
			var pram = {fid : me.infoprojectid.getValue()};
			var params = s.getProxy().extraParams;
			Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		//获奖情况结束

		//项目背景开始
		me.backgroundprojectid = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
		});
		var f_background = Ext.create('system.widget.FsdUeditor',{
			fieldLabel: '二、学科竞赛项目背景、特点、意义以及我校历年参赛情况',
			labelAlign:'right',
			labelWidth:90,
			width: 600,
			height : 400,
			name: 'f_background'
		});
		var backgroundForm = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '10 20 15 5',
			items:[me.backgroundprojectid, f_background]
		});
		//项目背景结束


		//项目实施过程开始
		me.implementcourseprojectid = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
		});
		var f_implementcourse = Ext.create('system.widget.FsdUeditor',{
			fieldLabel: '三、学科竞赛项目实施过程（包含组织、培训、指导、经费支出情况以及对现有实验条件利用情况）',
			labelAlign:'right',
			labelWidth:90,
			width: 600,
			height : 400,
			name: 'f_implementcourse'
		});
		var implementcourseForm = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '10 20 15 5',
			items:[me.implementcourseprojectid, f_implementcourse]
		});
		//项目实施过程结束


		//项目竞赛过程开始
		me.matchcourseprojectid = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
		});
		var f_matchcourse = Ext.create('system.widget.FsdUeditor',{
			fieldLabel: '四、学科竞赛项目竞赛过程',
			labelAlign:'right',
			labelWidth:90,
			width: 600,
			height : 400,
			name: 'f_matchcourse'
		});
		var matchcourseForm = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '10 20 15 5',
			items:[me.matchcourseprojectid, f_matchcourse]
		});
		//项目竞赛过程结束


		//项目竞赛成果开始
		me.achievementprojectid = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
		});
		var f_achievement = Ext.create('system.widget.FsdUeditor',{
			fieldLabel: '五、学科竞赛项目的成果（取得成绩、奖励情况）及应用情况（对学生或教学的促进作用）',
			labelAlign:'right',
			labelWidth:90,
			width: 600,
			height : 400,
			name: 'f_achievement'
		});
		var achievementForm = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '10 20 15 5',
			items:[me.achievementprojectid, f_achievement]
		});
		//项目竞赛成果结束


		//项目不足与改进开始
		me.summarizeprojectid = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
		});
		var f_summarize = Ext.create('system.widget.FsdUeditor',{
			fieldLabel: '六、存在不足与改进、今后竞赛展望',
			labelAlign:'right',
			labelWidth:90,
			width: 600,
			height : 400,
			name: 'f_summarize'
		});
		var summarizeForm = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '10 20 15 5',
			items:[me.summarizeprojectid, f_summarize]
		});
		//项目不足与改进结束


		//项目遇到的问题开始
		me.questionprojectid = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
		});
		var f_question = Ext.create('system.widget.FsdUeditor',{
			fieldLabel: '一、学科竞赛项目中的技术问题、难点及解决情况（总体设计、关键技术、技术指标、试验方法、研究过程中必要的图表、数据和表格等）',
			labelAlign:'right',
			labelWidth:90,
			width: 600,
			height : 400,
			name: 'f_question'
		});
		var questionForm = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '10 20 15 5',
			items:[me.questionprojectid, f_question]
		});
		//项目遇到的问题结束


		//项目创新开始
		me.innovateprojectid = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
		});
		var f_innovate = Ext.create('system.widget.FsdUeditor',{
			fieldLabel: '二、学科竞赛项目中的创新点（应用技术的发明、创新、改进与提高）',
			labelAlign:'right',
			labelWidth:90,
			width: 600,
			height : 400,
			name: 'f_innovate'
		});
		var innovateForm = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '10 20 15 5',
			items:[me.innovateprojectid, f_innovate]
		});
		//项目创新结束


		var previous = Ext.create('system.widget.FsdButton', {
			name : 'btnsave',
			text : '上一步',
			iconCls : 'previousIcon',
			handler : function() {
				if(me.steps == "2"){
					me.steps = "1";
					previous.setVisible(false);
					me.removeAll(false);
					me.add(me.infoForm);
					me.resetLocation(me);
					var titles = me.title.split("-");
					me.title = titles[0] + "- 基本信息";
					me.setTitle(me.title);
					var pram = {id : me.infoprojectid.getValue()};
					var param = {jsonData : Ext.encode(pram)};
					me.oper.util.formLoad(me.infoForm.getForm() , 'z_xkjnjssummarize/load_z_xkjnjssummarizebyid.do', param, null,
						function(){

						});
				}else if(me.steps == "3"){
					me.steps = "2";
					me.removeAll(false);

					me.add(prizegrid);
					prizegrid.getStore().reload();

					var titles = me.title.split("-");
					me.title = titles[0] + "- 获奖情况";
					me.setTitle(me.title);
				}else if(me.steps == "4"){
					me.steps = "3";
					me.removeAll(false);

					me.add(backgroundForm);
					me.resetLocation(me);

					var titles = me.title.split("-");
					me.title = titles[0] + "- 学科竞赛项目背景、特点、意义以及我校历年参赛情况";
					me.setTitle(me.title);

					var pram = {id : me.infoprojectid.getValue()};
					var param = {jsonData : Ext.encode(pram)};
					me.oper.util.formLoad(backgroundForm.getForm() , 'z_xkjnjssummarize/load_z_xkjnjssummarizebyid.do', param, null,
						function(){

						});
				}else if(me.steps == "5"){
					me.steps = "4";
					me.removeAll(false);

					me.add(implementcourseForm);
					me.resetLocation(me);

					var titles = me.title.split("-");
					me.title = titles[0] + "- 学科竞赛项目实施过程(包含组织、培训、指导、经费支出情况以及对现有实验条件利用情况)";
					me.setTitle(me.title);

					var pram = {id : me.infoprojectid.getValue()};
					var param = {jsonData : Ext.encode(pram)};
					me.oper.util.formLoad(implementcourseForm.getForm() , 'z_xkjnjssummarize/load_z_xkjnjssummarizebyid.do', param, null,
						function(){

						});
				}else if(me.steps == "6"){
					me.steps = "5";
					me.removeAll(false);

					me.add(matchcourseForm);
					me.resetLocation(me);

					var titles = me.title.split("-");
					me.title = titles[0] + "- 学科竞赛项目竞赛过程";
					me.setTitle(me.title);

					var pram = {id : me.infoprojectid.getValue()};
					var param = {jsonData : Ext.encode(pram)};
					me.oper.util.formLoad(matchcourseForm.getForm() , 'z_xkjnjssummarize/load_z_xkjnjssummarizebyid.do', param, null,
						function(){

						});
				}else if(me.steps == "7"){
					me.steps = "6";
					me.removeAll(false);

					me.add(achievementForm);
					me.resetLocation(me);

					var titles = me.title.split("-");
					me.title = titles[0] + "- 学科竞赛项目的成果（取得成绩、奖励情况）及应用情况（对学生或教学的促进作用）";
					me.setTitle(me.title);

					var pram = {id : me.infoprojectid.getValue()};
					var param = {jsonData : Ext.encode(pram)};
					me.oper.util.formLoad(achievementForm.getForm() , 'z_xkjnjssummarize/load_z_xkjnjssummarizebyid.do', param, null,
						function(){

						});
				}else if(me.steps == "8"){
					me.steps = "7";
					me.removeAll(false);

					me.add(summarizeForm);
					me.resetLocation(me);

					var titles = me.title.split("-");
					me.title = titles[0] + "- 存在不足与改进、今后竞赛展望";
					me.setTitle(me.title);

					var pram = {id : me.infoprojectid.getValue()};
					var param = {jsonData : Ext.encode(pram)};
					me.oper.util.formLoad(summarizeForm.getForm() , 'z_xkjnjssummarize/load_z_xkjnjssummarizebyid.do', param, null,
						function(){

						});
				}else if(me.steps == "9"){
					me.steps = "8";
					me.removeAll(false);

					me.add(questionForm);
					me.resetLocation(me);

					var titles = me.title.split("-");
					me.title = titles[0] + "- 学科竞赛项目中的技术问题、难点及解决情况（总体设计、关键技术、技术指标、试验方法、研究过程中必要的图表、数据和表格等）";
					me.setTitle(me.title);


					var pram = {id : me.infoprojectid.getValue()};
					var param = {jsonData : Ext.encode(pram)};
					me.oper.util.formLoad(questionForm.getForm() , 'z_xkjnjssummarize/load_z_xkjnjssummarizebyid.do', param, null,
						function(){

						});

					next.setText("下一步");
					draft.setVisible(false);
				}
			}
		});
		previous.setVisible(false);

		var next = Ext.create('system.widget.FsdButton', {
			name : 'btnsave',
			text : '下一步',
			iconCls : 'nextIcon',
			handler : function() {
				if(me.steps == "1"){
					me.oper.infoFormSubmit(me.infoForm,function(form, action, respText){
						me.infoprojectid.setValue(respText.data.id);
						me.backgroundprojectid.setValue(respText.data.id);
						me.implementcourseprojectid.setValue(respText.data.id);
						me.matchcourseprojectid.setValue(respText.data.id);
						me.achievementprojectid.setValue(respText.data.id);
						me.summarizeprojectid.setValue(respText.data.id);
						me.questionprojectid.setValue(respText.data.id);
						me.innovateprojectid.setValue(respText.data.id);

						if(me.grid != null){
							me.grid.getStore().reload();
						}
						me.removeAll(false);

						me.add(prizegrid);
						prizegrid.getStore().reload();
						me.resetLocation(me);
						me.steps = "2";
						var titles = me.title.split("-");
						me.title = titles[0] + "- 获奖情况";
						me.setTitle(me.title);
						previous.setVisible(true);
					});
				}else if(me.steps == "2"){
					me.steps = "3";
					me.removeAll(false);

					var pram = {id : me.infoprojectid.getValue()};
					var param = {jsonData : Ext.encode(pram)};
					me.oper.util.formLoad(backgroundForm.getForm() , 'z_xkjnjssummarize/load_z_xkjnjssummarizebyid.do', param, null,
						function(){

						});
					me.add(backgroundForm);
					me.resetLocation(me);

					var titles = me.title.split("-");
					me.title = titles[0] + "- 学科竞赛项目背景、特点、意义以及我校历年参赛情况";
					me.setTitle(me.title);
					previous.setVisible(true);
				}else if(me.steps == "3"){
					me.oper.backgroundFormSubmit(backgroundForm,function(form, action, respText){
						me.steps = "4";
						me.removeAll(false);

						var pram = {id : me.infoprojectid.getValue()};
						var param = {jsonData : Ext.encode(pram)};
						me.oper.util.formLoad(implementcourseForm.getForm() , 'z_xkjnjssummarize/load_z_xkjnjssummarizebyid.do', param, null,
							function(){

							});

						me.add(implementcourseForm);
						me.resetLocation(me);

						var titles = me.title.split("-");
						me.title = titles[0] + "- 学科竞赛项目实施过程（包含组织、培训、指导、经费支出情况以及对现有实验条件利用情况）";
						me.setTitle(me.title);
						previous.setVisible(true);
					});
				}else if(me.steps == "4"){
					me.oper.implementcourseFormSubmit(implementcourseForm,function(form, action, respText){
						me.steps = "5";
						me.removeAll(false);

						var pram = {id : me.infoprojectid.getValue()};
						var param = {jsonData : Ext.encode(pram)};
						me.oper.util.formLoad(matchcourseForm.getForm() , 'z_xkjnjssummarize/load_z_xkjnjssummarizebyid.do', param, null,
							function(){

							});

						me.add(matchcourseForm);
						me.resetLocation(me);

						var titles = me.title.split("-");
						me.title = titles[0] + "- 学科竞赛项目竞赛过程";
						me.setTitle(me.title);
						previous.setVisible(true);
					});
				}else if(me.steps == "5"){
					me.oper.matchcourseFormSubmit(matchcourseForm,function(form, action, respText){
						me.steps = "6";
						me.removeAll(false);

						var pram = {id : me.infoprojectid.getValue()};
						var param = {jsonData : Ext.encode(pram)};
						me.oper.util.formLoad(achievementForm.getForm() , 'z_xkjnjssummarize/load_z_xkjnjssummarizebyid.do', param, null,
							function(){

							});

						me.add(achievementForm);
						me.resetLocation(me);

						var titles = me.title.split("-");
						me.title = titles[0] + "- 学科竞赛项目的成果（取得成绩、奖励情况）及应用情况（对学生或教学的促进作用）";
						me.setTitle(me.title);
						previous.setVisible(true);
					});
				}else if(me.steps == "6"){
					me.oper.achievementFormSubmit(achievementForm,function(form, action, respText){
						me.steps = "7";
						me.removeAll(false);

						var pram = {id : me.infoprojectid.getValue()};
						var param = {jsonData : Ext.encode(pram)};
						me.oper.util.formLoad(summarizeForm.getForm() , 'z_xkjnjssummarize/load_z_xkjnjssummarizebyid.do', param, null,
							function(){

							});

						me.add(summarizeForm);
						me.resetLocation(me);

						var titles = me.title.split("-");
						me.title = titles[0] + "- 存在不足与改进、今后竞赛展望";
						me.setTitle(me.title);
						previous.setVisible(true);
					});
				}else if(me.steps == "7"){
					me.oper.summarizeFormSubmit(summarizeForm,function(form, action, respText){
						me.steps = "8";
						me.removeAll(false);

						var pram = {id : me.infoprojectid.getValue()};
						var param = {jsonData : Ext.encode(pram)};
						me.oper.util.formLoad(questionForm.getForm() , 'z_xkjnjssummarize/load_z_xkjnjssummarizebyid.do', param, null,
							function(){

							});

						me.add(questionForm);
						me.resetLocation(me);

						var titles = me.title.split("-");
						me.title = titles[0] + "- 学科竞赛项目中的技术问题、难点及解决情况（总体设计、关键技术、技术指标、试验方法、研究过程中必要的图表、数据和表格等）";
						me.setTitle(me.title);
						previous.setVisible(true);
					});
				}else if(me.steps == "8"){
					me.oper.questionFormSubmit(questionForm,function(form, action, respText){
						me.steps = "9";
						me.removeAll(false);

						var pram = {id : me.infoprojectid.getValue()};
						var param = {jsonData : Ext.encode(pram)};
						me.oper.util.formLoad(innovateForm.getForm() , 'z_xkjnjssummarize/load_z_xkjnjssummarizebyid.do', param, null,
							function(){

							});

						me.add(innovateForm);
						me.resetLocation(me);

						var titles = me.title.split("-");
						me.title = titles[0] + "- 学科竞赛项目中的创新点（应用技术的发明、创新、改进与提高）";
						me.setTitle(me.title);
						previous.setVisible(true);

						previous.setVisible(true);
						next.setText("提交项目");
						draft.setVisible(true);
					});
				}else if(me.steps == "9"){
					me.oper.innovateFormSubmit(innovateForm, "tj", function(form, action, respText){
						Ext.MessageBox.alert('提示', '项目提交成功！');
						if(me.grid != null){
							me.grid.getStore().reload();
						}
						me.close();
					});
				}
			}
		});

		var draft = Ext.create('system.widget.FsdButton', {
			name : 'btnsave',
			text : '保存草稿',
			iconCls : 'nextIcon',
			handler : function() {
				me.oper.innovateFormSubmit(innovateForm, "cg", function(form, action, respText){
					Ext.MessageBox.alert('提示', '项目提交成功！');
					if(me.grid != null){
						me.grid.getStore().reload();
					}
					me.close();
				});
			}
		});
		draft.setVisible(false);

		Ext.apply(this,{
			layout : 'fit',
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			items :[me.infoForm],
			buttons : [previous, draft, next, {
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	},
	/**
	 * 设置项目类型名称
	 * @param typename
	 */
	setFid:function (fid) {
		this.f_fid.setValue(fid);
	},

	/**
	 * 重置窗口的位置
	 * @param window
	 */
	resetLocation: function (window) {
		var docHeight = document.body.clientHeight;
		var docWidth = document.body.clientWidth;
		var winHeight = window.getHeight();
		var winWidth = window.getWidth();

		var y = (docHeight - winHeight)/2;
		var x = (docWidth - winWidth)/2;
		window.setPosition(x, y);
	}
});