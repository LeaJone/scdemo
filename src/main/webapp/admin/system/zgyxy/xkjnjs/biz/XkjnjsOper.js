/**
 * 学科技能竞赛管理操作类
 *
 * @author lumingbao
 */
Ext.define('system.zgyxy.xkjnjs.biz.XkjnjsOper', {

	form : null,
	util : Ext.create('system.util.util'),

    /**
     * 项目申报
     */
    add : function(grid, type){
        var me = this;
        var model = Ext.create("system.zgyxy.xkjnjs.biz.XkjnjsEdit");
        model.grid = grid;
        var tabID = 'projectbj';
        me.util.addTab(tabID, "竞赛申报", model, "fa-list-ul");
    },

    /**
     * 表单提交
     */
    formSubmit : function(formpanel, grid, type){
        var me = this;
        if (formpanel.form.isValid()) {
            var pram = {status : type};
            var param = {jsonData : Ext.encode(pram)};
            me.util.ExtFormSubmit('z_xkjnjs/save_z_xkjnjs_qx_z_xkjnjsbj.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(){
                    grid.getStore().reload();
                    Ext.MessageBox.alert('提示', '保存成功！');
                    me.util.closeTab();
                }, null, param);
        }
    },

    /**
     * 修改
     */
    editor : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要编辑的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能编辑一个项目！');
        }else{
            // 先得到主键
            var status = data[0].data.f_statusname;
            if(status == "草稿"){
                var id = data[0].data.id;
                var model = Ext.create("system.zgyxy.xkjnjs.biz.XkjnjsEdit");
                model.grid = grid;
                var pram = {id : id};
                var param = {jsonData : Ext.encode(pram)};
                me.util.formLoad(model.down('form').getForm() , 'z_xkjnjs/load_z_xkjnjsbyid.do' , param , null ,
                    function(){
                        me.util.addTab('projectbj', "竞赛项目编辑", model, "fa-list-ul");
                    });
            }else{
                Ext.MessageBox.alert('提示', '已提交的竞赛项目不支持编辑操作！');
            }
        }
    },

    /**
     * 删除
     */
    del : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要删除的项目！');
        }else{
            Ext.MessageBox.show({
                title : '提示',
                msg : '您确定要删除所选项目吗?',
                width : 250,
                buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                        var dir = new Array();
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
                        });
                        var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('z_xkjnjs/del_z_xkjnjs_qx_z_xkjnjssc.do' , param ,
                            function(){
                                Ext.MessageBox.alert('提示', '删除成功！');
                                grid.getStore().reload();
                            }, null, me.form);
                    }
                }
            });
        }
    },

    /**
     * 查看
     */
    view : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要查看的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能查看一个项目！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var model = Ext.create("system.zgyxy.xkjnjs.biz.XkjnjsEdit");
            model.grid = grid;
            model.down('button[name=btnsave1]').setVisible(false);
            model.down('button[name=btnsave2]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(model.down('form').getForm() , 'z_xkjnjs/load_z_xkjnjsbyid.do', param, null,
                function(){
                    me.util.addTab('projectbj', "竞赛项目查看", model, "fa-list-ul");
                });
        }
    },

    /**
     * PDF文件下载
     */
    download : function(f_pdf){
        var me = this;
        if(f_pdf != "" && f_pdf != null){
            Ext.MessageBox.show({
                title : '询问',
                msg : '你确定要下载PDF吗？',
                width : 250,
                buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                        window.open('Common/download.do?path=' + f_pdf);
                    }
                }
            });
        }
    },

    /**
     * 根据ID进行查看操作
     */
    viewById : function(id){
        var me = this;
        var model = Ext.create("system.zgyxy.xkjnjs.biz.XkjnjsEdit");
        model.down('button[name=btnsave1]').setVisible(false);
        model.down('button[name=btnsave2]').setVisible(false);
        var pram = {id : id};
        var param = {jsonData : Ext.encode(pram)};
        me.util.formLoad(model.down('form').getForm() , 'z_xkjnjs/load_z_xkjnjsbyid.do', param, null,
            function(){
                me.util.addTab('projectbj', "竞赛项目查看", model, "fa-list-ul");
            });
    }
});