﻿/**
 * 学科技能竞赛申报
 * @author lumingbao
 */
Ext.define(	'system.zgyxy.xkjnjs.biz.apply.XkjnjsApply', {
	extend : 'Ext.window.Window',
	grid : null,
	steps : '1',
	title :'',
	requires : [
		'system.zgyxy.domain.model.Extjs_Model_Z_domain',
		'system.zgyxy.domain.model.Extjs_Column_Z_domain',
		'system.zgyxy.budget.model.Extjs_Model_Budget',
		'system.zgyxy.budget.model.Extjs_Column_Budget',

		'system.zgyxy.xkjnjs.model.apply.Extjs_Model_XkjnjsTeacher',
		'system.zgyxy.xkjnjs.model.apply.Extjs_Column_XkjnjsTeacher'
	],
	oper : Ext.create('system.zgyxy.xkjnjs.biz.apply.XkjnjsApplyOper'),
	budgetOper : Ext.create('system.zgyxy.budget.biz.BudgetOper'),
	initComponent:function(){
		var me = this;

		// 项目基本信息表单项开始
		me.infoprojectid = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
		});

		var f_gamename = Ext.create('system.widget.FsdTextField',{
			fieldLabel: '竞赛名称',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name: 'f_gamename',
			maxLength: 500,
			allowBlank: false
		});

		var f_profession = Ext.create('system.widget.FsdTextField',{
			fieldLabel: '参赛专业',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name: 'f_profession',
			maxLength: 20,
			allowBlank: false
		});

		me.f_gamelevelid = Ext.create('system.widget.FsdComboBoxZD',{
			fieldLabel:'竞赛级别',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name : 'f_gamelevelid',//提交到后台的参数名
			key : 'xkjsjs-jb',//参数
			allowBlank: false,
			hiddenName : 'f_gamelevelname'//提交隐藏域Name
		});
		var f_gamelevelname = Ext.create('Ext.form.field.Hidden',{
			fieldLabel: '竞赛级别名称',
			name: 'f_gamelevelname'
		});
		var f_gametypeid = Ext.create('system.widget.FsdComboBoxZD',{
			fieldLabel:'竞赛类别',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name : 'f_gametypeid',//提交到后台的参数名
			key : 'xkjnjs-lb',//参数
			allowBlank: false,
			hiddenName : 'f_gametypename'//提交隐藏域Name
		});
		var f_gametypename = Ext.create('Ext.form.field.Hidden',{
			fieldLabel: '竞赛类别名称',
			name: 'f_gametypename'
		});


		var f_hostunit = Ext.create('system.widget.FsdTextField',{
			fieldLabel: '主办单位',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name: 'f_hostunit',
			maxLength: 100,
			allowBlank: false
		});
		var f_undertaker = Ext.create('system.widget.FsdTextField',{
			fieldLabel: '承办单位',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name: 'f_undertaker',
			maxLength: 100
		});

		var f_jslyxx = Ext.create('system.widget.FsdTextField',{
			fieldLabel: '竞赛联院信息',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name: 'f_jslyxx',
			maxLength: 100
		});

		var f_formid = Ext.create('system.widget.FsdComboBoxZD',{
			fieldLabel:'参赛形式',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name : 'f_formid',//提交到后台的参数名
			key : 'xkjnjs-xs',//参数
			allowBlank: false,
			hiddenName : 'f_formname'//提交隐藏域Name
		});
		var f_formname = Ext.create('Ext.form.field.Hidden',{
			fieldLabel: '参赛形式名称',
			name: 'f_formname'
		});

		var f_gametime = Ext.create('system.widget.FsdDateField',{
			fieldLabel: '竞赛时间',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name: 'f_gametime',
			allowBlank: false
		});

		var f_site = Ext.create('system.widget.FsdTextField',{
			fieldLabel: '竞赛地点',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name: 'f_site',
			maxLength: 20,
			allowBlank: false
		});

		var f_basename = Ext.create('system.widget.FsdTextField',{
			fieldLabel: '依托基地',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name: 'f_basename',
			maxLength: 100
		});

		var f_baseleadername = Ext.create('system.widget.FsdTextField',{
			fieldLabel: '基地负责人',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name: 'f_baseleadername',
			maxLength: 20
		});

		var f_applyformoney = Ext.create('system.widget.FsdTextField',{
			fieldLabel: '申请经费',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name: 'f_applyformoney',
			maxLength: 20,
			allowBlank: false
		});

		var f_matingmoney = Ext.create('system.widget.FsdTextField',{
			fieldLabel: '配套经费',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name: 'f_matingmoney',
			maxLength: 20
		});

		var f_matingmoneysource = Ext.create('system.widget.FsdTextField',{
			fieldLabel: '配套经费来源',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name: 'f_matingmoneysource',
			maxLength: 100
		});
		var f_group = Ext.create('system.widget.FsdNumber',{
			fieldLabel: '参赛队数',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name: 'f_group',
			maxLength: 10,
			minValue: 0,
			allowBlank: false,
			allowDecimals : false//是否允许输入小数
		});

		var f_people = Ext.create('system.widget.FsdNumber',{
			fieldLabel: '参赛人数',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name: 'f_people',
			maxLength: 10,
			minValue: 0,
			allowBlank: false,
			allowDecimals : false//是否允许输入小数
		});

		me.infoForm = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '15 20 15 5',
			items : [
				me.infoprojectid,
				f_gamename,
				me.f_gamelevelid,
				f_gamelevelname,
				f_gametypeid,
				f_gametypename,
				f_hostunit,
				f_undertaker,
				f_jslyxx,
				f_formid,
				f_formname,
				f_profession,
				f_gametime,
				f_site,
				f_basename,
				f_baseleadername,
				f_applyformoney,
				f_matingmoney,
				f_matingmoneysource,
				f_group,
				f_people
			]
		});
		// 项目基本信息表单项结束

		// 项目负责人信息表单项开始
		me.leaderprojectid = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
		});
		var f_leaderid = Ext.create('Ext.form.field.Hidden',{
			name: 'f_leaderid',
		});
		var f_leadername = Ext.create('system.widget.FsdTextField',{
			name: 'f_leadername',
			fieldLabel: '负责人姓名',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			maxLength: 500,
			allowBlank: false,
			readOnly: true,
		});
		var f_leaderphone = Ext.create('system.widget.FsdTextField',{
			name: 'f_leaderphone',
			fieldLabel: '负责人电话',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			maxLength: 500,
			allowBlank: false
		});
		var f_leadertitle = Ext.create('system.widget.FsdTextField',{
			name: 'f_leadertitle',
			fieldLabel: '负责人职称',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			maxLength: 500,
			allowBlank: false
		});
		var f_leaderbranch = Ext.create('system.widget.FsdTextField',{
			name: 'f_leaderbranch',
			fieldLabel: '负责人部门',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			maxLength: 500,
			allowBlank: false
		});
		var f_leaderresponsibility = Ext.create('system.widget.FsdTextField',{
			name: 'f_leaderresponsibility',
			fieldLabel: '负责人职责',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			maxLength: 500,
			allowBlank: false
		});
		var leaderForm = Ext.create('Ext.form.Panel',{
			tbar : [{
				xtype : "fsdbutton",
				text : "选择负责人",
				iconCls : 'folder_userIcon',
				handler : function() {
					var win = Ext.create("system.zgyxy.user.biz.UserChoose");
					win.setTitle("选择负责人")
					win.modal = true;
					win.setCallbackFun(function(obj, record){
						f_leaderid.setValue(record.data.id);
						f_leadername.setValue(record.data.realname);
						f_leaderphone.setValue(record.data.mobile);
						win.close();
					});
					win.show();
				}
			}],
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '0 20 15 5',
			items : [
				me.leaderprojectid,
				f_leaderid,
				f_leadername,
				f_leaderphone,
				f_leadertitle,
				f_leaderbranch,
				f_leaderresponsibility
			]
		});
		// 项目负责人信息表单项结束

		//指导教师开始
		// 创建表格
		var teachergrid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_xkjnjsteacher',
			model : 'model_z_xkjnjsteacher',
			baseUrl : 'z_xkjnjsteacher/load_pagedata.do',
			border : false,
			zAutoLoad : false,
			width: 800,
			height: 350,
			tbar : [{
				xtype : "fsdbutton",
				text : "添加指导教师",
				iconCls : 'add',
				handler : function() {
					me.oper.teacherAdd(teachergrid, me.infoprojectid.getValue());
				}
			}, ,'-', {
				xtype : "fsdbutton",
				text : "删除指导教师",
				iconCls : 'page_deleteIcon',
				handler : function() {
					me.oper.teacherDel(teachergrid);
				}
			},  '->', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					teachergrid.getStore().reload();
				}
			}]
		});
		teachergrid.getStore().on('beforeload', function(s) {
			var pram = {projectid : me.infoprojectid.getValue()};
			var params = s.getProxy().extraParams;
			Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		//指导教师结束

		//项目简介开始
		me.abstractprojectid = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
		});
		var f_abstract = Ext.create('system.widget.FsdTextArea',{
			fieldLabel: '项目简介',
			labelAlign: 'right',
			labelWidth: 90,
			width: 600,
			height: 200,
			name: 'f_abstract',
			allowBlank: false,
			maxLength:500
		});
		var abstractForm = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '10 20 15 5',
			items:[me.abstractprojectid, f_abstract]
		});
		//项目简介结束

		//项目方案开始
		me.schemeprojectid = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
		});
		var f_scheme = Ext.create('system.widget.FsdUeditor',{
			fieldLabel: '项目方案',
			labelAlign:'right',
			labelWidth:90,
			width: 800,
			height : 400,
			name: 'f_scheme'
		});
		var schemeForm = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '10 20 15 5',
			items:[me.schemeprojectid, f_scheme]
		});
		//项目方案结束

		//配套条件开始
		me.conditionprojectid = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
		});
		var f_condition = Ext.create('system.widget.FsdUeditor',{
			fieldLabel: '配套条件',
			labelAlign:'right',
			labelWidth:90,
			width: 800,
			height : 400,
			name: 'f_condition'
		});
		var conditionForm = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '10 20 15 5',
			items:[me.conditionprojectid, f_condition]
		});
		//配套条件结束

		//培训方案开始
		me.traineprojectid = Ext.create('Ext.form.field.Hidden',{
			name: 'id',
		});
		var f_train = Ext.create('system.widget.FsdUeditor',{
			fieldLabel: '培训方案',
			labelAlign:'right',
			labelWidth:90,
			width: 800,
			height : 400,
			name: 'f_train'
		});
		var trainForm = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '10 20 15 5',
			items:[me.traineprojectid, f_train]
		});
		//培训方案结束

		//经费预算开始
		var budgetgrid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_budget',
			model : 'model_z_budget',
			baseUrl : 'z_budget/load_pagedata.do',
			border : false,
			width: 800,
			height: 350,
			tbar : [{
				xtype : "fsdbutton",
				text : "添加",
				iconCls : 'add',
				handler : function() {
					me.budgetOper.add(budgetgrid, me.infoprojectid.getValue());
				}
			}, ,'-', {
				xtype : "fsdbutton",
				text : "编辑",
				iconCls : 'page_editIcon',
				handler : function() {
					me.budgetOper.editor(budgetgrid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.budgetOper.view(budgetgrid);
				}
			}, {
				xtype : "fsdbutton",
				text : "删除",
				iconCls : 'page_deleteIcon',
				handler : function() {
					me.budgetOper.del(budgetgrid);
				}
			},  '->', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					budgetgrid.getStore().reload();
				}
			}]
		});
		budgetgrid.getStore().on('beforeload', function(s) {
			var pram = {fid : me.infoprojectid.getValue()};
			var params = s.getProxy().extraParams;
			Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		//经费预算结束

		var previous = Ext.create('system.widget.FsdButton', {
			name : 'btnsave',
			text : '上一步',
			iconCls : 'previousIcon',
			handler : function() {
				if(me.steps == "2"){
					me.steps = "1";
					previous.setVisible(false);
					me.removeAll(false);
					me.add(me.infoForm);
					me.resetLocation(me);
					var titles = me.title.split("-");
					me.title = titles[0] + "- 基本信息";
					me.setTitle(me.title);
					var pram = {id : me.infoprojectid.getValue()};
					var param = {jsonData : Ext.encode(pram)};
					me.oper.util.formLoad(me.infoForm.getForm() , 'z_xkjnjs/load_z_xkjnjsbyid.do', param, null,
						function(){

						});
				}else if(me.steps == "3"){
					me.steps = "2";
					me.removeAll(false);
					me.add(leaderForm);
					me.resetLocation(me);
					var titles = me.title.split("-");
					me.title = titles[0] + "- 项目负责人信息";
					me.setTitle(me.title);
					var pram = {id : me.leaderprojectid.getValue()};
					var param = {jsonData : Ext.encode(pram)};
					me.oper.util.formLoad(leaderForm.getForm() , 'z_xkjnjs/load_z_xkjnjsbyid.do', param, null,
						function(){

						});
				}else if(me.steps == "4"){
					me.steps = "3";
					me.removeAll(false);
					me.add(teachergrid);
					me.resetLocation(me);
					var titles = me.title.split("-");
					me.title = titles[0] + "- 指导老师信息";
					me.setTitle(me.title);
					teachergrid.getStore().reload();
				}else if(me.steps == "5"){
					me.steps = "4";
					me.removeAll(false);
					me.add(abstractForm);
					me.resetLocation(me);
					var titles = me.title.split("-");
					me.title = titles[0] + "- 项目简介";
					me.setTitle(me.title);
					var pram = {id : me.abstractprojectid.getValue()};
					var param = {jsonData : Ext.encode(pram)};
					me.oper.util.formLoad(abstractForm.getForm() , 'z_xkjnjs/load_z_xkjnjsbyid.do', param, null,
						function(){

						});
				}else if(me.steps == "6"){
					me.steps = "5";
					me.removeAll(false);
					me.add(schemeForm);
					me.resetLocation(me);
					var titles = me.title.split("-");
					me.title = titles[0] + "- 实施方案及预期目标";
					me.setTitle(me.title);
					var pram = {id : me.infoprojectid.getValue()};
					var param = {jsonData : Ext.encode(pram)};
					me.oper.util.formLoad(schemeForm.getForm() , 'z_xkjnjs/load_z_xkjnjsbyid.do', param, null,
						function(){

						});
				}else if(me.steps == "7"){
					me.steps = "6";
					me.removeAll(false);
					me.add(conditionForm);
					me.resetLocation(me);
					var titles = me.title.split("-");
					me.title = titles[0] + "- 配套条件";
					me.setTitle(me.title);
					var pram = {id : me.infoprojectid.getValue()};
					var param = {jsonData : Ext.encode(pram)};
					me.oper.util.formLoad(conditionForm.getForm() , 'z_xkjnjs/load_z_xkjnjsbyid.do', param, null,
						function(){

						});
				}else if(me.steps == "8"){
					me.steps = "7";
					me.removeAll(false);
					me.add(trainForm);
					me.resetLocation(me);
					var titles = me.title.split("-");
					me.title = titles[0] + "- 培训方案";
					me.setTitle(me.title);
					next.setText("下一步");
					draft.setVisible(false);
					var pram = {id : me.infoprojectid.getValue()};
					var param = {jsonData : Ext.encode(pram)};
					me.oper.util.formLoad(trainForm.getForm() , 'z_xkjnjs/load_z_xkjnjsbyid.do', param, null,
						function(){

						});
				}
			}
		});
		previous.setVisible(false);

		var next = Ext.create('system.widget.FsdButton', {
			name : 'btnsave',
			text : '下一步',
			iconCls : 'nextIcon',
			handler : function() {
				if(me.steps == "1"){
					me.oper.infoFormSubmit(me.infoForm,function(form, action, respText){
						me.infoprojectid.setValue(respText.data.id);
						me.leaderprojectid.setValue(respText.data.id);

						me.abstractprojectid.setValue(respText.data.id);
						me.schemeprojectid.setValue(respText.data.id);
						if(me.grid != null){
							me.grid.getStore().reload();
						}
						me.removeAll(false);

						var pram = {id : me.infoprojectid.getValue()};
						var param = {jsonData : Ext.encode(pram)};
						me.oper.util.formLoad(leaderForm.getForm() , 'z_xkjnjs/load_z_xkjnjsbyid.do', param, null,
							function(){

							});
						me.add(leaderForm);
						me.resetLocation(me);
						me.steps = "2";
						var titles = me.title.split("-");
						me.title = titles[0] + "- 负责人信息";
						me.setTitle(me.title);
						previous.setVisible(true);
					});
				}else if(me.steps == "2"){
					me.oper.leaderFormSubmit(leaderForm,function(form, action, respText){
						if(me.grid != null){
							me.grid.getStore().reload();
						}
						me.removeAll(false);
						me.add(teachergrid);
						teachergrid.getStore().reload();
						me.resetLocation(me);
						me.steps = "3";
						var titles = me.title.split("-");
						me.title = titles[0] + "- 指导老师信息";
						me.setTitle(me.title);
						previous.setVisible(true);
					});
				}else if(me.steps == "3"){
					me.removeAll(false);
					var pram = {id : me.infoprojectid.getValue()};
					var param = {jsonData : Ext.encode(pram)};
					me.oper.util.formLoad(abstractForm.getForm() , 'z_xkjnjs/load_z_xkjnjsbyid.do', param, null,
						function(){

						});
					me.add(abstractForm);
					me.resetLocation(me);
					me.steps = "4";
					var titles = me.title.split("-");
					me.title = titles[0] + "- 竞赛内容简介";
					me.setTitle(me.title);
					previous.setVisible(true);
				}else if(me.steps == "4"){
					me.oper.abstractFormSubmit(abstractForm,function(form, action, respText){
						me.removeAll(false);
						var pram = {id : me.infoprojectid.getValue()};
						var param = {jsonData : Ext.encode(pram)};
						me.oper.util.formLoad(schemeForm.getForm() , 'z_xkjnjs/load_z_xkjnjsbyid.do', param, null,
							function(){

							});
						me.add(schemeForm);
						me.resetLocation(me);
						me.steps = "5";
						var titles = me.title.split("-");
						me.title = titles[0] + "- 实施方案及预期目标";
						me.setTitle(me.title);
						previous.setVisible(true);
					});
				}else if(me.steps == "5"){
					me.oper.schemeFormSubmit(schemeForm,function(form, action, respText){
						me.removeAll(false);
						var pram = {id : me.infoprojectid.getValue()};
						var param = {jsonData : Ext.encode(pram)};
						me.oper.util.formLoad(conditionForm.getForm() , 'z_xkjnjs/load_z_xkjnjsbyid.do', param, null,
							function(){

							});
						me.add(conditionForm);
						me.resetLocation(me);
						me.steps = "6";
						var titles = me.title.split("-");
						me.title = titles[0] + "- 配套条件";
						me.setTitle(me.title);
						previous.setVisible(true);
					});
				}else if(me.steps == "6"){
					me.oper.conditionFormSubmit(conditionForm,function(form, action, respText){
						me.removeAll(false);
						var pram = {id : me.infoprojectid.getValue()};
						var param = {jsonData : Ext.encode(pram)};
						me.oper.util.formLoad(trainForm.getForm() , 'z_xkjnjs/load_z_xkjnjsbyid.do', param, null,
							function(){

							});
						me.add(trainForm);
						me.resetLocation(me);
						me.steps = "7";
						var titles = me.title.split("-");
						me.title = titles[0] + "- 培训方案";
						me.setTitle(me.title);
						previous.setVisible(true);
					});
				}else if(me.steps == "7"){
					me.oper.trainFormSubmit(trainForm,function(form, action, respText){
						me.removeAll(false);
						me.add(budgetgrid);
						budgetgrid.getStore().reload();
						me.resetLocation(me);
						me.steps = "8";
						var titles = me.title.split("-");
						me.title = titles[0] + "- 经费预算";
						me.setTitle(me.title);
						previous.setVisible(true);
						next.setText("提交项目");
						draft.setVisible(true);
					});
				}else if(me.steps == "8"){
					me.oper.projectSubmit(me.infoprojectid.getValue(), "tj", function(form, action, respText){
						Ext.MessageBox.alert('提示', '项目提交成功！');
						if(me.grid != null){
							me.grid.getStore().reload();
						}
						me.close();
					});
				}
			}
		});

		var draft = Ext.create('system.widget.FsdButton', {
			name : 'btnsave',
			text : '保存草稿',
			iconCls : 'nextIcon',
			handler : function() {
				me.oper.projectSubmit(me.infoprojectid.getValue(), "cg", function(form, action, respText){
					Ext.MessageBox.alert('提示', '项目提交成功！');
					me.close();
				});
			}
		});
		draft.setVisible(false);

		Ext.apply(this,{
			layout : 'fit',
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			items :[me.infoForm],
			buttons : [previous, draft, next, {
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	},

	/**
	 * 设置项目类型编码
	 * @param typeid
	 */
	setTypeId:function(typeid){
		this.f_typeid.setValue(typeid);
	},
	/**
	 * 设置项目类型名称
	 * @param typename
	 */
	setTypeName:function (typename) {
		this.f_typename.setValue(typename);
	},

	/**
	 * 重置窗口的位置
	 * @param window
	 */
	resetLocation: function (window) {
		var docHeight = document.body.clientHeight;
		var docWidth = document.body.clientWidth;
		var winHeight = window.getHeight();
		var winWidth = window.getWidth();

		var y = (docHeight - winHeight)/2;
		var x = (docWidth - winWidth)/2;
		window.setPosition(x, y);
	}
});