﻿/**
 * 学科技能竞赛总结管理界面
 *
 * @author lumingbao
 */
Ext.define('system.zgyxy.xkjnjs.biz.zj.XkjnjsZjManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.zgyxy.xkjnjs.model.zj.Extjs_Column_XkjnjsZj',
	             'system.zgyxy.xkjnjs.model.zj.Extjs_Model_XkjnjsZj'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.zgyxy.xkjnjs.biz.zj.XkjnjsZjOper'),
    
	createTable : function() {
	    var me = this;

        /**
         * 查询文本框
         */
        var queryText =  Ext.create("Ext.form.TextField" , {
            name : 'queryParam',
            emptyText : '请输入项目名称',
            enableKeyEvents : true,
            width : 130,
            listeners : {
                specialkey : function(field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        grid.getStore().reload();
                    }
                }
            }
        });
	    
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_xkjnjszj',
			model : 'model_z_xkjnjszj',
			baseUrl : 'z_xkjnjs/load_pagedata.do',
			border : false,
			tbar : [{
                xtype : "fsdbutton",
                text : "提交竞赛总结",
                iconCls : 'add',
                handler : function() {
                    me.oper.add(grid);
                }
            }, ,'-', {
                xtype : "fsdbutton",
                text : "编辑竞赛总结",
                iconCls : 'page_editIcon',
                handler : function() {
                    me.oper.editor(grid);
                }
            }, {
                xtype : "fsdbutton",
                text : "删除竞赛总结",
                iconCls : 'page_deleteIcon',
                handler : function() {
                    me.oper.del(grid);
                }
            },  '->', queryText, {
                text: '查询',
                iconCls: 'magnifierIcon',
                handler: function () {
                    grid.getStore().reload();
                }
            }, {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]
		});
		grid.getStore().on('beforeload', function(s) {
            var text =  queryText.getValue();
            var pram = {name : text};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		return grid;
	}
});