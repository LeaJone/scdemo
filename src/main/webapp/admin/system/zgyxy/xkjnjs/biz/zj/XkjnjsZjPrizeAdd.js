﻿/**
 * 学科技能竞赛总结报告获奖信息添加界面
 * @author lumingbao
 */
Ext.define(	'system.zgyxy.xkjnjs.biz.zj.XkjnjsZjPrizeAdd', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.zgyxy.xkjnjs.biz.zj.XkjnjsZjOper'),
	initComponent:function(){
		var me = this;

		me.f_fid = Ext.create('Ext.form.field.Hidden',{
			name: 'f_fid',
		});

		var f_name = Ext.create('system.widget.FsdTextField',{
			name: 'f_name',
			fieldLabel: '作品名称',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			maxLength: 500,
			allowBlank: false,
			blankText : '请输入作品名称'
		});

		var f_typeid = Ext.create('system.widget.FsdComboBoxZD',{
			fieldLabel:'获奖级别',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name : 'f_typeid',//提交到后台的参数名
			key : 'jsjb',//参数
			allowBlank: false,
			hiddenName : 'f_typename'//提交隐藏域Name
		});
		var f_typename = Ext.create('Ext.form.field.Hidden',{
			fieldLabel: '获奖级别',
			name: 'f_typename'
		});

		var f_levelid = Ext.create('system.widget.FsdComboBoxZD',{
			fieldLabel:'奖励等级',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			name : 'f_levelid',//提交到后台的参数名
			key : 'hjdj',//参数
			allowBlank: false,
			hiddenName : 'f_levelname'//提交隐藏域Name
		});
		var f_levelname = Ext.create('Ext.form.field.Hidden',{
			fieldLabel: '奖励等级',
			name: 'f_levelname'
		});

		var f_teacher = Ext.create('system.widget.FsdTextField',{
			name: 'f_teacher',
			fieldLabel: '指导教师',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			maxLength: 500,
			allowBlank: false,
			blankText : '请输入指导教师'
		});

		var f_student = Ext.create('system.widget.FsdTextArea',{
			name : 'f_student',
			fieldLabel : '参赛学生',
			labelAlign :'right',
			labelWidth : 90,
			width : 450,
			allowBlank : false,
			maxLength : 200,
			blankText : '请输入参赛学生',
			maxLengthText : '课程简介不能超过200个字符'
		});
		var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '10 20 15 5',
			items : [
				me.f_fid,
				f_name,
				f_typeid,
				f_typename,
				f_levelid,
				f_levelname,
				f_teacher,
				f_student
			]
		});

		Ext.apply(this,{
			layout : 'fit',
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			items :[form],
			buttons : [{
				name : 'btnsave',
				text : '保存',
				iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
				handler : function() {
					me.oper.prizeFormSubmit(form, me.grid);
				}
			}, {
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	},

	/**
	 * 设置父级ID
	 * @param tutorid
	 */
	setFid : function (fid) {
		this.f_fid.setValue(fid);
	}
});