﻿/**
 * 指导老师添加界面
 * @author lumingbao
 */
Ext.define(	'system.zgyxy.xkjnjs.biz.apply.XkjnjsTeacherAdd', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.zgyxy.xkjnjs.biz.apply.XkjnjsApplyOper'),
	initComponent:function(){
		var me = this;

		me.f_fid = Ext.create('Ext.form.field.Hidden',{
			name: 'f_fid',
		});

		var f_teacherid = Ext.create('Ext.form.field.Hidden',{
			name: 'f_teacherid',
		});
		var f_teachername = Ext.create('system.widget.FsdTextField',{
			name: 'f_teachername',
			fieldLabel: '姓名',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			maxLength: 500,
			allowBlank: false,
			readOnly: true,
		});
		var f_teachersex = Ext.create('system.widget.FsdTextField',{
			name: 'f_teachersex',
			fieldLabel: '性别',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			maxLength: 500,
			allowBlank: false
		});
		var f_teachertitle = Ext.create('system.widget.FsdTextField',{
			name: 'f_teachertitle',
			fieldLabel: '职称',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			maxLength: 500,
			allowBlank: false
		});
		var f_teacherphone = Ext.create('system.widget.FsdTextField',{
			name: 'f_teacherphone',
			fieldLabel: '电话',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			maxLength: 500,
			allowBlank: false
		});
		var f_leaderresponsibility = Ext.create('system.widget.FsdTextField',{
			name: 'f_leaderresponsibility',
			fieldLabel: '承担的工作',
			labelAlign:'right',
			labelWidth:90,
			width : 450,
			maxLength: 500,
			allowBlank: false
		});
		var form = Ext.create('Ext.form.Panel',{
			tbar : [{
				xtype : "fsdbutton",
				text : "选择项目成员",
				iconCls : 'folder_userIcon',
				handler : function() {
					var win = Ext.create("system.zgyxy.user.biz.UserChoose");
					win.setTitle("选择指导老师")
					win.modal = true;
					win.setCallbackFun(function(obj, record){
						f_teacherid.setValue(record.data.id);
						f_teachername.setValue(record.data.realname);
						f_teacherphone.setValue(record.data.mobile);
						win.close();
					});
					win.show();
				}
			}],
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '0 20 15 5',
			items : [
				me.f_fid,
				f_teacherid,
				f_teachername,
				f_teachersex,
				f_teachertitle,
				f_teacherphone,
				f_leaderresponsibility
			]
		});

		Ext.apply(this,{
			layout : 'fit',
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			items :[form],
			buttons : [{
				name : 'btnsave',
				text : '保存',
				iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
				handler : function() {
					me.oper.teacherFormSubmit(form, me.grid);
				}
			}, {
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	},

	/**
	 * 设置父级ID
	 * @param tutorid
	 */
	setFid : function (fid) {
		this.f_fid.setValue(fid);
	}
});