﻿/**
 * 学科技能竞赛申报书查看界面
 * @author lumingbao
 */
Ext.define('system.zgyxy.xkjnjs.biz.apply.XkjnjsApplyBookView', {
	extend : 'Ext.window.Window',
	oper : Ext.create('system.zgyxy.xkjnjs.biz.apply.XkjnjsApplyOper'),
	projectid : '',
	initComponent:function(){
		var me = this;
		Ext.apply(this,{
			layout : 'fit',
			title: '查看项目申报书',
			width:900,
			height:600,
			bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
			resizable:false,// 改变大小
			buttons : [{
				name : 'btnsave',
				text : '下载申报书',
				iconCls : 'downIcon',
				popedomCode : 'lmbj',
				handler : function() {
					me.oper.downloadProjectApplyBook(me.projectid);
				}
			}, {
				text : '关闭',
				iconCls : 'deleteIcon',
				handler : function() {
					me.close();
				}
			}]
		});
		this.callParent(arguments);
	},

	/**
	 * 设置父级ID
	 * @param tutorid
	 */
	setProjectId : function (projectid) {
		this.projectid = projectid;
		this.html = "<iframe width='100%' height='100%' src='xkjnjs_sbs-"+ projectid +".htm'></iframe>";
	}
});