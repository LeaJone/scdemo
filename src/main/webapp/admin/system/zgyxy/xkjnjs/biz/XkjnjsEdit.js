﻿/**
 * 学科技能竞赛管理编辑界面
 * @author lumingbao
 */
Ext.define('system.zgyxy.xkjnjs.biz.XkjnjsEdit', {
	extend : 'system.widget.FsdFormPanel',
    grid : null,
	requires : [ 'system.zgyxy.team.model.Extjs_Z_team',
                 'system.zgyxy.team.model.Extjs_Z_teamanduser',
				 'system.abasis.employee.model.Extjs_A_employee',
        'system.zgyxy.xkjnjs.model.Extjs_Column_Xkjnjs',
        'system.zgyxy.xkjnjs.model.Extjs_Model_Xkjnjs'],
    oper : Ext.create('system.zgyxy.xkjnjs.biz.XkjnjsOper'),
	
	initComponent : function() {
		var me = this;

        var id = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '编号',
            name : 'id'
        });

        var f_gamename = Ext.create('system.widget.FsdTextField',{
            fieldLabel: '竞赛名称',
            labelAlign:'right',
            labelWidth:90,
            width : 900,
            name: 'f_gamename',
            maxLength: 500,
            allowBlank: false
        });

        var f_name = Ext.create('system.widget.FsdTextField',{
            fieldLabel: '项目名称',
            labelAlign:'right',
            labelWidth:90,
            width : 450,
            name: 'f_name',
            maxLength: 500,
            allowBlank: false
        });
        var f_profession = Ext.create('system.widget.FsdTextField',{
            fieldLabel: '参赛专业',
            labelAlign:'right',
            labelWidth:90,
            width : 450,
            name: 'f_profession',
            maxLength: 20,
            allowBlank: false
        });

        me.f_gamelevelid = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'竞赛级别',
            labelAlign:'right',
            labelWidth:90,
            width : 450,
            name : 'f_gamelevelid',//提交到后台的参数名
            key : 'xkjsjs-jb',//参数
            allowBlank: false,
            hiddenName : 'f_gamelevelname'//提交隐藏域Name
        });
        var f_gamelevelname = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '竞赛级别名称',
            name: 'f_gamelevelname'
        });
        var f_gametypeid = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'竞赛类别',
            labelAlign:'right',
            labelWidth:90,
            width : 450,
            name : 'f_gametypeid',//提交到后台的参数名
            key : 'xkjnjs-lb',//参数
            allowBlank: false,
            hiddenName : 'f_gametypename'//提交隐藏域Name
        });
        var f_gametypename = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '竞赛类别名称',
            name: 'f_gametypename'
        });


        var f_hostunit = Ext.create('system.widget.FsdTextField',{
            fieldLabel: '主办单位',
            labelAlign:'right',
            labelWidth:90,
            width : 450,
            name: 'f_hostunit',
            maxLength: 20,
            allowBlank: false
        });
        var f_undertaker = Ext.create('system.widget.FsdTextField',{
            fieldLabel: '承办单位',
            labelAlign:'right',
            labelWidth:90,
            width : 450,
            name: 'f_undertaker',
            maxLength: 20,
            allowBlank: false
        });

        var f_formid = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'参赛形式',
            labelAlign:'right',
            labelWidth:90,
            width : 450,
            name : 'f_formid',//提交到后台的参数名
            key : 'xkjnjs-xs',//参数
            allowBlank: false,
            hiddenName : 'f_formname'//提交隐藏域Name
        });
        var f_formname = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '参赛形式名称',
            name: 'f_formname'
        });
        var f_gametime = Ext.create('system.widget.FsdDateField',{
            fieldLabel: '竞赛时间',
            labelAlign:'right',
            labelWidth:90,
            width : 450,
            name: 'f_gametime',
            allowBlank: false
        });

        var f_site = Ext.create('system.widget.FsdTextField',{
            fieldLabel: '竞赛地点',
            labelAlign:'right',
            labelWidth:90,
            width : 450,
            name: 'f_site',
            maxLength: 20,
            allowBlank: false
        });

        var f_basename = Ext.create('system.widget.FsdTextField',{
            fieldLabel: '依托基地',
            labelAlign:'right',
            labelWidth:90,
            width : 450,
            name: 'f_basename',
            maxLength: 20,
            allowBlank: false
        });

        var f_baseleadername = Ext.create('system.widget.FsdTextField',{
            fieldLabel: '基地负责人',
            labelAlign:'right',
            labelWidth:90,
            width : 450,
            name: 'f_baseleadername',
            maxLength: 20,
            allowBlank: false
        });


        var f_applyformoney = Ext.create('system.widget.FsdTextField',{
            fieldLabel: '申请经费',
            labelAlign:'right',
            labelWidth:90,
            width : 450,
            name: 'f_applyformoney',
            maxLength: 20,
            allowBlank: false
        });

        var f_matingmoney = Ext.create('system.widget.FsdTextField',{
            fieldLabel: '配套经费',
            labelAlign:'right',
            labelWidth:90,
            width : 450,
            name: 'f_matingmoney',
            maxLength: 20,
            allowBlank: false
        });
        var f_matingmoneysource = Ext.create('system.widget.FsdTextField',{
            fieldLabel: '配套经费来源',
            labelAlign:'right',
            labelWidth:90,
            width : 450,
            name: 'f_matingmoneysource',
            maxLength: 20,
            allowBlank: false
        });


        var f_group = Ext.create('system.widget.FsdTextField',{
            fieldLabel: '参赛队数',
            labelAlign:'right',
            labelWidth:90,
            width : 450,
            name: 'f_group',
            maxLength: 20,
            allowBlank: false
        });
        var f_people = Ext.create('system.widget.FsdTextField',{
            fieldLabel: '参赛人数',
            labelAlign:'right',
            labelWidth:90,
            width : 450,
            name: 'f_people',
            maxLength: 20,
            allowBlank: false
        });

        var f_firstteachername = Ext.create('system.widget.FsdTextGrid', {
            zFieldLabel : '第一指导教师',
            zLabelWidth : 90,
            width : 450,
            zName : 'f_firstteachername',
            zColumn : 'column_project_employee', // 显示列
            zModel : 'model_a_employee',
            zBaseUrl : 'A_Employee/loadDataByType.do',
            zGridType : 'page',
            zGridWidth : 700,//弹出表格宽度
            zGridHeight : 600,//弹出表格高度
            zIsText1 : true,
            zTxtLabel1 : '第一指导教师姓名',
            zAllowBlank : false,
            zFunChoose : function(data){
                f_firstteacherid.setValue(data.id);
                f_firstteachername.setValue(data.realname);
            },
            zFunQuery : function(txt1){
                return {type : 'js', name : txt1};
            }
        });
        var f_firstteacherid = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '第一指导教师',
            name: 'f_firstteacherid'
        });

        var f_secondteachername = Ext.create('system.widget.FsdTextGrid', {
            zFieldLabel : '第二指导教师',
            zLabelWidth : 90,
            width : 450,
            zName : 'f_secondteachername',
            zColumn : 'column_project_employee', // 显示列
            zModel : 'model_a_employee',
            zBaseUrl : 'A_Employee/loadDataByType.do',
            zGridType : 'page',
            zGridWidth : 700,//弹出表格宽度
            zGridHeight : 600,//弹出表格高度
            zIsText1 : true,
            zTxtLabel1 : '第二指导教师姓名',
            zFunChoose : function(data){
                f_secondteacherid.setValue(data.id);
                f_secondteachername.setValue(data.realname);
            },
            zFunQuery : function(txt1){
                return {type : 'js', name : txt1};
            }
        });
        var f_secondteacherid = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '第二指导教师',
            name: 'f_secondteacherid'
        });
	    
	    var f_starttime = Ext.create('system.widget.FsdDateField',{
	    	fieldLabel: '开始时间',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 450,
	    	name: 'f_starttime',
            allowBlank: false
	    });

        var f_stoptime = Ext.create('system.widget.FsdDateField',{
            fieldLabel: '结束时间',
            labelAlign:'right',
            labelWidth:90,
            width: 450,
            name: 'f_stoptime',
            vtype : 'dateBetween',
            vtypeField : f_starttime,
            allowBlank: false
        });

        var f_abstract = Ext.create('system.widget.FsdTextArea',{
            fieldLabel: '竞赛内容简介',
            labelAlign: 'right',
            labelWidth: 90,
            width: 900,
            height: 200,
            name: 'f_abstract',
            allowBlank: false
        });
	    
	    var f_scheme = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '竞赛实施方案及预期目标',
	    	labelAlign: 'right',
	    	labelWidth: 90,
	        width: 900,
	        height: 200,
	    	name: 'f_scheme',
            allowBlank: false
	    });

        var f_condition = Ext.create('system.widget.FsdTextArea',{
            fieldLabel: '配套条件(设备、仪器、场地、人员等)',
            labelAlign:'right',
            labelWidth:90,
            width : 900,
            height : 200,
            name: 'f_condition',
            allowBlank: false
        });

        var f_train = Ext.create('system.widget.FsdTextArea',{
            fieldLabel: '培训方案',
            labelAlign:'right',
            labelWidth:90,
            width : 900,
            height : 200,
            name: 'f_train',
            allowBlank: false
        });

	    me.form = Ext.create('Ext.form.Panel',{
            tbar : [{
                xtype : "fsdbutton",
                popedomCode : 'projectbj',
                text : "保存草稿",
                name : 'btnsave1',
                iconCls : 'drafts',
                handler : function() {
                    me.oper.formSubmit(me, me.grid, "projectstate-cg");
                }
            }, {
                xtype : "fsdbutton",
                popedomCode : 'projectbj',
                text : "提 交",
                name : 'btnsave2',
                iconCls : 'acceptIcon',
                handler : function() {
                    me.oper.formSubmit(me, me.grid, "projectstate-tjwsh");
                }
            }],
			border : false,
            padding : '5 0 30 10',
			items : [id, f_gamelevelname, f_gametypename, f_formname, f_gamename,f_firstteacherid, f_secondteacherid,
                {
				layout : 'column',
				border : false,
				width : 900,
				items:[{
					columnWidth : 0.5,
					border:false,
					items:[f_name ,me.f_gamelevelid, f_hostunit, f_formid, f_site, f_baseleadername, f_matingmoney, f_group, f_firstteachername]
				}, {
					columnWidth : 0.5,
					border:false,
					items:[f_profession, f_gametypeid, f_undertaker, f_gametime, f_basename, f_applyformoney, f_matingmoneysource, f_people, f_secondteachername]
				}]
			}, f_abstract, f_scheme, f_condition, f_train]
	    });

        // 创建表格
        var budgetgrid = Ext.create("system.widget.FsdGridPagePanel", {
            column : 'column_z_xkjnjs',
            model : 'model_z_xkjnjs',
            baseUrl : 'z_xkjnjs/load_pagedata.do',
            border : false,
            tbar : [{
                xtype : "fsdbutton",
                text : "竞赛申报",
                iconCls : 'add',
                popedomCode : 'projectbj',
                handler : function() {
                    me.oper.add(grid, "cxxlxm");
                }
            }, ,'-', {
                xtype : "fsdbutton",
                text : "编辑",
                iconCls : 'page_editIcon',
                popedomCode : 'projectbj',
                handler : function() {
                    me.oper.editor(grid);
                }
            }, {
                text : '查看',
                iconCls : 'magnifierIcon',
                handler : function() {
                    me.oper.view(grid);
                }
            }, {
                xtype : "fsdbutton",
                text : "删除",
                iconCls : 'page_deleteIcon',
                popedomCode : 'projectsc',
                handler : function() {
                    me.oper.del(grid);
                }
            },  '->', {
                text : '刷新',
                iconCls : 'tbar_synchronizeIcon',
                handler : function() {
                    grid.getStore().reload();
                }
            }]
        });

        me.budgetPanel = Ext.create('Ext.panel.Panel',{
            title: '预算信息',
            layout : 'fit',
            disabled : true,
            items : [me.budgetgrid]
        });

        var tabpanel = Ext.create('Ext.tab.Panel', {
            border: 0,
            enableTabScroll: true,
            deferredRender: false,
            defaults: { autoScroll: true },
            items: [{
                border: 0,
                title: '基本信息',
                items: [me.form]
            }, me.budgetPanel]
        });

		Ext.apply(me, {
        	title: '竞赛申报',
			autoScroll : true,
	        items :[tabpanel]
	    });
		me.callParent(arguments);
	}
});