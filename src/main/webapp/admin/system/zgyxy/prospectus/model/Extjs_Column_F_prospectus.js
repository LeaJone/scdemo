﻿/**
 *  f_prospectus Column
 */

Ext.define('column_f_prospectus', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '项目名称',
            dataIndex : 'f_projectname',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '提交人',
            dataIndex : 'f_submitusername',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '状态',
            dataIndex : 'f_submitstatus',
            sortable : true,
            flex : 2,
            renderer :function(value ,metaData ,record ){
				if(value == 'wtj'){
					value="<font style='color:red;'>未提交</font>";
				}else if(value == 'ytj'){
					value="<font style='color:green;'>已提交</font>";
				}
				return value;
			}
        }
        ,
        {
            header : '提交时间',
            dataIndex : 'f_submittime',
            sortable : true,
            flex : 4,
			renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value);
			}
        }
    ]
});