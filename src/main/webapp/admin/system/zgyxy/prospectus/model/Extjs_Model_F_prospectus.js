﻿﻿/**
 *f_prospectus Model
 */

Ext.define('model_f_prospectus', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'f_projectname',
            type : 'string'
        }
        ,
        {
            name : 'f_projectcontent',
            type : 'string'
        }
        ,
        {
            name : 'f_submitusername',
            type : 'string'
        }
        ,
        {
            name : 'f_submittime',
            type : 'string'
        }
        ,
        {
            name : 'f_submitstatus',
            type : 'string'
        }
        ,
        {
            name : 'f_accessory',
            type : 'string'
        }
    ]
});