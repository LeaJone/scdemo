﻿/**
 * 执行计划书管理界面
 */

Ext.define('system.zgyxy.prospectus.biz.ProspectusManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*', 
	             'system.zgyxy.prospectus.model.Extjs_Column_F_prospectus',
	             'system.zgyxy.prospectus.model.Extjs_Model_F_prospectus'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.zgyxy.prospectus.biz.ProspectusOper'),
	
	createContent : function(){
		var me = this;
		
		var queryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入项目名称',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});
		
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_f_prospectus', // 显示列
			model: 'model_f_prospectus',
			baseUrl : 'f_prospectus/load_PageData.do',
			border : false,
			tbar : [{
				xtype : "fsdbutton",
				popedomCode : 'nrwzbj',
				text : "添加执行计划书",
				iconCls : 'add',
				handler : function(button) {
				    me.oper.addText(grid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'nrwzbj',
				text : "编辑",
				iconCls : 'page_editIcon',
				handler : function(button) {
				    me.oper.addText(grid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'rytj',
				text : "查看",
				iconCls : 'magnifierIcon',
				handler : function(button) {
				    me.oper.view(grid);
				}
			}, {
				xtype : "fsdbutton",
				text : "删除",
				iconCls : 'page_deleteIcon',
				handler : function(button) {
					me.oper.deleted(grid);
				}
			}, '->', queryText, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]
		});
		
		grid.getStore().on('beforeload', function(s) {
			var title =  queryText.getValue();
			var pram = {title : title};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		return grid;
	}
});