﻿/**
 * 计划书添加界面
 */
Ext.define('system.zgyxy.prospectus.biz.ProspectusEdit', {
	extend : 'Ext.window.Window',
	oper : Ext.create('system.zgyxy.prospectus.biz.ProspectusOper'),
	requires : [ 'system.zgyxy.project.model.Extjs_Column_Project',
                 'system.zgyxy.project.model.Extjs_Model_Project'],
	
	initComponent:function(){
	    var me = this;
	    
	    me.projectname = Ext.create('system.widget.FsdTextGrid', {
	    	zFieldLabel : '项目名称', 
	    	zLabelWidth : 80,
	    	width : 450,
		    zName : 'f_projectname',
		    zColumn : 'column_z_projectprospectus', // 显示列
		    zModel : 'model_z_project',
		    zBaseUrl : 'z_project/load_pagedataprospectus.do',
		    zAllowBlank : false,
		    zGridType : 'page',
			zGridWidth : 1000,//弹出表格宽度
			zGridHeight : 700,//弹出表格高度
		    zIsText1 : true,
		    zTxtLabel1 : '项目名称',
		    zFunChoose : function(data){
		    	me.projectid.setValue(data.id);
		    	me.projectname.setValue(data.f_name);
		    },
		    zFunQuery : function(txt1, txt2, txt3, txt4){
		    	return {fid : 'FSDCOMPANY', txt : txt1};
		    }
	    });
		me.projectid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '项目编号',
	    	name: 'f_projectid'
	    });
	     
	    me.content = Ext.create('system.widget.FsdUeditor',{
	    	fieldLabel: '详细内容',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 920,
	        height : 400,
	    	name: 'f_projectcontent',
	    	padding: '0 0 20 0'
	    });
	   
	    me.accessory = Ext.create('system.widget.FsdTextFileManage', {
	        width : 450,
	    	zName : 'f_accessory',
	    	zFieldLabel : '附件',
	    	zLabelWidth : 80,
	    	zIsShowButton : true,
	    	zIsUpButton : true,
	    	zIsReadOnly : true,
	    	zFileType : 'file',
	    	zFileAutoName : true,
	    	zFileUpPath : me.oper.util.getVideoPath(),
	    	zManageType : 'upload'
	    });
	    
	    me.on('show' , function(){
	    	//sslm.setText(sslmname.getValue());
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 20 10 20',
			items : [{
                name: "id",
                xtype: "hidden"
            } , me.projectname, me.projectid, me.content, me.accessory]
        });
	    
	    Ext.apply(this,{
	    	width : 1000,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable : false,// 改变大小
	        items : [form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
			    handler : function() {
    				me.oper.saveMain(form);
			    }
		    }, {
	        	name : 'btnsave',
			    text : '提交',
			    iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
			    handler : function() {
    				me.oper.submitMain(form);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});