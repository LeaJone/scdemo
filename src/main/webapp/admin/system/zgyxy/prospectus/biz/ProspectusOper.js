/**
 * 执行计划书操作类
 */

Ext.define('system.zgyxy.prospectus.biz.ProspectusOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 添加项目执行计划书
     */
	addText : function(grid){
		var win = Ext.create('system.zgyxy.prospectus.biz.ProspectusEdit');
        win.setTitle('添加项目执行计划书');
        win.grid = grid;
        win.modal = true;
        win.show();
	},
	
	/**
     * 保存项目执行计划任务书
     */
	saveMain : function(formpanel , grid , treepanel){
		this.formSave(formpanel , grid, treepanel, 'f_prospectus/save_prospectus_first.do');
	},
	formSave : function(formpanel , grid , treepanel, url){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit(url, formpanel.form, '正在保存项目执行计划书,请稍候.....',
			function(form, action){
			    formpanel.up('window').close();
			    grid.getStore().reload();
			    treepanel.getStore().load();
				Ext.MessageBox.alert('提示', '项目执行计划任务书保存成功！');
			});
        }
     },
	
	/**
     * 提交项目执行计划书
     */
	submitMain : function(formpanel , grid , treepanel){
		this.formSubmit(formpanel , grid, treepanel, 'f_prospectus/save_prospectus.do');
	},
	formSubmit : function(formpanel , grid , treepanel, url){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit(url, formpanel.form, '正在提交项目执行计划书,请稍候.....',
			function(form, action){
			    formpanel.up('window').close();
			    grid.getStore().reload();
			    treepanel.getStore().load();
				Ext.MessageBox.alert('提示', '项目执行计划书提交成功！');
			});
        }
     },
	
	/**
     * 删除项目执行计划书
     */
	deleted : function(grid){
   	 var me = this;
		 // 获取选中的行
	     var data = grid.getSelectionModel().getSelection();
         if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要删除的执行计划书！');
         }else {
       	 Ext.MessageBox.show({
       		 title : '询问',
       		 msg : '您确定要删除执行计划书吗?',
			     width : 250,
				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                    if (btn == 'yes') {
                   	    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
            			});
            			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('f_prospectus/deleted_prospectus.do' , param ,
                        function(response, options){
                       	grid.getStore().reload();
                       });
                    }
                }
            });
        }

	},
	
	/**
	 * 编辑项目执行计划任务书
	 */
	editorMain : function(grid , treepanel){
		this.editor(grid, treepanel, 'system.zgyxy.prospectus.biz.ProspectusEdit', '编辑项目执行计划书');
	},
	
	editor : function(grid, treepanel, url, title){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要编辑的项目执行计划书！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能编辑一个项目执行计划书！');
        }else{

            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            
            var win = Ext.create(url);
            win.setTitle(title);
            win.modal = true;
            win.grid = grid;
            win.treepanel =  treepanel;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'f_prospectus/load_ProspectusById.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},
	
	/**
	 * 查看项目执行计划任务书
	 */
	viewMain : function(grid){
		this.view(grid, 'system.zgyxy.prospectus.biz.ProspectusEdit', '查看项目执行计划书');
	},
 	
	view : function(grid, url, title){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要查看的计划书！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能查看一份计划书！');
        }else{

            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            
            var win = Ext.create(url);
            win.setTitle(title);
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'f_prospectus/load_ProspectusById.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},
});