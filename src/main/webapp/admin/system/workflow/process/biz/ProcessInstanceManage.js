﻿/**

 * 流程实例管理界面
 * @author lumingbao
 */

Ext.define('system.workflow.process.biz.ProcessInstanceManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.workflow.process.model.Extjs_Column_processInstance',
	             'system.workflow.process.model.Extjs_Model_processInstance'],
	header : false,
	border : 0,
	layout : 'fit',
	key:'',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.workflow.process.biz.ProcessInstanceOper'),
	
	createContent : function(){
		var me = this;

		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPanel", {
			column : 'column_processInstance', // 显示列
			model: 'model_processInstance',
			baseUrl : 'f_activiti/load_processInstancePageData.do',
			border: 0,
			tbar : [{
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var pram = {key : me.key};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});

		return grid;
	}
});