﻿/**

 * 流程定义管理界面
 * @author lumingbao
 */

Ext.define('system.workflow.process.biz.ProcessDefinitionManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.workflow.process.model.Extjs_Column_processDefinition',
	             'system.workflow.process.model.Extjs_Model_processDefinition'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.workflow.process.biz.ProcessDefinitionOper'),
	
	createContent : function(){
		var me = this;

		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_processDefinition', // 显示列
			model: 'model_processDefinition',
			baseUrl : 'f_activiti/load_processDefinitionPageData.do',
			border: 0,
			tbar : [{
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});

		return grid;
	}
});