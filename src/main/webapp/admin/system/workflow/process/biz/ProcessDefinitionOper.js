/**
 * 流程定义管理操作类
 */
Ext.define('system.workflow.process.biz.ProcessDefinitionOper', {

	form : null,
	util : Ext.create('system.util.util'),

    /**
     * 查看流程实例
     * @param grid
     */
    viewProcessInstance : function(name, key){
        var me = this;
        var model = Ext.create("system.workflow.process.biz.ProcessInstanceManage");
        model.key = key;
        me.util.addTab("viewProcessInstance" , name + "-流程实例" , model , 'fa-edit');
    }
});