﻿Ext.define('model_processInstance', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'businessKey',
            type : 'string'
        }
        ,
        {
            name : 'processDefinitionId',
            type : 'string'
        }
        ,
        {
            name : 'startTime',
            type : 'string'
        }
        ,
        {
            name : 'endTime',
            type : 'string'
        }
        ,
        {
            name : 'durationInMillis',
            type : 'string'
        }
        ,
        {
            name : 'state',
            type : 'string'
        }
        ,
        {
            name : 'activiti',
            type : 'string'
        }
    ]
});
