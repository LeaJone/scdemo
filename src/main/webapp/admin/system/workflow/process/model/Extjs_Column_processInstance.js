﻿Ext.define('column_processInstance', {
    oper : Ext.create('system.workflow.process.biz.ProcessInstanceOper'),
    columns: [
          {
              xtype : 'rownumberer',
              text : 'NO',
              sortable : true,
              width : 40
          }
          ,
        {
            header : '开始时间',
            dataIndex : 'startTime',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '结束时间',
            dataIndex : 'endTime',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '总耗时',
            dataIndex : 'durationInMillis',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '状态',
            dataIndex : 'state',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '当前节点',
            dataIndex : 'activiti',
            sortable : true,
            flex : 2
        }
         ,
        {
            xtype: 'gridcolumn',
            width: 500,
            dataIndex: 'operate',
            text: '操作',
            align: 'center',
            renderer: function (value, metadata, record, rowIndex, columnIndex, store) {

            }
        }
    ]
});