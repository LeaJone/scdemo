﻿/**
 *process Column
 *@author CodeSystem
 *文件名     Extjs_Column_process
 */

Ext.define('column_processDefinition', {
    columns: [
          {
              xtype : 'rownumberer',
              text : 'NO',
              sortable : true,
              width : 40
          }
        ,
        {
            header : '流程定义ID',
            dataIndex : 'id',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '流程定义KEY',
            dataIndex : 'key',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '流程定义部署id',
            dataIndex : 'deploymentid',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '流程定义名称',
            dataIndex : 'name',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : 'bpmn资源名称',
            dataIndex : 'resourcename',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : 'png资源名称',
            dataIndex : 'diagramresourcename',
            sortable : true,
            flex : 2
        }
        ,
        {
            xtype: 'gridcolumn',
            width: 500,
            dataIndex: 'operate',
            text: '操作项',
            align: 'center',
            renderer: function (value, metadata, record, rowIndex, columnIndex, store) {
                var oper = Ext.create('system.workflow.process.biz.ProcessDefinitionOper');

                var key = store.getAt(rowIndex).get('key');
                var name = store.getAt(rowIndex).get('name');
                var grid = this;

                var viewButton = Ext.create('Ext.button.Button', {
                    height: 19,
                    text: "<span style='color:#ffffff;font-weight: normal;line-height: 15px;'>查看实例</span>",
                    style: 'background:#157fcc;border:none;margin-right:5px;',
                    iconCls: 'fa fa-play-circle gridMenuIcon',
                    handler: function () {
                        oper.viewProcessInstance(name, key);
                    }
                });

                var id = metadata.record.id;//Ext.id();
                Ext.defer(function () {
                    Ext.widget('buttongroup', {
                        renderTo: id,
                        style: 'margin:0',
                        ui: 'footer',
                        items: [viewButton]
                    });
                }, 50);
                return Ext.String.format('<div id="{0}"></div>', id);
            }
        }
    ]
});