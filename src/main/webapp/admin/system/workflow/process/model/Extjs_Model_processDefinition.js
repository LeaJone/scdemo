﻿/**
 *process Model
 *@author CodeSystem
 *文件名     Extjs_Model_process
 *Model名    model_process
 */

Ext.define('model_processDefinition', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'key',
            type : 'string'
        }
        ,
        {
            name : 'deploymentid',
            type : 'string'
        }
        ,
        {
            name : 'name',
            type : 'string'
        }
        ,
        {
            name : 'resourcename',
            type : 'string'
        }
        ,
        {
            name : 'diagramresourcename',
            type : 'string'
        }
    ]
});
