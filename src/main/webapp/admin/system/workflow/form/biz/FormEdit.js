﻿/**
 * 表单添加界面
 * @author lumingbao
 */
Ext.define('system.workflow.form.biz.FormEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.workflow.form.biz.FormOper'),
	initComponent:function(){
	    var me = this;

        var key = new Ext.create('system.widget.FsdTextFieldZJF',{
            fieldLabel: '显示名称',
            labelAlign:'right',
            labelWidth:80,
            width : 360,
            name: 'f_displayname',
            fname : 'f_name',
            maxLength: 200,
            allowBlank: false
        });

	    var name = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '表单名称',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'f_name',
	    	maxLength: 200,
	    	allowBlank: false
	    });



		var wordtemplate = Ext.create('system.widget.FsdTextFileManage', {
			width : 360,
			zName : 'f_wordtemplate',
			zFieldLabel : 'word模板',
			zLabelWidth : 80,
			zIsShowButton : false,
			zIsUpButton : true,
			zIsReadOnly : false,
			zFileType : 'file',
			zFileAutoName : false,
			zFileUpPath : me.oper.util.getFilePath(),
			zManageType : 'upload'
		});



	    var description = new Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '表单描述',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'f_description',
	    	maxLength: 250,
            allowBlank: false
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 20 10 20',
			items : [{
                name: "id",
                xtype: "hidden"
            }, key, name, wordtemplate, description]
        });
	    
	    Ext.apply(this,{
            layout : 'fit',
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
			    handler : function() {
    				me.oper.formSubmit(form, me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});