﻿/**
 * 表单设计界面
 * @author lumingbao
 */
Ext.define('system.workflow.form.biz.FormDesign', {
    extend : 'Ext.window.Window',
    formid: '',
    designHtml:'',
    grid : null,
    oper : Ext.create('system.workflow.form.biz.FormOper'),
    initComponent:function(){
        var me = this;

        var toolbars = [['source', '|', 'undo', 'redo', '|','bold', 'italic', 'underline', 'fontborder', 'strikethrough',  'removeformat', '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist','|', 'fontfamily', 'fontsize', '|', 'indent', '|', 'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|',  'link', 'unlink',  '|',  'horizontal',  'spechars',  'wordimage', '|', 'inserttable', 'deletetable',  'mergecells',  'splittocells']];

        var name = Ext.create('system.widget.FsdUeditorFormDesign',{
            fieldLabel: '',
            toolbars: toolbars,
            labelAlign:'right',
            labelWidth:80,
            width : 800,
            height : 500,
            name: 'content'
        });

        name.setValue(this.designHtml);

        var form = Ext.create('Ext.form.Panel',{
            xtype : 'form',
            defaultType : 'textfield',
            header : false,// 是否显示标题栏
            border : 0,
            padding : '20 20 10 20',
            items : [{
                name: "id",
                xtype: "hidden"
            }, name]
        });

        var tbar = Ext.create("Ext.Toolbar", {
            items: [{
                xtype: "button",
                text: "文本框",
                handler: function () {
                    name.formDesign.exec('text');
                }
            }, {
                xtype: "button",
                text: "时间控件",
                handler: function () {
                    name.formDesign.exec('time');
                }
            }, {
                xtype: "button",
                text: "多行文本",
                handler: function () {
                    name.formDesign.exec('textarea');
                }
            }, {
                xtype: "button",
                text: "下拉菜单",
                handler: function () {
                    name.formDesign.exec('select');
                }
            }, {
                xtype: "button",
                text: "单选框",
                handler: function () {
                    name.formDesign.exec('radios');
                }
            }, {
                xtype: "button",
                text: "复选框",
                handler: function () {
                    name.formDesign.exec('checkboxs');
                }
            }, {
                xtype: "button",
                text: "上传控件",
                handler: function () {
                    name.formDesign.exec('upload');
                }
            }]
        });

        Ext.apply(this,{
            layout : 'fit',
            bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
            resizable:false,// 改变大小
            tbar: tbar,
            items :[form],
            buttons : [{
                name : 'btnsave',
                text : '保存',
                iconCls : 'acceptIcon',
                popedomCode : 'lmbj',
                handler : function() {
                    var parse_form = name.formDesign.fnCheckForm('save');
                    if(parse_form != null && parse_form != ""){
                        me.oper.designFormSubmit(me, me.formid, parse_form, me.grid);
                    }
                }
            }, {
                text : '关闭',
                iconCls : 'deleteIcon',
                handler : function() {
                    me.close();
                }
            }]
        });
        this.callParent(arguments);
    }
});