/**
 * 表单管理操作类
 */
Ext.define('system.workflow.form.biz.FormOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 添加
     */
	add : function(grid){
	    var win = Ext.create('system.workflow.form.biz.FormEdit', {
	    	grid : grid
	    });
        win.setTitle('创建表单');
        win.modal = true;
        win.show();
	},

    /**
     * 修改
     */
    editor : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            me.editorById(id, grid);
        }
    },

    /**
     * 通过ID修改对象
     * @param id
     */
    editorById : function(id, grid){
        var me = this;
        //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
        var win = Ext.create('system.workflow.form.biz.FormEdit');
        win.setTitle('修改表单');
        win.modal = true;
        win.grid = grid;
        var pram = {id : id};
        var param = {jsonData : Ext.encode(pram)};
        me.util.formLoad(win.down('form').getForm() , 'f_form/load_f_formbyid.do' , param , null ,
            function(response, options, respText){
                win.show();
            });
    },

    /**
     * 通过ID查看对象
     * @param id
     * @param grid
     */
    viewById : function(id, grid){
        var me = this;
        //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
        var win = Ext.create('system.workflow.form.biz.FormEdit');
        win.setTitle('修改表单');
        win.modal = true;
        win.grid = grid;
        win.down('button[name=btnsave]').setVisible(false);
        var pram = {id : id};
        var param = {jsonData : Ext.encode(pram)};
        me.util.formLoad(win.down('form').getForm() , 'f_form/load_f_formbyid.do' , param , null ,
            function(response, options, respText){
                win.show();
            });
    },

    /**
     * 表单设计
     * @param id
     * @param grid
     */
    design : function(id, grid, html){
        var win = Ext.create('system.workflow.form.biz.FormDesign', {
            formid: id,
            designHtml:html,
            grid : grid
        });
        win.setTitle('设计表单');
        win.modal = true;
        win.show();
    },

    /**
     * 使用表单
     * @param id
     * @param html
     */
    use : function(grid, id, taskid, employeeid){
        var me = this;
        var data = grid.getSelectionModel().getSelection();
        var isBing = data[0].data.column5;
        var formmodel = null;
        var pram = {id : id};
        var param = {jsonData : Ext.encode(pram)};
        me.util.ExtAjaxRequest('f_form/load_f_formbyid.do', param, function(response){
            formmodel = JSON.parse(response.responseText);
            if(formmodel.data.f_parsehtml != null && formmodel.data.f_parsehtml != ""){
                var htmlStr = "<form name='formusediv' class='fsd-form-design'>" + formmodel.data.f_parsehtml + "</form>";
                var win = new Ext.create('system.widget.FsdFormUse',{
                    htmlStr: htmlStr,
                    formid: id,
                    taskid:taskid,
                    employeeid:employeeid
                });
                if(isBing == null || isBing == ""){
                    win.down('button[name=btnExport]').setVisible(false);
                }
                win.setTitle('录入数据');
                win.modal = true;
                win.show();

                $(".fsdtimefield").each(function(){
                    laydate.render({
                        elem: this
                    });
                });

                layui.use('upload', function(){
                    var upload = layui.upload;
                    $(".fsduploadfield").each(function(){
                        var filetype = $(this).attr("filetype");
                        var fieldname = $(this).attr("fieldname");
                        upload.render({
                            elem: this
                            ,url: "Common/layuiupload/"+ filetype +""
                            ,accept: filetype //普通文件
                            ,done: function(res){
                                if(res.code == 0){
                                    var filepath = res.filepath;
                                    var hiddenControl = $("input[type=hidden][name='"+ fieldname +"']")[0];
                                    if(hiddenControl == null){
                                        Ext.MessageBox.alert("提示", "上传控件异常");
                                    }else{
                                        $(hiddenControl).val(filepath);
                                        Ext.MessageBox.alert("提示", "上传成功");
                                    }
                                }else{
                                    Ext.MessageBox.alert("提示", "上传失败");
                                }
                            }
                        });
                    });
                });

            }else{
                Ext.MessageBox.alert("提示", "请先设计表单");
            }
        }, null, null);

    },

    /**
     * 查看历史表单数据
     * @param grid
     * @param taskid
     * @param userid
     * @param formlist
     */
    viewFinishForm : function(grid, taskid, userid, formlist){
        var me = this;
        var buttonGroup = Ext.widget('buttongroup', {
            style: 'margin:5'
        });
        for(var i = 0;i<formlist.length;i++){
            buttonGroup.add(Ext.create('Ext.button.Button', {
                height: 19,
                text: "<span style='color:#ffffff;font-weight: normal;line-height: 15px;'>查看"+ formlist[i].f_displayname +"</span>",
                style: 'background:#157fcc;border:none;margin-right:5px;',
                iconCls: 'fa fa-play-circle gridMenuIcon',
                handler: function (button) {
                    for(var j = 0; j<buttonGroup.items.keys.length; j++){
                        var id = buttonGroup.items.keys[j];
                        var idd = button.getId();
                        if(id == idd){
                            me.use(grid, formlist[j].id, taskid, userid);
                        }
                    }
                }
            }));
        }
        Ext.create("Ext.window.Window", {
            title : '查看历史表单数据',
            layout : 'fit',
            modal : true,
            resizable:false,// 改变大小
            items: [buttonGroup]
        }).show();
    },

    /**
     * 删除
     */
    del : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要删除的项目！');
        }else {
            Ext.MessageBox.show({
                title : '询问',
                msg : '您确定要删除吗?',
                width : 250,
                buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                        var dir = new Array();
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
                        });
                        me.delById(dir, grid);
                    }
                }
            });
        }
    },

    /**
     * 通过ID删除对象
     * @param dir
     * @param grid
     */
    delById : function(dir, grid){
        var me = this;
        var pram = {ids : dir};
        var param = {jsonData : Ext.encode(pram)};
        me.util.ExtAjaxRequest('f_form/del_f_form_qx_f_formsc.do' , param ,
            function(response, options){
                grid.getStore().reload();
            }, null, me.form);
    },

    /**
     * 表单提交
     */
    designFormSubmit : function(window, formid , parse_form, grid){
        var me = this;
        var pram = {formid : formid, parse_form: parse_form};
        var param = {jsonData : Ext.encode(pram)};
        me.util.ExtAjaxRequest('f_form/form_savedesign.do' , param ,
            function(response, options){
                window.close();
                grid.getStore().reload();
                Ext.MessageBox.alert('提示', '保存成功！');
            }, null, null);
    },

    /**
     * 表单提交
     */
    formSubmit : function(formpanel , grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('f_form/save_f_form_qx_f_formbj.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action){
                    formpanel.up('window').close();
                    grid.getStore().reload();
                    Ext.MessageBox.alert('提示', '保存成功！');
                });
        }
    },

    /**
     * 是否启用停用
     */
    isEnabled : function(grid, isQT){
        var me = this;
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
                msg : '您确定要修改表单状态吗？',
                width : 250,
                buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                        var dir = new Array();
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
                        });
                        var pram = {ids : dir, isenabled : isQT};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('f_form/enabled_form_qx_formbj.do', param, function(){
                            grid.getStore().reload();
                            Ext.MessageBox.alert('提示', '表单状态修改成功！');
                        });
                    }
                }
            });
        }
    }
});