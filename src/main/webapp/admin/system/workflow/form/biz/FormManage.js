﻿/**

 * 表单管理界面
 * @author lumingbao
 */
Ext.define('system.workflow.form.biz.FormManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.workflow.form.model.Extjs_Column_form',
	             'system.workflow.form.model.Extjs_Model_form'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.workflow.form.biz.FormOper'),
	
	createContent : function(){
		var me = this;

		var queryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入表单名称',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});
		
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_form', // 显示列
			model: 'model_form',
			baseUrl : 'f_form/load_pagedata.do',
			border: 0,
			tbar : [{
				xtype : "button",
				text : "创建表单",
				iconCls : 'page_addIcon',
				popedomCode : 'lmbj',
				handler : function(button) {
					me.oper.add(grid);
				}
			}, {
                xtype : "fsdbutton",
                popedomCode : 'dwbj',
                text : "修改",
                iconCls : 'page_editIcon',
                handler : function() {
                    me.oper.editor(grid);
                }
            }, {
                xtype : "fsdbutton",
                popedomCode : 'dylbsc',
                text : "删除",
                iconCls : 'page_deleteIcon',
                handler : function(button) {
                    me.oper.del(grid);
                }
            }, '-', {
                xtype : "fsdbutton",
                popedomCode : 'jgbj',
                text : "启用",
                iconCls : 'tickIcon',
                handler : function() {
                    me.oper.isEnabled(grid, true);
                }
            }, {
                xtype : "fsdbutton",
                popedomCode : 'jgbj',
                text : "停用",
                iconCls : 'crossIcon',
                handler : function() {
                    me.oper.isEnabled(grid, false);
                }
            }, '->', queryText , {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  queryText.getValue();
			var pram = {name : name};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});

		return grid;
	}
});