﻿/**
 *form Column
 *@author CodeSystem
 *文件名     Extjs_Column_form
 */

Ext.define('column_form', {
    oper : Ext.create('system.workflow.form.biz.FormOper'),
    columns: [
          {
              xtype : 'rownumberer',
              text : 'NO',
              sortable : true,
              width : 40
          }
          ,
        {
            header : '表单名称',
            dataIndex : 'f_name',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '显示名称',
            dataIndex : 'f_displayname',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '添加时间',
            dataIndex : 'f_adddate',
            sortable : true,
            flex : 2,
            renderer :function(value, metaData, record){
                return system.UtilStatic.formatDateTime(value);
            }
        }
        ,
        {
            header : '绑定word模板',
            dataIndex : 'f_wordtemplate',
            sortable : true,
            flex : 2,
            renderer :function(value ,metaData ,record ){
                if(value == null || value == ""){
                    value = "<font style='color:red;'>未绑定</font>";
                }else{
                    value = "<font style='color:green;'>已绑定</font>";
                }
                return value;
            }
        }
        ,
        {
            header : '状态',
            dataIndex : 'f_status',
            sortable : true,
            flex : 2,
            renderer :function(value ,metaData ,record ){
                if(value == "true"){
                    value = "<font style='color:green;'>已启用</font>";
                }else{
                    value = "<font style='color:red;'>未启用</font>";
                }
                return value;
            }
        }
         ,{
            xtype: 'gridcolumn',
            width: 360,
            dataIndex: 'operate',
            text: '操作',
            align: 'center',
            renderer: function (value, metadata, record, rowIndex, columnIndex, store) {
                var oper = Ext.create('system.workflow.form.biz.FormOper');

                var grid = this;

                //设计按钮
                var designButton = Ext.create('Ext.button.Button', {
                    height: 19,
                    text: "<span style='color:#ffffff;font-weight: normal;line-height: 15px;'>设计</span>",
                    style: 'background:#157fcc;border:none;margin-right:5px;',
                    iconCls: 'fa fa-object-group gridMenuIcon',
                    handler: function () {
                        var id = store.getAt(rowIndex).get('id');
                        var  html = store.getAt(rowIndex).get('f_originalhtml');
                        oper.design(id, grid, html);
                    }
                });

                //编辑按钮
                var editButton = Ext.create('Ext.button.Button', {
                    height: 19,
                    text: "<span style='color:#ffffff;font-weight: normal;line-height: 15px;'>编辑</span>",
                    style: 'background:#157fcc;border:none;margin-right:5px;',
                    iconCls: 'fa fa-edit gridMenuIcon',
                    handler: function () {
                        var id = store.getAt(rowIndex).get('id');
                        oper.editorById(id, grid);
                    }
                });

                //查看按钮
                var viewButton = Ext.create('Ext.button.Button', {
                    height: 19,
                    text: "<span style='color:#ffffff;font-weight: normal;line-height: 15px;'>查看</span>",
                    style: 'background:#157fcc;border:none;margin-right:5px;',
                    iconCls: 'fa fa-eye gridMenuIcon',
                    handler: function () {
                        var id = store.getAt(rowIndex).get('id');
                        oper.viewById(id, grid);
                    }
                });

                //删除按钮
                var deleteButton = deleteButton = Ext.create('Ext.button.Button', {
                    height: 19,
                    text: "<span style='color:#ffffff;font-weight: normal;line-height: 15px;'>删除</span>",
                    style: 'background:#157fcc;border:none;margin-right:5px;',
                    iconCls: 'fa fa-trash-o gridMenuIcon',
                    handler: function () {
                        Ext.MessageBox.show({
                            title : '询问',
                            msg : '您确定要删除表单吗?',
                            width : 250,
                            buttons : Ext.MessageBox.YESNO,
                            icon : Ext.MessageBox.QUESTION,
                            fn : function(btn) {
                                if (btn == 'yes') {
                                    var id = store.getAt(rowIndex).get('id');
                                    var dir = new Array();
                                    dir.push(id);
                                    oper.delById(dir, grid);
                                }
                            }
                        });
                    }
                });

                //录入数据按钮
                var recordButton = Ext.create('Ext.button.Button', {
                    height: 19,
                    text: "<span style='color:#ffffff;font-weight: normal;line-height: 15px;'>录入数据</span>",
                    style: 'background:#157fcc;border:none;margin-right:5px;',
                    iconCls: 'fa fa-pencil gridMenuIcon',
                    handler: function () {
                        var id = store.getAt(rowIndex).get('id');
                        var taskid = "";
                        oper.use(grid, id, taskid);
                    }
                });

                var id = metadata.record.id;//Ext.id();
                Ext.defer(function () {
                    Ext.widget('buttongroup', {
                        renderTo: id,
                        style: 'margin:0',
                        ui: 'footer',
                        items: [designButton, editButton, viewButton, deleteButton, recordButton]
                    });
                }, 50);
                return Ext.String.format('<div id="{0}"></div>', id);
            }
        }
    ]
});