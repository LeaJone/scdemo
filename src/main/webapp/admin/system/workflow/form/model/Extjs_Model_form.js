﻿/**
 *form Model
 *@author CodeSystem
 *文件名     Extjs_Model_form
 *Model名    model_form
 */

Ext.define('model_form', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'f_name',
            type : 'string'
        }
        ,
        {
            name : 'f_displayname',
            type : 'string'
        }
        ,
        {
            name : 'f_wordtemplate',
            type : 'string'
        }
        ,
        {
            name : 'f_type',
            type : 'string'
        }
        ,
        {
            name : 'f_adddate',
            type : 'string'
        }
        ,
        {
            name : 'f_status',
            type : 'string'
        }
        ,
        {
            name : 'f_originalhtml',
            type : 'string'
        }
        ,
        {
            name : 'f_parsehtml',
            type : 'string'
        }
    ]
});
