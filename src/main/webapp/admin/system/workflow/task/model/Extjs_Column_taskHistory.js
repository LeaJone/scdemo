﻿Ext.define('column_taskhistory', {
    columns: [
          {
              xtype : 'rownumberer',
              text : 'NO',
              sortable : true,
              align : 'center',
              width : 50
          }
          ,
        {
            header : '节点名称',
            dataIndex : 'name',
            sortable : true,
            align : 'center',
            flex : 1
        }
        ,
        {
            header : '开始时间',
            dataIndex : 'startTime',
            sortable : true,
            align : 'center',
            flex : 1
        }
        ,
        {
            header : '状态',
            dataIndex : 'state',
            sortable : true,
            align : 'center',
            flex : 0.5,
            renderer :function(value){
                if(value == "执行中"){
                    return "<font style='color: green;'>"+ value +"</font>";
                }else{
                    return value;
                }
            }
        }
        ,
        {
            header : '审批意见',
            dataIndex : 'message',
            sortable : true,
            align : 'center',
            flex : 0.5,
            renderer :function(value){
                return "<font style='color: red;'>"+ value +"</font>";
            }
        }

    ]
});