﻿Ext.define('model_taskhistory', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'name',
            type : 'string'
        }
        ,
        {
            name : 'startTime',
            type : 'string'
        }
        ,
        {
            name : 'endTime',
            type : 'string'
        }
        ,
        {
            name : 'durationInMillis',
            type : 'string'
        }
        ,
        {
            name : 'state',
            type : 'string'
        }
        ,
        {
            name : 'message',
            type : 'string'
        }
    ]
});
