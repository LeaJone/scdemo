﻿Ext.define('column_task', {
    columns: [
          {
              xtype : 'rownumberer',
              text : 'NO',
              sortable : true,
              align : 'center',
              width : 50
          }
          ,
        {
            header : '任务类型',
            dataIndex : 'column2',
            sortable : true,
            align : 'center',
            flex : 1
        }
        ,
        {
            header : '申请人',
            dataIndex : 'column4',
            sortable : true,
            align : 'center',
            flex : 0.5
        }
        ,
        {
            header : '处理人',
            dataIndex : 'column1',
            sortable : true,
            align : 'center',
            flex : 0.5
        }
        ,
        {
            header : '当前节点',
            dataIndex : 'name',
            sortable : true,
            align : 'center',
            flex : 1
        }
        ,
        {
            header : '任务创建时间',
            dataIndex : 'createTime',
            sortable : true,
            flex : 1,
            align : 'center',
            renderer :function(value){
                return system.UtilStatic.formatDateTime(value);
            }
        }
        ,
        {
            xtype: 'gridcolumn',
            forceFit: true, //列表宽度自适应
            dataIndex: 'operate',
            text: '操作项',
            flex : 3,
            renderer: function (value, metadata, record, rowIndex, columnIndex, store) {
                var id = metadata.record.id;//Ext.id();
                var grid = this;
                metadata.style = "width:800";
                Ext.defer(function () {
                    var buttonGroup = Ext.widget('buttongroup', {
                        renderTo: id,
                        style: 'margin:0',
                        ui: 'footer'
                    });

                    var oper = Ext.create('system.workflow.task.biz.TaskOper');

                    var formMap = store.getAt(rowIndex).get('formMap');
                    if(formMap != null && formMap != ""){
                        var fromButton = Ext.create('Ext.button.Button', {
                            height: 19,
                            text: "<span style='color:#ffffff;font-weight: normal;line-height: 15px;'>"+ formMap.f_displayname +"</span>",
                            style: 'background:#157fcc;border:none;margin-right:5px;',
                            iconCls: 'fa fa-play-circle gridMenuIcon',
                            handler: function () {
                                var taskId = store.getAt(rowIndex).get('id');
                                var employeeid = store.getAt(rowIndex).get('column3');
                                var formOper = Ext.create('system.workflow.form.biz.FormOper');
                                formOper.use(grid, formMap.id, taskId, employeeid);
                            }
                        });
                        buttonGroup.add(fromButton);
                    }

                    var nextNodeList = store.getAt(rowIndex).get('nextNodeList');
                    if(nextNodeList != null && nextNodeList != ""){
                        for(var i = 0;i<nextNodeList.length;i++){
                            buttonGroup.add(Ext.create('Ext.button.Button', {
                                height: 19,
                                text: "<span style='color:#ffffff;font-weight: normal;line-height: 15px;'>"+ nextNodeList[i].name +"</span>",
                                style: 'background:#157fcc;border:none;margin-right:5px;',
                                iconCls: 'fa fa-play-circle gridMenuIcon',
                                handler: function (button) {
                                    var taskId = store.getAt(rowIndex).get('id');
                                    for(var j = 0; j<buttonGroup.items.keys.length; j++){
                                        var id = buttonGroup.items.keys[j];
                                        var idd = button.getId();
                                       if(id == idd){
                                           if(formMap != null && formMap != ""){
                                               oper.taskDispose(grid, taskId, nextNodeList[j-1].state);
                                           }else{
                                               oper.taskDispose(grid, taskId, nextNodeList[j].state);
                                           }
                                       }
                                    }
                                }
                            }));
                        }
                    }else{
                        buttonGroup.add(Ext.create('Ext.button.Button', {
                            height: 19,
                            text: "<span style='color:#ffffff;font-weight: normal;line-height: 15px;'>提交</span>",
                            style: 'background:#157fcc;border:none;margin-right:5px;',
                            iconCls: 'fa fa-play-circle gridMenuIcon',
                            handler: function (button) {
                                var taskId = store.getAt(rowIndex).get('id');
                                if(store.getAt(rowIndex).get('name') == "召集专家"){
                                    var win = Ext.create("system.zgyxy.user.biz.ZjChoose");
                                    win.setTitle("选择专家");
                                    win.modal = true;
                                    win.setCallbackFun(function(ids){
                                        oper.choseTaskDispose(grid, taskId, ids);
                                        win.close();
                                    });
                                    win.show();
                                }else if(store.getAt(rowIndex).get('name') == "专家网评（会签）"){
                                    var projectid = store.getAt(rowIndex).get('column6');
                                    var win = Ext.create("system.zgyxy.project.biz.apply.ProjectScore");
                                    win.setTitle("项目打分");
                                    win.modal = true;
                                    win.setProjectId(projectid);
                                    win.setCallbackFun(function(){
                                        oper.taskDispose(grid, taskId, '');
                                        win.close();
                                    });
                                    win.show();
                                }else{
                                    oper.taskDispose(grid, taskId, '');
                                }
                            }
                        }));
                    }

                    var finishedFormList = store.getAt(rowIndex).get('finishedFormList');
                    if(finishedFormList != null && finishedFormList != "" && finishedFormList.length > 0){
                        buttonGroup.add(Ext.create('Ext.button.Button', {
                            height: 19,
                            text: "<span style='color:#ffffff;font-weight: normal;line-height: 15px;'>查看历史表单数据</span>",
                            style: 'background:#157fcc;border:none;margin-right:5px;',
                            iconCls: 'fa fa-play-circle gridMenuIcon',
                            handler: function (button) {
                                var taskId = store.getAt(rowIndex).get('id');
                                var employeeid = store.getAt(rowIndex).get('column3');
                                var formOper = Ext.create('system.workflow.form.biz.FormOper');
                                formOper.viewFinishForm(grid, taskId, employeeid, finishedFormList);
                            }
                        }));
                    }

                    buttonGroup.add(Ext.create('Ext.button.Button', {
                        height: 19,
                        text: "<span style='color:#ffffff;font-weight: normal;line-height: 15px;'>查看审核历史</span>",
                        style: 'background:#157fcc;border:none;margin-right:5px;',
                        iconCls: 'fa fa-play-circle gridMenuIcon',
                        handler: function (button) {
                            var taskId = store.getAt(rowIndex).get('id');
                            var win = Ext.create('system.workflow.task.biz.TaskHistory');
                            win.setTitle("查看审核历史");
                            win.modal = true;
                            win.taskId = taskId;
                            win.grid.getStore().reload();
                            win.show();
                        }
                    }));

                    buttonGroup.add(Ext.create('Ext.button.Button', {
                        height: 19,
                        text: "<span style='color:#ffffff;font-weight: normal;line-height: 15px;'>查看项目信息</span>",
                        style: 'background:#157fcc;border:none;margin-right:5px;',
                        iconCls: 'fa fa-play-circle gridMenuIcon',
                        handler: function (button) {
                            var businesskey = store.getAt(rowIndex).get('column6');
                            var dir = businesskey.split(".");
                            var type = dir[0];
                            var projectid = dir[1];
                            if("xmtjlc" == type){
                                var win = Ext.create("system.zgyxy.project.biz.apply.ProjectApplyBookView");
                                win.setProjectId(projectid);
                                win.show();
                            }else if("xkjnjssblc" == type){
                                var win = Ext.create("system.zgyxy.xkjnjs.biz.apply.XkjnjsApplyBookView");
                                win.setProjectId(projectid);
                                win.show();
                            }else{
                                Ext.MessageBox.alert("提示", "项目类型未知错误");
                            }
                        }
                    }));
                }, 50);
                return Ext.String.format('<div id="{0}"></div>', id);
            }
        }
    ]
});