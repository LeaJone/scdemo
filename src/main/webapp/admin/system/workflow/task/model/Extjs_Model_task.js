﻿/**
 *task Model
 *@author CodeSystem
 *文件名     Extjs_Model_task
 *Model名    model_task
 */

Ext.define('model_task', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'name',
            type : 'string'
        }
        ,
        {
            name : 'column1',
            type : 'string'
        }
        ,
        {
            name : 'column2',
            type : 'string'
        }
        ,
        {
            name : 'column3',
            type : 'string'
        }
        ,
        {
            name : 'column4',
            type : 'string'
        }
        ,
        {
            name : 'column5',
            type : 'string'
        }
        ,
        {
            name : 'column6',
            type : 'string'
        }
        ,
        {
            name : 'createTime',
            type : 'string'
        }
        ,
        {
            name : 'formMap',
            type : 'data'
        }
        ,
        {
            name : 'nextNodeList',
            type : 'data'
        }
        ,
        {
            name : 'finishedFormList',
            type : 'data'
        }
    ]
});
