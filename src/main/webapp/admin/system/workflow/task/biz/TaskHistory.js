﻿/**
 * 历史审核记录
 * @author lumingbao
 */
Ext.define('system.workflow.task.biz.TaskHistory', {
	extend : 'Ext.window.Window',
	requires : [ 'system.workflow.task.model.Extjs_Column_taskHistory',
		'system.workflow.task.model.Extjs_Model_taskHistory'],
	taskId : '',
	oper : Ext.create('system.workflow.task.biz.TaskOper'),
	initComponent:function(){
	    var me = this;

		// 创建表格
		me.grid = Ext.create("system.widget.FsdGridPanel", {
			column : 'column_taskhistory', // 显示列
			model: 'model_taskhistory',
			baseUrl : 'f_activiti/load_historyListByTaskId.do',
			zAutoLoad : false,
			cellTip : true,
			border: 0,
			tbar : [{
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					me.grid.getStore().reload();
				}
			}]
		});
		me.grid.getStore().on('beforeload', function(s) {
			var pram = {taskId : me.taskId};
			var params = s.getProxy().extraParams;
			Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
	    
	    Ext.apply(this,{
            layout : 'fit',
			width:600,
			height:400,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[me.grid],
	        buttons : [{
			    text : '关闭',
			    iconCls : 'crossIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});