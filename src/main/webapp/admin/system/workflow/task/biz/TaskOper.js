/**
 * 任务管理操作类
 */
Ext.define('system.workflow.task.biz.TaskOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 添加
     */
	add : function(grid){
	    var win = Ext.create('system.workflow.design.biz.DesignEdit', {
	    	grid : grid
	    });
        win.setTitle('创建流程');
        win.modal = true;
        win.show();
	},

    /**
     * 表单提交
     */
    formSubmit : function(formpanel , grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('f_activiti/save_f_activiti_qx_f_activitibj.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(){
                    formpanel.up('window').close();
                    grid.getStore().reload();
                    Ext.MessageBox.alert('提示', '保存成功！');
                });
        }
    },

    deployModel : function(grid, rowIndex){
        var me = this;
        var store = grid.getStore();
        var dir = new Array();
        var id = store.getAt(rowIndex).get('id');
        dir.push(id);
        var pram = {ids : dir};
        var param = {jsonData : Ext.encode(pram)};
        me.util.ExtAjaxRequest('f_activiti/deploy_f_activiti.do' , param ,
            function(){
                grid.getStore().reload();
            }, null, null);
    },

    /**
     * 删除模型
     * @param grid
     */
    deleteModel : function(grid, rowIndex){
        var me = this;
        var store = grid.getStore();
        var dir = new Array();
        var id = store.getAt(rowIndex).get('id');
        dir.push(id);
        var pram = {ids : dir};
        var param = {jsonData : Ext.encode(pram)};
        me.util.ExtAjaxRequest('f_activiti/del_f_activitiModel.do' , param ,
            function(){
                grid.getStore().reload();
            }, null, null);
    },

    /**
     * 删除部署
     * @param grid
     */
    deleteDeployment : function(grid, rowIndex){
        var me = this;
        var store = grid.getStore();
        var dir = new Array();
        var id = store.getAt(rowIndex).get('deploymentId');
        dir.push(id);
        var pram = {ids : dir};
        var param = {jsonData : Ext.encode(pram)};
        me.util.ExtAjaxRequest('f_activiti/del_f_activitiDeployment.do' , param ,
            function(){
                grid.getStore().reload();
            }, null, null);
    },

    /**
     * 处理任务
     * @param projectjd
     * @param taskid
     */
    taskDispose : function(grid, taskid, state){
        var me = this;
        var win = Ext.create('system.workflow.task.biz.TaskDispose');
        win.setTitle("审核意见");
        win.collback = function(value){
            if(value == ""){
                Ext.MessageBox.alert("提示","请填写审核意见!");
            }else{
                Ext.MessageBox.show({
                    title : '询问',
                    msg : '您确定要处理该任务吗?',
                    width : 250,
                    buttons : Ext.MessageBox.YESNO,
                    icon : Ext.MessageBox.QUESTION,
                    fn : function(btn) {
                        if (btn == 'yes') {
                            me.util.ExtAjaxRequest('f_activiti/audit_task.do?taskid='+ taskid +'&state=' + state +"&message=" + value, null ,
                                function(){
                                    grid.getStore().reload();
                                    Ext.MessageBox.alert("提示","处理成功!");
                                    win.close();
                                }, null, null);
                        }
                    }
                });
            }
        };
        win.show();
    },

    /**
     * 选择专家任务处理
     * @param grid
     * @param taskid
     * @param state
     */
    choseTaskDispose : function(grid, taskid, ids){
        var me = this;
        me.util.ExtAjaxRequest('f_activiti/audit_choosetask.do?taskid='+ taskid +'&ids=' + ids , null ,
            function(){
                grid.getStore().reload();
                Ext.MessageBox.alert("提示","处理成功");
            }, null, null);

    }
});