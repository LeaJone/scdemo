﻿/**
 * 任务处理界面
 * @author lumingbao
 */
Ext.define('system.workflow.task.biz.TaskDispose', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.workflow.task.biz.TaskOper'),
	collback : null,
	initComponent:function(){
	    var me = this;

        me.description = new Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '审核意见',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'description',
	    	maxLength: 250,
            allowBlank: false
	    });

	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 20 10 20',
			items : [me.description]
        });
	    
	    Ext.apply(this,{
            layout : 'fit',
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '提交',
			    iconCls : 'acceptIcon',
			    handler : function() {
					if(me.collback != null){
						me.collback(me.description.getValue());
					}
			    }
		    },{
			    text : '关闭',
			    iconCls : 'crossIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	},

    /**
	 * 设置项目编码
     * @param id
     */
	setProjectId : function (id) {
		var me = this;
		me.projectid.setValue(id);
    },

    /**
	 * 设置任务ID
     * @param id
     */
	setTaskId : function (id) {
        var me = this;
        me.taskid.setValue(id);
    }
});