﻿/**
 *f_activiti Column
 *@author CodeSystem
 *文件名     Extjs_Column_F_activiti
 */

Ext.define('column_f_activiti', {
    oper : Ext.create('system.workflow.design.biz.DesignOper'),
    columns: [
          {
              xtype : 'rownumberer',
              text : 'NO',
              sortable : true,
              width : 40
          }
          ,
        {
            header : '流程名称',
            dataIndex : 'name',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '流程KEY',
            dataIndex : 'key',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '发布状态',
            dataIndex : 'deploymentId',
            sortable : true,
            flex : 2,
            renderer :function(value ,metaData ,record ){
                if(value != null && value != ""){
                    value = "<font style='color:green;'>已发布</font>";
                }else{
                    value = "<font>未发布</font>";
                }
                return value;
            }
        }
         ,{
            xtype: 'gridcolumn',
            width: 500,
            dataIndex: 'operate',
            text: '操作项',
            align: 'center',
            renderer: function (value, metadata, record, rowIndex, columnIndex, store) {
                var oper = Ext.create('system.workflow.design.biz.DesignOper');

                var deployState = store.getAt(rowIndex).get('deploymentId');
                var grid = this;

                var deployButton = null;
                var deleteButton = null;
                if(deployState != null && deployState != ""){

                    deployButton = Ext.create('Ext.button.Button', {
                        height: 19,
                        text: "<span style='color:#ffffff;font-weight: normal;line-height: 15px;'>取消部署</span>",
                        style: 'background:#157fcc;border:none;margin-right:5px;',
                        iconCls: 'fa fa-play-circle gridMenuIcon',
                        handler: function () {
                            Ext.MessageBox.show({
                                title : '询问',
                                msg : '您确定要发布流程吗?',
                                width : 250,
                                buttons : Ext.MessageBox.YESNO,
                                icon : Ext.MessageBox.QUESTION,
                                fn : function(btn) {
                                    if (btn == 'yes') {
                                        oper.deleteDeployment(grid, rowIndex);
                                    }
                                }
                            });
                        }
                    });
                }else{

                    deployButton = Ext.create('Ext.button.Button', {
                        height: 19,
                        text: "<span style='color:#ffffff;font-weight: normal;line-height: 15px;'>部署流程</span>",
                        style: 'background:#157fcc;border:none;margin-right:5px;',
                        iconCls: 'fa fa-play-circle gridMenuIcon',
                        handler: function () {
                            Ext.MessageBox.show({
                                title : '询问',
                                msg : '您确定要发布流程吗?',
                                width : 250,
                                buttons : Ext.MessageBox.YESNO,
                                icon : Ext.MessageBox.QUESTION,
                                fn : function(btn) {
                                    if (btn == 'yes') {
                                        oper.deployModel(grid, rowIndex);
                                    }
                                }
                            });
                        }
                    });

                    deleteButton = Ext.create('Ext.button.Button', {
                        height: 19,
                        text: "<span style='color:#ffffff;font-weight: normal;line-height: 15px;'>删除流程</span>",
                        style: 'background:#157fcc;border:none;margin-right:5px;',
                        iconCls: 'fa fa-trash-o gridMenuIcon',
                        handler: function () {
                            Ext.MessageBox.show({
                                title : '询问',
                                msg : '您确定要删除流程吗?',
                                width : 250,
                                buttons : Ext.MessageBox.YESNO,
                                icon : Ext.MessageBox.QUESTION,
                                fn : function(btn) {
                                    if (btn == 'yes') {
                                        oper.deleteModel(grid, rowIndex);
                                    }
                                }
                            });
                        }
                    });
                }

                var id = metadata.record.id;//Ext.id();
                Ext.defer(function () {
                    Ext.widget('buttongroup', {
                        renderTo: id,
                        style: 'margin:0',
                        ui: 'footer',
                        items: [
                            {
                                xtype: 'button',
                                height: 19,
                                text: "<span style='color:#ffffff;font-weight: normal;line-height: 15px;'>流程设计</span>",
                                style: 'background:#157fcc;border:none;margin-right:5px;',
                                iconCls: 'fa fa-edit gridMenuIcon',
                                handler: function () {
                                    var util = Ext.create('system.util.util');
                                    var modelId = store.getAt(rowIndex).get('id');
                                    var model = Ext.create("system.workflow.design.biz.DesignPanel");
                                    model.setHtml("<iframe src='../process-editor/modeler.html?modelId="+ modelId +"' frameborder='0' width='100%' height='100%'></iframe>");
                                    util.addTab("fsdworkflowdesign" , "流程设计器" , model , 'fa-edit');
                                }
                            },{
                                xtype: 'button',
                                height: 19,
                                text: "<span style='color:#ffffff;font-weight: normal;line-height: 15px;'>上传流程</span>",
                                style: 'background:#157fcc;border:none;margin-right:5px;',
                                iconCls: 'fa fa-cloud-upload gridMenuIcon',
                                handler: function () {
                                    var win = Ext.create('system.workflow.design.biz.DesignUpload');
                                    win.setTitle('上传流程');
                                    win.modal = true;
                                    win.show();
                                }
                            },deployButton, deleteButton
                        ]
                    });
                }, 50);
                return Ext.String.format('<div id="{0}"></div>', id);
            }
        }
    ]
});