﻿/**
 *f_activiti Model
 *@author CodeSystem
 *文件名     Extjs_Model_F_activiti
 *Model名    model_f_activiti
 */

Ext.define('model_f_activiti', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'name',
            type : 'string'
        }
        ,
        {
            name : 'key',
            type : 'string'
        }
        ,
        {
            name : 'deploymentId',
            type : 'string'
        }
        ,
        {
            name : 'version',
            type : 'string'
        }
    ]
});
