﻿/**

 * 流程设计界面
 * @author lumingbao
 */

Ext.define('system.workflow.design.biz.DesignPanel', {
	extend : 'system.widget.FsdFormPanel',
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.callParent();
	},

	setHtml :function(html){
		var me = this;
		me.html = html;
	}
});