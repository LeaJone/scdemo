/**
 * 流程设计操作类
 */
Ext.define('system.workflow.design.biz.DesignOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 添加
     */
	add : function(grid){
	    var win = Ext.create('system.workflow.design.biz.DesignEdit', {
	    	grid : grid
	    });
        win.setTitle('创建流程');
        win.modal = true;
        win.show();
	},

    /**
     * 表单提交
     */
    formSubmit : function(formpanel , grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('f_activiti/save_f_activiti_qx_f_activitibj.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action){
                    formpanel.up('window').close();
                    grid.getStore().reload();
                    Ext.MessageBox.alert('提示', '保存成功！');
                });
        }
    },

    deployModel : function(grid, rowIndex){
        var me = this;
        var store = grid.getStore();
        var dir = new Array();
        var id = store.getAt(rowIndex).get('id');
        dir.push(id);
        var pram = {ids : dir};
        var param = {jsonData : Ext.encode(pram)};
        me.util.ExtAjaxRequest('f_activiti/deploy_f_activiti.do' , param ,
            function(response, options){
                grid.getStore().reload();
            }, null, null);
    },

    /**
     * 删除模型
     * @param grid
     */
    deleteModel : function(grid, rowIndex){
        var me = this;
        var store = grid.getStore();
        var dir = new Array();
        var id = store.getAt(rowIndex).get('id');
        dir.push(id);
        var pram = {ids : dir};
        var param = {jsonData : Ext.encode(pram)};
        me.util.ExtAjaxRequest('f_activiti/del_f_activitiModel.do' , param ,
            function(response, options){
                grid.getStore().reload();
            }, null, null);
    },

    /**
     * 删除部署
     * @param grid
     */
    deleteDeployment : function(grid, rowIndex){
        var me = this;
        var store = grid.getStore();
        var dir = new Array();
        var id = store.getAt(rowIndex).get('deploymentId');
        dir.push(id);
        var pram = {ids : dir};
        var param = {jsonData : Ext.encode(pram)};
        me.util.ExtAjaxRequest('f_activiti/del_f_activitiDeployment.do' , param ,
            function(response, options){
                grid.getStore().reload();
            }, null, null);
    }
});