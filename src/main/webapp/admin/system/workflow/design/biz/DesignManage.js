﻿/**

 * 流程设计管理界面
 * @author lumingbao
 */

Ext.define('system.workflow.design.biz.DesignManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.workflow.design.model.Extjs_Column_F_activiti',
	             'system.workflow.design.model.Extjs_Model_F_activiti'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.workflow.design.biz.DesignOper'),
	
	createContent : function(){
		var me = this;
		
		var queryid = 'FSDMAIN';

		var queryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入流程名称',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});
		
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPanel", {
			column : 'column_f_activiti', // 显示列
			model: 'model_f_activiti',
			baseUrl : 'f_activiti/load_designpagedata.do',
			border: 0,
			tbar : [{
				xtype : "button",
				text : "创建流程",
				iconCls : 'page_addIcon',
				popedomCode : 'lmbj',
				handler : function(button) {
					me.oper.add(grid);
				}
			}, '->', queryText , {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  queryText.getValue();
			var pram = {fid : queryid , name : name};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});

		return grid;
	}
});