﻿/**
 * 流程设计上传界面
 * @author lumingbao
 */
Ext.define('system.workflow.design.biz.DesignUpload', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.workflow.design.biz.DesignOper'),
	initComponent:function(){
	    var me = this;

        var zipfile = Ext.create('system.widget.FsdTextFileManage', {
            width : 360,
            zName : 'zipfile',
            zFieldLabel : 'ZIP文件',
            zLabelWidth : 80,
            zIsShowButton : false,
            zIsUpButton : true,
            zFileType : 'file',
            zFileAutoName : false,
            zFileUpPath : me.oper.util.getTempPath(),
            zManageType : 'upload',
            zAllowBlank: false
        });

	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 20 10 20',
			items : [{
                name: "id",
                xtype: "hidden"
            }, zipfile]
        });
	    
	    Ext.apply(this,{
            layout : 'fit',
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
			    handler : function() {
    				me.oper.formSubmit(form, me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});