﻿/**
 * 流程设计添加界面
 * @author lumingbao
 */
Ext.define('system.workflow.design.biz.DesignEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.workflow.design.biz.DesignOper'),
	initComponent:function(){
	    var me = this;

	    var name = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '流程名称',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'f_name',
	    	maxLength: 25,
	    	allowBlank: false
	    });

        var key = new Ext.create('system.widget.FsdTextField',{
            fieldLabel: '流程KEY',
            labelAlign:'right',
            labelWidth:80,
            width : 360,
            name: 'f_key',
            maxLength: 25,
            allowBlank: false
        });

	    var description = new Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '流程描述',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'f_description',
	    	maxLength: 250,
            allowBlank: false
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 20 10 20',
			items : [{
                name: "id",
                xtype: "hidden"
            }, name, key, description]
        });
	    
	    Ext.apply(this,{
            layout : 'fit',
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
			    handler : function() {
    				me.oper.formSubmit(form, me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});