﻿/**
 * 数据监控管理界面
 * @author lumingbao
 */
Ext.define('system.abasis.datamonitor.biz.DruidMonitorManage', {
	extend : 'system.widget.FsdFormPanel',
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},

	createTable : function() {
	    var me = this;

        var panel2 = new Ext.Panel( {
            border : false,
            border : 0,
            fitToFrame: true,
            html: '<iframe id="frame1" src="../druid" frameborder="0" width="100%" height="100%"></iframe>'
        });

		return panel2;
	}
});