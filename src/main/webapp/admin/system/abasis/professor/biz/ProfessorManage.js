﻿/**

 * 机构管理界面
 * @author lumingbao
 */

Ext.define('system.abasis.professor.biz.ProfessorManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*', 'system.abasis.professor.model.Extjs_Z_professor'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.abasis.professor.biz.ProfessorOper'),
	
	createContent : function(){
		var me = this;
		
		var queryid = me.oper.util.getParams("company").id;
		
		/**
		 * 左边的树面板
		 */
		var treepanel = Ext.create("system.widget.FsdTreePanel", {
			icon : 'resources/admin/icon/chart_organisation.png',
			baseUrl : 'z_professor/load_AsyncProfessorTreeQuery.do',
			title: '职称',
			rootText : '主分类',
			rootId : me.oper.util.getParams("company").id,
			region : 'west', // 设置方位
			collapsible : true,//折叠
			width: 182,
			split: true
		});
		
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_professor', // 显示列
			model: 'model_z_professor',
			baseUrl : 'z_professor/load_data.do',
			tbar : [{
				xtype : "fsdbutton",
				text : "添加",
				iconCls : 'page_addIcon',
				popedomCode : 'jgbj',
				handler : function(button) {
					me.oper.add(grid , treepanel);
				}
			}, {
				xtype : "fsdbutton",
				text : "修改",
				iconCls : 'page_editIcon',
				popedomCode : 'jgbj',
				handler : function(button) {
					me.oper.editor(grid , treepanel);
				}
			}, {
				xtype : "fsdbutton",
				text : "删除",
				iconCls : 'page_deleteIcon',
				popedomCode : 'jgsc',
				handler : function(button) {
				    me.oper.del(grid , treepanel);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid);
				}
			}, '-', {
				xtype : "fsdbutton",
				popedomCode : 'jgbj',
				text : "启用",
				iconCls : 'tickIcon',
				handler : function(button) {
				    me.oper.isEnabled(grid, true);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'jgbj',
				text : "停用",
				iconCls : 'crossIcon',
				handler : function(button) {
				    me.oper.isEnabled(grid, false);
				}
			}, '->', new Ext.form.TextField( {
				id : 'queryProfessorName',
				name : 'queryParam',
				emptyText : '请输入职称名称',
				enableKeyEvents : true,
				listeners : {
					specialkey : function(field, e) {
						if (e.getKey() == Ext.EventObject.ENTER) {
							grid.getStore().reload();
						}
					}
				},
				width : 130
			}), {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  Ext.getCmp('queryProfessorName').getValue();
			var pram = {fid : queryid , name : name};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		var contentpanel = Ext.create("Ext.panel.Panel", {
			frame:true,
			split: true,
			region: 'center',
			layout: 'fit',
			title: '职称',
			items: [grid]
		});

		var page1_jExtPanel1_obj = new Ext.panel.Panel({
			bodyPadding: 3,
			header : false,
			border : 0,
			split: true,
			layout: 'border',
			items: [treepanel,contentpanel]
		});
		
		treepanel.on('itemclick', function(view,record,item,index,e){
			queryid = record.raw.id;
			grid.getStore().reload();
		});
		return page1_jExtPanel1_obj;
	}
});