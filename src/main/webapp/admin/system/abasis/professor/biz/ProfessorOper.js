/**
 * 机构管理界面操作类
 * @author lumingbao
 */

Ext.define('system.abasis.professor.biz.ProfessorOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 添加
     */
	add : function(grid , treepanel){
	    var win = Ext.create('system.abasis.professor.biz.ProfessorEdit');
        win.setTitle('职称添加');
        win.grid = grid;
        win.treepanel =  treepanel;
        win.modal = true;
        win.show();
	},
	
	/**
     * 表单提交
     */
     formSubmit : function(formpanel , grid , treepanel){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('z_professor/save_Professor_qx_jgbj.do' , formpanel.form , '正在提交数据,请稍候.....',
			function(form, action){
			    formpanel.up('window').close();
			    grid.getStore().reload();
			    treepanel.getStore().load();
				Ext.MessageBox.alert('提示', '保存成功！');
			});
        }
     },
     
     /**
      * 删除
      */
     del : function(grid , treepanel){
    	 var me = this;
 		 // 获取选中的行
 	     var data = grid.getSelectionModel().getSelection();
         if (data.length == 0) {
             Ext.MessageBox.alert('提示', '请先选中要删除的项目！');
         }else {
        	 Ext.MessageBox.show({
        		 title : '询问',
        		 msg : '您确定要删除吗?',
 			     width : 250,
 				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                     if (btn == 'yes') {
                    	 var dir = new Array();　
                         Ext.Array.each(data, function(items) {
                             var id = items.data.id;
                             dir.push(id);
             			});
             			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('z_professor/del_Professor_qx_jgsc.do' , param ,
                        function(response, options){
                        	grid.getStore().reload(); 
            			    treepanel.getStore().load();
                        }, null, me.form);
                     }
                 }
             });
         }

 	},
 	
 	/**
     * 修改
     */
	editor : function(grid , treepanel){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{

            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            
            var win = Ext.create('system.abasis.professor.biz.ProfessorEdit');
            win.setTitle('职称修改');
            win.modal = true;
            win.grid = grid;
            win.treepanel =  treepanel;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'z_professor/load_ProfessorByid.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }

	},
 	
 	/**
     * 查看
     */
	view : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if(data.length > 0){
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.abasis.professor.biz.ProfessorEdit');
            win.setTitle('职称查看');
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'z_professor/load_ProfessorByid.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},
	
	/**
     * 是否启用停用
     */
	isEnabled : function(grid, isQT){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
		        msg : '您确定要修改项目状态吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
					    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
			            });
             			var pram = {ids : dir, isenabled : isQT};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('z_professor/enabled_Professor_qx_jgbj.do', param,
                        function(response, options){
            				Ext.MessageBox.alert('提示', '项目状态修改成功！');
                        	grid.getStore().reload(); 
                        });
                    }
                }
            });
        }
	}
});