﻿/**
 *a_branch Model
 *@author CodeSystem
 *文件名     Extjs_A_branch
 *Model名    model_a_branch
 *Columns名  column_a_branch
 */

Ext.define('model_z_professor', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'code',
            type : 'string'
        }
        ,
        {
            name : 'name',
            type : 'string'
        }
        ,
        {
            name : 'parentname',
            type : 'string'
        }
        ,
        {
            name : 'statusname',
            type : 'string'
        }
        ,
        {
            name : 'sort',
            type : 'int'
        }
        ,
        {
            name : 'remark',
            type : 'string'
        }
    ]
});


/**
 *a_branch Columns
 *@author CodeSystem
 */
Ext.define('column_z_professor', {
    columns: [
        {
            header : '编号',
            dataIndex : 'code',
            align : 'center',
            flex : 1
        }
        ,
        {
            header : '名称',
            dataIndex : 'name',
            flex : 3
        }
        ,
        {
            header : '所属分类',
            dataIndex : 'parentname',
            flex : 3
        }
        ,
        {
            header : '状态',
            dataIndex : 'statusname',
            sortable : true,
            align : 'center',
            flex : 1
        }
        ,
        {
            header : '顺序号',
            dataIndex : 'sort',
            sortable : true,
            align : 'center',
            flex : 1
        }
        ,
        {
            header : '备注',
            dataIndex : 'remark',
            flex : 2
        }
    ]
});
