﻿/**
 *a_area Model
 *@author CodeSystem
 *文件名     Extjs_Model_A_area
 *Model名    model_a_area
 */

Ext.define('model_a_area', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'parentid',
            type : 'string'
        }
        ,
        {
            name : 'parentname',
            type : 'string'
        }
        ,
        {
            name : 'code',
            type : 'string'
        }
        ,
        {
            name : 'name',
            type : 'string'
        }
        ,
        {
            name : 'admincode',
            type : 'string'
        }
        ,
        {
            name : 'areacode',
            type : 'string'
        }
        ,
        {
            name : 'postcode',
            type : 'string'
        }
        ,
        {
            name : 'remark',
            type : 'string'
        }
    ]
});
