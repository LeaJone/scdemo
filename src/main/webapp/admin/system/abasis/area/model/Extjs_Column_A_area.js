﻿/**
 *a_area Column
 *@author CodeSystem
 *文件名     Extjs_Column_A_area
 *Columns名  column_a_area
 */

Ext.define('column_a_area', {
    columns: [
        {
            header : '名称',
            dataIndex : 'name',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '所属地域',
            dataIndex : 'parentname',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '行政区代码',
            dataIndex : 'admincode',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '区号代码',
            dataIndex : 'areacode',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '邮政编码',
            dataIndex : 'postcode',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '备注',
            dataIndex : 'remark',
            sortable : true,
            flex : 1
        }
    ]
});
