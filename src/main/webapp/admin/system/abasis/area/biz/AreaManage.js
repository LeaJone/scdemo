﻿/**

 * 地域管理界面
 * @author lw
 */

Ext.define('system.abasis.area.biz.AreaManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 
	             'Ext.grid.*', 
	             'Ext.toolbar.Paging', 
	             'Ext.data.*', 
	             'system.abasis.area.model.Extjs_Column_A_area', 
	             'system.abasis.area.model.Extjs_Model_A_area'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.abasis.area.biz.AreaOper'),
	
	createContent : function(){
		var me = this;
		
		var queryid = 'FSDAREA';

		/**
		 * 查询文本框
		 */
		var queryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入地域名称',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().reload();
					}
				}
			}
		});
		
		/**
		 * 左边的树面板
		 */
		var treepanel = Ext.create("system.widget.FsdTreePanel", {
			icon : 'resources/admin/icon/chart_organisation.png',
			baseUrl : 'a_area/load_AsyncAreaTree.do',
			title: '地域类别',
			rootText : '地域类别',
			rootId : 'FSDAREA',
			region : 'west', // 设置方位
			collapsible : true,//折叠
			width: 182,
			split: true
		});
		
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_a_area', // 显示列
			model: 'model_a_area',
			baseUrl : 'a_area/load_data.do',
			tbar : [{
				xtype : "fsdbutton",
				popedomCode : 'dylbbj',
				text : "添加",
				iconCls : 'page_addIcon',
				handler : function(button) {
					me.oper.add(grid , treepanel);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'dylbbj',
				text : "修改",
				iconCls : 'page_editIcon',
				handler : function(button) {
					me.oper.editor(grid , treepanel);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'dylbsc',
				text : "删除",
				iconCls : 'page_deleteIcon',
				handler : function(button) {
				    me.oper.del(grid , treepanel);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid);
				}
			}, '->', queryText, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  queryText.getValue();
			var pram = {fid : queryid , name : name};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		var contentpanel = Ext.create("Ext.panel.Panel", {
			frame:true,
			split: true,
			region: 'center',
			layout: 'fit',
			title: '类别信息',
			items: [grid]
		});

		var page1_jExtPanel1_obj = new Ext.panel.Panel({
			bodyPadding: 3,
			header : false,
			border : 0,
			split: true,
			layout: 'border',
			items: [treepanel,contentpanel]
		});
		
		treepanel.on('itemclick', function(view,record,item,index,e){
			queryid = record.raw.id;
			grid.getStore().reload();
		});
		return page1_jExtPanel1_obj;
	}
});