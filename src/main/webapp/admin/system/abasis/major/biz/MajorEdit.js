﻿/**
 * 栏目添加界面
 * @author lumingbao
 */
Ext.define('system.abasis.major.biz.MajorEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	treepanel : null,
	oper : Ext.create('system.abasis.major.biz.MajorOper'),
	initComponent:function(){
	    var me = this;
	    
	    var gjid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '根级ID',
	    	name: 'baseid',
	    	value: 'FSDMAIN'
	    });
	    
	    var sslmname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属专业名称',
	    	name: 'parentname'
	    });
	    var sslm = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属专业',
	    	labelAlign:'right',
	    	labelWidth:80,
	    	rootText : '主栏目',
	    	rootId : 'FSDMAIN',
	        width : 360,
	        name : 'parentid',
	        baseUrl:'z_major/load_AsyncMajorTreeQuery.do',//访问路劲
	        allowBlank: false,
	        hiddenName : 'parentname'//隐藏域Name
	    });
	    
	    var lmmc = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '专业名称',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'title',
	    	maxLength: 25,
	    	allowBlank: false
	    });
	    
	    var pxbh = new Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '排序编号',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'sort',
	    	maxLength: 4,
	    	minValue: 0,
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank: false
	    });
	    
	    var describe = new Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '专业描述',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'describe',
	    	maxLength: 250
	    });
	    
	    me.on('show' , function(){
	    	sslm.setText(sslmname.getValue());
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 20 10 20',
			items : [{
                name: "id",
                xtype: "hidden"
            } , gjid, sslmname, sslm, lmmc, , pxbh, describe]//lmtp2col ,
        });
	    
	    Ext.apply(this,{
            layout : 'fit',
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
			    handler : function() {
    				me.oper.submitMain(form, me.grid , me.treepanel);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});