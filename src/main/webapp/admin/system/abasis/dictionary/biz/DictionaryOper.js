/**
 * 字典管理操作类
 * @author lumingbao
 */

Ext.define('system.abasis.dictionary.biz.DictionaryOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 添加
     */
    addType : function(grid){
	    var win = Ext.create('system.abasis.dictionary.biz.DictionaryTypeEdit');
        win.setTitle('添加字典类型');
        win.grid = grid;
        win.modal = true;
        win.show();
	},

    /**
     * 修改
     */
    editorType : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{

            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框

            var win = Ext.create('system.abasis.dictionary.biz.DictionaryTypeEdit');
            win.setTitle('修改字典类型');
            win.modal = true;
            win.grid = grid;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(win.down('form').getForm() , 'Sys_SystemDictionary/load_sys_systemdictionaryById.do' , param , null ,
                function(response, options, respText){
                    win.show();
                });
        }
    },

    /**
     * 删除
     */
    delType : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要删除的项目！');
        }else {
            Ext.MessageBox.show({
                title : '询问',
                msg : '您确定要删除吗?',
                width : 250,
                buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                        var dir = new Array();
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
                        });
                        var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('Sys_SystemDictionary/del_sys_systemdictionaryType.do', param,
                            function(response, options){
                                grid.getStore().reload();
                            }, null, me.form);
                    }
                }
            });
        }
    },

    /**
     * 查看
     */
    viewType : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if(data.length > 0){
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.abasis.dictionary.biz.DictionaryTypeEdit');
            win.setTitle('查看字典类型');
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(win.down('form').getForm() , 'Sys_SystemDictionary/load_sys_systemdictionaryById.do' , param , null ,
                function(response, options, respText){
                    win.show();
                });
        }
    },

	/**
     * 表单提交
     */
     formSubmitType : function(formpanel, grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('Sys_SystemDictionary/save_sys_systemdictionaryType.do', formpanel.form, '正在提交数据,请稍候.....',
			function(form, action){
			    formpanel.up('window').close();
			    grid.getStore().reload();
				Ext.MessageBox.alert('提示', '保存成功！');
			});
        }
     },

    /**
     * 添加
     */
    add : function(grid, code, name){
        var win = Ext.create('system.abasis.dictionary.biz.DictionaryEdit');
        win.setTitle(name + '-添加字典');
        win.grid = grid;
        win.modal = true;
        win.setValue(code);
        win.show();
    },

    /**
     * 修改
     */
    editor : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{

            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框

            var win = Ext.create('system.abasis.dictionary.biz.DictionaryEdit');
            win.setTitle('修改字典');
            win.modal = true;
            win.grid = grid;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(win.down('form').getForm() , 'Sys_SystemDictionary/load_sys_systemdictionaryById.do' , param , null ,
                function(response, options, respText){
                    win.show();
                });
        }
    },

    /**
     * 删除
     */
    del : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要删除的项目！');
        }else {
            Ext.MessageBox.show({
                title : '询问',
                msg : '您确定要删除吗?',
                width : 250,
                buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                        var dir = new Array();
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
                        });
                        var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('Sys_SystemDictionary/del_sys_systemdictionary.do', param,
                            function(response, options){
                                grid.getStore().reload();
                            }, null, me.form);
                    }
                }
            });
        }
    },

    /**
     * 查看
     */
    view : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if(data.length > 0){
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.abasis.dictionary.biz.DictionaryEdit');
            win.setTitle('查看字典类型');
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(win.down('form').getForm() , 'Sys_SystemDictionary/load_sys_systemdictionaryById.do' , param , null ,
                function(response, options, respText){
                    win.show();
                });
        }
    },

    /**
     * 表单提交
     */
    formSubmit : function(formpanel, grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('Sys_SystemDictionary/save_sys_systemdictionary.do', formpanel.form, '正在提交数据,请稍候.....',
                function(form, action){
                    formpanel.up('window').close();
                    grid.getStore().reload();
                    Ext.MessageBox.alert('提示', '保存成功！');
                });
        }
    }
});