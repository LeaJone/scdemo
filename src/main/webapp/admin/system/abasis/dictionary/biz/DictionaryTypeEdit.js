﻿/**
 * 字典类型编辑界面
 * @author lumingbao
 */
Ext.define('system.abasis.dictionary.biz.DictionaryTypeEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.abasis.dictionary.biz.DictionaryOper'),
	initComponent:function(){
	    var me = this;

        var code = new Ext.create('system.widget.FsdTextField',{
            fieldLabel: '编码',
            labelAlign:'right',
            labelWidth:70,
            width : 340,
            name: 'code',
            maxLength: 20,
            allowBlank: false
        });

	    var name = new Ext.create('system.widget.FsdTextFieldZJF',{
	    	fieldLabel: '名称',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'name',
            fname : 'symbol',
	    	maxLength: 20,
	    	allowBlank: false
	    });

        var symbol = new Ext.create('system.widget.FsdTextField',{
            fieldLabel: '助记符',
            labelAlign:'right',
            labelWidth:70,
            width : 340,
            name: 'symbol',
            maxLength: 20,
            allowBlank: false,
            blankText: '请输入助记符',
            maxLengthText: '助记符不能超过20个字符'
        });

	    var remark = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'remark',
	    	maxLength: 50
	    });

	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [{
                name: "id",
                xtype: "hidden"
            }, code, name, symbol, remark]
        });
	        
	    Ext.apply(this,{
	        width:400,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'cslbbj',
	        	name : 'btnsave',
			    text : '　保存　',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.formSubmitType(form, me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});