﻿/**

 * 字典管理界面
 * @author lumingbao
 */

Ext.define('system.abasis.dictionary.biz.DictionaryManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*', 
	             'system.widget.FsdButton',
	             'system.abasis.dictionary.model.Extjs_Column_Dictionary',
				 'system.abasis.dictionary.model.Extjs_Column_DictionaryType',
	             'system.abasis.dictionary.model.Extjs_Model_Dictionary'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},

	code : "",
	nowname: "",

	oper : Ext.create('system.abasis.dictionary.biz.DictionaryOper'),
	
	createContent : function(){
		var me = this;
		
		var queryid = me.oper.util.getParams("company").id;

		/**
		 * 查询文本框
		 */
		var queryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入参数名称',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});

        // 创建表格
        var typeGrid = Ext.create("system.widget.FsdGridPagePanel", {
            column : 'column_dictionarytype', // 显示列
            model: 'model_dictionary',
            baseUrl : 'Sys_SystemDictionary/get_dictionaryTypePageList.do',
            tbar : [{
                xtype : "fsdbutton",
                text : "添加",
                iconCls : 'page_addIcon',
                popedomCode : 'cslbbj',
                handler : function(button) {
                    me.oper.addType(typeGrid);
                }
            }, {
                xtype : "fsdbutton",
                text : "修改",
                iconCls : 'page_editIcon',
                popedomCode : 'cslbbj',
                handler : function(button) {
                    me.oper.editorType(typeGrid);
                }
            }, {
                xtype : "fsdbutton",
                text : "删除",
                iconCls : 'page_deleteIcon',
                popedomCode : 'cslbsc',
                handler : function(button) {
                    me.oper.delType(typeGrid);
                }
            }, {
                text : '查看',
                iconCls : 'magnifierIcon',
                handler : function() {
                    me.oper.viewType(typeGrid);
                }
            }, '->', queryText, {
                text : '查询',
                iconCls : 'magnifierIcon',
                handler : function() {
                    typeGrid.getStore().loadPage(1);
                }
            }, {
                text : '刷新',
                iconCls : 'tbar_synchronizeIcon',
                handler : function() {
                    typeGrid.getStore().reload();
                }
            }]
        });

        typeGrid.getStore().on('beforeload', function(s) {
            var name =  queryText.getValue();
            var pram = { name : name};
            var params = s.getProxy().extraParams;
            Ext.apply(params,{jsonData : Ext.encode(pram)});
        });

        typeGrid.selModel.on('selectionchange', function(obj, selected, eOpts){
            if (selected.length > 0){
                me.code = selected[0].data.code;
                me.nowname = selected[0].data.name;
                grid.getStore().loadPage(1);
            }
        });

		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_dictionary', // 显示列
			model: 'model_dictionary',
			baseUrl : 'Sys_SystemDictionary/get_dictionaryPageList.do',
			tbar : [{
				xtype : "fsdbutton",
				text : "添加",
				iconCls : 'page_addIcon',
				popedomCode : 'cslbbj',
				handler : function(button) {
					if(me.code == "" || me.nowname == ""){
                        Ext.MessageBox.alert('提示', '请先从左侧选择字典类型！');
					}else{
                        me.oper.add(grid, me.code, me.nowname);
					}
				}
			}, {
				xtype : "fsdbutton",
				text : "修改",
				iconCls : 'page_editIcon',
				popedomCode : 'cslbbj',
				handler : function(button) {
					me.oper.editor(grid);
				}
			}, {
				xtype : "fsdbutton",
				text : "删除",
				iconCls : 'page_deleteIcon',
				popedomCode : 'cslbsc',
				handler : function(button) {
				    me.oper.del(grid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid);
				}
			}, '->', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  queryText.getValue();
			var pram = {code : me.code};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});

        var contentpane = Ext.create("Ext.panel.Panel", {
            frame:true,
            split: true,
            region: 'west',
            layout: 'fit',
            collapsible : true,//折叠
            width: '50%',
            title: '字典类型',
            items: [typeGrid]
        });
		
		var contentpanel = Ext.create("Ext.panel.Panel", {
			frame:true,
			split: true,
			region: 'center',
			layout: 'fit',
            width: '50%',
			title: '字典列表',
			items: [grid]
		});

		var page1_jExtPanel1_obj = new Ext.panel.Panel({
			bodyPadding: 3,
			header : false,
			border : 0,
			split: true,
			layout: 'border',
			items: [contentpane, contentpanel]
		});
		
		return page1_jExtPanel1_obj;
	}
});