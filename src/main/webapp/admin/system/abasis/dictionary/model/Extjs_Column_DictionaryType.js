﻿/**
 *dictionary Column
 *@author CodeSystem
 *文件名     Extjs_Column_Dictionary
 *Columns名  column_dictionary
 */

Ext.define('column_dictionarytype', {
    columns: [
              {
                  xtype : 'rownumberer',
                  text : 'NO',
                  sortable : true,
                  width : 40
              }
              ,
        {
            header : '编码',
            dataIndex : 'code',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '类型名',
            dataIndex : 'name',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '类型值',
            dataIndex : 'symbol',
            sortable : true,
            flex : 1
        }
    ]
});
