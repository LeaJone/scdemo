﻿/**
 *dictionary Column
 *@author CodeSystem
 *文件名     Extjs_Column_Dictionary
 *Columns名  column_dictionary
 */

Ext.define('column_dictionary', {
    columns: [
              {
                  xtype : 'rownumberer',
                  text : 'NO',
                  sortable : true,
                  width : 40
              }
              ,
        {
            header : '编码',
            dataIndex : 'code',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '字典名',
            dataIndex : 'name',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '字典值',
            dataIndex : 'symbol',
            sortable : true,
            flex : 1
        }
    ]
});
