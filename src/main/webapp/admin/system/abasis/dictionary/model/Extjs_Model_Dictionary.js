﻿/**
 *b_parameterlist Model
 *@author CodeSystem
 *文件名     Extjs_Model_B_parameterlist
 *Model名    model_b_parameterlist
 */

Ext.define('model_dictionary', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'code',
            type : 'string'
        }
        ,
        {
            name : 'name',
            type : 'string'
        }
        ,
        {
            name : 'symbol',
            type : 'string'
        }
        ,
        {
            name : 'remark',
            type : 'string'
        }
    ]
});
