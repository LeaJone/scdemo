﻿/**
 * 通知添加界面
 * @author lw
 */
Ext.define('system.abasis.notify.biz.NotifyShow', {
	extend : 'system.widget.FsdFormPanel',
	requires : ['system.widget.FsdTreeComboBox'],
	oper : Ext.create('system.abasis.notify.biz.NotifyOper'),
	initComponent:function(){
	    var me = this;
	    
	    var title = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '标题',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 780,
	    	name: 'title',
	    	maxLength: 200,
	    	allowBlank: false,
	    	blankText: '请输入标题',
	    	maxLengthText: '标题不能超过200个字符'
	    });
	   
	    var abstracts = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '摘要',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 780,
	    	name: 'abstracts',	    	
	    	maxLength: 250,
	    	allowBlank: true,
	    	blankText: '请输入摘要',
	    	maxLengthText: '摘要不能超过250个字符'
	    });
	   
//	    var content = Ext.create('system.widget.FsdUeditor',{
//	    	fieldLabel: '内容',
//	    	labelAlign:'right',
//	    	labelWidth:60,
//	        width : 820,
//	        height : 1000,
//	    	name: 'content',
//	    	toolbars : [[]]
//	    });
	    
	    me.content = Ext.create('Ext.panel.Panel',{
			autoScroll : true,
			header : false,// 是否显示标题栏
	        width : 700,
	        height : 1000,
			border : 0,
			padding : '20 0 0 20'
        });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			autoScroll : true,
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [
            title, abstracts, me.content
            ]
        });
	        
	    Ext.apply(this,{
	        items :[form]
	    });
	    this.callParent(arguments);
	}
});