﻿/**
 * 通知管理界面
 * @author lw
 */
Ext.define('system.abasis.notify.biz.NotifyManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*', 
	             'system.widget.FsdButton',
	             'system.abasis.notify.model.Extjs_Column_C_notify', 
	             'system.abasis.notify.model.Extjs_Model_C_notify'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.abasis.notify.biz.NotifyOper'),
    
	createTable : function() {
	    var me = this;

		/**
		 * 查询文本框
		 */
		var queryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入通知标题',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().reload();
					}
				}
			}
		});

		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_c_notify', // 显示列
			model : 'model_c_notify',
			baseUrl : 'a_notify/load_pagedata.do',
			border : false,
			tbar : [{
				xtype : "fsdbutton",
				text : "添加",
				iconCls : 'page_addIcon',
				popedomCode : 'tzbj',
				handler : function() {
					me.oper.add(grid);
				}
			}, {
				xtype : "fsdbutton",
				text : "修改",
				iconCls : 'page_editIcon',
				popedomCode : 'tzbj',
				handler : function() {
					me.oper.editor(grid);
				}
			}, {
				xtype : "fsdbutton",
				text : "删除",
				iconCls : 'page_deleteIcon',
				popedomCode : 'tzsc',
				handler : function() {
				    me.oper.del(grid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid);
				}
			}, '-', {
				xtype : "fsdbutton",
				text : "选择置顶",
				iconCls : 'page_deleteIcon',
				popedomCode : 'tzbj',
				handler : function(button) {
				    me.oper.isFirstly(grid, true);
				}
			}, {
				xtype : "fsdbutton",
				text : "取消置顶",
				iconCls : 'page_deleteIcon',
				popedomCode : 'tzbj',
				handler : function(button) {
				    me.oper.isFirstly(grid, false);
				}
			}, '-', {
				xtype : "fsdbutton",
				text : "启用",
				iconCls : 'page_deleteIcon',
				popedomCode : 'tzbj',
				handler : function(button) {
				    me.oper.isEnabled(grid, true);
				}
			}, {
				xtype : "fsdbutton",
				text : "停用",
				iconCls : 'page_deleteIcon',
				popedomCode : 'tzbj',
				handler : function(button) {
				    me.oper.isEnabled(grid, false);
				}
			}, '->', queryText, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var title =  queryText.getValue();
			var pram = {title : title};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});

		return grid;
	}
});