﻿/**
 * 通知管理界面
 * @author lw
 */
Ext.define('system.abasis.notify.biz.NotifyPanel', {
	extend : 'Ext.panel.Panel',
	requires : [ 'system.abasis.notify.model.Extjs_Column_C_notify', 
	             'system.abasis.notify.model.Extjs_Model_C_notify' ],
	layout : 'fit',
	title: '通知',
	
	initComponent : function() {
		var me = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.abasis.notify.biz.NotifyOper'),
    
	createTable : function() {
	    var me = this;

		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPanel", {
			column : 'column_c_notify_qy', // 显示列
			model : 'model_c_notify',
			baseUrl : 'a_notify/load_dataqy.do',
			border : false,
			selType : 'rowmodel',
			tbar : [{
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]
		});
		grid.on('cellclick', function(thisObj, td, cellIndex, record, tr, rowIndex, e, eOpts) {
			if (cellIndex == 4){
				me.oper.viewid(record.get('id'));
			}
		});

		return grid;
	}
});