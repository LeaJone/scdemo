﻿/**
 * 通知添加界面
 * @author lw
 */
Ext.define('system.abasis.notify.biz.NotifyEdit', {
	extend : 'Ext.window.Window',
	requires : ['system.widget.FsdTreeComboBox'],
	grid : null,
	oper : Ext.create('system.abasis.notify.biz.NotifyOper'),
	initComponent:function(){
	    var me = this;
	    
	    var title = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '标题',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 780,
	    	name: 'title',
	    	maxLength: 200,
	    	allowBlank: false,
	    	blankText: '请输入标题',
	    	maxLengthText: '标题不能超过200个字符'
	    });
	   
	    var abstracts = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '摘要',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 780,
	    	name: 'abstracts',	    	
	    	maxLength: 250,
	    	allowBlank: true,
	    	blankText: '请输入摘要',
	    	maxLengthText: '摘要不能超过250个字符'
	    });
	   
	    var content = Ext.create('system.widget.FsdUeditor',{
	    	fieldLabel: '内容',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 800,
	        height : 440,
	    	name: 'content'
	    });

	    var type = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'类型',
	        labelAlign:'right',
	    	labelWidth:60,
	        width : 390,
    	    name : 'type',//提交到后台的参数名
	        key : 'tzlx',//参数
    	    hiddenName : 'typename'//提交隐藏域Name
	    });
	    var typename = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '类型名',
	    	name: 'typename'
	    });
	   
	    var overdue = Ext.create('system.widget.FsdDateField',{
	    	fieldLabel: '过期时间',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 390,
	    	name: 'overdue',
	    	allowBlank: true,
	    	blankText: '请输入过期时间'
	    });

	    var status = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'状态',
	        labelAlign:'right',
	    	labelWidth:60,
	        width : 390,
    	    name : 'status',//提交到后台的参数名
	        key : 'tzzt',//参数
    	    hiddenName : 'statusname'//提交隐藏域Name
	    });
	    var statusname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '状态名',
	    	name: 'statusname'
	    });
	    
	    var remark = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 390,
	    	name: 'remark',
	    	maxLength: 50,
	    	maxLengthText: '备注不能超过50个字符'
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [{
                name: "id",
                xtype: "hidden"
            }, typename, statusname, 
            title, abstracts, content, 
            {
                layout : 'column',
                border : false,
				padding : '25 0 0 0',
    	        width : 780,
                items:[{
                    columnWidth : 0.5,
                    border:false,
                    items:[type, overdue]
                }, {
                    columnWidth : 0.5,
                    border:false,
                    items:[status, remark]
                }]
			}]
        });
	        
	    Ext.apply(this,{
	        width:880,
	        height:650,
			autoScroll : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'tzbj',
	        	name : 'btnsave',
			    text : '　保存　',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.formSubmit(form, me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});