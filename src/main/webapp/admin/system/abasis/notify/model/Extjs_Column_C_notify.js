﻿/**
 *c_notify Column
 *@author CodeSystem
 *文件名     Extjs_Column_C_notify
 *Columns名  column_c_notify
 *           column_c_notify_qy
 */

Ext.define('column_c_notify', {
	
	requires : [ 'system.util.UtilStatic' ],
	
    columns: [
        {
            header : '标题',
            dataIndex : 'title',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '摘要',
            dataIndex : 'abstracts',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '类型',
            dataIndex : 'typename',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '置顶',
            dataIndex : 'firstly',
            sortable : true,
            flex : 1,
			renderer :function(value ,metaData ,record ){
				if(value == 'true'){
					//metaData.style='color:blue;text-decoration:underline;cursor:pointer;';下划线
					value="<font style='color:red;'>已置顶</font>";
				}else{
					value="<font style='color:blue;'>未置顶</font>";
				}
				return value;
			}
        }
        ,
        {
            header : '过期时间',
            dataIndex : 'overdue',
            sortable : true,
            flex : 1,
			renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value, false);
			}
        }
        ,
        {
            header : '备注',
            dataIndex : 'remark',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '状态',
            dataIndex : 'statusname',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '新建时间',
            dataIndex : 'adddate',
            sortable : true,
            flex : 2,
			renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value);
			}
        }
        ,
        {
            header : '新建人',
            dataIndex : 'addemployeename',
            sortable : true,
            flex : 1
        }
    ]
});


/**
 *启用列表
 *Columns名  column_c_notify_qy
 */
Ext.define('column_c_notify_qy', {
	
	requires : [ 'system.util.UtilStatic' ],
	
    columns: [
        {
            header : '标题',
            dataIndex : 'title',
            sortable : true,
            flex : 3
        }
        ,
        {
            header : '摘要',
            dataIndex : 'abstracts',
            sortable : true,
            flex : 6
        }
        ,
        {
            header : '类型',
            dataIndex : 'typename',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '新建时间',
            dataIndex : 'adddate',
            sortable : true,
            flex : 2,
			renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value);
			}
        }
        ,
        {
            header : '查看',
            dataIndex : 'id',
            flex : 1,
			renderer :function(value ,metaData ,record ){
				metaData.style='color:blue;text-decoration:underline;cursor:pointer;';//下划线
				value="<font style='color:blue;'>查看</font>";
				return value;
			}
        }
    ]
});
