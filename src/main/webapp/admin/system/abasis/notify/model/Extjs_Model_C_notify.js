﻿/**
 *c_notify Model
 *@author CodeSystem
 *文件名     Extjs_Model_C_notify
 *Model名    model_c_notify
 */

Ext.define('model_c_notify', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'companyid',
            type : 'string'
        }
        ,
        {
            name : 'title',
            type : 'string'
        }
        ,
        {
            name : 'abstracts',
            type : 'string'
        }
        ,
        {
            name : 'type',
            type : 'string'
        }
        ,
        {
            name : 'typename',
            type : 'string'
        }
        ,
        {
            name : 'firstly',
            type : 'string'
        }
        ,
        {
            name : 'overdue',
            type : 'string'
        }
        ,
        {
            name : 'remark',
            type : 'string'
        }
        ,
        {
            name : 'status',
            type : 'string'
        }
        ,
        {
            name : 'statusname',
            type : 'string'
        }
        ,
        {
            name : 'adddate',
            type : 'string'
        }
        ,
        {
            name : 'addemployeeid',
            type : 'string'
        }
        ,
        {
            name : 'addemployeename',
            type : 'string'
        }
    ]
});
