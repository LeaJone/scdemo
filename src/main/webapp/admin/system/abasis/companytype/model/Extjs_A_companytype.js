﻿/**
 *a_companytype Model
 *@author CodeSystem
 *文件名     Extjs_A_companytype
 *Model名    model_a_companytype
 *Columns名  column_a_companytype
 */

Ext.define('model_a_companytype', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'name',
            type : 'string'
        }
        ,
        {
            name : 'parentid',
            type : 'string'
        }
        ,
        {
            name : 'parentname',
            type : 'string'
        }
        ,
        {
            name : 'symbol',
            type : 'string'
        }
        ,
        {
            name : 'remark',
            type : 'string'
        }
        ,
        {
            name : 'sort',
            type : 'int'
        }
    ]
});


/**
 *a_companytype Columns
 *@author CodeSystem
 */
Ext.define('column_a_companytype', {
    columns: [
        {
            header : '名称',
            dataIndex : 'name'
        }
        ,
        {
            header : '助记符',
            dataIndex : 'symbol'
        }
        ,
        {
            header : '所属类型',
            dataIndex : 'parentname'
        }
        ,
        {
            header : '顺序号',
            dataIndex : 'sort'
        }
        ,
        {
            header : '备注',
            dataIndex : 'remark'
        }
    ]
});
