﻿/**
 * 默认类型添加界面
 * @author lw
 */
Ext.define('system.abasis.companytype.biz.DefaultEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	treepanel : null,
	oper : Ext.create('system.abasis.companytype.biz.CompanyTypeOper'),
	initComponent:function(){
	    var me = this;

	    var gjid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '根级ID',
	    	name: 'baseid',
	    	value: 'FSDDEFAULT'
	    });
	    
	    var lbbm = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '类别编码',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'code',
	    	maxLength: 20,
	    	allowBlank: false,
	    	blankText: '请输入类别编码',
	    	maxLengthText: '类别编码不能超过20个字符'
	    });
	    
	    var ssfj = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属类别',
	    	labelAlign:'right',
	    	labelWidth:60,
	    	rootText : '默认类别',
	    	rootId : 'FSDDEFAULT',
	        width : 340,
	        name : 'parentid',
	        baseUrl:'A_CompanyType/load_AsyncCompanyTypeTree.do',//访问路劲	        
	        allowBlank: false,
	        blankText: '请选择所属类别',
	        hiddenName : 'parentname'//隐藏域Name
	    });
	    var ssfjname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属类别名称',
	    	name: 'parentname'
	    });
	    
	    var lbmc = new Ext.create('system.widget.FsdTextFieldZJF',{
	    	fieldLabel: '类别名称',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'name',
	    	fname : 'symbol',
	    	maxLength: 20,
	    	allowBlank: false,
	    	blankText: '请输入类别名称',
	    	maxLengthText: '类别名称不能超过20个字符'
	    });
	    
	    var zjf = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '助记符',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'symbol',
	    	maxLength: 20,
	    	allowBlank: false,
	    	blankText: '请输入助记符',
	    	maxLengthText: '助记符不能超过20个字符'
	    });
	    
	    var pxbh = new Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '排序编号',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'sort',
	    	maxLength: 3,
	    	minValue: 0,
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank: false,
	    	blankText: '请输入排序编号',
	    	maxLengthText: '排序编号不能超过3个字符'
	    });
	    
	    var beizhu = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'remark',
	    	maxLength: 50,
	    	maxLengthText: '备注不能超过50个字符'
	    });
	        
	    Ext.apply(this,{
	        width:400,
	        height:250,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[{
				xtype : 'form',
				id : 'DefaultTypeAddForm',
				defaultType : 'textfield',
				header : false,// 是否显示标题栏
				border : 0,
				padding : '20 0 0 20',
				items : [{
                    name: "id",
                    xtype: "hidden"
                }, gjid, ssfjname, ssfj, lbmc, zjf, pxbh , beizhu]
	        }],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'mrlbbj',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.submitDefault(Ext.getCmp('DefaultTypeAddForm') , me.grid , me.treepanel);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});