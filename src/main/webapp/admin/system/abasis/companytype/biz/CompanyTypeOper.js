/**
 * 类型界面操作类
 * @author liuwei
 */

Ext.define('system.abasis.companytype.biz.CompanyTypeOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 添加
     */
	addDefault : function(grid , treepanel){
	    var win = Ext.create('system.abasis.companytype.biz.DefaultEdit');
        win.setTitle('默认类别添加');
        win.grid = grid;
        win.treepanel =  treepanel;
        win.modal = true;
        win.show();
	},
	/**
     * 添加
     */
	addArea : function(grid , treepanel){
	    var win = Ext.create('system.abasis.companytype.biz.AreaEdit');
        win.setTitle('地域类别添加');
        win.grid = grid;
        win.treepanel =  treepanel;
        win.modal = true;
        win.show();
	},
	/**
     * 添加
     */
	addProfession : function(grid , treepanel){
	    var win = Ext.create('system.abasis.companytype.biz.ProfessionEdit');
        win.setTitle('行业类别添加');
        win.grid = grid;
        win.treepanel =  treepanel;
        win.modal = true;
        win.show();
	},
	/**
     * 添加
     */
	addUser : function(grid , treepanel){
	    var win = Ext.create('system.abasis.companytype.biz.UserEdit');
        win.setTitle('人员类别添加');
        win.grid = grid;
        win.treepanel =  treepanel;
        win.modal = true;
        win.show();
	},
	/**
     * 添加
     */
	addJob : function(grid , treepanel){
	    var win = Ext.create('system.abasis.companytype.biz.JobEdit');
        win.setTitle('职务类别添加');
        win.grid = grid;
        win.treepanel =  treepanel;
        win.modal = true;
        win.show();
	},

	submitDefault : function(formpanel , grid , treepanel){
		this.formSubmit(formpanel , grid , treepanel, 'A_CompanyType/save_CompanyType_qx_mrlbbj.do');
	},
	submitArea : function(formpanel , grid , treepanel){
		this.formSubmit(formpanel , grid, treepanel, 'A_CompanyType/save_CompanyType_qx_dylbbj.do');
	},
	submitProfession : function(formpanel , grid , treepanel){
		this.formSubmit(formpanel , grid, treepanel, 'A_CompanyType/save_CompanyType_qx_hylbbj.do');
	},
	submitUser : function(formpanel , grid , treepanel){
		this.formSubmit(formpanel , grid, treepanel, 'A_CompanyType/save_CompanyType_qx_rylbbj.do');
	},
	submitJob : function(formpanel , grid , treepanel){
		this.formSubmit(formpanel , grid, treepanel, 'A_CompanyType/save_CompanyType_qx_zwlbbj.do');
	},
	/**
     * 表单提交
     */
     formSubmit : function(formpanel , grid , treepanel, url){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit(url , formpanel.form , '正在提交数据,请稍候.....',
			function(form, action){
			    formpanel.up('window').close();
			    grid.getStore().reload();
			    treepanel.getStore().load();
				Ext.MessageBox.alert('提示', '保存成功！');
			},
			function (form, action){
				Ext.MessageBox.alert('异常', action.result.msg);
			});
        }
     },

    delDefault : function(grid , treepanel){
 		this.del(grid , treepanel, 'A_CompanyType/del_CompanyType_qx_mrlbsc.do');
 	},
 	delArea : function(grid , treepanel){
 		this.del(grid, treepanel, 'A_CompanyType/del_CompanyType_qx_dylbsc.do');
 	},
 	delProfession : function(grid , treepanel){
 		this.del(grid, treepanel, 'A_CompanyType/del_CompanyType_qx_hylbsc.do');
 	},
 	delUser : function(grid , treepanel){
 		this.del(grid, treepanel, 'A_CompanyType/del_CompanyType_qx_rylbsc.do');
 	},
 	delJob : function(grid , treepanel){
 		this.del(grid, treepanel, 'A_CompanyType/del_CompanyType_qx_zwlbsc.do');
 	},
     /**
      * 删除
      */
     del : function(grid , treepanel, url){
    	 var me = this;
 		 // 获取选中的行
 	     var data = grid.getSelectionModel().getSelection();
         if (data.length == 0) {
             Ext.MessageBox.alert('提示', '请先选中要删除的项目！');
         }else {
        	 Ext.MessageBox.show({
        		 title : '询问',
        		 msg : '您确定要删除吗?',
 			     width : 250,
 				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                     if (btn == 'yes') {
                    	 var dir = new Array();　
                         Ext.Array.each(data, function(items) {
                             var id = items.data.id;
                             dir.push(id);
             			});
             			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest(url , param ,
                        function(response, options){
                        	grid.getStore().reload(); 
            			    treepanel.getStore().load();
                        }, null, me.form);
                     }
                 }
             });
         }

 	},

	editorDefault : function(grid , treepanel){
		this.editor(grid, treepanel, 'system.abasis.companytype.biz.DefaultEdit', '默认类别修改');
	},
	editorArea : function(grid , treepanel){
		this.editor(grid, treepanel, 'system.abasis.companytype.biz.AreaEdit', '地域类别修改');
	},
	editorProfession : function(grid , treepanel){
		this.editor(grid, treepanel, 'system.abasis.companytype.biz.ProfessionEdit', '行业类别修改');
	},
	editorUser : function(grid , treepanel){
		this.editor(grid, treepanel, 'system.abasis.companytype.biz.UserEdit', '人员类别修改');
	},
	editorJob : function(grid , treepanel){
		this.editor(grid, treepanel, 'system.abasis.companytype.biz.JobEdit', '职务类别修改');
	},
 	/**
     * 修改
     */
	editor : function(grid , treepanel, url, title){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{

            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            
            var win = Ext.create(url);
            win.setTitle(title);
            win.modal = true;
            win.grid = grid;
            win.treepanel =  treepanel;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'A_CompanyType/load_CompanyTypeByid.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},

	viewDefault : function(grid){
		this.view(grid, 'system.abasis.companytype.biz.DefaultEdit', '默认类别查看');
	},
	viewArea : function(grid){
		this.view(grid, 'system.abasis.companytype.biz.AreaEdit', '地域类别查看');
	},
	viewProfession : function(grid){
		this.view(grid, 'system.abasis.companytype.biz.ProfessionEdit', '行业类别查看');
	},
	viewUser : function(grid){
		this.view(grid, 'system.abasis.companytype.biz.UserEdit', '人员类别查看');
	},
	viewJob : function(grid){
		this.view(grid, 'system.abasis.companytype.biz.JobEdit', '职务类别查看');
	},
 	/**
     * 查看
     */
	view : function(grid, url, title){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if(data.length > 0){
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create(url);
            win.setTitle(title);
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'A_CompanyType/load_CompanyTypeByid.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	}
});