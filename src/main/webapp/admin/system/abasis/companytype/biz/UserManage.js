﻿/**

 * 用户类型管理界面
 * @author lumingbao
 */

Ext.define('system.abasis.companytype.biz.UserManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*', 'system.abasis.companytype.model.Extjs_A_companytype'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.abasis.companytype.biz.CompanyTypeOper'),
	
	createContent : function(){
		var me = this;
		
		var queryid = 'FSDUSER';
		
		/**
		 * 左边的树面板
		 */
		var treepanel = Ext.create("system.widget.FsdTreePanel", {
			icon : 'resources/admin/icon/chart_organisation.png',
			baseUrl : 'A_CompanyType/load_AsyncCompanyTypeTree.do',
			title: '人员类别',
			rootText : '人员类别',
			rootId : 'FSDUSER',
			region : 'west', // 设置方位
			collapsible : true,//折叠
			width: 182,
			split: true
		});
		
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_a_companytype', // 显示列
			model: 'model_a_companytype',
			baseUrl : 'A_CompanyType/load_data.do',
			tbar : [{
				xtype : "fsdbutton",
				popedomCode : 'rylbbj',
				text : "添加",
				iconCls : 'page_addIcon',
				handler : function(button) {
					me.oper.addUser(grid , treepanel);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'rylbbj',
				text : "修改",
				iconCls : 'page_editIcon',
				handler : function(button) {
					me.oper.editorUser(grid , treepanel);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'rylbsc',
				text : "删除",
				iconCls : 'page_deleteIcon',
				handler : function(button) {
				    me.oper.delUser(grid , treepanel);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.viewUser(grid);
				}
			}, '->', new Ext.form.TextField( {
				id : 'queryUserTypeName',
				name : 'queryParam',
				emptyText : '请输入类别名称',
				enableKeyEvents : true,
				listeners : {
					specialkey : function(field, e) {
						if (e.getKey() == Ext.EventObject.ENTER) {
							grid.getStore().reload();
						}
					}
				},
				width : 130
			}), {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  Ext.getCmp('queryUserTypeName').getValue();
			var pram = {fid : queryid , name : name};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		var contentpanel = Ext.create("Ext.panel.Panel", {
			frame:true,
			split: true,
			region: 'center',
			layout: 'fit',
			title: '类别信息',
			items: [grid]
		});

		var page1_jExtPanel1_obj = new Ext.panel.Panel({
			bodyPadding: 3,
			header : false,
			border : 0,
			split: true,
			layout: 'border',
			items: [treepanel,contentpanel]
		});
		
		treepanel.on('itemclick', function(view,record,item,index,e){
			queryid = record.raw.id;
			grid.getStore().reload();
		});
		return page1_jExtPanel1_obj;
	}
});