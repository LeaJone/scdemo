/**
 * SEO优化配置操作类
 * 
 * @author LiHanlin
 */
Ext.define('system.abasis.seo.biz.SeoOper', {

	form : null,
	util : Ext.create('system.util.util'),

	/**
	 * 保存
	 */
	submitSEO : function(formpanel, grid) {
		var me = this;
		if (formpanel.down('form').isValid()) {
			me.util.ExtFormSubmit('sys_seo/save_seo_qx_seobj.do', formpanel.down('form'), '正在提交数据,请稍候.....', function(form, action) {
				Ext.MessageBox.alert('提示', '保存成功！');
			},function(form, action) {
				Ext.MessageBox.alert('提示', '保存失败！');
			});
		}
	},

	/**
	 * 重置
	 */
	resetSEO : function(id) {
		var me = this;
		var pram = {
			id : id
		};
		var param = {
			jsonData : Ext.encode(pram)
		};
		me.util.ExtAjaxRequest('sys_seo/del_seo_qx_seosc.do', param, function(response, options) {
			Ext.MessageBox.alert('提示', '重置成功！');
		}, null, me.form);
	}

});