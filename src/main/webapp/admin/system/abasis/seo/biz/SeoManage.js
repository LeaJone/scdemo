﻿/**
 * SEO配置界面
 * 
 * @author LiHanlin
 */
Ext.define('system.abasis.seo.biz.SeoManage', {
	extend : 'system.widget.FsdFormPanel',
	oper : Ext.create('system.abasis.seo.biz.SeoOper'),
	initComponent : function() {
		var me = this;

		var id = 'FSDSITESEO';

		var remark = Ext.create('system.widget.FsdTextArea', {
			fieldLabel : '配置须知',
			labelAlign : 'right',
			labelWidth : 60,
			width : 780,
			name : 'remark',
			readOnly : true,
			padding : '10 10 10 10'
		});

		var title = new Ext.create('system.widget.FsdTextField', {
			fieldLabel : '标题',
			labelAlign : 'right',
			labelWidth : 60,
			width : 780,
			name : 'title',
			maxLength : 100,
			allowBlank : false,
			padding : '10 10 10 10'
		});

		var keywords = Ext.create('system.widget.FsdTextArea', {
			fieldLabel : '关键词',
			labelAlign : 'right',
			labelWidth : 60,
			width : 780,
			name : 'keywords',
			maxLength : 200,
			allowBlank : false,
			padding : '10 10 10 10'
		});

		var description = Ext.create('system.widget.FsdTextArea', {
			fieldLabel : '网站描述',
			labelAlign : 'right',
			labelWidth : 60,
			width : 780,
			height : 150,
			name : 'description',
			maxLength : 500,
			allowBlank : false,
			padding : '10 10 10 10'
		});

		var fieldPanel = Ext.create('Ext.form.FieldSet', {
			title : '全站配置',
			width : 820,
			padding : '10 0 15 10',
			items : [ {
				layout : 'column',
				border : false,
				items : [ 
					{
					name : "id",
					xtype : "hidden"
				}, remark, title, keywords, description ]
			} ]
		});

		var form = Ext.create('Ext.form.Panel', {
			xtype : 'form',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [ fieldPanel ]
		});

		me.on('boxready', function(obj, width, height, eOpts) {
			var pram = {
				id : id
			};
			var param = {
				jsonData : Ext.encode(pram)
			};
			me.oper.util.formLoad(me.down('form').getForm(), 'sys_seo/load_seobyid.do', param, null, function(response, options, respText) {});
		});

		Ext.apply(this, {
			title : 'SEO全站优化配置',
			tbar : [ {
				xtype : "fsdbutton",
				popedomCode : 'seobj',
				text : "保存",
				name : 'btnsave',
				iconCls : 'acceptIcon',
				handler : function(button) {
					me.oper.submitSEO(me, id);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'seosc',
				text : "重置",
				name : 'btnreset',
				iconCls : 'tbar_synchronizeIcon',
				handler : function(button) {
					me.oper.resetSEO(id);
					var obj = {
						title : null,
						keywords : null,
						description : null
					}
					me.down('form').getForm().setValues(obj);
				}
			} ],
			items : [ form ]
		});
		this.callParent(arguments);
	}
});