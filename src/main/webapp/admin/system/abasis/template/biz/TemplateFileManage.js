﻿/**
 * 模板管理界面
 * @author lmb
 */
Ext.define('system.abasis.template.biz.TemplateManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*', 
	             'system.widget.FsdButton',
	             'system.abasis.template.model.Extjs_Column_Sys_templatefile',
	             'system.abasis.template.model.Extjs_Model_Sys_templatefile'],
	header : false,
	border : 0,
	layout : 'fit',
	url : '',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
		
		//打开文件夹
		window.openFile = function(type, url){
			if(type == "1"){
				me.oper.editFile(url);
			}else if(type == "0"){
				me.url = url;
				me.grid.getStore().reload();
			}
		};
		
		//重命名
		window.renameFile = function(type, url, name){
			me.oper.rename(type, url, name, me.grid);
		};
		
		//删除
		window.deleteFile = function(type, url){
			me.oper.del(type, url, me.grid);
		};
	},
	
    oper : Ext.create('system.abasis.template.biz.TemplateOper'),
	queryObj : null,
    
	createTable : function() {
	    var me = this;
	    
		// 创建表格
		me.grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_sys_template', // 显示列
			model : 'model_sys_template',
			baseUrl : 'Sys_Template/load_pagedata.do',
			border : false,
			tbar : [{
				text : '新建模板',
				iconCls : 'page_addIcon',
				handler : function() {
					me.oper.view(me.grid);
				}
			}, {
				text : '返回上一级',
				iconCls : 'arrow_redoIcon',
				handler : function() {
					if(me.url != null && me.url != ""){
						var dir= new Array();
						dir = me.url.split("/");
						var newPaht = "";
						for (i=0;i<dir.length ;i++ ){   
							if(i < dir.length-1){
								if(i == 0){
									newPaht = newPaht + dir[i];
								}else{
									newPaht = newPaht + "/" + dir[i];
								}
								
							}
						}
					}
					me.url = newPaht;
					me.grid.getStore().reload();
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					me.grid.getStore().reload();
				}
			}]
		});
		me.grid.getStore().on('beforeload', function(s) {
			var pram = {url : me.url};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});

		return me.grid;
	}
});