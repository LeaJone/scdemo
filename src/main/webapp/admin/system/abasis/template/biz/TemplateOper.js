/**
 * 模板操作类
 * @author lmb
 */
Ext.define('system.abasis.template.biz.TemplateOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 编辑文件
     */
	editFile : function(url){
	    var me = this;
	    
	    var model = me.util.getTab("templateedit");
		if (model == null){
			model = Ext.create("system.abasis.template.biz.TemplateEdit");
		}
		var pram = {url : url};
        var param = {jsonData : Ext.encode(pram)};
        me.util.formLoad(model.form, 'Sys_Template/load_filecontent.do', param, null, 
        function(response, options, respText, data){
        	me.util.addTab("templateedit" , "模板编辑" , model , "resources/admin/icon/vcard_add.png");
        });
	},
	
	/**
	 * 重命名
	 */
	rename : function(tyle, url, name, grid){
	    var win = Ext.create('system.abasis.template.biz.TemplateRename');
        win.setTitle('重命名');
        win.grid = grid;
        win.modal = true;
        win.url.setValue(url);
        win.mc.setValue(name);
        win.show();
	},
	
	/**
	 * 重命名提交
	 */
	renameSubmit : function(formpanel, grid){
		var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('Sys_Template/save_rename.do' , formpanel.form , '正在提交数据,请稍候.....',
			function(form, action){
			    formpanel.up('window').close();
			    grid.getStore().reload();
				Ext.MessageBox.alert('提示', '保存成功！');
			});
        }
	},

    /**
     * 重命名提交
     */
    /**
     * 提交文件内容
     */
    addformSubmit : function(formpanel, grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('a_template/save_a_template_qx_a_templatebj.do' , formpanel.form , '正在提交数据,请稍候.....',
                function(form, action){
                    formpanel.up('window').close();
                    grid.getStore().reload();
                    Ext.MessageBox.alert('提示', '保存成功！');
                });
        }
    },
	
	/**
	 * 提交文件内容
	 */
	formSubmit : function(formpanel){
		var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('Sys_Template/save_filecontent.do' , formpanel.form , '正在提交数据,请稍候.....',
			function(form, action){
				Ext.MessageBox.alert('提示', '保存成功！');
			});
        }
	},
	
	/**
     * 删除
    */
    del : function(tyle, url, grid){
    	var me = this;
       	 Ext.MessageBox.show({
       		 title : '询问',
       		 msg : '您确定要删除吗?',
       		 width : 250,
       		 buttons : Ext.MessageBox.YESNO,
             icon : Ext.MessageBox.QUESTION,
             fn : function(btn) {
            	 if (btn == 'yes') {
            		 var pram = {url : url};
            		 var param = {jsonData : Ext.encode(pram)};
            		 me.util.ExtAjaxRequest('Sys_Template/del_files.do' , param ,
            		 function(response, options){
            			 Ext.MessageBox.alert('提示', '删除成功！');
                       	 grid.getStore().reload();
                     }, null, me.form);
            	 }
             }
       	 });
	},

    /**
     * 重命名
     */
    addTemplate : function(grid){
        var win = Ext.create('system.abasis.template.biz.TemplateAdd');
        win.setTitle('新建模板');
        win.grid = grid;
        win.modal = true;
        win.show();
    },

    /**
     * 修改
     */
    editor : function(grid ){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{

            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框

            var win = Ext.create('system.abasis.template.biz.TemplateAdd');
            win.setTitle('模板修改');
            win.modal = true;
            win.grid = grid;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(win.down('form').getForm() , 'a_template/load_a_templatebyid.do' , param , null ,
                function(response, options, respText){
                    win.show();
                });
        }
    },
});