﻿/**
 * 模板管理界面
 * @author lmb
 */
Ext.define('system.abasis.template.biz.TemplateManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*', 
	             'system.widget.FsdButton',
	             'system.abasis.template.model.Extjs_Column_Sys_template', 
	             'system.abasis.template.model.Extjs_Model_Sys_template'],
	header : false,
	border : 0,
	layout : 'fit',
	url : '',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
		
		//打开文件夹
		window.openFile = function(type, url){
			if(type == "1"){
				me.oper.editFile(url);
			}else if(type == "0"){
				me.url = url;
				me.grid.getStore().reload();
			}
		};
		
		//重命名
		window.renameFile = function(type, url, name){
			me.oper.rename(type, url, name, me.grid);
		};
		
		//删除
		window.deleteFile = function(type, url){
			me.oper.del(type, url, me.grid);
		};
	},
	
    oper : Ext.create('system.abasis.template.biz.TemplateOper'),
	queryObj : null,
    
	createTable : function() {
	    var me = this;
	    
		// 创建表格
		me.grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_sys_template', // 显示列
			model : 'model_sys_template',
			baseUrl : 'a_template/load_pagedata.do',
			border : false,
			tbar : [{
				text : '新建模板',
				iconCls : 'page_addIcon',
				handler : function() {
					me.oper.addTemplate(me.grid);
				}
			}, {
                xtype : "fsdbutton",
                text : "修改",
                iconCls : 'page_editIcon',
                popedomCode : 'yqljbj',
                handler : function(button) {
                    me.oper.editor(me.grid);
                }
            }, {
                xtype : "fsdbutton",
                text : "删除",
                iconCls : 'page_deleteIcon',
                popedomCode : 'yqljsc',
                handler : function(button) {
                    me.oper.del(me.grid);
                }
            }, {
                text : '查看',
                iconCls : 'magnifierIcon',
                handler : function() {
                    me.oper.view(me.grid);
                }
            }, '-', {
                xtype : "fsdbutton",
                text : "审核通过",
                iconCls : 'tickIcon',
                popedomCode : 'yqljbj',
                handler : function(button) {
                    me.oper.auditing(me.grid , 'true');
                }
            }, {
                xtype : "fsdbutton",
                text : "取消审核",
                iconCls : 'crossIcon',
                popedomCode : 'yqljbj',
                handler : function(button) {
                    me.oper.auditing(me.grid , 'false');
                }
            }, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					me.grid.getStore().reload();
				}
			}]
		});
		me.grid.getStore().on('beforeload', function(s) {
			var pram = {url : me.url};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});

		return me.grid;
	}
});