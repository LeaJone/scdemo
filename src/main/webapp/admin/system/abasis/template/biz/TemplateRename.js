﻿/**
 * 模板重命名
 * @author lmb
 */
Ext.define('system.abasis.template.biz.TemplateRename', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.abasis.template.biz.TemplateOper'),
	initComponent:function(){
	    var me = this;
	    
	    me.url = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '路径',
	    	name : 'url'
	    });
	    
	    me.mc = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '名称',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'name',
	    	maxLength: 400,
	    	allowBlank: false,
	    	padding: '6 0 0 0',
	    });
	    
	    var formpanel = new Ext.form.Panel({
	    	defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '10 0 0 10',
			items : [me.url, me.mc]
	    });
	    Ext.apply(this,{
	        width:400,
	        height:120,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[formpanel],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    zIsSpace : true,
			    handler : function() {
    				me.oper.renameSubmit(formpanel , me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});