﻿/**
 * 新建模板
 * @author lmb
 */
Ext.define('system.abasis.template.biz.TemplateAdd', {
	extend : 'Ext.window.Window',
    requires : ['system.widget.FsdTreeComboBox'],
	grid : null,
	oper : Ext.create('system.abasis.template.biz.TemplateOper'),
	initComponent:function(){
	    var me = this;

        var loginUser = me.oper.util.getParams("user");

        var bm = Ext.create('system.widget.FsdTextField',{
            fieldLabel: '模板编码',
            labelAlign:'right',
            labelWidth:60,
            width : 440,
            name: 'code',
            maxLength: 20,
            allowBlank: false
        });

        var mc = Ext.create('system.widget.FsdTextField',{
            fieldLabel: '模板名称',
            labelAlign:'right',
            labelWidth:60,
            width : 440,
            name: 'name',
            maxLength: 20,
            allowBlank: false
        });

        var Paht = Ext.create('system.widget.FsdTextFileManage', {
            width : 440,
            zName : 'paht',
            zFieldLabel : '模板文件',
            zLabelWidth : 60,
            zIsUpButton : true,
            zFileType : 'file',
            zFileAutoName : true,
            zFileUpPath : me.oper.util.getSystemPath(),
            zManageType : 'upload',
            zAllowBlank: false
        });

        var ImageUrl1 = Ext.create('system.widget.FsdTextFileManage', {
            width : 440,
            zName : 'imageurl1',
            zFieldLabel : '预览图1',
            zLabelWidth : 60,
            zIsShowButton : true,
            zIsUpButton : true,
            zFileType : 'image',
            zFileAutoName : true,
            zFileUpPath : me.oper.util.getSystemPath(),
            zManageType : 'upload'
        });

        var ImageUrl2 = Ext.create('system.widget.FsdTextFileManage', {
            width : 440,
            zName : 'imageurl2',
            zFieldLabel : '预览图2',
            zLabelWidth : 60,
            zIsShowButton : true,
            zIsUpButton : true,
            zFileType : 'image',
            zFileAutoName : true,
            zFileUpPath : me.oper.util.getSystemPath(),
            zManageType : 'upload'
        });

        var ImageUrl3 = Ext.create('system.widget.FsdTextFileManage', {
            width : 440,
            zName : 'imageurl3',
            zFieldLabel : '预览图3',
            zLabelWidth : 60,
            zIsShowButton : true,
            zIsUpButton : true,
            zFileType : 'image',
            zFileAutoName : true,
            zFileUpPath : me.oper.util.getSystemPath(),
            zManageType : 'upload'
        });

        var ImageUrl4 = Ext.create('system.widget.FsdTextFileManage', {
            width : 440,
            zName : 'imageurl4',
            zFieldLabel : '预览图4',
            zLabelWidth : 60,
            zIsShowButton : true,
            zIsUpButton : true,
            zFileType : 'image',
            zFileAutoName : true,
            zFileUpPath : me.oper.util.getSystemPath(),
            zManageType : 'upload'
        });

        var ImageUrl5 = Ext.create('system.widget.FsdTextFileManage', {
            width : 440,
            zName : 'imageurl5',
            zFieldLabel : '预览图5',
            zLabelWidth : 60,
            zIsShowButton : true,
            zIsUpButton : true,
            zFileType : 'image',
            zFileAutoName : true,
            zFileUpPath : me.oper.util.getSystemPath(),
            zManageType : 'upload'
        });

        var beizhu = new Ext.create('system.widget.FsdTextField',{
            fieldLabel: '备注信息',
            labelAlign:'right',
            labelWidth:60,
            width : 440,
            name: 'remark',
            maxLength: 50
        });

        var form = Ext.create('Ext.form.Panel',{
            xtype : 'form',
            defaultType : 'textfield',
            header : false,// 是否显示标题栏
            border : 0,
            padding : '20 0 0 20',
            items : [{
                name: "id",
                xtype: "hidden"
            } , bm, mc, Paht, ImageUrl1, ImageUrl2, ImageUrl3, ImageUrl4, ImageUrl5, beizhu]
        });

        Ext.apply(this,{
            width:490,
            height:350,
            bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
            resizable:false,// 改变大小
            items :[form],
            buttons : [{
                name : 'btnsave',
                text : '保存',
                iconCls : 'acceptIcon',
                handler : function() {
                    me.oper.addformSubmit(form, me.grid);
                }
            }, {
                text : '关闭',
                iconCls : 'deleteIcon',
                handler : function() {
                    me.close();
                }
            }]
        });
	    this.callParent(arguments);
	}
});