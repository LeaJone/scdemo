﻿/**
 * 模板编辑界面
 * @author lmb
 */
Ext.define('system.abasis.template.biz.TemplateEdit', {
	extend : 'Ext.panel.Panel',
	url: '',
	
	oper : Ext.create('system.abasis.template.biz.TemplateOper'),
	
	initComponent : function() {
		var me = this;
		
	    me.url = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '路径',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 920,
	    	name: 'url',
	    	maxLength: 50,
	    	colspan : 2,
	    	readOnly : true,
	    });
	    
	    me.content = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '内容',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 940,
	        height : 500,
	        margin:'0 0 28 0',
	    	name: 'content',
	    	colspan : 2
	    });
	    
	    me.form = Ext.create('Ext.form.Panel',{
				border : false,
				padding : '10 0 30 20',
				layout: {
			        type: 'table', 
			        columns: 2 //每行有几列
			    },
				items : [me.url, me.content]
	    });
	    
		Ext.apply(me, {
        	title: '模板信息编辑',
			autoScroll : true,
	        tbar : [{
				xtype : "fsdbutton",
				popedomCode : 'wzbj',
				text : "内容提交",
	        	name : 'btnsave',
				iconCls : 'acceptIcon',
				handler : function(button) {
					me.oper.formSubmit(me.form);
				}
			}],
	        items :[me.form]
	    });
		me.callParent(arguments);
	}
});