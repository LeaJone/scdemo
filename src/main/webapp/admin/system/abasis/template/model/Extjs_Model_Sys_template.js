﻿/**
 *sys_template Model
 *@author CodeSystem
 *文件名     Extjs_Model_Sys_template
 *Model名    model_sys_template
 */

Ext.define('model_sys_template', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'name',
            type : 'string'
        }
        ,
        {
            name : 'code',
            type : 'string'
        }
        ,
        {
            name : 'adddate',
            type : 'string'
        }
    ]
});
