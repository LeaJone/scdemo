﻿/**
 *sys_template Model
 *@author CodeSystem
 *文件名     Extjs_Model_Sys_template
 *Model名    model_sys_template
 */

Ext.define('model_sys_templatefile', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'name',
            type : 'string'
        }
        ,
        {
            name : 'type',
            type : 'string'
        }
        ,
        {
            name : 'size',
            type : 'string'
        }
        ,
        {
            name : 'updateTime',
            type : 'string'
        }
        ,
        {
            name : 'url',
            type : 'string'
        }
    ]
});
