﻿/**
 *sys_template Column
 *@author CodeSystem
 *文件名     Extjs_Column_Sys_template
 *Columns名  column_sys_template
 */

Ext.define('column_sys_templatefile', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            width : 40
        }
        ,
        {
            header : '模板名称',
            dataIndex : 'name',
            sortable : true,
            flex : 3,
	        renderer :function(value, metadata, record, rowIndex, columnIndex, store){
	        	return "<a href='#' onClick=\"openFile('"+store.getAt(rowIndex).get('type')+"', '"+store.getAt(rowIndex).get('url')+"');\">"+ value +"</a>";
  			}
        }
        ,
        {
            header : '类型',
            dataIndex : 'type',
            sortable : true,
            align : 'left',
            flex : 1,
	        renderer :function(value, metadata, record, rowIndex, columnIndex, store){
	        	if(value == "0"){
	        		return "文件夹";
	        	}else if(value == "1"){
	        		return "文件";
	        	}
  			}
        }
        ,
        {
            header : '大小',
            dataIndex : 'size',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '修改时间',
            dataIndex : 'updateTime',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '操作',
            dataIndex : 'logdepict',
            sortable : true,
            align : 'center',
            flex : 1,
	        renderer :function(value, metadata, record, rowIndex, columnIndex, store){
	        	
	        	if(store.getAt(rowIndex).get('type') == "0"){
	        		return "<a href='#' onClick=\"openFile('"+store.getAt(rowIndex).get('type')+"', '"+store.getAt(rowIndex).get('url')+"');\">进入</a> | <a href='#' onClick=\"renameFile('"+store.getAt(rowIndex).get('type')+"', '"+store.getAt(rowIndex).get('url')+"', '"+store.getAt(rowIndex).get('name')+"');\">重命名</a> | <a href='#' onClick=\"deleteFile('"+store.getAt(rowIndex).get('type')+"', '"+store.getAt(rowIndex).get('url')+"');\">删除</a>";
	        	}else if(store.getAt(rowIndex).get('type') == "1"){
	        		return "<a href='#' onClick=\"openFile('"+store.getAt(rowIndex).get('type')+"', '"+store.getAt(rowIndex).get('url')+"');\">编辑</a> | <a href='#' onClick=\"renameFile('"+store.getAt(rowIndex).get('type')+"', '"+store.getAt(rowIndex).get('url')+"', '"+store.getAt(rowIndex).get('name')+"');\">重命名</a> | <a href='#' onClick=\"deleteFile('"+store.getAt(rowIndex).get('type')+"', '"+store.getAt(rowIndex).get('url')+"');\">删除</a>";
	        	}
  			}
        }
    ]
});
