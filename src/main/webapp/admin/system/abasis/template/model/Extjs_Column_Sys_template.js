﻿/**
 *sys_template Column
 *@author CodeSystem
 *文件名     Extjs_Column_Sys_template
 *Columns名  column_sys_template
 */

Ext.define('column_sys_template', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            width : 40
        }
        ,
        {
            header : '模板名称',
            dataIndex : 'name',
            sortable : true,
            flex : 2,
	        renderer :function(value, metadata, record, rowIndex, columnIndex, store){
	        	return "<a href='#' onClick=\"openFile('"+store.getAt(rowIndex).get('type')+"', '"+store.getAt(rowIndex).get('url')+"');\">"+ value +"</a>";
  			}
        }
        ,
        {
            header : '编码',
            dataIndex : 'code',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '新增时间',
            dataIndex : 'adddate',
            sortable : true,
            flex : 1,
            renderer :function(value, metaData, record){
                return system.UtilStatic.formatDateTime(value);
            }
        }
        ,
        {
            header : '预览',
            dataIndex : 'logdepict',
            sortable : true,
            align : 'center',
            flex : 1,
            renderer :function(value, metadata, record, rowIndex, columnIndex, store){
                return "<a target='_blank' href='../template/"+ store.getAt(rowIndex).get('code') +"/index.htm'>预览</a>";
            }
        }
    ]
});
