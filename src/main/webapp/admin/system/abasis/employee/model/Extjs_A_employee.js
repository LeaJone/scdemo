﻿/**
 *a_employee Model
 *@author CodeSystem
 *文件名     Extjs_A_employee
 *Model名    model_a_employee
 *Columns名  column_a_employee, column_a_employee_role
 */

Ext.define('model_a_employee', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'branchid',
            type : 'string'
        }
        ,
        {
            name : 'branchname',
            type : 'string'
        }
        ,
        {
            name : 'code',
            type : 'string'
        }
        ,
        {
            name : 'loginname',
            type : 'string'
        }
        ,
        {
            name : 'realname',
            type : 'string'
        }
        ,
        {
            name : 'mobile',
            type : 'string'
        }
        ,
        {
            name : 'email',
            type : 'string'
        }
        ,
        {
            name : 'sex',
            type : 'string'
        }
        ,
        {
            name : 'sexname',
            type : 'string'
        }
        ,
        {
            name : 'status',
            type : 'string'
        }
        ,
        {
            name : 'statusname',
            type : 'string'
        }
        ,
        {
            name : 'type',
            type : 'string'
        }
        ,
        {
            name : 'typename',
            type : 'string'
        }
        ,
        {
            name : 'usertype',
            type : 'string'
        }
        ,
        {
            name : 'usertypename',
            type : 'string'
        }
        ,
        {
            name : 'academyname',
            type : 'string'
        }
        ,
        {
            name : 'registerclass',
            type : 'string'
        }
        ,
        {
            name : 'xueliid',
            type : 'string'
        }
        ,
        {
            name : 'xueliname',
            type : 'string'
        }
        ,
        {
            name : 'resourceid',
            type : 'string'
        }
        ,
        {
            name : 'resourcename',
            type : 'string'
        }
        ,
        {
            name : 'workaddress',
            type : 'string'
        }
        ,
        {
            name : 'areatypename',
            type : 'string'
        }
        ,
        {
            name : 'studymajorname',
            type : 'string'
        }
        ,
        {
            name : 'busymajorname',
            type : 'string'
        }
        ,
        {
            name : 'iswechatbind',
            type : 'string'
        }
        ,
        {
            name : 'iswechatpush',
            type : 'string'
        }
    ]
});


/**
 *a_employee Columns
 *@author CodeSystem
 */
Ext.define('column_a_employee', {
    columns: [
		{
			header : '学号/工号(账号)',
			dataIndex : 'code'
		}
        ,
        {
            header : '姓名',
            dataIndex : 'realname'
        }
		,
        {
            header : '院系',
            dataIndex : 'branchname'
        }
		,
		{
            header : '班级',
            dataIndex : 'registerclass',
            renderer :function(value){
                if(value == ""){
                    return "未知"
                }
                return value;
            }
        }
        ,
        {
            header : '性别',
            dataIndex : 'sexname',
            renderer :function(value){
                if(value == ""){
                    return "未知"
                }
                return value;
            }
        }
        ,
        {
            header : '类型',
            dataIndex : 'usertypename'
        }
        ,
        {
            header : '类别',
            dataIndex : 'typename'
        }
        ,
        {
            header : '微信',
            dataIndex : 'iswechatbind',
            sortable : true,
            align : 'center',
            renderer :function(value){
                if(value == 'true'){
                    value="<font style='color:green;'>已绑定</font>";
                }else{
                    value="<font>未绑定</font>";
                }
                return value;
            }
        }
        ,
        {
            header : '推送',
            dataIndex : 'iswechatpush',
            sortable : true,
            align : 'center',
            renderer :function(value){
                if(value == 'true'){
                    value="<font style='color:blue;'>是</font>";
                }else{
                    value="<font>否</font>";
                }
                return value;
            }
        }
        ,
        {
            header : '状态',
            dataIndex : 'statusname',
            renderer :function(value ,metaData ,record ){
				if(value == '启用'){
					value="<font style='color:green;'>启用</font>";
				}else{
					value="<font style='color:red;'>停用</font>";
				}
				return value;
			}
        }
    ]
});

/**
 *a_employee Columns
 *@author CodeSystem
 */
Ext.define('column_a_employeeemail', {
    columns: [
    	{
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            align : 'center',
            flex : 0.5
        }
    	,
        {
            header : '姓名',
            dataIndex : 'realname',
            align : 'center',
            flex : 1
        }
        ,
        {
            header : '院系',
            dataIndex : 'branchname',
            align : 'center',
            flex : 1
        }
    ]
});

/**
 *a_employee Columns
 *@author CodeSystem
 */
Ext.define('column_a_employee_zj', {
    columns: [
		{
            header : '姓名',
            dataIndex : 'realname'
        }
		,
		{
            header : '学术领域',
            dataIndex : 'areatypename'
        }
		,
        {
            header : '性别',
            dataIndex : 'sexname'
        }
		,
        {
            header : '所学专业',
            dataIndex : 'studymajorname'
        }
		,
		{
            header : '从事专业',
            dataIndex : 'busymajorname'
        }
		,
		{
            header : '职称',
            dataIndex : 'registerclass'
        }
        ,
        {
            header : '工作单位 ',
            dataIndex : 'workaddress'
        }
        ,
        {
            header : '来源',
            dataIndex : 'resourcename'
        }
        ,
        {
            header : '类别',
            dataIndex : 'typename'
        }
        ,
        {
            header : '状态',
            dataIndex : 'statusname',
            renderer :function(value ,metaData ,record ){
				if(value == '启用'){
					value="<font style='color:green;'>启用</font>";
				}else{
					value="<font style='color:red;'>停用</font>";
				}
				return value;
			}
        }
    ]
});

/**
 *a_employee Columns
 *@author CodeSystem
 */
Ext.define('column_a_employee_role', {
    columns: [
        {
            header : '部门',
            dataIndex : 'branchname'
        }
        ,
        {
            header : '姓名',
            dataIndex : 'realname'
        }
        ,
        {
            header : '类型',
            dataIndex : 'typename'
        }
        ,
        {
            header : '状态',
            dataIndex : 'statusname'
        }
    ]
});

/**
 * 项目管理选择指导教师、项目负责人面板
 */
Ext.define('column_project_employee', {
    columns: [
        {
            header : '姓名',
            dataIndex : 'realname',
            align : 'center',
            flex : 1
        }
        ,
        {
            header : '性别',
            dataIndex : 'sexname',
            align : 'center',
            flex : 0.5
        }
        ,
        {
            header : '院系',
            dataIndex : 'academyname',
            align : 'center',
            flex : 1
        }
        ,
        {
            header : '专业',
            dataIndex : 'branchname',
            align : 'center',
            flex : 1
        }
    ]
});
