﻿/**
 * 人员添加界面
 * @author lumingbao
 */
Ext.define('system.abasis.employee.biz.UserInfoEdit', {
	extend : 'Ext.window.Window',
	requires : ['system.widget.FsdTreeComboBox'],
	grid : null,
	treepanel : null,
	oper : Ext.create('system.abasis.employee.biz.UserOper'),
	initComponent:function(){
	    var me = this;
	    
	    var dwid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '单位编号',
	    	name: 'companyid',
	    	value : me.oper.util.getParams("company").id,
	    });
	    var dwmc = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '单位名称',
	    	name: 'companyname',
	    	value : me.oper.util.getParams("company").name,
	    });
	    
	    var loginUser = me.oper.util.getParams("user");
	     
	    me.videourl = Ext.create('system.widget.FsdTextFileManage', {
	        width : 390,
	    	zName : '',
	    	zFieldLabel : '信息上传',
	    	zLabelWidth : 70,
	    	zIsShowButton : false,
	    	zIsUpButton : true,
	    	zIsReadOnly : true,
	    	zFileType : 'file',
	    	zFileAutoName : false,
	    	zFileUpPath : me.oper.util.getVideoPath(),
	    	zManageType : 'manage'
	    });
	    
	    if (loginUser.isadmin != true){
		    ssjg.setReadOnly(true);
		    ssjg.setValue(loginUser.branchid);
		    ssjg.setText(loginUser.branchname);
		    ssjgname.setValue(loginUser.branchname);
		    rylb.setReadOnly(true);
	    }
	    
	    me.on('show' , function(){
	    	ssjg.setText(ssjgname.getValue());
	    	/*sslb.setText(sslbname.getValue());
	    	ssdy.setText(ssdyname.getValue());
	    	sshy.setText(sshyname.getValue());*/
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [{
                name: "id",
                xtype: "hidden"
            } , dwid ,dwmc, me.videourl]
        });
	        
	    Ext.apply(this,{
	        width:490,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '上传',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.xinxiSubmit(form, me.grid, me.treepanel);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});