/**
 * 用户管理界面操作类
 * @author fsd
 * @since 4014-05-27
 */
Ext.define('system.abasis.employee.biz.GtnwUserOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 添加
     */
	add : function(grid){
	    var win = Ext.create('system.abasis.employee.biz.GtnwUserEdit');
        win.setTitle('人员添加');
        win.grid = grid;
        win.modal = true;
        win.show();
	},
	
	/**
     * 修改
     */
	editor : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{

            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            
            var win = Ext.create('system.abasis.employee.biz.GtnwUserEdit');
            win.setTitle('人员修改');
            win.modal = true;
            win.grid = grid;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'A_Employee/load_EmployeeByid.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},
 	
 	/**
     * 查看
     */
	view : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if(data.length > 0){
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.abasis.employee.biz.GtnwUserEdit');
            win.setTitle('人员查看');
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'A_Employee/load_EmployeeByid.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},
	
	/**
     * 删除
     */
	del : function(grid){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
		        msg : '您确定要删除吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
					    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
			            });
             			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('A_Employee/del_Employee_qx_rysc.do' , param ,
                        function(response, options){
            				Ext.MessageBox.alert('提示', '删除成功！');
                        	grid.getStore().reload(); 
                        }, null, me.form);
                    }
                }
            });
        }
	},

	/**
     * 重置密码
     */
	resetPassword : function(grid){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
		        msg : '您确定要重置密码吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
					    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
			            });
             			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('A_Employee/pwd_Employee_qx_rybj.do' , param ,
                        function(response, options){
            				Ext.MessageBox.alert('提示', '密码重置成功！');
                        });
                    }
                }
            });
        }
	},
 	
 	/**
     * 更改用户密码
     */
	updatePassword : function(grid){
 		var me = this;
		 // 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要更改的记录！');
        }else {
        	var win = Ext.create('system.abasis.employee.biz.GtnwUpdatePWD');
            win.modal = true;
            win.updateFun = function(pwd){
            	var dir = new Array();　
                Ext.Array.each(data, function(items) {
                        var id = items.data.id;
                        dir.push(id);
        		});
                var pram = {ids : dir, pwd : pwd};
                var param = {jsonData : Ext.encode(pram)};
                me.util.ExtAjaxRequest('A_Employee/pwdjmmm_Employee_qx_rybj.do', param,
                function(response, options){
    				Ext.MessageBox.alert('提示', '密码修改成功！');
                });
            };
            win.show();
        }
	},

	/**
     * 是否启用停用
     */
	isEnabled : function(grid, isQT){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
		        msg : '您确定要修改人员状态吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
					    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
			            });
             			var pram = {ids : dir, isenabled : isQT};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('A_Employee/enabled_Employee_qx_rybj.do' , param ,
                        function(response, options){
            				Ext.MessageBox.alert('提示', '人员状态修改成功！');
                        	grid.getStore().reload(); 
                        });
                    }
                }
            });
        }
	},
	
	/**
     * 表单提交
     */
     formSubmit : function(formpanel , grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('A_Employee/save_Employee_qx_rybj.do' , formpanel.form , '正在提交数据,请稍候.....',
			function(form, action){
			    formpanel.up('window').close();
			    grid.getStore().reload();
				Ext.MessageBox.alert('提示', '保存成功！');
			},
			function (form, action){
				Ext.MessageBox.alert('异常', action.result.msg);
			});
        }
     },
 	
 	/**
      * 表单提交
      */
      formSubmitSelf : function(formpanel){
         var me = this;
         if (formpanel.form.isValid()) {
             me.util.ExtFormSubmit('A_Employee/save_EmployeeSelf.do' , formpanel.form , '正在提交数据,请稍候.....',
 			function(form, action){
 			    formpanel.up('window').close();
 				Ext.MessageBox.alert('提示', '个人信息修改成功，下次登录生效！');
 			},
 			function (form, action){
 				Ext.MessageBox.alert('异常', action.result.msg);
 			});
         }
      },
     
     /**
      * 修改密码表单提交
      */
      pwdEditFormSubmit : function(formpanel){
         var me = this;
         if (formpanel.form.isValid()) {
             me.util.ExtFormSubmit('A_Employee/pwdedit_Employee.do' , formpanel.form , '正在提交数据,请稍候.....',
 			function(form, action){
 			    formpanel.up('window').close();
 				Ext.MessageBox.alert('提示', '密码修改成功，下次登录生效！');
 			},
 			function (form, action){
 				Ext.MessageBox.alert('异常', action.result.msg);
 			});
         }
      },
     
     /**
 	 * 加载权限菜单
 	 */
    LoadMenuTree : function(treepanel){
 	    var me = this;
 	    treepanel.getRootNode().removeAll();
 	    me.util.ExtAjaxRequest('Sys_SystemMenu/get_AllQxMenuCheck.do', null,
 	    function(response, options, respText){
 	    	if(respText.data != ""){
 	    		treepanel.getRootNode().appendChild(respText.data);
 	            treepanel.expandAll();
 	    	}
 	    },
 	    null, treepanel);
 	},
	/**
	 * 加载右侧栏目
	 */
	LoadSubjectTree : function(treepanel){
	    var me = this;
	    treepanel.getRootNode().removeAll();
	    me.util.ExtAjaxRequest('B_Subject/get_AllQxSubjectCheck.do', {jsonData : Ext.encode({id : 'FSDMAIN'})},
	    function(response, options, respText){
	    	if(respText.data != ""){
	    		treepanel.getRootNode().appendChild(respText.data);
	            treepanel.expandAll();
	    	}
	    }, null, treepanel);
	},
	/**
	 * 加载右侧栏目Wap
	 */
	LoadSubjectWapTree : function(treepanel){
	    var me = this;
	    treepanel.getRootNode().removeAll();
	    me.util.ExtAjaxRequest('B_Subject/get_AllQxSubjectWapCheck.do', {jsonData : Ext.encode({id : 'FSDMOBILE'})},
	    function(response, options, respText){
	    	if(respText.data != ""){
	    		treepanel.getRootNode().appendChild(respText.data);
	            treepanel.expandAll();
	    	}
	    }, null, treepanel);
	},
	/**
	 * 加载右侧机构部门
	 */
	LoadBranchTree : function(treepanel){
	    var me = this;
	    treepanel.getRootNode().removeAll();
	    me.util.ExtAjaxRequest('A_Branch/get_AllQxBranchCheck.do', null,
	    function(response, options, respText){
	    	if(respText.data != ""){
	    		treepanel.getRootNode().appendChild(respText.data);
	            treepanel.expandAll();
	    	}
	    }, null, treepanel);
	},

	/**
     * 获取人员分配角色
     */
	GetUserPopedom : function(form, ids, grid, treepopedom, treesubject, treesubjectwap,treebranch){
	    var me = this;
	    if (ids.length > 0){
		    var pram = {ids : ids};
	        var param = {jsonData : Ext.encode(pram)};
	        me.util.ExtAjaxRequest('Sys_PopedomAllocate/get_RoleIdsByUserIds.do' , param ,
	        function(response, options, respText){
	        	var ids = respText.data;
	        	var gridIDs = new Array();
	        	grid.getSelectionModel().deselectAll();
	        	var models = new Array();
	        	grid.store.each(function (row) {
	        		Ext.Array.each(ids, function(id) {
	        			if (row.data.id == id){
		        			gridIDs.push(id);
		        			models.push(row);
			                return false;
	        			}
	        		});
	        	});
	        	if (models.length > 0)
	        		grid.getSelectionModel().select(models, true, true);
	        	me.GetGroupPopedom(form, gridIDs, treepopedom, treesubject, treesubjectwap,treebranch);
	        }, null, form);
	    }else{
	    }
	},
	
	/**
     * 获取角色权限信息
     */
	GetGroupPopedom : function(form, ids, treepopedom, treesubject, treesubjectwap,treebranch){
	    var me = this;
	    if (ids.length > 0){
		    var pram = {ids : ids};
	        var param = {jsonData : Ext.encode(pram)};
	        me.util.ExtAjaxRequest('Sys_PopedomAllocate/get_PopedomIdsByRoleIds.do' , param ,
	        function(response, options, respText){
	        	var map = respText.data;
	        	me.FindChildNode(treepopedom.getRootNode(), map.qxlxcd);
	        	me.FindChildNode(treesubject.getRootNode(), map.qxlxlm);
	        	me.FindChildNode(treesubjectwap.getRootNode(), map.qxlxlmw);
	        	me.FindChildNode(treebranch.getRootNode(), map.qxlxjg);
	        }, null, form);
	    }else{
	    	var array = new Array();
	    	me.FindChildNode(treepopedom.getRootNode(), array);
        	me.FindChildNode(treesubject.getRootNode(), array);
        	me.FindChildNode(treesubjectwap.getRootNode(), array);
        	me.FindChildNode(treebranch.getRootNode(), array);
	    }
	},
	FindChildNode : function(node, ids) {  
	    var me = this;
        var childnodes = node.childNodes;  
        Ext.Array.each(childnodes, function(rootnode) {
        	rootnode.raw.checked = false;
        	rootnode.set('checked',false);
        	Ext.Array.each(ids, function(id) {
	            if (rootnode.raw.id == id) {
	                rootnode.raw.checked = true;
	            	rootnode.set('checked',true);
	                return false;
	            }
        	});
            if (rootnode.childNodes.length > 0){
            	me.FindChildNode(rootnode, ids);
            }
        });
    },
	
	/**
	 * 保存数据
	 */
	SaveUserRole : function(form, usergrid, rolegrid){
		var me = this;
		var userModels = usergrid.getSelectionModel().getSelection();
		if (userModels.length == 0){
			Ext.MessageBox.alert('提示', '未选择需要授权人员信息！');
			return;
		}
		var userids = new Array();
		Ext.Array.each(userModels, function(umodel) {
			userids.push(umodel.data.id)
    	});
		var roleModels = rolegrid.getSelectionModel().getSelection();
		var roleids = new Array();
		if (roleModels.length != 0){
			Ext.Array.each(roleModels, function(rmodel) {
				roleids.push(rmodel.data.id)
	    	});
		}
		me.util.ExtAjaxRequest('Sys_PopedomAllocate/save_UserPopdom.do' , 
				{jsonData : Ext.encode({userids : userids , roleids : roleids})} ,
		function(response, options, respText){
			Ext.MessageBox.alert('提示', '保存成功！');
		}, null, form);
	},
	
	/**
	 * 用户重置密码
	 */
	ResetPassword : function(formpanel){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('A_Employee/update_EmployeePWD.do' , formpanel.form , '正在提交数据,请稍候.....',
			function(form, action){
			    formpanel.up('window').close();
			    Ext.MessageBox.alert('提示', '密码修改成，下次登录生效！');
			},
			function (form, action){
				Ext.MessageBox.alert('异常', action.result.msg);
			});
        }
     }
});