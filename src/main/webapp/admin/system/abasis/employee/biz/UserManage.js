﻿/**

 * 用户管理界面
 * @author lumingbao
 */

Ext.define('system.abasis.employee.biz.UserManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*', 'system.abasis.employee.model.Extjs_A_employee'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.abasis.employee.biz.UserOper'),
	
	createContent : function(){
		var me = this;
		
		var queryid = me.oper.util.getParams("company").id;

		/**
		 * 左边的树面板
		 */
		var treepanel = Ext.create("system.widget.FsdTreePanel", {
			icon : 'resources/admin/icon/chart_organisation.png',
			baseUrl : 'A_Branch/load_AsyncBranchTreeByPopdom.do',
			title: '组织机构',
			rootText : me.oper.util.getParams("company").name,
			rootId : me.oper.util.getParams("company").id,
			region : "west", // 设置方位
			collapsible : true,//折叠
			width: 200,
			split: true
		});
		
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_a_employee', // 显示列
			model: 'model_a_employee',
			baseUrl : 'A_Employee/load_data.do',
			tbar : {
				enableOverflow: true,//控件过多，出现下拉按钮
				items : [{
				xtype : "fsdbutton",
				text : "添加",
				iconCls : 'page_addIcon',
				handler : function(button) {
					me.oper.add(grid);
				}
			}, {
				xtype : "fsdbutton",
				text : "修改",
				iconCls : 'page_editIcon',
				handler : function(button) {
					me.oper.editor(grid);
				}
			}, {
				xtype : "fsdbutton",
				text : "删除",
				iconCls : 'page_deleteIcon',
				handler : function(button) {
				    me.oper.del(grid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid);
				}
			}, {
				xtype : "fsdbutton",
				text : "启用",
				iconCls : 'tickIcon',
				handler : function(button) {
				    me.oper.isEnabled(grid, true);
				}
			}, {
				xtype : "fsdbutton",
				text : "停用",
				iconCls : 'crossIcon',
				handler : function(button) {
				    me.oper.isEnabled(grid, false);
				}
			},  '-', {
					xtype : "fsdbutton",
					text : "开启推送",
					iconCls : 'tickIcon',
					handler : function() {
						me.oper.isWeChatPush(grid, 'true');
					}
				}, {
					xtype : "fsdbutton",
					text : "关闭推送",
					iconCls : 'crossIcon',
					handler : function() {
						me.oper.isWeChatPush(grid, 'false');
					}
				}, '-', {
					xtype : "fsdbutton",
					text : "扫描绑定",
					iconCls : 'wechat1Icon',
					handler : function() {
						me.oper.userWeChat(grid);
					}
				}, {
					xtype : "fsdbutton",
					text : "查看微信",
					iconCls : 'wechat1Icon',
					handler : function() {
						me.oper.userView(grid);
					}
				}, '-', {
					xtype : "fsdbutton",
					text : "用户同步",
					iconCls : 'usergoIcon',
					handler : function() {
						me.oper.employeeSync(grid);
					}
				}, '->', new Ext.form.TextField( {
				id : 'queryEmployeeName',
				name : 'queryParam',
				emptyText : '请输入姓名查询',
				enableKeyEvents : true,
				listeners : {
					specialkey : function(field, e) {
						if (e.getKey() == Ext.EventObject.ENTER) {
							grid.getStore().reload();
						}
					}
				},
				width : 120
			}), {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}, {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]}		
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  Ext.getCmp('queryEmployeeName').getValue();
			var pram = {fid : queryid , name : name};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		var contentpanel = Ext.create("Ext.panel.Panel", {
			frame:true,
			split: true,
			region: 'center',
			layout: 'fit',
			title: '人员信息',
			items: [grid]
		});
		
		var page1_jExtPanel1_obj = new Ext.panel.Panel({
			bodyPadding: 3,
			header : false,
			border : 0,
			split: true,
			layout: 'border',
			items: [treepanel,contentpanel]
		});
		
		treepanel.on('itemclick', function(view,record,item,index,e){
			queryid = record.raw.id;
			grid.getStore().reload();
		});
		
		return page1_jExtPanel1_obj;
	}
});