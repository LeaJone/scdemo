﻿/**
 * 人员添加界面
 * @author lumingbao
 */
Ext.define('system.abasis.employee.biz.ZhuanjiaEdit', {
	extend : 'Ext.window.Window',
	requires : ['system.widget.FsdTreeComboBox',
				'system.abasis.major.model.Extjs_Column_Z_major',
                'system.abasis.major.model.Extjs_Model_Z_major',
        		'system.zgyxy.domain.model.Extjs_Column_Z_domain',
        		'system.zgyxy.domain.model.Extjs_Model_Z_domain'],
	grid : null,
	zjid : '',
	oper : Ext.create('system.abasis.employee.biz.ZhuanjiaOper'),
	initComponent:function(){
	    var me = this;

	    var loginUser = me.oper.util.getParams("user");
	   
	    var zh = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '登陆账号',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'code',
	    	maxLength: 20,
	    	allowBlank: false
	    });
	    
	    var xm = new Ext.create('system.widget.FsdTextFieldZJF',{
	    	fieldLabel: '姓名',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'realname',
	    	fname : 'symbol',
	    	maxLength: 25,
	    	allowBlank: false
	    });
	    
	    var xb = new Ext.create('system.widget.FsdComboBoxZD',{
	    	fieldLabel: '性别',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'sex',
	        key : 'xb',//参数
    	    hiddenName : 'sexname'//提交隐藏域Name
	    });
	    var xbcode = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '性别名',
	    	name: 'sexname'
	    });

        var xslyname = Ext.create('system.widget.FsdTextGrid', {
            zFieldLabel : '主领域',
            zLabelWidth : 80,
            width : 360,
            zName : 'areatypename',
            zColumn : 'column_z_domainxz', // 显示列
            zModel : 'model_z_domain',
            zBaseUrl : 'z_domain/load_AllDomainListByPopdom.do',
            zAllowBlank : false,
            zGridType : 'list',
            zGridWidth : 700,//弹出表格宽度
            zGridHeight : 600,//弹出表格高度
            zIsText1 : true,
            zTxtLabel1 : '领域名称',
            zFunChoose : function(data){
                xsly.setValue(data.id);
                xslyname.setValue(data.title);
            },
            zFunQuery : function(txt1){
                return {fid : 'FSDMAIN', txt : txt1};
            }
        });
        var xsly = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '领域编码',
            name: 'areatypeid'
        });
	    
	    var ssjg = new system.widget.FsdTreeComboBox({
	    	fieldLabel : '所属机构',
	    	labelAlign:'right',
	    	labelWidth:80,
	    	rootText : me.oper.util.getParams("company").name,
	    	rootId : me.oper.util.getParams("company").id,
	    	rootVisible : false,//不显示根节点
	        width : 360,
	        name : 'branchid',
	        baseUrl:'A_Branch/load_AsyncBranchTreeByPopdom.do',//访问路劲
	        hiddenName : 'branchname'
	    });
	    var ssjgname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属机构',
	    	name: 'branchname'
	    });
	    
	    me.zy1name = Ext.create('system.widget.FsdTextGrid', {
	    	zFieldLabel : '所学专业', 
	    	zLabelWidth : 80,
	    	width : 360,
		    zName : 'studymajorname',
		    zColumn : 'column_z_majorxz', // 显示列
		    zModel : 'model_z_major',
		    zBaseUrl : 'z_major/load_AllMajorListByPopdom.do',
		    zGridType : 'list',
			zGridWidth : 700,//弹出表格宽度
			zGridHeight : 600,//弹出表格高度
		    zIsText1 : true,
		    zTxtLabel1 : '专业名称',
		    zFunChoose : function(data){
		    	me.zy1.setValue(data.id);
		    	me.zy1name.setValue(data.title);
		    },
		    zFunQuery : function(txt1){
		    	return {fid : 'FSDMAIN', txt : txt1};
		    }
	    });
	    me.zy1 = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所学专业',
	    	name: 'studymajorid'
	    });
	    
	    me.zy2name = Ext.create('system.widget.FsdTextGrid', {
	    	zFieldLabel : '从事专业', 
	    	zLabelWidth : 80,
	    	width : 360,
		    zName : 'busymajorname',
		    zColumn : 'column_z_majorxz', // 显示列
		    zModel : 'model_z_major',
		    zBaseUrl : 'z_major/load_AllMajorListByPopdom.do',
		    zGridType : 'list',
			zGridWidth : 700,//弹出表格宽度
			zGridHeight : 600,//弹出表格高度
		    zIsText1 : true,
		    zTxtLabel1 : '专业名称',
		    zFunChoose : function(data){
		    	me.zy2.setValue(data.id);
		    	me.zy2name.setValue(data.title);
		    },
		    zFunQuery : function(txt1, txt2, txt3, txt4){
		    	return {fid : 'FSDMAIN', txt : txt1};
		    }
	    });
	    me.zy2 = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '从事专业',
	    	name: 'busymajorid'
	    });
	    	    
	    var zc = new system.widget.FsdTreeComboBox({
	    	fieldLabel : '职称',
	    	labelAlign:'right',
	    	labelWidth:80,
	    	rootText : me.oper.util.getParams("company").name,
	    	rootId : me.oper.util.getParams("company").id,
	    	rootVisible : false,//不显示根节点
	        width : 360,
	        name : 'registerclassid',
	        baseUrl:'z_professor/load_AsyncProfessorTreeByPopdom.do',//访问路劲
	        hiddenName : 'registerclass'
	    });
	    var zcname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '职称',
	    	name: 'registerclass'
	    });	
	   
	    var gzdw = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '工作单位',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'workaddress',
	    	maxLength: 200
	    });
	    
	    var zw = new Ext.create('system.widget.FsdComboBoxZD',{
	    	fieldLabel: '职务',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'jobtypeid',
	        key : 'zw',//参数
    	    hiddenName : 'jobtypename'//提交隐藏域Name
	    });
	    var zwname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '职务',
	    	name: 'jobtypename'
	    });
	    
	    var xl = new Ext.create('system.widget.FsdComboBoxZD',{
	    	fieldLabel: '最高学历',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'xueliid',
	        key : 'xl',//参数
    	    hiddenName : 'xueliname'//提交隐藏域Name
	    });
	    var xlname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '最高学历',
	    	name: 'xueliname'
	    });
	    
	    var xw = new Ext.create('system.widget.FsdComboBoxZD',{
	    	fieldLabel: '最高学位',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'xueweiid',
	        key : 'xw',//参数
    	    hiddenName : 'xueweiname'//提交隐藏域Name
	    });
	    var xwname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '最高学位',
	    	name: 'xueweiname'
	    });
	     	      
	    var sjhm = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '手机号码',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'mobile',
	    	maxLength: 11,
	    	minLength: 11
	    });
	    
	    var email = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '电子邮箱',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'email',
			vtype : 'email',
	    	maxLength: 100
	    });
	    
	    var ly = new Ext.create('system.widget.FsdComboBoxZD',{
	    	fieldLabel: '专家来源',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'resourceid',
	        key : 'ryly',//参数
    	    hiddenName : 'resourcename'//提交隐藏域Name
	    });
	    var lycode = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '用户类别名',
	    	name: 'resourcename'
	    });
	    
	    var rylb = new Ext.create('system.widget.FsdComboBoxZD',{
	    	fieldLabel: '人员类别',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'type',
	        key : 'rylb',//参数
    	    hiddenName : 'typename'//提交隐藏域Name
	    });
	    var rylbcode = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '用户类别名',
	    	name: 'typename'
	    });

	    var ryzt = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'人员状态',
	        labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
    	    name : 'status',//提交到后台的参数名
	        key : 'ryzt',//参数
    	    hiddenName : 'statusname'//提交隐藏域Name
	    });
	    var ryztcode = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '人员状态名',
	    	name: 'statusname'
	    });
	    
	    var beizhu = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'remark',
	    	maxLength: 50
	    });

	    if (loginUser.isadmin != true){
		    rylb.setReadOnly(true);
	    }
	    
	    me.on('show' , function(){
	    	zc.setText(zcname.getValue());
            ssjg.setText(ssjgname.getValue());
	    });
	    
	    me.form1 = Ext.create('Ext.form.Panel',{
			border : 0.5,
			padding : '10 20 10 20',
			items : [{
				layout : 'column',
                border : false,
    	        width : 900,
    	        padding : '10 0 10 0',
				items : [{
	                columnWidth : 0.5,
	                border:false,
	                items:[xm, xb, xbcode, email, xw, xwname, me.zy2, me.zy2name, zw, zwname, ly, lycode, rylb, rylbcode, ssjg, ssjgname]
	            }, {
	                columnWidth : 0.5,
	                border:false,
	                items:[zh, sjhm, xl, xlname, me.zy1, me.zy1name, gzdw, zc, zcname, ryzt, ryztcode, beizhu]
	            }]
            }]
        });
	    
	    me.form2 = Ext.create('Ext.form.Panel',{
			border : 0.5,
			padding : '10 20 10 20',
			items : [{
				layout : 'column',
                border : false,
    	        width : 900,
    	        padding : '10 0 10 0',
				items : [{
	                columnWidth : 0.5,
	                border:false,
	                items:[ xsly, xslyname ]
	            }]
            }]
        });

        /**
         * 右下方的大选项卡控件
         */
        var tabPanel = new Ext.tab.Panel({
            layout : 'fit',
            activeTab: 0,
            split: true,//拖动
            deferredRender: true,//是否在显示每个标签的时候再渲染标签中的内容.默认true
            items: [
                {
                    xtype: 'panel',
                    layout: 'fit',
                    title: '基本信息',
                    items: [
                        me.form1
                    ]},{
                    xtype: 'panel',
                    layout: 'fit',
                    title: '研究领域',
                    items: [
                        me.form2
                    ]}
            ]
        });


        var buttonItem = [{
            name : 'btnsave',
            text : '保存',
            iconCls : 'acceptIcon',
            handler : function() {
                me.oper.formSubmit(me, me.form1, me.form2, me.grid, me.zjid);
            }
        }, {
            text : '关闭',
            iconCls : 'deleteIcon',
            handler : function() {
                me.close();
            }
        }];
        Ext.apply(this,{
            layout : 'fit',
            resizable:false,// 改变大小
            bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
            items :[tabPanel],
            buttons : buttonItem
        });
        this.callParent(arguments);
	}
});