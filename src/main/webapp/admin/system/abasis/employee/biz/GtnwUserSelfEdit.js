﻿/**
 * 用户自维护界面
 * @author lw
 */
Ext.define('system.abasis.employee.biz.GtnwUserSelfEdit', {
	extend : 'Ext.window.Window',
	oper : Ext.create('system.abasis.employee.biz.GtnwUserOper'),
	initComponent:function(){
	    var me = this;

	    var loginUser = me.oper.util.getParams("user");
	    
	    var bm = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '编码',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'code',
	    	maxLength: 20,
	    	readOnly : true,
	    	allowBlank: false
	    });
	    
	    var ssjg = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属机构',
	    	labelAlign:'right',
	    	labelWidth:60,
	    	rootText : me.oper.util.getParams("company").name,
	    	rootId : me.oper.util.getParams("company").id,
	    	rootVisible : false,//不显示根节点
	        width : 340,
	        name : 'branchid',
	        baseUrl:'A_Branch/load_AsyncBranchTree.do',//访问路劲
	    	readOnly : true,
	        allowBlank: false,
	        hiddenName : 'branchname'
	    });
	    var ssjgname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属机构',
	    	name: 'branchname'
	    });	    
	   
	    var dlm = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '登录名',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'loginname',
	    	maxLength: 20,
	    	allowBlank: false
	    });
	    
	    var xm = new Ext.create('system.widget.FsdTextFieldZJF',{
	    	fieldLabel: '姓名',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'realname',
//	    	fname : 'symbol',
	    	maxLength: 25,
	    	allowBlank: false
	    });
	   
	    var duty = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '职务',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'duty',
	    	maxLength: 20
	    });
	   
	    var roomnumber = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '房间号',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'roomnumber',
	    	maxLength: 20
	    });  

	    var xb = new Ext.create('system.widget.FsdComboBoxZD',{
	    	fieldLabel: '性别',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'sex',
	        key : 'xb',//参数
    	    hiddenName : 'sexname'//提交隐藏域Name
	    });
	    var xbcode = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '性别名',
	    	name: 'sexname'
	    });
	    
	    var email = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '电子邮箱',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'email',
			vtype : 'email',
	    	maxLength: 100
	    });
	    
	    var beizhu = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'remark',
	    	maxLength: 50
	    });
	    
	    me.on('show' , function(){
	    	ssjg.setText(ssjgname.getValue());
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [{
                name: "id",
                xtype: "hidden"
            } , 
            ssjgname, xbcode, 
            bm , ssjg, dlm, xm, duty, roomnumber, 
            xb, email, beizhu]
        });
	        
	    Ext.apply(this,{
	        width:400,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.formSubmitSelf(form);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});