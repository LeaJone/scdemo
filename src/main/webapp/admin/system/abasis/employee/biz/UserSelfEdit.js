﻿/**
 * 用户自维护界面
 * @author lw
 */
Ext.define('system.abasis.employee.biz.UserSelfEdit', {
	extend : 'Ext.window.Window',
	requires : ['system.widget.FsdTreeComboBox'],
	oper : Ext.create('system.abasis.employee.biz.UserOper'),
	initComponent:function(){
	    var me = this;

	    var loginUser = me.oper.util.getParams("user");
	    
	    var bm = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '编码',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'code',
	    	maxLength: 20,
	    	readOnly : true,
	    	allowBlank: false
	    });
	    
	    var ssjg = new system.widget.FsdTreeComboBox({
	    	fieldLabel : '所属机构',
	    	labelAlign:'right',
	    	labelWidth:60,
	    	rootText : me.oper.util.getParams("company").name,
	    	rootId : me.oper.util.getParams("company").id,
	    	rootVisible : false,//不显示根节点
	        width : 340,
	        name : 'branchid',
	        baseUrl:'A_Branch/load_AsyncBranchTree.do',//访问路劲
	    	readOnly : true,
	        allowBlank: false,
	        hiddenName : 'branchname'
	    });
	    var ssjgname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属机构',
	    	name: 'branchname'
	    });	    
	   
	    var dlm = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '登录名',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'loginname',
	    	maxLength: 20,
	    	allowBlank: false
	    });
	    
	    var xm = new Ext.create('system.widget.FsdTextFieldZJF',{
	    	fieldLabel: '姓名',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'realname',
	    	fname : 'symbol',
	    	maxLength: 20,
	    	allowBlank: false
	    });
	    
	    var zjf = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '助记符',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'symbol',
	    	maxLength: 20,
	    	allowBlank: false
	    });
	    
	    var sslb = Ext.create('system.widget.FsdTreeComboBox', {
	    	fieldLabel : '所属类别',
	    	labelAlign:'right',
	    	labelWidth:60,
	    	rootText : '所属类别',
	    	rootId : 'FSDUSER',
	        width : 340,
	        name : 'usertypeid',
	        baseUrl:'A_CompanyType/load_AsyncCompanyTypeTree.do',//访问路劲
	        allowBlank: false,
	        hiddenName : 'usertypename'//隐藏域Name
	    });
	    var sslbname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属类别',
	    	name: 'usertypename'
	    });
	    
	    var ssdy = Ext.create('system.widget.FsdTreeComboBox', {
	    	fieldLabel : '所属地域',
	    	labelAlign:'right',
	    	labelWidth:60,
	    	rootText : '地域类别',
	    	rootId : 'FSDAREA',
	        width : 340,
	        name : 'areatypeid',
	        baseUrl:'a_area/load_AsyncAreaTree.do',//访问路劲
	        allowBlank: false,
	        hiddenName : 'areatypename'//隐藏域Name
	    });
	    var ssdyname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属地域名称',
	    	name: 'areatypename'
	    });
	    
	    var sshy = Ext.create('system.widget.FsdTreeComboBox', {
	    	fieldLabel : '所属职务',
	    	labelAlign:'right',
	    	labelWidth:60,
	    	rootText : '职务类别',
	    	rootId : 'FSDJOB',
	        width : 340,
	        name : 'jobtypeid',
	        baseUrl:'A_CompanyType/load_AsyncCompanyTypeTree.do',//访问路劲
	        allowBlank: false,
	        hiddenName : 'jobtypename'//隐藏域Name
	    });
	    var sshyname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属行业ID',
	    	name: 'jobtypename'
	    });	    

	    var xb = new Ext.create('system.widget.FsdComboBoxZD',{
	    	fieldLabel: '性别',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'sex',
	        key : 'xb',//参数
    	    hiddenName : 'sexname'//提交隐藏域Name
	    });
	    var xbcode = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '性别名',
	    	name: 'sexname'
	    });
	    
	    var email = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '电子邮箱',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'email',
			vtype : 'email',
	    	maxLength: 100
	    });
	    
	    var beizhu = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'remark',
	    	maxLength: 50
	    });
	    
	    me.on('show' , function(){
	    	ssjg.setText(ssjgname.getValue());
	    	sslb.setText(sslbname.getValue());
	    	ssdy.setText(ssdyname.getValue());
	    	sshy.setText(sshyname.getValue());
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [{
                name: "id",
                xtype: "hidden"
            } , 
            sslbname, ssdyname, sshyname, ssjgname, xbcode, 
            bm , ssjg, dlm, xm, zjf, 
            sslb, ssdy, sshy, xb, email, beizhu]
        });
	        
	    Ext.apply(this,{
	        width:400,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.formSubmitSelf(form);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});