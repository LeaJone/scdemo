﻿/**
 * 人员添加界面
 * @author lumingbao
 */
Ext.define('system.abasis.employee.biz.UserEdit', {
	extend : 'Ext.window.Window',
	requires : ['system.widget.FsdTreeComboBox'],
	grid : null,
	treepanel : null,
	oper : Ext.create('system.abasis.employee.biz.UserOper'),
	initComponent:function(){
	    var me = this;
	    
	    var dwid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '单位编号',
	    	name: 'companyid',
	    	value : me.oper.util.getParams("company").id,
	    });
	    var dwmc = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '单位名称',
	    	name: 'companyname',
	    	value : me.oper.util.getParams("company").name,
	    });
	    
	    var loginUser = me.oper.util.getParams("user");
	   
	    var bm = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '学号/工号',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'code',
	    	maxLength: 20,
	    	allowBlank: false
	    });
	    
	    var yx = new system.widget.FsdTreeComboBox({
	    	fieldLabel : '院系',
	    	labelAlign:'right',
	    	labelWidth:80,
	    	rootText : me.oper.util.getParams("company").name,
	    	rootId : me.oper.util.getParams("company").id,
	    	rootVisible : false,//不显示根节点
	        width : 360,
	        name : 'branchid',
	        baseUrl:'A_Branch/load_AsyncBranchTreeByPopdom.do',//访问路劲
	        allowBlank: false,
	        hiddenName : 'branchname'
	    });
	    var yxname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属院系',
	    	name: 'branchname'
	    });
	   
	    var bj = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '班级',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'registerclass',
	    	maxLength: 50
	    });
	    
	    var xm = new Ext.create('system.widget.FsdTextFieldZJF',{
	    	fieldLabel: '姓名',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'realname',
	    	fname : 'symbol',
	    	maxLength: 25,
	    	allowBlank: false
	    });
	    
	    var imagelogo = Ext.create('system.widget.FsdTextFileManage', {
	        width : 360,
	    	zName : 'imagelogo',
	    	zFieldLabel : '头像',
	    	zLabelWidth : 80,
	    	zIsShowButton : true,
	    	zIsGetButton : false,
	    	zIsUpButton : true,
	    	zIsReadOnly : true,
	    	allowBlank: false,
	    	zContentObj : me.content,
	    	zFileUpPath : me.oper.util.getImagePath()
	    });
	    
	    var xb = new Ext.create('system.widget.FsdComboBoxZD',{
	    	fieldLabel: '性别',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'sex',
	        key : 'xb',//参数
    	    hiddenName : 'sexname'//提交隐藏域Name
	    });
	    var xbcode = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '性别名',
	    	name: 'sexname'
	    });
	    
	    var zjf = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '助记符',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'symbol',
	    	maxLength: 20
	    });
	    	    
	    var idcard = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '身份证号',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'idcard',
	    	maxLength: 18
	    });
	    
	    var sjhm = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '手机号码',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'mobile',
	    	maxLength: 11,
	    	minLength: 11
	    });
	    
	    var email = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '电子邮箱',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'email',
			vtype : 'email',
	    	maxLength: 100
	    });
	    
	    var yhlx = new Ext.create('system.widget.FsdComboBoxZD',{
	    	fieldLabel: '用户类型',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'usertypeid',
	        key : 'yhlx',//参数
    	    hiddenName : 'usertypename',//提交隐藏域Name
    	    allowBlank: false
	    });
	    var yhlxname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '用户类型',
	    	name: 'usertypename'
	    });
	    
	    var rylb = new Ext.create('system.widget.FsdComboBoxZD',{
	    	fieldLabel: '用户类别',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'type',
	        key : 'rylb',//参数
	        allowBlank: false,
    	    hiddenName : 'typename'//提交隐藏域Name
	    });
	    var rylbcode = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '用户类别名',
	    	name: 'typename'
	    });

		var departmentadmin = new Ext.create('system.widget.FsdComboBoxZD',{
			fieldLabel: '院系管理员',
			labelAlign:'right',
			labelWidth:80,
			width : 360,
			name: 'departmentadmin',
			key : 'sf',//参数
			allowBlank: false
		});

	    var ryzt = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'人员状态',
	        labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
    	    name : 'status',//提交到后台的参数名
	        key : 'ryzt',//参数
	        allowBlank: false,
    	    hiddenName : 'statusname'//提交隐藏域Name
	    });
	    var ryztcode = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '人员状态名',
	    	name: 'statusname'
	    });
	    
	    var beizhu = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'remark',
	    	maxLength: 50
	    });

	    me.on('show' , function(){
	    	yx.setText(yxname.getValue());
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [{
                name: "id",
                xtype: "hidden"
            } , 
            dwid , dwmc, bm, yx, yxname, bj, imagelogo, xm, zjf, xb, xbcode, idcard, sjhm, email, yhlx, yhlxname,
            rylb, rylbcode , departmentadmin, ryzt, ryztcode, beizhu]
        });
	        
	    Ext.apply(this,{
	        width:450,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.formSubmit(form, me.grid, me.treepanel);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});