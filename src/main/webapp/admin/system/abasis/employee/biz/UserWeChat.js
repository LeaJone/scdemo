﻿/**
 * 微信二维码界面
 * @author lw
 */
Ext.define('system.abasis.employee.biz.UserWeChat', {
	extend : 'Ext.window.Window',
	grid:null,
	oper : Ext.create('system.abasis.employee.biz.UserOper'),
	initComponent:function(){
	    var me = this;
	    
	    me.logoImg = Ext.create('Ext.panel.Panel', {
    	    width: 340,
    	    height: 360,
    	    margin: 0,
    	    layout: 'fit',
    	    html: ""
    	});
	    
	    Ext.apply(this,{
	        width:352,
	        height:405,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[me.logoImg],
	        buttons : [{
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    }),
	    
	    me.on("close", function(panel, opts){
	    	if(me.grid != null){
	    		me.grid.getStore().reload();
	    	}
	    });
	    this.callParent(arguments);
	},
	
	setImageUrl:function(url){
		var me = this;
		me.logoImg.update("<img width='340' height='340' src='"+ url +"' />");
	}
});