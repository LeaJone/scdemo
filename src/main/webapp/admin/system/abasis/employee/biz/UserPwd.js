﻿/**
 * 角色添加界面
 * @author lumingbao
 */
Ext.define('system.abasis.employee.biz.UserPwd', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.abasis.employee.biz.UserOper'),
	initComponent:function(){
	    var me = this;

	    var yhbh = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '用户编号',
	    	name: 'userid',
	    	value : me.oper.util.getParams("user").id,
	    });
	   
	    var jmm = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '旧密码',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'oldpwd',	   
			inputType : 'password',
	    	maxLength: 20,
	    	allowBlank: false
	    });
	   
	    var xmm1 = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '新密码',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'newpwd1',
			inputType : 'password',
	    	maxLength: 20,
	    	regex: /^(?!\d+$)(?![A-Za-z]+$)[a-zA-Z0-9]{8,}$/i,
	    	regexText: '密码必须同时包含字母和数字,且最少有8位',
	    	allowBlank: false
	    });
	   
	    var xmm2 = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '确认密码',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'newpwd2',
			vtype : 'password',
			vtypeField : xmm1,
			inputType : 'password',
	    	maxLength: 20,
	    	allowBlank: false
	    });

		var tipLabel = Ext.create('Ext.form.Label',{
	    	text: "注：密码必须同时包含字母和数字,且最少有8位",
			padding : '20 20 0 0',
	    	style:{color:'red'}
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{  
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [yhbh, jmm, xmm1, xmm2, tipLabel]
        });
	        
	    Ext.apply(this,{
	        width:400,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.pwdEditFormSubmit(form);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});