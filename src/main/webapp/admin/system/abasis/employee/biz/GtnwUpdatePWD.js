﻿/**
 * 文章类型变更
 * @author lw
 */
Ext.define('system.abasis.employee.biz.GtnwUpdatePWD', {
	extend : 'Ext.window.Window',
	updateFun : null,
    title : '用户密码修改',

	initComponent:function(){
	    var me = this;
	    
	    var pwd = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '加密密码',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 350,
	    	name: 'pwd',
	    	maxLength: 50,
	    	allowBlank: false
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [pwd]
        });
	        
	    Ext.apply(this,{
	        width:410,
	        height:130,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '更改',
			    iconCls : 'acceptIcon',
			    handler : function() {
			    	if (form.form.isValid()) {
	    				if (me.updateFun != null){
	    					me.updateFun(pwd.getValue());
	    				    me.close();
	    				}
			    	}
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});