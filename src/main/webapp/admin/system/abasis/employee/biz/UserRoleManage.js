﻿/**
 * 人员授权管理界面
 * @author lw
 */

Ext.define('system.abasis.employee.biz.UserRoleManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*', 
	             'system.abasis.employee.model.Extjs_A_employee', 
	             'system.abasis.popedomgroup.model.Extjs_Sys_popedomgroup'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.abasis.employee.biz.UserOper'),
	
	createContent : function(){
		var me = this;
		var queryid = me.oper.util.getParams("company").id;

		/**
		 * 左边的树面板
		 */
		var treepanel = Ext.create("system.widget.FsdTreePanel", {
			icon : 'resources/admin/icon/chart_organisation.png',
			baseUrl : 'A_Branch/load_AsyncBranchTreeByPopdom.do',
			title: '组织机构',
			rootText : me.oper.util.getParams("company").name,
			rootId : me.oper.util.getParams("company").id,
			region : "west", // 设置方位
			collapsible : true,//折叠
			width: 250,
    		minWidth : 200,
			split: true
		});
		
		treepanel.on('itemclick', function(view,record,item,index,e){
			queryid = record.raw.id;
			usergrid.getStore().reload();
		});
		
		// 创建表格
		var rolegrid = Ext.create("system.widget.FsdGridPanel", {
			column : 'column_sys_popedomuser', // 显示列
			model : 'model_sys_popedomgroup',
			baseUrl : 'Sys_PopedomGroup/load_datauser.do',
			border : false
		});
		rolegrid.selModel.on('selectionchange', function(obj, selected, eOpts){
			var ids = new Array();
			if (selected.length > 0){
				Ext.Array.each(selected, function(item) {
	                ids.push(item.data.id);
	            });
			}
			me.oper.GetGroupPopedom(me, ids, treepopedom, treesubject, treesubjectwap,treebranch);
		});
		
		var rolepanel = Ext.create("Ext.panel.Panel", {
			title: "角色信息",
			region : "center", // 设置方位
			split: true,
			width: 300,
			layout: 'fit',
			items:[rolegrid],
			tbar : [{
				xtype : "button",
				text : "保存",
				iconCls : 'acceptIcon',
				handler : function() {
					me.oper.SaveUserRole(me, usergrid, rolegrid);
				}
        	}
			]
		});
		
		// 创建表格
		var usergrid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_a_employee_role', // 显示列
			model: 'model_a_employee',
			baseUrl : 'A_Employee/load_data1.do',
			border : false,
			tbar : [new Ext.form.TextField( {
				id : 'queryEmplRoleName',
				name : 'queryParam',
				emptyText : '请输入人员名称',
				enableKeyEvents : true,
				listeners : {
					specialkey : function(field, e) {
						if (e.getKey() == Ext.EventObject.ENTER) {
							usergrid.getStore().reload();
						}
					}
				},
				width : 130
			}), {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					usergrid.getStore().reload();
				}
			}]
		});
		usergrid.getStore().on('beforeload', function(s) {
			var name =  Ext.getCmp('queryEmplRoleName').getValue();
			var pram = {fid : queryid , name : name};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		usergrid.selModel.on('selectionchange', function(obj, selected, eOpts){
			var ids = new Array();
			if (selected.length > 0){
				Ext.Array.each(selected, function(item) {
                    ids.push(item.data.id);
	            });
			}
			me.oper.GetUserPopedom(me, ids, rolegrid, treepopedom, treesubject, treesubjectwap,treebranch);
		});
		
		var userpanel = Ext.create("Ext.panel.Panel", {
			title: "人员信息",
			region : "west", // 设置方位
			split: true,
			width: 700,
			layout: 'fit',
			items:[usergrid]
		});
		
		/**
		 *  中间的面板
		 */
		var contpanel = new Ext.panel.Panel({
			header : false,
			region : "center", // 设置方位
			border : 0,
			layout: 'border',
			items: [userpanel,rolepanel]
		});
		
		/**
		 *  菜单权限面板
		 */
		var treepopedom = Ext.create("Ext.tree.Panel", {
    		width: 300,
    		minWidth : 150,
            rootVisible : false,//不显示根节点
            tbar : [{
					xtype : "button",
					text : "展开",
					iconCls : 'expand-all',
					handler : function() {
						treepopedom.expandAll();
					}
				},{
					xtype : "button",
					text : "收缩",
					iconCls : 'collapse-all',
					handler : function() {
						treepopedom.collapseAll();
					}
				}]
    	});
		/**
		 *  栏目权限面板
		 */
		var treesubject = Ext.create("system.widget.FsdTreePanel", {
    		width: 300,
    		minWidth : 150,
            rootVisible : false,//不显示根节点
            baseUrl :'B_Subject/get_QxSubjectCheck.do',
	        zTreeRoot : 'children',//一次性加载
            zTreeParams : {id : 'FSDMAIN'},
            tbar : [{
					xtype : "button",
					text : "展开",
					iconCls : 'expand-all',
					handler : function() {
						treesubject.expandAll();
					}
				},{
					xtype : "button",
					text : "收缩",
					iconCls : 'collapse-all',
					handler : function() {
						treesubject.collapseAll();
					}
				}]
    	});
		/**
		 *  Wap栏目权限面板
		 */
		var treesubjectwap = null;
//		var treesubjectwap = Ext.create("Ext.tree.Panel", {
//    		width: 300,
//    		minWidth : 150,
//            rootVisible : false,//不显示根节点
//            baseUrl :'B_Subject/get_QxSubjectCheck.do',
//	        zTreeRoot : 'children',//一次性加载
//            zTreeParams : {id : 'FSDMOBILE'},
//            tbar : [{
//					xtype : "button",
//					text : "展开",
//					iconCls : 'expand-all',
//					handler : function() {
//						treesubjectwap.expandAll();
//					}
//				},{
//					xtype : "button",
//					text : "收缩",
//					iconCls : 'collapse-all',
//					handler : function() {
//						treesubjectwap.collapseAll();
//					}
//				}]
//    	});
		/**
		 *  机构权限面板
		 */
		var treebranch = Ext.create("Ext.tree.Panel", {
    		width: 300,
    		minWidth : 150,
            rootVisible : false,//不显示根节点
            tbar : [{
					xtype : "button",
					text : "展开",
					iconCls : 'expand-all',
					handler : function() {
						treebranch.expandAll();
					}
				},{
					xtype : "button",
					text : "收缩",
					iconCls : 'collapse-all',
					handler : function() {
						treebranch.collapseAll();
					}
				}]
    	});
		
		
		 /**
	     * 右边的大选项卡控件
	     */
	    var qxTabPanel = new Ext.tab.Panel({
	    	title : "权限信息",
			width: 346,
			height: 300,
			activeTab: 0,
			region: 'east',
			split: true,//拖动
    		collapsible : true, //折叠
			collapseDirection: 'right',//折叠方向
			deferredRender: false,//是否在显示每个标签的时候再渲染标签中的内容.默认true
			items: [
				{
					xtype: 'panel',
					layout: 'fit',
					title: '菜单权限',
					items: [
					        	treepopedom
					]},{
						xtype: 'panel',
						layout: 'fit',
						title: '栏目权限',
						items: [
						        treesubject  
					]},
//					{
//						xtype: 'panel',
//						layout: 'fit',
//						title: 'WAP栏目权限',
//						items: [
//						        treesubjectwap
//					]},
					{
						xtype: 'panel',
						layout: 'fit',
						title: '部门权限',
						items: [
						        treebranch
					]}
			]
		});

	    me.on('afterrender', function(obj, eOpts){
			me.oper.LoadMenuTree(treepopedom);
			me.oper.LoadBranchTree(treebranch);
	    });

		var page1_jExtPanel1_obj = new Ext.panel.Panel({
			bodyPadding: 3,
			header : false,
			border : 0,
			split: true,
			layout: 'border',
			items: [treepanel,contpanel,qxTabPanel]
		});
		return page1_jExtPanel1_obj;
	}
});