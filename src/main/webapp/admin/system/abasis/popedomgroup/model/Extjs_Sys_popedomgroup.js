﻿/**
 *sys_popedomgroup Model
 *@author CodeSystem
 *文件名     Extjs_Sys_popedomgroup
 *Model名    model_sys_popedomgroup
 *Columns名  column_sys_popedomgroup
 */

Ext.define('model_sys_popedomgroup', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'name',
            type : 'string'
        }
        ,
        {
            name : 'remark',
            type : 'string'
        }
        ,
        {
            name : 'sort',
            type : 'string'
        }
        ,
        {
            name : 'symbol',
            type : 'string'
        }
    ]
});


/**
 *sys_popedomgroup Columns
 *@author CodeSystem
 */
Ext.define('column_sys_popedomgroup', {
    columns: [
        {
            header : '权限组名',
            dataIndex : 'name'
        }
        ,
        {
            header : '助记符',
            dataIndex : 'symbol'
        }
        ,
        {
            header : '序号',
            dataIndex : 'sort'
        }
        ,
        {
            header : '备注',
            dataIndex : 'remark'
        }
    ]
});

/**
 *sys_popedomgroup Columns
 *@author CodeSystem
 */
Ext.define('column_sys_popedomuser', {
    columns: [
        {
            header : '权限组名',
            dataIndex : 'name',
            flex : 3
        }
    ]
});