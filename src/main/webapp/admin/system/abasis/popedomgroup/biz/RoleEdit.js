﻿/**
 * 角色添加界面
 * @author lumingbao
 */
Ext.define('system.abasis.popedomgroup.biz.RoleEdit', {
	extend : 'Ext.window.Window',
	requires : ['system.widget.FsdTreeComboBox'],
	grid : null,
	oper : Ext.create('system.abasis.popedomgroup.biz.RoleOper'),
	initComponent:function(){
	    var me = this;

	    var dwid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '单位编号',
	    	name: 'companyid',
	    	value : me.oper.util.getParams("company").id,
	    });

		var jsbm = new Ext.create('system.widget.FsdTextField',{
			fieldLabel: '角色编码',
			labelAlign:'right',
			labelWidth:60,
			width : 340,
			name: 'code',
			maxLength: 20,
			allowBlank: false
		});
	    
	    var qxzm = new Ext.create('system.widget.FsdTextFieldZJF',{
	    	fieldLabel: '权限组名',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'name',
	    	fname : 'symbol',
	    	maxLength: 20,
	    	allowBlank: false,
	    	blankText: '请输入权限组名',
	    	maxLengthText: '权限组名不能超过20个字符'
	    });
	   
	    var zjf = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '助记符',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'symbol',	    	
	    	maxLength: 20,
	    	allowBlank: false,
	    	blankText: '请输入助记符',
	    	maxLengthText: '助记符不能超过20个字符'
	    });

	    var xh = new Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '序号',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'sort',
	    	maxLength: 3,
	    	minValue: 0,
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank: false,
	    	blankText: '请输入序号',
	    	maxLengthText: '序号不能超过3个字符'
	    });
	    
	    var beizhu = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'remark',
	    	maxLength: 50,
	    	maxLengthText: '备注不能超过50个字符'
	    });
	        
	    Ext.apply(this,{
	        width:400,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[{
				xtype : 'form',
				id : 'RoleAddForm',
				defaultType : 'textfield',
				header : false,// 是否显示标题栏
				border : 0,
				padding : '20 0 0 20',
				items : [{
                    name: "id",
                    xtype: "hidden"
                } , dwid, jsbm, qxzm , zjf , xh , beizhu]
	        }],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'jsbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.formSubmit(Ext.getCmp('RoleAddForm') , me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});