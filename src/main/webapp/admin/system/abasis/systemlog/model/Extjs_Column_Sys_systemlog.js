﻿/**
 *sys_systemlog Column
 *@author CodeSystem
 *文件名     Extjs_Column_Sys_systemlog
 *Columns名  column_sys_systemlog
 */

Ext.define('column_sys_systemlog', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            width : 40
        }
        ,
        {
            header : '操作时间',
            dataIndex : 'logtime',
            sortable : true,
            flex : 3,
			renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value, true);
			}
        }
        ,
        {
            header : '操作人',
            dataIndex : 'employeename',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '操作类型',
            dataIndex : 'logtypename',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '描述',
            dataIndex : 'logdepict',
            sortable : true,
            flex : 6
        }
    ]
});
