﻿/**
 *sys_systemlog Model
 *@author CodeSystem
 *文件名     Extjs_Model_Sys_systemlog
 *Model名    model_sys_systemlog
 */

Ext.define('model_sys_systemlog', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'logtime',
            type : 'string'
        }
        ,
        {
            name : 'employeeid',
            type : 'string'
        }
        ,
        {
            name : 'employeename',
            type : 'string'
        }
        ,
        {
            name : 'logtype',
            type : 'string'
        }
        ,
        {
            name : 'logtypename',
            type : 'string'
        }
        ,
        {
            name : 'logdepict',
            type : 'string'
        }
    ]
});
