/**
 * 系统日志管理界面操作类
 * @author lw
 */
Ext.define('system.abasis.systemlog.biz.SystemLogOper', {

	form : null,
	util : Ext.create('system.util.util'),

	
	/**
     * 高级查询
     */
	query : function(winForm, grid){
	    var win = Ext.create('system.abasis.systemlog.biz.SystemLogQuery');
        win.setTitle('系统日志查询');
        win.form = winForm;
        win.grid = grid;
        win.modal = true;
        win.show();
	},
	
 	/**
     * 查看
     */
	view : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if(data.length > 0){
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.abasis.systemlog.biz.SystemLogEdit');
            win.setTitle('系统日志查看');
            win.modal = true;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'Sys_SystemLog/load_SystemLogByid.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	}
});