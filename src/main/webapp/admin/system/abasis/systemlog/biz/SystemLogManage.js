﻿/**
 * 通知管理界面
 * @author lw
 */
Ext.define('system.abasis.systemlog.biz.SystemLogManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*', 
	             'system.widget.FsdButton',
	             'system.abasis.systemlog.model.Extjs_Column_Sys_systemlog', 
	             'system.abasis.systemlog.model.Extjs_Model_Sys_systemlog'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.abasis.systemlog.biz.SystemLogOper'),
	queryObj : null,
    
	createTable : function() {
	    var me = this;

		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_sys_systemlog', // 显示列
			model : 'model_sys_systemlog',
			baseUrl : 'Sys_SystemLog/load_pagedatacz.do',
			border : false,
			tbar : [{
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid);
				}
			}, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.query(me, grid);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]
		});
		grid.getStore().on('beforeload', function(s) {
			if (me.queryObj == null){
				me.queryObj = {};
			}
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(me.queryObj)});
		});

		return grid;
	}
});