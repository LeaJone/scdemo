﻿/**
 * 机构添加界面
 * @author lumingbao
 */
Ext.define('system.abasis.branch.biz.BranchEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	treepanel : null,
	oper : Ext.create('system.abasis.branch.biz.BranchOper'),
	initComponent:function(){
	    var me = this;
	    
	    var ssbmname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属机构名称',
	    	name: 'parentname'
	    });
	    
	    var ssbm = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属机构',
	    	labelAlign:'right',
	    	labelWidth:80,
	    	rootText : me.oper.util.getParams("company").name,
	    	rootId : me.oper.util.getParams("company").id,
	        width : 340,
	        name : 'parentid',
	        baseUrl:'A_Branch/load_AsyncBranchTreeQuery.do',//访问路劲	        
	        allowBlank: false,
	        hiddenName : 'parentname'//隐藏域Name
	    });
	    
	   
	    var bmbm = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '机构编码',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'code',
	    	maxLength: 20,
	    	allowBlank: false
	    });
	    
	    var bmmc = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '机构名称',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'name',
	    	maxLength: 20,
	    	allowBlank: false
	    });
	    
	    var pxbh = Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '排序编号',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'sort',
	    	maxLength: 3,
	    	minValue: 0,
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank: false
	    });
	    
	    var beizhu = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'remark',
	    	maxLength: 50,
	    	maxLengthText: '备注不能超过50个字符'
	    });
	    
	    me.on('show' , function(){
	    	ssbm.setText(ssbmname.getValue());
	    });
	    
	    Ext.apply(this,{
	        width:420,
	        height:270,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[{
				xtype : 'form',
				id : 'BranchAddForm',
				defaultType : 'textfield',
				header : false,// 是否显示标题栏
				border : 0,
				padding : '20 0 0 20',
				items : [{
                    name: "id",
                    xtype: "hidden"
                } , ssbmname , ssbm , bmbm , bmmc , pxbh , beizhu]
	        }],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'jgbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.formSubmit(Ext.getCmp('BranchAddForm') , me.grid , me.treepanel);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});