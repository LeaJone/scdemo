/**
 * 机构添加界面
 * @author lw
 */
Ext.define('system.abasis.systemlogread.biz.SystemLogReadQuery', {
	extend : 'Ext.window.Window',
	grid : null,
	form : null,
	oper : Ext.create('system.abasis.systemlogread.biz.SystemLogReadOper'),
	initComponent:function(){
	    var me = this;
	    
	    var employeename = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '操作人',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'employeename',
	    	maxLength: 50,
	    	maxLengthText: '标题不能超过50个字符'
	    });
	    
	    var logtype = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'日志类型',
	        labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
    	    name : 'logtype',//提交到后台的参数名
	        key : 'xtrzlx',
	        zNotCodes : ['xtrzbc', 'xtrzsc', 'xtrzqt']
	    });
	    
	    var stime = Ext.create('system.widget.FsdDateTime',{
	    	fieldLabel: '起始时间',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'stime'
	    });
	    
	    var etime = Ext.create('system.widget.FsdDateTime',{
	    	fieldLabel: '结束时间',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'etime'
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [employeename, logtype, stime, etime]
        });
	        
	    Ext.apply(this,{
	        width:410,
	        height:210,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        items :[form],
	        buttons : [{
			    text : '查询',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				if (me.form != null && me.grid != null){
    					me.form.queryObj = form.getForm().getValues();
    					me.grid.getStore().reload();
    				    me.close();
    				}
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});