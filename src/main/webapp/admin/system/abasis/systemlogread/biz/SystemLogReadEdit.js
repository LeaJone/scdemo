Ext.define('system.abasis.systemlogread.biz.SystemLogReadEdit',{
	extend : 'Ext.window.Window',
	requires : ['system.widget.FsdTreeComboBox'],
	grid : null,
	oper : Ext.create('system.abasis.systemlogread.biz.SystemLogReadOper'),
	initComponent:function(){
	    var me = this;
	    
	    var logtime = Ext.create('system.widget.FsdDateTime',{
	    	fieldLabel: '操作时间',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'logtime'
	    });
	    
	    var employeename = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '操作人',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'employeename'
	    });
	    
	    var logtypename = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '日志类型',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'logtypename'
	    });
	    
	    var logdepict = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '操作描述',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'logdepict'
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			header : false,// 是否显示标题栏
			border : false,
			padding : '20 0 0 20',
			items : [logtime, employeename, logtypename, logdepict]
        });
	        
	    Ext.apply(this,{
	        width:400,
	        height:270,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});