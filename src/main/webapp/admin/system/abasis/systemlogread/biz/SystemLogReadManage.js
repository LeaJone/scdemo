Ext.define('system.abasis.systemlogread.biz.SystemLogReadManage',{
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*', 
	             'system.widget.FsdButton',
	             'system.abasis.systemlogread.model.Extjs_Column_Sys_systemlogread', 
	             'system.abasis.systemlogread.model.Extjs_Model_Sys_systemlogread'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
	oper : Ext.create('system.abasis.systemlogread.biz.SystemLogReadOper'),
	queryObj : null,
	
	createTable : function() {
	    var me = this;

		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_sys_systemlogread', // 显示列
			model : 'model_sys_systemlogread',
			baseUrl : 'Sys_SystemLog/load_pagedataaq.do',
			border : false,
			tbar : [{
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid);
				}
			}, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.query(me, grid);
				}
			}, '-', {
                text : "导出Excel",
                iconCls : 'articlewj',
                handler : function() {
                    me.oper.download(me.queryObj);
                }
            }, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]
		});
		grid.getStore().on('beforeload', function(s) {
			if (me.queryObj == null){
				me.queryObj = {};
			}
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(me.queryObj)});
		});

		return grid;
	}
});