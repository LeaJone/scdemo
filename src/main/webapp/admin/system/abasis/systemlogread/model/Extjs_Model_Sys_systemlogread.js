Ext.define('model_sys_systemlogread',{
	extend:'Ext.data.Model',
	fields:[
		{
			name : 'id',
            type : 'string'
		}
		,
		{
			name : 'logtime',
            type : 'string'
		}
		,
		{
			name : 'employeeid',
            type : 'string'
		}
		,
		{
			name : 'employeename',
            type : 'string'
		}
		,
		{
			name : 'logtype',
            type : 'string'
		}
		,
		{
			name : 'logtypename',
            type : 'string'
		}
		,
		{
			name : 'logdepict',
            type : 'string'
		}
	]
});