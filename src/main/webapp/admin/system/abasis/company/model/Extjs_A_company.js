﻿/**
 *a_company Model
 *@author CodeSystem
 *文件名     Extjs_A_company
 *Model名    model_a_company
 *Columns名  column_a_company
 */

Ext.define('model_a_company', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'areatypeid',
            type : 'string'
        }
        ,
        {
            name : 'areatypename',
            type : 'string'
        }
        ,
        {
            name : 'code',
            type : 'string'
        }
        ,
        {
            name : 'defaulttypeid',
            type : 'string'
        }
        ,
        {
            name : 'defaulttypename',
            type : 'string'
        }
        ,
        {
            name : 'name',
            type : 'string'
        }
        ,
        {
            name : 'parentid',
            type : 'string'
        }
        ,
        {
            name : 'parentname',
            type : 'string'
        }
        ,
        {
            name : 'professiontypeid',
            type : 'string'
        }
        ,
        {
            name : 'professiontypename',
            type : 'string'
        }
        ,
        {
            name : 'remark',
            type : 'string'
        }
        ,
        {
            name : 'status',
            type : 'string'
        }
        ,
        {
            name : 'statusname',
            type : 'string'
        }
    ]
});


/**
 *a_company Columns
 *@author CodeSystem
 */
Ext.define('column_a_company', {
    columns: [
        {
            header : '编号',
            dataIndex : 'code',
            width : 30
        }
        ,
        {
            header : '站点名称',
            dataIndex : 'name'
        }
        ,
        {
            header : '状态',
            dataIndex : 'statusname'
        }
        ,
        {
            header : '备注',
            dataIndex : 'remark'
        }
        ,
        {
            header : '操作',
            dataIndex : 'logdepict',
            sortable : true,
            align : 'center',
            width : 50,
            renderer :function(value, metadata, record, rowIndex, columnIndex, store){
                return "<a target='_blank' href='../"+ store.getAt(rowIndex).get('code') +"/index.htm'>进入站点</a>";
            }
        }
    ]
});
