﻿Ext.define('column_template', {
    columns: [
          {
              xtype : 'rownumberer',
              text : 'NO',
              sortable : true,
              width : 40
          }
          ,
          {
              header : '名称',
              dataIndex : 'name',
              sortable : true,
              width : '35%',
          }
          ,
          {
              header : '编码',
              dataIndex : 'code',
              sortable : true,
              width : '65%'
          }
        ,
        {
            header : '预览',
            dataIndex : 'logdepict',
            sortable : true,
            align : 'center',
            renderer :function(value, metadata, record, rowIndex, columnIndex, store){
                return "<a target='_blank' href='../template/"+ store.getAt(rowIndex).get('code') +"/index.htm'>预览</a>";
            }
        }
    ]
});
