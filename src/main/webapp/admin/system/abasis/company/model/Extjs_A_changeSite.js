﻿/**
 *changeSite Model
 *@author CodeSystem
 *文件名     Extjs_A_changeSite
 *Model名    model_a_changeSite
 *Columns名  column_a_changeSite
 */

Ext.define('model_a_changeSite', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'name',
            type : 'string'
        }
    ]
});


/**
 *changeSite Columns
 *@author CodeSystem
 */
Ext.define('column_a_changeSite', {
    columns: [
        {
            header : '编号',
            dataIndex : 'id',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '单位名称',
            dataIndex : 'name',
            sortable : true,
            flex : 2
        }
    ]
});
