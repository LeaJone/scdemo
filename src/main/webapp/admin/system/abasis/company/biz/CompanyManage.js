﻿/**
 * 单位管理界面
 * @author lumingbao
 */
Ext.define('system.abasis.company.biz.CompanyManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*', 'system.abasis.company.model.Extjs_A_company'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.abasis.company.biz.CompanyOper'),
    
	createTable : function() {
	    var me = this;

		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPanel", {
			column : 'column_a_company', // 显示列
			model : 'model_a_company',
			baseUrl : 'A_Company/load_pagedata.do',
			tbar : [{
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
				text : "添加",
				iconCls : 'page_addIcon',
				handler : function() {
					me.oper.add(grid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
				text : "修改",
				iconCls : 'page_editIcon',
				handler : function() {
					me.oper.editor(grid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'dwsc',
				text : "删除",
				iconCls : 'page_deleteIcon',
				handler : function() {
				    me.oper.del(grid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid);
				}
			}, '-', {
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
				text : "启用",
				iconCls : 'page_deleteIcon',
				handler : function(button) {
				    me.oper.isEnabled(grid, true);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
				text : "停用",
				iconCls : 'page_deleteIcon',
				handler : function(button) {
				    me.oper.isEnabled(grid, false);
				}
			}, '->', new Ext.form.TextField( {
				id : 'queryCompanyName',
				name : 'queryParam',
				emptyText : '请输入角色名称',
				enableKeyEvents : true,
				listeners : {
					specialkey : function(field, e) {
						if (e.getKey() == Ext.EventObject.ENTER) {
							grid.getStore().reload();
						}
					}
				},
				width : 130
			}), {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  Ext.getCmp('queryCompanyName').getValue();
			var pram = {name : name};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});

		return grid;
	}
});