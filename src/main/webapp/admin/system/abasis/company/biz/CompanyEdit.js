﻿/**
 * 单位编辑界面
 * @author lumingbao
 */
Ext.define('system.abasis.company.biz.CompanyEdit', {
	extend : 'Ext.window.Window',
	requires : ['system.widget.FsdTreeComboBox', 'system.abasis.company.model.Extjs_Column_template', 'system.abasis.company.model.Extjs_Model_template'],
	grid : null,
	oper : Ext.create('system.abasis.company.biz.CompanyOper'),
	initComponent:function(){
	    var me = this;	    
	   
	    var dwbm = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '站点编码',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'code',
	    	maxLength: 20,
	    	allowBlank: false,
	    	blankText: '请输入站点编码',
	    	maxLengthText: '站点编码不能超过20个字符'
	    });
	    
	    var dwmc = new Ext.create('system.widget.FsdTextFieldZJF',{
	    	fieldLabel: '站点名称',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'name',
	    	fname : 'symbol',
	    	maxLength: 25,
	    	allowBlank: false,
	    	blankText: '请输入单位站点',
	    	maxLengthText: '站点名称不能超过25个字符'
	    });
	    
	    var zjf = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '助记符',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'symbol',
	    	maxLength: 20,
	    	allowBlank: false,
	    	blankText: '请输入助记符',
	    	maxLengthText: '助记符不能超过20个字符'
	    });

        me.templatename = Ext.create('system.widget.FsdTextGrid', {
            zFieldLabel : '站点模板',
            labelWidth:60,
            width : 340,
            zName : 'templatename',
            zColumn : 'column_template', // 显示列
            zModel : 'model_template',
            zBaseUrl : 'a_template/load_pagedata.do',
            zAllowBlank : false,
            zGridType : 'list',
            zGridWidth : 600,//弹出表格宽度
            zGridHeight : 500,//弹出表格高度
            zIsText1 : true,
            zTxtLabel1 : '站点模板',
            zFunChoose : function(data){
                me.templateid.setValue(data.id);
                me.templatename.setValue(data.name);
            }
        });
        me.templateid = Ext.create('Ext.form.field.Hidden',{
            fieldLabel: '站点模板编号',
            name: 'templateid'
        });

	    var dwzt = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'站点状态',
	        labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
    	    name : 'status',//提交到后台的参数名
	        key : 'dwzt',//参数
    	    hiddenName : 'statusname'//提交隐藏域Name
	    });
	    var dwztname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '站点状态名',
	    	name: 'statusname'
	    });
	    
	    var beizhu = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'remark',
	    	maxLength: 50,
	    	maxLengthText: '备注不能超过50个字符'
	    });
	        
	    Ext.apply(this,{
	        width:400,
	        height:260,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[{
				xtype : 'form',
				id : 'CompanyAddForm',
				defaultType : 'textfield',
				header : false,// 是否显示标题栏
				border : 0,
				padding : '20 0 0 20',
				items : [{
		                    name: "id",
		                    xtype: "hidden"
		                } , 
		                dwbm , dwmc , zjf , dwzt, me.templatename, me.templateid, beizhu , dwztname]
	        }],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.formSubmit(Ext.getCmp('CompanyAddForm') , me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});