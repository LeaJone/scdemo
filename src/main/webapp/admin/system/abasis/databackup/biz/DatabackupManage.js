﻿/**
 * 数据备份管理界面
 * @author Administrator
 */
Ext.define('system.abasis.databackup.biz.DatabackupManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*', 
	             'system.widget.FsdButton',
	             'system.abasis.databackup.model.Extjs_Column_Sys_databackup', 
	             'system.abasis.databackup.model.Extjs_Model_Sys_databackup'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.abasis.databackup.biz.DatabackupOper'),
    
	createTable : function() {
	    var me = this;

		/**
		 * 查询文本框
		 */
		var queryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入文件名称',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});

		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_sys_databackup', // 显示列
			model : 'model_sys_databackup',
			baseUrl : 'sys_databackup/load_pagedata.do',
			border : false,
			tbar : [{
				xtype : "fsdbutton",
				text : "备份数据",
				iconCls : 'page_addIcon',
				popedomCode : 'wdbj',
				handler : function() {
					me.oper.add(grid);
				}
			}, {
				text : '查看文件',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid);
				}
			}, {
				xtype : "fsdbutton",
				text : "删除文件",
				iconCls : 'page_deleteIcon',
				popedomCode : 'wdsc',
				handler : function() {
				    me.oper.del(grid);
				}
			}, {
				text : '恢复数据(慎用)',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					me.oper.recovery(grid);
				}
			}, '->', queryText, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			},]			
		});
		
		grid.getStore().on('beforeload', function(s) {
			var name =  queryText.getValue();
			var pram = {name : name};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});

		grid.on('cellclick', function(thisObj, td, cellIndex, record, tr, rowIndex, e, eOpts) {
			if (cellIndex == 6){
				me.oper.download(grid);
				me.oper.preview(record.get('filename'));
			}
		});
		return grid;
	}
});