﻿/**
 * 数据备份查看界面
 * @author Administrator
 */

Ext.define('system.abasis.databackup.biz.DatabackupEdit', {
	extend : 'Ext.window.Window',
	requires : ['system.widget.FsdTreeComboBox'],
	grid : null,
	oper : Ext.create('system.abasis.databackup.biz.DatabackupOper'),
	initComponent : function() {
		var me = this;
		
		var backupdate = new Ext.create('system.widget.FsdDateTime', {
	    	fieldLabel : '备份时间',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 360,
	    	name : 'backupdate'
	    });
		
		var backupemployeename = new Ext.create('system.widget.FsdTextField', {
			fieldLabel : '备份用户',
		    labelAlign : 'right',
		    labelWidth : 80,
		    width : 360,
		    name : 'backupemployeename'
		});
		
		var filepath = new Ext.create('system.widget.FsdTextField', {
			fieldLabel : '保存路径',
		    labelAlign : 'right',
		    labelWidth : 80,
		    width : 360,
		    name : 'filepath'
		});
		
		var filename = new Ext.create('system.widget.FsdTextField', {
			fieldLabel : '文件名称',
		    labelAlign : 'right',
		    labelWidth : 80,
		    width : 360,
		    name : 'filename'
		});
		
		var filesize = Ext.create('system.widget.FsdTextField', {
			fieldLabel : '文件大小',
		    labelAlign : 'right',
		    labelWidth : 80,
		    width : 360,
		    name : 'filesize'
		});
		
		var form = Ext.create('Ext.form.Panel', {
			xtype : 'form',
			defaultType : 'textfield',
			header : false,
			border : 0,
			padding : '20 0 0 20',
			items : [{
                name : "id",
                xtype : "hidden"
            }, backupdate, backupemployeename, filepath, filename, filesize]
        });
	        
	    Ext.apply(this, {
	        width : 440,
	        height : 260,
	        collapsible : true,
	        bodyStyle : "background-color : #ffffff;",
	        resizable : false,
	        items : [form],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'wxzhbj',
	        	name : 'btnsave',
			    text : '　保存　',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.formSubmit(form, me.grid, true);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});