/**
 * 数据备份管理界面操作类
 * @author Administrator
 */

Ext.define('system.abasis.databackup.biz.DatabackupOper', {
	
	form : null,
	util : Ext.create('system.util.util'),
	
 	/**
     * 查看文件
     */
	view : function(grid){
		var me = this;
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要查看的数据！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能查看一条数据！');
        }else {  
	        // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.abasis.databackup.biz.DatabackupEdit');
            win.setTitle('数据详情查看');
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm(), 'sys_databackup/view_databackup.do', param, null, 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},
	
	/**
     * 删除文件
     */
	del : function(grid){
		var me = this;
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要删除的数据！');
        }else if (data.length > 1){
        	Ext.MessageBox.alert('提示', '每次只能删除一条数据');
        }else {
        	Ext.MessageBox.show({
        		title : '询问',
        		msg : '您确定要删除本数据吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                	if (btn == 'yes') {
                		var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
            			});
                        var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('sys_databackup/del_databackup.do', param,
                        function(response, options){
	                       	grid.getStore().reload();
                        }, null, me.form);
                    }
                }
            });
        }
	},
	
	/**
	 * 备份文件 
	 */
	add : function(grid){
		var me = this;
		var data = grid.getSelectionModel().getSelection();
        Ext.MessageBox.show({
        	title : '询问',
        	msg : '您确定要对数据进行备份吗?',
			width : 250,
			buttons : Ext.MessageBox.YESNO,
            icon : Ext.MessageBox.QUESTION,
            fn : function(btn) {
            	if (btn == 'yes') {
            		var dir = new Array();　
                    Ext.Array.each(data, function(items) {
                        var id = items.data.id;
                        dir.push(id);
            		});
                    var pram = {ids : dir};
                    var param = {jsonData : Ext.encode(pram)};
                    me.util.ExtAjaxRequest('sys_databackup/add_databackup.do', param,
                    function(response, options){
	                    grid.getStore().reload();
                    }, null, me.form);
                }
            }
        });
	},
	
	/**
     * 上传备份文件至服务器
     */
	download : function(grid){
		var me = this;
	    var data = grid.getSelectionModel().getSelection();
		var id = data[0].data.id;
        var pram = {id : id};
        var param = {jsonData : Ext.encode(pram)};
        me.util.ExtAjaxRequest('sys_databackup/download_databackup.do', param,
        function(response, options){
        	grid.getStore().reload();
        }, null, me.form);
	},
	
	/**
     * 还原备份文件
     */
	recovery : function(grid){
		var me = this;
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要恢复的数据！');
        }else if(data.length > 1){
        	Ext.MessageBox.alert('提示', '每次只能恢复一份数据！');
        }else {
        	Ext.MessageBox.show({
        		title : '询问',
        		msg : '您确定要对本数据进行恢复吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                	if (btn == 'yes') {
                		var id = data[0].data.id;
                        var pram = {id : id};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('sys_databackup/recovery_databackup.do', param,
                        function(response, options){
	                       	grid.getStore().reload();
                        }, null, me.form);
                    }
                }
            });
        }
	},
	
	/**
     * 下载
     */
	preview : function(filename){
		var me = this;
		if (me.contentUrl == null || me.contentUrl == ''){
			me.util.ExtAjaxRequest('Sys_SystemParameters/get_systemdatabackupdownloadurl.do', null,
	        function(response, options, respText){
				me.contentUrl = respText.data;
				if (me.contentUrl != null && me.contentUrl != ''){
					window.open(me.contentUrl.replace('XXXXXX', filename));
				}
	        });
		}else{
			window.open(me.contentUrl.replace('XXXXXX', filename));
		}
	}
	
});