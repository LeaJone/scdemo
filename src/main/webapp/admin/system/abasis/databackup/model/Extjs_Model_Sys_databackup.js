﻿/**
 * Sys_databackup Model
 * @author Administrator
 * 文件名  Extjs_Model_Sys_databackup
 * Model名  model_Sys_databackup
 */

Ext.define('model_sys_databackup', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'filename',
            type : 'string'
        }
        ,
        {
            name : 'filepath',
            type : 'string'
        }
        ,
        {
            name : 'backupdate',
            type : 'string'
        }
        ,
        {
            name : 'backupemployeename',
            type : 'string'
        }
    ]
});
