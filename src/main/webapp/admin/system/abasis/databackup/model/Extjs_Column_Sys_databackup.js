﻿/**
 * Sys_databackup Column
 * @author Administrator
 * 文件名     Extjs_Column_Sys_databackup
 * Columns名  column_Sys_databackup
 * xtype: 'textfield', 'numbercolumn', 'booleancolumn', 'datecolumn'   format : '0.00'
 * align: 'left', 'center', 'right'
 */

Ext.define('column_sys_databackup', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            align : 'center',
            flex : 0.1
        }
        ,
        {
            header : '文件名称',
            dataIndex : 'filename',
            sortable : true,
            align : 'center',
            flex : 1
        }
        ,
        {
            header : '保存路径',
            dataIndex : 'filepath',
            sortable : true,
            align : 'center',
            flex : 1
        }
        ,
        {
            header : '备份时间',
            dataIndex : 'backupdate',
            sortable : true,
            align : 'center',
            flex : 1,
			renderer : function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value);
			}
        }
        ,
        {
            header : '备份用户',
            dataIndex : 'backupemployeename',
            sortable : true,
            align : 'center',
            flex : 1
        }
        ,
        {
            header : '备份文件下载',
            dataIndex : 'filename',
            sortable : true,
            align : 'center',
            flex : 1,
  			renderer :function(value ,metaData ,record ){
  				metaData.style='color:blue;cursor:pointer;';
  				value="<font style='color:blue;'>下载</font>";
  				return value;
  			}
        }
    ]
});
