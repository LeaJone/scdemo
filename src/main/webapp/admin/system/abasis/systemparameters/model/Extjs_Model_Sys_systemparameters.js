﻿/**
 *sys_systemparameters Model
 *@author CodeSystem
 *文件名     Extjs_Model_Sys_systemparameters
 *Model名    model_sys_systemparameters
 */

Ext.define('model_sys_systemparameters', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'code',
            type : 'string'
        }
        ,
        {
            name : 'companyid',
            type : 'string'
        }
        ,
        {
            name : 'type',
            type : 'string'
        }
        ,
        {
            name : 'name',
            type : 'string'
        }
        ,
        {
            name : 'value',
            type : 'string'
        }
        ,
        {
            name : 'remark',
            type : 'string'
        }
    ]
});
