﻿/**
 *sys_systemparameters Column
 *@author CodeSystem
 *文件名     Extjs_Column_Sys_systemparameters
 *Columns名  column_sys_systemparameters
 */

Ext.define('column_sys_systemparameters', {
    columns: [
        {
            header : '编码',
            dataIndex : 'code',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '参数名称',
            dataIndex : 'name',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '参数值',
            dataIndex : 'value',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '备注',
            dataIndex : 'remark',
            sortable : true,
            flex : 1
        }
    ]
});
