﻿/**
 * 角色管理界面
 * @author lw
 */
Ext.define('system.abasis.systemparameters.biz.SystemParametersManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*', 
	             'system.widget.FsdButton',
	             'system.abasis.systemparameters.model.Extjs_Column_Sys_systemparameters',
	             'system.abasis.systemparameters.model.Extjs_Model_Sys_systemparameters'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();

		Ext.getCmp('systemparametersbtnadd').setVisible(false);
		Ext.getCmp('systemparametersbtndel').setVisible(false);
	},
	
    oper : Ext.create('system.abasis.systemparameters.biz.SystemParametersOper'),
    
	createTable : function() {
	    var me = this;

		/**
		 * 查询文本框
		 */
		var queryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入参数名称',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().reload();
					}
				}
			}
		});

		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_sys_systemparameters', // 显示列
			model : 'model_sys_systemparameters',
			baseUrl : 'Sys_SystemParameters/load_pagedata.do',
			border : false,
			tbar : [{
				id : 'systemparametersbtnadd',
				xtype : "fsdbutton",
				text : "添加",
				iconCls : 'page_addIcon',
				handler : function() {
					me.oper.add(grid);
				}
			}, {
				xtype : "fsdbutton",
				text : "修改",
				iconCls : 'page_editIcon',
				handler : function() {
					me.oper.editor(grid);
				}
			}, {
				id : 'systemparametersbtndel',
				xtype : "fsdbutton",
				text : "删除",
				iconCls : 'page_deleteIcon',
				handler : function() {
				    me.oper.del(grid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid);
				}
			}, {
				xtype : "fsdbutton",
				text : "导出",
				iconCls : 'articlewj',
				popedomCode : 'bmxz',
				handler : function() {
					me.oper.download();
				}
			}, '->', queryText, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					var name =  queryText.getValue();
					if (name == '101202303'){
						Ext.getCmp('systemparametersbtnadd').setVisible(true);
						Ext.getCmp('systemparametersbtndel').setVisible(true);
						return;
					}
					grid.getStore().reload();
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  queryText.getValue();
			var pram = {name : name};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});

		return grid;
	}
});