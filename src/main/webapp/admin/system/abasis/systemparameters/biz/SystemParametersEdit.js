﻿/**
 * 角色添加界面
 * @author lw
 */
Ext.define('system.abasis.systemparameters.biz.SystemParametersEdit', {
	extend : 'Ext.window.Window',
	requires : ['system.widget.FsdTreeComboBox'],
	grid : null,
	oper : Ext.create('system.abasis.systemparameters.biz.SystemParametersOper'),
	initComponent:function(){
	    var me = this;
	    
	    me.code = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '参数编码',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 340,
	    	name: 'code',
	    	maxLength: 50,
	    	allowBlank: false,
	    	blankText: '请输入参数编码',
	    	maxLengthText: '参数编码不能超过50个字符'
	    });
	    
	    me.name = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '参数名称',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 340,
	    	name: 'name',
	    	maxLength: 50,
	    	allowBlank: false,
	    	blankText: '请输入参数名称',
	    	maxLengthText: '参数名称不能超过50个字符'
	    });
	   
	    var value = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '参数值',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 340,
	    	name: 'value',	    	
	    	maxLength: 200,
	    	blankText: '请输入参数值',
	    	maxLengthText: '参数值不能超过200个字符'
	    });
	    
	    me.remark = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 340,
	    	name: 'remark',
	    	maxLength: 50,
	    	maxLengthText: '备注不能超过50个字符'
	    });
	    
	    var imageurlcol = Ext.create('system.widget.FsdTextFileManage', {
	        width : 340,
	    	zName : 'imageurl1',
	    	zFieldLabel : '图片地址',
	    	zLabelWidth : 90,
	    	zIsShowButton : true, 
	    	zIsUpButton : true,
	    	zFileType : 'image',
	    	zFileAutoName : false,
	    	zFileUpPath : me.oper.util.getSystemPath(),
	    	zManageType : 'upload'
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [{
                name: "id",
                xtype: "hidden"
            }, me.code, me.name, value, me.remark, imageurlcol]
        });
	        
	    Ext.apply(this,{
	        width:400,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '　保存　',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.formSubmit(form, me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});