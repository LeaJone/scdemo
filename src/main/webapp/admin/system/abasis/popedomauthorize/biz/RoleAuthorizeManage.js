﻿/**

 * 角色授权管理界面
 * @author lumingbao
 */

Ext.define('system.abasis.popedomauthorize.biz.RoleAuthorizeManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*', 
	             'system.abasis.popedomgroup.model.Extjs_Sys_popedomgroup'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.abasis.popedomauthorize.biz.RoleAuthorizeOper'),
	
	//标记当前选中角色ID
	roleId : "",
	
	createContent : function(){
		var me = this;
		
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPanel", {
			column : 'column_sys_popedomgroup', // 显示列
			model : 'model_sys_popedomgroup',
			baseUrl : 'Sys_PopedomGroup/load_data.do',
			border : false,
			multiSelect : true
		});
		grid.selModel.on('selectionchange', function(obj, selected, eOpts){
			if (selected.length > 0){
				var ids = [selected[0].data.id];
				qxTabPanel.setTitle("权限信息-" + selected[0].data.name);
				me.oper.GetGroupPopedom(me, ids, treepopedom, treesubject, treesubjectwap,treebranch);
			}else{
				qxTabPanel.setTitle("权限信息");
				var ids = new Array();
				me.oper.GetGroupPopedom(me, ids, treepopedom, treesubject, treesubjectwap,treebranch);
			} 
		});
		grid.selModel.setSelectionMode('SINGLE');
		
		/**
		 * 左边的面板
		 */
		var rolepanel = Ext.create("Ext.panel.Panel", {
			title: "角色信息",
			region : "center", // 设置方位
			layout: 'fit',
			items:[grid]
		});

		/**
		 * 菜单权限面板
		 */
		var treepopedom = Ext.create("Ext.tree.Panel", {
    		width: 500,
    		minWidth : 400,
    		border : 0,
            rootVisible : false,//不显示根节点
            tbar : [{
				    xtype : "fsdbutton",
				    popedomCode : 'cdsq',
					text : "保存菜单",
					iconCls : 'acceptIcon',
					handler : function() {
						me.oper.save(me, grid, treepopedom, 'qxlxcd');
					}
            	},{
					xtype : "button",
					text : "展开",
					iconCls : 'expand-all',
					handler : function() {
						treepopedom.expandAll();
					}
				},{
					xtype : "button",
					text : "收缩",
					iconCls : 'collapse-all',
					handler : function() {
						treepopedom.collapseAll();
					}
				}
				]
    	});

	    /**
		 * 栏目权限面板
		 */
		var treesubject = Ext.create("system.widget.FsdTreePanel", {
    		width: 400,
    		minWidth : 300,
    		border : 0,
            rootVisible : false,//不显示根节点
            baseUrl :'B_Subject/get_QxSubjectCheck.do',
	        zTreeRoot : 'children',//一次性加载
            zTreeParams : {id : 'FSDMAIN'},
            tbar : [{
				    xtype : "fsdbutton",
				    popedomCode : 'cdsq',
					text : "保存栏目",
					iconCls : 'acceptIcon',
					handler : function() {
						me.oper.save(me, grid, treesubject, 'qxlxlm');
					}
            	},{
					xtype : "button",
					text : "展开",
					iconCls : 'expand-all',
					handler : function() {
						treesubject.expandAll();
					}
				},{
					xtype : "button",
					text : "收缩",
					iconCls : 'collapse-all',
					handler : function() {
						treesubject.collapseAll();
					}
				}
				]
    	});
		
	    /**
		 * Wap栏目权限面板
		 */
		var treesubjectwap = null;
//		var treesubjectwap = Ext.create("system.widget.FsdTreePanel", {
//    		width: 400,
//    		minWidth : 300,
//    		border : 0,
//            rootVisible : false,//不显示根节点
//            baseUrl :'B_Subject/get_QxSubjectCheck.do',
//	        zTreeRoot : 'children',//一次性加载
//            zTreeParams : {id : 'FSDMOBILE'},
//            tbar : [{
//				    xtype : "fsdbutton",
//				    popedomCode : 'cdsq',
//					text : "保存Wap栏目",
//					iconCls : 'acceptIcon',
//					handler : function() {
//						me.oper.save(me, grid, treesubjectwap, 'qxlxlmw');
//					}
//            	},{
//					xtype : "button",
//					text : "展开",
//					iconCls : 'expand-all',
//					handler : function() {
//						treesubjectwap.expandAll();
//					}
//				},{
//					xtype : "button",
//					text : "收缩",
//					iconCls : 'collapse-all',
//					handler : function() {
//						treesubjectwap.collapseAll();
//					}
//				}
//				]
//    	});

	    /**
		 * 机构权限面板
		 */
		var treebranch = Ext.create("Ext.tree.Panel", {
    		width: 400,
    		minWidth : 300,
    		border : 0,
            rootVisible : false,//不显示根节点
            tbar : [{
				    xtype : "fsdbutton",
				    popedomCode : 'cdsq',
					text : "保存机构",
					iconCls : 'acceptIcon',
					handler : function() {
						me.oper.save(me, grid, treebranch, 'qxlxjg');
					}
            	},{
					xtype : "button",
					text : "展开",
					iconCls : 'expand-all',
					handler : function() {
						treebranch.expandAll();
					}
				},{
					xtype : "button",
					text : "收缩",
					iconCls : 'collapse-all',
					handler : function() {
						treebranch.collapseAll();
					}
				}
				]
    	});
		
	    me.on('afterrender', function(obj, eOpts){
			me.oper.LoadMenuTree(treepopedom);
			me.oper.LoadBranchTree(treebranch);
	    });
	    
	    /**
	     * 右边的大选项卡控件
	     */
	    var qxTabPanel = new Ext.tab.Panel({
	    	title : "权限信息",
			width: 400,
			height: 300,
			activeTab: 0,
			region: 'east',
			split: true,//拖动
			collapsible : true, //折叠
			collapseDirection: 'right',//折叠方向
			deferredRender: false,//是否在显示每个标签的时候再渲染标签中的内容.默认true
			items: [
					{
						xtype: 'panel',
						layout: 'fit',
						title: '菜单权限',
						items: [
						        treepopedom
					]},
					{
						xtype: 'panel',
						layout: 'fit',
						title: '栏目权限',
						items: [
						        treesubject
					]},
//					{
//						xtype: 'panel',
//						layout: 'fit',
//						title: 'WAP栏目权限',
//						items: [
//						        treesubjectwap
//					]},
					{
						xtype: 'panel',
						layout: 'fit',
						title: '机构权限',
						items: [
						        treebranch
					]}
			]
		});

		var page1_jExtPanel1_obj = new Ext.panel.Panel({
			bodyPadding: 3,
			header : false,
			border : 0,
			split: true,
			layout: 'border',
			items: [rolepanel,qxTabPanel]
		});
		return page1_jExtPanel1_obj;
	}
});