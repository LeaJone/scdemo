/**
 * 角色授权管理界面操作类
 * @author lumingbao
 */

Ext.define('system.abasis.popedomauthorize.biz.RoleAuthorizeOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
	 * 加载右侧系统菜单
	 */
	LoadMenuTree : function(treepanel){
	    var me = this;
	    treepanel.getRootNode().removeAll();
//	    me.util.ExtAjaxRequest('Sys_SystemMenu/get_AllQxMenuGroup.do' , {jsonData : Ext.encode({id : id})} ,
	    me.util.ExtAjaxRequest('Sys_SystemMenu/get_AllQxMenuCheck.do', null,
	    function(response, options, respText){
	    	if(respText.data != ""){
	    		treepanel.getRootNode().appendChild(respText.data);
	            treepanel.expandAll();
	    	}
	    }, null, treepanel);
	},
	/**
	 * 加载右侧机构部门
	 */
	LoadBranchTree : function(treepanel){
	    var me = this;
	    treepanel.getRootNode().removeAll();
	    me.util.ExtAjaxRequest('A_Branch/get_AllQxBranchCheck.do', null,
	    function(response, options, respText){
	    	if(respText.data != ""){
	    		treepanel.getRootNode().appendChild(respText.data);
	            treepanel.expandAll();
	    	}
	    }, null, treepanel);
	},	
	
	
	
	/**
	 * 保存数据
	 */
	save : function(form, grid, treepanel, qxlx){
		var me = this;
		var roleModels = grid.getSelectionModel().getSelection();
		if (roleModels.length == 0){
			Ext.MessageBox.alert('提示', '未选择需要授权角色信息！');
			return;
		}
		if (roleModels.length > 1){
			Ext.MessageBox.alert('提示', '只能选择一个角色进行授权！');
			return;
		}
		var checkedNodes = treepanel.getChecked();//tree必须事先创建好.
		var ids = [];
		for(var i=0 ; i < checkedNodes.length ; i++){
			ids.push(checkedNodes[i].internalId)
		}
		me.util.ExtAjaxRequest('Sys_PopedomAllocate/save_RolePopdom.do', 
				{jsonData : Ext.encode({id : roleModels[0].data.id, ids : ids, qxlx : qxlx})} ,
		function(response, options, respText){
			Ext.MessageBox.alert('提示', '保存成功！');
		}, null, form);
	},
	
	/**
     * 获取角色权限信息
     */
	GetGroupPopedom : function(form, ids, treepopedom, treesubject, treesubjectwap,treebranch){
	    var me = this;
	    if (ids.length > 0){
		    var pram = {ids : ids};
	        var param = {jsonData : Ext.encode(pram)};
	        me.util.ExtAjaxRequest('Sys_PopedomAllocate/get_PopedomIdsByRoleIds.do' , param ,
	        function(response, options, respText){
	        	var map = respText.data;
	        	me.FindChildNode(treepopedom.getRootNode(), map.qxlxcd);
	        	me.FindChildNode(treesubject.getRootNode(), map.qxlxlm);
	        	if (treesubjectwap != null)
	        		me.FindChildNode(treesubjectwap.getRootNode(), map.qxlxlmw);
	        	me.FindChildNode(treebranch.getRootNode(), map.qxlxjg);
	        }, null, form);
	    }else{
	    	var array = new Array();
	    	me.FindChildNode(treepopedom.getRootNode(), array);
        	me.FindChildNode(treesubject.getRootNode(), array);
        	if (treesubjectwap != null)
        		me.FindChildNode(treesubjectwap.getRootNode(), array);
        	me.FindChildNode(treebranch.getRootNode(), array);
	    }
	},
	FindChildNode : function(node, ids) {  
	    var me = this;
        var childnodes = node.childNodes;  
        Ext.Array.each(childnodes, function(rootnode) {
        	rootnode.raw.checked = false;
        	rootnode.set('checked',false);
        	Ext.Array.each(ids, function(id) {
	            if (rootnode.raw.id == id) {
	                rootnode.raw.checked = true;
	            	rootnode.set('checked',true);
	                return false;
	            }
        	});
            if (rootnode.childNodes.length > 0){
            	me.FindChildNode(rootnode, ids);
            }
        });
    }
});