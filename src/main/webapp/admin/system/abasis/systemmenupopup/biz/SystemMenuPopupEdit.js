﻿/**
 * 快捷菜单编辑界面
 * @author lmb
 */
Ext.define('system.abasis.systemmenupopup.biz.SystemMenuPopupEdit', {
	extend : 'Ext.window.Window',
	requires : [],
	
	initComponent : function() {
		var me = this;
		
		var oper = Ext.create('system.abasis.systemmenupopup.biz.SystemMenuPopupEditOper');
		
		var panel = Ext.create("Ext.tree.Panel", {
    		split : true,//拖动
    		animate : true,
            rootVisible : false,//不显示根节点
            minWidth : 90,
            height : 400,
            width : 300
    	});
		
		oper.LoadMenuTree(panel);
		
		Ext.apply(this,{
			layout : 'fit',
			width: 300,
			title : '定制快捷菜单',
	        resizable:false,// 改变大小
	        items :[panel],
	        buttons : [{
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
			    	oper.saveKjcd(panel);
			    	me.close();
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
		this.callParent(arguments);
	}
});