﻿/**
 * 快捷菜单面板
 * @author lmb
 */
Ext.define('system.abasis.systemmenupopup.biz.SystemMenuPopupPanel', {
	extend : 'Ext.panel.Panel',
	oper : Ext.create('system.abasis.systemmenupopup.biz.SystemMenuPopupEditOper'),
	requires : [],
	layout : 'fit',
	tbar : [{
		xtype : "fsdbutton",
		text : "快捷菜单设置",
		iconCls : 'collapse-all',
		handler : function(button) {
			var win = Ext.create('system.abasis.systemmenupopup.biz.SystemMenuPopupEdit');
			win.modal = true;
			win.show();
		}
	}],
	
	initComponent : function() {
		var me = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
    
	createTable : function() {
	    var me = this;
	    var buttons = Ext.create('Ext.container.ButtonGroup', {
	    	id : 'systemMenuPopupBuggonGroup',
	    	columns: 7,
	    	border : false
	    });
	    me.oper.loadJkcd(buttons);
	    return buttons;
	}
});