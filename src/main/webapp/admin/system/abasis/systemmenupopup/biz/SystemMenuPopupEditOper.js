/**
 * 定制快捷菜单操作类
 * 
 * @author fsd
 * @since 4014-05-27
 */
Ext.define('system.abasis.systemmenupopup.biz.SystemMenuPopupEditOper', {

	util : Ext.create('system.util.util'),
	
	/**
	 * 加载系统菜单
	 */
	LoadMenuTree : function(treepanel){
	    var me = this;
	    me.util.ExtAjaxRequest('Common/user_checkMenu.do' , {} ,
	    function(response, options, respText){
	    	if(respText.data != ""){
	    		treepanel.getRootNode().appendChild(respText.data);
	            treepanel.expandAll();
	    	}
	    },
	    function(response, options){
	    });
	},
	
	/**
	 * 提交
	 */
	saveKjcd : function(treepanel){
		var me = this;
		var checkedNodes = treepanel.getChecked();// tree必须事先创建好.
		var ids = [];
		for(var i=0 ; i < checkedNodes.length ; i++){
			ids.push(checkedNodes[i].internalId)
		}
		me.util.ExtAjaxRequest('Sys_SystemMenuPopup/save_systemMenuPopup.do', 
				{jsonData : Ext.encode({ids : ids})} ,
		function(response, options, respText){
			me.loadJkcd(Ext.getCmp("systemMenuPopupBuggonGroup"));
			Ext.MessageBox.alert('提示', '保存成功');
		}, null, treepanel);
	},
	
	/**
	 * 加载快捷菜单
	 */
	loadJkcd : function(buttons){
		var me = this;
		buttons.removeAll();
		me.util.ExtAjaxRequest('Common/user_checkMenu.do', null ,
		function(response, options, respText){
			for (var i=0; i<respText.data.length; i++){
				if(respText.data[i].checked){
					var itemButton = Ext.create('Ext.Button', {
						text : respText.data[i].text,
					    iconAlign : 'left',
					    scale : 'medium',
					    width : 150,
					    entity : respText.data[i],
					    style: 'margin-top:5px;margin-left:5px',
					   	icon : respText.data[i].icon,
					    pressed : true,
					    menu :[],
					    handler : function(obj) {
					    	if(obj.entity.module != ""){
					        	var model = Ext.create(obj.entity.module);
						        me.util.addTab(obj.entity.id , obj.entity.text , model , obj.entity.icon);
					        }
					    }
					});
					if(!itemButton.entity.leaf){
						var mainMenu = new Ext.menu.Menu();
					    for(var j = 0; j<itemButton.entity.children.length; j++){
					    	mainMenu.add({
					    		text : itemButton.entity.children[j].text,
					    		icon : itemButton.entity.children[j].icon,
					    		entity : itemButton.entity.children[j],
					    		handler : function(obj) {
					    			if(obj.entity.module != ""){
					    				var model = Ext.create(obj.entity.module);
								        me.util.addTab(obj.entity.id , obj.entity.text , model , obj.entity.icon);
							        }
					    		}
					    	});
					    }
					    itemButton.menu = mainMenu;
					}
					buttons.add(itemButton);
				}
				if(!respText.data[i].leaf){
					for(var j = 0; j<respText.data[i].children.length; j++){
						if(respText.data[i].children[j].checked){
					    	var itemButton1 = Ext.create('Ext.Button', {
								text : respText.data[i].children[j].text,
							    iconAlign : 'left',
							    scale : 'medium',
							    width : 150,
							    entity : respText.data[i].children[j],
							    style: 'margin-top:5px;margin-left:5px',
							    icon : respText.data[i].children[j].icon,
							    pressed : true,
							    handler : function(obj) {
							    	if(obj.entity.module != ""){
							    		var model = Ext.create(obj.entity.module);
								    	me.util.addTab(obj.entity.id , obj.entity.text , model , obj.entity.icon);
							        }
							    }
					    	});
					    	buttons.add(itemButton1);
				    	}
				    }
				}    		
			}
		}, null, null);
	}
});