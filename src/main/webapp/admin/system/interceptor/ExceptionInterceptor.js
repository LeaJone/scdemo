/**
 * 异常拦截器
 * 处理后台返回的有异常信息的请求(包括代码异常，业务异常，session超时)
 */
Ext.define('system.interceptor.ExceptionInterceptor',{
	extend:'system.interceptor.BaseInterceptor',
	
	interceptor:function(options,response){
		
		var resultData = Ext.decode(response.responseText);
		
		/**
		 * 处理代码异常和业务异常
		 */
		if(resultData.isException){
			Ext.MessageBox.alert(resultData.title,resultData.message);
			return false;
		}
		
		/**
		 * 处理Session超时
		 */
		if(resultData.isSessionOut){
			Ext.MessageBox.alert('提示','登陆超时，请重新登陆！',function(){
				window.location = 'login.html';
			});
			return;
		}
		
		/**
		 * 处理权限
		 */
		if(resultData.isNoAuthority){
			Ext.MessageBox.alert('提示', '权限不足！');
			return false;
		}
		return true;
	}
	
});