﻿/**
 * 版块编辑界面
 * @author lhl
 */
Ext.define('system.jbbs.board.biz.BoardEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	treepanel : null,
	oper : Ext.create('system.jbbs.board.biz.BoardOper'),
	initComponent:function(){
	    var me = this;
	    
	    var gjid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '根级ID',
	    	name: 'baseid',
	    	value: 'FSDBOARD'
	    });
	    
	    var sslmname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属版块名称',
	    	name: 'parentname'
	    });
	    var sslm = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属版块',
	    	labelAlign:'right',
	    	labelWidth:80,
	    	rootText : '主版块',
	    	rootId : 'FSDBOARD',
	        width : 360,
	        name : 'parentid',
	        baseUrl:'j_board/load_AsyncTree.do',//访问路劲
	        allowBlank: false,
	        hiddenName : 'parentname'//隐藏域Name
	    });
	    
	    var lmmc = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '版块名称',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'title',
	    	maxLength: 20,
	    	allowBlank: false
	    });
	    
	    var lmms = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '版块描述',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'description',
	    	maxLength: 100
	    });
	    
	    var lmtp1col = Ext.create('system.widget.FsdTextFileManage', {
	        width : 360,
	    	zName : 'imageurl1',
	    	zFieldLabel : '版块图片',
	    	zLabelWidth : 80,
	    	zIsShowButton : true, 
	    	zIsUpButton : true,
	    	zFileType : 'image',
	    	zFileAutoName : false,
	    	zFileUpPath : me.oper.util.getSystemPath(),
	    	zManageType : 'manage'
	    });

        var ispublish = new Ext.form.RadioGroup({
            fieldLabel: '允许发帖',
            labelAlign:'right',
            labelWidth:80,
			width: 360,
			items: [{
				name: 'ispublish',
				inputValue: 'true',
				boxLabel: '是',
				checked: true
			}, {
				name: 'ispublish',
				inputValue: 'false',
				boxLabel: '否'
            }]
		});
	    
	    var pxbh = new Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '排序编号',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'sort',
	    	maxLength: 3,
	    	minValue: 0,
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank: false
	    });
	    
	    var beizhu = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'remark',
	    	maxLength: 50,
	    	maxLengthText: '备注不能超过50个字符'
	    });
	    
	    me.on('show' , function(){
	    	sslm.setText(sslmname.getValue());
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
            padding : '15 15 10 0',
			items : [{
                name: "id",
                xtype: "hidden"
            } , gjid, sslmname, sslm, lmmc, lmms, lmtp1col, ispublish, pxbh, beizhu]
        });
	    
	    Ext.apply(this,{
            layout : 'fit',
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
				popedomCode : 'ltbkbj',
			    handler : function() {
    				me.oper.submitMain(form, me.grid , me.treepanel);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});