﻿Ext.define('column_j_board', {
    columns: [
        {
            header : '版块名称',
            dataIndex : 'title',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '所属版块',
            dataIndex : 'parentname',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '版块描述',
            dataIndex : 'description',
            sortable : true,
            flex : 3
        }
        ,
        {
            header : '版块图片',
            dataIndex : 'imageurl',
            sortable : true,
            align : 'center',
            flex : 0.5,
            renderer: function (value) {
                if (value != null && value != "") {
                    value = "<font style='color:green;'>已上传</font>";
                } else {
                    value = "<font style='color:red;'>未上传</font>";
                }
                return value;
            }
        }
        ,
        {
            header : '是否允许发帖',
            dataIndex : 'ispublish',
            sortable : true,
            align : 'center',
            flex : 0.5,
            renderer: function (value) {
                if (value == "false") {
                    value = "<font style='color:black;'>否</font>";
                } else {
                    value = "<font style='color:blue;'>是</font>";
                }
                return value;
            }
        }
        ,
        {
            header : '顺序号',
            dataIndex : 'sort',
            align : 'center',
            sortable : true,
            flex : 0.5
        }

    ]
});