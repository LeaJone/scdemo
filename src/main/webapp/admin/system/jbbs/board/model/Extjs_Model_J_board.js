﻿Ext.define('model_j_board', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'title',
            type : 'string'
        }
        ,
        {
            name : 'parentname',
            type : 'string'
        }
        ,
        {
            name : 'description',
            type : 'string'
        }
        ,
        {
            name : 'imageurl',
            type : 'string'
        }
        ,
        {
            name : 'ispublish',
            type : 'string'
        }
        ,
        {
            name : 'sort',
            type : 'int'
        }
    ]
});
