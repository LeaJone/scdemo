/**
 * 帖子回复操作类
 *
 * @author lhl
 */

Ext.define('system.jbbs.black.biz.BlackOper', {

	form : null,
	util : Ext.create('system.util.util'),

    /**
     * 删除帖子
     */
    delTz : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要删除的帖子！');
        }else {
            Ext.MessageBox.show({
                title : '询问',
                msg : '你确定要删除吗？',
                width : 250,
                buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                        var dir = new Array();
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
                        });
                        var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('j_posts/del_j_posts_qx_lttzsc.do', param, function(){
                            grid.getStore().reload();
                        }, null, me.form);
                    }
                }
            });
        }
    },

     /**
      * 删除回复
      */
     delHf : function(grid){
    	 var me = this;
 		 // 获取选中的行
 	     var data = grid.getSelectionModel().getSelection();
         if (data.length == 0) {
             Ext.MessageBox.alert('提示', '请先选中要删除的评论！');
         }else {
        	 Ext.MessageBox.show({
        		 title : '询问',
        		 msg : '你确定要删除吗？',
 			     width : 250,
 				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                     if (btn == 'yes') {
                    	 var dir = new Array();　
                         Ext.Array.each(data, function(items) {
                             var id = items.data.id;
                             dir.push(id);
             			});
             			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('j_reply/del_j_reply_qx_lthfsc.do', param,
                        function(){
                        	grid.getStore().reload(); 
                        }, null, me.form);
                    }
                }
            });
        }
    },

    /**
     * 禁言
     */
    shutup : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要禁言的用户！');
        }else {
            Ext.MessageBox.show({
                title : '询问',
                msg : '是否屏蔽所选用户的所有发言？',
                width : 250,
                buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    var dir = new Array();
                    Ext.Array.each(data, function(items) {
                        var id = items.data.id;
                        dir.push(id);
                    });
                    var pram = {ids : dir};
                    var param = {jsonData : Ext.encode(pram)};
                    if (btn == 'yes') {
                        me.util.ExtAjaxRequest('j_black/shutup_j_black_qx_lthfjyyes.do', param, function(){
                            Ext.MessageBox.alert('提示', '操作成功！');
                        }, null, me.form);
                    }else if(btn == 'no'){
                        me.util.ExtAjaxRequest('j_black/shutup_j_black_qx_lthfjyno.do', param, function(){
                            Ext.MessageBox.alert('提示', '操作成功！');
                        }, null, me.form);
                    }
                }
            });
        }
    },

    /**
     * 查看帖子
     */
    viewTz : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if(data.length > 0){
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.jbbs.posts.biz.PostsEdit');
            win.setTitle('帖子查看');
            win.modal = true;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(win.down('form').getForm() , 'j_posts/load_j_postsbyid.do', param, null,
                function(){
                    win.show();
                });
        }
    },

    /**
     * 查看回复
     */
    viewHf : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if(data.length > 0){
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.jbbs.reply.biz.ReplyEdit');
            win.setTitle('帖子回复查看');
            win.modal = true;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(win.down('form').getForm() , 'j_reply/load_j_replybyid.do', param, null,
                function(){
                    win.show();
                });
        }
    },

    /**
     * 解禁
     */
    ban : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选中要解除禁言的用户！');
        } else {
            Ext.MessageBox.show({
                title : '询问',
                msg : '是否恢复所选用户的全部信息？',
                width : 250,
                buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    var dir = new Array();
                    Ext.Array.each(data, function(items) {
                        var id = items.data.id;
                        dir.push(id);
                    });
                    var pram = {ids : dir};
                    var param = {jsonData : Ext.encode(pram)};
                    if (btn == 'yes') {
                        me.util.ExtAjaxRequest('j_black/shutup_j_black_qx_ltjyjcyes.do', param, function(){
                            Ext.MessageBox.alert('提示', '操作成功！');
                            grid.getStore().reload();
                        }, null, me.form);
                    }else if(btn == 'no'){
                        me.util.ExtAjaxRequest('j_black/shutup_j_black_qx_ltjyjcno.do', param, function(){
                            Ext.MessageBox.alert('提示', '操作成功！');
                            grid.getStore().reload();
                        }, null, me.form);
                    }
                }
            });
        }
    }

});