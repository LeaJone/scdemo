﻿/**
 * 黑名单管理界面
 *
 * @author lhl
 */

Ext.define('system.jbbs.black.biz.BlackManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.jbbs.reply.model.Extjs_Column_Reply',
	             'system.jbbs.reply.model.Extjs_Model_Reply',
				 'system.jbbs.posts.model.Extjs_Column_J_posts',
				 'system.jbbs.posts.model.Extjs_Model_J_posts',
				 'system.jbbs.black.model.Extjs_Column_Black',
				 'system.jbbs.black.model.Extjs_Model_Black'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.jbbs.black.biz.BlackOper'),
	
	createContent : function(){
		var me = this;

		var queryid = '';

		/**
		 * 查询文本框（人员）
		 */
		var ryQueryText =  Ext.create("Ext.form.TextField" , {
			name : 'ryQueryParam',
			emptyText : '请输入人员姓名',
			enableKeyEvents : true,
			width : 120,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						ryGrid.getStore().loadPage(1);
					}
				}
			}
		});

        /**
         * 查询文本框（帖子）
         */
        var tzQueryText =  Ext.create("Ext.form.TextField" , {
            name : 'tzQueryParam',
            emptyText : '请输入帖子标题',
            enableKeyEvents : true,
            width : 130,
            listeners : {
                specialkey : function(field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        tzGrid.getStore().loadPage(1);
                    }
                }
            }
        });

        /**
         * 查询文本框（回复）
         */
        var hfQueryText =  Ext.create("Ext.form.TextField" , {
            name : 'queryParam',
            emptyText : '请输入帖子标题',
            enableKeyEvents : true,
            width : 130,
            listeners : {
                specialkey : function(field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        hfGrid.getStore().loadPage(1);
                    }
                }
            }
        });

        // 人员表格
        var ryGrid = Ext.create("system.widget.FsdGridPagePanel", {
            column : 'column_j_black',
            model: 'model_j_black',
            baseUrl : 'j_black/load_pagedata.do',
            tbar : [{
                xtype : "fsdbutton",
                text : "解禁",
                iconCls : 'page_deleteIcon',
                popedomCode : 'ltjyjc',
                handler : function() {
                    me.oper.ban(ryGrid);
                }
            }, '->', ryQueryText, {
                text : '查询',
                iconCls : 'magnifierIcon',
                handler : function() {
                    ryGrid.getStore().loadPage(1);
                }
            }]
        });
        ryGrid.getStore().on('beforeload', function(s) {
            var username = ryQueryText.getValue();
            var pram = {
                username : username};
            var params = s.getProxy().extraParams;
            Ext.apply(params,{jsonData : Ext.encode(pram)});
        });
        ryGrid.on('itemclick', function(view,record){
            queryid = record.raw.userid;
            tzGrid.getStore().loadPage(1);
            grid.getStore().loadPage(1);
        });

        // 帖子表格
        var tzGrid = Ext.create("system.widget.FsdGridPagePanel", {
            column : 'column_j_posts_reply',
            model: 'model_j_posts_reply',
            baseUrl : 'j_posts/load_pagedata.do',
            zAutoLoad : false,
            tbar : [{
                text : '查看',
                iconCls : 'magnifierIcon',
                handler : function() {
                    me.oper.viewTz(tzGrid);
                }
            }, {
                xtype : "fsdbutton",
                text : "删除",
                iconCls : 'page_deleteIcon',
                popedomCode : 'lttzsc',
                handler : function() {
                    me.oper.delTz(tzGrid);
                }
            }, '->', tzQueryText, {
                text : '查询',
                iconCls : 'magnifierIcon',
                handler : function() {
                    tzGrid.getStore().loadPage(1);
                }
            }, '-', {
                text : '刷新',
                iconCls : 'tbar_synchronizeIcon',
                handler : function() {
                    tzGrid.getStore().reload();
                }
            }]
        });
        tzGrid.getStore().on('beforeload', function(s) {
            var title = tzQueryText.getValue();
            var pram = {
                blackid : queryid,
                title : title};
            var params = s.getProxy().extraParams;
            Ext.apply(params,{jsonData : Ext.encode(pram)});
        });
		
		// 回复表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_j_reply',
			model: 'model_j_reply',
			baseUrl : 'j_reply/load_pagedata.do',
            zAutoLoad : false,
			tbar : [{
                text : '查看',
                iconCls : 'magnifierIcon',
                handler : function() {
                    me.oper.viewHf(grid);
                }
            }, {
				xtype : "fsdbutton",
				text : "删除",
				iconCls : 'page_deleteIcon',
				popedomCode : 'lthfsc',
				handler : function() {
				    me.oper.delHf(grid);
				}
			}, '->', hfQueryText, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var title = hfQueryText.getValue();
			var pram = {
                    blackid : queryid,
					ptitle : title};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});

        var ryPanel = Ext.create("Ext.panel.Panel", {
            layout:'fit',
            frame:true,
            split: true,
            region : 'west',
            title: '黑名单',
            width: '20%',
            items: [ryGrid]
        });

        var tzPanel = Ext.create("Ext.panel.Panel", {
            layout:'fit',
            frame:true,
            split: true,
            region : 'center',
            title: '帖子信息',
            width: '40%',
            items: [tzGrid]
        });
		
		var hfpanel = Ext.create("Ext.panel.Panel", {
            layout: 'fit',
			frame:true,
			split: true,
			region: 'east',
            width: '40%',
			title: '帖子回复信息',
			items: [grid]
		});

		var page1_jExtPanel1_obj = new Ext.panel.Panel({
			bodyPadding: 3,
			header : false,
			border : 0,
			split: true,
			layout: 'border',
			items: [ryPanel, tzPanel, hfpanel]
		});
		
		return page1_jExtPanel1_obj;
	}
});