﻿Ext.define('column_j_black', {
    columns: [
        {
            header : '黑名单姓名',
            dataIndex : 'username',
            sortable : true,
            align : 'left',
            flex : 1
        }
    ]
});
