﻿/**
 * 团队添加界面
 */
Ext.define('system.jbbs.posts.biz.PostsFixEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.jbbs.posts.biz.PostsOper'),
	initComponent:function(){
	    var me = this;
	    var title = new Ext.create('system.widget.FsdTextField',{
            fieldLabel: '帖子标题',
            labelAlign:'right',
            labelWidth:70,
            width : 720,
            name: 'title'
        });

	    var content = Ext.create('system.widget.FsdUeditor',{
            fieldLabel: '帖子内容',
            labelAlign:'right',
            labelWidth:70,
            width : 750,
            height : 500,
            name: 'content',
            margin:'0 0 30 0'
        });

	    var form = Ext.create('Ext.form.Panel',{
	    	border : false,
			padding : '5 0 0 5',
			items : [{
				name: "id",
				xtype: "hidden"
			}, title, content]
        });
	        
	    Ext.apply(this,{
	    	width:730,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
				xtype : "fsdbutton",
				name : 'btnsave',
				text : '保存',
				iconCls : 'acceptIcon',
				handler : function() {
					me.oper.formSubmit(form , me.grid);
				}
			}, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});