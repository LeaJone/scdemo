/**
 * 帖子操作类
 *
 * @author lhl
 */

Ext.define('system.jbbs.posts.biz.PostsOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 修改
     */
	editor : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的帖子！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能修改一条帖子！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.jbbs.posts.biz.PostsFixEdit');
            win.setTitle('帖子修改');
            win.modal = true;
            win.grid = grid;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'j_posts/load_j_postsbyid.do' , param , null , 
	        function(){
	            win.show();
	        });
        }
	},
	
	/**
     * 表单提交
     */
     formSubmit : function(formpanel, grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('j_posts/save_z_post_qx_z_teambj.do' , formpanel.form , '正在提交数据,请稍候.....',
			function(form, action){
                formpanel.up('window').close();
				Ext.MessageBox.alert('提示', '保存成功！');
                grid.getStore().reload();
            });
        }
     },
	
     /**
      * 删除
      */
     del : function(grid){
    	 var me = this;
 		 // 获取选中的行
 	     var data = grid.getSelectionModel().getSelection();
         if (data.length == 0) {
             Ext.MessageBox.alert('提示', '请先选中要删除的帖子！');
         }else {
        	 Ext.MessageBox.show({
        		 title : '询问',
        		 msg : '你确定要删除吗？',
 			     width : 250,
 				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                     if (btn == 'yes') {
                    	 var dir = new Array();　
                         Ext.Array.each(data, function(items) {
                             var id = items.data.id;
                             dir.push(id);
             			});
             			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('j_posts/del_j_posts_qx_lttzsc.do', param, function(){
                        	grid.getStore().reload(); 
                        }, null, me.form);
                     }
                 }
             });
         }
 	},

 	/**
     * 查看
     */
	view : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if(data.length > 0){
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.jbbs.posts.biz.PostsEdit');
            win.setTitle('帖子查看');
            win.modal = true;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'j_posts/load_j_postsbyid.do', param, null,
	        function(){
	            win.show();
	        });
        }
	},
	
	/**
     * 置顶
     */
	isEnabled : function(grid, isQT){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
		        msg : '你确定要执行该操作吗？',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
					    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
			            });
             			var pram = {ids : dir, firstly : isQT};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('j_posts/firstly_j_posts_qx_lttzbj.do', param,
                        function(){
                        	grid.getStore().reload(); 
                        });
                    }
                }
            });
        }
	},

    /**
     * 禁言
     */
    shutup : function(grid){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要禁言的用户！');
        }else {
            Ext.MessageBox.show({
                title : '询问',
                msg : '是否屏蔽所选用户的所有发言？',
                width : 250,
                buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    var dir = new Array();
                    Ext.Array.each(data, function(items) {
                        var id = items.data.id;
                        dir.push(id);
                    });
                    var pram = {ids : dir};
                    var param = {jsonData : Ext.encode(pram)};
                    if (btn == 'yes') {
                        me.util.ExtAjaxRequest('j_black/shutup_j_black_qx_lttzjyyes.do', param, function(){
                            Ext.MessageBox.alert('提示', '操作成功！');
                        }, null, me.form);
                    }else if(btn == 'no'){
                        me.util.ExtAjaxRequest('j_black/shutup_j_black_qx_lttzjyno.do', param, function(){
                            Ext.MessageBox.alert('提示', '操作成功！');
                        }, null, me.form);
                    }
                }
            });
        }
    }

});