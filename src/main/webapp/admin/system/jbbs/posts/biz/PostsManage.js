﻿/**
 * 帖子管理界面
 *
 * @author lhl
 */

Ext.define('system.jbbs.posts.biz.PostsManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.jbbs.posts.model.Extjs_Column_J_posts',
	             'system.jbbs.posts.model.Extjs_Model_J_posts'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.jbbs.posts.biz.PostsOper'),
	
	createContent : function(){
		var me = this;

		var queryid = 'FSDBOARD';

		/**
		 * 查询文本框
		 */
		var queryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入帖子标题',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});
		
		/**
		 * 左边的树面板
		 */
		var treepanel = Ext.create("system.widget.FsdTreePanel", {
			icon : 'resources/admin/icon/chart_organisation.png',
			baseUrl : 'j_board/load_AsyncTree.do',
			title: '版块结构',
			rootText : '主版块',
			rootId : queryid,
			region : 'west', // 设置方位
			collapsible : true,//折叠
			width: 182,
			split: true
		});
		treepanel.on('itemclick', function(view,record){
			queryid = record.raw.id;
			grid.getStore().loadPage(1);
		});
		
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_j_posts', // 显示列
			model: 'model_j_posts',
			baseUrl : 'j_posts/load_pagedata.do',
			tbar : [{
                text : '查看',
                iconCls : 'magnifierIcon',
                handler : function() {
                    me.oper.view(grid);
                }
            },{
				xtype : "fsdbutton",
				text : "修改",
				iconCls : 'page_editIcon',
				handler : function() {
					me.oper.editor(grid);
				}
			}, {
				xtype : "fsdbutton",
				text : "删除",
				iconCls : 'page_deleteIcon',
				popedomCode : 'lttzsc',
				handler : function() {
				    me.oper.del(grid);
				}
			}, '-', {
				xtype : "fsdbutton",
				text : "帖子置顶",
				iconCls : 'tickIcon',
				popedomCode : 'lttzbj',
				handler : function() {
				    me.oper.isEnabled(grid, true);
				}
			}, {
				xtype : "fsdbutton",
				text : "取消置顶",
				iconCls : 'crossIcon',
				popedomCode : 'lttzbj',
				handler : function() {
				    me.oper.isEnabled(grid, false);
				}
			}, '-', {
                xtype : "fsdbutton",
                text : "禁言",
                iconCls : 'crossIcon',
                popedomCode : 'ltjy',
                handler : function() {
                    me.oper.shutup(grid);
                }
            }, '->', queryText, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var title =  queryText.getValue();
			var pram = {
					fid : queryid,
					title : title};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		var contentpanel = Ext.create("Ext.panel.Panel", {
			frame:true,
			split: true,
			region: 'center',
			layout: 'fit',
			title: '帖子信息',
			items: [grid]
		});

		var page1_jExtPanel1_obj = new Ext.panel.Panel({
			bodyPadding: 3,
			header : false,
			border : 0,
			split: true,
			layout: 'border',
			items: [treepanel,contentpanel]
		});
		
		return page1_jExtPanel1_obj;
	}
});