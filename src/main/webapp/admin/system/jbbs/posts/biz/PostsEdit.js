/**
 * 帖子编辑界面
 *
 * @author lhl
 */
Ext.define('system.jbbs.posts.biz.PostsEdit',{
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.jbbs.posts.biz.PostsOper'),
    initComponent:function(){
        var me = this;

        var pubtime = Ext.create('system.widget.FsdDateTime',{
            fieldLabel: '发帖时间',
            labelAlign:'right',
            labelWidth:70,
            width : 360,
            name: 'pubtime'
        });

        var pubusername = new Ext.create('system.widget.FsdTextField',{
            fieldLabel: '发帖人',
            labelAlign:'right',
            labelWidth:70,
            width : 360,
            name: 'pubusername'
        });

        var title = new Ext.create('system.widget.FsdTextField',{
            fieldLabel: '帖子标题',
            labelAlign:'right',
            labelWidth:70,
            width : 720,
            name: 'title'
        });

        var content = Ext.create('system.widget.FsdUeditor',{
            fieldLabel: '帖子内容',
            labelAlign:'right',
            labelWidth:70,
            width : 750,
            height : 500,
            name: 'content',
            margin:'0 0 30 0'
        });

        var panel = Ext.create('Ext.form.FieldSet',{
            title : '帖子信息',
            items:[{
                layout : 'column',
                border : false,
                items:[{
                    columnWidth : 0.5,
                    border:false,
                    items:[pubtime]
                }, {
                    columnWidth : 0.5,
                    border:false,
                    items:[pubusername]
                }]
            }, title, content ]
        });

        var form = Ext.create('Ext.form.Panel',{
            xtype : 'form',
            header : false,
            border : false,
            padding : '20 20 15 20',
            items : [
                {
                    name: "id",
                    xtype: "hidden"
                }, panel ]
        });

        var buttonItem = [{
            text : '关闭',
            iconCls : 'deleteIcon',
            handler : function() {
                me.close();
            }
        }];
        Ext.apply(this,{
            layout : 'fit',
            bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
            resizable:false,// 改变大小
            items :[form],
            buttons : buttonItem
        });
        this.callParent(arguments);
    }
});