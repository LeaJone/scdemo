﻿Ext.define('model_j_posts', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'boardtitle',
            type : 'string'
        }
        ,
        {
            name : 'title',
            type : 'string'
        }
        ,
        {
            name : 'firstly',
            type : 'string'
        }
        ,
        {
            name : 'pubtime',
            type : 'string'
        }
        ,
        {
            name : 'iscomment',
            type : 'string'
        }
        ,
        {
            name : 'pubusername',
            type : 'string'
        }
    ]
});

Ext.define('model_j_posts_reply', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'title',
            type : 'string'
        }
        ,
        {
            name : 'pubtime',
            type : 'string'
        }
        ,
        {
            name : 'pubusername',
            type : 'string'
        }
    ]
});
