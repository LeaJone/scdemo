﻿Ext.define('column_j_posts', {
    columns: [
        {
            xtype : 'rownumberer',
            text : '',
            sortable : true,
            align : 'left',
            width : 30
        }
        ,
        {
            header : '所属版块',
            dataIndex : 'boardtitle',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '贴子标题',
            dataIndex : 'title',
            sortable : true,
            align : 'left',
            flex : 3
        }
        ,
        {
            header : '置顶',
            dataIndex : 'firstly',
            sortable : true,
            align : 'center',
            flex : 0.5,
			renderer :function(value){
				if(value == 'true'){
					value="<font style='color:blue;'>是</font>";
				}else{
					value="否";
				}
				return value;
			}
        }
        ,
        {
            header : '是否开启评论',
            dataIndex : 'iscomment',
            sortable : true,
            align : 'center',
            flex : 0.5,
            renderer :function(value){
                if(value == 'on'){
                    value="开启";
                }else{
                    value="<font style='color:red;'>关闭</font>";
                }
                return value;
            }
        }
        ,
        {
            header : '发帖时间',
            dataIndex : 'pubtime',
            sortable : true,
            align : 'center',
            flex : 1,
			renderer :function(value){
				return system.UtilStatic.formatDateTime(value);
			}
        }
        ,
        {
            header : '发帖人',
            dataIndex : 'pubusername',
            sortable : true,
            align : 'center',
            flex : 1
        }
    ]
});


Ext.define('column_j_posts_reply', {
    columns: [
        {
            header : '贴子标题',
            dataIndex : 'title',
            sortable : true,
            align : 'left',
            flex : 2.5
        }
        ,
        {
            header : '发帖时间',
            dataIndex : 'pubtime',
            sortable : true,
            align : 'center',
            flex : 1.5,
            renderer :function(value){
                return system.UtilStatic.formatDateTime(value);
            }
        }
        ,
        {
            header : '发帖人',
            dataIndex : 'pubusername',
            sortable : true,
            align : 'center',
            flex : 1
        }
    ]
});
