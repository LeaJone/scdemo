﻿Ext.define('column_j_reply', {
    columns: [
        {
            xtype : 'rownumberer',
            text : '',
            sortable : true,
            align : 'left',
            width : 30
        }
        ,
        {
            header : '主帖标题',
            dataIndex : 'poststitle',
            sortable : true,
            align : 'left',
            flex : 2.5
        }

        ,
        {
            header : '回复时间',
            dataIndex : 'retime',
            sortable : true,
            align : 'center',
            flex : 1.5,
			renderer :function(value){
				return system.UtilStatic.formatDateTime(value);
			}
        }
        ,
        {
            header : '回复人',
            dataIndex : 'reusername',
            sortable : true,
            align : 'center',
            flex : 1
        }
    ]
});
