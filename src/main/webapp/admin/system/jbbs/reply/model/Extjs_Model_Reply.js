﻿Ext.define('model_j_reply', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'poststitle',
            type : 'string'
        }
        ,
        {
            name : 'retime',
            type : 'double'
        }
        ,
        {
            name : 'reusername',
            type : 'string'
        }
    ]
});
