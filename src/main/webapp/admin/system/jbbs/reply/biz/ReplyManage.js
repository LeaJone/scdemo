﻿/**
 * 帖子回复管理界面
 *
 * @author lhl
 */

Ext.define('system.jbbs.reply.biz.ReplyManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.jbbs.reply.model.Extjs_Column_Reply',
	             'system.jbbs.reply.model.Extjs_Model_Reply',
				 'system.jbbs.posts.model.Extjs_Column_J_posts',
				 'system.jbbs.posts.model.Extjs_Model_J_posts'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.jbbs.reply.biz.ReplyOper'),
	
	createContent : function(){
		var me = this;

		var queryid = 'FSDBOARD';

		/**
		 * 查询文本框
		 */
		var queryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入帖子标题',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});

        /**
         * 查询文本框
         */
        var tzQueryText =  Ext.create("Ext.form.TextField" , {
            name : 'tzQueryParam',
            emptyText : '请输入帖子标题',
            enableKeyEvents : true,
            width : 130,
            listeners : {
                specialkey : function(field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        tzGrid.getStore().loadPage(1);
                    }
                }
            }
        });
		
		/**
		 * 左边的树面板
		 */
		var treepanel = Ext.create("system.widget.FsdTreePanel", {
			icon : 'resources/admin/icon/chart_organisation.png',
			baseUrl : 'j_board/load_AsyncTree.do',
			title: '版块结构',
			rootText : '主版块',
			rootId : queryid,
			region : 'west', // 设置方位
			collapsible : true,//折叠
			width: 182,
			split: true
		});
		treepanel.on('itemclick', function(view,record){
			queryid = record.raw.id;
            tzGrid.getStore().loadPage(1);
		});

        // 创建表格
        var tzGrid = Ext.create("system.widget.FsdGridPagePanel", {
            column : 'column_j_posts_reply',
            model: 'model_j_posts_reply',
            baseUrl : 'j_posts/load_pagedata.do',
            zAutoLoad : false,
            tbar : [{
                text : '查看',
                iconCls : 'magnifierIcon',
                handler : function() {
                    me.oper.viewTz(tzGrid);
                }
            }, '->', tzQueryText, {
                text : '查询',
                iconCls : 'magnifierIcon',
                handler : function() {
                    tzGrid.getStore().loadPage(1);
                }
            }, '-', {
                text : '刷新',
                iconCls : 'tbar_synchronizeIcon',
                handler : function() {
                    tzGrid.getStore().reload();
                }
            }]
        });
        tzGrid.getStore().on('beforeload', function(s) {
            var title = tzQueryText.getValue();
            var pram = {
                fid : queryid,
                title : title};
            var params = s.getProxy().extraParams;
            Ext.apply(params,{jsonData : Ext.encode(pram)});
        });
        tzGrid.on('itemclick', function(view,record){
            queryid = record.raw.id;
            grid.getStore().loadPage(1);
        });
		
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_j_reply', // 显示列
			model: 'model_j_reply',
			baseUrl : 'j_reply/load_pagedata.do',
            zAutoLoad : false,
			tbar : [{
                text : '查看',
                iconCls : 'magnifierIcon',
                handler : function() {
                    me.oper.view(grid);
                }
            }, {
				xtype : "fsdbutton",
				text : "删除",
				iconCls : 'page_deleteIcon',
				popedomCode : 'lthfsc',
				handler : function() {
				    me.oper.del(grid);
				}
			}, '-', {
                xtype : "fsdbutton",
                text : "禁言",
                iconCls : 'crossIcon',
                popedomCode : 'ltjy',
                handler : function() {
                    me.oper.shutup(grid);
                }
            }, '->', queryText, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var title = queryText.getValue();
			var pram = {
					fid : queryid,
					ptitle : title};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});

        var tzPanel = Ext.create("Ext.panel.Panel", {
            layout:'fit',
            frame:true,
            split: true,
            region : 'center',
            title: '帖子信息',
            width: '45%',
            items: [tzGrid]
        });
		
		var hfpanel = Ext.create("Ext.panel.Panel", {
            layout: 'fit',
			frame:true,
			split: true,
			region: 'east',
            width: '40%',
			title: '帖子回复信息',
			items: [grid]
		});

		var page1_jExtPanel1_obj = new Ext.panel.Panel({
			bodyPadding: 3,
			header : false,
			border : 0,
			split: true,
			layout: 'border',
			items: [treepanel, tzPanel, hfpanel]
		});
		
		return page1_jExtPanel1_obj;
	}
});