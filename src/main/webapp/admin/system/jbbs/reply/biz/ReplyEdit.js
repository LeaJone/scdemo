﻿/**
 * 帖子回复编辑界面
 *
 * @author lhl
 */
Ext.define('system.jbbs.reply.biz.ReplyEdit',{
    extend : 'Ext.window.Window',
    grid : null,
    oper : Ext.create('system.jbbs.reply.biz.ReplyOper'),
    initComponent:function(){
        var me = this;

        var retime = Ext.create('system.widget.FsdDateTime',{
            fieldLabel: '回复时间',
            labelAlign:'right',
            labelWidth:70,
            width : 350,
            name: 'retime'
        });

        var reusername = new Ext.create('system.widget.FsdTextField',{
            fieldLabel: '回复人',
            labelAlign:'right',
            labelWidth:70,
            width : 350,
            name: 'reusername'
        });

        var poststitle = new Ext.create('system.widget.FsdTextField',{
            fieldLabel: '帖子标题',
            labelAlign:'right',
            labelWidth:70,
            width : 700,
            name: 'poststitle'
        });

        var content = Ext.create('system.widget.FsdUeditor',{
            fieldLabel: '回复内容',
            labelAlign:'right',
            labelWidth:70,
            width : 730,
            height : 500,
            name: 'content',
            padding:'0 0 50 0'
        });

        var panel = Ext.create('Ext.form.FieldSet',{
            title : '回帖信息',
            items:[{
                layout : 'column',
                border : false,
                items:[{
                    columnWidth : 0.5,
                    border:false,
                    items:[retime]
                }, {
                    columnWidth : 0.5,
                    border:false,
                    items:[reusername]
                }]
            }, poststitle, content ]
        });

        var form = Ext.create('Ext.form.Panel',{
            xtype : 'form',
            header : false,
            border : false,
            padding : '20 20 15 20',
            items : [
                {
                    name: "id",
                    xtype: "hidden"
                }, panel ]
        });

        var buttonItem = [{
            text : '关闭',
            iconCls : 'deleteIcon',
            handler : function() {
                me.close();
            }
        }];
        Ext.apply(this,{
            layout : 'fit',
            bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
            resizable:false,// 改变大小
            items :[form],
            buttons : buttonItem
        });
        this.callParent(arguments);
    }
});