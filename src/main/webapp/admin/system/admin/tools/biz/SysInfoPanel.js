﻿/**
 * 系统信息面板
 * @author lmb
 */
Ext.define('system.admin.tools.biz.SysInfoPanel', {
	extend : 'Ext.panel.Panel',
	requires : [ 'system.admin.tools.model.Extjs_Column_SysInfo', 
	             'system.admin.tools.model.Extjs_Model_SysInfo' ],
	layout : 'fit',
	title: '服务器信息',
	
	initComponent : function() {
		var me = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
    
	createTable : function() {
	    var me = this;

		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPanel", {
			column : 'column_sysinfo', // 显示列
			model : 'model_sysinfo',
			baseUrl : 'Common/get_sysInfo.do',
			border : false,
			selType : 'rowmodel'
		});
		return grid;
	}
});