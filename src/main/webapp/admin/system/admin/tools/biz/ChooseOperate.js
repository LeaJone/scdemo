﻿/**
 * 顺序号选择界面
 * @author lw
 */
Ext.define('system.admin.tools.biz.ChooseOperate', {
	extend : 'Ext.window.Window',

	model : '',
	column : '',
	baseUrl : '',
	gridType : 'page',//page分页控件，list无分页列表
	funQuery : null,//查询前事件
	isText1 : false,
	isText2 : false,
	isText3 : false,
	isText4 : false,
	txtLabel1 : '',
	txtLabel2 : '',
	txtLabel3 : '',
	txtLabel4 : '',
	txtWidth1 : 150,
	txtWidth2 : 150,
	txtWidth3 : 150,
	txtWidth4 : 150,
	funChoose : null,//选择确定
	multiSelect : false,//默认单选，true为多选

	
	initComponent:function(){
	    var me = this;

		me.items = [ me.createList() ];
	        
	    Ext.apply(this,{
	        width:1000,
	        height:460,
	    	layout : 'fit',
	        buttons : [{
				xtype: 'label',
				text: ''
			}, {
			    text : '确定',
			    iconCls : 'acceptIcon',
			    handler : function() {
			    	if (me.funChoose != null){
					    var dwModels = me.gridList.getSelectionModel().getSelection();
					    if (dwModels.length == 0) {
				            Ext.MessageBox.alert('提示', '请先选中定位记录！');
				            return;
						}
					    if (!me.multiSelect){
						    me.funChoose(dwModels[0].data, function(){
					        	me.close();
						    });
					    }else{
					    	var list = new Array();
					    	for ( var i = 0; i < dwModels.length; i++) {
								list.push(dwModels[i].data);
							}
					    	me.funChoose(list, function(){
					        	me.close();
						    });
					    }
			    	}
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    
	    this.callParent(arguments);
	},
	
	createList : function() {
	    var me = this;

	    var queryText1 = null;
	    if (me.isText1){
		    queryText1 = Ext.create("system.widget.FsdTextField" , {
				emptyText : '请输入' + me.txtLabel1,
				width : me.txtWidth1
			});
	    }

	    var queryText2 = null;
	    if (me.isText2){
			queryText2 = Ext.create("system.widget.FsdTextField" , {
				emptyText : '请输入' + me.txtLabel2,
				width : me.txtWidth2
			});
	    }

	    var queryText3 = null;
	    if (me.isText3){
			queryText3 = Ext.create("system.widget.FsdTextField" , {
				emptyText : '请输入' + me.txtLabel3,
				width : me.txtWidth3
			});
	    }

	    var queryText4 = null;
	    if (me.isText4){
			queryText4 = Ext.create("system.widget.FsdTextField" , {
				emptyText : '请输入' + me.txtLabel4,
				width : me.txtWidth4
			});
	    }

	    var gridtool = 'system.widget.FsdGridPagePanel';
	    if (me.gridType == 'list'){
	    	gridtool = 'system.widget.FsdGridPanel'
	    }
		// 创建表格
	    me.gridList = Ext.create(gridtool, {
			column : me.column, // 显示列
			model : me.model,
			baseUrl : me.baseUrl,
			border : false,
			tbar : [queryText1, queryText2, queryText3, queryText4 ,{
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.gridList.getStore().reload();
				}
			}],
			listeners : {
				itemdblclick : function(obj, record, item, index, e, eOpts ){
					if(me.funChoose != null){
						me.funChoose(record.data, function(){
				        	me.close();
					    });
					}
				}
			}
		});
	    me.gridList.getStore().on('beforeload', function(s) {
	    	var txt1 = null;
			if (me.isText1){
				txt1 = queryText1.getValue();
			}
			var txt2 = null;
			if (me.isText2){
				txt2 =  queryText2.getValue();
			}
			var txt3 = null;
			if (me.isText3){
				txt3 =  queryText3.getValue();
			}
			var txt4 = null;
			if (me.isText4){
				txt4 =  queryText4.getValue();
			}
			var queryObj = {};
			if (me.funQuery != null)
				queryObj = me.funQuery(txt1, txt2, txt3, txt4);				
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(queryObj)});
		});
	    if (!me.multiSelect)
	    	me.gridList.selModel.setSelectionMode('SINGLE');

		return me.gridList;
	}


});