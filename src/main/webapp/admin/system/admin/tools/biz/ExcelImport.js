﻿/**
 * Excel导入界面界面
 * @author lumingbao
 */
Ext.define('system.admin.tools.biz.ExcelImport', {
	extend : 'Ext.window.Window',
	requires : ['system.widget.FsdTreeComboBox'],
	oper : Ext.create('system.admin.tools.biz.ExcelImportOper'),
	readUrl : '',//解析Excel地址
	postUrl : '',//导入处理的地址
	grid : null,
	initComponent:function(){
	    var me = this;
	    
	    var sheet = Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '页数',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 550,
	    	name: 'sheet',
	    	maxLength: 3,
	    	minValue: 1,
	    	value :'1',
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank: false,
	    	blankText: '请输入页数',
	    	maxLengthText: '页数不能超过3个字符',
	    	colspan : 2
	    });
	    
	    var ksh = Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '开始行',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 275,
	    	name: 'ksh',
	    	maxLength: 3,
	    	minValue: 1,
	    	value :'1',
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank: false,
	    	blankText: '请输入开始行',
	    	maxLengthText: '开始行不能超过3个字符'
	    });
	    
	    var ksl = Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '开始列',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 275,
	    	name: 'ksl',
	    	maxLength: 3,
	    	minValue: 1,
	    	value :'1',
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank: false,
	    	blankText: '请输入开始列',
	    	maxLengthText: '开始列不能超过3个字符'
	    });
	    
	    var hs = Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '行数',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 275,
	    	name: 'hs',
	    	maxLength: 10,
	    	minValue: 1,
	    	value :'1',
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank: false,
	    	blankText: '请输入行数',
	    	maxLengthText: '行数不能超过3个字符'
	    });
	    
	    var ls = Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '列数',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 275,
	    	name: 'ls',
	    	maxLength: 3,
	    	minValue: 1,
	    	value :'1',
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank: false,
	    	blankText: '请输入列数',
	    	maxLengthText: '列数不能超过3个字符'
	    });
	    
	    
	    var excel = Ext.create('system.widget.FsdTextFileManage',{
	    	 width : 550,
	    	 zFieldLabel: 'Excel文件',
	    	 labelAlign:'right',
	    	 zLabelWidth:70,
	    	 zName : 'excel',
	    	 zIsUpButton : true,
	    	 zFileType : 'file',
	    	 zFileAutoName : false,
	    	 zAllowBlank : false,
	    	 ZBlankText : '请上传Excel文件',
	    	 zFileUpPath : me.oper.util.getFilePath(),
	    	 colspan : 2
	    });
	    
	    var formpanel = new Ext.form.Panel({
			layout: 'absolute',
			border : 0,
			items: [
				{
					xtype: 'fieldset',
					x: 4,
					y: 4,
					width: 580,
					height: 130,
					title: '基本',
					layout: {
				        type: 'table', 
				        columns: 2, //每行有几列 
				        //默认样式 
				        tableAttrs: {
				            style: "width:100;height:40;" 
				        }
				    },
					items: [sheet, ksh, ksl, hs, ls, excel]
				}
			]
		});
	        
	    Ext.apply(this,{
	        width:600,
	        height:200,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[formpanel],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
	        	name : 'btnsave',
			    text : '解析',
			    iconCls : 'acceptIcon',
			    zIsSpace : true,
			    handler : function() {
			    	if(me.readUrl != null && me.readUrl !=""){
			    		me.oper.formSubmit(me.grid, formpanel , me.readUrl);
			    	}else{
			    		Ext.MessageBox.alert('提示', '缺少解析地址参数！');
			    	}
			    }
		    },{
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
	        	name : 'btnsave',
			    text : '导入',
			    iconCls : 'acceptIcon',
			    zIsSpace : true,
			    handler : function() {
			    	if(me.postUrl != null && me.postUrl !=""){
			    		me.oper.formSubmit(me.grid, formpanel, me.postUrl);
			    	}else{
			    		Ext.MessageBox.alert('提示', '缺少导入地址参数！');
			    	}
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});