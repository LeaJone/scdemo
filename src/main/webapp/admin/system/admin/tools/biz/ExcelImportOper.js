/**
 * 建设用地审批界面操作类
 * @author lumingbao
 */

Ext.define('system.admin.tools.biz.ExcelImportOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 表单提交
     */
     formSubmit : function(grid, formpanel, url){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit(url , formpanel.form , '正在提交数据,请稍候.....',
			function(form, action){
			    formpanel.up('window').close();
				Ext.MessageBox.alert('提示', '保存成功！');
				if(grid != null){
					grid.getStore().reload();
				}
			});
        }
     }
});