﻿/**
 * 顺序号选择界面
 * @author lw
 */
Ext.define('system.admin.tools.biz.SortOperate', {
	extend : 'Ext.window.Window',

	model : '',
	column : '',
	baseUrl : '',
	funQuery : null,//查询前事件
	funChoose : null,//选择确定
	submitUrl : null,
	models : [],
	util : null,
	
	funAccept : function(data) {
	    var me = this;
    	if (me.submitUrl != null && me.submitUrl != ''){
		    var dwID = data.id;
	    	var xzIDs = new Array();
		    var xzModels = me.gridChoose.getStore().getRange();
		    for ( var i = 0; i < xzModels.length; i++) {
		    	xzIDs.push(xzModels[i].data.id);
		    }
			var pram = {ids : xzIDs, id : dwID};
	        me.util.ExtAjaxRequest(
        		me.submitUrl, 
        		{jsonData : Ext.encode(pram)},
		        function(response, options){
		        	me.close();
		        	if (me.funChoose != null)
		        		me.funChoose();
		        }, 
		        null, me
			);
    	}
    },
	
	initComponent:function(){
	    var me = this;
	    
	    me.items = [ 
	                 me.createList(),
	                 me.createChoose()
	                 ];
	        
	    Ext.apply(this,{
	        width:800,
	        height:500,
			layout: 'border',
	        buttons : [{
				xtype: 'label',
				text: '选择定位记录，“插入记录”会插入到指定记录前，并修改后续所有记录的顺序号！'
			}, {
			    text : '确定',
			    iconCls : 'acceptIcon',
			    handler : function(){
			    	var dwModels = me.gridList.getSelectionModel().getSelection();
				    if (dwModels.length == 0) {
			            Ext.MessageBox.alert('提示', '请先选中定位记录！');
			            return;
					}
				    me.funAccept(dwModels[0].data);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    
	    this.callParent(arguments);
	},
	
	createChoose : function() {
	    var me = this;

		// 创建表格
		me.gridChoose = Ext.create("system.widget.FsdGridPanel", {
			column : me.column, // 显示列
			model : me.model,
			border : false,
			selType : 'rowmodel',
			zAutoLoad : false
		});
		
		me.gridChoose.getStore().add(me.models);
		
		var contentpanel = Ext.create("Ext.panel.Panel", {
			frame:true,
			split: true,
			items: [me.gridChoose],
			region : 'west', // 设置方位
			layout: 'fit',
			title: '插入记录',
			width: 350
		});

		return contentpanel;
	},
	
	createList : function() {
	    var me = this;

		// 创建表格
	    me.gridList = Ext.create("system.widget.FsdGridPagePanel", {
			column : me.column, // 显示列
			model : me.model,
			baseUrl : me.baseUrl,
			border : false,
			multiSelect : true,
			listeners : {
				itemdblclick : function(obj, record, item, index, e, eOpts ){
					me.funAccept(record.data);
				}
			}
		});
	    me.gridList.getStore().on('beforeload', function(s) {
	    	var queryObj = {};
			if (me.funQuery != null)
				queryObj = me.funQuery();				
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(queryObj)});
		});
	    me.gridList.selModel.setSelectionMode('SINGLE');
		
		var contentpanel = Ext.create("Ext.panel.Panel", {
			frame:true,
			split: true,
			items: [me.gridList],
			region: 'east',
			layout: 'fit',
			title: '选择定位记录',
			width: 435
		});

		return contentpanel;
	}
});