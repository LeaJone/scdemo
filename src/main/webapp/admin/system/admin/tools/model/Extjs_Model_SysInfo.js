﻿/**
 *sysinfo Model
 *@author CodeSystem
 *文件名     Extjs_Model_SysInfo
 *Model名    model_sysinfo
 */

Ext.define('model_sysinfo', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'key',
            type : 'string'
        }
        ,
        {
            name : 'value',
            type : 'string'
        }
    ]
});
