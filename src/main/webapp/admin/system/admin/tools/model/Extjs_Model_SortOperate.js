﻿/**
 *b_wordkeybad Model
 *@author CodeSystem
 *文件名     Extjs_Model_SortOperate
 *Model名    model_sortoperate
 */

Ext.define('model_sortoperate', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'name',
            type : 'string'
        }
        ,
        {
            name : 'title',
            type : 'string'
        }
        ,
        {
            name : 'mc',
            type : 'string'
        }
        ,
        {
            name : 'sort',
            type : 'string'
        }
        ,
        {
            name : 'sxh',
            type : 'string'
        }
    ]
});
