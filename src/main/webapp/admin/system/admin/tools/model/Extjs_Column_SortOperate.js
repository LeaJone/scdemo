﻿/**
 *b_wordkeybad Column
 *@author CodeSystem
 *文件名     Extjs_Column_SortOperate
 *Columns名  column_b_wordkeybad
 */

Ext.define('column_name', {
    columns: [
		  {
		      xtype : 'rownumberer',
		      text : 'NO',
		      align : 'center',
		      width : 30
		  }
		  ,
        {
            header : '名称',
            dataIndex : 'name',
            flex : 4
        }
          ,
          {
              header : '顺序号',
              dataIndex : 'sort',
              align : 'right',
		      width : 50
          }
    ]
});

Ext.define('column_title', {
    columns: [
          {
              xtype : 'rownumberer',
              text : 'NO',
		      align : 'center',
              width : 30
          }
          ,
        {
            header : '标题',
            dataIndex : 'title',
            flex : 4
        }
          ,
          {
              header : '顺序号',
              dataIndex : 'sort',
              align : 'right',
		      width : 50
          }
    ]
});

Ext.define('column_mc', {
    columns: [
		  {
		      xtype : 'rownumberer',
		      text : 'NO',
		      align : 'center',
		      width : 30
		  }
		  ,
        {
            header : '名称',
            dataIndex : 'mc',
            flex : 4
        }
          ,
          {
              header : '顺序号',
              dataIndex : 'sxh',
              align : 'right',
		      width : 50
          }
    ]
});
