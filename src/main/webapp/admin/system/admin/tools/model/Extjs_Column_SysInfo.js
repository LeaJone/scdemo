﻿/**
 *sysinfo Column
 *@author CodeSystem
 *文件名     Extjs_Column_SysInfo
 *Columns名  column_sysinfo
 */

Ext.define('column_sysinfo', {
    columns: [
        {
            header : '键',
            dataIndex : 'key',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '值',
            dataIndex : 'value',
            sortable : true,
            flex : 1
        }
    ]
});
