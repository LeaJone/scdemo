/**
 * 密码初始化操作类
 * @author fsd
 * @since 4014-05-27
 */
Ext.define('system.admin.js.initOper', {

	util : Ext.create('system.util.util'),
	
	/**
     * 修改密码表单提交
     */
     pwdEditFormSubmit : function(formpanel){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('A_Employee/pwdedit_Employee.do' , formpanel.form , '正在提交数据,请稍候.....',
			function(form, action){
			    formpanel.up('window').close();
				location.reload();
			});
        }
     }
});