﻿/**
 * 我的工作台
 * 
 * @author fsd
 * @since 4014-05-27
 */ 
Ext.define('system.admin.js.home', {
	extend : 'Ext.panel.Panel',
	requires : [ 'system.widget.FsdButton'],
	header : false,
	border : false,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.items = [ me.createHome() ];
		me.callParent();
	},
	
	createHome : function() {
	    var me = this;
        
	    // var dbsx = Ext.create('system.abasis.systemmenupopup.biz.SystemMenuPopupPanel', {
	    // 	region: 'north',            //子元素的方位：north、west、east、center、south
		// 	split: true,
		// 	layout: 'fit',
		// 	border : 0,
		// 	height : '50%'
	    // });

		var dbsx = Ext.create('Ext.panel.Panel', {
			region: 'north',
			split: true,
			layout: 'fit',
			border : 0,
			height : '50%',
			html: "<iframe frameborder='0' width='100%' height='100%' src='/admin/console.htm'></iframe>"
		});
		
	    var tzpanel = Ext.create('system.abasis.notify.biz.NotifyPanel', {
	    	region : "center", // 设置方位
	    	width: '45%',
			split: true
	    });
	    
//	    var fwqxx = Ext.create('system.admin.tools.biz.SysInfoPanel', {
//	    	region : "center" ,
//			split: true,
//			layout: 'fit',
//			title: '服务器信息'
//	    });
	    	    
	    var homePanel = Ext.create('Ext.Panel', {
	        border : 0,
	        border : false,
	        layout: 'border',
	        defaults: {
	            split: true//是否有分割线
	        },
	        items: [dbsx, tzpanel]
	    });
	    
        return homePanel;
	}
});