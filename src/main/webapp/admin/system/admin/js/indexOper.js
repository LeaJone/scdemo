/**
 * 首页操作类
 * @author fsd
 * @since 4014-05-27
 */
Ext.define('system.admin.js.indexOper', {

	util : Ext.create('system.util.util'),
	
	/**
	 * 加载右侧系统菜单
	 */
	LoadMenuTree : function(menuanel){
	    var me = this;
	    me.util.ExtAjaxRequest('Common/user_menu.do' , {} ,
	    function(response, options, respText){
	    	if(respText.data != ""){
                var data = respText.data;
                for (var i = 0; i < data.length; i++) {
                    // var treepanel = Ext.create("Ext.tree.Panel", {
                    //     rootVisible: false,
                    //     border:false,
                    //     root: {
                    //         text: 'root',
                    //         expanded: true,
                    //     }
                    // });
					//
                    // /**
                    //  * 点击左边tree 的叶子节点， 动态添加右边的tab页
                    //  */
                    // treepanel.on('itemclick', function(view,record,item,index,e){
                    //     if(record.raw.leaf){
                    //         if(record.raw.isDialog == "dialog"){
                    //             var form = '';
                    //             var param = '';
                    //             if (record.raw.module.indexOf('?') != -1){
                    //                 var array = record.raw.module.split('?');
                    //                 form = array[0];
                    //                 param = array[1];
                    //             }else{
                    //                 form = record.raw.module;
                    //             }
                    //             var model = Ext.create(form);
                    //             //model.setParamObj(param);
                    //             //model.setIconPic(record.raw.icon);
                    //             me.util.openWin(record.raw.text, model, record.raw.icon, true);
                    //         }else{
                    //             var contentTable = Ext.getCmp('mainindextabpanel');
                    //             var tabitems = contentTable.items;
                    //             for(var i=0; i < tabitems.length; i++){
                    //                 var c=tabitems.get(i).id;
                    //                 if(c == record.raw.id){
                    //                     contentTable.setActiveTab(i);
                    //                     return;
                    //                 }
                    //             }
					//
                    //             var form = '';
                    //             var param = '';
                    //             if (record.raw.module.indexOf('?') != -1){
                    //                 var array = record.raw.module.split('?');
                    //                 form = array[0];
                    //                 param = array[1];
                    //             }else{
                    //                 form = record.raw.module;
                    //             }
                    //             var model = Ext.create(form);
                    //             model.setParamObj(param);
                    //             model.setIconPic(record.raw.icon);
                    //             me.util.addTab(record.raw.id , record.raw.text , model , record.raw.icon);
                    //         }
					//
                    //     }
                    // });
					//
                    // treepanel.getRootNode().appendChild(data[i].children);

                    var items = Ext.create('Ext.panel.Panel', {
                        header:{
                            cls:'x-panel-header-new'
                        },
						minHeight:200,
                        title: "<span style='color:#ffffff;'>"+ data[i].text +"</span>",
                        autoScroll: true
                    });

					var children = data[i].children;

                    for (var j = 0; j < children.length; j++) {
                        var itemButton = Ext.create('Ext.Button', {
                            text : "<span style='color:#565656;margin-left: 20px;font-weight: normal;'>"+ children[j].text +"</span>",
                            iconAlign : 'left',
							textAlign: 'left',
                            scale : 'medium',
                            width : '100%',
                            height: 35,
                            style: 'padding-top:7px;margin-top:5px;background:#f1f1f1;border:none;',
                            iconCls : "fa "+ children[j].icon +" menuIcon",
							fsddata:children[j],
                            handler : function(button) {
                                if(button.fsddata.leaf){
                                	if(button.fsddata.isDialog == "dialog"){
                                		var form = '';
                                		var param = '';
                                		if (button.fsddata.module.indexOf('?') != -1){
                                			var array = button.fsddata.module.split('?');
                                			form = array[0];
                                			param = array[1];
                                		}else{
                                			form = button.fsddata.module;
                                		}
                                		var model = Ext.create(form);
                                		//model.setParamObj(param);
										//model.setIconPic(button.fsddata.icon);
										me.util.openWin(button.fsddata.text, model, button.fsddata.icon, true);
                                	}else{
                                		var contentTable = Ext.getCmp('mainindextabpanel');
                                		var tabitems = contentTable.items;
                                		for(var i=0; i < tabitems.length; i++){
                                			var c=tabitems.get(i).id;
                                			if(c == button.fsddata.id){
                                				contentTable.setActiveTab(i);
                                				return;
                                			}
                                		}
                                 		var form = '';
                                		var param = '';
                                		if (button.fsddata.module.indexOf('?') != -1){
                                			var array = button.fsddata.module.split('?');
                                			form = array[0];
                                			param = array[1];
                                		}else{
                                			form = button.fsddata.module;
                                		}
                                		var model = Ext.create(form);
                                		model.setParamObj(param);
                                		model.setIconPic(button.fsddata.icon);
                                		me.util.addTab(button.fsddata.id , button.fsddata.text , model , button.fsddata.icon);
                                	}
                                 }
                            }
                        });
                        items.add(itemButton);
					}

                    menuanel.add(items);
				}
	    	}
	    },
	    function(response, options){
	    }, menuanel);
	},

	/**
	 * 加载系统标题图标
	 */
	LoadLogoTitle : function(logoimgpanel){
	    var me = this;
	    var loadMarsk = new Ext.LoadMask(logoimgpanel, {
	    	msg:'加载菜单，请稍候...',    
	    	removeMask:true // 完成后移除    
	    });
	    loadMarsk.show();  //显示
	    me.util.ExtAjaxRequest('Sys_SystemParameters/get_systemlogotitle.do', null,
	    function(response, options, respText){
	    	logoimgpanel.el.setHTML("<img height='60' src='resources/project/images/index_banner.png' />");
	    	loadMarsk.hide();  //隐藏
	    },
	    function(response, options){
	    	loadMarsk.hide();  //隐藏
	    });
	},
	
	/**
	 * 加载全局参数信息
	 */
	loadAllInfo : function(success , failure){
		var myMask = new Ext.LoadMask(Ext.getBody(),{msg:"系统初始化，请稍后........."});
	    myMask.show();
	    Ext.Ajax.request({
	    	url:'Common/all_getAllInfo.do',
	    	method:"POST",
	    	success:function(response, options){
	    		if (myMask != undefined){
	    			myMask.hide();
	    		}
	    		if(success != null){
	    			success(response, options);
	    		}
	    	}, 
	    	failure:function(response, options){
	    		if (myMask != undefined){
	    			myMask.hide();
	    		}
	    		if(failure != null){
	    			failure(response, options);
	    		}
	    	}  
	    });
	},
	
	/**
	 * 生成静态页
	 */
	makeHtml : function(companyid, win){
	    var me = this;
	    me.util.ExtAjaxRequest('Common/makeHtml.do' , {jsonData : Ext.encode({companyid : companyid})} ,
	    function(response, options, respText){
	    	if(respText.data != ""){
	    		Ext.Msg.alert('提示', '生成成功！');
	    	}
	    }, null, win);
	},

    /**
     * 清理缓存
     */
    cleanCache : function(companyid, win){
        var me = this;
        me.util.ExtAjaxRequest('Common/cleanCache.do' , {jsonData : Ext.encode({companyid : companyid})} ,
            function(response, options, respText){
                if(respText.data != ""){
                    Ext.Msg.alert('提示', '清理成功！');
                }
            }, null, win);
    },
	
	/**
	 * 系统解锁
	 */
	Unlock : function (form, success){
		var me = this;
		if (form.isValid()) {
			me.util.ExtFormSubmit('Main/MenuItemOper.aspx?action=load' , form , '正在处理,请稍候.....',
			function(form, action){
				if(success != null){
				    success(form, action);
				}
			},
			function (form, action){
				Ext.MessageBox.alert('异常', action.result.msg);
			});
		}
	},
	
	/**
	 * 注销退出
	 */
	LoginOut : function(){
		var me = this;
	    me.util.ExtAjaxRequest('logout.do' , {} ,
	    function(response, options, respText){
            window.location.href = 'login.html';
	    });
	},
	
	/**
	 * 提交用户布局设置
	 */
	saveUserLayout : function(layout){
		
	},
	
	/**
	 * 提交用户皮肤设置
	 */
	saveUserTheme : function(theme){
		
	},

	/**
	 * 加载用户未读消息
	 */
	LoadUnreadMessage : function(button){
		var me = this;
		me.util.ExtAjaxRequest('z_email/load_EmailNotreadByid.do' , {} ,
			function(response, options, respText){
				button.setText("消息("+ respText.data +")");
			},
			function(response, options){
			}, null);
	},

	/**
	 * 打开绑定微信窗口
	 */
	openWechatWindow: function () {
		var me = this;
		var user = me.util.getParams("user");
		if(user.iswechatbind == null || user.iswechatbind == "" || user.iswechatbind == "false"){
			var pram = {id : user.id};
			var param = {jsonData : Ext.encode(pram)};
			me.util.ExtAjaxRequest("e_userinfo/get_weichatQRcode.do", param,
				function(response, options, respText){
					var win = Ext.create('system.abasis.employee.biz.UserWeChat');
					win.setTitle('请用微信扫码二维码，绑定平台微信公众平台。');
					win.modal = true;
					win.setImageUrl(respText.data);
					win.show();
				}, null, me.form);
		}
	}
});