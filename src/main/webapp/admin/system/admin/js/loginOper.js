/**
 * 登陆页面操作类
 * @author fsd
 * @since 4014-05-27
 */
Ext.define('system.admin.js.loginOper', {
	
	util : Ext.create('system.util.util'),
	
	login : function(form,checkcode){
		var me = this;
		if (form.isValid()) {
			me.util.ExtFormSubmit('login.do' , form , '正在验证您的身份,请稍候.....',
			function(form, action){
				me.util.setCookie("fsd.login.account", form.findField('account').value, 240);//设置cookie记住用户名
				me.util.setCookie("fsd.lockflag", '1', 240);//设置cookie标注系统为未锁定
				window.location.href = 'index.htm';
			},
			function (form, action){
				checkcode.loadCodeImg();//刷新验证码
				Ext.MessageBox.alert(action.result.title, action.result.message);
			});
		}
	},

	changeSite : function(form){
		var me = this;
		Ext.MessageBox.show({
			title : '询问',
			msg : '您确定要切换到该站点吗?',
			width : 250,
			buttons : Ext.MessageBox.YESNO,
			icon : Ext.MessageBox.QUESTION,
			fn : function(btn) {
				if (btn == 'yes') {
                	me.util.ExtFormSubmit('changeSiteInfo.do' , form , '正在切换站点,请稍候.....',
                	function(form, action){
        				window.location.href = 'index.htm';
        			},
        			function (form, action){
        				Ext.MessageBox.alert('提示', '站点切换失败，请联系管理员解决问题！');
        			});
                }
            }
        });
	}
});