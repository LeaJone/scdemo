﻿/**
 * 站点切换编辑界面
 * @author lhl
 */
Ext.define('system.admin.js.changeSite', {
	extend : 'Ext.window.Window',
	requires : [ 'system.abasis.company.model.Extjs_A_changeSite'],
	grid : null,
	oper : Ext.create('system.admin.js.loginOper'),
	initComponent:function(){
	    var me = this;
	    
	    var zdmc = Ext.create('system.widget.FsdTextGrid', {
	    	zFieldLabel : '请选择要切换的站点信息', 
	    	zLabelWidth : 150,
	    	width : 450,
		    zName : 'name',
		    zColumn : 'column_a_changeSite', // 显示列
		    zModel : 'model_a_changeSite',
		    zBaseUrl : 'A_Company/load_alldatabysitechange.do',
		    zAllowBlank : false,
		    zGridType : 'list',
		    zIsText1 : true,
		    zTxtLabel1 : '站点名称',	
		    zGridWidth : 500,
		    zFunChoose : function(data){
		    	zdmc.setValue(data.name);
		    	zdid.setValue(data.id);
		    },
		    zFunQuery : function(txt1, txt2, txt3, txt4){
		    	return {name : txt1};
		    }
	    });
	    var zdid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '站点ID',
	    	name: 'id'
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [ zdmc, zdid ]
        });
	        
	    Ext.apply(this,{
	        width:550,
	        height:120,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'zjqhbj',
	        	name : 'btnsave',
			    text : '确定',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.changeSite(form);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    }),
	    this.callParent(arguments);
	}
});