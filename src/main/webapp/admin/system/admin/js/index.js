/**
 * 首页布局
 * @author fsd
 * @since 2014-05-27
 */
Ext.onReady(function() {
	
    var util = Ext.create('system.util.util');
	var oper = Ext.create('system.admin.js.indexOper');

    oper.loadAllInfo(
    	function(response, options){
    		var respText = Ext.JSON.decode(response.responseText);
    		
    		util.setParams("user" , respText.user);	
    		util.setParams("company" , respText.company);
    		util.setParams("popdom" , respText.popedom);

    		CreateAll();
    	},
    	function(response, options){
    		
    });

    function CreateAll(){

        /**
         * 左边的菜单面板
         */
        var menupanel = Ext.create('Ext.panel.Panel', {
            title : "系统导航",
            region : "west", // 设置方位
            split : true,//拖动
            collapsible : true,//折叠
            width: 250,
			autoScroll : true,
            layout: {
                type: 'accordion',
                hideCollapseTool: true,
                titleCollapse: true,//点击title条 就触发折叠/扩展
                animate: true,
                activeOnTop: false //是否扩展面板总是显示在最顶端
            }
        });
    	

    	var homepanel = Ext.create('system.admin.js.home');

    	/**
    	 * 中间的tabpanel
    	 */
    	var contentTable = Ext.create('Ext.tab.Panel', {
    		id : 'mainindextabpanel',
    		activeTab : 0, // 指定默认的活动tab
    		enableTabScroll : true, // 选项卡过多时，允许滚动
    		deferredRender : false,
    		defaults : {
    			autoScroll : true
    		},
    		plugins : Ext.create('Ext.ux.TabCloseMenu', {
    			closeTabText : '关闭当前',
    			closeOthersTabsText : '关闭其他',
    			closeAllTabsText : '关闭所有'
    		}),
    		items : [ {
    			id : "syjs",
    			title : '我的工作台',
    			iconCls : 'fa fa-university tabIcon',
    			layout : 'fit',
    			border : false,
    			items: [homepanel]
    		}],
    		listeners:{
                tabchange:function(tp,p){ 
                	tablePanelChangeListeners(p);
                } 
            }
    	});
    	
    	/**
    	 * 中间的面板
    	 */
    	var contentPanel = Ext.create('Ext.panel.Panel' ,{
    		id : 'mainindexpanel',
    		title: '综合信息平台',
    		header : false,
    		border : false,
    		region : "center" ,
    		layout : 'fit',		 
    		items : [contentTable]
    	});
    	
    	/**
    	 * 头部面板
    	 */
    	var titlePanel = Ext.create('Ext.panel.Panel' ,{
    		header : false,
    		border : false,
    		region : "north" ,
    		height : 60,
            bodyStyle: 'background:#3992d1;',
    		collapsible : true,
    		contentEl:'north'
    	});
    	
    	var tbtext = system.UtilStatic.chooseObjIDText;

    	/**
    	 * 底部面板
    	 */
    	var bottomPanel = Ext.create('Ext.Toolbar' ,{
    		region : "south" ,
    		border : false,
    		height : 30,
            style: 'background:#3992d1;',
    		items: [
    				{
    					xtype: 'label',
    					id: 'company_label',
						style : 'color:#ffffff;',
    					text: '单位：' + util.getParams("user").companyname
    				},{
    					xtype: 'label',
    					id: 'branch_label',
                    	style : 'color:#ffffff;',
    					text: '　　部门：' + util.getParams("user").branchname
    				},{
    					xtype: 'label',
    					id: 'loginuser_label',
                    	style : 'color:#ffffff;',
    					text: '　　欢迎您：' + util.getParams("user").realname
    				}, {
    					xtype: 'label',
                    	style : 'color:#ffffff;',
    					text: '　　　　　　　　　　'
    				}, 
    				tbtext, '->',
    		        {
    					xtype: 'label',
    					id: 'nowtime_label',
                        style : 'color:#ffffff;',
    					text: ''
    				}
    		]
    	});
    		
    	/**
    	 * 总布局
    	 */
    	var allview = new Ext.Viewport({
    		title : "Viewport",
    		layout : "border", // 这里是（border）边界布局
    		border : false,
    		defaults : {
    			frame : true
    		},
    		items : [ menupanel,titlePanel, contentPanel, bottomPanel]
    	});

    	/**
    	 * 生成静态页
    	 */
        var makeHtml = Ext.create('Ext.menu.Item' ,{
			text : '生成静态页',
			icon : 'resources/admin/icon/home.png',
			handler : function() {
				Ext.MessageBox.show({
	        		 title : '询问',
	        		 msg : '您确定要生成静态页吗?',
	 			     width : 250,
	 				 buttons : Ext.MessageBox.YESNO,
	                 icon : Ext.MessageBox.QUESTION,
	                 fn : function(btn) {
	                     if (btn == 'yes') {
	                    	 oper.makeHtml(util.getParams("user").companyid, allview);
	                     }
	                 }
	             });
			}
		});

        /**
         * 清理缓存
         */
        var cleanCache = Ext.create('Ext.menu.Item' ,{
            text : '清空缓存',
            icon : 'resources/admin/icon/home.png',
            handler : function() {
                Ext.MessageBox.show({
                    title : '询问',
                    msg : '您确定要清空缓存吗?',
                    width : 250,
                    buttons : Ext.MessageBox.YESNO,
                    icon : Ext.MessageBox.QUESTION,
                    fn : function(btn) {
                        if (btn == 'yes') {
                            oper.cleanCache(util.getParams("user").companyid, allview);
                        }
                    }
                });
            }
        });
    	
    	/**
    	 * 首选项菜单
    	 */
    	var mainMenu = new Ext.menu.Menu({
    		id : 'mainMenu',
    		items : [cleanCache]
    	});

    	/**
    	 * 系统标题图片
    	 */
    	var logoImg = new Ext.Component({
    	    width: 800,
    	    height: 45,
    	    margin: 0,
			html: '<img height=\'55\' src=\'resources/project/images/index_banner.png\' />',
//    	    style: {
//    	        color: '#FFFFFF',
//    	        backgroundColor:'#000000'
//    	    },
    		renderTo : 'logoDiv'
    	});

        /**
         * 消息提醒
         */
        var messageButton = new Ext.Button({
            text : '消息(0)',
            iconCls : 'bell2Icon',
            iconAlign : 'left',
            scale : 'medium',
            width : 90,
            height : 40,
            pressed : true,
            renderTo : 'themeDiv',
            handler : function() {
                var model = Ext.create("system.email.inbox.biz.InboxManage");
                util.addTab("sjx" , "收件箱" , model , "fa-envelope-o");
            }
        });
    	
    	/**
    	 * 首选项按钮
    	 */
    	var configButton = new Ext.Button({
    		text : '首选项',
    		iconCls : 'config2Icon',
    		iconAlign : 'left',
    		scale : 'medium',
    		width : 110,
			height : 40,
    		tooltip : '<span style="font-size:12px">个人设置</span>',
    		pressed : true,
    		renderTo : 'configDiv',
    		menu : mainMenu
    	});
    	
    	/**
    	 * 退出按钮
    	 */
    	var closeButton = new Ext.Button({
    		iconCls : 'cancel_48Icon',
    		iconAlign : 'left',
    		scale : 'medium',
    		width : 32,
            height : 40,
    		tooltip : '<span style="font-size:12px">切换用户,安全退出系统</span>',
    		pressed : true,
    		arrowAlign : 'right',
    		renderTo : 'closeDiv',
    		handler : function() {
    			logout();
    		}
    	});

    	/**
    	 * tablePanel切换事件（目的：改变导航的内容）
    	 */
    	var tablePanelChangeListeners = function(object){
    		contentPanel.setTitle("综合信息平台-" + object.title);
    	}

    	oper.LoadMenuTree(menupanel);
    	//oper.LoadLogoTitle(logoImg);
		oper.LoadUnreadMessage(messageButton);
		oper.openWechatWindow();
    }

    /**
	 * 注销退出
	 */
	function logout(){
		Ext.MessageBox.show({
				title : '提示',
				msg : '确认要注销系统,退出登录吗?',
				width : 250,
				buttons : Ext.MessageBox.YESNO,
				icon : Ext.MessageBox.QUESTION,
				fn : function(btn) {
					if (btn == 'yes') {
						Ext.MessageBox.show({
						    title : '请等待',
						    msg : '正在注销...',
						    width : 300,
						    wait : true,
						    waitConfig : {
                                interval : 50
                            }
						});
					    oper.LoginOut();
					}
				}
			});
	}
});

/**
 * 显示系统时钟
 */
function showTime() {
	var date = new Date();
	
	var nian = date.getFullYear(); 
	var yue = date.getMonth()+1;
	var day = date.getDate();
	var xinqi = date.getDay();
	var xq = "";
	switch(xinqi){
	case 0:
		xq = "星期天";
		break;
	case 1:
		xq = "星期一";
		break;
	case 2:
		xq = "星期二";
		break;
	case 3:
		xq = "星期三";
		break;
	case 4:
		xq = "星期四";
		break;
	case 5:
		xq = "星期五";
		break;
	case 6:
		xq = "星期六";
		break;
	default:
	 
	}

	
	var h = date.getHours();
	h = h < 10 ? '0' + h : h;
	var m = date.getMinutes();
	m = m < 10 ? '0' + m : m;
	var s = date.getSeconds();
	s = s < 10 ? '0' + s : s;
	Ext.getCmp('nowtime_label').setText(nian + "年" + yue + "月" + day + "日　" + xq + "　" + h + ":" + m + ":" + s);
}

window.onload = function() {
	setInterval("showTime()", 1000);
}