/**
 * 登陆页面
 * 
 * @author fsd
 * @since 4014-05-27
 */
Ext.onReady(function() {
	
	var util = Ext.create('system.util.util');
	
	var checkcode = Ext.create('system.widget.FsdCaptcha',{
		zFieldLabel : '验证码',
		zName : 'Captcha',
		zFieldStyle:'background: url(resources/admin/icon/lock.png) no-repeat 2px 2px;padding-left:20px;',
		zMaxLength : 20,
        zLabelWidth : 45,
		width : 330,
		imgWidth : 75,
		imgHeight : 22,
		zAllowBlank : false,
        isLoader:true,
        ZBlankText : '验证码不能为空',
        ZMaxLengthText : '验证码的最大长度为20个字符',
        codeUrl: 'captcha-image.do',
        zListeners : {
			specialkey : function(field, e) {
				if (e.getKey() == Ext.EventObject.ENTER) {
					login();
				}
			}
		}
    });
	
	/**
	 * 总布局面板
	 */
	var panel = Ext.create('Ext.panel.Panel', {
		border : false,
		sortable : true,
		items :[{
			border : false,
			html : '<img border="0" width="450" height="85" src="resources/project/images/login_banner.png" />'
		},{
			xtype : 'tabpanel',
			id : 'loginTabs',
			activeTab : 0,
			height : 180,
			items : [{
				title : "身份认证",
				xtype : 'form',
				id : 'loginForm',
				defaults : {
					width : 300
				},
				bodyStyle : 'padding:30px 0px 0px 50px',
				defaultType : 'textfield',
				items : [{
							fieldLabel : '帐&nbsp;&nbsp;&nbsp;号',
							name : 'Loginname',
							id : 'account',
							fieldStyle:'background: url(resources/admin/icon/user.gif) no-repeat 2px 2px;padding-left:20px;',
							blankText : '帐号不能为空,请输入!',
							maxLength : 30,
							labelWidth : 45,
							maxLengthText : '账号的最大长度为30个字符',
							allowBlank : false,
							listeners : {
								specialkey : function(field, e) {
									if (e.getKey() == Ext.EventObject.ENTER) {
										Ext.getCmp('password').focus();
									}
								}
							}
						},
						
						
//						{
//							fieldLabel : '密&nbsp;码',
//							name : 'Password',
//							id : 'password',
//							fieldStyle:'background: url(resources/admin/icon/key.gif) no-repeat 2px 2px;padding-left:20px;',
//							inputType : 'password',
//							blankText : '密码不能为空,请输入!',
//							maxLength : 20,
//							labelWidth : 40,
//							maxLengthText : '密码的最大长度为20个字符',
//							allowBlank : false,
//							listeners : {
//								specialkey : function(field, e) {
//									if (e.getKey() == Ext.EventObject.ENTER) {
//										login();
//									}
//								}
//							}
//						}]
						
						{
							fieldLabel : '密&nbsp;&nbsp;&nbsp;码',
							name : 'Password',
							id : 'password',
							fieldStyle:'background: url(resources/admin/icon/key.gif) no-repeat 2px 2px;padding-left:20px;',
							inputType : 'password',
							blankText : '密码不能为空,请输入!',
							maxLength : 20,
							labelWidth : 45,
							maxLengthText : '密码的最大长度为20个字符',
							allowBlank : false,
							listeners : {
								specialkey : function(field, e) {
									if (e.getKey() == Ext.EventObject.ENTER) {
										checkcode.txtField.focus();
									}
								}
							}
						}, checkcode]
			}]
		}]
	});

	// 清除按钮上下文菜单
	var mainMenu = Ext.create('Ext.menu.Menu', {
		id : 'mainMenu',
		items : [{
			text : '清除记忆',
			iconCls : 'status_awayIcon',
			handler : function() {
				util.clearCookie('fsd.login.account');
				var account = Ext.getCmp('account');
				Ext.getCmp('loginForm').form.reset();
				account.setValue('');
				account.focus();
			}
		}]
	});
	
	/**
	 * 总布局窗口
	 */
	var win = Ext.create('Ext.window.Window', {
		title : "兰州工业学院大学生创新创业信息平台",
		renderTo : Ext.getBody(),
		layout : 'fit',
		width : 460,
		height : 340,
		closeAction : 'hide',
		plain : true,
		modal : false,
		collapsible : false,
		titleCollapse : true,
		maximizable : false,
		draggable : false,
		closable : false,
		resizable : false,
		items : panel,
		buttons : [{
			text : '&nbsp;登录',
			iconCls : 'acceptIcon',
			handler : function() {
					login();
			}
		}, {
			text : '&nbsp;选项',
			iconCls : 'tbar_synchronizeIcon',
			menu : mainMenu
		}]
	});

	win.show();
	
	/**
	 * 填充Cookie里记的用户名
	 */
	setTimeout(function() {
		var account = Ext.getCmp('account');
		var password = Ext.getCmp('password');
		var c_account = util.getCookie('fsd.login.account');
		account.setValue(c_account);
		if (Ext.isEmpty(c_account)) {
			account.focus();
		} else {
			password.focus();
		}
	}, 200);

	/**
	 * 提交登陆请求
	 */
	function login() {
		var oper = Ext.create('system.admin.js.loginOper');
		oper.login(Ext.getCmp('loginForm').form,checkcode);
	}
});