/*
 * @author lumingbao
 * 表格面板
 */
Ext.define('system.widget.FsdGridPagePanel',{
	extend : 'Ext.grid.GridPanel',
	alias: 'fsdgridpagepanel',
	model : '',
	column : '',
	baseUrl : '',
	pageSize : 20,
	stripeRows : true, // 斑马线效果
	loadMask : true, // 显示遮罩和提示功能,即加载Loading……
	forceFit : true, // 自动填满表格
	selType : 'checkboxmodel', // 多选框选择模式
	multiSelect : true,
	
	zAutoLoad : true,
	
	initComponent : function(){
		var me = this;
		var store = Ext.create('Ext.data.Store', {
            autoLoad : me.zAutoLoad,
            pageSize: me.pageSize,
            model: me.model,
            proxy: {
                type: 'ajax',
                url: me.baseUrl,
                timeout: 300000,//300秒
                reader: { root: 'data', totalProperty: 'totalCount' }
            }
        });
        Ext.apply(this, {
        	store : store,
        	columns : Ext.create(me.column).columns,
        	bbar : new Ext.PagingToolbar({
	        	store: store,
	        	displayInfo:true
	        })
        });
        this.callParent(arguments);
        
        //grid注册选择事件，显示ID信息
        system.UtilStatic.setGridChooseObjID(me);
	}
});