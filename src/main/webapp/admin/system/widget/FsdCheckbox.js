/*
 * @author lw
 * 复选框
 */
Ext.define('system.widget.FsdCheckbox',{
	extend : 'Ext.form.Checkbox',
	alias: 'fsdcheckbox',
	popedomCode : null,//权限代码
	initComponent : function(){
		var me = this;
        this.callParent(arguments);
	}
});