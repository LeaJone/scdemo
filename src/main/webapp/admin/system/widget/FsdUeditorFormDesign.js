/*
 * @author lumingbao
 * 百度编辑器表单设计器
 */
Ext.define('system.widget.FsdUeditorFormDesign',{
	extend:Ext.form.FieldContainer,
	mixins: {
        field: Ext.form.field.Field
	},
	alias: 'fsdueditor',
	alternateClassName: 'Ext.form.UEditor',
	ueditorInstance: null,
    initialized: false,
    tempVal : null,//临时内容
    toolbars : null,//个性化工具栏
    formDesign : null,
    initComponent: function () {
        var me = this;
		var required = '<span data-qtip="Required">&nbsp;&nbsp;</span>';
		Ext.apply(this, {
	    	afterLabelTextTpl : required
        });
        
        me.addEvents('initialize', 'change');
        var id = me.id + '-ueditor';
        me.html = '<script id="' + id + '" type="text/plain" name="' + me.name + '"></script>';
        me.callParent(arguments);
        me.initField();
        me.on('render', function () {
            var width = me.width;
            var height = me.height - 60;
            var config = {initialFrameWidth: width, initialFrameHeight: height};
            if(me.toolbars != null){
            	config = {initialFrameWidth: width, initialFrameHeight: height, toolbars: me.toolbars,toolleipi:true,textarea: 'design_content',wordCount:false,elementPathEnabled:false};
            }
            me.ueditorInstance = UE.getEditor(id, config);
            me.ueditorInstance.ready(function () {
                me.initialized = true;
                me.fireEvent('initialize', me);
                me.ueditorInstance.addListener('contentChange', function () {
                    me.fireEvent('change', me);
                });
                if(me.tempVal != null){
                    me.ueditorInstance.setContent(me.tempVal);
                }
            });

            me.createformDesign()
        });
    },
    getValue: function () {
        var me = this,
            value = '';
        if (me.initialized) {
            value = me.ueditorInstance.getContent();
        }
        me.value = value;
        return value;
    },
    setValue: function (value) {
    	var me = this;
    	me.tempVal = value;
        if (value === null || value === undefined) {
            value = '';
        }
        if (me.initialized) {
            me.ueditorInstance.setContent(value);
        }
        return me;
    },
    onDestroy: function () {
    	if(this.ueditorInstance != null){
    		 this.ueditorInstance.destroy();
    	}
    },
    createformDesign: function () {
	    var formEditor = this.ueditorInstance;
        this.formDesign = {
            /*执行控件*/
            exec : function (method) {
                formEditor.execCommand(method);
            },
            /*
             Javascript 解析表单
             template 表单设计器里的Html内容
             fields 字段总数
             */
            parse_form:function(template,fields){
                //正则  radios|checkboxs|select 匹配的边界 |--|  因为当使用 {} 时js报错 (plugins|fieldname|fieldflow)
                var preg =  /(\|-<span(((?!<span).)*plugins=\"(radios|checkboxs|select)\".*?)>(.*?)<\/span>-\||<(img|input|textarea|select).*?(<\/select>|<\/textarea>|\/>))/gi,preg_attr =/(\w+)=\"(.?|.+?)\"/gi,preg_group =/<input.*?\/>/gi;
                if(!fields) fields = 0;

                var template_parse = template,template_data = new Array(),add_fields=new Object(),checkboxs=0;

                var pno = 0;
                template.replace(preg, function(plugin,p1,p2,p3,p4,p5,p6){
                    var parse_attr = new Array(),attr_arr_all = new Object(),name = '', select_dot = '' , is_new=false;
                    var p0 = plugin;
                    var tag = p6 ? p6 : p4;
                    //alert(tag + " \n- t1 - "+p1 +" \n-2- " +p2+" \n-3- " +p3+" \n-4- " +p4+" \n-5- " +p5+" \n-6- " +p6);

                    if(tag == 'radios' || tag == 'checkboxs')
                    {
                        plugin = p2;
                    }else if(tag == 'select')
                    {
                        plugin = plugin.replace('|-','');
                        plugin = plugin.replace('-|','');
                    }
                    plugin.replace(preg_attr, function(str0,attr,val) {
                        if(attr=='name')
                        {
                            if(val=='NEWFIELD')
                            {
                                is_new=true;
                                fields++;
                                val = 'data_'+fields;
                            }
                            name = val;
                        }

                        if(tag=='select' && attr=='value')
                        {
                            if(!attr_arr_all[attr]) attr_arr_all[attr] = '';
                            attr_arr_all[attr] += select_dot + val;
                            select_dot = ',';
                        }else
                        {
                            attr_arr_all[attr] = val;
                        }
                        var oField = new Object();
                        oField[attr] = val;
                        parse_attr.push(oField);
                    })
                    /*alert(JSON.stringify(parse_attr));return;*/
                    if(tag =='checkboxs') /*复选组  多个字段 */
                    {
                        plugin = p0;
                        plugin = plugin.replace('|-','');
                        plugin = plugin.replace('-|','');
                        var name = 'checkboxs_'+checkboxs;
                        attr_arr_all['parse_name'] = name;
                        attr_arr_all['name'] = '';
                        attr_arr_all['value'] = '';

                        attr_arr_all['content'] = '<span plugins="checkboxs"  title="'+attr_arr_all['title']+'">';
                        var dot_name ='', dot_value = '';
                        p5.replace(preg_group, function(parse_group) {
                            var is_new=false,option = new Object();
                            parse_group.replace(preg_attr, function(str0,k,val) {
                                if(k=='name')
                                {
                                    if(val=='NEWFIELD')
                                    {
                                        is_new=true;
                                        fields++;
                                        val = 'data_'+fields;
                                    }

                                    attr_arr_all['name'] += dot_name + val;
                                    dot_name = ',';

                                }
                                else if(k=='value')
                                {
                                    attr_arr_all['value'] += dot_value + val;
                                    dot_value = ',';

                                }
                                option[k] = val;
                            });

                            if(!attr_arr_all['options']) attr_arr_all['options'] = new Array();
                            attr_arr_all['options'].push(option);
                            if(!option['checked']) option['checked'] = '';
                            var checked = option['checked'] ? 'checked="checked"' : '';
                            attr_arr_all['content'] +='<input type="checkbox" name="'+option['name']+'" value="'+option['value']+'" fieldname="' + attr_arr_all['fieldname'] + option['fieldname'] + '" fieldflow="' + attr_arr_all['fieldflow'] + '" '+checked+'/>'+option['value']+'&nbsp;';

                            if(is_new)
                            {
                                var arr = new Object();
                                arr['name'] = option['name'];
                                arr['plugins'] = attr_arr_all['plugins'];
                                arr['fieldname'] = attr_arr_all['fieldname'] + option['fieldname'];
                                arr['fieldflow'] = attr_arr_all['fieldflow'];
                                add_fields[option['name']] = arr;
                            }

                        });
                        attr_arr_all['content'] += '</span>';

                        //parse
                        template = template.replace(plugin,attr_arr_all['content']);
                        template_parse = template_parse.replace(plugin,'{'+name+'}');
                        template_parse = template_parse.replace('{|-','');
                        template_parse = template_parse.replace('-|}','');
                        template_data[pno] = attr_arr_all;
                        checkboxs++;

                    }else if(name)
                    {
                        if(tag =='radios') /*单选组  一个字段*/
                        {
                            plugin = p0;
                            plugin = plugin.replace('|-','');
                            plugin = plugin.replace('-|','');
                            attr_arr_all['value'] = '';
                            attr_arr_all['content'] = '<span plugins="radios" name="'+attr_arr_all['name']+'" title="'+attr_arr_all['title']+'">';
                            var dot='';
                            p5.replace(preg_group, function(parse_group) {
                                var option = new Object();
                                parse_group.replace(preg_attr, function(str0,k,val) {
                                    if(k=='value')
                                    {
                                        attr_arr_all['value'] += dot + val;
                                        dot = ',';
                                    }
                                    option[k] = val;
                                });
                                option['name'] = attr_arr_all['name'];
                                if(!attr_arr_all['options']) attr_arr_all['options'] = new Array();
                                attr_arr_all['options'].push(option);
                                if(!option['checked']) option['checked'] = '';
                                var checked = option['checked'] ? 'checked="checked"' : '';
                                attr_arr_all['content'] +='<input type="radio" name="'+attr_arr_all['name']+'" value="'+option['value']+'"  '+checked+'/>'+option['value']+'&nbsp;';

                            });
                            attr_arr_all['content'] += '</span>';

                        }else
                        {
                            attr_arr_all['content'] = is_new ? plugin.replace(/NEWFIELD/,name) : plugin;
                        }
                        //attr_arr_all['itemid'] = fields;
                        //attr_arr_all['tag'] = tag;
                        template = template.replace(plugin,attr_arr_all['content']);
                        template_parse = template_parse.replace(plugin,'{'+name+'}');
                        template_parse = template_parse.replace('{|-','');
                        template_parse = template_parse.replace('-|}','');
                        if(is_new)
                        {
                            var arr = new Object();
                            arr['name'] = name;
                            arr['plugins'] = attr_arr_all['plugins'];
                            arr['title'] = attr_arr_all['title'];
                            arr['orgtype'] = attr_arr_all['orgtype'];
                            arr['fieldname'] = attr_arr_all['fieldname'];
                            arr['fieldflow'] = attr_arr_all['fieldflow'];
                            add_fields[arr['name']] = arr;
                        }
                        template_data[pno] = attr_arr_all;


                    }
                    pno++;
                })
                var view = template.replace(/{\|-/g,'');
                view = view.replace(/-\|}/g,'');
                var parse_form = new Object({
                    'fields':fields,//总字段数
                    'template':template,//完整html
                    'parse':view,
                    'data':template_data,//控件属性
                    'add_fields':add_fields//新增控件
                });
                return JSON.stringify(parse_form);
            },
            /*type  =  save 保存设计 versions 保存版本  close关闭 */
            fnCheckForm : function ( type ) {
                if(formEditor.queryCommandState( 'source' ))
                    formEditor.execCommand('source');//切换到编辑模式才提交，否则有bug

                if(formEditor.hasContents()){
                    formEditor.sync();/*同步内容*/

                    //获取表单设计器里的内容
                    formeditor = formEditor.getContent();

                    var template_data = new Array();

                    $("<form>" + formeditor + "</form>").find('input,textarea').each(function(){
                        var type = $(this).attr('type');
                        if(type != null && type != "button"){
                            var map = {};
                            map['name'] = $(this).attr('name');
                            map['fieldname'] = $(this).attr('fieldname');
                            map['title'] = $(this).attr('title');
                            map['plugins'] = $(this).attr('plugins');
                            map['fieldflow'] = $(this).attr('fieldflow');
                            map['orgtype'] = $(this).attr('orgtype');
                            template_data.push(map)
                        }
                    });

                    var parse_form = new Object({
                        'template':formeditor,//完整html
                        'parse':formeditor,
                        'data':template_data//控件属性
                    });
                    return JSON.stringify(parse_form);
                } else {
                    Ext.MessageBox.alert("提示", '表单内容不能为空！');
                    return "";
                }
            } ,
            /*预览表单*/
            fnReview : function (){
                if(formEditor.queryCommandState( 'source' ))
                    formEditor.execCommand('source');/*切换到编辑模式才提交，否则部分浏览器有bug*/

                if(formEditor.hasContents()){
                    formEditor.sync();       /*同步内容*/

                    alert("你点击了预览,请自行处理....");
                    return false;
                    //--------------以下仅参考-------------------------------------------------------------------


                    /*设计form的target 然后提交至一个新的窗口进行预览*/
                    document.saveform.target="mywin";
                    window.open('','mywin',"menubar=0,toolbar=0,status=0,resizable=1,left=0,top=0,scrollbars=1,width=" +(screen.availWidth-10) + ",height=" + (screen.availHeight-50) + "\"");

                    document.saveform.action="";
                    document.saveform.submit(); //提交表单
                } else {
                    alert('表单内容不能为空！');
                    return false;
                }
            }
        };
    }
});