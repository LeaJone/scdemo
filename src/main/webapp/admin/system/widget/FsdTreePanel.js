/*
 * @author lumingbao
 * 树面板
 */
Ext.define('system.widget.FsdTreePanel',{
	extend : 'Ext.tree.Panel',
	alias: 'fsdtreepanel',
	baseUrl : null,//数据加载地址，异步加载调用
	params : '',//参数
	rootText : '根',//根节点文本
	rootId : '0',//根节点id
	rootExpanded : true,//根节点是否展开
    rootVisible : true,//显示根节点

    zTreeRoot : 'data',//data异步加载，children一次性加载
    zTreeParams : null,//一次性添加树参数
    zChosseValues : null,//控件所需选中id数组
    
	initComponent : function(){
		var me = this;
		
		me.store = Ext.create('Ext.data.TreeStore', {
        	proxy: {
                    type: 'ajax',
                    url: me.baseUrl,
                    reader: {
                        type: 'json',
                        root : me.zTreeRoot,
                    }
        	},
        	listeners: {
        		beforeload : function(s){ 
                	if (me.zTreeRoot == 'children'){
            			if (me.zTreeParams != null){
                	        var params = s.getProxy().extraParams;
                	        Ext.apply(params,{jsonData : Ext.encode(me.zTreeParams)});
            			}
                    }
                }
            },
        	root: {
	            text: me.rootText,
	            id: me.rootId,
	            expanded: this.rootExpanded//默认展开根节点
	        }
        });
        
        this.callParent(arguments);
	},
	
	setRootId : function(rootId){
		var me = this;
		me.getRootNode().data.id = rootId;
		me.getRootNode().raw.id = rootId;
	},

	findChildNodeChecked : function(node, ids) {  
	    var me = this;
        var childnodes = node.childNodes;  
        Ext.Array.each(childnodes, function(rootnode) {
        	rootnode.raw.checked = false;
        	rootnode.set('checked',false);
        	if (ids != null){
	        	Ext.Array.each(ids, function(id) {
		            if (rootnode.raw.id == id) {
		                rootnode.raw.checked = true;
		            	rootnode.set('checked',true);
		                return false;
		            }
	        	});
        	}
            if (rootnode.childNodes.length > 0){
            	me.findChildNodeChecked(rootnode, ids);
            }
        });
    },
    
    /*
     * 控件加载数据完成后，检查是否有默认选中值信息
     */
    checkChosseValues : function(){
	    var me = this;
    	if (me.zChosseValues != null){
    		me.findChildNodeChecked(me.getRootNode(), me.zChosseValues);
    	}
    },
    
    /*
     * 控件已经加载数据，检查是否有默认选中值信息
     */
    setChosseValues : function(value){
	    var me = this;
	    me.zChosseValues = value;
	    me.findChildNodeChecked(me.getRootNode(), me.zChosseValues);
    }
});