/*
 * @author lumingbao
 * 支持分页和检索的下拉框
 */
Ext.define('system.widget.FsdComboBoxPage',{
	extend : 'Ext.form.ComboBox',
	alias: 'fsdpagecombobox',
	
	id : null, //控件ID
	baseUrl : null,//数据加载地址
    emptyText : null,//默认提示信息
    valueField : null,//后台对应的隐藏域字段
    displayField : null,//后台对应的显示字段
    name : null,//提交到后台的参数名
    hiddenName : null,//隐藏域Name
    
    forceSelection : false,// 值为true时将限定选中的值为列表中的值， 值为false则允许用户将任意文本设置到字段（默认为false）。
    selectOnFocus : true,// 值为 ture时表示字段获取焦点时自动选择字段既有文本(默认为false)。
    editable : true,// 是否编辑
    allowBlank : true,
    blankText : '不允许为空！',
    queryParam : 'jsonData',
    minChars:1,
    
    zAutoLoad : false,//是否自动加载
    zPageSize : 1,
    zCallback : null,//回调函数
	zParams : null,//参数
	initComponent : function(){
		var me = this;
		var blanktext = '必填项，请输入';
		if (me.fieldLabel != null)
			blanktext = blanktext + '“' + me.fieldLabel + "”";
		var required = '<span data-qtip="Required">&nbsp;&nbsp;</span>';
		if (!me.allowBlank)
			required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
		
        Ext.apply(this, {
	    	blankText: blanktext,
	    	afterLabelTextTpl : required,
            fieldLabel : this.fieldLabel,
            emptyText : this.emptyText || '请选择',
            mode : 'remote',
            store : new Ext.data.JsonStore({
                fields : [this.valueField,this.displayField],
                remoteSort : true,
                autoLoad : me.zAutoLoad,
                pageSize : me.zPageSize,
                proxy: {
                    type: 'ajax',
                    method:'post',
                    url : this.baseUrl,
                    reader: {
                        type: 'json',
                        root : "data",
                        totalProperty: 'totalCount'
                    }
                } 
            }),
            valueField : this.valueField,
            displayField : this.displayField,
            name : this.name,
            listeners : {
            	beforequery: function (e) {
                    var combo = e.combo;
//                    if (!e.forceAll) {//第一次打开下拉值为true
                        var input = e.query;
                        var pram = {key : input};
                        if (me.zParams != null){
                        	for (var name in me.zParams) {
								pram[name] = me.zParams[name];
							}
                        }
                        e.query = Ext.encode(pram);
                        combo.expand();
//                    }
                },
                blur : function(field, The, eOpts ){
					if (me.getValue()==me.getRawValue()){
						me.setValue(null);
					}
                },
                specialkey: function(field,e){
                	switch (e.getKey()) {
                	case Ext.EventObject.ENTER:
	        			me.isValid();
	        			if(me.nextSibling() != null){
	        				me.nextSibling().focus();
	        			}
                		break;
					case Ext.EventObject.BACKSPACE:
					case Ext.EventObject.DELETE:
//						me.setValue(null);
						break;
					default:
						break;
					}
	        	},
	        	select : function(combo,record,index){
	        		if(me.hiddenName != null){
	        			this.up('form').getForm().findField(me.hiddenName).setValue(combo.getRawValue());
	        		}
	        		if(me.zCallback != null){
	        			me.zCallback(record[0].data);
	        		}
                }
            }
        });
        this.superclass.initComponent.call(this);
	}
});