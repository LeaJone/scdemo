/*
 * @author lw
 * 按钮组合控件
 */
Ext.define('system.widget.FsdNumberUnit',{
	extend : 'Ext.container.Container',
	alias: 'fsdtextbutton',
	
	/*-----------文本框参数-------------------*/
	zName : '', //文本框控件Name
	zFieldLabel : '', //控件Lable名称
	zLabelAlign : 'right', //控件Lable对齐方式
	zLabelWidth : 60, //控件Lable宽度
	zReadOnly : false, //控件只读
	zAllowBlank : true, //是否必填
	zMaxLength : 20, //输入长度
	zMinValue: 0, //最小值
	zAllowDecimals : true,//是否允许输入小数
	/*-----------按钮参数-------------------*/
	zIsUnit : false, //是否显示单位
	zUnitText : '（元）',
	zUnitWidth : 30,
	zUnitStyle : null,
	
	//获取文本控件值
	zGetValue : function(){
		var me = this;
		return me.numContent.getValue();
	},
	//设置文本控件值
	zSetValue : function(value){
		var me = this;
		return me.numContent.setValue(value);
	},
	//验证控件值
	zIsValid : function(){
		var me = this;
		return me.numContent.isValid();
	},
	//设置文本框是否必填
	zSetAllowBlank : function(isAllowBlank){
		var me = this;
		me.numContent.allowBlank = isAllowBlank;
	},
	
	initComponent : function(){
		var me = this;
		
		var numWidth = me.width;
		if (me.zIsUnit)
			numWidth -= me.zUnitWidth;
		
	    me.numContent = Ext.create('system.widget.FsdNumber',{
	    	name : me.zName,
	    	fieldLabel : me.zFieldLabel,
	    	labelAlign : me.zLabelAlign,
	    	labelWidth : me.zLabelWidth,
	        width : numWidth,
	    	readOnly : me.zReadOnly,
	    	allowBlank : me.zAllowBlank,
	    	maxLength : me.zMaxLength,
	    	minValue: me.zMinValue,
	    	allowDecimals : me.zAllowDecimals
	    });
	    
	    var unitLabel = Ext.create('Ext.form.Label',{
	    	padding : '3 0 0 0',
	    	width : me.zUnitWidth,
	    	style : me.zUnitStyle,
	    	text : me.zUnitText
	    });

		if (!me.zIsUnit)
			unitLabel = null;
	    
        Ext.apply(this, {
        	layout: "hbox",
	    	padding : '0 0 5 0',
        	items:[me.numContent, unitLabel]
        });

        this.callParent(arguments);
	}
});