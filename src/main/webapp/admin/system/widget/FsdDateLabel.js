/*
 * @author lw
 * 日期控件
 */
Ext.define('system.widget.FsdDateLabel',{
	extend : 'Ext.form.field.Date',
	alias: 'fsddatefield',
	
	format : 'Y年m月d日',
	altFormats : 'Ymd',
	submitFormat : 'Ymd',
	invalidText : '无效的日期格式，正确的日期格式是：yyyy年MM月dd日或yyyyMMdd',
	readOnly : true,
	
	initComponent : function(){
		var me = this;
        Ext.apply(this, {
           	fieldStyle:'background:none; border-style: none none solid none;',
	    	afterLabelTextTpl : '<span data-qtip="Required">&nbsp;&nbsp;</span>',
        	listeners: {
	        	specialkey: function(field,e){
	        		if (e.getKey()==Ext.EventObject.ENTER){
	        			me.isValid()
	        			if(me.nextSibling() != null){
	        				me.nextSibling().focus();
	        			}
	        		}  
	        	}  
        	}
        });
        this.callParent(arguments);
	}
});