/*
 * @author lw
 * 文件上传组合控件
 */
Ext.define('system.widget.FsdTextFileManage',{
	extend : 'Ext.container.Container',
	alias: 'fsdfilemanage',
	
	/*-----------文本框参数-------------------*/
	zName : '', //文本框控件Name
	zFieldLabel : '', //控件Lable名称
	zLabelAlign : 'right', //控件Lable对齐方式
	zLabelWidth : 60, //控件Lable宽度
	zIsReadOnly : true, //文本框控件只读
	zAllowBlank : true, //文本框是否必填
	ZBlankText : '请选择文件', //必填提示
	zMaxLength : 200, //文本框输入长度
	/*-----------按钮参数-------------------*/
	zIsShowButton : false, //是否显示查看按钮
	zIsGetButton : false,  //是否显示取图按钮
	zIsUpButton : false,   //是否显示上传按钮
	zIsDownButton : false,   //是否显示下载按钮
	/*-----------控件参数-------------------*/
	zContentObj : null, //取图富文本编辑器对象
	zContentObjFun : null, //取图富文本编辑器对象方法
	zFileUpPath : '', //文件上传根目录
	zFileType : 'image', //文件类型，image图片，video视频，voice语音，file其他文件
	zFileAutoName : true, //文件名称，文件自动修改上传名称
	zManageType : 'upload', //文件操作类型，upload只上传，manage文件管理
	
	//设置文本框是否必填
	zSetAllowBlank : function(isAllowBlank){
		var me = this;
		me.txtFile.allowBlank = isAllowBlank;
	},
	
	initComponent : function(){
		var me = this;
		
		var txtWidth = me.width;
		if (me.zIsShowButton)
			txtWidth -= 40;
		if (me.zIsGetButton)
			txtWidth -= 40;
		if (me.zIsUpButton)
			txtWidth -= 40;
		if (me.zIsDownButton)
			txtWidth -= 40;
		
	    me.txtFile = Ext.create('system.widget.FsdTextField',{
	    	name : me.zName,
	    	fieldLabel : me.zFieldLabel,
	    	labelAlign : me.zLabelAlign,
	    	labelWidth : me.zLabelWidth,
	        width : txtWidth,
	    	readOnly : me.zIsReadOnly,
	    	allowBlank : me.zAllowBlank,
	    	blankText : me.ZBlankText,
	    	maxLength : me.zMaxLength,
	    	maxLengthText: "字符长度不能超过" + me.zMaxLength + "个字符"
	    });
	    var xzbttp1 = Ext.create('Ext.Button', {
	        text: '查看',
	        width : 40,
	        handler: function() {
	        	if(me.txtFile.getValue() != null && me.txtFile.getValue() != ''){
	        		switch (me.zFileType) {
					case 'image':
			        	var manager = new FsdFileManager();
				        manager.setImgUrl = "../" + me.txtFile.getValue();
				        manager.getImgviewDialog();
						break;
					case 'video':
						var manager = new FsdFileManager();
				        manager.setVideoUrl = "../" + me.txtFile.getValue();
				        manager.getVideoviewDialog();
						break;
					case 'voice':
						var manager = new FsdFileManager();
				        manager.setVideoUrl = "../" + me.txtFile.getValue();
				        manager.getVideoviewDialog();
						break;
					case 'file':
						location.href = "../" + me.txtFile.getValue();
					}
	        	}else{
					Ext.MessageBox.alert("提示","请先上传文件");
				}
	        }
	    });
	    var xzbttp2 = Ext.create('Ext.Button', {
	        text: '取图',
	        width : 40,
	        handler: function() {
	        	if (me.zContentObjFun != null){
	        		me.zContentObj = me.zContentObjFun();
	        	}
	        	if(me.zContentObj == null){
	        		Ext.MessageBox.alert('提示', '取图依赖的富文本编辑器未配置！');
	        		return;
	        	}
	        	var manager = new FsdFileManager();
	    		manager.htmlContent = me.zContentObj.getValue();
	            manager.setSelectListener = function(path) {
	            	me.txtFile.setValue(path);
	            }
	            manager.getImgLoadDialog();
	        }
	    });
	    var xzbttp3 = Ext.create('Ext.Button', {
	        text: '上传',
	        width : 40,
	        handler: function() {
	        	if (me.zFileUpPath == ''){
	        		Ext.MessageBox.alert('提示', '上传文件路径未配置！');
	        		return;
	        	}

	        	switch (me.zManageType) {
				case 'upload':
		        	var manager = new FsdFileManager();
		            manager.autoName = me.zFileAutoName;
		            manager.createDateFolder = true;
		            manager.fileType = me.zFileType;
		            manager.rootpath = me.zFileUpPath;
		            manager.setOkListener = function(serverPathList) {
		                if (serverPathList.length > 0) {
		                	me.txtFile.setValue(serverPathList[0]);
		                }
		            }
		            manager.getUploadDialog();
					break;
				case 'manage':
		        	var manager = new FsdFileManager();
		            manager.autoName = me.zFileAutoName;
		            manager.fileType = me.zFileType;
		            manager.rootpath = me.zFileUpPath;
		            manager.setSelectListener = function(item1, item2 , item3) {
		            	me.txtFile.setValue(item3);
		            };
		            manager.getManagerDialog();
					break;
				}
	        }
	    });
	    var xzbttp4 = Ext.create('Ext.Button', {
	        text: '下载',
	        width : 40,
	        handler: function() {
				var value = me.txtFile.getValue();
				if (value != ''){
					window.open("../" + value, '下载文件'); 
				}
	        }
	    });

		if (!me.zIsShowButton)
			xzbttp1 = null;
		if (!me.zIsGetButton)
			xzbttp2 = null;
		if (!me.zIsUpButton)
			xzbttp3 = null;
		if (!me.zIsDownButton)
			xzbttp4 = null;
	    
        Ext.apply(this, {
        	layout: "hbox",
	    	padding : '0 0 5 0',
        	items:[me.txtFile, xzbttp1, xzbttp2, xzbttp3, xzbttp4]
        });
        
//		if (me.zIsShowButton && me.zIsGetButton && me.zIsUpButton && me.zIsDownButton){
//		} else if (me.zIsShowButton && me.zIsGetButton && me.zIsDownButton){
//	        Ext.apply(this, {
//	        	layout: "hbox",
//		    	padding : '0 0 5 0',
//	        	items:[me.txtFile, xzbttp1, xzbttp2, xzbttp4]
//	        });
//		} else if (me.zIsShowButton && me.zIsGetButton){
//	        Ext.apply(this, {
//	        	layout: "hbox",
//		    	padding : '0 0 5 0',
//	        	items:[me.txtFile, xzbttp1, xzbttp2]
//	        });
//		} else if (me.zIsShowButton && me.zIsUpButton){
//	        Ext.apply(this, {
//	        	layout: "hbox",
//		    	padding : '0 0 5 0',
//	        	items:[me.txtFile, xzbttp1, xzbttp3]
//	        });
//		} else if (me.zIsGetButton && me.zIsUpButton){
//	        Ext.apply(this, {
//	        	layout: "hbox",
//		    	padding : '0 0 5 0',
//	        	items:[me.txtFile, xzbttp2, xzbttp3]
//	        });
//		} else if (me.zIsShowButton){
//	        Ext.apply(this, {
//	        	layout: "hbox",
//		    	padding : '0 0 5 0',
//	        	items:[me.txtFile, xzbttp1]
//	        });
//		} else if (me.zIsGetButton){
//	        Ext.apply(this, {
//	        	layout: "hbox",
//		    	padding : '0 0 5 0',
//	        	items:[me.txtFile, xzbttp2]
//	        });
//		} else if (me.zIsUpButton){
//	        Ext.apply(this, {
//	        	layout: "hbox",
//		    	padding : '0 0 5 0',
//	        	items:[me.txtFile, xzbttp3]
//	        });
//		} else {
//	        Ext.apply(this, {
//	        	layout: "hbox",
//		    	padding : '0 0 5 0',
//	        	items:[me.txtFile]
//	        });
//		}
        
        this.callParent(arguments);
	}
});