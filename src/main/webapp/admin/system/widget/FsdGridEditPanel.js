/*
 * @author lw
 * 编辑表格面板
 */
Ext.define('system.widget.FsdGridEditPanel',{
	extend : 'Ext.grid.GridPanel',
	alias: 'fsdgridpanell',
	model : '',
	column : '',
	baseUrl : '',
	stripeRows : true, // 斑马线效果
	loadMask : true, // 显示遮罩和提示功能,即加载Loading……
	forceFit : true, // 自动填满表格
	selType : "cellmodel", // 多选框选择模式
	multiSelect : true,
	
	zAutoLoad : true,//是否自动加载数据
	zCellEditing : null,
	
	initComponent : function(){
		var me = this;
		me.zCellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1
        });
		var store = Ext.create('Ext.data.Store', {
            model: me.model,
            proxy: {
                type : 'ajax',
                url : me.baseUrl,
                timeout: 300000,//300秒
                reader : { root: 'data'}
            },
            autoLoad : me.zAutoLoad
        });
        Ext.apply(this, {
        	store : store,
        	columns : Ext.create(me.column).columns,
            plugins: [ me.zCellEditing ],
        });
        this.callParent(arguments);
        
        //grid注册选择事件，显示ID信息
        system.UtilStatic.setGridChooseObjID(me);
	}
});