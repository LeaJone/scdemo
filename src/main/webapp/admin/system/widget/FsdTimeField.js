/*
 * @author lw
 * 时间控件
 */
Ext.define('system.widget.FsdTimeField',{
	extend : 'Ext.form.field.Time',
	alias: 'fsddatefield',
	
	format : 'H:i:s',
	altFormats : 'his',
	submitFormat : 'his',
	invalidText : '无效的时间格式，正确的时间格式是：H:i:s或his',
	hideTrigger : true, //下拉按钮隐藏
	
	initComponent : function(){
		var me = this;
		var blanktext = '必填项，请输入';
		if (me.fieldLabel != null)
			blanktext = blanktext + '“' + me.fieldLabel + "”";
		var required = '<span data-qtip="Required">&nbsp;&nbsp;</span>';
		if (!me.allowBlank)
			required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
        Ext.apply(this, {
	    	blankText: blanktext,
	    	afterLabelTextTpl : required,
        	listeners: {
	        	specialkey: function(field,e){
	        		if (e.getKey()==Ext.EventObject.ENTER){
	        			me.isValid()
	        			if(me.nextSibling() != null){
	        				me.nextSibling().focus();
	        			}
	        		}  
	        	}  
        	}
        });
        this.callParent(arguments);
	}
});