/*
 * @author lumingbao
 * 按钮
 */
Ext.define('system.widget.FsdButton',{
	extend : 'Ext.button.Button',
	alias: 'widget.fsdbutton',
	util : Ext.create('system.util.util'),

	popedomCode : null,//权限代码
	zPopedomCodeArray : null,//权限代码组
	zIsSpace : false,
	initComponent : function(){
		var me = this;
		if (!me.util.getParams("user").isadmin){
			/**
			 * 如果权限代码不为空，则该按钮需要验证权限
			 */
			if(me.popedomCode != null && me.popedomCode != ''){
				/**
				 * 禁用按钮
				 */
				me.disable();
				
				/**
				 * 得到权限集合
				 */
				var qxlist = me.util.getParams("popdom").qxlxcd;
				
				/**
				 * 如果权限集合不为空，便利权限集合，如果登陆用户拥有该按钮权限，则将该按钮设置为可用
				 */
				if(qxlist != null){
					for(i=0; i < qxlist.length; i++){
						if(qxlist[i] == me.popedomCode){
							me.enable();
							break;
						}
					}
				}
			}
			if(me.zPopedomCodeArray != null && me.zPopedomCodeArray.length != 0){
				me.disable();
				var qxlist = me.util.getParams("popdom").qxlxcd;
				if(qxlist != null){
					var isReturn = false;
					for(i=0; i < qxlist.length; i++){
						for(j=0; j<me.zPopedomCodeArray.length; j++){
							if(qxlist[i] == me.zPopedomCodeArray[j]){
								me.enable();
								isReturn = true;
								break;
							}
						}
						if (isReturn){
							break;
						}
					}
				}
			}
		}
		
		if (me.zIsSpace){
	        Ext.apply(this, {
	        	text : '　' + me.text + '　'
	        });
		}
        
        this.callParent(arguments);
	}
});