/*
 * @author lumingbao
 * 助记符文本框
 */
Ext.define('system.widget.FsdTextFieldZJF',{
	extend : 'Ext.form.TextField',
	alias: 'fsdtextfieldzjf',
	fname : '',
	util : Ext.create('system.util.util'),
	initComponent : function(){
		var me = this;
		var blanktext = '必填项，请输入';
		if (me.fieldLabel != null)
			blanktext = blanktext + '“' + me.fieldLabel + "”";
		var maxlengthtext = '“' + me.fieldLabel + '”不能超过' + me.maxLength + '个字符';
		var required = '<span data-qtip="Required">&nbsp;&nbsp;</span>';
		if (!me.allowBlank)
			required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
        Ext.apply(this, {
	    	blankText: blanktext,
	    	maxLengthText: maxlengthtext,
	    	afterLabelTextTpl : required,
        	listeners: {
	        	specialkey: function(field,e){
	        		if (e.getKey()==Ext.EventObject.ENTER){
	        			me.isValid()
	        			if(me.nextSibling() != null){
	        				me.nextSibling().focus();
	        			}
	        		}  
	        	},
	        	blur : function(field , e){
	        		me.getData();
	        	}
        	}
        });
        this.callParent(arguments);
	},
	
	getData : function(){
		var me = this;
		if(me.fname != '' && me.up('form').getForm().findField(me.fname).getValue() == "" && me.getValue() != ""){
			var pram = {key : me.getValue()};
            var param = {jsonData : Ext.encode(pram)};
            me.util.ExtAjaxRequest('Common/all_hanziToPinyin.do' , param ,
            function(response, options , respText){
            	me.up('form').getForm().findField(me.fname).setValue(respText.data);
            });
		}
	}
});