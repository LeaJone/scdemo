/*
 * @author lumingbao
 * 文本框
 */
Ext.define('system.widget.FsdNumber',{
	extend : 'Ext.form.field.Number',
	alias: 'fsdnumber',
	initComponent : function(){
		var me = this;
		var blanktext = '必填项，请输入';
		if (me.fieldLabel != null)
			blanktext = blanktext + '“' + me.fieldLabel + "”";
		var maxlengthtext = '“' + me.fieldLabel + '”不能超过' + me.maxLength + '位数字';
		var required = '<span data-qtip="Required">&nbsp;&nbsp;</span>';
		if (!me.allowBlank)
			required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
        Ext.apply(this, {
	    	blankText: blanktext,
	    	maxLengthText: maxlengthtext,
	    	afterLabelTextTpl : required,
        	listeners: {
	        	specialkey: function(field,e){
	        		if (e.getKey()==Ext.EventObject.ENTER){
	        			me.isValid()
	        			if(me.nextSibling() != null){
	        				me.nextSibling().focus();
	        			}
	        		}  
	        	}  
        	}
        });
        this.callParent(arguments);
	}
});