/*
 * @author lumingbao
 * Grid选择组合控件
 */
Ext.define('system.widget.FsdTextGridById',{
	extend : 'Ext.container.Container',
	alias: 'fsdtextgridbyid',
	
	/*-----------文本框参数-------------------*/
	zName : '', //文本框控件Name
	zFieldLabel : '', //控件Lable名称
	zLabelAlign : 'right', //控件Lable对齐方式
	zLabelWidth : 60, //控件Lable宽度
	zIsReadOnly : true, //文本框控件只读
	zAllowBlank : true, //文本框是否必填
	url : '', 
	column : '',
	model : '',
	select : null,
	
	zModel : '',//Grid model对象
	zColumn : '',//Grid列对象
	zBaseUrl : '',//加载数据地址
	zTeamId : '',
	zFunQuery : null,//查询前事件
	zIsText1 : false,
	zIsText2 : false,
	zIsText3 : false,
	zIsText4 : false,
	zTxtLabel1 : '',
	zTxtLabel2 : '',
	zTxtLabel3 : '',
	zTxtLabel4 : '',
	zTxtWidth1 : 150,
	zTxtWidth2 : 150,
	zTxtWidth3 : 150,
	zTxtWidth4 : 150,
	zFunChoose : null,//选择回调函数

	zGridType : 'page',//page分页控件，list无分页列表
	zGridWidth : 1000,//弹出表格宽度
	zGridHeight : 460,//弹出表格高度
	
	initComponent : function(){
		var me = this;
		
		var txtWidth = me.width - 40;
		
	    me.textCon = Ext.create('system.widget.FsdTextField',{
	    	name : me.zName,
	    	fieldLabel : me.zFieldLabel,
	    	labelAlign : me.zLabelAlign,
	    	labelWidth : me.zLabelWidth,
	        width : txtWidth,
	    	readOnly : me.zIsReadOnly,
	    	allowBlank : me.zAllowBlank
	    });
	    
	    me.button = Ext.create('Ext.Button', {
	        text: '选择',
	        width : 40,
	        handler: function() {
	        	if(me.zTeamId == "" || me.zTeamId == null){
                    Ext.MessageBox.alert('提示', '请先选择项目团队！');
				}else{
                    Win.show();
				}
	        }
	    });
	    
	    var queryText1 = null;
	    if (me.zIsText1){
		    queryText1 = Ext.create("system.widget.FsdTextField" , {
				emptyText : '请输入' + me.zTxtLabel1,
				width : me.txtWidth1
			});
	    }
	    var queryText2 = null;
	    if (me.zIsText2){
			queryText2 = Ext.create("system.widget.FsdTextField" , {
				emptyText : '请输入' + me.zTxtLabel2,
				width : me.txtWidth2
			});
	    }
	    var queryText3 = null;
	    if (me.zIsText3){
			queryText3 = Ext.create("system.widget.FsdTextField" , {
				emptyText : '请输入' + me.zTxtLabel3,
				width : me.txtWidth3
			});
	    }
	    var queryText4 = null;
	    if (me.zIsText4){
			queryText4 = Ext.create("system.widget.FsdTextField" , {
				emptyText : '请输入' + me.zTxtLabel4,
				width : me.txtWidth4
			});
	    }
	    
	    var gridtool = 'system.widget.FsdGridPagePanel';
	    if (me.zGridType == 'list'){
	    	gridtool = 'system.widget.FsdGridPanel'
	    }
    	var grid = Ext.create(gridtool, {
			column : me.zColumn, // 显示列
			model : me.zModel,
			baseUrl : me.zBaseUrl,
			tbar : [queryText1, queryText2, queryText3, queryText4 ,{
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}],
			listeners : {
				itemdblclick : function(obj, record, item, index, e, eOpts){
					if(me.zFunChoose != null){
						Win.hide();
						me.zFunChoose(record.data);
					}
				}
			}
		});
    	grid.getStore().on('beforeload', function(s) {
	        var txt1 = null;
			if (me.zIsText1){
				txt1 = queryText1.getValue();
			}
			var txt2 = null;
			if (me.zIsText2){
				txt2 =  queryText2.getValue();
			}
			var txt3 = null;
			if (me.zIsText3){
				txt3 =  queryText3.getValue();
			}
			var txt4 = null;
			if (me.zIsText4){
				txt4 =  queryText4.getValue();
			}
			var queryObj = {};
			if (me.zFunQuery != null)
				queryObj = me.zFunQuery(txt1, txt2, txt3, txt4);
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(queryObj)});
		});
    	var Win = new Ext.Window({
    		title: '选择数据(双击选择)',
			border: 0,  
			frame:false,  
			closeAction :'hide',
			resizable :false,
			modal : true,
	        width: me.zGridWidth,
	        height: me.zGridHeight,
			layout: 'fit',
			items : [grid]  
		});
    	Win.on('show', function(obj, eOpts){
    		grid.getStore().reload();
	    });

	    Ext.apply(this, {
        	layout: "hbox",
	    	padding : '0 0 5 0',
        	items:[me.textCon, me.button]
        });
        
        this.callParent(arguments);
	},
	
	setValue:function(value){
		this.textCon.setValue(value);
	},
	
	getValue:function(){
		return this.textCon.getValue();
	}
});