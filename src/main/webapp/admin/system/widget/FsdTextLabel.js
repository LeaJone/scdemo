/*
 * @author lw
 * 显示文本框
 */
Ext.define('system.widget.FsdTextLabel',{
	extend : 'Ext.form.TextField',
	alias: 'fsdtextlabel',
	border : false,
	readOnly : true,

	initComponent : function(){
		var me = this;
        Ext.apply(this, {
           	fieldStyle:'background:none; border-style: none none solid none;',
//        	fieldStyle:'background:none; border-right: 0px solid;border-top: 0px solid;border-left: 0px solid;border-bottom: #000000 1px solid;',
//        	fieldStyle:'background-color: #DFE9F6;border-color: #DFE9F6; background-image: none;',
	    	afterLabelTextTpl : '<span data-qtip="Required">&nbsp;&nbsp;</span>',
        	listeners: {
	        	specialkey : function(field,e){
	        		if (e.getKey()==Ext.EventObject.ENTER){
	        			me.isValid()
	        			if(me.nextSibling() != null){
	        				me.nextSibling().focus();
	        			}
	        		}  
	        	}
        	}
        });
        this.callParent(arguments);
	}
});