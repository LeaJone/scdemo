/*
 * @author lw
 * 颜色选择组合控件
 */
Ext.define('system.widget.FsdTextColor',{
	extend : 'Ext.container.Container',
	alias: 'fsdfilemanage',
	
	/*-----------文本框参数-------------------*/
	zName : '', //文本框控件Name
	zFieldLabel : '', //控件Lable名称
	zLabelAlign : 'right', //控件Lable对齐方式
	zLabelWidth : 60, //控件Lable宽度
	zIsReadOnly : true, //文本框控件只读
	zAllowBlank : true, //文本框是否必填
	ZBlankText : '请选择输入颜色', //必填提示
	zValue : '',
	
	initComponent : function(){
		var me = this;
		
		var txtWidth = me.width - 40;
		
	    me.txtColor = Ext.create('system.widget.FsdTextField',{
	    	name : me.zName,
	    	fieldLabel : me.zFieldLabel,
	    	labelAlign : me.zLabelAlign,
	    	labelWidth : me.zLabelWidth,
	        width : txtWidth,
	    	readOnly : me.zIsReadOnly,
	    	allowBlank : me.zAllowBlank,
	    	blankText : me.ZBlankText,
	    	value : me.zValue
	    });
	    var btnColor = Ext.create('Ext.Button', {
	        text: '选择',
	        width : 40,
	        handler: function() {
	        	var colorWin = new Ext.Window({
	    			border :0,  
	    			frame:false,  
	    			closeAction :'hide',
	    			resizable :false,
	    			draggable : false,
	    			modal : true,
	    			width :155,
	    			height :125,  
	    			items : [{
	    				xtype:'colorpicker',  
	    				listeners: {
	    					select: function(picker, selColor) {
	    						var color = '#' + selColor;
	    						me.txtColor.setValue(color);
	    						colorWin.hide();
	    					}
	    				}  
	    			}]  
	    		});
	    		colorWin.show();
	        }
	    });

	    Ext.apply(this, {
        	layout: "hbox",
	    	padding : '0 0 5 0',
        	items:[me.txtColor, btnColor]
        });
        
        this.callParent(arguments);
	}
});