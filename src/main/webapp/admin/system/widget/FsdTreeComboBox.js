/*
 * @author lumingbao
 * 下拉树
 */
Ext.define('system.widget.FsdTreeComboBox', {
    extend: 'Ext.form.field.Picker',
    xtype: 'fsdtreecombobox',
    uses: [
        'Ext.tree.Panel'
    ],
    triggerCls: Ext.baseCSSPrefix + 'form-arrow-trigger',
    displayField:'text',
    config: {
    	/**
    	 * 获取数据的路径
    	 */
    	baseUrl : '',
    	
    	/**
    	 * 根节点默认文本
    	 */
        rootText : '根节点',
        
        /**
         * 根节点默认ID
         */
        rootId : '0',
        
        /**
         * 隐藏域Name
         */
        hiddenName : null,
        
        /**
         * @cfg {Ext.data.TreeStore} store
         * A tree store that the tree picker will be bound to
         */
        store: null,
        
        /**
         * @cfg {String} displayField
         * The field inside the model that will be used as the node's text.
         * Defaults to the default value of {@link Ext.tree.Panel}'s `displayField` configuration.
         */
        displayField: null,

		/**
		 * 默认显示根节点
		 */
		rootVisible: true,
		
        /**
         * @cfg {Array} columns
         * An optional array of columns for multi-column trees
         */
        columns: null,

        /**
         * @cfg {Boolean} selectOnTab
         * Whether the Tab key should select the currently highlighted item. Defaults to `true`.
         */
        selectOnTab: true,

        /**
         * @cfg {Number} maxPickerHeight
         * The maximum height of the tree dropdown. Defaults to 300.
         */
        maxPickerHeight: 200,

        /**
         * @cfg {Number} minPickerHeight
         * The minimum height of the tree dropdown. Defaults to 100.
         */
        minPickerHeight: 100
    },
   
    editable: false,
    emptyText : '请选择',
    
    zTreeRoot : 'data',//data异步加载，children一次性加载
    zTreeParams : null,//一次性添加树参数
    zOnChange : null,//回调函数
    //一次性加载例子
//    me.branchname = Ext.create('Ext.form.field.Hidden',{
//    	fieldLabel: '所属机构名称',
//    	name: 'branchname'
//    });
//	me.branchid = Ext.create('system.widget.FsdTreeComboBox',{
//    	fieldLabel : '所属机构',
//    	labelAlign:'right',
//    	labelWidth:80,
//    	rootText : me.oper.util.getParams("company").name,
//    	rootId : me.oper.util.getParams("company").id,
//		rootVisible: false,
//        width : 450,
//        name : 'branchid',
//        baseUrl:'A_Branch/load_AllBranchTreeByPopdom.do',
//        allowBlank: false,
//        zTreeRoot : 'children',//一次性加载
//        zTreeParams : {fid : me.oper.util.getParams("company").id},
//        hiddenName : 'branchname'//隐藏域Name
//    });
    
    zCallback : null,//回调函数
    zTreeUrl : null,//一次性添加树链接
    zDefaultSelectID : null,//默认选中项目的ID值
    zHiddenObject : null,

    initComponent: function() {
        var me = this;
        
        var blanktext = '必填项，请输入';
		if (me.fieldLabel != null)
			blanktext = blanktext + '“' + me.fieldLabel + "”";
		var required = '<span data-qtip="Required">&nbsp;&nbsp;</span>';
		if (!me.allowBlank)
			required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
		Ext.apply(this, {
	    	blankText: blanktext,
	    	afterLabelTextTpl : required
        });
		
        me.store = Ext.create('Ext.data.TreeStore', {
        	proxy: {
                    type: 'ajax',
                    url: me.baseUrl,
                    reader: {
                        type: 'json',
                        root : me.zTreeRoot
                    }
        	},
        	listeners: {
        		beforeload : function(s){ 
                	if (me.zTreeRoot == 'children'){
            			if (me.zTreeParams != null){
                	        var params = s.getProxy().extraParams;
                	        Ext.apply(params,{jsonData : Ext.encode(me.zTreeParams)});
            			}
                    }
                }
            },
        	root: {
	            text: me.rootText,
	            id: me.rootId,
	            expanded: true//默认展开根节点
	        }
        });
        
        me.listeners = {
	        	specialkey: function(field,e){
	        		if (e.getKey()==Ext.EventObject.ENTER){
	        			me.isValid()
	        			if(me.nextSibling() != null){
	        				me.nextSibling().focus();
	        			}
	        		}
	        	},
	        	select : function(field, value, eOpts){
	        		if(me.zCallback != null){
	        			me.zCallback(value.data);
	        		}
                },
        		change : function(obj, newValue, oldValue, eOpts){ 
        			if (me.zOnChange != null)
        				me.zOnChange(obj, newValue, oldValue, eOpts);
                }
        };
        me.callParent(arguments);
        me.addEvents(
            /**
             * @event select
             * Fires when a tree node is selected
             * @param {Ext.ux.TreePicker} picker        This tree picker
             * @param {Ext.data.Model} record           The selected record
             */
            'select'
        );

        me.mon(me.store, {
            scope: me,
            load: me.onLoad,
            update: me.onUpdate
        });

        if (me.zTreeUrl != null){
        	var util = Ext.create('system.util.util');
        	util.ExtAjaxRequest(me.zTreeUrl, me.zTreeParams,
        		    function(response, options, respText){
        		    	if(respText.data != ""){
        		    		me.store.getRootNode().appendChild(respText.data);
        		    		if (me.zDefaultSelectID != null)
        		    			me.setValue(me.zDefaultSelectID);
        		    	}
        		    }, null, me.up("form"));
        }
    },

    /**
     * Creates and returns the tree panel to be used as this field's picker.
     */
    createPicker: function() {
        var me = this,
            picker = Ext.create('Ext.tree.Panel',{
                shrinkWrapDock: 2,//没有这个会报错：Layout run failed
                store: me.store,
                floating: true,
                displayField: me.displayField,
                columns: me.columns,
                minHeight: me.minPickerHeight,
                maxHeight: me.maxPickerHeight,
                rootVisible: me.rootVisible,
                manageHeight: true,//在到达minHeight后，展开节点时自动调整高度
                shadow: true,//组件旁边有没有阴影效果
                listeners: {
                    scope: me,
                    itemclick: me.onItemClick
                },
                viewConfig: {
                    listeners: {
                        scope: me,
                        render: me.onViewRender
                    }
                }
            }),
            view = picker.getView();
            
        if (Ext.isIE9 && Ext.isStrict) {
            // In IE9 strict mode, the tree view grows by the height of the horizontal scroll bar when the items are highlighted or unhighlighted.
            // Also when items are collapsed or expanded the height of the view is off. Forcing a repaint fixes the problem.
            view.on({
                scope: me,
                highlightitem: me.repaintPickerView,
                unhighlightitem: me.repaintPickerView,
                afteritemexpand: me.repaintPickerView,
                afteritemcollapse: me.repaintPickerView
            });
        }
        return picker;
    },
    
    onViewRender: function(view){
        view.getEl().on('keypress', this.onPickerKeypress, this);
    },

    /**
     * repaints the tree view
     */
    repaintPickerView: function() {
        var style = this.picker.getView().getEl().dom.style;

        // can't use Element.repaint because it contains a setTimeout, which results in a flicker effect
        style.display = style.display;
    },

    /**
     * Aligns the picker to the input element
     */
    alignPicker: function() {
        var me = this,
            picker;

        if (me.isExpanded) {
            picker = me.getPicker();
            if (me.matchFieldWidth) {
                // Auto the height (it will be constrained by max height)
                picker.setWidth(me.bodyEl.getWidth());
            }
            if (picker.isFloating()) {
                me.doAlign();
            }
        }
    },

    /**
     * Handles a click even on a tree node
     * @private
     * @param {Ext.tree.View} view
     * @param {Ext.data.Model} record
     * @param {HTMLElement} node
     * @param {Number} rowIndex
     * @param {Ext.EventObject} e
     */
    onItemClick: function(view, record, node, rowIndex, e) {
        this.selectItem(record);
    },

    /**
     * Handles a keypress event on the picker element
     * @private
     * @param {Ext.EventObject} e
     * @param {HTMLElement} el
     */
    onPickerKeypress: function(e, el) {
        var key = e.getKey();

        if(key === e.ENTER || (key === e.TAB && this.selectOnTab)) {
            this.selectItem(this.picker.getSelectionModel().getSelection()[0]);
        }
    },

    /**
     * Changes the selection to a given record and closes the picker
     * @private
     * @param {Ext.data.Model} record
     */
    selectItem: function(record) {
        var me = this;
        me.setValue(record.data.id);
        if(me.hiddenName != null){
			this.up('form').getForm().findField(me.hiddenName).setValue(record.data.text);
		}
		if (me.zHiddenObject != null){
			me.zHiddenObject.setValue(record.data.text);
		}
        me.picker.hide();
        me.inputEl.focus();
        me.fireEvent('select', me, record);

    },

    /**
     * Runs when the picker is expanded.  Selects the appropriate tree node based on the value of the input element,
     * and focuses the picker so that keyboard navigation will work.
     * @private
     */
    onExpand: function() {
        var me = this,
            picker = me.picker,
            store = picker.store,
            value = me.value,
            node;

        
        if (value) {
            node = store.getNodeById(value);
        }
        
        if (!node) {
            node = store.getRootNode();
        }
        
        picker.selectPath(node.getPath());

        Ext.defer(function() {
            picker.getView().focus();
        }, 1);
    },

    /**
     * Sets the specified value into the field
     * @param {Mixed} value
     * @return {Ext.ux.TreePicker} this
     */
    setValue: function(value) {
        var me = this,
            record;

        me.value = value;

        if (me.store.loading) {
            // Called while the Store is loading. Ensure it is processed by the onLoad method.
            return me;
        }
            
        // try to find a record in the store that matches the value
        record = value ? me.store.getNodeById(value) : me.store.getRootNode();
        if (value === undefined) {
            record = me.store.getRootNode();
            me.value = record.getId();
        } else {
            record = me.store.getNodeById(value);
        }

        // set the raw value to the record's display field if a record was found
        me.setRawValue(record ? record.get(me.displayField) : '');
        return me;
    },
    
    setText : function(value){
    	var me = this;
    	me.setRawValue(value);
    },
    
    getSubmitValue: function(){
        return this.value;    
    },

    /**
     * Returns the current data value of the field (the idProperty of the record)
     * @return {Number}
     */
    getValue: function() {
        return this.value;
    },

    /**
     * Handles the store's load event.
     * @private
     */
    onLoad: function() {
        var value = this.value;

        if (value) {
            this.setValue(value);
        }
    },
    
    onUpdate: function(store, rec, type, modifiedFieldNames){
        var display = this.displayField;
        
        if (type === 'edit' && modifiedFieldNames && Ext.Array.contains(modifiedFieldNames, display) && this.value === rec.getId()) {
            this.setRawValue(rec.get(display));
        }
    }
});

