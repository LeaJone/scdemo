/*
 * @author lw
 * 显示文本框
 */
Ext.define('system.widget.FsdTextAreaLabel',{
	extend : 'Ext.form.field.TextArea',
	alias: 'fsdtextarealabel',
	border : false,
	readOnly : true,
//	rows : 8,
	
	initComponent : function(){
		var me = this;
        Ext.apply(this, {
        	fieldStyle:'background:none; border-style:dashed;',
//        	fieldStyle:'background-color: #DFE9F6;border-color: #DFE9F6; background-image: none;',
	    	afterLabelTextTpl : '<span data-qtip="Required">&nbsp;&nbsp;</span>',
        	listeners: {
	        	specialkey : function(field,e){
	        		if (e.getKey()==Ext.EventObject.ENTER){
	        			me.isValid()
	        			if(me.nextSibling() != null){
	        				me.nextSibling().focus();
	        			}
	        		}  
	        	},
	        	change : function(field, newValue, oldValue, eOpts ){
	        		if (me.zIsDate){
	        			
	        		}
	        	}
        	}
        });
        this.callParent(arguments);
	}
});