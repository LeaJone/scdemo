/*
 * @author lw
 * 自定义数据验证
 */

Ext.onReady(function() {
	
	Ext.apply(Ext.form.VTypes, {

        /* 电子邮箱验证 */
        emailText : '电子邮箱格式不正确',

        /* 手机号码验证 */
        cellphone : function(value, field) {
            var regex = /0?(13|14|15|16|17|18|19)[0-9]{9}$/;
            if (regex.test(value))
                return true;
            return false;
        },
        cellphoneText : '请输入正确的手机号码',
		
		/**
		 * 密码验证
		 */
	    password : function(value, field){
	    	var pwd = field.vtypeField;
            return (value == pwd.getValue());
	    },
	    passwordText : "两次密码输入不一致，请重新输入！",
	    
		/**
		 * 身份证号验证
		 */
	    idCard :  function(value, field) {
	    	var validate = Ext.create('system.util.IdCardValidate');
	    	return validate.IdCardValidate(value);
	    },
	    idCardText: '公民身份证号不合法，请重新输入！',

		/**
		 * 起始终止时间闭区间验证
		 */
	    dateBetween :  function(value, field) {
	    	var strat = field.vtypeField.getValue();
	    	if (strat == null)
	    		return true;
	    	if(value.indexOf("年") != -1)
	    		value = value.substr(0, 4) + '-' + value.substr(5, 2) + '-' + value.substr(8, 2);
	    	var end = new Date(value);
	    	if (Ext.Date.isEqual(strat, end))
	    		return true;
	    	var dt = new Date("1970-01-01");
            return !Ext.Date.between(end, dt, strat);
	    },
	    dateBetweenText: '终止时间大于起始时间，请重新输入！'
	});
	
});