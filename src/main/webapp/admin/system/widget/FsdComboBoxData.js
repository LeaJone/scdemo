/*
 * @author lumingbao
 * 数据下拉框
 */
Ext.define('system.widget.FsdComboBoxData',{
	extend : 'Ext.form.ComboBox',
	alias: 'fsdcomboboxdata',
    forceSelection : true,// 值为true时将限定选中的值为列表中的值， 值为false则允许用户将任意文本设置到字段（默认为false）。
    selectOnFocus : true,// 值为 ture时表示字段获取焦点时自动选择字段既有文本(默认为false)。
    triggerAction : 'all',//在单击触发器时要执行的操作。
    editable : false,// 是否编辑
    allowBlank : false,
    blankText : '不允许为空！',
    hiddenName : null,//隐藏域Name
    callback : null,//回调函数

	zBaseUrl : null,//数据加载地址
	zParam : null,//参数
    zValueField : '',//后台对应的隐藏域字段
    zDisplayField : '',//后台对应的显示字段
    zAutoLoad : true, //是否自动加载
	initComponent : function(){
		var me = this;
		var blanktext = '必填项，请输入';
		if (me.fieldLabel != null)
			blanktext = blanktext + '“' + me.fieldLabel + "”";
		var required = '<span data-qtip="Required">&nbsp;&nbsp;</span>';
		if (!me.allowBlank)
			required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
        Ext.apply(this, {
        	blankText: blanktext,
	    	afterLabelTextTpl : required,
	    	fieldLabel : this.fieldLabel,
            emptyText : this.emptyText || '请选择',
            queryMode: 'local',
            store : new Ext.data.JsonStore({
                fields : [me.zValueField,me.zDisplayField],
                remoteSort : true,
                autoLoad : me.zAutoLoad,
                proxy: {
                    type: 'ajax',
                    url : me.zBaseUrl ,
                    reader: {
                        type: 'json',
                        root : "data"
                    }
                } 
            }),
            valueField : me.zValueField,
            displayField : me.zDisplayField,
            listeners: {
	        	specialkey: function(field,e){
	        		if (e.getKey()==Ext.EventObject.ENTER){
	        			me.isValid()
	        			if(me.nextSibling() != null){
	        				me.nextSibling().focus();
	        			}
	        		}
	        	},
	        	select : function(combo,record,index){
	        		if(me.hiddenName != null){
	        			this.up('form').getForm().findField(me.hiddenName).setValue(record[0].data.code);
	        		}
	        		if(me.callback != null){
	        			me.callback(record[0].data.code , record[0].data.name);
	        		}
                }
        	}
        });
        
        this.getStore().on('beforeload', function(s) {
        	if(me.zParam != null){
    	        var params = s.getProxy().extraParams;
    	        Ext.apply(params, me.zParam);
        	}
		});
        
        this.superclass.initComponent.call(this);
	}
});