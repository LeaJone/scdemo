/*
 * @author lumingbao
 * 百度编辑器
 */
Ext.define('system.widget.FsdUeditor',{
	extend:Ext.form.FieldContainer,
	mixins: {
        field: Ext.form.field.Field
	},
	alias: 'fsdueditor',
	alternateClassName: 'Ext.form.UEditor',
	ueditorInstance: null,
    initialized: false,
    tempVal : null,//临时内容
    toolbars : null,//个性化工具栏
    initComponent: function () {
        var me = this;
		var required = '<span data-qtip="Required">&nbsp;&nbsp;</span>';
		Ext.apply(this, {
	    	afterLabelTextTpl : required
        });
        
        me.addEvents('initialize', 'change');
        var id = me.id + '-ueditor';
        me.html = '<script id="' + id + '" type="text/plain" name="' + me.name + '"></script>';
        me.callParent(arguments);
        me.initField();
        me.on('render', function () {
            var width = me.width - 105;
            var height = me.height - 109;
            var config = {initialFrameWidth: width, initialFrameHeight: height};
            if(me.toolbars != null){
            	config = {initialFrameWidth: width, initialFrameHeight: height, toolbars: me.toolbars};
            }
            me.ueditorInstance = UE.getEditor(id, config);
            me.ueditorInstance.ready(function () {
                me.initialized = true;
                me.fireEvent('initialize', me);
                me.ueditorInstance.addListener('contentChange', function () {
                    me.fireEvent('change', me);
                });
                if(me.tempVal != null){
                    me.ueditorInstance.setContent(me.tempVal);
                }
            });
        });
    },
    getValue: function () {
        var me = this,
            value = '';
        if (me.initialized) {
            value = me.ueditorInstance.getContent();
        }
        me.value = value;
        return value;
    },
    setValue: function (value) {
    	var me = this;
    	me.tempVal = value;
        if (value === null || value === undefined) {
            value = '';
        }
        if (me.initialized) {
            me.ueditorInstance.setContent(value);
        }
        return me;
    },
    onDestroy: function () {
    	if(this.ueditorInstance != null){
    		 this.ueditorInstance.destroy();
    	}
    }
});