/**
 * 管理界面，Panel界面添加到Tab
 * @author lw
 */
Ext.define('system.widget.FsdFormPanelTab',{
	extend : 'Ext.tab.Panel',
	deferredRender: false,//是否在显示每个标签的时候再渲染标签中的内容.默认true

	iconPic : '',
	paramObj : null,
	
	initComponent : function() {
		this.callParent(arguments);
	},
	
	setParamObj : function(param) {
		var me = this;
		if (param != null && param.indexOf('&') != -1){
			me.paramObj = {};
			var array = param.split('&');
			var arr = null;
			for (var i = 0; i < array.length; i++) {
				arr = array[i].split('=');
				me.paramObj[arr[0]] = arr[1];
			}
		}
	},
	
	setIconPic : function(iconPic) {
		var me = this;
		me.iconPic = iconPic;
	}
});