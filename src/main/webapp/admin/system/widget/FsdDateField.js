/*
 * @author lw
 * 日期控件
 */
Ext.define('system.widget.FsdDateField',{
	extend : 'Ext.form.field.Date',
	alias: 'fsddatefield',
	
	format : 'Y年m月d日',
	altFormats : 'Ymd',
	submitFormat : 'Ymd',
	invalidText : '无效的日期格式，正确的日期格式是：yyyy年MM月dd日或yyyyMMdd',
	
	initComponent : function(){
		var me = this;
		var blanktext = '必填项，请输入';
		if (me.fieldLabel != null)
			blanktext = blanktext + '“' + me.fieldLabel + "”";
		var required = '<span data-qtip="Required">&nbsp;&nbsp;</span>';
		if (!me.allowBlank)
			required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
        Ext.apply(this, {
	    	blankText: blanktext,
	    	afterLabelTextTpl : required,
        	listeners: {
	        	specialkey: function(field,e){
	        		if (e.getKey()==Ext.EventObject.ENTER){
	        			me.isValid()
	        			if(me.nextSibling() != null){
	        				me.nextSibling().focus();
	        			}
	        		}  
	        	}  
        	}
        });
        this.callParent(arguments);
	}
});