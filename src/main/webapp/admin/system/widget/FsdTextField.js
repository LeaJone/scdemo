/*
 * @author lumingbao
 * 文本框
 */
Ext.define('system.widget.FsdTextField',{
	extend : 'Ext.form.TextField',
	alias: 'fsdtextfield',
	
    zOnChange : null,//回调函数
	initComponent : function(){
		var me = this;
		var blanktext = '必填项，请输入';
		if (me.fieldLabel != null)
			blanktext = blanktext + '“' + me.fieldLabel + "”";
		var maxlengthtext = '“' + me.fieldLabel + '”不能超过' + me.maxLength + '个字符';
		var required = '<span data-qtip="Required">&nbsp;&nbsp;</span>';
		if (!me.allowBlank)
			required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
        Ext.apply(this, {
	    	blankText: blanktext,
	    	maxLengthText: maxlengthtext,
	    	afterLabelTextTpl : required,
        	listeners: {
	        	specialkey: function(field,e){
	        		if (e.getKey()==Ext.EventObject.ENTER){
	        			me.isValid()
	        			if(me.nextSibling() != null){
	        				me.nextSibling().focus();
	        			}
	        		}  
	        	},
        		change : function(obj, newValue, oldValue, eOpts){ 
        			if (me.zOnChange != null)
        				me.zOnChange(obj, newValue, oldValue, eOpts);
                }
        	}
        });
        this.callParent(arguments);
	}
});