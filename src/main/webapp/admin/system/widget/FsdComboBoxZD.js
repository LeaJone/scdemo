/*
 * @author lumingbao
 * 字典下拉框
 */
Ext.define('system.widget.FsdComboBoxZD',{
	extend : 'Ext.form.ComboBox',
	alias: 'fsdcomboboxzd',
	id : null, //控件ID
    baseUrl:'Sys_SystemDictionary/load_sysdict_key.do',//数据加载地址
	key : null,//参数
    emptyText : null,//默认提示信息
    valueField:'code',//加载数据时后台对应的隐藏域字段
    displayField:'name',//加载数据时后台对应的显示字段
    forceSelection : true,// 值为true时将限定选中的值为列表中的值， 值为false则允许用户将任意文本设置到字段（默认为false）。
    //selectOnFocus : true,// 值为 ture时表示字段获取焦点时自动选择字段既有文本(默认为false)。
    editable : false,// 是否编辑
    allowBlank : true,
    
    hiddenName : null,//隐藏域Name
    zHiddenObject : null,
    zCallback : null,//回调函数
    zDefaultCode : null,//默认选中值
    zIsAddEmpty : false,//是否添加空值选项
    zYesCodes : null,//显示集合
    zNotCodes : null,//不显示集合
    
	initComponent : function(){
		var me = this;
		var blanktext = '必填项，请输入';
		if (me.fieldLabel != null)
			blanktext = blanktext + '“' + me.fieldLabel + "”";
		var required = '<span data-qtip="Required">&nbsp;&nbsp;</span>';
		if (!me.allowBlank)
			required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
		
		var jsonStore = Ext.create('Ext.data.JsonStore', {
            fields : [me.valueField,me.displayField],
            remoteSort : true,
//            autoLoad : true,
            proxy: {
                type: 'ajax',
                url : me.baseUrl ,
                reader: {
                    type: 'json',
                    root : "data"
                }
            } 
        });
		
        Ext.apply(this, {
	    	blankText: blanktext,
	    	afterLabelTextTpl : required,
            fieldLabel : this.fieldLabel,
            emptyText : this.emptyText || '请选择',
            queryMode: 'local',
            store : jsonStore,
            triggerAction : 'all',
            valueField : me.valueField,
            displayField : me.displayField,
            listeners: {
	        	specialkey: function(field,e){
	        		if (e.getKey()==Ext.EventObject.ENTER){
	        			me.isValid()
	        			if(me.nextSibling() != null){
	        				me.nextSibling().focus();
	        			}
	        		}
	        	},
	        	select : function(combo,record,index){
	        		if(me.hiddenName != null && me.up('form') != null){
	        			me.up('form').getForm().findField(me.hiddenName).setValue(record[0].data.name);
	        		}
	        		if (me.zHiddenObject != null){
	        			me.zHiddenObject.setValue(record[0].data.name);
	        		}
	        		if(me.zCallback != null){
	        			me.zCallback(record[0].data.code , record[0].data.name);
	        		}
                }
        	}
        });
        this.getStore().on('beforeload', function(s) {
        	if(me.key != null){
        		var param = {jsonData : Ext.encode({
        			key : me.key, 
        			isempty : me.zIsAddEmpty,
        			yescodes : me.zYesCodes,
        			notcodes : me.zNotCodes
        		})};
    	        var params = s.getProxy().extraParams;
    	        Ext.apply(params, param);
        	}
		});
		
		jsonStore.load({
		    callback: function(records, operation, success) {
		    	if (records != null && records.length > 0){
		    		
		    		if (me.zDefaultCode != null){
			    		for(var i=0; i<records.length; i++){
			    			if(records[i].data.code == me.zDefaultCode){
			    				me.select(me.zDefaultCode);
			    				if(me.hiddenName != null && me.up('form') != null){
				        			me.up('form').getForm().findField(me.hiddenName).setValue(records[i].data.name);
				        		}
				        		if (me.zHiddenObject != null){
				        			me.zHiddenObject.setValue(records[i].data.name);
				        		}
				        		if(me.zCallback != null){
				        			me.zCallback(records[i].data.code , records[i].data.name);
				        		}
			    				return;
			    			}
			    		}
			    	}
		    	}
		    }
		});
		
        this.superclass.initComponent.call(this);
	}
});