/*
 * @author lw
 * 工作流处理窗体
 */
Ext.define('system.widget.FsdWindowFlow',{
	extend : 'Ext.window.Window',
	//是否为工作流展示数据，false正常显示编辑界面，true标示工作流展示数据不需要任何操作
	isShowFlowData : false,
	/**
	 * 根据ID加载表单数据
	 * @param id 数据对象ID
	 * @param url 数据对象请求地址
	 * @param success 请求成功方法
	 * @param failure 请求失败方法
	 */
	loadFormData : function(id, url, success, failure){
	}
});