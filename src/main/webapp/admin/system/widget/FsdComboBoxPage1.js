/*
 * @author lw
 * 支持分页和检索的下拉框
 */
Ext.define('system.widget.FsdComboBoxPage',{
	extend : 'Ext.form.ComboBox',
	alias: 'fsdpagecombobox',
	
	minChars : 0,
	emptyText : "请选择...",
	forceSelection : false,	
	mustSelection:false,
	typeAhead:false,//文本输入时自动填充
	triggerAction : 'query',
	minChars:0,
	matchFieldWidth:false,
	/**
	 * 下拉框宽度
	 * @type 
	 */
	listConfig:{width:230},
	pageSize:20,//分页 
	queryMode: 'remote',
	queryParam:'query',//此参数在该类中没有用(因为覆写了getParams方法)
	valueField : 'id',
	displayField : 'name',
	hiddenNamea : null,//隐藏域Name

    zCallback : null,//回调函数
	zParams : '',//参数
	/**
	 * 初始化的方法
	 */
	constructor: function(config) {
		var me=this;
		me.callParent(arguments);
		me.afterInitWally();
	},
	/**
	 * 初始化完成后执行的方法
	 */
	afterInitWally:function(){
		var me=this;		
		var store = new Ext.data.JsonStore({
            fields : [this.valueField,this.displayField],
            remoteSort : true,
            autoLoad : false,
            proxy: {
                type: 'ajax',
                method:'post',
                url : this.baseUrl + this.zParams ,
                reader: {
                    type: 'json',
                    root : "data",
                    totalProperty: 'totalCount'
                }
            } 
        });
		me.bindStore(store);
	},
	/**
	 * 重写选择器的宽度设定方式
     * Aligns the picker to the input element
     * @protected
     */
    alignPicker: function() {
        var me = this,
            picker = me.getPicker();

        if (me.isExpanded) {
            if (me.matchFieldWidth) {
                // Auto the height (it will be constrained by min and max width) unless there are no records to display.
                picker.setWidth(me.bodyEl.getWidth());
            }else{//重写,在设定的宽度小于combox的宽度时,使用combox的宽度
            	if(picker.getWidth()<me.bodyEl.getWidth()){
            		picker.setWidth(me.bodyEl.getWidth());
            	}
            }
            if (picker.isFloating()) {
                me.doAlign();
            }
        }
    },
	/**
	 * 增加事件
	 */
	initEvents: function() {
        var me = this;
        me.callParent();
        me.on('focus',me.onfocusWally,me);
    },
    /**
     * 获得焦点的事件
     * @param {} comp
     * @param {} eventObject
     * @param {} eventOption
     */
    onfocusWally:function(comp, eventObject, eventOption ){
    	var me = this;
        if (!me.readOnly && !me.disabled && me.editable) {
            me.doQueryTask.delay(me.queryDelay);
        }
    },
	/**
	 * 覆写超类的方法.方便后台接收
	 * @param {} queryString
	 * @return {}
	 */
    getParams: function(queryString) {
    	var me = this;
        return {jsonData : Ext.encode({key : queryString})};
    },
    /**
     * 覆写超类的方法,失去焦点时,输入框中的值不存在与下拉框中就清空输入框的值而不是显示最后一次的值
     */
     assertValue: function() {
        var me = this,
            value = me.getRawValue(),
            rec;

        if (me.forceSelection) {
            if (me.multiSelect) {
                // For multiselect, check that the current displayed value matches the current
                // selection, if it does not then revert to the most recent selection.
                if (value !== me.getDisplayValue()) {
                    me.setValue(me.lastSelection);
                }
            } else {
                // For single-select, match the displayed value to a record and select it,
                // if it does not match a record then revert to the most recent selection.
                rec = me.findRecordByDisplay(value);
                if (rec) {
                    me.select(rec);
                } else {
                    me.setValue([]);
                }
            }
        }else{
			rec = me.findRecordByDisplay(value);
			if (rec) {
			    me.select(rec);
			} else {
			    me.setValue([]);
			}
        }
        me.collapse();
    }
});