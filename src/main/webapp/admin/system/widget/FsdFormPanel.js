/**
 * 管理界面，Panel界面添加到Tab
 * @author lw
 */
Ext.define('system.widget.FsdFormPanel',{
	extend : 'Ext.panel.Panel',
	header : false,
	border : 0,
	iconPic : '',
	paramObj : null,
	
	initComponent : function() {
		this.callParent(arguments)
	},
	
	setParamObj : function(param) {
		var me = this;
		if (param != null && param.indexOf('&') != -1){
			me.paramObj = {};
			var array = param.split('&');
			var arr = null;
			for (var i = 0; i < array.length; i++) {
				arr = array[i].split('=');
				me.paramObj[arr[0]] = arr[1];
			}
		}
	},
	
	setIconPic : function(iconPic) {
		var me = this;
		me.iconPic = iconPic;
	}
});