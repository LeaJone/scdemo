﻿/**
 * 自定义表单使用
 * @author lumingbao
 */
Ext.define('system.widget.FsdFormUse', {
	extend : 'Ext.window.Window',
	formid : '',
	taskid:'',
    employeeid:'',
	htmlStr:'',
    util : Ext.create('system.util.util'),
	initComponent:function(){
	    var me = this;

        var form = Ext.create('Ext.form.Panel',{
            defaultType : 'textfield',
            html:me.htmlStr,
            header : false,// 是否显示标题栏
            border : 0,
            padding : '5 10 10 10',
            autoScroll : true,
            items : [{
                name: "formid",
                xtype: "hidden",
                value:me.formid
            },{
                name: "taskid",
                xtype: "hidden",
                value:me.taskid
            },{
                name: "employeeid",
                xtype: "hidden",
                value:me.employeeid
            }]
        });
        
        var fillFormField = function(key, value){
        	$("input[type=text][name='"+key+"']").each(function(){
        		if($(this).hasClass('fsdtimefield')){
        			if(value.length == 8){
        				value = value.substring(0, 4) + '-' + value.substring(4, 6) + '-' + value.substring(6, 8);
        			}
        		}
    			$(this).attr("value", value);
        	});
            $("textarea[name='"+key+"']").html(value);
            $("input[type=radio][name='"+key+"'][value='"+value+"']").attr("checked",'checked');
            $("input[type=hidden][name='"+key+"'][plugins=upload]").attr("value", value);
            if(key.indexOf("f_upload_file") != -1){
                $("input[type=hidden][name='"+key+"']").after("&nbsp;&nbsp;&nbsp;<input type=\"button\" value=\"下载附件\" class=\"fsduploadfield layui-btn\" onclick=\"window.open('../"+value+"');\"/>");
            }
        };

        me.on('show' , function(){
            var pram = {formid : me.formid, employeeid : me.employeeid};
            var param = {jsonData : Ext.encode(pram)};
            me.util.ExtAjaxRequest('f_form/load_form.do', param, function(response){
                var obj = JSON.parse(response.responseText);
                var form = obj.data.formMap;
                var info = obj.data.infoMap;
                $.each(info,function(defaultKey,defaultValue){
                	fillFormField(defaultKey, defaultValue);
//                    $("input[type=text][name='"+defaultKey+"']").attr("value", defaultValue);
//                    $("textarea[name='"+defaultKey+"']").html(defaultValue);
//                    $("input[type=radio][name='"+defaultKey+"'][value='"+defaultValue +"']").attr("checked",'checked');
                });
                $.each(form,function(key,value){
                    if(value == null || value == ""){
                        $.each(info,function(key,value){
                        	fillFormField(key, value);
//                            $("input[type=text][name='"+infoKey+"']").attr("value", infoValue);
//                            $("textarea[name='"+infoKey+"']").html(infoValue);
//                            $("input[type=radio][name='"+infoKey+"'][value='"+infoValue +"']").attr("checked",'checked');
                        });
                    }else{
                    	fillFormField(key, value);
//                        $("input[type=text][name='"+key+"']").attr("value", value);
//                        $("textarea[name='"+key+"']").html(value);
//                        $("input[type=radio][name='"+key+"'][value='"+value+"']").attr("checked",'checked');
//                        $("input[type=hidden][name='"+key+"'][plugins=upload]").attr("value", value);
//                        if(key.indexOf("f_upload_file") != -1){
//                            $("input[type=hidden][name='"+key+"']").after("&nbsp;&nbsp;&nbsp;<input type=\"button\" value=\"下载附件\" class=\"fsduploadfield layui-btn\" onclick=\"window.open('../"+value+"');\"/>");
//                        }
                    }
                });
			}, null, form);
            // 表单映射关系
            me.util.ExtAjaxRequest('f_form/mirror_value.do', param, function(response){
            	var obj = JSON.parse(response.responseText);
            	var info = obj.data;
            	if(info != null){
            		$.each(info,function(defaultKey,defaultValue){
//            			console.log('formmirror', defaultKey + '-->' + defaultValue);
                    	fillFormField(defaultKey, defaultValue);
//                        $("input[type=text][name='"+defaultKey+"']").attr("value", defaultValue);
//                        $("textarea[name='"+defaultKey+"']").html(defaultValue);
//                        $("input[type=radio][name='"+defaultKey+"'][value='"+defaultValue +"']").attr("checked",'checked');
//                        $("input[type=hidden][name='"+defaultKey+"'][plugins=upload]").attr("value", defaultValue);
//                        if(defaultKey.indexOf("f_upload_file") != -1){
//                            $("input[type=hidden][name='"+defaultKey+"']").after("&nbsp;&nbsp;&nbsp;<input type=\"button\" value=\"下载附件\" class=\"fsduploadfield layui-btn\" onclick=\"window.open('../"+defaultValue+"');\"/>");
//                        }
                    });
            	}
            }, null, form);
            me.util.ExtAjaxRequest('F_tbl_aquestion/load_question.do', param, function(response){
                var obj = JSON.parse(response.responseText);
                var questions = obj.data;
                $.each(questions,function(index, obj){
                    $("textarea[name='f_thjl_content']").prepend(obj.questioncode + obj.questiontitle + "\n\n\n\n\n");
                });

            }, null, form);
        });

	    Ext.apply(this,{
            layout : 'fit',
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
			maxHeight:500,
			width:800,
			items:[form],
	        buttons : [{
                name : 'btnExport',
                text : '导出',
                iconCls : 'articlewj',
                handler : function() {
                    var taskjson = form.getValues();
                    var formjson = $("form[name=formusediv]").serializeObject();
                    var pram = {taskjson : taskjson, formjson : formjson};
                    var param = {jsonData : Ext.encode(pram)};
                    me.util.ExtAjaxRequest('f_form/export_form.do', param, function(response){
                        var respText = Ext.JSON.decode(response.responseText);
                        window.open(respText.data.httpUrl);
                        me.close();
                        Ext.MessageBox.alert("提示","导出成功！");
                    }, null, null);
                }
            }, '-', {
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
			    handler : function() {
					var taskjson = form.getValues();
					var formjson = $("form[name=formusediv]").serializeObject();
                    var pram = {taskjson : taskjson, formjson : formjson};
                    var param = {jsonData : Ext.encode(pram)};
                    me.util.ExtAjaxRequest('f_form/save_form.do', param, function(){
                    	Ext.MessageBox.alert("提示","保存成功！");
                    	me.close();
					}, null, null);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});