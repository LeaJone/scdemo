/*
 * @author lumingbao
 * 验证码布局
 */
Ext.define('system.widget.FsdCaptcha',{
	extend: 'Ext.container.Container',
	alias: 'widget.fsdcaptcha',
	codeUrl:Ext.BLANK_IMAGE_URL,
	isLoader:true,
	imgWidth : 100,//验证码图片宽度
	imgHeight : 30,//验证码图片高度
	
	/*-----------文本框参数-------------------*/
	zName : '', //文本框控件Name
	zFieldLabel : '', //控件Lable名称
	zFieldStyle : '',
	zLabelAlign : 'right', //控件Lable对齐方式
	zLabelWidth : 60, //控件Lable宽度
	zMaxLength : 10,
	zIsReadOnly : true, //文本框控件只读
	zAllowBlank : true, //文本框是否必填
	ZBlankText : '请选择输入验证码', //必填提示
	ZMaxLengthText : '验证码的最大长度为20个字符',
	zValue : '',
	isLoader:true,
	zListeners : null,
	initComponent : function(){
		var me = this;
		
		var txtWidth = me.width - 110;
		me.txtField = Ext.create('Ext.form.field.Text',{
		    name : me.zName,
		    fieldStyle : me.zFieldStyle,
		    fieldLabel : me.zFieldLabel,
		    labelWidth : me.zLabelWidth,
		    maxLength : me.zMaxLength,
		    width : txtWidth,
		    allowBlank : me.zAllowBlank,
		    blankText : me.ZBlankText,
		    maxLengthText : me.ZMaxLengthText,
		    value : me.zValue,
		    listeners : me.zListeners
	    });
		
		me.image = Ext.create('Ext.Img', {
			src : this.codeUrl + '?id=' + Math.random(),
			width:me.imgWidth,
			height:me.imgHeight,
			margin : '0 0 0 5',
			listeners : {
				click : me.loadCodeImg,
				element : "el",
				scope : this
			}
		});
		Ext.apply(this, {
        	layout: "hbox",
	    	padding : '0 0 5 0',
        	items:[me.txtField, me.image]
        });
		
        this.callParent(arguments);
	},
	
	loadCodeImg: function() {
		
        this.image.setSrc(this.codeUrl + '?id=' + Math.random());
    }
});