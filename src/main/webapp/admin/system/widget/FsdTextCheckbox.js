/*
 * @author lw
 * 复选文本组合控件
 */
Ext.define('system.widget.FsdTextCheckbox',{
	extend : 'Ext.container.Container',
	alias: 'fsdtextcheckbox',
	
	/*-----------复选框参数-------------------*/
	zNameCheckbox : '', //复选框控件Name
	zFieldLabel : '', //控件Lable名称
	zLabelAlign : 'right', //控件Lable对齐方式
	zLabelWidth : 60, //控件Lable宽度
	zInputValue : 'true', //复选框选中默认值
	/*-----------文本框参数-------------------*/
	zNameText : '', //文本框控件Name
	zAllowBlank : true, //文本框是否必填
	zMaxLength : 200, //文本框输入长度
	
	
	initComponent : function(){
		var me = this;
		
		var chkWidth = me.zLabelWidth + 20;
		var txtWidth = me.width - chkWidth;

		var required = '<span data-qtip="Required">&nbsp;&nbsp;</span>';
		if (!me.zAllowBlank)
			required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
		me.checkbox = Ext.create('system.widget.FsdCheckbox',{
	    	afterLabelTextTpl : required,
	    	fieldLabel : me.zFieldLabel,
	    	labelAlign : me.zLabelAlign,
	    	labelWidth : me.zLabelWidth,
	        width : chkWidth,
	    	name : me.zNameCheckbox,
	    	inputValue : me.zInputValue
	    });
		
	    me.txtValue = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel : me.zFieldLabel,
	    	name : me.zNameText,
	    	hideLabel : true,
	        width : txtWidth,
	    	allowBlank : me.zAllowBlank,
	    	maxLength: me.zMaxLength
	    });

	    me.checkbox.on('change', function (obj, newValue, oldValue, eOpts) {
	    	me.txtValue.allowBlank = !newValue;
	    });

	    Ext.apply(this, {
        	layout: "hbox",
	    	padding : '0 0 5 0',
        	items:[me.checkbox, me.txtValue]
        });
        
        this.callParent(arguments);
	}
});