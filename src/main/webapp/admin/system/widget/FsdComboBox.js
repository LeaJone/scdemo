/*
 * @author lw
 * 传统下拉框
 */
Ext.define('system.widget.FsdComboBox',{
	extend : 'Ext.form.ComboBox',
	alias: 'fsdcombobox',
    emptyText : null,//默认提示信息
    forceSelection : true,// 值为true时将限定选中的值为列表中的值， 值为false则允许用户将任意文本设置到字段（默认为false）。
    selectOnFocus : true,// 值为 ture时表示字段获取焦点时自动选择字段既有文本(默认为false)。
    editable : false,// 是否编辑
    blankText : '不允许为空！',
    queryMode: 'local',
    hiddenName : null,//隐藏域Name
    
    zQueryMode : 'local', //'remote' 
    zValueField : null,//加载数据时后台对应的隐藏域字段
    zDisplayField : null,//加载数据时后台对应的显示字段
    zCallBack : null,//回调函数
    
	initComponent : function(){
		var me = this;
		var blanktext = '必填项，请输入';
		if (me.fieldLabel != null)
			blanktext = blanktext + '“' + me.fieldLabel + "”";
		var required = '<span data-qtip="Required">&nbsp;&nbsp;</span>';
		if (!me.allowBlank)
			required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
		
		var jsonStore = null;
		if (me.zQueryMode == 'local'){
//			jsonStore = Ext.create('Ext.data.JsonStore', {
//	            fields : [me.zValueField,me.zDisplayField]
//	        });
//			jsonStore = Ext.create('Ext.data.JsonStore');
		}else{
			jsonStore = Ext.create('Ext.data.JsonStore', {
	            fields : [me.zValueField,me.zDisplayField],
	            remoteSort : true,
//	            autoLoad : true,
	            proxy: {
	                type: 'ajax',
	                url : me.baseUrl ,
	                reader: {
	                    type: 'json',
	                    root : "data"
	                }
	            } 
	        });
			Ext.apply(this, {
	            queryMode: me.zQueryMode,
	            store : jsonStore,
	            valueField : me.zValueField,
	            displayField : me.zDisplayField,
	        });
		}
		
        Ext.apply(this, {
	    	blankText: blanktext,
	    	afterLabelTextTpl : required,
            fieldLabel : this.fieldLabel,
            emptyText : this.emptyText || '请选择',
            listeners: {
	        	specialkey: function(field,e){
	        		if (e.getKey()==Ext.EventObject.ENTER){
	        			me.isValid()
	        			if(me.nextSibling() != null){
	        				me.nextSibling().focus();
	        			}
	        		}
	        	},
	        	select : function(combo,record,index){
	        		if(me.hiddenName != null){
	        			this.up('form').getForm().findField(me.hiddenName).setValue(record[0].data.code);
	        		}
	        		if(me.callback != null){
	        			me.zCallBack(record[0].data);
	        		}
                }
        	}
        });
        this.superclass.initComponent.call(this);
	},
	
	/**
     * 覆写超类的方法,失去焦点时,输入框中的值不存在与下拉框中就清空输入框的值而不是显示最后一次的值
     */
     assertValue: function() {
        var me = this,
            value = me.getRawValue(),
            rec;

        if (me.forceSelection) {
            if (me.multiSelect) {
                // For multiselect, check that the current displayed value matches the current
                // selection, if it does not then revert to the most recent selection.
                if (value !== me.getDisplayValue()) {
                    me.setValue(me.lastSelection);
                }
            } else {
                // For single-select, match the displayed value to a record and select it,
                // if it does not match a record then revert to the most recent selection.
                rec = me.findRecordByDisplay(value);
                if (rec) {
                    me.select(rec);
                } else {
                    me.setValue([]);
                }
            }
        }else{
			rec = me.findRecordByDisplay(value);
			if (rec) {
			    me.select(rec);
			} else {
			    me.setValue([]);
			}
        }
        me.collapse();
    }
});

//例如
//var arrayYear = new Array();
//var myDate = new Date();
//var num = myDate.getFullYear();
//for ( var i = num; i >= 2014; i--) {
//	arrayYear. push([String(i), String(i) + '年']);
//}
//var year = Ext.create('system.widget.FsdComboBox',{
//    fieldLabel:'统计年份',
//    labelAlign:'right',
//	labelWidth:60,
//    width : 150,
//    name : 'auditing',//提交到后台的参数名
//    store : arrayYear
//});