/*
 * @author lw
 * 按钮组合控件
 */
Ext.define('system.widget.FsdTextButton',{
	extend : 'Ext.container.Container',
	alias: 'fsdtextbutton',
	
	/*-----------文本框参数-------------------*/
	zName : '', //文本框控件Name
	zFieldLabel : '', //控件Lable名称
	zLabelAlign : 'right', //控件Lable对齐方式
	zLabelWidth : 60, //控件Lable宽度
	zReadOnly : false, //文本框控件只读
	zAllowBlank : true, //文本框是否必填
	zMaxLength : 200, //文本框输入长度
	zVtype : '',//特殊验证参数
	/*-----------按钮参数-------------------*/
	zIsButton1 : false, //是否显示按钮1
	zButton1Text : '按1',
	zButton1Width : 40,
	zButton1Callback : null,
	zIsButton2 : false, //是否显示取图按钮
	zButton2Text : '按2',
	zButton2Width : 40,
	zButton2Callback : null,
	zIsButton3 : false, //是否显示上传按钮
	zButton3Text : '按3',
	zButton3Width : 40,
	zButton3Callback : null,
	zIsButton4 : false, //是否显示下载按钮
	zButton4Text : '按4',
	zButton4Width : 40,
	zButton4Callback : null,
	
	//获取文本控件值
	zGetValue : function(){
		var me = this;
		return me.txtContent.getValue();
	},
	//设置文本控件值
	zSetValue : function(value){
		var me = this;
		return me.txtContent.setValue(value);
	},
	//验证控件值
	zIsValid : function(){
		var me = this;
		return me.txtContent.isValid();
	},
	//设置文本框是否必填
	zSetAllowBlank : function(isAllowBlank){
		var me = this;
		me.txtContent.allowBlank = isAllowBlank;
	},
	
	initComponent : function(){
		var me = this;
		
		var txtWidth = me.width;
		if (me.zIsButton1)
			txtWidth -= me.zButton1Width;
		if (me.zIsButton2)
			txtWidth -= me.zButton2Width;
		if (me.zIsButton3)
			txtWidth -= me.zButton3Width;
		if (me.zIsButton4)
			txtWidth -= me.zButton4Width;
		
	    me.txtContent = Ext.create('system.widget.FsdTextField',{
	    	name : me.zName,
	    	fieldLabel : me.zFieldLabel,
	    	labelAlign : me.zLabelAlign,
	    	labelWidth : me.zLabelWidth,
	        width : txtWidth,
	    	readOnly : me.zReadOnly,
	    	allowBlank : me.zAllowBlank,
	    	maxLength : me.zMaxLength,
			vtype : me.zVtype
	    });
	    var button1 = Ext.create('Ext.Button', {
	        text: me.zButton1Text,
	        width : me.zButton1Width,
	        handler: function() {
	        	if (me.zButton1Callback != null){
	        		me.zButton1Callback(me);
	        	}
	        }
	    });
	    var button2 = Ext.create('Ext.Button', {
	        text: me.zButton2Text,
	        width : me.zButton2Width,
	        handler: function() {
	        	if (me.zButton2Callback != null){
	        		me.zButton2Callback(me);
	        	}
	        }
	    });
	    var button3 = Ext.create('Ext.Button', {
	        text: me.zButton3Text,
	        width : me.zButton3Width,
	        handler: function() {
	        	if (me.zButton3Callback != null){
	        		me.zButton3Callback(me);
	        	}
	        }
	    });
	    var button4 = Ext.create('Ext.Button', {
	        text: me.zButton4Text,
	        width : me.zButton4Width,
	        handler: function() {
	        	if (me.zButton4Callback != null){
	        		me.zButton4Callback(me);
	        	}
	        }
	    });

		if (!me.zIsButton1)
			button1 = null;
		if (!me.zIsButton2)
			button2 = null;
		if (!me.zIsButton3)
			button3 = null;
		if (!me.zIsButton4)
			button4 = null;
	    
        Ext.apply(this, {
        	layout: "hbox",
	    	padding : '0 0 5 0',
        	items:[me.txtContent, button1, button2, button3, button4]
        });

        this.callParent(arguments);
	}
});