/*
 * @author lw
 * 日期控件
 * 
 * 所需控件配合 
 * system.widget.FsdDateTimePicker
 * system.widget.FsdTimePickerField
 */
Ext.define('system.widget.FsdDateTimeLabel', {
      extend: 'Ext.form.field.Date',
      alias: 'fsddatetime',
      requires: ['system.widget.FsdDateTimePicker'],

  	format : 'Y年m月d日 H:i:s',
  	altFormats : 'YmdHis',
  	submitFormat : 'YmdHis',
  	invalidText : '无效的时间格式<br/>正确的时间格式是：<br/>&nbsp;&nbsp;yyyy年MM月dd日 HH:ii:ss<br/>&nbsp;&nbsp;或yyyyMMddHHiiss',
	readOnly : true,
	isDate : false,
  	
      initComponent: function() {
          this.format = this.format;
          if(this.isDate){
        	  this.format = 'Y年m月d日';
          }
	        Ext.apply(this, {
	           	fieldStyle:'background:none; border-style: none none solid none;',
		    	afterLabelTextTpl : '<span data-qtip="Required">&nbsp;&nbsp;</span>'
	        });
          this.callParent();
      },
      
      // overwrite
      createPicker: function() {
          var me = this,
              format = Ext.String.format;
          return Ext.create('system.widget.FsdDateTimePicker', {
                ownerCt: me.ownerCt,
                renderTo: document.body,
                floating: true,
                hidden: true,
                focusOnShow: true,
                minDate: me.minValue,
                maxDate: me.maxValue,
                disabledDatesRE: me.disabledDatesRE,
                disabledDatesText: me.disabledDatesText,
                disabledDays: me.disabledDays,
                disabledDaysText: me.disabledDaysText,
                format: me.format,
                showToday: me.showToday,
                startDay: me.startDay,
                minText: format(me.minText, me.formatDate(me.minValue)),
                maxText: format(me.maxText, me.formatDate(me.maxValue)),
                listeners: {
                    scope: me,
                    select: me.onSelect
                },
                keyNavConfig: {
                    esc: function() {
                        me.collapse();
                    }
                }
            });
      }
  });


/**
 使用方法
{
    xtype:'datetimefield',
    width : 300,
    labelWidth : 80,
    endDateField:'etime',
    vtype:'daterange',
    fieldLabel: '记录时间下限',
    format: 'Y-m-d H:i:s ',
    name:'stime'
},
{
    xtype:'datetimefield',
    width : 300,
    labelWidth : 80,
    startDateField:'stime',
    vtype:'daterange',
    fieldLabel: '记录时间上限',
    format: 'Y-m-d H:i:s ',
    name:'etime'
},
 */