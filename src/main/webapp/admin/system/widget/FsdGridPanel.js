/*
 * @author lumingbao
 * 表格面板
 */
Ext.define('system.widget.FsdGridPanel',{
	extend : 'Ext.grid.GridPanel',
	alias: 'fsdgridpanell',
	model : '',
	column : '',
	baseUrl : '',
	stripeRows : true, // 斑马线效果
	loadMask : true, // 显示遮罩和提示功能,即加载Loading……
	forceFit : true, // 自动填满表格
	selType : "checkboxmodel", // 多选框选择模式，selType : 'rowmodel'无复选框
	multiSelect : true,
	
	zAutoLoad : true,//是否自动加载数据
	
	initComponent : function(){
		var me = this;
		var store = Ext.create('Ext.data.Store', {
            model: me.model,
            proxy: {
                type : 'ajax',
                url : me.baseUrl,
                timeout: 300000,//300秒
                reader : { root: 'data'}
            },
            autoLoad : me.zAutoLoad
        });
        Ext.apply(this, {
        	store : store,
        	columns : Ext.create(me.column).columns
        });
        this.callParent(arguments);
        
        //grid注册选择事件，显示ID信息
        system.UtilStatic.setGridChooseObjID(me);
	}
});