/**
 * 回收站操作类
 * @author lumingbao
 */

Ext.define('system.email.star.biz.StarOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 删除邮件
     */
     deleted : function(grid){
   	 var me = this;
		 // 获取选中的行
	     var data = grid.getSelectionModel().getSelection();
         if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要删除的邮件！');
         }else {
       	 Ext.MessageBox.show({
       		 title : '询问',
       		 msg : '您确定要删除邮件吗?',
			     width : 250,
				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                    if (btn == 'yes') {
                   	    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
            			});
            			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('z_email/deleted_email.do' , param ,
                        function(response, options){
                       	grid.getStore().reload();
                       });
                    }
                }
            });
        }

	},
	
	/**
     * 取消星标
     */
     nostar : function(grid){
   	 var me = this;
		 // 获取选中的行
	     var data = grid.getSelectionModel().getSelection();
         if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要取消星标的邮件！');
         }else {
       	 Ext.MessageBox.show({
       		 title : '询问',
       		 msg : '您确定要让本邮件取消星标吗?',
			     width : 250,
				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                    if (btn == 'yes') {
                   	    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
            			});
            			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('z_email/nostar_email.do' , param ,
                        function(response, options){
                       	grid.getStore().reload();
                       });
                    }
                }
            });
        }

	},
	
	/**
     * 彻底删除邮件
     */
    reldelete : function(grid){
   	 var me = this;
		 // 获取选中的行
	     var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要彻底删除的邮件！');
        }else {
       	 Ext.MessageBox.show({
       		 title : '询问',
       		 msg : '您确定要彻底删除邮件吗?',
			     width : 250,
				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                    if (btn == 'yes') {
                   	    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
            			});
            			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('z_email/reldel_email.do' , param ,
                        function(response, options){
                       	grid.getStore().reload();
                       });
                    }
                }
            });
        }
	},
	
	/**
     * 是否已读
     */
	isEnabledMain : function(grid, isQT){
  		this.isEnabled(grid, isQT, 'z_email/enabled_email_star.do');
  	 },
	
	isEnabled : function(grid, isQT, url){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少选择一份邮件！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
		        msg : '您确定要修改邮件阅读状态吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
					    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
			            });
             			var pram = {ids : dir, isenabled : isQT};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest(url, param,
                        function(response, options){
            				Ext.MessageBox.alert('提示', '项目状态修改成功！');
                        	grid.getStore().reload(); 
                        });
                    }
                }
            });
        }
	},
	
	 /**
     * 邮件详情查看
     */
 	 view : function(grid){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选择要查看的邮件！');
        }else if(data.length > 1){
        	Ext.MessageBox.alert('提示', '每次只能查看一份邮件！');
        }else {
        	// 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = window.open("emailView-"+ id +".htm");
            win.modal = true;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
        }
	},
	
    /**
     * 查看
     */
	view : function(grid, title){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要查看的信件！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能查看一份信件！');
        }else{

            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            
            var win = Ext.create("system.email.star.biz.StarEdit");
            win.setTitle(title);
            win.modal = true;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'z_email/load_EmailByid.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	            grid.getStore().reload();
	        });
        }
	},
});