﻿/**
 * 栏目添加界面
 * @author lumingbao
 */
Ext.define('system.email.star.biz.StarEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	treepanel : null,
	oper : Ext.create('system.email.inbox.biz.InboxOper'),
	
	initComponent:function(){
	    var me = this;
	    
	    var xjmc = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '回复标题',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 900,
	    	name: 'messagetitle',
	    	maxLength: 200
	    });
	    
	    var xjnr = Ext.create('system.widget.FsdUeditor',{
	    	fieldLabel: '回复内容',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 920,
	        height : 420,
	    	name: 'messagetext'
	    });
	   
	    me.on('show' , function(){
	    	//sslm.setText(sslmname.getValue());
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 20 10 20',
			items : [{
                name: "id",
                xtype: "hidden"
            } , xjmc, xjnr]
        });
	    
	    Ext.apply(this,{
	    	width:1000,
	        height:600,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [/*{
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
			    handler : function() {
    				me.oper.saveMain(form, me.grid , me.treepanel);
			    }
		    }, {
	        	name : 'btnsave',
			    text : '发送',
			    iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
			    handler : function() {
    				me.oper.submitMain(form, me.grid , me.treepanel);
			    }
		    },*/ {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});