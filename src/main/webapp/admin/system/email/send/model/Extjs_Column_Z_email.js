﻿/**
 *b_article Column
 *@author CodeSystem
 *文件名     Extjs_Column_B_article
 *Columns名  column_b_article
 */

Ext.define('column_z_email', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '是否星标',
            dataIndex : 'isstar',
            sortable : true,
            flex : 2,
            renderer :function(value ,metaData ,record ){
				if(value == 'true'){
					value="<font style='color:green;'>是</font>";
				}else if(value == 'false'){
					value="<font style='color:red;'>否</font>";
				}
				return value;
			}
        }
        ,
        {
            header : '收件人',
            dataIndex : 'recname',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '信件标题',
            dataIndex : 'messagetitle',
            sortable : true,
            flex : 4
        }
        ,
        {
            header : '是否阅读',
            dataIndex : 'readstatus',
            sortable : true,
            flex : 2,
            renderer :function(value ,metaData ,record ){
				if(value == '已读'){
					value="<font style='color:green;'>已读</font>";
				}else if(value == '未读'){
					value="<font style='color:red;'>未读</font>";
				}
				return value;
			}
        }
        ,
        {
            header : '发送时间',
            dataIndex : 'senddate',
            sortable : true,
            flex : 4,
			renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value);
			}
        }
    ]
});