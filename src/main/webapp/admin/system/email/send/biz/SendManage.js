﻿/**
 * 已发送邮件管理界面
 * @author lumingbao
 */

Ext.define('system.email.send.biz.SendManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*', 
	             'system.email.send.model.Extjs_Column_Z_email',
	             'system.email.send.model.Extjs_Model_Z_email'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.email.send.biz.SendOper'),
	
	createContent : function(){
		var me = this;
		
		var queryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入邮件标题',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});
		
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_email', // 显示列
			model: 'model_z_email',
			baseUrl : 'z_email/load_SendEmail.do',
			border : false,
			tbar : [{
				xtype : "fsdbutton",
				popedomCode : 'nrwzbj',
				text : "写信",
				iconCls : 'articlewz',
				handler : function(button) {
				    me.oper.addText(grid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'rytj',
				text : "星标邮件",
				iconCls : 'starIcon',
				handler : function(button) {
				    me.oper.star(grid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'rytj',
				text : "查看",
				iconCls : 'magnifierIcon',
				handler : function(button) {
				    me.oper.view(grid);
				}
			}, {
				xtype : "fsdbutton",
				text : "删除",
				iconCls : 'arrow_redoIcon',
				handler : function(button) {
					me.oper.deleted(grid);
				}
			}, {
				xtype : "button",
				text : "彻底删除",
				iconCls : 'page_deleteIcon',
				handler : function(button) {
				    me.oper.reldelete(grid);
				}
			},'->', queryText, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]
		});
		
		grid.getStore().on('beforeload', function(s) {
			var title =  queryText.getValue();
			var pram = {title : title};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		return grid;
	}
});