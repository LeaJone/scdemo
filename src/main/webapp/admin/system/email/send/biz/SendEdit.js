﻿/**
 * 栏目添加界面
 * @author lumingbao
 */
Ext.define('system.email.send.biz.SendEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	treepanel : null,
	oper : Ext.create('system.email.send.biz.SendOper'),
	requires : [ 'system.abasis.employee.model.Extjs_A_employee'],
	
	initComponent:function(){
	    var me = this;
	    
	    me.username = Ext.create('system.widget.FsdTextGrid', {
	    	zFieldLabel : '收件人', 
	    	zLabelWidth : 80,
	    	width : 900,
		    zName : 'recname',
		    zColumn : 'column_a_employeeemail', // 显示列
		    zModel : 'model_a_employee',
		    zBaseUrl : 'A_Employee/load_data2.do',
		    zAllowBlank : false,
		    zGridType : 'list',
			zGridWidth : 700,//弹出表格宽度
			zGridHeight : 600,//弹出表格高度
		    zIsText1 : true,
		    zTxtLabel1 : '收件人',
		    zFunChoose : function(data){
		    	me.userid.setValue(data.id);
		    	me.username.setValue(data.realname);
		    },
		    zFunQuery : function(txt1, txt2, txt3, txt4){
		    	return {fid : 'FSDCOMPANY', txt : txt1};
		    }
	    });
		me.userid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '用户编号',
	    	name: 'recid'
	    });
	    
	    var xjmc = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '信件标题',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 900,
	    	name: 'messagetitle',
	    	maxLength: 200
	    });
	    
	    var xjnr = Ext.create('system.widget.FsdUeditor',{
	    	fieldLabel: '信件内容',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 920,
	        height : 420,
	    	name: 'messagetext'
	    });
	   
	    me.on('show' , function(){
	    	//sslm.setText(sslmname.getValue());
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 20 10 20',
			items : [{
                name: "id",
                xtype: "hidden"
            } , me.username, me.userid, xjmc, xjnr]
        });
	    
	    Ext.apply(this,{
	    	width:1000,
	        height:600,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
			    handler : function() {
    				me.oper.saveMain(form, me.grid , me.treepanel);
			    }
		    }, {
	        	name : 'btnsave',
			    text : '发送',
			    iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
			    handler : function() {
    				me.oper.submitMain(form, me.grid , me.treepanel);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});