/**
 * 草稿箱操作类
 * @author lumingbao
 */

Ext.define('system.email.draft.biz.DraftOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 星标邮件
     */
	star : function(grid){
   	 var me = this;
		 // 获取选中的行
	     var data = grid.getSelectionModel().getSelection();
         if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要星标的邮件！');
         }else {
       	 Ext.MessageBox.show({
       		 title : '询问',
       		 msg : '您确定要星标邮件吗?',
			     width : 250,
				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                    if (btn == 'yes') {
                   	    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
            			});
            			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('z_email/star_email.do' , param ,
                        function(response, options){
                       	grid.getStore().reload();
                       });
                    }
                }
            });
        }

	},
	
	/**
     * 删除邮件
     */
	deleted : function(grid){
   	 var me = this;
		 // 获取选中的行
	     var data = grid.getSelectionModel().getSelection();
         if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要删除的邮件！');
         }else {
       	 Ext.MessageBox.show({
       		 title : '询问',
       		 msg : '您确定要删除邮件吗?',
			     width : 250,
				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                    if (btn == 'yes') {
                   	    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
            			});
            			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('z_email/deleted_email.do' , param ,
                        function(response, options){
                       	grid.getStore().reload();
                       });
                    }
                }
            });
        }

	},
	
	/**
     * 彻底删除邮件
     */
    reldelete : function(grid){
   	 var me = this;
		 // 获取选中的行
	     var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要彻底删除的邮件！');
        }else {
       	 Ext.MessageBox.show({
       		 title : '询问',
       		 msg : '您确定要彻底删除邮件吗?',
			     width : 250,
				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                    if (btn == 'yes') {
                   	    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
            			});
            			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('z_email/reldel_email.do' , param ,
                        function(response, options){
                       	grid.getStore().reload();
                       });
                    }
                }
            });
        }
	},
	
	 /**
     * 邮件详情查看
     */
 	 view : function(grid){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选择要查看的邮件！');
        }else if(data.length > 1){
        	Ext.MessageBox.alert('提示', '每次只能查看一份邮件！');
        }else {
        	// 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = window.open("emailView-"+ id +".htm");
            win.modal = true;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
        }
	},
});