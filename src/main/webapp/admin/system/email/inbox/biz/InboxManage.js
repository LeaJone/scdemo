﻿/**
 * 草稿箱管理界面
 * @author lumingbao
 */

Ext.define('system.email.inbox.biz.InboxManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*', 
	             'system.email.inbox.model.Extjs_Column_Z_email',
	             'system.email.inbox.model.Extjs_Model_Z_email'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.email.inbox.biz.InboxOper'),
	
	createContent : function(){
		var me = this;
		
		var queryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入邮件标题',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});
		
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_z_email', // 显示列
			model: 'model_z_email',
			baseUrl : 'z_email/load_InboxEmail.do',
			border : false,
			tbar : [
				{
					xtype : "fsdbutton",
					text : "查看",
					iconCls : 'magnifierIcon',
					handler : function(button) {
					    me.oper.view(grid);
					}
				},/* {
					xtype : "fsdbutton",
					text : "回复",
					iconCls : 'page_editIcon',
					handler : function(button) {
					    me.oper.answer(grid);
					}
				}, */{
				xtype : "fsdbutton",
				text : "星标邮件",
				iconCls : 'starIcon',
				handler : function(button) {
				    me.oper.star(grid);
				}
			}, {
				xtype : "fsdbutton",
				text : "删除",
				iconCls : 'page_deleteIcon',
				handler : function(button) {
					me.oper.deleted(grid);
				}
			}, {
				xtype : "button",
				text : "彻底删除",
				iconCls : 'page_deleteIcon',
				handler : function(button) {
				    me.oper.reldelete(grid);
				}
			}, {
				xtype : "fsdbutton",
				text : "标为已读",
				iconCls : 'bookopenIcon',
				handler : function(button) {
				    me.oper.isEnabledMain(grid, true);
				}
			}, {
				xtype : "fsdbutton",
				text : "标为未读",
				iconCls : 'bookIcon',
				handler : function(button) {
				    me.oper.isEnabledMain(grid, false);
				}
			},'->', queryText, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]
		});
		
		grid.getStore().on('beforeload', function(s) {
			var title =  queryText.getValue();
			var pram = {title : title};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		return grid;
	}
});