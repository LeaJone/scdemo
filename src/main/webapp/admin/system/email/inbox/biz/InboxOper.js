/**
 * 草稿箱操作类
 * @author lumingbao
 */

Ext.define('system.email.inbox.biz.InboxOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 星标邮件
     */
	star : function(grid){
   	 var me = this;
		 // 获取选中的行
	     var data = grid.getSelectionModel().getSelection();
         if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要星标的邮件！');
         }else {
       	 Ext.MessageBox.show({
       		 title : '询问',
       		 msg : '您确定要星标邮件吗?',
			     width : 250,
				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                    if (btn == 'yes') {
                   	    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
            			});
            			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('z_email/star_email.do' , param ,
                        function(response, options){
                       	grid.getStore().reload();
                       });
                    }
                }
            });
        }

	},
	
	/**
     * 删除邮件
     */
	deleted : function(grid){
   	 var me = this;
		 // 获取选中的行
	     var data = grid.getSelectionModel().getSelection();
         if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要删除的邮件！');
         }else {
       	 Ext.MessageBox.show({
       		 title : '询问',
       		 msg : '您确定要删除邮件吗?',
			     width : 250,
				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                    if (btn == 'yes') {
                   	    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
            			});
            			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('z_email/deleted_email.do' , param ,
                        function(response, options){
                       	grid.getStore().reload();
                       });
                    }
                }
            });
        }

	},
	
	/**
     * 彻底删除邮件
     */
    reldelete : function(grid){
   	 var me = this;
		 // 获取选中的行
	     var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要彻底删除的邮件！');
        }else {
       	 Ext.MessageBox.show({
       		 title : '询问',
       		 msg : '您确定要彻底删除邮件吗?',
			     width : 250,
				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                    if (btn == 'yes') {
                   	    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
            			});
            			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('z_email/reldel_email.do' , param ,
                        function(response, options){
                       	grid.getStore().reload();
                       });
                    }
                }
            });
        }
	},
	
	 /**
     * 邮件详情查看
     */
 	 view : function(grid){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选择要查看的邮件！');
        }else if(data.length > 1){
        	Ext.MessageBox.alert('提示', '每次只能查看一份邮件！');
        }else {
        	// 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = window.open("emailView-"+ id +".htm");
            win.modal = true;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
        }
	},
	
	/**
     * 是否已读
     */
	isEnabledMain : function(grid, isQT){
  		this.isEnabled(grid, isQT, 'z_email/enabled_email_star.do');
  	 },
	
	isEnabled : function(grid, isQT, url){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少选择一份邮件！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
		        msg : '您确定要修改邮件阅读状态吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
					    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
			            });
             			var pram = {ids : dir, isenabled : isQT};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest(url, param,
                        function(response, options){
            				Ext.MessageBox.alert('提示', '项目状态修改成功！');
                        	grid.getStore().reload(); 
                        });
                    }
                }
            });
        }
	},
	
	/**
     * 回信
     */
	answer : function(grid){
		var win = Ext.create('system.email.inbox.biz.InboxEdit');
        win.setTitle('回信');
        win.grid = grid;
        win.modal = true;
        win.show();
	},
	
	/**
     * 信件保存
     */
	saveMain : function(formpanel , grid , treepanel){
		this.formSave(formpanel , grid, treepanel, 'z_email/save_email_first.do');
	},
	formSave : function(formpanel , grid , treepanel, url){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit(url, formpanel.form, '正在保存信件,请稍候.....',
			function(form, action){
			    formpanel.up('window').close();
			    grid.getStore().reload();
			    treepanel.getStore().load();
				Ext.MessageBox.alert('提示', '信件保存成功！');
			});
        }
     },
	
	/**
     * 信件提交
     */
	submitMain : function(formpanel , grid , treepanel){
		this.formSubmit(formpanel , grid, treepanel, 'z_email/save_email.do');
	},
	formSubmit : function(formpanel , grid , treepanel, url){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit(url, formpanel.form, '正在发送信件,请稍候.....',
			function(form, action){
			    formpanel.up('window').close();
			    grid.getStore().reload();
			    treepanel.getStore().load();
				Ext.MessageBox.alert('提示', '信件回复成功！');
			});
        }
     },
     
     /**
      * 查看
      */
 	view : function(grid, title){
 		var me = this;
 		// 获取选中的行
 	    var data = grid.getSelectionModel().getSelection();
         if (data.length == 0) {
             Ext.MessageBox.alert('提示', '请先选中要查看的信件！');
         }else if(data.length > 1){
             Ext.MessageBox.alert('提示', '每次只能查看一份信件！');
         }else{

             // 先得到主键
             var id = data[0].data.id;
             //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
             
             var win = Ext.create("system.email.inbox.biz.InboxEdit");
             win.setTitle(title);
             win.modal = true;
             var pram = {id : id};
             var param = {jsonData : Ext.encode(pram)};
 	        me.util.formLoad(win.down('form').getForm() , 'z_email/load_EmailByid.do' , param , null , 
 	        function(response, options, respText){
 	            win.show();
 	            grid.getStore().reload();
 	        });
         }
 	},
});