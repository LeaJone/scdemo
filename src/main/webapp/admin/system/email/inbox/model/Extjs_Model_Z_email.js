﻿﻿/**
 *b_article Model
 *@author CodeSystem
 *文件名     Extjs_Model_B_article
 *Model名    model_b_article
 */

Ext.define('model_z_email', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'messagetitle',
            type : 'string'
        }
        ,
        {
            name : 'messagetext',
            type : 'string'
        }
        ,
        {
            name : 'savetime',
            type : 'string'
        }
        ,
        {
            name : 'pushstatusid',
            type : 'string'
        }
        ,
        {
            name : 'pushstatus',
            type : 'string'
        }
        ,
        {
            name : 'sendid',
            type : 'string'
        }
        ,
        {
            name : 'sendname',
            type : 'string'
        }
        ,
        {
            name : 'senddate',
            type : 'string'
        }
        ,
        {
            name : 'recid',
            type : 'string'
        }
        ,
        {
            name : 'recname',
            type : 'string'
        }
        ,
        {
            name : 'readstatusid',
            type : 'string'
        }
        ,
        {
            name : 'readstatus',
            type : 'string'
        }
        ,
        {
            name : 'deleted',
            type : 'string'
        }
        ,
        {
            name : 'isstar',
            type : 'string'
        }
    ]
});