/**
 * 知识库界面操作类
 */

Ext.define('system.iquestions.knowledgeInfo.biz.knowledgeInfoOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 添加知识
     */
	addText : function(grid){
		var me = this;
		var model = Ext.create("system.iquestions.knowledgeInfo.biz.knowledgeInfoEditPanel");
		model.grid = grid;
		me.util.addTab("addzsbj" , "添加知识信息" , model , "resources/admin/icon/vcard_add.png");
	},
	
	
	/**
     * 表单提交
     */
	formSubmit : function(formpanel, grid){
        var me = this;
        if (formpanel.form.form.isValid()) {
            me.util.ExtFormSubmit('i_knowledgeinfo/save_i_knowledgeinfo_qx_i_knowledgeinfobj.do' , formpanel.form.form , '正在提交数据,请稍候.....',
			function(form, action, respText){
            	if(grid != null){
            		grid.getStore().reload();//刷新管理界面的表格
            	}
            	formpanel.idNo.setValue(action.result.id);
				Ext.MessageBox.alert('提示', '保存成功！');
			});
        }
     },
 	
 	
     
     /**
      * 删除
      */
     del : function(grid){
    	 var me = this;
 		 // 获取选中的行
 	     var data = grid.getSelectionModel().getSelection();
         if (data.length == 0) {
             Ext.MessageBox.alert('提示', '请先选中要删除的记录！');
         }else {
        	 Ext.MessageBox.show({
        		 title : '询问',
        		 msg : '您确定要删除吗?',
 			     width : 250,
 				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                     if (btn == 'yes') {
                    	 var dir = new Array();　
                         Ext.Array.each(data, function(items) {
                             var id = items.data.id;
                             dir.push(id);
             			});
             			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('i_knowledgeinfo/del_i_knowledgeinfo_qx_i_knowledgeinfosc.do' , param ,
                        function(response, options){
                        	grid.getStore().reload();
                        }, null, me.form);
                     }
                 }
             });
         }

 	},
 	
 	/**
     * 修改
     */
	editor : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的记录！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个记录！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
    		var model = me.util.getTab("editzsbj");
    		if (model == null){
    			model = Ext.create("system.iquestions.knowledgeInfo.biz.knowledgeInfoEditPanel");
    			model.grid = grid;
    		}
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(model.form, 'i_knowledgeinfo/load_i_knowledgeinfobyid.do', param, null, 
            function(response, options, respText, data){
            	me.util.addTab("editzsbj" , "修改知识信息" , model , "resources/admin/icon/vcard_edit.png");
            });
        }
	},
 	
 	
 	/**
     * 查看
     */
	view : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要查看的记录！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个记录！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            var model = Ext.create("system.iquestions.knowledgeInfo.biz.knowledgeInfoEditPanel");
            model.viewObject();
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            me.util.formLoad(model.form, 'i_knowledgeinfo/load_i_knowledgeinfobyid.do', param, null, 
            function(response, options, respText, data){
            	me.util.addTab(id + "viewzsbj", "查看知识信息", model, "resources/admin/icon/vcard_edit.png");
            });
        }
	},
	
 	
 	
	/**
     * 是否审核
     */
	isEnabled : function(grid, isQT){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
		        msg : '您确定要修改审核状态吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
					    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
			            });
             			var pram = {ids : dir, isenabled : isQT};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('i_knowledgeinfo/enabled_qdnrb_qx.do' , param ,
                        function(response, options){
            				Ext.MessageBox.alert('提示', '审核状态修改成功！');
                        	grid.getStore().reload(); 
                        });
                    }
                }
            });
        }
	}
});