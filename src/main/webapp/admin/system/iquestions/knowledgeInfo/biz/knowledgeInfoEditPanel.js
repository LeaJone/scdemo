/**
 * 知识库编辑界面
 * @author 
 */

Ext.define('system.iquestions.knowledgeInfo.biz.knowledgeInfoEditPanel', {
	extend : 'Ext.panel.Panel',
	oper : Ext.create('system.iquestions.knowledgeInfo.biz.knowledgeInfoOper'),
	
	grid : null,//管理表格
	formWin : null,//编辑窗体
	
	viewObject : function(){
		var me = this;
		me.down('button[name=btnsave]').setVisible(false);
		me.down('button[name=btnnew]').setVisible(false);
	},
	
	initComponent : function() {
		var me = this;
		
		me.idNo = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '编号',
	    	name : 'id'
	    });

	    var bankinfoname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属题库名称',
	    	name: 'bankinfoname'
	    });
	    var bankinfoid = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属库',
	    	labelAlign:'right',
	    	labelWidth:80,
	    	rootText : '知识库',
	    	rootId : 'FSDKNOWLEDGE',
	        width : 450,
	        name : 'bankinfoid',
	        baseUrl:'i_bankinfo/load_AsyncObjectTree.do',//访问路劲
	        allowBlank: false,
	        hiddenName : 'bankinfoname'//隐藏域Name
	    });
		
	    
		var questiontypename = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '题目分类名称',
	    	name: 'questiontypename'
	    });
	    var questiontypeid = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属分类',
	    	labelAlign:'right',
	    	labelWidth:80,
	    	rootText : '题目分类',
	    	rootId : 'FSDQUESTIONTYPE',
			rootVisible: true,
	        width : 450,
	        name : 'questiontypeid',
	        baseUrl:'i_bankquestiontype/load_AsyncObjectTree.do',//访问路劲
	        allowBlank: false,
	        hiddenName : 'questiontypename'//隐藏域Name
	    });
	    
	    var aurl = Ext.create('system.widget.FsdTextCheckbox',{
	    	zNameCheckbox : 'isaurl',
	    	zFieldLabel : '外链接',
	    	zLabelAlign : 'right',
	    	zLabelWidth : 80,
	    	zInputValue : 'true',
	    	zNameText : 'aurl',
	        width : 450,
	        zMaxLength : 400
	    });
	    
	    var sourceurl = Ext.create('system.widget.FsdTextCheckbox',{
	    	zNameCheckbox : 'sourceisurl',
	    	zFieldLabel : '来源链接',
	    	zLabelAlign : 'right',
	    	zLabelWidth : 80,
	    	zInputValue : 'true',
	    	zNameText : 'sourceurl',
	        width : 450,
	        zMaxLength : 400
	    });
	    
	    var source = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '来源',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 450,
	    	name: 'source',
	    	maxLength: 25
	    });
	    
	    var title = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '标题',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 900,
	    	name: 'title',
	    	maxLength: 100,
	    	allowBlank: false
	    });
	    
	    var subtitle = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '副标题',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 900,
	    	name: 'subtitle',
	    	maxLength: 100
	    });
	    
	    var showtitle = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '显示标题',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 900,
	    	name: 'showtitle',
	    	maxLength: 100,
	    	allowBlank: false
	    });
	    
	    var abstracts = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '关键词',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 900,
	    	name: 'abstracts',
	    	maxLength: 250
	    });
	    
	    var content = Ext.create('system.widget.FsdUeditor',{
	    	fieldLabel: '内容',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 920,
	        height : 400,
	    	name: 'content'
	    });
	    
	    var keynote = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '重点',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 900,
	    	name: 'keynote',
	    	maxLength: 250,
			padding : '25 0 0 0'
	    });
	    
	    var parse = Ext.create('system.widget.FsdUeditor',{
	    	fieldLabel: '解析',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 920,
	        height : 400,
	    	name: 'parse'
	    });
	    
	    var levelname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '难易程度名称',
	    	name: 'levelname'
	    });
	    var levelcode = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'难易程度',
	        labelAlign:'right',
	    	labelWidth:80,
	    	width : 450,
    	    name : 'levelcode',//提交到后台的参数名
	        key : 'i_nycd',//参数
	        allowBlank: false,
	        zHiddenObject : levelname,
	        zDefaultCode : 'i_nywz'
	    });
	    
	    var keynotename = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '重点程度名称',
	    	name: 'keynotename'
	    });
	    var keynotecode = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'重点程度',
	        labelAlign:'right',
	    	labelWidth:80,
	    	width : 450,
    	    name : 'keynotecode',//提交到后台的参数名
	        key : 'i_zdcd',//参数
	        allowBlank: false,
	        zHiddenObject : keynotename,
	        zDefaultCode : 'i_zdwz'
	    });
	    
	    var imageurl1 = Ext.create('system.widget.FsdTextFileManage', {
	        width : 450,
	    	zName : 'imageurl1',
	    	zFieldLabel : '标题图片',
	    	zLabelWidth : 80,
	    	zIsShowButton : true,
	    	zIsGetButton : true,
	    	zIsUpButton : true,
	    	zIsReadOnly : false,
	    	zContentObj : content,
	    	zFileUpPath : me.oper.util.getImagePath()
	    });
	    
	    var videourl = Ext.create('system.widget.FsdTextFileManage', {
	        width : 450,
	    	zName : 'videourl',
	    	zFieldLabel : '内容视频',
	    	zLabelWidth : 80,
	    	zIsShowButton : true,
	    	zIsUpButton : true,
	    	zIsReadOnly : false,
	    	zFileType : 'video',
	    	zFileAutoName : false,
	    	zFileUpPath : me.oper.util.getVideoPath(),
	    	zManageType : 'manage'
	    });
	    
	    var voiceurl = Ext.create('system.widget.FsdTextFileManage', {
	        width : 450,
	    	zName : 'voiceurl',
	    	zFieldLabel : '内容语音',
	    	zLabelWidth : 80,
	    	zIsShowButton : true,
	    	zIsUpButton : true,
	    	zIsReadOnly : false,
	    	zFileType : 'voice',
	    	zFileAutoName : false,
	    	zFileUpPath : me.oper.util.getVoicePath(),
	    	zManageType : 'manage'
	    });
	    
	    var remark = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 450,
	    	name: 'remark',
	    	maxLength: 50
	    });
	    
	    var sxh = Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '顺序号',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 450,
	    	name: 'sort',
	    	maxLength: 3,
	    	minValue: 0,
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank: false
	    });
	    
	    me.form = Ext.create('Ext.form.Panel',{
				border : false,
//				trackResetOnLoad : true,
				padding : '10 0 30 20',
				items : [bankinfoname, questiontypename,levelname,keynotename,me.idNo,
                {
                    layout : 'column',
                    border : false,
        	        width : 900,
                    items:[{
                        columnWidth : 0.5,
                        border:false,
                        items:[bankinfoid,levelcode, sxh, source]
                    }, {
                        columnWidth : 0.5,
                        border:false,
                        items:[questiontypeid ,keynotecode, aurl, sourceurl]
                    }]
    			}, 
    			title, subtitle, showtitle, abstracts, content, keynote, parse,
    			{
                    layout : 'column',
    				padding : '25 0 0 0',
                    border : false,
        	        width : 900,
                    items:[{
                        columnWidth : 0.5,
                        border : false,
                        items : [imageurl1, voiceurl]
                    }, {
                        columnWidth : 0.5,
                        border : false,
                        items : [videourl, remark]
                    }]
    			}]
	    });
	    
	    me.on('boxready' , function(obj, width, height, eOpts){
        	questiontypeid.setText(questiontypename.getValue());
        	bankinfoid.setText(bankinfoname.getValue());
	    });
	    
		Ext.apply(me, {
			autoScroll : true,
	        tbar : [{
				xtype : "fsdbutton",
				popedomCode : 'zskbj',
				text : "保存提交",
	        	name : 'btnsave',
				iconCls : 'acceptIcon',
				handler : function(button) {
					me.oper.formSubmit(me, me.grid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'zskbj',
				text : "新增内容",
	        	name : 'btnnew',
				iconCls : 'tbar_synchronizeIcon',
				handler : function(button) {
					var obj = {
						id : null,
						source : null,
						title : null,
						subtitle : null,
						showtitle : null,
						abstracts : null,
						keynote : null,
						parse : null,
						content : null,
						imageurl1 : null,
						videourl : null,
						voiceurl : null,
						sort : null,
						sourceurl : null,
						isaurl : null,
						aurl : null,
						sourceisurl : null,
						remark : null
					};
					me.form.getForm().setValues(obj);
				}
			}],
	        items :[me.form]
	    });
		me.callParent(arguments);
	}
});