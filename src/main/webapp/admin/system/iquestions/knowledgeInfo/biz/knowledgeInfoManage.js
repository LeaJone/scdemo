/**
 * 知识库管理界面
 * @author 
 */

Ext.define('system.iquestions.knowledgeInfo.biz.knowledgeInfoManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.iquestions.knowledgeInfo.model.Extjs_Column_I_knowledgeInfo',
	             'system.iquestions.knowledgeInfo.model.Extjs_Model_I_knowledgeInfo',
	             'system.admin.tools.model.Extjs_Column_SortOperate',
		         'system.admin.tools.model.Extjs_Model_SortOperate'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.iquestions.knowledgeInfo.biz.knowledgeInfoOper'),
	
	createContent : function(){
		var me = this;
		
		var queryid = 'FSDKNOWLEDGE';
		var typeCode = 'FSDKNOWLEDGE';
		var typeName = '知识库分类';
		
		/**
		 * 左边的树面板
		 */
		var treepanel = Ext.create("system.widget.FsdTreePanel", {
			icon : 'resources/admin/icon/chart_organisation.png',
			baseUrl : 'i_bankinfo/load_AsyncObjectTree.do',
			title: typeName + '结构',
			rootText : typeName,
			rootId : typeCode,
			region : 'west', // 设置方位
			collapsible : true,//折叠
			width: 250,
			split: true
		});
		treepanel.on('itemclick', function(view,record,item,index,e){
			queryid = record.raw.id;
			grid.getStore().loadPage(1);
		});
		
		
		var queryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入题库名称',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});
		
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_i_knowledgeInfo', // 显示列
			model: 'model_i_knowledgeInfo',
			baseUrl : 'i_knowledgeinfo/load_pagedata.do',
			
			tbar : [{
				xtype : "button",
				text : "添加",
				iconCls : 'page_addIcon',
				popedomCode : 'tkbj',
				handler : function(button) {
					me.oper.addText(grid);
				}
			}, {
				xtype : "button",
				text : "修改",
				iconCls : 'page_editIcon',
				popedomCode : 'tkbj',
				handler : function(button) {
					me.oper.editor(grid);
				}
			}, {
				xtype : "button",
				text : "删除",
				iconCls : 'page_deleteIcon',
				popedomCode : 'tksc',
				handler : function(button) {
				    me.oper.del(grid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid);
				}
			}, {
				text : '排序',
				iconCls : 'up',
				handler : function() {
					var name =  queryText.getValue();
					var pram = {fid : queryid , name : name};
					me.oper.util.sortOperate(
							grid, 
							'column_title', 
							'model_sortoperate',
							grid.baseUrl,
							'i_knowledgeinfo/sort_i_knowledgeinfo.do',
							function(){return pram;},
				        	function(){treepanel.getStore().load();});
				}
			}, '-' ,{
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
				text : "审核通过",
				iconCls : 'tickIcon',
				handler : function(button) {
				    me.oper.isEnabled(grid, true);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'dwbj',
				text : "取消审核",
				iconCls : 'crossIcon',
				handler : function(button) {
				    me.oper.isEnabled(grid, false);
				}
			}, '->', queryText , {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  queryText.getValue();
			var pram = {fid : queryid , name : name};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		var contentpanel = Ext.create("Ext.panel.Panel", {
			frame:true,
			split: true,
			region: 'center',
			layout: 'fit',
			title: '分类信息',
			items: [grid]
		});

		var page1_jExtPanel1_obj = new Ext.panel.Panel({
			bodyPadding: 3,
			header : false,
			border : 0,
			split: true,
			layout: 'border',
			items: [treepanel,contentpanel]
		});
		return page1_jExtPanel1_obj;
	}
});