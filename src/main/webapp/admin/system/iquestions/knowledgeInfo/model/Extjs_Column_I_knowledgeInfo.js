/**
 *i_knowledgeInfo Column
 *@author CodeSystem
 *文件名     Extjs_Column_I_knowledgeInfo
 *Columns名  column_i_knowledgeInfo
 */

Ext.define('column_i_knowledgeInfo', {
    columns: [
          {
              xtype : 'rownumberer',
              text : 'NO',
              sortable : true,
              width : 40
          }
          ,
          {
              header : '标题',
              dataIndex : 'title',
              sortable : true,
              flex : 6
          }
          ,
        {
            header : '库称',
            dataIndex : 'bankinfoname',
            sortable : true,
            flex : 3
        }
        ,
        {
            header : '题目分类',
            dataIndex : 'questiontypename',
            sortable : true,
            flex : 3
        }
        ,
        {
            header : '外链接',
            dataIndex : 'isaurl',
            sortable : true,
            flex : 1,
			renderer :function(value ,metaData ,record ){
				if(value == 'true'){
					value="<font style='color:blue;'>是</font>";
				}else{
					value="<font style='color:gray;'>否</font>";
				}
				return value;
			}
        }
        ,
        {
            header : '图片',
            dataIndex : 'imageurl1',
            sortable : true,
            flex : 1,
			renderer :function(value ,metaData ,record ){
				if(value != ''){
					value="<font style='color:blue;'>是</font>";
				}else{
					value='';
				}
				return value;
			}
        }
        ,
        {
            header : '视频',
            dataIndex : 'videourl',
            sortable : true,
            flex : 1,
			renderer :function(value ,metaData ,record ){
				if(value != ''){
					value="<font style='color:blue;'>是</font>";
				}else{
					value='';
				}
				return value;
			}
        }

        ,
        {
            header : '语音',
            dataIndex : 'voiceurl',
            sortable : true,
            flex : 1,
			renderer :function(value ,metaData ,record ){
				if(value != ''){
					value="<font style='color:blue;'>是</font>";
				}else{
					value='';
				}
				return value;
			}
        }
        ,
        {
            header : '新增时间',
            dataIndex : 'adddate',
            sortable : true,
            flex : 3,
            renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value);
			}
        }
        ,
        {
            header : '审核',
            dataIndex : 'auditing',
            sortable : true,
            flex : 1,
			renderer :function(value ,metaData ,record ){
				if(value == 'true'){
					value="<font style='color:green;'>审</font>";
				}else{
					value="<font style='color:red;'>停</font>";
				}
				return value;
			}
        }
        ,
        {
            header : '顺序号',
            dataIndex : 'sort',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '备注',
            dataIndex : 'remark',
            sortable : true,
            flex : 2
        }
    ]
});