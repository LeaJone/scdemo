/**
 *i_knowledgeInfo Model
 *@author CodeSystem
 *文件名     Extjs_Model_I_knowledgeInfo
 *Model名    model_i_knowledgeInfo
 */

Ext.define('model_i_knowledgeInfo', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'bankinfoname',
            type : 'string'
        }
             ,
        {
            name : 'levelname',
            type : 'string'
        }
        ,
        {
            name : 'questiontypename',
            type : 'string'
        }
        ,
        {
            name : 'title',
            type : 'string'
        }
        ,
        {
            name : 'isaurl',
            type : 'string'
        }
        ,
        {
            name : 'imageurl1',
            type : 'string'
        }
        ,
        {
            name : 'videourl',
            type : 'string'
        }
        ,
        {
            name : 'voiceurl',
            type : 'string'
        }
        ,
        {
            name : 'adddate',
            type : 'string'
        }
        ,
        {
            name : 'auditing',
            type : 'string'
        }
        ,
        {
            name : 'sort',
            type : 'string'
        }
        ,
        {
            name : 'remark',
            type : 'string'
        }
    ]
});