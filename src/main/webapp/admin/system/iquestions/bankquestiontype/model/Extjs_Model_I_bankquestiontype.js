/**
 *i_bankQuestionType Model
 *@author CodeSystem
 *文件名     Extjs_Model_I_bankquestiontype
 *Model名    model_i_bankQuestionType
 */

Ext.define('model_i_bankQuestionType', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
             ,
        {
            name : 'name',
            type : 'string'
        }
        ,
        {
            name : 'description',
            type : 'string'
        }
        ,
        {
            name : 'sort',
            type : 'string'
        }
        ,
        {
            name : 'adddate',
            type : 'string'
        }
        ,
        {
            name : 'remark',
            type : 'string'
        }
    ]
});
