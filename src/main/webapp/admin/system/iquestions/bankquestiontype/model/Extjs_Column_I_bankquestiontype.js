/**
 *i_bankQuestionType Column
 *@author CodeSystem
 *文件名     Extjs_Column_I_bankquestiontype
 *Columns名  column_i_bankQuestionType
 */

Ext.define('column_i_bankQuestionType', {
    columns: [
        {
            header : '名称',
            dataIndex : 'name',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '描述',
            dataIndex : 'description',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '顺序号',
            dataIndex : 'sort',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '新增时间',
            dataIndex : 'adddate',
            sortable : true,
            flex : 1,
            renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value);
			}
        }
        ,
        {
            header : '备注',
            dataIndex : 'remark',
            sortable : true,
            flex : 1
        }
    ]
});