/**
 * 题目分类界面操作类
 */

Ext.define('system.iquestions.bankquestiontype.biz.bankquestiontypeOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 添加
     */
	addMain : function(grid , treepanel){
	    var win = Ext.create('system.iquestions.bankquestiontype.biz.bankquestiontypeEdit');
        win.setTitle('类别添加');
        win.grid = grid;
        win.treepanel =  treepanel;
        win.modal = true;
        win.show();
	},
	submitMain : function(formpanel , grid , treepanel){
		this.formSubmit(formpanel , grid, treepanel, 'i_bankquestiontype/save_i_bankquestiontype_qx_i_bankquestiontypebj.do');
	},
	/**
     * 表单提交
     */
     formSubmit : function(formpanel , grid , treepanel, url){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit(url, formpanel.form, '正在提交数据,请稍候.....',
			function(form, action){
			    formpanel.up('window').close();
			    grid.getStore().reload();
			    treepanel.getStore().load();
				Ext.MessageBox.alert('提示', '保存成功！');
			});
        }
     },
     delMain : function(grid , treepanel){
   		this.del(grid, treepanel, 'i_bankquestiontype/del_i_bankquestiontype_qx_i_bankquestiontypesc.do');
   	 },
   	 /**
      * 删除
      */
     del : function(grid , treepanel, url){
    	 var me = this;
 		 // 获取选中的行
 	     var data = grid.getSelectionModel().getSelection();
         if (data.length == 0) {
             Ext.MessageBox.alert('提示', '请先选中要删除的项目！');
         }else {
        	 Ext.MessageBox.show({
        		 title : '询问',
        		 msg : '您确定要删除吗?',
 			     width : 250,
 				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                     if (btn == 'yes') {
                    	 var dir = new Array();　
                         Ext.Array.each(data, function(items) {
                             var id = items.data.id;
                             dir.push(id);
             			});
             			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest(url, param,
                        function(response, options){
                        	grid.getStore().reload();
            			    treepanel.getStore().load();
                        }, null, me.form);
                     }
                 }
             });
         }

 	},
 	editorMain : function(grid , treepanel){
		this.editor(grid, treepanel, 'system.iquestions.bankquestiontype.biz.bankquestiontypeEdit', '题目修改');
	},
	/**
     * 修改
     */
	editor : function(grid, treepanel, url, title){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{

            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            
            var win = Ext.create(url);
            win.setTitle(title);
            win.modal = true;
            win.grid = grid;
            win.treepanel =  treepanel;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'i_bankquestiontype/load_i_bankquestiontypebyid.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},
	viewMain : function(grid){
		this.view(grid, 'system.iquestions.bankquestiontype.biz.bankquestiontypeEdit', '题目查看');
	},
	/**
     * 查看
     */
	view : function(grid, url, title){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{

            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            
            var win = Ext.create(url);
            win.setTitle(title);
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'i_bankquestiontype/load_i_bankquestiontypebyid.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	}
	
});