/**
 * 题目分类编辑界面
 */

Ext.define('system.iquestions.bankquestiontype.biz.bankquestiontypeEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	treepanel : null,
	oper : Ext.create('system.iquestions.bankquestiontype.biz.bankquestiontypeOper'),
	initComponent:function(){
	    var me = this;
	    
	    var gjid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '根级ID',
	    	name: 'baseid',
	    	value: 'FSDQUESTIONTYPE'
	    });
	    
	    var sslmname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属题库名称',
	    	name: 'parentname'
	    });
	    var sslm = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属类别',
	    	labelAlign:'right',
	    	labelWidth:80,
	    	rootText : '主类别',
	    	rootId : 'FSDQUESTIONTYPE',
	        width : 360,
	        name : 'parentid',
	        baseUrl:'i_bankquestiontype/load_AsyncObjectTree.do',//访问路劲
	        allowBlank: false,
	        hiddenName : 'parentname'//隐藏域Name
	    });
	    
	    var lmmc = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '类别名称',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'title',
	    	maxLength: 20,
	    	allowBlank: false
	    });
	    
	    var mc = Ext.create('system.widget.FsdTextFieldZJF',{
	    	fieldLabel: '名称',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'name',
	    	fname: 'symbol',
	    	maxLength: 200,
	    	allowBlank: false
	    });
	    
	    var zjf = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '助记符',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'symbol',
	    	maxLength: 20,
	    	allowBlank: false
	    });
	    
	    var ms = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '描述',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'description',
	    	maxLength: 200
	    });
	    
	    var sxh = Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '顺序号',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'sort',
	    	maxLength: 3,
	    	minValue: 0,
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank: false
	    });
	    
	    var beizhu = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'remark',
	    	maxLength: 100
	    });
	    
	    me.on('show' , function(){
	    	sslm.setText(sslmname.getValue());
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [{
                name: "id",
                xtype: "hidden"
            } , gjid, sslmname, sslm, mc, zjf,
            sxh, beizhu, ms]//lmtp2col , 
        });
	    
	    Ext.apply(this,{
	        width:450,
	        height:320,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
			    handler : function() {
    				me.oper.submitMain(form, me.grid , me.treepanel);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});