/**
 * 题目分类管理界面
 */

Ext.define('system.iquestions.bankquestiontype.biz.bankquestiontypeManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.iquestions.bankquestiontype.model.Extjs_Column_I_bankquestiontype',
	             'system.iquestions.bankquestiontype.model.Extjs_Model_I_bankquestiontype',
	             'system.admin.tools.model.Extjs_Column_SortOperate',
		         'system.admin.tools.model.Extjs_Model_SortOperate'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.iquestions.bankquestiontype.biz.bankquestiontypeOper'),
	
	createContent : function(){
		var me = this;
		
		var queryid = 'FSDQUESTIONTYPE';
		
		/**
		 * 左边的树面板
		 */
		var treepanel = Ext.create("system.widget.FsdTreePanel", {
			icon : 'resources/admin/icon/chart_organisation.png',
			baseUrl : 'i_bankquestiontype/load_AsyncObjectTree.do',
			title: '类别结构',
			rootText : '主类别',
			rootId : 'FSDQUESTIONTYPE',
			region : 'west', // 设置方位
			collapsible : true,//折叠
			width: 182,
			split: true
		});
		
		
		var queryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入类别名称',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});
		
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_i_bankQuestionType', // 显示列
			model: 'model_i_bankQuestionType',
			baseUrl : 'i_bankquestiontype/load_pagedata.do',
			
			tbar : [{
				xtype : "button",
				text : "添加",
				iconCls : 'page_addIcon',
				popedomCode : 'tkbj',
				handler : function(button) {
					me.oper.addMain(grid , treepanel);
				}
			}, {
				xtype : "button",
				text : "修改",
				iconCls : 'page_editIcon',
				popedomCode : 'tkbj',
				handler : function(button) {
					me.oper.editorMain(grid , treepanel);
				}
			}, {
				xtype : "button",
				text : "删除",
				iconCls : 'page_deleteIcon',
				popedomCode : 'tksc',
				handler : function(button) {
				    me.oper.delMain(grid , treepanel);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.viewMain(grid);
				}
			}, {
				text : '排序',
				iconCls : 'up',
				handler : function() {
					var name =  queryText.getValue();
					var pram = {fid : queryid , name : name};
					me.oper.util.sortOperate(
							grid, 
							'column_title', 
							'model_sortoperate',
							grid.baseUrl,
							'i_bankquestiontype/sort_i_bankquestiontype.do',
							function(){return pram;},
				        	function(){treepanel.getStore().load();});
				}
			}, '->', queryText , {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  queryText.getValue();
			var pram = {fid : queryid , name : name};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		var contentpanel = Ext.create("Ext.panel.Panel", {
			frame:true,
			split: true,
			region: 'center',
			layout: 'fit',
			title: '分类信息',
			items: [grid]
		});

		var page1_jExtPanel1_obj = new Ext.panel.Panel({
			bodyPadding: 3,
			header : false,
			border : 0,
			split: true,
			layout: 'border',
			items: [treepanel,contentpanel]
		});
		
		treepanel.on('itemclick', function(view,record,item,index,e){
			queryid = record.raw.id;
			grid.getStore().loadPage(1);
		});
		return page1_jExtPanel1_obj;
	}
});