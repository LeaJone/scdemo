/**
 * 试题选项编辑界面
 */
Ext.define('system.iquestions.voteinfo.biz.TestQuestionTermEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	tjid : '',
	tmid : '',
	oper : Ext.create('system.iquestions.voteinfo.biz.TestinfoOper'),
	initComponent:function(){
	    var me = this;
	    
	    var testinfoid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属题卷编号',
	    	name: 'testinfoid',
	    	value : me.tjid
	    });
	    
	    var testquestionid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属题目编号',
	    	name: 'testquestionid',
	    	value : me.tmid
	    });
	    
	    var code = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '选项编码',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'code',
	    	maxLength: 10,
	    	allowBlank: false
	    });
	    
	    var description = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '描述',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'description',
	    	maxLength: 100,
	    	allowBlank: false
	    });
	    
	    var sort = Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '顺序号',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'sort',
	    	maxLength: 3,
	    	minValue: 0,
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank: false
	    });
	    
	    var remark = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'remark',
	    	maxLength: 50
	    });
	    
	    me.on('show' , function(){
	    	
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 10',
			items : [{
                name: "id",
                xtype: "hidden"
            }, testinfoid, testquestionid, code, description, sort, remark]
        });
	    
	    Ext.apply(this,{
	        width:470,
	        height:265,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
			    handler : function() {
    				me.oper.submitShitixuanxiang(form, me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});