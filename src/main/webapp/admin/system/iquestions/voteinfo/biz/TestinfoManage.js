﻿/**

 * 投票管理界面
 * @author lw
 */

Ext.define('system.iquestions.voteinfo.biz.TestinfoManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.iquestions.voteinfo.model.Extjs_Column_I_testquestion',
	             'system.iquestions.voteinfo.model.Extjs_Model_I_testquestion',
	             'system.iquestions.voteinfo.model.Extjs_Column_I_testinfo',
	             'system.iquestions.voteinfo.model.Extjs_Model_I_testinfo',
	             'system.iquestions.voteinfo.model.Extjs_Column_I_testquestionterm',
	             'system.iquestions.voteinfo.model.Extjs_Model_I_testquestionterm'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.iquestions.voteinfo.biz.TestinfoOper'),
	
	createContent : function(){
		var me = this;

		var typeCode = 'FSDVOTE';
		var typeName = '投票库';
		var tikuID = 'FSDVOTE'; //记录当前选中的题库ID
		var shijuanID = ''; //记录当前选中的试卷ID
		var shitiID = '';//记录当前选中的试题ID

		/**
		 * 左边的题库树面板
		 */
		var treepanel = Ext.create("system.widget.FsdTreePanel", {
			icon : 'resources/admin/icon/chart_organisation.png',
			baseUrl : 'i_bankinfo/load_AsyncObjectTree.do',
			title: '投票分类',
			rootId : typeCode,
			rootText : typeName,
			region : 'west', // 设置方位
			collapsible : true,//折叠
			width: '15%',
			split: true
		});
		
		var shijuanqueryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入投票名称',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						shijuangrid.getStore().loadPage(1);
					}
				}
			}
		});
		
		// 创建投票表格
		var shijuangrid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_i_testinfo', // 显示列
			model: 'model_i_testinfo',
			baseUrl : 'tp_testinfo/load_pagedata.do',
			tbar : [{
				xtype : "button",
				text : "添加",
				iconCls : 'page_addIcon',
				popedomCode : 'tpkbj',
				handler : function(button) {
					me.oper.addTijuan(shijuangrid);
				}
			}, {
				xtype : "button",
				text : "修改",
				iconCls : 'page_editIcon',
				popedomCode : 'tpkbj',
				handler : function(button) {
					me.oper.editorTijuan(shijuangrid);
				}
			}, {
				xtype : "button",
				text : "删除",
				iconCls : 'page_deleteIcon',
				popedomCode : 'tpksc',
				handler : function(button) {
				    me.oper.delTijuan(shijuangrid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.viewTijuan(shijuangrid);
				}
			}, '-', {
				xtype : "fsdbutton",
				popedomCode : 'tpkbj',
				text : "启用",
				iconCls : 'tickIcon',
				handler : function(button) {
				    me.oper.isEnabled(shijuangrid, true);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'tpkbj',
				text : "停用",
				iconCls : 'crossIcon',
				handler : function(button) {
				    me.oper.isEnabled(shijuangrid, false);
				}
			}, '->', shijuanqueryText , {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					shijuangrid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					shijuangrid.getStore().reload();
				}
			}]			
		});
		shijuangrid.getStore().on('beforeload', function(s) {
			var name =  shijuanqueryText.getValue();
			var pram = {fid : tikuID , name : name};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
			shitigrid.getStore().removeAll();
			daangrid.getStore().removeAll();
		});
		shijuangrid.on('selectionchange', function(obj, selected, eOpts){
			if (selected.length == 1){
				shijuanID = selected[0].data.id;
				shitiID = '';
				shitigrid.getStore().loadPage(1);
				daangrid.getStore().removeAll();
			}else{
				shijuanID = '';
				shitiID = '';
				shitigrid.getStore().removeAll();
				daangrid.getStore().removeAll();
			}
		});
		
		
		
		var shitiqueryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入题目名称',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						shitigrid.getStore().loadPage(1);
					}
				}
			}
		});
		// 创建试题表格
		var shitigrid = Ext.create("system.widget.FsdGridPanel", {
			column : 'column_i_testquestion', // 显示列
			model: 'model_i_testquestion',
			baseUrl : 'tp_testquestion/load_pagedata.do',
			zAutoLoad : false,
			tbar : [{
				xtype : "button",
				text : "添加",
				iconCls : 'page_addIcon',
				popedomCode : 'tpkbj',
				handler : function(button) {
					me.oper.addShiti(shitigrid, shijuangrid);
				}
			}, {
				xtype : "button",
				text : "修改",
				iconCls : 'page_editIcon',
				popedomCode : 'tpkbj',
				handler : function(button) {
					me.oper.editorShiti(shitigrid);
				}
			}, {
				xtype : "button",
				text : "删除",
				iconCls : 'page_deleteIcon',
				popedomCode : 'tpksc',
				handler : function(button) {
				    me.oper.delShiti(shitigrid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.viewShiti(shitigrid);
				}
			}, {
				text : '排序',
				iconCls : 'up',
				handler : function() {
					var name =  shitiqueryText.getValue();
					var pram = {fid : shijuanID , name : name};
					me.oper.util.sortOperate(
							shitigrid, 
							shitigrid.column, 
							shitigrid.model,
							shitigrid.baseUrl,
							'tp_testquestion/sort_testquestion.do',
							function(){return pram;},
				        	function(){});
				}
			}, '->', shitiqueryText , {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					shitigrid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					shitigrid.getStore().reload();
				}
			}]			
		});
		shitigrid.getStore().on('beforeload', function(s) {
			var name =  shitiqueryText.getValue();
			var pram = {fid : shijuanID , name : name};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
			daangrid.getStore().removeAll();
		});
		shitigrid.on('selectionchange', function(obj, selected, eOpts){
			if (selected.length == 1){
				shitiID = selected[0].data.id;
				daangrid.getStore().loadPage(1);
			}else{
				shitiID = '';
				daangrid.getStore().removeAll();
			}
		});
		
		// 创建试题选项表格
		var daangrid = Ext.create("system.widget.FsdGridPanel", {
			column : 'column_i_testquestionterm', // 显示列
			model: 'model_i_testquestionterm',
			baseUrl : 'tp_testquestionterm/load_pagedata.do',
			zAutoLoad : false,
			tbar : [{
				xtype : "button",
				text : "添加",
				iconCls : 'page_addIcon',
				popedomCode : 'tpkbj',
				handler : function(button) {
					me.oper.addShitixuanxiang(daangrid, shitigrid);
				}
			}, {
				xtype : "button",
				text : "修改",
				iconCls : 'page_editIcon',
				popedomCode : 'tpkbj',
				handler : function(button) {
					me.oper.editorShitixuanxiang(daangrid);
				}
			}, {
				xtype : "button",
				text : "删除",
				iconCls : 'page_deleteIcon',
				popedomCode : 'tpksc',
				handler : function(button) {
				    me.oper.delShitixuanxiang(daangrid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.viewShitixuanxiang(daangrid);
				}
			}, {
				text : '排序',
				iconCls : 'up',
				handler : function() {
					var pram = {fid : shitiID};
					me.oper.util.sortOperate(
							daangrid, 
							'column_i_testquestionterm', 
							'model_i_testquestionterm',
							'tp_testquestionterm/load_pagedata.do',
							'tp_testquestionterm/sort_testquestionterm.do',
							function(){return pram;},
				        	function(){});
				}
			}, '->', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					daangrid.getStore().reload();
				}
			}]			
		});
		daangrid.getStore().on('beforeload', function(s) {
			var pram = {fid : shitiID};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		var shijuanpanel = Ext.create("Ext.panel.Panel", {
			layout:'fit',
			frame:true,
			split: true,
			region : 'center',
			title: '投票信息',
			width: '45%',
			items: [shijuangrid]
		});
		
		var shitipanel = Ext.create("Ext.panel.Panel", {
			border : 0,
			layout: 'fit',
			region: 'center',
			split: true,
			frame:true,
			title: '题目信息',
			height: '65%',
			items: [shitigrid]
		});
		
		var daanpanel = Ext.create("Ext.panel.Panel", {
			border : 0,
			layout: 'fit',
			region: 'south',
			split: true,
			frame:true,
			title: '题目选项',
			height: '35%',
			items: [daangrid]
		});
		
		var all1panel = Ext.create("Ext.panel.Panel", {
			layout: 'border',
			frame:true,
			split: true,
			region: 'east',
			width: '40%',
			items: [shitipanel, daanpanel]
		});
		
		

		var allPanel = Ext.create('Ext.panel.Panel', {
			bodyPadding: 3,
			header : false,
			border : 0,
			split: true,
			layout: 'border',
			items: [treepanel,shijuanpanel,all1panel]
		});
		
		
		//题库节点单击事件，刷新试卷列表
		treepanel.on('itemclick', function(view,record,item,index,e){
			tikuID = record.raw.id;
			shijuanID = '';
			shitiID = '';
			shijuangrid.getStore().loadPage(1);
			shitigrid.getStore().removeAll();
			daangrid.getStore().removeAll();
		});
		return allPanel;
	}
});