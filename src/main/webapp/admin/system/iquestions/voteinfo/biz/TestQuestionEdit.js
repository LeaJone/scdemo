/**
 * 试题编辑界面
 */
Ext.define('system.iquestions.voteinfo.biz.TestQuestionEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	tjid : '',
	oper : Ext.create('system.iquestions.voteinfo.biz.TestinfoOper'),
	initComponent:function(){
	    var me = this;
	    
	    var testinfoid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属题卷编号',
	    	name: 'testinfoid',
	    	value : me.tjid
	    });
	    
	    var typename = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '题型名称',
	    	name: 'typename'
	    });
	    var typecode = new Ext.create('system.widget.FsdComboBoxZD',{
	    	fieldLabel: '题型',
	    	labelAlign:'right',
	    	labelWidth:80,
	    	width : 420,
	    	name: 'typecode',
	        key : 'i_tmlx',//参数
	        allowBlank: false,
    	    hiddenName : 'typename'//提交隐藏域Name
	    });
	    
	    var code = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '题目编码',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'code',
	    	maxLength: 10,
	    	allowBlank: false
	    });
	    
	    var problem = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '题目',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'problem',
	    	maxLength: 100,
	    	allowBlank: false
	    });
	    
	    var content = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '内容',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'content'
	    });
	    
	    var sort = Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '顺序号',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'sort',
	    	maxLength: 3,
	    	minValue: 0,
	    	allowBlank: false,
	    	allowDecimals : false//是否允许输入小数
	    });
	    
	    var remark = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'remark',
	    	maxLength: 100
	    });
	    
	    me.on('show' , function(){
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 10',
			items : [{
                name: "id",
                xtype: "hidden"
            }, testinfoid, typename, typecode, code, problem, content, sort, remark]
        });
	    
	    Ext.apply(this,{
	        width:470,
	        height:315,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
			    handler : function() {
    				me.oper.submitShiti(form, me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});