/**
 * 投票管理界面操作类
 * @author lw
 */

Ext.define('system.iquestions.voteinfo.biz.TestinfoOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 添加题卷
     */
	addTijuan : function(grid){
	    var win = Ext.create('system.iquestions.voteinfo.biz.TestinfoEdit');
        win.setTitle('投票添加');
        win.grid = grid;
        win.modal = true;
        win.show();
	},
	
	/**
     * 添加试题
     */
	addShiti : function(grid, shijuangrid){
		var data = shijuangrid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选要添加题目的所属投票记录！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{
            var id = data[0].data.id;
    	    var win = Ext.create('system.iquestions.voteinfo.biz.TestQuestionEdit',{
    	    	tjid : id
    	    });
            win.setTitle('题目添加');
            win.grid = grid;
            win.modal = true;
            win.show();
        }
	},
	
	/**
     * 添加试题选项
     */
	addShitixuanxiang : function(grid, shitigrid){
		var data = shitigrid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选要添加选项的所属题目记录！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{
    	    var win = Ext.create('system.iquestions.voteinfo.biz.TestQuestionTermEdit',{
    	    	tjid : data[0].data.testinfoid,
    	    	tmid : data[0].data.id
    	    });
            win.setTitle('题目选项添加');
            win.grid = grid;
            win.modal = true;
            win.show();
        }
	},
	
	/**
	 * 提交题卷表单
	 */
	submitTijuan : function(formpanel, grid){
		this.formSubmit(formpanel, grid, 'tp_testinfo/save_testinfo_qx_tpkbj.do');
	},
	
	/**
	 * 提交试题表单
	 */
	submitShiti : function(formpanel, grid){
		this.formSubmit(formpanel, grid, 'tp_testquestion/save_testquestion_qx_tpkbj.do');
	},
	
	/**
	 * 提交试题选项表单
	 */
	submitShitixuanxiang : function(formpanel, grid){
		this.formSubmit(formpanel, grid, 'tp_testquestionterm/save_testquestionterm_qx_tpkbj.do');
	},
	
	/**
     * 表单提交
     */
     formSubmit : function(formpanel , grid, url){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit(url, formpanel.form, '正在提交数据,请稍候.....',
			function(form, action){
			    formpanel.up('window').close();
			    grid.getStore().reload();
				Ext.MessageBox.alert('提示', '保存成功！');
			});
        }
     },
     
     delTijuan : function(grid , treepanel){
  		this.del(grid, 'tp_testinfo/del_testinfo_qx_tpksc.do');
  	 },
  	 delShiti : function(grid , treepanel){
  		this.del(grid, 'tp_testquestion/del_testquestion_qx_tpksc.do');
  	 },
  	 delShitixuanxiang : function(grid , treepanel){
  		this.del(grid, 'tp_testquestionterm/del_testquestionterm_qx_tpksc.do');
  	 },
     /**
      * 删除
      */
     del : function(grid, url){
    	 var me = this;
 		 // 获取选中的行
 	     var data = grid.getSelectionModel().getSelection();
         if (data.length == 0) {
             Ext.MessageBox.alert('提示', '请先选中要删除的项目！');
         }else {
        	 Ext.MessageBox.show({
        		 title : '询问',
        		 msg : '您确定要删除吗?',
 			     width : 250,
 				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                     if (btn == 'yes') {
                    	 var dir = new Array();　
                         Ext.Array.each(data, function(items) {
                             var id = items.data.id;
                             dir.push(id);
             			});
             			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest(url, param,
                        function(response, options){
                        	grid.getStore().reload();
                        }, null, me.form);
                     }
                 }
             });
         }

 	},
 	
 	editorTijuan : function(grid){
		this.editor(grid, 'system.iquestions.voteinfo.biz.TestinfoEdit', 
				'修改投票', 'tp_testinfo/load_testinfobyid.do');
	},
	editorShiti : function(grid){
		this.editor(grid, 'system.iquestions.voteinfo.biz.TestQuestionEdit', 
				'修改题目', 'tp_testquestion/load_testquestionbyid.do');
	},
	editorShitixuanxiang : function(grid){
		this.editor(grid, 'system.iquestions.voteinfo.biz.TestQuestionTermEdit', 
				'修改题目选项', 'tp_testquestionterm/load_testquestiontermbyid.do');
	},
 	/**
     * 修改
     */
	editor : function(grid, model, title, url){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create(model);
            win.setTitle(title);
            win.modal = true;
            win.grid = grid;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , url, param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},
 	
	viewTijuan : function(grid){
		this.view(grid, 'system.iquestions.voteinfo.biz.TestinfoEdit', 
				'查看投票', 'tp_testinfo/load_testinfobyid.do');
	},
	viewShiti : function(grid){
		this.view(grid, 'system.iquestions.voteinfo.biz.TestQuestionEdit', 
				'查看题目', 'tp_testquestion/load_testquestionbyid.do');
	},
	viewShitixuanxiang : function(grid){
		this.view(grid, 'system.iquestions.voteinfo.biz.TestQuestionTermEdit', 
				'查看题目选项', 'tp_testquestionterm/load_testquestiontermbyid.do');
	},
 	/**
     * 查看
     */
	view : function(grid, model, title, url){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{

            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            
            var win = Ext.create(model);
            win.setTitle(title);
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , url, param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},

	/**
     * 是否启用停用
     */
	isEnabled : function(grid, isQT){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
		        msg : '您确定要修改状态吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
					    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
			            });
             			var pram = {ids : dir, isenabled : isQT};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('tp_testinfo/enabled_testinfo_qx_tpkbj.do' , param ,
                        function(response, options){
            				Ext.MessageBox.alert('提示', '状态修改成功！');
                        	grid.getStore().reload(); 
                        });
                    }
                }
            });
        }
	}
});