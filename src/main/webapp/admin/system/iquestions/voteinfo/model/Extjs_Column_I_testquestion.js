﻿/**
 *I_TESTQUESTION Column
 *@author CodeSystem
 *文件名     Extjs_Column_I_testquestion
 *Columns名  column_i_testquestion
 *xtype: 'textfield', 'numbercolumn', 'booleancolumn', 'datecolumn'   format : '0.00'
 *align: 'left', 'center', 'right'
 */

Ext.define('column_i_testquestion', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            align : 'center',
            width : 30
        }
        ,
        {
            header : '题型',
            dataIndex : 'typename',
            sortable : true,
            align : 'center',
            width : 50
        }
        ,
        {
            header : '题目编码',
            dataIndex : 'code',
            sortable : true,
            align : 'left',
            width : 50
        }
        ,
        {
            header : '题目',
            dataIndex : 'problem',
            sortable : true,
            align : 'left',
            width : '50%'
        }
        ,
        {
            header : '顺序号',
            dataIndex : 'sort',
            sortable : true,
            align : 'left',
            width : 40
        }
    ]
});
