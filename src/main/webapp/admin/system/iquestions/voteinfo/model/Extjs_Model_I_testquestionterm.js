﻿/**
 *I_TESTQUESTIONTERM Model
 *@author CodeSystem
 *文件名     Extjs_Model_I_testquestionterm
 *Model名    model_i_testquestionterm
 */

Ext.define('model_i_testquestionterm', {
    extend: 'Ext.data.Model',
    fields: [
             {
                 name : 'id',
                 type : 'string'
             }
             ,
        {
            name : 'code',
            type : 'string'
        }
        ,
        {
            name : 'description',
            type : 'string'
        }
        ,
        {
            name : 'sort',
            type : 'string'
        }
    ]
});
