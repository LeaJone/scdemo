﻿/**
 *I_TESTQUESTION Model
 *@author CodeSystem
 *文件名     Extjs_Model_I_testquestion
 *Model名    model_i_testquestion
 */

Ext.define('model_i_testquestion', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'testinfoid',
            type : 'string'
        }
        ,
        {
            name : 'typename',
            type : 'string'
        }
        ,{
            name : 'code',
            type : 'string'
        }
        ,{
            name : 'problem',
            type : 'string'
        }
        ,
        {
            name : 'sort',
            type : 'string'
        }
    ]
});
