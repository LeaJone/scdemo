﻿/**
 *I_TESTINFO Model
 *@author CodeSystem
 *文件名     Extjs_Model_I_testinfo
 *Model名    model_i_testinfo
 */

Ext.define('model_i_testinfo', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'kindname',
            type : 'string'
        }
        ,
        {
            name : 'bankinfoname',
            type : 'string'
        }
        ,
        {
            name : 'name',
            type : 'string'
        }
        ,
        {
            name : 'statusname',
            type : 'string'
        }
        ,
        {
            name : 'adddate',
            type : 'string'
        }
        ,
        {
            name : 'addemployeename',
            type : 'string'
        }
    ]
});
