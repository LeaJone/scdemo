﻿/**
 *I_TESTQUESTIONTERM Column
 *@author CodeSystem
 *文件名     Extjs_Column_I_testquestionterm
 *Columns名  column_i_testquestionterm
 *xtype: 'textfield', 'numbercolumn', 'booleancolumn', 'datecolumn'   format : '0.00'
 *align: 'left', 'center', 'right'
 */

Ext.define('column_i_testquestionterm', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            align : 'center',
            width : 30
        }
        ,
        {
            header : '题目编码',
            dataIndex : 'code',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '新增时间',
            dataIndex : 'adddate',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '新增人',
            dataIndex : 'addemployeename',
            sortable : true,
            align : 'left',
            flex : 1
        }
    ]
});
