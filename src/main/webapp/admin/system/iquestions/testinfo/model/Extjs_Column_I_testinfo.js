﻿/**
 *I_TESTINFO Column
 *@author CodeSystem
 *文件名     Extjs_Column_I_testinfo
 *Columns名  column_i_testinfo
 *xtype: 'textfield', 'numbercolumn', 'booleancolumn', 'datecolumn'   format : '0.00'
 *align: 'left', 'center', 'right'
 */

Ext.define('column_i_testinfo', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            align : 'center',
            width : 30
        }
        ,
        {
            header : '题卷类型',
            dataIndex : 'kindname',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '所属题库',
            dataIndex : 'bankinfoname',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '名称',
            dataIndex : 'name',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '状态',
            dataIndex : 'statusname',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '新增时间',
            dataIndex : 'adddate',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '新增人',
            dataIndex : 'addemployeename',
            sortable : true,
            align : 'left',
            flex : 1
        }
    ]
});
