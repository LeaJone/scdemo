﻿/**
 *I_TESTQUESTION Column
 *@author CodeSystem
 *文件名     Extjs_Column_I_testquestion
 *Columns名  column_i_testquestion
 *xtype: 'textfield', 'numbercolumn', 'booleancolumn', 'datecolumn'   format : '0.00'
 *align: 'left', 'center', 'right'
 */

Ext.define('column_i_testquestion', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            align : 'center',
            width : 30
        }
        ,
        {
            header : '所属题卷',
            dataIndex : 'testinfoname',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '题型名称',
            dataIndex : 'typename',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '题目',
            dataIndex : 'problem',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '新增时间',
            dataIndex : 'adddate',
            sortable : true,
            align : 'left',
            flex : 1
        }
        ,
        {
            header : '新增人',
            dataIndex : 'addemployeename',
            sortable : true,
            align : 'left',
            flex : 1
        }
    ]
});
