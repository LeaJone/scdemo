/**
 * 试题选项编辑界面
 */
Ext.define('system.iquestions.testinfo.biz.TestQuestionTermEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.iquestions.testinfo.biz.TestinfoOper'),
	initComponent:function(){
	    var me = this;
	    
	    var tijuanid = "";
	    
	    var testinfoid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属题卷编号',
	    	name: 'testinfoid'
	    });
	    
	    var testinfoname = Ext.create('system.widget.FsdTextGrid', {
	    	zFieldLabel : '所属题卷', 
	    	zLabelWidth : 80,
	    	width : 420,
		    zName : 'testinfoname',
		    zBaseUrl :'i_testinfo/load_pagedataall.do',
		    zColumn : 'column_i_testinfo', // 显示列
		    zModel : 'model_i_testinfo',
		    zAllowBlank: false,
		    zIsText1 : true,
		    zTxtLabel1 : '所属题卷',
		    zFunChoose : function(data){
		    	testinfoname.setValue(data.name);
		    	testinfoid.setValue(data.id);
		    },
		    zFunQuery : function(txt1, txt2, txt3, txt4){
		    	return {key : txt1};
		    }
	    });
	    
	    
	    var testquestionid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属试题编号',
	    	name: 'testquestionid'
	    });
	    var testquestionname = Ext.create('system.widget.FsdTextGrid', {
	    	zFieldLabel : '所属试题', 
	    	zLabelWidth : 80,
	    	width : 420,
		    zName : 'testquestionname',
		    zBaseUrl :'i_testquestion/load_pagedataall.do',
		    zColumn : 'column_i_testquestion', // 显示列
		    zModel: 'model_i_testquestion',
		    zAllowBlank: false,
		    zIsText1 : true,
		    zTxtLabel1 : '所属试题',
		    zFunChoose:function(data){
		    	testquestionname.setValue(data.problem);
		    	testquestionid.setValue(data.id);
		    },
		    zFunQuery : function(txt1, txt2, txt3, txt4){
		    	return {key : txt1};
		    }
	    });
	    
	    var code = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '条件编码',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'code',
	    	maxLength: 200,
	    	allowBlank: false
	    });
	    
	    var isanswer = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'是否答案',
	        labelAlign:'right',
	        labelWidth:80,
	        width : 420,
    	    name : 'isanswer',//提交到后台的参数名
	        key : 'xb',//参数
    	    hiddenName : 'isanswercode'//提交隐藏域Name
	    });
	    var isanswercode = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '是否答案名',
	    	name: 'isanswercode'
	    });
	    
	    var score = Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '分数设定',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'score',
	    	maxLength: 3,
	    	minValue: 0,
	    	allowDecimals : true//是否允许输入小数
	    });
	    
	    var sort = Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '顺序号',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'sort',
	    	maxLength: 3,
	    	minValue: 0,
	    	allowDecimals : false//是否允许输入小数
	    });
	    
	    var parse = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '解析',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'parse',
	    	maxLength: 100
	    });
	    
	    var beizhu = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'remark',
	    	maxLength: 100
	    });
	    
	    var ms = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '描述',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'description',
	    	maxLength: 200
	    });
	    
	    me.on('show' , function(){
	    	
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 10',
			items : [{
                name: "id",
                xtype: "hidden"
            }, testinfoname, testinfoid, testquestionname, testquestionid, code, isanswer, isanswercode,  score, sort, parse, beizhu, ms]
        });
	    
	    Ext.apply(this,{
	        width:470,
	        height:400,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
			    handler : function() {
    				me.oper.submitShitixuanxiang(form, me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});