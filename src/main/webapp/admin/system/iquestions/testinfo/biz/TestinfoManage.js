﻿/**

 * 试卷管理界面
 * @author lumingbao
 */

Ext.define('system.iquestions.testinfo.biz.TestinfoManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*', 
	             'system.iquestions.testinfo.model.Extjs_Column_I_testquestion',
	             'system.iquestions.testinfo.model.Extjs_Model_I_testquestion',
	             'system.iquestions.testinfo.model.Extjs_Column_I_testinfo',
	             'system.iquestions.testinfo.model.Extjs_Model_I_testinfo',
	             'system.iquestions.testinfo.model.Extjs_Column_I_testquestionterm',
	             'system.iquestions.testinfo.model.Extjs_Model_I_testquestionterm'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.iquestions.testinfo.biz.TestinfoOper'),
	
	createContent : function(){
		var me = this;
		
		var tikuID = 'FSDINVESTIGATE'; //记录当前选中的题库ID
		var shijuanID = ''; //记录当前选中的试卷ID
		var shitiID = '';//记录当前选中的试题ID
		
		/**
		 * 左边的题库树面板
		 */
		var treepanel = Ext.create("system.widget.FsdTreePanel", {
			icon : 'resources/admin/icon/chart_organisation.png',
			baseUrl : 'i_bankinfo/load_AsyncObjectTree.do',
			title: '题库结构',
			rootText : '主题库',
			rootId : 'FSDINVESTIGATE',
			region : 'west', // 设置方位
			collapsible : true,//折叠
			width: '15%',
			split: true
		});
		
		var shijuanqueryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入试卷名称',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});
		
		// 创建试卷表格
		var shijuangrid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_i_testinfo', // 显示列
			model: 'model_i_testinfo',
			baseUrl : 'i_testinfo/load_pagedata.do',
			tbar : [{
				xtype : "button",
				text : "添加",
				iconCls : 'page_addIcon',
				popedomCode : 'lmbj',
				handler : function(button) {
					me.oper.addTijuan(shijuangrid);
				}
			}, {
				xtype : "button",
				text : "修改",
				iconCls : 'page_editIcon',
				popedomCode : 'lmbj',
				handler : function(button) {
					me.oper.editorTijuan(shijuangrid);
				}
			}, {
				xtype : "button",
				text : "删除",
				iconCls : 'page_deleteIcon',
				popedomCode : 'lmsc',
				handler : function(button) {
				    me.oper.delTijuan(shijuangrid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.viewTijuan(shijuangrid);
				}
			}, '->', shijuanqueryText , {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					shijuangrid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					shijuangrid.getStore().reload();
				}
			}]			
		});
		shijuangrid.getStore().on('beforeload', function(s) {
			var name =  shijuanqueryText.getValue();
			var pram = {fid : tikuID , name : name};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		shijuangrid.on('itemclick', function(obj, record, item, index, e, eOpts) {
			if(record != null && record.data != null && record.data.id != null){
				me.shijuanID = record.data.id;
				shitiID = '';
				shitigrid.getStore().loadPage(1);
				daangrid.getStore().removeAll();
			}
		});
		
		
		
		var shitiqueryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入题目名称',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});
		// 创建试题表格
		var shitigrid = Ext.create("system.widget.FsdGridPanel", {
			column : 'column_i_testquestion', // 显示列
			model: 'model_i_testquestion',
			baseUrl : 'i_testquestion/load_pagedata.do',
			zAutoLoad : false,
			tbar : [{
				xtype : "button",
				text : "添加",
				iconCls : 'page_addIcon',
				popedomCode : 'lmbj',
				handler : function(button) {
					me.oper.addShiti(shitigrid);
				}
			}, {
				xtype : "button",
				text : "修改",
				iconCls : 'page_editIcon',
				popedomCode : 'lmbj',
				handler : function(button) {
					me.oper.editorShiti(shitigrid);
				}
			}, {
				xtype : "button",
				text : "删除",
				iconCls : 'page_deleteIcon',
				popedomCode : 'lmsc',
				handler : function(button) {
				    me.oper.delShiti(shitigrid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.viewShiti(shitigrid);
				}
			}, '->', shitiqueryText , {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					shitigrid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					shitigrid.getStore().reload();
				}
			}]			
		});
		shitigrid.getStore().on('beforeload', function(s) {
			var name =  shitiqueryText.getValue();
			var pram = {fid : me.shijuanID , name : name};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		shitigrid.on('itemclick', function(obj, record, item, index, e, eOpts) {
			if(record != null && record.data != null && record.data.id != null){
				me.shitiID = record.data.id;
				daangrid.getStore().loadPage(1);
			}
		});
		
		// 创建试题选项表格
		var daangrid = Ext.create("system.widget.FsdGridPanel", {
			column : 'column_i_testquestionterm', // 显示列
			model: 'model_i_testquestionterm',
			baseUrl : 'i_testquestionterm/load_pagedata.do',
			zAutoLoad : false,
			tbar : [{
				xtype : "button",
				text : "添加",
				iconCls : 'page_addIcon',
				popedomCode : 'lmbj',
				handler : function(button) {
					me.oper.addShitixuanxiang(daangrid);
				}
			}, {
				xtype : "button",
				text : "修改",
				iconCls : 'page_editIcon',
				popedomCode : 'lmbj',
				handler : function(button) {
					me.oper.editorShitixuanxiang(daangrid);
				}
			}, {
				xtype : "button",
				text : "删除",
				iconCls : 'page_deleteIcon',
				popedomCode : 'lmsc',
				handler : function(button) {
				    me.oper.delShitixuanxiang(daangrid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.viewShitixuanxiang(daangrid);
				}
			}, '->', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					daangrid.getStore().reload();
				}
			}]			
		});
		daangrid.getStore().on('beforeload', function(s) {
			var pram = {fid : me.shitiID};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		var shijuanpanel = Ext.create("Ext.panel.Panel", {
			layout:'fit',
			frame:true,
			split: true,
			region : 'center',
			title: '试卷信息',
			width: '45%',
			items: [shijuangrid]
		});
		
		var shitipanel = Ext.create("Ext.panel.Panel", {
			border : 0,
			layout: 'fit',
			region: 'north',
			split: true,
			frame:true,
			title: '试题信息',
			height: '70%',
			items: [shitigrid]
		});
		
		var daanpanel = Ext.create("Ext.panel.Panel", {
			border : 0,
			layout: 'fit',
			region: 'center',
			split: true,
			frame:true,
			title: '试题选项',
			height: '30%',
			items: [daangrid]
		});
		
		var all1panel = Ext.create("Ext.panel.Panel", {
			layout: 'border',
			frame:true,
			split: true,
			region: 'east',
			width: '40%',
			items: [shitipanel, daanpanel]
		});
		
		

		var allPanel = Ext.create('Ext.panel.Panel', {
			bodyPadding: 3,
			header : false,
			border : 0,
			split: true,
			layout: 'border',
			items: [treepanel,all1panel,shijuanpanel]
		});
		
		
		//题库节点单击事件，刷新试卷列表
		treepanel.on('itemclick', function(view,record,item,index,e){
			tikuID = record.raw.id;
			shijuanID = '';
			shitiID = '';
			shijuangrid.getStore().loadPage(1);
			shitigrid.getStore().removeAll();
			daangrid.getStore().removeAll();
		});
		return allPanel;
	}
});