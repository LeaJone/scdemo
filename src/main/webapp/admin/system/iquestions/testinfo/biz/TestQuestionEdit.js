/**
 * 试题编辑界面
 */
Ext.define('system.iquestions.testinfo.biz.TestQuestionEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.iquestions.testinfo.biz.TestinfoOper'),
	initComponent:function(){
	    var me = this;
	    
	    var testinfoid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属题卷编号',
	    	name: 'testinfoid'
	    });
	    
	    var testinfoname = Ext.create('system.widget.FsdTextGrid', {
	    	zFieldLabel : '所属题卷', 
	    	zLabelWidth : 80,
	    	width : 420,
		    zName : 'testinfoname',
		    zBaseUrl :'i_testinfo/load_pagedataall.do',
		    zColumn : 'column_i_testinfo', // 显示列
		    zModel : 'model_i_testinfo',
		    zAllowBlank : false,
		    zIsText1 : true,
		    zTxtLabel1 : '题卷名称',
		    zFunChoose : function(data){
		    	testinfoname.setValue(data.name);
		    	testinfoid.setValue(data.id);
		    },
		    zFunQuery : function(txt1, txt2, txt3, txt4){
		    	return {key : txt1};
		    }
	    });
	    
	    var typename = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '题型名称',
	    	name: 'typename'
	    });
	    var typecode = new Ext.create('system.widget.FsdComboBoxZD',{
	    	fieldLabel: '题型',
	    	labelAlign:'right',
	    	labelWidth:80,
	    	width : 420,
	    	name: 'typecode',
	        key : 'xb',//参数
	        allowBlank: false,
    	    hiddenName : 'typename'//提交隐藏域Name
	    });
	    
	    var code = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '题目编码',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'code',
	    	maxLength: 200,
	    	allowBlank: false
	    });
	    
	    var problem = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '题目',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'problem',
	    	maxLength: 200,
	    	allowBlank: false
	    });
	    
	    var score = Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '分数设定',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'score',
	    	maxLength: 3,
	    	minValue: 0,
	    	allowDecimals : true//是否允许输入小数
	    });
	    
	    var sort = Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '顺序号',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'sort',
	    	maxLength: 3,
	    	minValue: 0,
	    	allowDecimals : false//是否允许输入小数
	    });
	    
	    var keynote = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '重点',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'keynote',
	    	maxLength: 100
	    });
	    
	    var parse = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '解析',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'parse',
	    	maxLength: 100
	    });
	    
	    var beizhu = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'remark',
	    	maxLength: 100
	    });
	    
	    var ms = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '描述',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'description',
	    	maxLength: 200
	    });
	    
	    me.on('show' , function(){
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 10',
			items : [{
                name: "id",
                xtype: "hidden"
            }, testinfoname, testinfoid, typename, typecode, code, problem, score, sort, keynote, parse, beizhu, ms]
        });
	    
	    Ext.apply(this,{
	        width:470,
	        height:420,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
			    handler : function() {
    				me.oper.submitShiti(form, me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});