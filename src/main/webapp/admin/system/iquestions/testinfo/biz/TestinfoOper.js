/**
 * 试卷管理界面操作类
 * @author lumingbao
 */

Ext.define('system.iquestions.testinfo.biz.TestinfoOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 添加题卷
     */
	addTijuan : function(grid){
	    var win = Ext.create('system.iquestions.testinfo.biz.TestinfoEdit');
        win.setTitle('题卷添加');
        win.grid = grid;
        win.modal = true;
        win.show();
	},
	
	/**
     * 添加试题
     */
	addShiti : function(grid){
	    var win = Ext.create('system.iquestions.testinfo.biz.TestQuestionEdit');
        win.setTitle('试题添加');
        win.grid = grid;
        win.modal = true;
        win.show();
	},
	
	/**
     * 添加试题选项
     */
	addShitixuanxiang : function(grid){
	    var win = Ext.create('system.iquestions.testinfo.biz.TestQuestionTermEdit');
        win.setTitle('试题选项添加');
        win.grid = grid;
        win.modal = true;
        win.show();
	},
	
	/**
	 * 提交题卷表单
	 */
	submitTijuan : function(formpanel, grid){
		this.formSubmit(formpanel, grid, 'i_testinfo/save_i_testinfo_qx_i_testinfobj.do');
	},
	
	/**
	 * 提交试题表单
	 */
	submitShiti : function(formpanel, grid){
		this.formSubmit(formpanel, grid, 'i_testquestion/save_i_testquestion_qx_i_testquestionbj.do');
	},
	
	/**
	 * 提交试题选项表单
	 */
	submitShitixuanxiang : function(formpanel, grid){
		this.formSubmit(formpanel, grid, 'i_testquestionterm/save_i_testquestionterm_qx_i_testquestiontermbj.do');
	},
	
	/**
     * 表单提交
     */
     formSubmit : function(formpanel , grid, url){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit(url, formpanel.form, '正在提交数据,请稍候.....',
			function(form, action){
			    formpanel.up('window').close();
			    grid.getStore().reload();
				Ext.MessageBox.alert('提示', '保存成功！');
			});
        }
     },
     
     delTijuan : function(grid , treepanel){
  		this.del(grid, 'i_testinfo/del_i_testinfo_qx_i_testinfosc.do');
  	 },
  	 delShiti : function(grid , treepanel){
  		this.del(grid, 'i_testquestion/del_i_testquestion_qx_i_testquestionsc.do');
  	 },
  	 delShitixuanxiang : function(grid , treepanel){
  		this.del(grid, 'i_testquestionterm/del_i_testquestionterm_qx_i_testquestiontermsc.do');
  	 },
     /**
      * 删除
      */
     del : function(grid, url){
    	 var me = this;
 		 // 获取选中的行
 	     var data = grid.getSelectionModel().getSelection();
         if (data.length == 0) {
             Ext.MessageBox.alert('提示', '请先选中要删除的项目！');
         }else {
        	 Ext.MessageBox.show({
        		 title : '询问',
        		 msg : '您确定要删除吗?',
 			     width : 250,
 				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                     if (btn == 'yes') {
                    	 var dir = new Array();　
                         Ext.Array.each(data, function(items) {
                             var id = items.data.id;
                             dir.push(id);
             			});
             			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest(url, param,
                        function(response, options){
                        	grid.getStore().reload();
                        }, null, me.form);
                     }
                 }
             });
         }

 	},
 	
 	editorTijuan : function(grid){
		this.editor(grid, 'system.iquestions.testinfo.biz.TestinfoEdit', '修改题卷', 'i_testinfo/load_i_testinfobyid.do');
	},
	editorShiti : function(grid){
		this.editor(grid, 'system.iquestions.testinfo.biz.TestQuestionEdit', '修改试题', 'i_testquestion/load_i_testquestionbyid.do');
	},
	editorShitixuanxiang : function(grid){
		this.editor(grid, 'system.iquestions.testinfo.biz.TestQuestionTermEdit', '修改试题选项', 'i_testquestionterm/load_i_testquestiontermbyid.do');
	},
 	/**
     * 修改
     */
	editor : function(grid, model, title, url){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{

            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            
            var win = Ext.create(model);
            win.setTitle(title);
            win.modal = true;
            win.grid = grid;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , url, param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},
 	
	viewTijuan : function(grid){
		this.view(grid, 'system.iquestions.testinfo.biz.TestinfoEdit', '查看题卷', 'i_testinfo/load_i_testinfobyid.do');
	},
	viewShiti : function(grid){
		this.view(grid, 'system.iquestions.testinfo.biz.TestQuestionEdit', '查看试题', 'i_testquestion/load_i_testquestionbyid.do');
	},
	viewShitixuanxiang : function(grid){
		this.view(grid, 'system.iquestions.testinfo.biz.TestQuestionTermEdit', '查看试题选项', 'i_testquestionterm/load_i_testquestiontermbyid.do');
	},
 	/**
     * 查看
     */
	view : function(grid, model, title, url){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{

            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            
            var win = Ext.create(model);
            win.setTitle(title);
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , url, param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	}
});