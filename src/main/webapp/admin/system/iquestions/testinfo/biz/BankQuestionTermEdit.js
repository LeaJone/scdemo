/**
 * 试题选项编辑界面
 */
Ext.define('system.iquestions.testinfo.biz.BankQuestionTermEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.iquestions.testinfo.biz.TestinfoOper'),
	initComponent:function(){
	    var me = this;
	    
	    var sslmname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属题库名称',
	    	name: 'bankinfoname'
	    });
	    var sslm = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属题库',
	    	labelAlign:'right',
	    	labelWidth:80,
	    	rootText : '主题库',
	    	rootId : 'FSDINVESTIGATE',
	        width : 420,
	        name : 'bankinfoid',
	        baseUrl:'i_bankinfo/load_AsyncSubjectTree.do',//访问路劲
	        allowBlank: false,
	        hiddenName : 'bankinfoname'//隐藏域Name
	    });
	    
	    var tjlxcode = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '题卷类型名称',
	    	name: 'kindname'
	    });
	    var tjlx = new Ext.create('system.widget.FsdComboBoxZD',{
	    	fieldLabel: '题卷类型',
	    	labelAlign:'right',
	    	labelWidth:80,
	    	width : 420,
	    	name: 'kindcode',
	        key : 'xb',//参数
	        allowBlank: false,
    	    hiddenName : 'kindname'//提交隐藏域Name
	    });
	    
	    var mc = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '名称',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'name',
	    	maxLength: 200,
	    	allowBlank: false
	    });
	    
	    var dtsc = Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '答题时长',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'answertime',
	    	maxLength: 3,
	    	minValue: 0,
	    	allowDecimals : false//是否允许输入小数
	    });
	    
	    var zfs = Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '总分数',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'totalscore',
	    	maxLength: 3,
	    	minValue: 0,
	    	allowDecimals : false//是否允许输入小数
	    });
	    
	    var tjzt = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'状态',
	        labelAlign:'right',
	        labelWidth:80,
	        width : 420,
    	    name : 'status',//提交到后台的参数名
	        key : 'ryzt',//参数
	        allowBlank: false,
    	    hiddenName : 'statusname'//提交隐藏域Name
	    });
	    var tjztcode = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '状态名',
	    	name: 'statusname'
	    });
	    
	    var beizhu = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'remark',
	    	maxLength: 100
	    });
	    
	    var ms = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '描述',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'description',
	    	maxLength: 200
	    });
	    
	    me.on('show' , function(){
	    	
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 10',
			items : [{
                name: "id",
                xtype: "hidden"
            }, sslmname, sslm, tjlx, tjlxcode, mc, tjzt, tjztcode, dtsc, zfs, beizhu, ms]
        });
	    
	    Ext.apply(this,{
	        width:470,
	        height:450,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
				popedomCode : 'lmbj',
			    handler : function() {
    				me.oper.submitShitixuanxiang(form, me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});