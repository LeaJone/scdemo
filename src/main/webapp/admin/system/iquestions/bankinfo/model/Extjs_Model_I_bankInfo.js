﻿/**
 *i_bankInfo Model
 *@author CodeSystem
 *文件名     Extjs_Model_I_bankInfo
 *Model名    model_i_bankInfo
 */

Ext.define('model_i_bankInfo', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
             ,
        {
            name : 'name',
            type : 'string'
        }
        ,
        {
            name : 'description',
            type : 'string'
        }
        ,
        {
            name : 'sort',
            type : 'string'
        }
        ,
        {
            name : 'adddate',
            type : 'string'
        }
        ,
        {
            name : 'remark',
            type : 'string'
        }
    ]
});
