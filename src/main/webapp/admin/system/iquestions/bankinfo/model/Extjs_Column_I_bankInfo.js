﻿/**
 *i_bankInfo Column
 *@author CodeSystem
 *文件名     Extjs_Column_I_bankInfo
 *Columns名  column_i_bankInfo
 */

Ext.define('column_i_bankInfo', {
    columns: [
              {
                  xtype : 'rownumberer',
                  text : 'NO',
                  sortable : true,
                  align : 'center',
                  width : 40
              }
              ,
        {
            header : '名称',
            dataIndex : 'name',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '描述',
            dataIndex : 'description',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '顺序号',
            dataIndex : 'sort',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '新增时间',
            dataIndex : 'adddate',
            sortable : true,
            flex : 1,
            renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value);
			}
        }
        ,
        {
            header : '备注',
            dataIndex : 'remark',
            sortable : true,
            flex : 1
        }
    ]
});