/**
 * 知识库分类界面
 */
Ext.define('system.iquestions.bankinfo.biz.bankinfoZSManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.iquestions.bankinfo.model.Extjs_Column_I_bankInfo',
	             'system.iquestions.bankinfo.model.Extjs_Model_I_bankInfo',
	             'system.admin.tools.model.Extjs_Column_SortOperate',
		         'system.admin.tools.model.Extjs_Model_SortOperate'],
	header : false,
	border : 0,
	layout : 'fit',
	oper : Ext.create('system.iquestions.bankinfo.biz.bankinfoOper'),
	
	initComponent : function() {
		var me = this;
		me.oper.form = me;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	createContent : function(){
		var me = this;
		
		var queryid = 'FSDKNOWLEDGE';
		var typeCode = 'FSDKNOWLEDGE';
		var typeName = '知识库';
		
		/**
		 * 左边的树面板
		 */
		var treepanel = Ext.create("system.widget.FsdTreePanel", {
			icon : 'resources/admin/icon/chart_organisation.png',
			baseUrl : 'i_bankinfo/load_AsyncObjectTree.do',
			title: typeName + '分类',
			rootText : typeName,
			rootId : typeCode,
			region : 'west', // 设置方位
			collapsible : true,//折叠
			width: 250,
			split: true
		});
		
		
		var queryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入' + typeName + '名称',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});
		
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_i_bankInfo', // 显示列
			model: 'model_i_bankInfo',
			baseUrl : 'i_bankinfo/load_pagedata.do',
			tbar : [{
				xtype : "fsdbutton",
				text : "添加",
				iconCls : 'page_addIcon',
				popedomCode : 'zskflbj',
				handler : function(button) {
					me.oper.add(grid , treepanel, typeCode, typeName);
				}
			}, {
				xtype : "fsdbutton",
				text : "修改",
				iconCls : 'page_editIcon',
				popedomCode : 'zskflbj',
				handler : function(button) {
					me.oper.editor(grid, treepanel, typeCode, typeName);
				}
			}, {
				xtype : "fsdbutton",
				text : "删除",
				iconCls : 'page_deleteIcon',
				popedomCode : 'zskflsc',
				handler : function(button) {
				    me.oper.delData(grid, treepanel, typeCode);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid, typeCode, typeName);
				}
			}, {
				text : '排序',
				iconCls : 'up',
				handler : function() {
					var name =  queryText.getValue();
					var pram = {fid : queryid , name : name};
					me.oper.util.sortOperate(
							grid, 
							'column_name', 
							'model_sortoperate',
							grid.baseUrl,
							'i_bankinfo/sort_i_bankinfo.do',
							function(){return pram;},
				        	function(){treepanel.getStore().load();});
				}
			}, '->', queryText , {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  queryText.getValue();
			var pram = {fid : queryid , name : name};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		var contentpanel = Ext.create("Ext.panel.Panel", {
			frame:true,
			split: true,
			region: 'center',
			layout: 'fit',
			title: typeName + '信息',
			items: [grid]
		});

		var page1_jExtPanel1_obj = new Ext.panel.Panel({
			bodyPadding: 3,
			header : false,
			border : 0,
			split: true,
			layout: 'border',
			items: [treepanel,contentpanel]
		});
		
		treepanel.on('itemclick', function(view,record,item,index,e){
			queryid = record.raw.id;
			grid.getStore().loadPage(1);
		});
		return page1_jExtPanel1_obj;
	}
});