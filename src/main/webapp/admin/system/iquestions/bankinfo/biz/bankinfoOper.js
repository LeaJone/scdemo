/**
 * 题库管理界面操作类
 */
Ext.define('system.iquestions.bankinfo.biz.bankinfoOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 添加
     */
	add : function(grid, treepanel, typeCode, typeName){
	    var win = Ext.create('system.iquestions.bankinfo.biz.bankinfoEdit', {
	    	grid : grid,
	    	treepanel : treepanel,
	    	typeCode : typeCode,
	    	typeName : typeName
	    });
        win.setTitle(typeName + '分类添加');
        win.modal = true;
        win.show();
	},
	
	submitData : function(formpanel, grid, treepanel, typeCode){
		 switch (typeCode) {
		 case 'FSDKNOWLEDGE'://知识库
				this.formSubmit(formpanel, grid, treepanel, 'i_bankinfo/save_i_bankinfo_qx_zskflbj.do');
				break;
		 case 'FSDQUESTIONS'://题目库
				this.formSubmit(formpanel, grid, treepanel, 'i_bankinfo/save_i_bankinfo_qx_tmkflbj.do');
				break;
		 case 'FSDLIBRARY'://题卷库
				this.formSubmit(formpanel, grid, treepanel, 'i_bankinfo/save_i_bankinfo_qx_tjkflbj.do');
				break;
		 case 'FSDINVESTIGATE'://问卷库
				this.formSubmit(formpanel, grid, treepanel, 'i_bankinfo/save_i_bankinfo_qx_wjkflbj.do');
				break;
		 case 'FSDVOTE'://投票库
				this.formSubmit(formpanel, grid, treepanel, 'i_bankinfo/save_i_bankinfo_qx_tpkflbj.do');
				break;
		}
	},
	/**
     * 表单提交
     */
     formSubmit : function(formpanel , grid , treepanel, url){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit(url, formpanel.form, '正在提交数据,请稍候.....',
			function(form, action){
			    formpanel.up('window').close();
			    grid.getStore().reload();
			    treepanel.getStore().load();
				Ext.MessageBox.alert('提示', '保存成功！');
			});
        }
     },
     
     delData : function(grid , treepanel, typeCode){
    	 switch (typeCode) {
    	 case 'FSDKNOWLEDGE':
	    	   	this.del(grid, treepanel, 'i_bankinfo/del_i_bankinfo_qx_zskflsc.do');
 			break;
    	 case 'FSDQUESTIONS':
	    	   	this.del(grid, treepanel, 'i_bankinfo/del_i_bankinfo_qx_tmkflsc.do');
 			break;
    	 case 'FSDLIBRARY':
	    	   	this.del(grid, treepanel, 'i_bankinfo/del_i_bankinfo_qx_tjkflsc.do');
 			break;
    	 case 'FSDINVESTIGATE':
	    	   	this.del(grid, treepanel, 'i_bankinfo/del_i_bankinfo_qx_wjkflsc.do');
 			break;
    	 case 'FSDVOTE':
	    	   	this.del(grid, treepanel, 'i_bankinfo/del_i_bankinfo_qx_tpkflsc.do');
 			break;
		}
   	 },
   	 /**
      * 删除
      */
     del : function(grid , treepanel, url){
    	 var me = this;
 		 // 获取选中的行
 	     var data = grid.getSelectionModel().getSelection();
         if (data.length == 0) {
             Ext.MessageBox.alert('提示', '请先选中要删除的项目！');
         }else {
        	 Ext.MessageBox.show({
        		 title : '询问',
        		 msg : '您确定要删除吗?',
 			     width : 250,
 				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                     if (btn == 'yes') {
                    	 var dir = new Array();　
                         Ext.Array.each(data, function(items) {
                             var id = items.data.id;
                             dir.push(id);
             			});
             			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest(url, param,
                        function(response, options){
                        	grid.getStore().reload();
            			    treepanel.getStore().load();
                        }, null, me.form);
                     }
                 }
             });
         }
 	},

	/**
     * 修改
     */
	editor : function(grid, treepanel, typeCode, typeName){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{

            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            
            var win = Ext.create('system.iquestions.bankinfo.biz.bankinfoEdit', {
    	    	grid : grid,
    	    	treepanel : treepanel,
    	    	typeCode : typeCode,
    	    	typeName : typeName
            });
            win.setTitle(typeName + '分类编辑');
            win.modal = true;
            win.grid = grid;
            win.treepanel =  treepanel;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'i_bankinfo/load_i_bankinfobyid.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},

	/**
     * 查看
     */
	view : function(grid, typeCode, typeName){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.iquestions.bankinfo.biz.bankinfoEdit', {
    	    	typeCode : typeCode,
    	    	typeName : typeName
            });
            win.setTitle(typeName + '分类查看');
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'i_bankinfo/load_i_bankinfobyid.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	}
});