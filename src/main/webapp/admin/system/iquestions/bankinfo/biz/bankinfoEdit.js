/**
 * 题库编辑界面
 */
Ext.define('system.iquestions.bankinfo.biz.bankinfoEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	treepanel : null,
	typeCode : 'FSDINVESTIGATE',
	typeName : '题目库',
	oper : Ext.create('system.iquestions.bankinfo.biz.bankinfoOper'),
	initComponent:function(){
	    var me = this;
	    
	    var baseid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '根级ID',
	    	name: 'baseid',
	    	value: me.typeCode
	    });
	    
	    var parentname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属库名称',
	    	name: 'parentname'
	    });
	    var parentid = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属库名',
	    	labelAlign:'right',
	    	labelWidth:80,
	    	rootText : me.typeName,
	    	rootId : me.typeCode,
	        width : 360,
	        name : 'parentid',
	        baseUrl:'i_bankinfo/load_AsyncObjectTree.do',//访问路劲
	        allowBlank: false,
	        hiddenName : 'parentname'//隐藏域Name
	    });
	    
	    var name = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '名称',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'name',
	    	maxLength: 200,
	    	allowBlank: false
	    });
	    
	    var description = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '描述',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'description',
	    	maxLength: 200
	    });
	    
	    var sort = Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '顺序号',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'sort',
	    	maxLength: 3,
	    	minValue: 0,
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank: false
	    });
	    
	    var remark = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'remark',
	    	maxLength: 100
	    });
	    
	    me.on('show' , function(){
	    	parentid.setText(parentname.getValue());
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [{
                name: "id",
                xtype: "hidden"
            } , baseid, parentname, parentid, name, 
            sort, remark, description]//lmtp2col , 
        });
	    
	    Ext.apply(this,{
	        width:450,
	        height:290,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
//				popedomCode : 'lmbj',
			    handler : function() {
    				me.oper.submitData(form, me.grid, me.treepanel, me.typeCode);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});