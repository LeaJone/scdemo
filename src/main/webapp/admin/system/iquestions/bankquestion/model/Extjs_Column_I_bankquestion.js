﻿/**
 *I_BANKQUESTION Column
 *@author CodeSystem
 *文件名     Extjs_Column_I_bankquestion
 *Columns名  column_i_bankquestion
 */

Ext.define('column_i_bankquestion', {
	columns : [ {
		xtype : 'rownumberer',
		text : 'NO',
		sortable : true,
		align : 'center',
		width : 30
	}, {
		header : '题型',
		dataIndex : 'typename',
		sortable : true,
		align : 'center',
		width : 50
	}, {
		header : '难度',
		dataIndex : 'levelname',
		sortable : true,
		align : 'center',
		width : 50
	}, {
		header : '重点程度',
		dataIndex : 'keynotename',
		sortable : true,
		align : 'center',
		width : 50
	}, {
		header : '题目',
		dataIndex : 'problem',
		sortable : true,
		align : 'left',
		width : '30%'
	}, {
		header : '审核',
		dataIndex : 'auditing',
		sortable : true,
		align : 'center',
		width : 30,
		renderer : function(value, metaData, record) {
			if (value == 'true') {
				value = "<font style='color:green;'>审</font>";
			} else {
				value = "<font style='color:red;'>停</font>";
			}
			return value;
		}
	}, {
		header : '顺序号',
		dataIndex : 'sort',
		sortable : true,
		align : 'center',
		width : 30
	} ]
});
