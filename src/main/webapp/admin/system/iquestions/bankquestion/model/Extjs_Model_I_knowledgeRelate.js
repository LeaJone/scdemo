/**
 *i_knowledgeRelate Model
 *@author CodeSystem
 *文件名     Extjs_Model_I_knowledgeRelate
 *Model名    model_i_knowledgeRelate
 */

Ext.define('model_i_knowledgeRelate', {
	extend : 'Ext.data.Model',
	fields : [ {
		name : 'ID',
		type : 'string'
	}, {
		name : 'RELATEID',
		type : 'string'
	}, {
		name : 'BELONGSID',
		type : 'string'
	}, {
		name : 'TYPE',
		type : 'string'
	}, {
		name : 'TITLE',
		type : 'string'
	} ]
});