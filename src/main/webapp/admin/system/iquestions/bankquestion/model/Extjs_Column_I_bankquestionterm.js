﻿/**
 * I_BANKQUESTIONTERM Column
 * @author CodeSystem 
 * 文件名 Extjs_Column_I_bankquestionterm 
 * Columns名    column_i_bankquestionterm
 */

Ext.define('column_i_bankquestionterm', {
	columns : [ {
		xtype : 'rownumberer',
		text : 'NO',
		sortable : true,
		align : 'center',
		width : 30
	}, {
		header : '选项编码',
		dataIndex : 'code',
		sortable : true,
		align : 'center',
		width : 50
	}, {
		header : '描述',
		dataIndex : 'description',
		sortable : true,
		align : 'left',
		width : '35%'
	}, {
		header : '是否答案',
		dataIndex : 'isanswer',
		sortable : true,
		align : 'center',
		width : 50,
		renderer : function(value ,metaData ,record ){
			if(value == 'true'){
				value="<font>是</font>";
			}else{
				value="<font>否</font>";
			}
			return value;
		}
	}, {
		header : '顺序号',
		dataIndex : 'sort',
		sortable : true,
		align : 'center',
		width : 40
	} ]
});
