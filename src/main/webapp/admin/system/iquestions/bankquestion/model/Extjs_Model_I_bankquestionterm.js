﻿/**
 * I_BANKQUESTIONTERM Model
 * @author CodeSystem 
 * 文件名 Extjs_Model_I_bankquestionterm 
 * Model名 model_i_bankquestionterm
 */

Ext.define('model_i_bankquestionterm', {
	extend : 'Ext.data.Model',
	fields : [ {
		name : 'id',
		type : 'string'
	}, {
		name : 'code',
		type : 'string'
	}, {
		name : 'description',
		type : 'string'
	}, {
		name : 'isanswer',
		type : 'string'
	}, {
		name : 'sort',
		type : 'string'
	} ]
});
