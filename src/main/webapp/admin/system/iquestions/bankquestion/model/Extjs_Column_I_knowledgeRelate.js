/**
 *i_knowledgeRelate Column
 *@author CodeSystem
 *文件名     Extjs_Column_I_knowledgeRelate
 *Columns名  column_i_knowledgeRelate
 */

Ext.define('column_i_knowledgeRelate', {
	columns : [ {
		xtype : 'rownumberer',
		text : 'NO',
		sortable : true,
		align : 'center',
		width : 80
	}, {
		header : '关联类型',
		dataIndex : 'TYPE',
		sortable : true,
		align : 'center',
		width : 80
	}, {
		header : '知识关联名称',
		dataIndex : 'TITLE',
		sortable : true,
		align : 'center',
		width : '50%'
	} ]
});