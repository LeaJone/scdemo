﻿/**
 *I_BANKQUESTION Model
 *@author CodeSystem
 *文件名     Extjs_Model_I_bankquestion
 *Model名    model_i_bankquestion
 */

Ext.define('model_i_bankquestion', {
	extend : 'Ext.data.Model',
	fields : [ {
		name : 'id',
		type : 'string'
	}, {
		name : 'typename',
		type : 'string'
	}, {
		name : 'levelname',
		type : 'string'
	}, {
		name : 'keynotename',
		type : 'string'
	}, {
		name : 'problem',
		type : 'string'
	}, {
		name : 'auditing',
		type : 'string'
	}, {
		name : 'sort',
		type : 'string'
	} ]
});
