/**
 * 知识库管理界面
 * @author 
 */

Ext.define('system.iquestions.bankquestion.biz.KnowledgeRelateEdit', {
	extend : 'Ext.window.Window',
	requires : [ 'system.iquestions.knowledgeInfo.model.Extjs_Column_I_knowledgeInfo',
	             'system.iquestions.knowledgeInfo.model.Extjs_Model_I_knowledgeInfo',
	             'system.admin.tools.model.Extjs_Column_SortOperate',
		         'system.admin.tools.model.Extjs_Model_SortOperate'],
	oper : Ext.create('system.iquestions.bankquestion.biz.BankQuestionOper'),
	header : false,
	border : 0,
	layout : 'fit',
	grid : null,
	belongid : '',
	initComponent : function() {
		var me = this;
		me.oper.winForm = this;
		var queryid = 'FSDKNOWLEDGE';
		var typeCode = 'FSDKNOWLEDGE';
		var typeName = '知识库分类';
		
		me.belongsid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属ID',
	    	name: 'belongsid',
	    	value : me.belongid
	    });
		
		/**
		 * 左边的树面板
		 */
		var treepanel = Ext.create("system.widget.FsdTreePanel", {
			icon : 'resources/admin/icon/chart_organisation.png',
			baseUrl : 'i_bankinfo/load_AsyncObjectTree.do',
			title: typeName + '结构',
			rootText : typeName,
			rootId : typeCode,
			region : 'west', // 设置方位
			collapsible : true,//折叠
			width: 182,
			split: true
		});
		treepanel.on('itemclick', function(view,record,item,index,e){
			queryid = record.raw.id;
			grid.getStore().loadPage(1);
		});
		
		
		var queryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入题库名称',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});
		
		// 创建表格
		var zslbgrid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_i_knowledgeInfo', // 显示列
			model: 'model_i_knowledgeInfo',
			baseUrl : 'i_knowledgeinfo/load_pagedata.do'		
		});
		zslbgrid.getStore().on('beforeload', function(s) {
			var name =  queryText.getValue();
			var pram = {fid : queryid , name : name};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		var contentpanel = Ext.create("Ext.panel.Panel", {
			frame:true,
			split: true,
			region: 'center',
			layout: 'fit',
			title: '分类信息',
			items: [zslbgrid]
		});

		var page1_jExtPanel1_obj = new Ext.panel.Panel({
			bodyPadding: 3,
			header : false,
			border : 0,
			split: true,
			layout: 'border',
			items: [treepanel,contentpanel]
		});
		
		Ext.apply(this,{
	        width : 1600,
	        height : 600,
	        autoScroll : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[page1_jExtPanel1_obj],
	        buttons : [{
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
				popedomCode : 'zsglbj',
			    handler : function() {
			    	me.oper.submitZhiShi(zslbgrid,me.belongid,me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
		});
		this.callParent(arguments);
	}
});