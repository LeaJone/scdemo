﻿/**
 * 题目库管理界面
 * @author lhl
 */

Ext.define('system.iquestions.bankquestion.biz.BankQuestionManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.iquestions.bankquestion.model.Extjs_Column_I_bankquestion',
	             'system.iquestions.bankquestion.model.Extjs_Model_I_bankquestion',
	             'system.iquestions.bankquestion.model.Extjs_Column_I_bankquestionterm',
	             'system.iquestions.bankquestion.model.Extjs_Model_I_bankquestionterm'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.iquestions.bankquestion.biz.BankQuestionOper'),
	
	createContent : function(){
		var me = this;

		var typeCode = 'FSDQUESTIONS';
		var typeName = '题目库';
		var tikuID = 'FSDQUESTIONS'; //记录当前选中的题库ID
		var timuID = ''; //记录当前选中的题目ID

		/**
		 * 左边的题库树面板
		 */
		var treepanel = Ext.create("system.widget.FsdTreePanel", {
			icon : 'resources/admin/icon/chart_organisation.png',
			baseUrl : 'i_bankinfo/load_AsyncObjectTree.do',
			title: '题目分类',
			rootId : typeCode,
			rootText : typeName,
			region : 'west', // 设置方位
			collapsible : true,//折叠
			width: '15%',
			split: true
		});
		
		var timuqueryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入题目名称',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						timugrid.getStore().loadPage(1);
					}
				}
			}
		});
		// 创建题目表格
		var timugrid = Ext.create("system.widget.FsdGridPanel", {
			column : 'column_i_bankquestion', // 显示列
			model: 'model_i_bankquestion',
			baseUrl : 'i_bankquestion/load_pagedata.do',
			tbar : [{
				xtype : "button",
				text : "添加",
				iconCls : 'page_addIcon',
				popedomCode : 'tmkbj',
				handler : function(button) {
					me.oper.addTimu(timugrid);
				}
			}, {
				xtype : "button",
				text : "修改",
				iconCls : 'page_editIcon',
				popedomCode : 'tmkbj',
				handler : function(button) {
					me.oper.editorTimu(timugrid);
				}
			}, {
				xtype : "button",
				text : "删除",
				iconCls : 'page_deleteIcon',
				popedomCode : 'tmksc',
				handler : function(button) {
				    me.oper.delTimu(timugrid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.viewTimu(timugrid);
				}
			}, {
				text : '排序',
				iconCls : 'up',
				handler : function() {
					var name =  timuqueryText.getValue();
					var pram = {fid : tikuID , name : name};
					me.oper.util.sortOperate(
							timugrid, 
							timugrid.column, 
							timugrid.model,
							timugrid.baseUrl,
							'i_bankquestion/sort_bankquestion.do',
							function(){return pram;},
				        	function(){}
					);
				}
			}, '-', {
				xtype : "fsdbutton",
				popedomCode : 'tmkbj',
				text : "审核",
				iconCls : 'tickIcon',
				handler : function(button) {
				    me.oper.isEnabled(timugrid, true);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'tmkbj',
				text : "停审",
				iconCls : 'crossIcon',
				handler : function(button) {
				    me.oper.isEnabled(timugrid, false);
				}
			}, '->', timuqueryText , {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					timugrid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					timugrid.getStore().reload();
				}
			}]			
		});
		timugrid.getStore().on('beforeload', function(s) {
			var name =  timuqueryText.getValue();
			var pram = {fid : tikuID , name : name};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
	        xuanxianggrid.getStore().removeAll();
		});
		timugrid.on('selectionchange', function(obj, selected, eOpts){
			if (selected.length == 1){
				timuID = selected[0].data.id;
				xuanxianggrid.getStore().loadPage(1);
			}else{
				timuID = '';
				xuanxianggrid.getStore().removeAll();
			}
		});
		
		// 创建试题选项表格
		var xuanxianggrid = Ext.create("system.widget.FsdGridPanel", {
			column : 'column_i_bankquestionterm', // 显示列
			model: 'model_i_bankquestionterm',
			baseUrl : 'i_bankquestionterm/load_pagedata.do',
			zAutoLoad : false,
			tbar : [{
				xtype : "button",
				text : "添加",
				iconCls : 'page_addIcon',
				popedomCode : 'tmkbj',
				handler : function(button) {
					me.oper.addTimuxuanxiang(xuanxianggrid, timugrid);
				}
			}, {
				xtype : "button",
				text : "修改",
				iconCls : 'page_editIcon',
				popedomCode : 'tmkbj',
				handler : function(button) {
					me.oper.editorTimuxuanxiang(xuanxianggrid);
				}
			}, {
				xtype : "button",
				text : "删除",
				iconCls : 'page_deleteIcon',
				popedomCode : 'tmksc',
				handler : function(button) {
				    me.oper.delTimuxuanxiang(xuanxianggrid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.viewTimuxuanxiang(xuanxianggrid);
				}
			}, {
				text : '排序',
				iconCls : 'up',
				handler : function() {
					var pram = {fid : timuID};
					me.oper.util.sortOperate(
							xuanxianggrid, 
							'column_i_bankquestionterm', 
							'model_i_bankquestionterm',
							'i_bankquestionterm/load_pagedata.do',
							'i_bankquestionterm/sort_bankquestionterm.do',
							function(){return pram},
				        	function(){});
				}
			}, '->', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					xuanxianggrid.getStore().reload();
				}
			}]			
		});
		xuanxianggrid.getStore().on('beforeload', function(s) {
			var pram = {fid : timuID};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		var timupanel = Ext.create("Ext.panel.Panel", {
			border : 0,
			region : 'west',
			split: true,
			frame:true,
			title: '题目信息',
			width: '60%',
			layout:'fit',
			items:[timugrid]
		});
		
		var xuanxiangpanel = Ext.create("Ext.panel.Panel", {
			border : 0,
			region: 'center',
			split: true,
			title: '题目选项',
			layout:'fit',
			items:[xuanxianggrid]
		});
		
		var rightpanel = Ext.create("Ext.panel.Panel", {
			layout: 'border',
			split: true,
			region: 'center',
			width: '85%',
			items: [timupanel,xuanxiangpanel]
		});
		
		var allPanel = Ext.create('Ext.panel.Panel', {
			bodyPadding: 3,
			header : false,
			border : 0,
			layout: 'border',
			items: [treepanel,rightpanel]
		});
		
		//题库节点单击事件，刷新试卷列表
		treepanel.on('itemclick', function(view,record,item,index,e){
			tikuID = record.raw.id;
			timuID = '';
			timugrid.getStore().loadPage(1);
			xuanxianggrid.getStore().removeAll();
		});
		return allPanel;
	}
});