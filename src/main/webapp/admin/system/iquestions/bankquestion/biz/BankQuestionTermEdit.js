/**
 * 题目选项编辑界面
 */
Ext.define('system.iquestions.bankquestion.biz.BankQuestionTermEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	tmid : '',
	oper : Ext.create('system.iquestions.bankquestion.biz.BankQuestionOper'),
	initComponent:function(){
	    var me = this;
	    
	    var bankquestionid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属题目编号',
	    	name: 'bankquestionid',
	    	value : me.tmid
	    });
	    
	    var code = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '选项编码',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'code',
	    	maxLength: 10,
	    	allowBlank: false
	    });
	    
	    var description = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '描述',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'description',
	    	maxLength: 100,
	    	allowBlank: false
	    });
	    
	    var imageurl1 = Ext.create('system.widget.FsdTextFileManage', {
	        width : 420,
	    	zName : 'imageurl1',
	    	zFieldLabel : '标题图片1',
	    	zLabelWidth : 80,
	    	zIsShowButton : true,
	    	zIsGetButton : true,
	    	zIsUpButton : true,
	    	zIsReadOnly : false,
	    	zContentObj : content,
	    	zFileUpPath : me.oper.util.getImagePath()
	    });
	    
	    var imageurl2 = Ext.create('system.widget.FsdTextFileManage', {
	        width : 420,
	    	zName : 'imageurl2',
	    	zFieldLabel : '标题图片2',
	    	zLabelWidth : 80,
	    	zIsShowButton : true,
	    	zIsGetButton : true,
	    	zIsUpButton : true,
	    	zIsReadOnly : false,
	    	zContentObj : content,
	    	zFileUpPath : me.oper.util.getImagePath()
	    });
	    
	    var videourl = Ext.create('system.widget.FsdTextFileManage', {
	        width : 420,
	    	zName : 'videourl',
	    	zFieldLabel : '内容视频',
	    	zLabelWidth : 80,
	    	zIsShowButton : true,
	    	zIsUpButton : true,
	    	zIsReadOnly : false,
	    	zFileType : 'video',
	    	zFileAutoName : false,
	    	zFileUpPath : me.oper.util.getVideoPath(),
	    	zManageType : 'manage'
	    });
	    
	    var voiceurl = Ext.create('system.widget.FsdTextFileManage', {
	        width : 420,
	    	zName : 'voiceurl',
	    	zFieldLabel : '内容语音',
	    	zLabelWidth : 80,
	    	zIsShowButton : true,
	    	zIsUpButton : true,
	    	zIsReadOnly : false,
	    	zFileType : 'voice',
	    	zFileAutoName : false,
	    	zFileUpPath : me.oper.util.getVoicePath(),
	    	zManageType : 'manage'
	    });

		var isanswer = new Ext.form.RadioGroup({
			fieldLabel : '是否答案',
			labelAlign:'right',
			labelWidth:80,
			width : 200,
			items : [ {
				name : 'isanswer',
				inputValue : 'true',
				boxLabel : '是',
				checked : true
			}, {
				name : 'isanswer',
				inputValue : 'false',
				boxLabel : '否'
			} ]
		});
		
		var parse = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '解析',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'parse'
	    });
	    
	    var sort = Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '顺序号',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'sort',
	    	maxLength: 3,
	    	minValue: 0,
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank: false
	    });
	    
	    var remark = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'remark',
	    	maxLength: 50
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 10',
			items : [{
                name: "id",
                xtype: "hidden"
            }, bankquestionid, code, description, imageurl1, imageurl2, videourl, voiceurl, isanswer, parse, sort, remark]
        });
	    
	    Ext.apply(this,{
	        width:500,
	        height:500,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
				popedomCode : 'tmkbj',
			    handler : function() {
    				me.oper.submitTimuxuanxiang(form,me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});