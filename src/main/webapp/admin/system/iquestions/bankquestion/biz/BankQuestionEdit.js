/**
 * 题目编辑界面
 */
Ext.define('system.iquestions.bankquestion.biz.BankQuestionEdit', {
	extend : 'Ext.panel.Panel',
	requires : [ 'system.iquestions.bankquestion.model.Extjs_Column_I_knowledgeRelate',
	             'system.iquestions.bankquestion.model.Extjs_Model_I_knowledgeRelate'],
	oper : Ext.create('system.iquestions.bankquestion.biz.BankQuestionOper'),
	layout: 'fit',
	grid: null,
	belongsid : '',
	relateid: '',
	viewObject : function(){
		var me = this;
		me.down('button[name=btnsave]').setVisible(false);
		me.down('button[name=btnnew]').setVisible(false);
		me.down('button[name=btnadd]').setVisible(false);
		me.down('button[name=btndel]').setVisible(false);
	 },
	initComponent:function(){
	    var me = this;
	    
	    me.idNo = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '编号',
	    	name : 'id'
	    });
	    
	    var tmxxTypeCode = 'FSDQUESTIONS';
		var tmxxTypeName = '题目库';
		
		var tmflTypeCode = 'FSDQUESTIONTYPE';
		var tmflTypeName = '题目分类';
	    
		me.sslmname = Ext.create('Ext.form.field.Hidden',{
			fieldLabel: '所属题目库名称',
			name: 'bankinfoname'
		});
		me.sslm = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属题目库',
	    	labelAlign:'right',
	    	labelWidth:75,
	    	rootText : tmxxTypeName,
	    	rootId : tmxxTypeCode,
	        width : 340,
	        name : 'bankinfoid',
	        baseUrl:'i_bankinfo/load_AsyncObjectTree.do',//访问路劲
	        allowBlank: false,
	        hiddenName : 'bankinfoname'//隐藏域Name
	    });
	    
	    var typename = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '题型名称',
	    	name: 'typename'
	    });
	    var typecode = new Ext.create('system.widget.FsdComboBoxZD',{
	    	fieldLabel: '题型',
	    	labelAlign:'right',
	    	labelWidth:75,
	    	width : 340,
	    	name: 'typecode',
	        key : 'i_tmlx',//参数
	        allowBlank: false,
    	    hiddenName : 'typename'//提交隐藏域Name
	    });
	    
	    var levelname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '难度名称',
	    	name: 'levelname'
	    });
	    var levelcode = new Ext.create('system.widget.FsdComboBoxZD',{
	    	fieldLabel: '难易程度',
	    	labelAlign:'right',
	    	labelWidth:75,
	    	width : 340,
	    	name: 'levelcode',
	        key : 'i_nycd',//参数
	        allowBlank: false,
    	    hiddenName : 'levelname'//提交隐藏域Name
	    });
	    
	    var keynotename = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '重点名称',
	    	name: 'keynotename'
	    });
	    var keynotecode = new Ext.create('system.widget.FsdComboBoxZD',{
	    	fieldLabel: '重点程度',
	    	labelAlign:'right',
	    	labelWidth:75,
	    	width : 340,
	    	name: 'keynotecode',
	        key : 'i_zdcd',//参数
	        allowBlank: false,
    	    hiddenName : 'keynotename'//提交隐藏域Name
	    });
	    
	    var ssflname = Ext.create('Ext.form.field.Hidden',{
			fieldLabel: '题目分类名称',
			name: 'questiontypename'
		});
		var ssfl = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '题目分类',
	    	labelAlign:'right',
	    	labelWidth:75,
	    	rootText : tmflTypeName,
	    	rootId : tmflTypeCode,
	        width : 340,
	        name : 'questiontypeid',
	        baseUrl:'i_bankinfo/load_AsyncObjectTree.do',//访问路劲
	        allowBlank: false,
	        hiddenName : 'questiontypename'//隐藏域Name
	    });
	    
	    var problem = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '题目',
	    	labelAlign:'right',
	    	labelWidth:75,
	        width : 750,
	    	name: 'problem',
	    	maxLength: 100,
	    	allowBlank: false
	    });
	    
	    var content = Ext.create('system.widget.FsdUeditor',{
	    	fieldLabel: '内容',
	    	labelAlign:'right',
	    	labelWidth:75,
	    	width : 776,
	    	height : 300,
	    	name: 'content',
	    	padding : '25 0 0 0'
	    });
	    
	    var keynote = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '重点',
	    	labelAlign:'right',
	    	labelWidth:75,
	        width : 750,
	    	name: 'keynote'
	    });
	    
	    var parse = Ext.create('system.widget.FsdUeditor',{
	    	fieldLabel: '解析',
	    	labelAlign:'right',
	    	labelWidth:75,
	    	width : 776,
	    	height : 300,
	    	name: 'parse'
	    });
	    
	    var sort = Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '顺序号',
	    	labelAlign:'right',
	    	labelWidth:75,
	        width : 340,
	    	name: 'sort',
	    	maxLength: 3,
	    	minValue: 0,
	    	allowBlank: false,
	    	allowDecimals : false//是否允许输入小数
	    });
	    
	    var remark = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:75,
	        width : 340,
	    	name: 'remark',
	    	maxLength: 100
	    });
	    
	    /*
	     * 题目面板
	     */
	    me.timupanel = Ext.create('Ext.form.Panel',{
			width: '50%',
			title : '添加题目',
			resizable:false,// 改变大小
			autoScroll : true,
			region : 'center', // 设置方位
			items : [me.sslmname, typename,levelname,levelname,keynotename,ssflname,me.idNo,
            {
				padding : '20 0 0 0',
                layout : 'column',
                border : false,
    	        width : 825,
                items:[{
                    columnWidth : 0.5,
                    border:false,
                    items:[me.sslm,typecode]
                }, {
                    columnWidth : 0.5,
                    border:false,
                    items:[levelcode ,keynotecode]
                }, {
                	columnWidth : 0.5,
                    border:false,
                    items:[ssfl]
                }]
			}, problem,content,keynote,parse,content,
			{
                layout : 'column',
				padding : '20 0 0 0',
                border : false,
    	        width : 825,
                items:[{
                    columnWidth : 0.5,
                    border : false,
                    items : [sort]
                }, {
                    columnWidth : 0.5,
                    border : false,
                    items : [remark]
                }]
			}]
	    });
	    
	    me.on('boxready' , function(obj, width, height, eOpts){
	    	ssfl.setText(ssflname.getValue());
	    });
	    
	    me.on('boxready' , function(){
	    	me.sslm.setText(me.sslmname.getValue());
	    });
	    
	    me.knowledgeGrid = Ext.create("system.widget.FsdGridPanel", {
			column : 'column_i_knowledgeRelate', // 显示列
			model: 'model_i_knowledgeRelate',
			baseUrl : 'i_knowledgerelate/load_data.do',
			tbar : [{
				xtype : "button",
				text : "添加",
				name : "btnadd",
				iconCls : 'page_addIcon',
				popedomCode : 'zsglbj',
				handler : function(button) {
					me.oper.addZhiShi(me.knowledgeGrid,me.belongsid);
				}
			}, {
				xtype : "button",
				text : "删除",
				name : "btndel",
				iconCls : 'page_deleteIcon',
				popedomCode : 'zsglsc',
				handler : function(button) {
				    me.oper.delZhiShi(me.knowledgeGrid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.currView(me.knowledgeGrid,knowledgepanel,me);
				}
			}]
	    });
	    me.knowledgeGrid.getStore().on('beforeload', function(s) {
			var pram = {fid : me.belongsid};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
	    
	    /*
	     * 关联知识面板
	     */
	    var zsglpanel = Ext.create("Ext.panel.Panel", {
			border : 0,
			layout: 'fit',
			resizable:false,// 改变大小
			autoScroll : true,
			region: 'north',
			split: true,
			frame:true,
			title: '关联知识',
			height: '30%',
			items: [me.knowledgeGrid]
		});
	    
	    me.bankinfoname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属题库名称',
	    	name: 'bankinfoname'
	    });
	    me.bankinfoid = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属知识库',
	    	labelAlign:'right',
	    	labelWidth:75,
	    	rootText : '知识库',
	    	rootId : 'FSDKNOWLEDGE',
	        width : 345,
	        name : 'bankinfoid',
	        baseUrl:'i_bankinfo/load_AsyncObjectTree.do',//访问路劲
	        allowBlank: false,
	        hiddenName : 'bankinfoname'//隐藏域Name
	    });
	    
		var questiontypename = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '题目分类名称',
	    	name: 'questiontypename'
	    });
	    var questiontypeid = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属分类',
	    	labelAlign:'right',
	    	labelWidth:75,
	    	rootText : '题目分类',
	    	rootId : 'FSDQUESTIONTYPE',
			rootVisible: true,
	        width : 345,
	        name : 'questiontypeid',
	        baseUrl:'i_bankquestiontype/load_AsyncObjectTree.do',//访问路劲
	        allowBlank: false,
	        hiddenName : 'questiontypename'//隐藏域Name
	    });
	    
	    var title = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '标题',
	    	labelAlign:'right',
	    	labelWidth:75,
	        width : 760,
	    	name: 'title',
	    	maxLength: 100,
	    	allowBlank: false
	    });
	    
	    var abstracts = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '简介',
	    	labelAlign:'right',
	    	labelWidth:75,
	        width : 760,
	    	name: 'abstracts',
	    	maxLength: 250
	    });
	    
	    var content = Ext.create('system.widget.FsdUeditor',{
	    	fieldLabel: '内容',
	    	labelAlign:'right',
	    	labelWidth:75,
	        width : 786,
	        height:300,
	    	toolbars : [['source']],
	    	name: 'content'
	    });
	    
	    var keynote = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '重点',
	    	labelAlign:'right',
	    	labelWidth:75,
	        width : 760,
	    	name: 'keynote',
	    	maxLength: 250,
	    	margin:'-50 0 0 0'
	    });
	    
	    var parse = Ext.create('system.widget.FsdUeditor',{
	    	fieldLabel: '解析',
	    	labelAlign:'right',
	    	labelWidth:75,
	        width : 786,
	        height:300,
	        toolbars : [['source']],
	    	name: 'parse'
	    });
	    
	    var remark = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:75,
	        width : 760,
	    	name: 'remark',
	    	maxLength: 50,
	    	margin:'-50 0 0 0'
	    });
	    
	    var levelname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '难易程度名称',
	    	name: 'levelname'
	    });
	    var levelcode = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'难易程度',
	        labelAlign:'right',
	    	labelWidth:75,
	    	width : 345,
    	    name : 'levelcode',//提交到后台的参数名
	        key : 'i_nycd',//参数
	        allowBlank: false,
	        zHiddenObject : levelname,
	        zDefaultCode : 'i_nywz'
	    });
	    
	    var keynotename = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '重点程度名称',
	    	name: 'keynotename'
	    });
	    var keynotecode = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'重点程度',
	        labelAlign:'right',
	    	labelWidth:75,
	    	width : 345,
    	    name : 'keynotecode',//提交到后台的参数名
	        key : 'i_zdcd',//参数
	        allowBlank: false,
	        zHiddenObject : keynotename,
	        zDefaultCode : 'i_zdwz'
	    });
	    
	    /*
	     * 关联知识详细信息面板
	     */
	   
	    var knowledgepanel = Ext.create('Ext.form.Panel',{
	    	title : '关联知识详细信息',
	    	autoScroll : true,
	    	resizable:false,// 改变大小
			region: 'center',
			items : [me.bankinfoname, questiontypename,levelname,keynotename,
        	{
				padding : '20 0 0 0',
            	layout : 'column',
            	border : false,
        	 	width : 835,
                items:[{
                	columnWidth : 0.5,
                    border:false,
                    items:[me.bankinfoid,questiontypeid]
                }, {
                    columnWidth : 0.5,
                    border:false,
                    items:[levelcode,keynotecode]
                }]
    		}, title, abstracts, content, keynote, parse,remark]
	    });
	    
		 me.all1panel = Ext.create("Ext.panel.Panel", {
			layout: 'border',
			disabled:true,
			frame:true,
			split: true,
			region: 'east',
			width: '50%',
			items: [zsglpanel, knowledgepanel]
		});
	    
	    var allPanel = Ext.create('Ext.panel.Panel', {
			bodyPadding: 3,
			header : false,
			border : 0,
			split: true,
			layout: 'border',
			items: [me.timupanel,me.all1panel]
		});
	    	
	    Ext.apply(me, {
			autoScroll : true,
	        tbar : [{
				xtype : "fsdbutton",
				popedomCode : 'tmkbj',
				text : "保存提交",
	        	name : 'btnsave',
				iconCls : 'acceptIcon',
				handler : function(button) {
					me.oper.submitTimu(me.grid,me,me.knowledgeGrid);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'tmkbj',
				text : "新增内容",
	        	name : 'btnnew',
				iconCls : 'tbar_synchronizeIcon',
				handler : function(button) {
					var obj = {
						id : null,
						problem : null,
						keynote : null,
						parse : null,
						content : null,
						sort : null,
						remark : null
					};
					me.timupanel.getForm().setValues(obj);
					me.all1panel.setDisabled(true);
					knowledgepanel.getForm().reset();
				}
			}],
	        items :[allPanel]
	    });
		me.callParent(arguments);
	}
});