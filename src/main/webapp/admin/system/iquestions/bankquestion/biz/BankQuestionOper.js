/**
 * 题目管理界面操作类
 * 
 * @author lhl
 */

Ext.define('system.iquestions.bankquestion.biz.BankQuestionOper', {

	form : null,
	util : Ext.create('system.util.util'),
	winForm : null,
	
	/**
	 * 添加题目
	 */
	addTimu : function(grid){
		var me = this;
		var model = Ext.create("system.iquestions.bankquestion.biz.BankQuestionEdit");
		model.grid = grid;
		me.util.addTab("addtmbj" , "添加题目信息" , model , "resources/admin/icon/vcard_add.png");
	},
	
	/**
	 * 添加试题选项
	 */
	addTimuxuanxiang : function(grid, timugrid){
		var data = timugrid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选要添加选项的所属题目记录！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{
    	    var win = Ext.create('system.iquestions.bankquestion.biz.BankQuestionTermEdit',{
    	    	tmid : data[0].data.id
    	    });
            win.setTitle('题目选项添加');
            win.grid = grid;
            win.modal = true;
            win.show();
        }
	},
	
	/**
	 * 添加关联知识
	 */
	addZhiShi : function(grid,timuID){
		var me = this;
		var win = Ext.create('system.iquestions.bankquestion.biz.KnowledgeRelateEdit',{
			belongid : timuID
		});
		win.setTitle('知识关联添加');
        win.grid = grid;
        win.modal = true;
        win.show();
	},
	
	/**
	 * 题目表单提交
	 */
	submitTimu : function(grid,panel,knowledgegrid){
        var me = this;
        if (panel.timupanel.form.isValid()) {
        	panel.all1panel.setDisabled(false);
            me.util.ExtFormSubmit('i_bankquestion/save_bankquestion_qx_tmkbj.do', panel.timupanel.form, '正在提交数据,请稍候.....',
			function(form, action){
            	panel.belongsid = action.result.data;
            	grid.getStore().reload();
            	knowledgegrid.getStore().reload();
				Ext.MessageBox.alert('提示', '保存成功！');
			});
        }
     },
	
	/**
	  * 题目选项表单提交
	  */
	submitTimuxuanxiang : function(formpanel,grid,url){
       var me = this;
       if (formpanel.form.isValid()) {
           me.util.ExtFormSubmit('i_bankquestionterm/save_bankquestionterm_qx_tmkbj.do', formpanel.form, '正在提交数据,请稍候.....',
			function(form, action){
          	formpanel.up('window').close();
			    grid.getStore().reload();
				Ext.MessageBox.alert('提示', '保存成功！');
			});
       }
    },
	
	/**
	 * 知识关联表单
	 */
	 submitZhiShi : function(grid,beid,glgrid){
	       var me = this;
	       var data = grid.getSelectionModel().getSelection();
	       if (data.length == 0) {
	    	   Ext.MessageBox.alert('提示', '请选中要要关联的知识点');
	       }else {
	    	   var dir = new Array();
               Ext.Array.each(data, function(items) {
                   var id = items.data.id;
                   dir.push(id);
   			   });
   			   var pram = {ids : dir, ssid : beid};
   			   var param = {jsonData : Ext.encode(pram)};
   			   me.util.ExtAjaxRequest('i_knowledgerelate/save_i_knowledgerelate_qx_zsglbj.do', param, function(response, options){
   				   me.winForm.close();
   				   glgrid.getStore().reload();
   			   }, null, me.form)
	       }
	    },
     
  	 delTimu : function(grid , treepanel){
  		this.del(grid, 'i_bankquestion/del_bankquestion_qx_tmksc.do');
  	 },
  	 delTimuxuanxiang : function(grid , treepanel){
  		this.del(grid, 'i_bankquestionterm/del_bankquestionterm_qx_tmksc.do');
  	 },
  	 delZhiShi : function(grid){
  		this.ZhishiDel(grid, 'i_knowledgerelate/del_i_knowledgerelate_qx_zsglsc.do');
  	 },
  	 
     /**
	   * 通用删除
	   */
     del : function(grid, url){
    	 var me = this;
 		 // 获取选中的行
 	     var data = grid.getSelectionModel().getSelection();
         if (data.length == 0) {
             Ext.MessageBox.alert('提示', '请先选中要删除的项目！');
         }else {
        	 Ext.MessageBox.show({
        		 title : '询问',
        		 msg : '您确定要删除吗?',
 			     width : 250,
 				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                     if (btn == 'yes') {
                    	 var dir = new Array();　
                         Ext.Array.each(data, function(items) {
                             var id = items.data.id;
                             dir.push(id);
             			});
             			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest(url, param,
                        function(response, options){
                        	grid.getStore().reload();
                        }, null, me.form);
                     }
                 }
             });
         }
 	},
 	
 	/**
 	 * 知识删除
 	 */
 	ZhishiDel : function(grid, url){
   	 var me = this;
		 // 获取选中的行
	     var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要删除的项目！');
        }else {
       	 Ext.MessageBox.show({
       		 title : '询问',
       		 msg : '您确定要删除吗?',
			     width : 250,
				 buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                   	 var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.ID;
                            dir.push(id);
            			});
            			var pram = {ids : dir};
                       var param = {jsonData : Ext.encode(pram)};
                       me.util.ExtAjaxRequest(url, param,
                       function(response, options){
                       	grid.getStore().reload();
                       }, null, me.form);
                    }
                }
            });
        }
	},
	
	editorTimuxuanxiang : function(grid){
		this.timuxuanxiangEditor(grid, 'system.iquestions.bankquestion.biz.BankQuestionTermEdit', 
				'修改题目选项', 'i_bankquestionterm/load_bankquestiontermbyid.do');
	},
	
	/**
	 * 修改题目
	 */
	editorTimu : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            // 用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var model = me.util.getTab("edittmbj");
            if (model == null){
    			model = Ext.create("system.iquestions.bankquestion.biz.BankQuestionEdit");
    			model.grid = grid;
    			model.belongsid = id;
    			model.all1panel.setDisabled(false);
    		}
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(model.timupanel, 'i_bankquestion/load_bankquestionbyid.do', param , null , 
	        function(response, options, respText, data){
	        	me.util.addTab("edittmbj" , "修改题目" , model , "resources/admin/icon/vcard_edit.png");
	        });
        }
	},
	
 	/**
	 * 修改题目选项
	 */
	timuxuanxiangEditor : function(grid, model, title, url){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            // 用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create(model);
            win.setTitle(title);
            win.modal = true;
            win.grid = grid;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , url, param , null , 
	        function(response, options, respText){
	        	win.show();
	        });
        }
	},
	
	viewTimuxuanxiang : function(grid){
		this.timuxuanxiangView(grid, 'system.iquestions.bankquestion.biz.BankQuestionTermEdit', 
				'查看题目选项', 'i_bankquestionterm/load_bankquestiontermbyid.do');
	},
	viewZhiShi : function(grid){
		this.currView(grid, 'system.iquestions.bankquestion.biz.KnowledgeRelateEdit', 
				'查看关联知识', 'i_knowledgerelate/load_i_knowledgerelatebyid.do');
	},
	
	/**
	 * 查看题目
	 */
	viewTimu : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要查看的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            // 用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var model = Ext.create("system.iquestions.bankquestion.biz.BankQuestionEdit");
            model.viewObject();
            model.belongsid = id;
            model.all1panel.setDisabled(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(model.timupanel,'i_bankquestion/load_bankquestionbyid.do', param , null , 
	        function(response, options, respText, data){
	        	me.util.addTab(id + "viewtmbj", "查看题目", model, "resources/admin/icon/vcard_edit.png");
	        });
        }
	},
	
 	/**
	 * 查看题目选项
	 */
	timuxuanxiangView : function(grid, model, title, url){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要查看的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            // 用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create(model);
            win.setTitle(title);
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , url, param , null , 
	        function(response, options, respText){
	        	win.show();
	        });
        }
	},
	
	/**
	 * 查看关联知识
	 */
	currView : function(grid,formpanel,panel){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要查看的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{
            // 先得到主键
            var id = data[0].data.RELATEID;
            // 用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
            var win = Ext.create(formpanel);
	        me.util.formLoad(formpanel.getForm(),'i_knowledgeinfo/load_i_knowledgeinfobyid.do',param,null,
	        function(response, options, respText){
	        	panel.bankinfoid.setText(panel.bankinfoname.getValue());
	        });
        }
	},
	
	/**
	 * 是否启用停用
	 */
	isEnabled : function(grid, isQT){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
		        msg : '您确定要修改状态吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
					    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
			            });
             			var pram = {ids : dir, isenabled : isQT};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('i_bankquestion/enabled_bankquestion_qx_tmkbj.do' , param ,
                        function(response, options){
            				Ext.MessageBox.alert('提示', '状态修改成功！');
                        	grid.getStore().reload(); 
                        });
                    }
                }
            });
        }
	}
});