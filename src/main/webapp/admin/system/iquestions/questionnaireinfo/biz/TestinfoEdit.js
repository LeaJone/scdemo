/**
 * 问卷编辑界面
 */
Ext.define('system.iquestions.questionnaireinfo.biz.TestinfoEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.iquestions.questionnaireinfo.biz.TestinfoOper'),
	initComponent:function(){
	    var me = this;

		var typeCode = 'FSDINVESTIGATE';
		var typeName = '问卷库';
		
	    var sslmname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属问卷库名称',
	    	name: 'bankinfoname'
	    });
	    var sslm = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属问卷库',
	    	labelAlign:'right',
	    	labelWidth:80,
	    	rootText : typeName,
	    	rootId : typeCode,
	        width : 420,
	        name : 'bankinfoid',
	        baseUrl:'i_bankinfo/load_AsyncObjectTree.do',//访问路劲
	        allowBlank: false,
	        hiddenName : 'bankinfoname'//隐藏域Name
	    });
	    
	    var mc = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '名称',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'name',
	    	fname : 'symbol',
	    	maxLength: 200,
	    	allowBlank: false
	    });
	    
	    var startDate = Ext.create('system.widget.FsdDateField',{
	    	fieldLabel: '开始时间',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	        allowBlank: false,
	    	name: 'stratdate'
	    });
	    
	    var stopDate = Ext.create('system.widget.FsdDateField',{
	    	fieldLabel: '结束时间',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'enddate'
	    		
	    });
	    
	    var beizhu = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'remark',
	    	maxLength: 100
	    });
	    
	    var ms = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '描述',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 420,
	    	name: 'description',
	    	maxLength: 200
	    });
	    
	    me.on('show' , function(){
	    	
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 10',
			items : [{
                name: "id",
                xtype: "hidden"
            }, sslmname, sslm, mc, startDate, stopDate, beizhu, ms]
        });
	    
	    Ext.apply(this,{
	        width:470,
	        height:315,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
				popedomCode : 'wjkbj',
			    handler : function() {
    				me.oper.submitTijuan(form, me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});