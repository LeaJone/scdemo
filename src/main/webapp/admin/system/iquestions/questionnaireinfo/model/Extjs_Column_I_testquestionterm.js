﻿/**
 *I_TESTQUESTIONTERM Column
 *@author CodeSystem
 *文件名     Extjs_Column_I_testquestionterm
 *Columns名  column_i_testquestionterm
 *xtype: 'textfield', 'numbercolumn', 'booleancolumn', 'datecolumn'   format : '0.00'
 *align: 'left', 'center', 'right'
 */

Ext.define('column_i_testquestionterm', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            align : 'center',
            width : 30
        }
        ,
        {
            header : '选项编码',
            dataIndex : 'code',
            sortable : true,
            align : 'left',
            width : 50
        }
        ,
        {
            header : '描述',
            dataIndex : 'description',
            sortable : true,
            align : 'left',
            width : '50%'
        }
        ,
        {
            header : '顺序号',
            dataIndex : 'sort',
            sortable : true,
            align : 'left',
            width : 40
        }
    ]
});
