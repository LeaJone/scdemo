﻿/**
 *I_TESTINFO Column
 *@author CodeSystem
 *文件名     Extjs_Column_I_testinfo
 *Columns名  column_i_testinfo
 *xtype: 'textfield', 'numbercolumn', 'booleancolumn', 'datecolumn'   format : '0.00'
 *align: 'left', 'center', 'right'
 */

Ext.define('column_i_testinfo', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            align : 'center',
            width : 30
        }
        ,
        {
            header : '所属问卷库',
            dataIndex : 'bankinfoname',
            sortable : true,
            align : 'left',
            flex : 3
        }
        ,
        {
            header : '名称',
            dataIndex : 'name',
            sortable : true,
            align : 'left',
            flex : 4
        }
        ,
        {
            header : '开始时间',
            dataIndex : 'stratdate',
            sortable : true,
            flex : 2,
            renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value);
			}
        }
        ,
        {
            header : '结束时间',
            dataIndex : 'enddate',
            sortable : true,
            flex : 2,
            renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value);
			}
        }
        ,
        {
            header : '状态',
            dataIndex : 'statusname',
            sortable : true,
            align : 'left',
            flex : 1
        }
    ]
});
