﻿/**
 *I_TESTINFO Model
 *@author CodeSystem
 *文件名     Extjs_Model_I_testinfo
 *Model名    model_i_testinfo
 */

Ext.define('model_i_testinfo', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'bankinfoname',
            type : 'string'
        }
        ,
        {
            name : 'name',
            type : 'string'
        }
        ,
        {
            name : 'stratdate',
            type : 'string'
        }
        ,
        {
            name : 'enddate',
            type : 'string'
        }
        ,
        {
            name : 'statusname',
            type : 'string'
        }
    ]
});
