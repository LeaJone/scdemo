/**
 * 权限验证
 */
Ext.define('system.util.UtilStatic', {
	
	//system.UtilStatic.formatDateTime(value); 使用方法
	
	subjectZTCode : 'FSDMAIN', //主栏目
	subjectXGCode : 'FSDRELATE', //相关栏目
	subjectWapCode : 'FSDMOBILE', //Wap栏目
	subjectWapXGCode : 'FSDMOBRELATE', //Wap相关栏目
	subjectZWCode : 'FSDGOVERNMENT', //政务栏目

	articleRelateLM : "wzgllm", //文章关联栏目
	articleRelateXG : "wzglxg", //文章关联相关栏目
	articleRelateWAP : "wzglwap", //文章关联Wap栏目
	articleRelateWAPXG : "wzglwapxg", //文章关联Wap相关栏目
	
	articleTypeWZ : 'wzwz', //文章
	articleTypeZT : 'wzzt', //组图
	articleTypeSP : 'wzsp', //视频
	articleTypeWJ : 'wzwj', //文件
	
	popedomCD : 'qxlxcd',//菜单权限
	popedomJG : 'qxlxjg',//机构权限
	popedomLM : 'qxlxlm',//栏目权限
	popedomLMW : 'qxlxlmw',//Wap栏目权限
	/*
	 * 选中记录对象ID信息
	 */
	chooseObjIDText : Ext.create('Ext.toolbar.TextItem'),
	/*
	 * grid注册选择事件，显示ID信息
	 */
	setGridChooseObjID : function(grid){
		var me = this;
		grid.on('selectionchange', function(obj, selected, eOpts){
			if (selected.length == 1){
				if(selected[0].data != null && selected[0].data.id != null){
					me.chooseObjIDText.setText('选择记录ID：' + selected[0].data.id);
				}else{
					me.chooseObjIDText.setText('');
				}
			}else{
				me.chooseObjIDText.setText('');
			}
		});
	},

	/**
	 * 转换时间格式
	 */
	formatDateTime : function(strDate, isTime){
		if (strDate.toString().length == 14){
			if (isTime != null && isTime == false){
				return strDate.toString().substring(0, 4) + '年' + 
					strDate.toString().substring(4, 6) + '月' + 
					strDate.toString().substring(6, 8) + '日';
			}
			return strDate.toString().substring(0, 4) + '年' + 
				strDate.toString().substring(4, 6) + '月' + 
				strDate.toString().substring(6, 8) + '日 ' + 
				strDate.toString().substring(8, 10) + ':' + 
				strDate.toString().substring(10, 12) + ':' + 
				strDate.toString().substring(12, 14);
		}else if(strDate.toString().length == 6){
			return strDate.toString().substring(0, 4) + '年' +
				strDate.toString().substring(4, 6) + '月';
		}else{
			return strDate.toString().substring(0, 4) + '年' + 
				strDate.toString().substring(4, 6) + '月' + 
				strDate.toString().substring(6, 8) + '日';
		}
	},
	
	/**
	 * 根据给定的宽度获得等比缩放的高度
	 * imgUrl : 图片地址
	 * width : 宽度
	 */
	getImageHeight : function(imgUrl, width){
		var img = new Image();
		img.src = '/FsdSoft' + imgUrl;
		var imgwidth = img.width;
	    var imgheight = img.height;
	    if (imgwidth == 0)
	    	return 0;
	    var con = width/imgwidth;
	    return imgheight * con;
	}
}
, 
function() {
	system.UtilStatic = new this();
}
);