﻿Ext.define('system.util.util', {
	/**
	 * Ajax统一请求
	 */
	ExtAjaxRequest : function(url, param, success, failure, element){
		var me = this;
		var loadMarsk = null;
		if (element != null){
			loadMarsk = new Ext.LoadMask(element, {  
	 			msg:'正在处理数据，请稍候......',  
	 			removeMask:true // 完成后移除  
	 		}); 
	 		loadMarsk.show();  //显示 
		}
		Ext.Ajax.request({
            url: url, 
            params: param,
            timeout: 300000,//300秒
            success: function (response, options) {
            	if (loadMarsk != null)
            		loadMarsk.hide();  //隐藏 
            	if(success != null){
            		var respText = Ext.JSON.decode(response.responseText);
            		if(respText.success){
            			success(response, options, respText);
            		}
				}
            }, 
            failure: function (response, options) {
            	if (loadMarsk != null)
            		loadMarsk.hide();  //隐藏 
                if (failure != null){
                    failure(response, options);
                }
            }
        });
	},
	
	/**
	 * 统一表单提交
	 */
	ExtFormSubmit : function(url, form, waitMsg, success, failure, params){
		var me = this;
		if(waitMsg == null || waitMsg == ""){
			waitMsg = '数据提交中,请稍候.....';
		}
		form.submit({
			url : url,
			method : 'POST',
			waitTitle : '提示',
			waitMsg : waitMsg,
			params : params,
			success : function(form, action) {
				if(success != null){
					var respText = Ext.JSON.decode(action.response.responseText);
            		if(respText.success){
            			success(form, action, respText);
            		}
				}
			},
			failure : function(form, action) {
                if (failure != null){
                    failure(form, action);
                }
			}
		});
	},
	
	/**
	 * 统一表单自动加载
	 */
	 formLoad : function(from , url , params ,waitMsg, success, failure){
		if(waitMsg == null || waitMsg == ""){
			waitMsg = '数据加载中,请稍候.....';
		}
		from.load({
			url : url,
            method:'POST',
            waitTitle : '提示',
			waitMsg : waitMsg,
			params : params,
			success : function(form, action) {
				if(success != null){
            		var respText = Ext.JSON.decode(action.response.responseText);
            		if(respText.success){
                		from.object = action.result.data;
                		success(form, action, respText, action.result);
            		}
				}
			},
			failure : function(form, action) {
                if (failure != null){
                    failure(form, action);
                }
			}
        });
	 },
	
	/**
	 * 统一获得gridStore
	 */
	 getGridStore : function(url , model , params , success , failure){
	    if(params == null){
	        params = {};
	    }
	    // 创建Store
		var store = Ext.create('Ext.data.Store', {
			model : model,
			proxy : {
				type : 'ajax',
				params : params,
				url : url,
				method: 'POST',
				reader : {
					type : 'json',// 数据格式为json
					root : "data"
				}
			},
			autoLoad : true,
			success: function(response, options) {      
                if(success != null){
                	var respText = Ext.JSON.decode(options.response.responseText);
                	if(respText.success){
    				    success(response, options, respText);
                	}
			    }
            },
            failure: function(response, options) {
            	if(failure != null){
				    failure(response, options);
			    }
            }
		});
		return store;
	 },
	
	/**
	 * 设置Cookie
	 */
	setCookie : function(name,value,time){
		var me = this;
		var Days = time; //此 cookie 将被保存 30 天
	    var exp  = new Date();    //new Date("December 31, 9998");
	    exp.setTime(exp.getTime() + Days*24*60*60*1000);
	    document.cookie = name + "="+ escape (value) + ";path=/;expires=" + exp.toGMTString();
	},
	/**
	 * 得到Cookie
	 */
	getCookie : function(name){
		var me = this;
		var arr = document.cookie.match(new RegExp("(^| )"+name+"=([^;]*)(;|$)"));
		if(arr != null) return unescape(arr[2]); return null;
	},
	/**
	 * 清除Cookie
	 */
	clearCookie : function(name){
		var me = this;
		me.setCookie(name , '' , -1);
	},
	
	/**
	 * 设置全局参数
	 * 
     *util.setParams("user" , respText.user);	
     *util.setParams("company" , respText.company);
     *util.setParams("popdom" , respText.popedom);
	 */
	setParams : function(key , value){
		Ext[key] = value;
	},
	
	/**
	 * 得到全局参数
	 */
	getParams : function(key){
		return Ext[key];
	},
	
	/**
	 * 获取打开一个选项卡
	 */
	getTab : function(id){
		var contentTable = Ext.getCmp('mainindextabpanel');
		var tabitems = contentTable.items;
		for(var i=0; i < tabitems.length; i++){
			if(tabitems.get(i).id == id){
				return tabitems.get(i).items.get(0);
			}
		}
		return null;
	},
	
	/**
	 * 打开一个选项卡
	 */
	addTab : function(id , text , module , icon){
		var contentTable = Ext.getCmp('mainindextabpanel');
		var contentPanel = Ext.getCmp('mainindexpanel');
		var tabitems = contentTable.items;
		for(var i=0; i < tabitems.length; i++){
			var c=tabitems.get(i).id;
			if(c == id){
				contentTable.setActiveTab(i);
				return;
			}
		}
		contentTable.add({
            title: text,
            id: id,
            iconCls : 'tabIcon fa ' + icon,
            closable: true, //这个tab可以被关闭
            layout : 'fit',
            border : 0 ,
            items : [module]
        });
		contentTable.setActiveTab(contentTable.items.length - 1);
		contentPanel.setTitle("综合信息平台-" + text);
	},
	
	/**
	 * 打开一个窗口
	 */
	openWin : function(text, win, icon, ismodal){
		win.setTitle(text);
		win.modal = ismodal;
		win.show();
	},
	
	/**
	 * 关闭当前激活的选项卡
	 */
	closeTab : function(){
		var contentTable = Ext.getCmp('mainindextabpanel');
		var tab = contentTable.getActiveTab(); //当前tab的id
		contentTable.remove(tab);
	},
	
	//颜色选择器
	colorPicker : function(method){
		var colorWin = new Ext.Window({
			border :0,  
			frame:false,  
			closeAction :'hide',
			resizable :false,
			draggable : false,
			modal : true,
			width :155,
			height :125,  
			items : [{
				xtype:'colorpicker',  
				listeners: {
					select: function(picker, selColor) {
						var color = '#' + selColor;
						method(color);
						colorWin.hide();
					}
				}  
			}]  
		});
		colorWin.show();  
	},
	
	sortOperate : function(grid, column, model, baseUrl, submitUrl, funQuery, funChoose){
		var models = grid.getSelectionModel().getSelection();
		if (models.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中记录后排序！');
            return;
		}
        var win = Ext.create('system.admin.tools.biz.SortOperate', {
			column : column, // 显示列
			model: model,
			baseUrl : baseUrl,
			models : models,
			submitUrl : submitUrl,
        	funQuery : funQuery,
        	util : this,
        	funChoose : function(){
        		if (grid != null)
	        		grid.getStore().reload();
	        	if (funChoose != null)
	        		funChoose();
        	}
        });
        win.setTitle('记录排序');
        win.modal = true;
        win.show();
	},

	/*
	 * 上传系统所需路径
	 */
	getSystemPath : function(){
		var me = this;
		return 'uploadfiles/' + me.getParams("company").id + '/system/'
	},
	/*
	 * 上传图片路径
	 */
	getImagePath : function(){
		var me = this;
		return 'uploadfiles/' + me.getParams("company").id + '/image/';
	},
	/*
	 * 上传文件路径路径
	 */
	getFilePath : function(){
		var me = this;
		return 'uploadfiles/' + me.getParams("company").id + '/file/';
	},
	/*
	 * 上传视频路径
	 */
	getVideoPath : function(){
		var me = this;
		return 'uploadfiles/' + me.getParams("company").id + '/video/'
	},
	/*
	 * 上传语音路径
	 */
	getVoicePath : function(){
		var me = this;
		return 'uploadfiles/' + me.getParams("company").id + '/voice/'
	},
	/*
	 * 上传临时文件路径
	 */
	getTempPath : function(){
		var me = this;
		return 'uploadfiles/temp/'
	},
	/*
	 * 检查权限
	 */
	checkPopedom : function(popedomType, code){
		var me = this;
		if(code == null || code == ''){
			return false;
		}
		if (me.getParams("user").isadmin){
			return true;
		}
		var qxlist = null;
		switch (popedomType) {
		case system.UtilStatic.popedomCD:
			qxlist = me.getParams("popdom").qxlxcd;
			break;
		case system.UtilStatic.popedomJG:
			qxlist = me.getParams("popdom").qxlxjg;
			break;
		case system.UtilStatic.popedomLM:
			qxlist = me.getParams("popdom").qxlxlm;
			break;
		case system.UtilStatic.popedomLMW:
			qxlist = me.getParams("popdom").qxlxlmw;
			break;
		}
		if(qxlist == null){
			return false;
		}
		for(i=0; i < qxlist.length; i++){
			if(qxlist[i] == code){
				return true;
			}
		}
	},
	
	/**
	 * 文本控件光标处插入指定内容
	 * obj : 文本控件
	 * value : 制定内容
	 */
	cursorInsert : function(obj, value){
		if(document.selection){
			obj.inputEl.dom.focus();
			var rng=document.selection.createRange();
			rng.setEndPoint("StartToStart",obj.inputEl.dom.createTextRange());
			var eng=document.selection.createRange();
			eng.setEndPoint("EndToEnd",obj.inputEl.dom.createTextRange());
			obj.setValue(rng.text + value + eng.text);
		}else{
			var rulevalue = obj.getValue();
	        var start = obj.inputEl.dom.selectionStart;
	        var end = obj.inputEl.dom.selectionEnd;
	        var oriValue = obj.getValue().toString();
	        obj.setValue(oriValue.substring(0,start) + value + oriValue.substring(end));
		}
	}
});
