/**
 * 单位类型界面操作类
 * @author lw
 */

Ext.define('system.bcms.wordkeybad.biz.WordKeyBadOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 添加
     */
	addKeyWord : function(grid){
	    var win = Ext.create('system.bcms.wordkeybad.biz.KeyWordEdit');
        win.setTitle('关键词添加');
        win.grid = grid;
        win.modal = true;
        win.show();
	},
	/**
     * 添加
     */
	addBadWord : function(grid){
	    var win = Ext.create('system.bcms.wordkeybad.biz.BadWordEdit');
        win.setTitle('敏感词添加');
        win.grid = grid;
        win.modal = true;
        win.show();
	},

	submitKeyWord : function(formpanel , grid){
		this.formSubmit(formpanel, grid, 'b_wordkeybad/save_WordKeyBad_qx_gjcbj.do');
	},
	submitBadWord : function(formpanel , grid){
		this.formSubmit(formpanel, grid, 'b_wordkeybad/save_WordKeyBad_qx_mgcbj.do');
	},
	/**
     * 表单提交
     */
     formSubmit : function(formpanel, grid, url){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit(url , formpanel.form , '正在提交数据,请稍候.....',
			function(form, action){
			    formpanel.up('window').close();
			    grid.getStore().reload();
				Ext.MessageBox.alert('提示', '保存成功！');
			},
			function (form, action){
				Ext.MessageBox.alert('异常', action.result.msg);
			});
        }
     },

    delKeyWord : function(grid){
 		this.del(grid, treepanel, 'b_wordkeybad/del_WordKeyBad_qx_gjcsc.do');
 	},
 	delBadWord : function(grid){
 		this.del(grid, 'b_wordkeybad/del_WordKeyBad_qx_mgcsc.do');
 	},
     /**
      * 删除
      */
     del : function(grid, url){
    	 var me = this;
 		 // 获取选中的行
 	     var data = grid.getSelectionModel().getSelection();
         if (data.length == 0) {
             Ext.MessageBox.alert('提示', '请先选中要删除的项目！');
         }else {
        	 Ext.MessageBox.show({
        		 title : '询问',
        		 msg : '您确定要删除吗?',
 			     width : 250,
 				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                     if (btn == 'yes') {
                    	 var dir = new Array();　
                         Ext.Array.each(data, function(items) {
                             var id = items.data.id;
                             dir.push(id);
             			});
             			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest(url, param,
                        function(response, options){
                        	grid.getStore().reload();
                        }, null, me.form);
                     }
                 }
             });
         }
 	},

	editorKeyWord : function(grid){
		this.editor(grid, 'system.bcms.wordkeybad.biz.KeyWordEdit', '关键词修改');
	},
	editorBadWord : function(grid){
		this.editor(grid, 'system.bcms.wordkeybad.biz.BadWordEdit', '敏感词修改');
	},
 	/**
     * 修改
     */
	editor : function(grid, url, title){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{

            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            
            var win = Ext.create(url);
            win.setTitle(title);
            win.modal = true;
            win.grid = grid;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'b_wordkeybad/load_WordKeyBadByid.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},

	viewKeyWord : function(grid){
		this.view(grid, 'system.bcms.wordkeybad.biz.KeyWordEdit', '关键词查看');
	},
	viewBadWord : function(grid){
		this.view(grid, 'system.bcms.wordkeybad.biz.BadWordEdit', '敏感词查看');
	},
 	/**
     * 查看
     */
	view : function(grid, url, title){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if(data.length > 0){
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create(url);
            win.setTitle(title);
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'b_wordkeybad/load_WordKeyBadByid.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},
	
	isEnabledKeyWord : function(grid, isQT){
		this.isEnabled(grid, isQT, 'b_wordkeybad/enabled_WordKeyBad_qx_gjcbj.do', '关键词');
	},
	isEnabledBadWord : function(grid, isQT){
		this.isEnabled(grid, isQT, 'b_wordkeybad/enabled_WordKeyBad_qx_mgcbj.do', '敏感词');
	},
	/**
     * 是否启用停用
     */
	isEnabled : function(grid, isQT, url, message){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
		        msg : '您确定要修改' + message + '状态吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
					    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
			            });
             			var pram = {ids : dir, isenabled : isQT};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest(url , param ,
                        function(response, options){
            				Ext.MessageBox.alert('提示', message + '状态修改成功！');
                        	grid.getStore().reload(); 
                        });
                    }
                }
            });
        }
	}
});