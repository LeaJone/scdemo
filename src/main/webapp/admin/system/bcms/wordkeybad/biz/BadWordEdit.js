﻿/**
 * 机构添加界面
 * @author lw
 */
Ext.define('system.bcms.wordkeybad.biz.BadWordEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.bcms.wordkeybad.biz.WordKeyBadOper'),
	initComponent:function(){
	    var me = this;
	    
	    var baseid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属根类',
	    	name: 'baseid',
	    	value: 'FSDBADWORD'
	    });
	    
	    var wordtypeid = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属类型',
	    	labelAlign:'right',
	    	labelWidth:70,
	    	rootText : '敏感词类型',
	    	rootId : 'FSDBADWORD',
	    	rootVisible : false,//不显示根节点
	        width : 340,
	        name : 'wordtypeid',
	        baseUrl:'b_wordtype/load_AsyncWordTypeTree.do',//访问路劲	        
	        allowBlank: false,
	        blankText: '请选择所属类型',
	        hiddenName : 'wordtypename'//隐藏域Name
	    });
	    var wordtypename = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属类型名称',
	    	name: 'wordtypename'
	    });
	    
	    var name = new Ext.create('system.widget.FsdTextFieldZJF',{
	    	fieldLabel: '敏感词',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'name',
	    	fname : 'symbol',
	    	maxLength: 20,
	    	allowBlank: false,
	    	blankText: '请输入敏感词名称',
	    	maxLengthText: '敏感词不能超过20个字符'
	    });
	    
	    var symbol = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '助记符',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'symbol',
	    	maxLength: 20,
	    	allowBlank: false,
	    	blankText: '请输入助记符',
	    	maxLengthText: '助记符不能超过20个字符'
	    });

	    var status = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'状态',
	        labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
    	    name : 'status',//提交到后台的参数名
	        key : 'gjczt',//参数
    	    hiddenName : 'statusname'//提交隐藏域Name
	    });
	    var statusname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '状态名',
	    	name: 'statusname'
	    });
	    
	    var remark = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'remark',
	    	maxLength: 50,
	    	maxLengthText: '备注不能超过50个字符'
	    });
	    
	    me.on('show' , function(){
	    	wordtypeid.setText(wordtypename.getValue());
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [{
                name: "id",
                xtype: "hidden"
            }, baseid, wordtypename, statusname, wordtypeid, name, symbol, status, remark]
        });
	        
	    Ext.apply(this,{
	        width:400,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'mgcbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.submitBadWord(form, me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});