﻿/**
 *b_wordkeybad Column
 *@author CodeSystem
 *文件名     Extjs_Column_B_wordkeybad
 *Columns名  column_b_wordkeybad
 */

Ext.define('column_b_wordkeybad', {
    columns: [
              {
                  xtype : 'rownumberer',
                  text : 'NO',
                  sortable : true,
                  width : 40
              }
              ,
        {
            header : '名词',
            dataIndex : 'name',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '所属类型',
            dataIndex : 'wordtypename',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '助记符',
            dataIndex : 'symbol',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '状态',
            dataIndex : 'statusname',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '备注',
            dataIndex : 'remark',
            sortable : true,
            flex : 1
        }
    ]
});
