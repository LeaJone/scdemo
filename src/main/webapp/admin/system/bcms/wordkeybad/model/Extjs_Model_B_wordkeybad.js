﻿/**
 *b_wordkeybad Model
 *@author CodeSystem
 *文件名     Extjs_Model_B_wordkeybad
 *Model名    model_b_wordkeybad
 */

Ext.define('model_b_wordkeybad', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'companyid',
            type : 'string'
        }
        ,
        {
            name : 'wordtypeid',
            type : 'string'
        }
        ,
        {
            name : 'wordtypename',
            type : 'string'
        }
        ,
        {
            name : 'name',
            type : 'string'
        }
        ,
        {
            name : 'symbol',
            type : 'string'
        }
        ,
        {
            name : 'remark',
            type : 'string'
        }
        ,
        {
            name : 'status',
            type : 'string'
        }
        ,
        {
            name : 'statusname',
            type : 'string'
        }
    ]
});
