/**
 * 单位类型界面操作类
 * @author lw
 */

Ext.define('system.bcms.wordtype.biz.WordTypeOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 添加
     */
	addKeyWord : function(grid , treepanel){
	    var win = Ext.create('system.bcms.wordtype.biz.KeyWordEdit');
        win.setTitle('关键词类型添加');
        win.grid = grid;
        win.treepanel =  treepanel;
        win.modal = true;
        win.show();
	},
	/**
     * 添加
     */
	addBadWord : function(grid , treepanel){
	    var win = Ext.create('system.bcms.wordtype.biz.BadWordEdit');
        win.setTitle('敏感词类型添加');
        win.grid = grid;
        win.treepanel =  treepanel;
        win.modal = true;
        win.show();
	},

	submitKeyWord : function(formpanel , grid , treepanel){
		this.formSubmit(formpanel, grid, treepanel, 'b_wordtype/save_WordType_qx_gjclxbj.do');
	},
	submitBadWord : function(formpanel , grid , treepanel){
		this.formSubmit(formpanel, grid, treepanel, 'b_wordtype/save_WordType_qx_mgclxbj.do');
	},
	/**
     * 表单提交
     */
     formSubmit : function(formpanel , grid , treepanel, url){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit(url , formpanel.form , '正在提交数据,请稍候.....',
			function(form, action){
			    formpanel.up('window').close();
			    grid.getStore().reload();
			    treepanel.getStore().load();
				Ext.MessageBox.alert('提示', '保存成功！');
			},
			function (form, action){
				Ext.MessageBox.alert('异常', action.result.msg);
			});
        }
     },

    delKeyWord : function(grid , treepanel){
 		this.del(grid, treepanel, 'b_wordtype/del_WordType_qx_gjclxsc.do');
 	},
 	delBadWord : function(grid , treepanel){
 		this.del(grid, treepanel, 'b_wordtype/del_WordType_qx_mgclxsc.do');
 	},
     /**
      * 删除
      */
     del : function(grid , treepanel, url){
    	 var me = this;
 		 // 获取选中的行
 	     var data = grid.getSelectionModel().getSelection();
         if (data.length == 0) {
             Ext.MessageBox.alert('提示', '请先选中要删除的项目！');
         }else {
        	 Ext.MessageBox.show({
        		 title : '询问',
        		 msg : '您确定要删除吗?',
 			     width : 250,
 				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                     if (btn == 'yes') {
                    	 var dir = new Array();　
                         Ext.Array.each(data, function(items) {
                             var id = items.data.id;
                             dir.push(id);
             			});
             			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest(url, param,
                        function(response, options){
                        	grid.getStore().reload(); 
            			    treepanel.getStore().load();
                        }, null, me.form);
                     }
                 }
             });
         }
 	},

	editorKeyWord : function(grid , treepanel){
		this.editor(grid, treepanel, 'system.bcms.wordtype.biz.KeyWordEdit', '关键词类型修改');
	},
	editorBadWord : function(grid , treepanel){
		this.editor(grid, treepanel, 'system.bcms.wordtype.biz.BadWordEdit', '敏感词类型修改');
	},
 	/**
     * 修改
     */
	editor : function(grid , treepanel, url, title){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{

            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            
            var win = Ext.create(url);
            win.setTitle(title);
            win.modal = true;
            win.grid = grid;
            win.treepanel =  treepanel;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'b_wordtype/load_WordTypeByid.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},

	viewKeyWord : function(grid){
		this.view(grid, 'system.bcms.wordtype.biz.KeyWordEdit', '关键词类型查看');
	},
	viewBadWord : function(grid){
		this.view(grid, 'system.bcms.wordtype.biz.BadWordEdit', '敏感词类型查看');
	},
 	/**
     * 查看
     */
	view : function(grid, url, title){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if(data.length > 0){
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create(url);
            win.setTitle(title);
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'b_wordtype/load_WordTypeByid.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	}
});