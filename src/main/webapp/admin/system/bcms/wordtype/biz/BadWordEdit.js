﻿/**
 * 机构添加界面
 * @author lw
 */
Ext.define('system.bcms.wordtype.biz.BadWordEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	treepanel : null,
	oper : Ext.create('system.bcms.wordtype.biz.WordTypeOper'),
	initComponent:function(){
	    var me = this;

	    var baseid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '根级ID',
	    	name: 'baseid',
	    	value: 'FSDBADWORD'
	    });
	    
	    var parentid = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属类型',
	    	labelAlign:'right',
	    	labelWidth:70,
	    	rootText : '敏感词类型',
	    	rootId : 'FSDBADWORD',
	        width : 340,
	        name : 'parentid',
	        baseUrl:'b_wordtype/load_AsyncWordTypeTree.do',//访问路劲	        
	        allowBlank: false,
	        blankText: '请选择所属类型',
	        hiddenName : 'parentname'//隐藏域Name
	    });
	    var parentname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属类型名称',
	    	name: 'parentname'
	    });
	    
	    var name = new Ext.create('system.widget.FsdTextFieldZJF',{
	    	fieldLabel: '敏感词类型',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'name',
	    	fname : 'symbol',
	    	maxLength: 20,
	    	allowBlank: false,
	    	blankText: '请输入类型名称',
	    	maxLengthText: '敏感词类型不能超过20个字符'
	    });
	    
	    var symbol = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '助记符',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'symbol',
	    	maxLength: 20,
	    	allowBlank: false,
	    	blankText: '请输入助记符',
	    	maxLengthText: '助记符不能超过20个字符'
	    });
	    
	    var sort = new Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '排序编号',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'sort',
	    	maxLength: 3,
	    	minValue: 0,
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank: false,
	    	blankText: '请输入排序编号',
	    	maxLengthText: '排序编号不能超过3个字符'
	    });
	    
	    var remark = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'remark',
	    	maxLength: 50,
	    	maxLengthText: '备注不能超过50个字符'
	    });
	    
	    me.on('show' , function(){
	    	parentid.setText(parentname.getValue());
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [{
                name: "id",
                xtype: "hidden"
            }, baseid, parentname, parentid, name, symbol, sort, remark]
        });
	        
	    Ext.apply(this,{
	        width:400,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'mgclxbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.submitBadWord(form, me.grid, me.treepanel);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});