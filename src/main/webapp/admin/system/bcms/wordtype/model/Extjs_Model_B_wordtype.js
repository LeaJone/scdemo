﻿/**
 *b_wordtype Model
 *@author CodeSystem
 *文件名     Extjs_Model_B_wordtype
 *Model名    model_b_wordtype
 */

Ext.define('model_b_wordtype', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'parentid',
            type : 'string'
        }
        ,
        {
            name : 'parentname',
            type : 'string'
        }
        ,
        {
            name : 'name',
            type : 'string'
        }
        ,
        {
            name : 'symbol',
            type : 'string'
        }
        ,
        {
            name : 'sort',
            type : 'int'
        }
        ,
        {
            name : 'remark',
            type : 'string'
        }
    ]
});
