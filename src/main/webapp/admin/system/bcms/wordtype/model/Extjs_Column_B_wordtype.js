﻿/**
 *b_wordtype Column
 *@author CodeSystem
 *文件名     Extjs_Column_B_wordtype
 *Columns名  column_b_wordtype
 */

Ext.define('column_b_wordtype', {
    columns: [
              {
                  xtype : 'rownumberer',
                  text : 'NO',
                  sortable : true,
                  width : 40
              }
              ,
        {
            header : '名称',
            dataIndex : 'name',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '助记符',
            dataIndex : 'symbol',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '所属类别',
            dataIndex : 'parentname',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '序号',
            dataIndex : 'sort',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '备注',
            dataIndex : 'remark',
            sortable : true,
            flex : 1
        }
    ]
});
