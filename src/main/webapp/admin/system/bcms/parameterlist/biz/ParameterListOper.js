/**
 * 单位类型界面操作类
 * @author lw
 */

Ext.define('system.bcms.parameterlist.biz.ParameterListOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 添加
     */
	add : function(grid){
	    var win = Ext.create('system.bcms.parameterlist.biz.ParameterListEdit');
        win.setTitle('参数添加');
        win.grid = grid;
        win.modal = true;
        win.show();
	},

	/**
     * 表单提交
     */
     formSubmit : function(formpanel, grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('b_parameterlist/save_ParameterList_qx_cslbbj.do', formpanel.form, '正在提交数据,请稍候.....',
			function(form, action){
			    formpanel.up('window').close();
			    grid.getStore().reload();
				Ext.MessageBox.alert('提示', '保存成功！');
			});
        }
     },

     /**
      * 删除
      */
     del : function(grid){
    	 var me = this;
 		 // 获取选中的行
 	     var data = grid.getSelectionModel().getSelection();
         if (data.length == 0) {
             Ext.MessageBox.alert('提示', '请先选中要删除的项目！');
         }else {
        	 Ext.MessageBox.show({
        		 title : '询问',
        		 msg : '您确定要删除吗?',
 			     width : 250,
 				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                     if (btn == 'yes') {
                    	 var dir = new Array();　
                         Ext.Array.each(data, function(items) {
                             var id = items.data.id;
                             dir.push(id);
             			});
             			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('b_parameterlist/del_ParameterList_qx_cslbsc.do', param,
                        function(response, options){
                        	grid.getStore().reload(); 
                        }, null, me.form);
                     }
                 }
             });
         }
 	},

 	/**
     * 修改
     */
	editor : function(grid , treepanel){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{

            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            
            var win = Ext.create('system.bcms.parameterlist.biz.ParameterListEdit');
            win.setTitle('参数修改');
            win.modal = true;
            win.grid = grid;
            win.treepanel =  treepanel;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'b_parameterlist/load_ParameterListByid.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},

 	/**
     * 查看
     */
	view : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if(data.length > 0){
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.bcms.parameterlist.biz.ParameterListEdit');
            win.setTitle('参数查看');
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'b_parameterlist/load_ParameterListByid.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},
	
	/**
     * 是否启用停用
     */
	isEnabled : function(grid, isQT){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
		        msg : '您确定要修改参数状态吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
					    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
			            });
             			var pram = {ids : dir, isenabled : isQT};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('b_parameterlist/enabled_ParameterList_qx_cslbbj.do', param,
                        function(response, options){
                        	grid.getStore().reload(); 
                        });
                    }
                }
            });
        }
	},
	
    /**
     * 导出
     */
	download : function(){
  		var me = this;
    	Ext.MessageBox.show({
   		title : '询问',
   		msg : '您确定要导出吗?',
			width : 250,
			buttons : Ext.MessageBox.YESNO,
            icon : Ext.MessageBox.QUESTION,
            fn : function(btn) {
            	if (btn == 'yes') {
            		me.util.ExtAjaxRequest('b_parameterlist/download.do' , '' ,
            		function(response, options){
            			var respText = Ext.JSON.decode(response.responseText);
                   	window.open(respText.data.httpUrl);
            		}, null, me.form);
            	}
            }
    	});
	}
});