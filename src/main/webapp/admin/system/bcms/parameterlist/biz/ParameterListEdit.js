﻿/**
 * 参数列表添加界面
 * @author lw
 */
Ext.define('system.bcms.parameterlist.biz.ParameterListEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.bcms.parameterlist.biz.ParameterListOper'),
	initComponent:function(){
	    var me = this;
	    
	    var parametertypeid = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属类型',
	    	labelAlign:'right',
	    	labelWidth:70,
	    	rootText : '参数类型',
	    	rootId : me.oper.util.getParams("company").id,
	    	rootVisible : false,//不显示根节点
	        width : 340,
	        name : 'parametertypeid',
	        baseUrl:'b_parametertype/load_AsyncParameterTypeTree.do',//访问路劲	        
	        allowBlank: false,
	        hiddenName : 'parametertypename'//隐藏域Name
	    });
	    var parametertypename = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属类型名称',
	    	name: 'parametertypename'
	    });
	    
	    var name = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '名称',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'name',
	    	fname : 'symbol',
	    	maxLength: 20,
	    	allowBlank: false
	    });
	    
	    var title = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '显示标题',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'title',
	    	maxLength: 50
	    });
	    
	    var belongsid = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '对应ID',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'belongsid',
	    	maxLength: 50
	    });
	    
	    var urlpath = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '链接地址',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'urlpath',
	    	maxLength: 200
	    });
	    
	    var imagepanel = Ext.create('system.widget.FsdTextFileManage', {
	        width : 340,
	    	zName : 'imagepath',
	    	zFieldLabel : '图片地址',
	    	zLabelWidth : 70,
	    	zIsShowButton : true, 
	    	zIsUpButton : true,
	    	zFileType : 'image',
	    	zFileAutoName : false,
	    	zIsReadOnly : false,
	    	zFileUpPath : me.oper.util.getSystemPath(),
	    	zManageType : 'upload'
	    });
	    
	    var parameter1 = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '扩展参数1',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'parameter1',
	    	maxLength: 50
	    });
	    
	    var parameter2 = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '扩展参数2',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'parameter2',
	    	maxLength: 50
	    });
	    
	    var parameter3 = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '扩展参数3',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'parameter3',
	    	maxLength: 50
	    });
	    
	    var sort = new Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '排序编号',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'sort',
	    	maxLength: 3,
	    	minValue: 0,
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank: false
	    });
	    
	    var remark = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'remark',
	    	maxLength: 50
	    });
	    
	    me.on('show' , function(){
	    	parametertypeid.setText(parametertypename.getValue());
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [{
                name: "id",
                xtype: "hidden"
            }, parametertypename, parametertypeid, name, 
            title, belongsid, urlpath, imagepanel, parameter1, parameter2, parameter3, sort, remark]
        });
	        
	    Ext.apply(this,{
	        width:400,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'cslbbj',
	        	name : 'btnsave',
			    text : '　保存　',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.formSubmit(form, me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});