﻿/**
 *b_parameterlist Model
 *@author CodeSystem
 *文件名     Extjs_Model_B_parameterlist
 *Model名    model_b_parameterlist
 */

Ext.define('model_b_parameterlist', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'parametertypeid',
            type : 'string'
        }
        ,
        {
            name : 'parametertypename',
            type : 'string'
        }
        ,
        {
            name : 'name',
            type : 'string'
        }
        ,
        {
            name : 'symbol',
            type : 'string'
        }
        ,
        {
            name : 'title',
            type : 'string'
        }
        ,
        {
            name : 'belongsid',
            type : 'string'
        }
        ,
        {
            name : 'urlpath',
            type : 'string'
        }
        ,
        {
            name : 'imagepath',
            type : 'string'
        }
        ,
        {
            name : 'parameter1',
            type : 'string'
        }
        ,
        {
            name : 'parameter2',
            type : 'string'
        }
        ,
        {
            name : 'parameter3',
            type : 'string'
        }
        ,
        {
            name : 'sort',
            type : 'int'
        }
        ,
        {
            name : 'status',
            type : 'string'
        }
        ,
        {
            name : 'statusname',
            type : 'string'
        }
        ,
        {
            name : 'remark',
            type : 'string'
        }
    ]
});
