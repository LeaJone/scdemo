﻿/**
 *b_parameterlist Column
 *@author CodeSystem
 *文件名     Extjs_Column_B_parameterlist
 *Columns名  column_b_parameterlist
 */

Ext.define('column_b_parameterlist', {
    columns: [
              {
                  xtype : 'rownumberer',
                  text : 'NO',
                  sortable : true,
                  width : 30
              }
              ,
        {
            header : '名称',
            dataIndex : 'name',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '所属类型',
            dataIndex : 'parametertypename',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '显示标题',
            dataIndex : 'title',
            sortable : true,
            flex : 1
        }
        ,
        {
        	header : '对应ID',
            dataIndex : 'belongsid',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '链接地址',
            dataIndex : 'urlpath',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '图片地址',
            dataIndex : 'imagepath',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '扩展参数1',
            dataIndex : 'parameter1',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '扩展参数2',
            dataIndex : 'parameter2',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '扩展参数3',
            dataIndex : 'parameter3',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '顺序号',
            dataIndex : 'sort',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '状态',
            dataIndex : 'statusname',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '备注',
            dataIndex : 'remark',
            sortable : true,
            flex : 1
        }
    ]
});
