﻿/**
 *b_parametertype Column
 *@author CodeSystem
 *文件名     Extjs_Column_B_parametertype
 *Columns名  column_b_parametertype
 */

Ext.define('column_b_parametertype', {
    columns: [
              {
                  xtype : 'rownumberer',
                  text : 'NO',
                  sortable : true,
                  width : 30
              }
              ,
        {
            header : '名称',
            dataIndex : 'name',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '助记符',
            dataIndex : 'symbol',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '所属类型',
            dataIndex : 'parentname',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '顺序号',
            dataIndex : 'sort',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '备注',
            dataIndex : 'remark',
            sortable : true,
            flex : 1
        }
    ]
});
