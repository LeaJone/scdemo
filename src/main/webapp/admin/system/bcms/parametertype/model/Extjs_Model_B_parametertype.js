﻿/**
 *b_parametertype Model
 *@author CodeSystem
 *文件名     Extjs_Model_B_parametertype
 *Model名    model_b_parametertype
 */

Ext.define('model_b_parametertype', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'parentid',
            type : 'string'
        }
        ,
        {
            name : 'parentname',
            type : 'string'
        }
        ,
        {
            name : 'name',
            type : 'string'
        }
        ,
        {
            name : 'symbol',
            type : 'string'
        }
        ,
        {
            name : 'sort',
            type : 'int'
        }
        ,
        {
            name : 'remark',
            type : 'string'
        }
    ]
});
