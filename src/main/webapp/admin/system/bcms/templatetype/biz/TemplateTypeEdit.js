/**
 * 专题添加界面
 */
Ext.define('system.bcms.templatetype.biz.TemplateTypeEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	treepanel : null,
	oper : Ext.create('system.bcms.templatetype.biz.TemplateTypeOper'),
	initComponent:function(){
	    var me = this;
	    var parentid = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属栏目专题',
	    	labelAlign:'right',
	    	labelWidth:90,
	    	rootText : '专题模板',
	    	rootId : me.oper.util.getParams("company").id,
	        width : 340,
	        name : 'parentid',
	        baseUrl:'b_templatetype/load_AsyncTemplateTypeTree.do',//访问路劲	        
	        allowBlank: false,
	        blankText: '请选择专题模板',
	        hiddenName : 'parentname'//隐藏域Name
	    });
	    var parentname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属模板名称',
	    	name: 'parentname'
	    });
	    
	    var code = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '专题编码',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 340,
	    	name: 'code',
	    	maxLength: 20,
	    	allowBlank: false
	    });
	    
	    var name = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '专题名称',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 340,
	    	name: 'name',
	    	maxLength: 20,
	    	allowBlank: false
	    });

	    var filepath = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '文件路径'
	    });
	    var typename = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '模板类型名称',
	    	name: 'typename'
	    });
	    var typecode = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'模板类型',
	        labelAlign:'right',
	    	labelWidth:90,
	        width : 260,
    	    name : 'typecode',//提交到后台的参数名
	        key : 'b_ztmblx',//参数
	        allowBlank: false,
	    	zHiddenObject : typename,//提交隐藏域对象
			zCallback : function(code, name) {
                switch (code) {
				case 'b_ztmb11':
					filepath.setValue('../resource/b_ztmb11.jpg');
					break;
				case 'b_ztmb12':
					filepath.setValue('../resource/b_ztmb12.jpg');
					break;
				case 'b_ztmb21':
					filepath.setValue('../resource/b_ztmb21.jpg');
					break;
				case 'b_ztmb22':
					filepath.setValue('../resource/b_ztmb22.jpg');
					break;
				}
            }
	    });  
	    var button = Ext.create('Ext.Button', {
	        text: '查看模板',
	        width : 80,
	        handler: function() {
	        	if(filepath.getValue() != ""){
	        		var manager = new FsdFileManager();
			        manager.setImgUrl = filepath.getValue();
			        manager.getImgviewDialog();
	        	}
	        }
	    });
	    var typecodebutton = Ext.create('Ext.container.Container',{
	        width : 340,
	        layout: "hbox",
	    	padding : '0 0 5 0',
        	items:[typecode, button]
	    });  
	    
	    var accessurl = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '访问地址',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 340,
	    	name: 'accessurl'
	    });
	    
	    var subjectid = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '对应栏目ID',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 340,
	    	name: 'subjectid',
	    	maxLength: 32,
	    	allowBlank: false
	    });
	    
	    var sort = new Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '排序编号',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 340,
	    	name: 'sort',
	    	maxLength: 3,
	    	minValue: 0,
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank: false
	    });
	    
	    var remark = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:90,
	        width : 340,
	    	name: 'remark',
	    	maxLength: 50
	    });
	    
	    var url1 = Ext.create('system.widget.FsdTextButton',{
	        zName: 'title',
	    	zFieldLabel : '访问地址',
	    	zLabelAlign : 'right',
	    	zLabelWidth : 90,
	        width : 340,
	        zIsButton1 : true,
	    	zButton1Text : '生成地址',
	    	zButton1Width : 60,
	    	zButton1Callback : function(obj){
	    		obj.zSetValue(str);
	    	}
	    });
	    
	    me.on('show' , function(){
	    	parentid.setText(parentname.getValue());
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [{
                name: "id",
                xtype: "hidden"
            }, parentname, parentid,typecodebutton,typename,code,name,accessurl,subjectid,sort,remark]
        });
	        
	    Ext.apply(this,{
	        width:420,
	        height:288,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'cslxbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.formSubmit(form, me.grid, me.treepanel);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});