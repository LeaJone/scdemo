/**
 *b_templatetype Model
 *@author CodeSystem
 *文件名     Extjs_Model_B_templatetype
 *Model名    model_b_templatetype
 */

Ext.define('model_b_templatetype', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'parentid',
            type : 'string'
        }
        ,
        {
            name : 'parentname',
            type : 'string'
        } ,
        {
            name : 'code',
            type : 'string'
        } ,
        {
            name : 'name',
            type : 'string'
        } ,
        {
            name : 'accessurl',
            type : 'string'
        } ,
        {
            name : 'sort',
            type : 'int'
        }
        ,
        {
            name : 'remark',
            type : 'string'
        }
    ]
});
