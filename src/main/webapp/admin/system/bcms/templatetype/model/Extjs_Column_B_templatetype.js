/**
 *b_templatetype Column
 *@author CodeSystem
 *文件名     Extjs_Column_B_templatetype
 *Columns名  column_b_templatetype
 */

Ext.define('column_b_templatetype', {
    columns: [
              {
                  xtype : 'rownumberer',
                  text : 'NO',
                  sortable : true,
                  width : 30
              }
              ,
        {
            header : '所属专题名称',
            dataIndex : 'parentname',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '专题编码',
            dataIndex : 'code',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '专题名称',
            dataIndex : 'name',
            sortable : true,
            flex : 1
        },
        {
            header : '访问地址',
            dataIndex : 'accessurl',
            sortable : true,
            flex : 1
        },
        {
            header : '顺序号',
            dataIndex : 'sort',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '备注',
            dataIndex : 'remark',
            sortable : true,
            flex : 1
        }
    ]
});
