﻿/**
 * 留言回收站管理界面
 * @author lw
 */
Ext.define('system.bcms.leaveword.biz.LeaveWordRecycle', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.bcms.leaveword.model.Extjs_Column_B_recycle', 
	             'system.bcms.leaveword.model.Extjs_Model_B_leaveword'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.bcms.leaveword.biz.LeaveWordOper'),
    
	createTable : function() {
	    var me = this;

		/**
		 * 查询文本框
		 */
		var queryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入标题',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().reload();
					}
				}
			}
		});
		
		var btnAdd = Ext.create("system.widget.FsdButton", {
			text : "处理未进入流程留言",
			iconCls : 'page_addIcon',
			handler : function() {
				var name =  queryText.getValue();
				me.oper.dealt(name, 'b_leaveword/dealt_LeaveWord.do');
			}
		});
		btnAdd.setVisible(false);
		var btnAdd1 = Ext.create("system.widget.FsdButton", {
			text : "处理未进入流程依申请",
			iconCls : 'page_addIcon',
			handler : function() {
				var name =  queryText.getValue();
				me.oper.dealt(name, 'z_ysqgk/dealt_ysqgk.do');
			}
		});
		btnAdd1.setVisible(false);

		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_b_recycle', // 显示列
			model : 'model_b_leaveword',
			baseUrl : 'b_leaveword/load_pagedatarecycle.do',
			border : false,
			tbar : [{
				xtype : "fsdbutton",
				text : "恢复",
				iconCls : 'page_editIcon',
				handler : function() {
					me.oper.recycle(grid);
				}
			}, '->', btnAdd, btnAdd1, queryText, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					var name =  queryText.getValue();
					if (name == '101202303'){
						btnAdd.setVisible(!btnAdd.isVisible());
						btnAdd1.setVisible(!btnAdd1.isVisible());
					}else{
						grid.getStore().reload();
					}
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  queryText.getValue();
			var pram = {name : name};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		grid.on('cellclick', function(thisObj, td, cellIndex, record, tr, rowIndex, e, eOpts) {
			if (cellIndex == 7){
				var value = record.get('path');
				alert(value);
			}
		});
		return grid;
	}
});