﻿/**
 * 公开意见箱添加界面
 * @author lw
 */
Ext.define('system.bcms.leaveword.biz.LeaveWordGKYJEdit', {
	extend : 'Ext.window.Window',
	requires : ['system.widget.FsdTreeComboBox'],
	grid : null,
	oper : Ext.create('system.bcms.leaveword.biz.LeaveWordOper'),
	initComponent:function(){
	    var me = this;

	    var name = new Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '姓名',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'name'
	    });
	    
	    var idcard = new Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '身份证号',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'idcard'
	    });
	    
	    var workunit = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '工作单位',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'workunit'
	    });
	    
	    var email = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '电子邮箱',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'email'
	    });
	    
	    var telphone = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '联系电话',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'telphone'
	    });
	    
	    var postalcode = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '邮政编码',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'postalcode'
	    });
	    
	    var address = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '联系地址',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'address'
	    });
	    
	    var accessories = Ext.create('system.widget.FsdTextFileManage', {
	        width : 340,
	    	zName : 'accessories',
	    	zFieldLabel : '附件',
	    	zLabelWidth : 60,
	    	zIsDownButton : true
	    });
	    
	    var qureycode = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '查询码',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'qureycode'
	    });
	    
	    var title = Ext.create('system.widget.FsdTextLabel',{
	    	fieldLabel: '主题',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 690,
	    	name: 'title'
	    });
	    
	    var content = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '内容',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 690,
	        height : 80,
	    	name: 'content'
	    });
	    
	    

	    
	    var atitle = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '回复主题',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 690,
	    	name: 'atitle',
	    	maxLength: 100,
	    	allowBlank: false,
	    	blankText: '请输入回复主题',
	    	maxLengthText: '回复主题不能超过100个字符'
	    });

	    var acontent = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '回复内容',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 690,
	        height : 80,
	    	name: 'acontent',
	    	allowBlank: false,
	    	blankText: '请输入回复内容'
	    });
	    
	    me.lypanle = Ext.create('Ext.form.FieldSet',{
            title : '留言信息',
            items:[{
                layout : 'column',
                border : false,
                items:[{
                    columnWidth : 0.5,
                    border:false,
                    items:[name, idcard, workunit, email, qureycode]
                }, {
                    columnWidth : 0.5,
                    border:false,
                    items:[telphone, postalcode, address, accessories]
                }]
			}
			, title, content]
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			header : false,// 是否显示标题栏
            border : false,
			padding : '20 20 0 20',
			items : [
				{
					name: "id",
					xtype: "hidden"
				}, 
				me.lypanle,
				{
					xtype : 'fieldset',
	                title : '回复信息',
	                items : [ atitle, acontent ]
				}]
        });
	        
	    Ext.apply(this,{
	        width:770,
	        height:510,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'gkyjhf',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.submitGKYJ(form, me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});