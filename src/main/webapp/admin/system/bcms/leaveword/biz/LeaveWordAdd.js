﻿/**
 * 角色添加界面
 * @author lw
 */
Ext.define('system.bcms.leaveword.biz.LeaveWordAdd', {
	extend : 'Ext.window.Window',
	requires : ['system.widget.FsdTreeComboBox'],
	grid : null,
	oper : Ext.create('system.bcms.leaveword.biz.LeaveWordOper'),
	initComponent:function(){
	    var me = this;

	    var type = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '类型',
	    	labelWidth:60,
	        width : 302,
	    	name: 'type',
	    	value : 'lybzx',
	    	maxLength: 100,
	    	allowBlank: false,
	    	blankText: '请输入文件名',
	    	maxLengthText: '文件名不能超过100个字符'
	    });
	    var typename = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '类型名称',
	    	labelWidth:60,
	        width : 302,
	    	name: 'typename',
	    	value : '在线咨询',
	    	maxLength: 100,
	    	allowBlank: false,
	    	blankText: '请输入文件名',
	    	maxLengthText: '文件名不能超过100个字符'
	    });
	    
	    var name = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '姓名',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 302,
	    	name: 'name',
	    	maxLength: 100,
	    	allowBlank: false,
	    	blankText: '请输入文件名',
	    	maxLengthText: '文件名不能超过100个字符'
	    });
	    
	    var idcard = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '身份证号',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 302,
	    	name: 'idcard',
	    	maxLength: 100,
	    	allowBlank: false,
	    	blankText: '请输入文件名',
	    	maxLengthText: '文件名不能超过100个字符'
	    });
	    
	    var workunit = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '工作单位',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 302,
	    	name: 'workunit',
	    	maxLength: 200,
	    	allowBlank: false,
	    	blankText: '请输入文件路径',
	    	maxLengthText: '文件路径不能超过200个字符'
	    });
	    
	    var email = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '电子邮箱',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 302,
	    	name: 'email',
	    	maxLength: 200,
	    	allowBlank: false,
	    	blankText: '请输入文件路径',
	    	maxLengthText: '文件路径不能超过200个字符'
	    });
	    
	    var telphone = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '联系电话',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 302,
	    	name: 'telphone',
	    	maxLength: 200,
	    	allowBlank: false,
	    	blankText: '请输入文件路径',
	    	maxLengthText: '文件路径不能超过200个字符'
	    });
	    
	    var postalcode = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '邮政编码',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 302,
	    	name: 'postalcode',
	    	maxLength: 200,
	    	allowBlank: false,
	    	blankText: '请输入文件路径',
	    	maxLengthText: '文件路径不能超过200个字符'
	    });
	    
	    var address = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '联系地址',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 302,
	    	name: 'address',
	    	maxLength: 200,
	    	allowBlank: false,
	    	blankText: '请输入文件路径',
	    	maxLengthText: '文件路径不能超过200个字符'
	    });
	    
	    var title = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '主题',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 302,
	    	name: 'title',
	    	maxLength: 200,
	    	allowBlank: false,
	    	blankText: '请输入文件路径',
	    	maxLengthText: '文件路径不能超过200个字符'
	    });
	    
	    var content = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '内容',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 302,
	    	name: 'content',
	    	maxLength: 200,
	    	allowBlank: false,
	    	blankText: '请输入文件路径',
	    	maxLengthText: '文件路径不能超过200个字符'
	    });
	    
	    var pathcol = Ext.create('system.widget.FsdTextFileManage', {
	        width : 302,
	    	zName : 'accessories',
	    	zFieldLabel : '附件',
	    	zLabelWidth : 60,
	    	zIsUpButton : true,
	    	zIsDownButton : true,
	    	zBlankText : '请选择输入附件路径',
	    	zFileType : 'file',
	    	zFileAutoName : false,
	    	zFileUpPath : me.oper.util.getFilePath(),
	    	zManageType : 'manage'
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [type, typename, name, idcard, workunit, email, 
			         telphone, postalcode, address, title, content, pathcol]
        });
	        
	    Ext.apply(this,{
	        width:400,
	        height:440,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : '',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.addSubmit(form, me.grid);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});