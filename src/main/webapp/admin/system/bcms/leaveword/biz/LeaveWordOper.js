/**
 * 留言板界面操作类
 * @author lw
 */
Ext.define('system.bcms.leaveword.biz.LeaveWordOper', {
	
	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 添加
     */
	add : function(grid){
	    var win = Ext.create('system.bcms.leaveword.biz.LeaveWordAdd');
        win.setTitle('留言添加');
        win.grid = grid;
        win.modal = true;
        win.show();
	},
	/**
     * 表单提交
     */
    addSubmit : function(formpanel, grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('b_leaveword/insert_LeaveWord.do', formpanel.form, '正在提交数据,请稍候.....',
			function(form, action){
			    grid.getStore().reload();
				Ext.MessageBox.alert('提示', '保存成功！');
			});
        }
    },
    

	submitPT : function(formpanel, grid){
		this.formSubmit(formpanel, grid, 'b_leaveword/save_LeaveWord_qx_ptlyhf.do');
	},
	submitZX : function(formpanel, grid){
		this.formSubmit(formpanel, grid, 'b_leaveword/save_LeaveWord_qx_zxzxhf.do');
	},
	submitLD : function(formpanel, grid){
		this.formSubmit(formpanel, grid, 'b_leaveword/save_LeaveWord_qx_ldxxhf.do');
	},
	submitXF : function(formpanel, grid){
		this.formSubmit(formpanel, grid, 'b_leaveword/save_LeaveWord_qx_xfxxhf.do');
	},
	submitJB : function(formpanel, grid){
		this.formSubmit(formpanel, grid, 'b_leaveword/save_LeaveWord_qx_jbxxhf.do');
	},
	submitJJ : function(formpanel, grid){
		this.formSubmit(formpanel, grid, 'b_leaveword/save_LeaveWord_qx_jjjchf.do');
	},
	submitGKYJ : function(formpanel, grid){
		this.formSubmit(formpanel, grid, 'b_leaveword/save_LeaveWord_qx_gkyjhf.do');
	},
	/**
     * 表单提交
     */
    formSubmit : function(formpanel , grid, url){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit(url, formpanel.form, '正在提交数据,请稍候.....',
			function(form, action){
			    formpanel.up('window').close();
			    grid.getStore().reload();
				Ext.MessageBox.alert('提示', '保存成功！');
			});
        }
    },
	
    editorPT : function(grid){
 		this.editor(grid, 'system.bcms.leaveword.biz.LeaveWordEdit');
 	},
 	editorZX : function(grid){
 		this.editor(grid, 'system.bcms.leaveword.biz.LeaveWordZXEdit');
 	},
 	editorLD : function(grid){
 		this.editor(grid, 'system.bcms.leaveword.biz.LeaveWordLDEdit');
 	},
 	editorXF : function(grid){
 		this.editor(grid, 'system.bcms.leaveword.biz.LeaveWordXFEdit');
 	},
 	editorJB : function(grid){
 		this.editor(grid, 'system.bcms.leaveword.biz.LeaveWordJBEdit');
 	},
 	editorJJ : function(grid){
 		this.editor(grid, 'system.bcms.leaveword.biz.LeaveWordJJEdit');
 	},
 	editorGKYJ : function(grid){
 		this.editor(grid, 'system.bcms.leaveword.biz.LeaveWordGKYJEdit');
 	},
	/**
     * 修改
     */
	editor : function(grid, url){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{
        	// 先得到主键
            var id = data[0].data.id;
            var win = Ext.create(url);
            win.setTitle('回复留言');
            win.grid = grid;
            win.modal = true;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'b_leaveword/load_LeaveWordByid.do' , param , null , 
	        function(response, options, respText, obj){
	            win.setTitle(obj.typename);
	            win.show();
	        });
        }
	},
 	
	viewPT : function(grid){
 		this.view(grid, 'system.bcms.leaveword.biz.LeaveWordEdit');
 	},
 	viewZX : function(grid){
 		this.view(grid, 'system.bcms.leaveword.biz.LeaveWordZXEdit');
 	},
 	viewLD : function(grid){
 		this.view(grid, 'system.bcms.leaveword.biz.LeaveWordLDEdit');
 	},
 	viewXF : function(grid){
 		this.view(grid, 'system.bcms.leaveword.biz.LeaveWordXFEdit');
 	},
 	viewJB : function(grid){
 		this.view(grid, 'system.bcms.leaveword.biz.LeaveWordJBEdit');
 	},
 	viewJJ : function(grid){
 		this.view(grid, 'system.bcms.leaveword.biz.LeaveWordJJEdit');
 	},
 	viewGKYJ : function(grid){
 		this.view(grid, 'system.bcms.leaveword.biz.LeaveWordGKYJEdit');
 	},
 	/**
     * 查看
     */
	view : function(grid, url){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if(data.length > 0){
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create(url);
            win.setTitle('信息查看');
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'b_leaveword/load_LeaveWordByid.do' , param , null , 
	        function(response, options, respText, obj){
	            win.setTitle(obj.typename);
	            win.show();
	        });
        }
	},
	
	/**
     * 删除
     */
	del : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要删除的项目！');
        }else {
        	Ext.MessageBox.show({
        		title : '询问',
        		msg : '您确定要删除吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                   	 var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
            			});
                        var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('b_leaveword/del_LeaveWord_qx_wdsc.do' , param ,
                        function(response, options){
	                       	grid.getStore().reload();
                        }, null, me.form);
                    }
                }
            });
        }
	},
	
	/**
     * 恢复
     */
	recycle : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要恢复的项目！');
        }else {
        	Ext.MessageBox.show({
        		title : '询问',
        		msg : '您确定要恢复吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                   	 var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
            			});
                        var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('b_leaveword/recycle_LeaveWord.do' , param ,
                        function(response, options){
	                       	grid.getStore().reload();
                        }, null, me.form);
                    }
                }
            });
        }
	},
	
	auditPT : function(grid, isQT){
 		this.audit(grid, isQT, 'b_leaveword/audit_LeaveWord_qx_ptlysh.do');
 	},
 	auditZX : function(grid, isQT){
 		this.audit(grid, isQT, 'b_leaveword/audit_LeaveWord_qx_zxzxsh.do');
 	},
 	auditLD : function(grid, isQT){
 		this.audit(grid, isQT, 'b_leaveword/audit_LeaveWord_qx_ldxxsh.do');
 	},
 	auditXF : function(grid, isQT){
 		this.audit(grid, isQT, 'b_leaveword/audit_LeaveWord_qx_xfxxsh.do');
 	},
 	auditJB : function(grid, isQT){
 		this.audit(grid, isQT, 'b_leaveword/audit_LeaveWord_qx_jbxxsh.do');
 	},
 	auditJJ : function(grid, isQT){
 		this.audit(grid, isQT, 'b_leaveword/audit_LeaveWord_qx_jjjcsh.do');
 	},
 	auditGKYJ : function(grid, isQT){
 		this.audit(grid, isQT, 'b_leaveword/audit_LeaveWord_qx_gkyjsh.do');
 	},
	/**
     * 是否审核
     */
 	audit : function(grid, isQT, url){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
		        msg : '您确定要修改信息状态吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
					    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
			            });
             			var pram = {ids : dir, status : isQT};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest(url, param,
                        function(response, options){
                        	grid.getStore().reload(); 
                        });
                    }
                }
            });
        }
	},
 	
	/**
     * 处理未进入流程的信箱数据
     */
 	dealt : function(id, url){
		var me = this;
		var pram = {id : id};
        var param = {jsonData : Ext.encode(pram)};
		me.util.ExtAjaxRequest(url, param,
        function(response, options){
			Ext.MessageBox.alert('提示', '处理成功！');
        }, null, me.form);
	}
});