﻿/**
 * 留言管理界面
 * @author lw
 */
Ext.define('system.bcms.leaveword.biz.LeaveWordGKYJManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*', 
	             'system.widget.FsdButton',
	             'system.bcms.leaveword.model.Extjs_Column_B_leaveword', 
	             'system.bcms.leaveword.model.Extjs_Model_B_leaveword'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.bcms.leaveword.biz.LeaveWordOper'),
    
	createTable : function() {
	    var me = this;
		
		var queryid = 'lybgkyj';//举报信箱

		/**
		 * 查询文本框
		 */
		var queryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入标题',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});

		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_b_leaveword', // 显示列
			model : 'model_b_leaveword',
			baseUrl : 'b_leaveword/load_pagedata.do',
			border : false,
			tbar : [{
				xtype : "fsdbutton",
				popedomCode : 'gkyjhf',
				text : "回复",
				iconCls : 'page_editIcon',
				handler : function() {
					me.oper.editorGKYJ(grid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.viewGKYJ(grid);
				}
			}, '-', {
				xtype : "fsdbutton",
				popedomCode : 'gkyjsh',
				text : "审核通过",
				iconCls : 'page_deleteIcon',
				handler : function(button) {
				    me.oper.auditGKYJ(grid, true);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'gkyjsh',
				text : "审核折回",
				iconCls : 'page_deleteIcon',
				handler : function(button) {
				    me.oper.auditGKYJ(grid, false);
				}
			}, '->', queryText, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  queryText.getValue();
			var pram = {fid : queryid, name : name};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		grid.on('cellclick', function(thisObj, td, cellIndex, record, tr, rowIndex, e, eOpts) {
			if (cellIndex == 7){
				var value = record.get('path');
				alert(value);
			}
		});
		return grid;
	}
});