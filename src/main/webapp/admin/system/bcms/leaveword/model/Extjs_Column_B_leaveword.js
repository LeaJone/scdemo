﻿/**
 *b_leaveword Column
 *@author CodeSystem
 *文件名     Extjs_Column_B_leaveword
 *Columns名  column_b_leaveword
 */

Ext.define('column_b_leaveword', {
    columns: [
              {
                  xtype : 'rownumberer',
                  text : 'NO',
                  sortable : true,
                  width : 30
              }
              ,
        {
            header : '姓名',
            dataIndex : 'name',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '身份证号',
            dataIndex : 'idcard',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '标题',
            dataIndex : 'title',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '附件',
            dataIndex : 'accessories',
            sortable : true,
            flex : 1,
			renderer :function(value ,metaData ,record ){
				if(value != ''){
					//metaData.style='color:blue;text-decoration:underline;cursor:pointer;';下划线
					value="<font style='color:blue;'>有附件</font>";
				}
				return value;
			}
        }
        ,
        {
            header : '提交时间',
            dataIndex : 'submitdate',
            sortable : true,
            flex : 1,
			renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value, false);
			}
        }
        ,
        {
            header : '审核',
            dataIndex : 'auditing',
            sortable : true,
            flex : 1,
			renderer :function(value ,metaData ,record ){
				if(value == 'true'){
					//metaData.style='color:blue;text-decoration:underline;cursor:pointer;';下划线
					value="<font style='color:blue;'>已审核</font>";
				}else{
					value="<font style='color:red;'>未审核</font>";
				}
				return value;
			}
        }
        ,
        {
            header : '是否回复',
            dataIndex : 'isreply',
            sortable : true,
            flex : 1,
			renderer :function(value ,metaData ,record ){
				if(value == 'true'){
					//metaData.style='color:blue;text-decoration:underline;cursor:pointer;';下划线
					value="<font style='color:blue;'>已回复</font>";
				}else{
					value="<font style='color:red;'>未回复</font>";
				}
				return value;
			}
        }
        ,
        {
            header : '回复时间',
            dataIndex : 'adddate',
            sortable : true,
            flex : 1,
			renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value, false);
			}
        }
        ,
        {
            header : '回复部门',
            dataIndex : 'abranchname',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '回复人',
            dataIndex : 'addemployeename',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '流程',
            dataIndex : 'flowstatusname',
            sortable : true,
            flex : 1
        }
    ]
});
