/**
 * 文本文章操作类
 * @author lw
 */

Ext.define('system.bcms.article.biz.ArticleOper', {

	form : null,
	util : Ext.create('system.util.util'),
	contentUrl : null,

	/**
	 * 拷贝内容，过滤样式
	 */
	copyText : function(form, content, mcontent){
	    var me = this;
		me.util.ExtAjaxRequest('B_Article/get_ArticleMContent.do', 
				{jsonData : Ext.encode({content : content.getValue()})},
			    function(response, options, respText){
			    	if(respText.data != null){
			    		mcontent.setValue(respText.data);
			    	}
			    }, null, form);
	},
	/**
	 * 拷贝内容，过滤样式
	 */
	copyTextYB : function(form, content, mcontent){
	    var me = this;
		me.util.ExtAjaxRequest('B_Article/get_ArticleMContentYB.do', 
				{jsonData : Ext.encode({content : content.getValue()})},
			    function(response, options, respText){
			    	if(respText.data != null){
			    		mcontent.setValue(respText.data);
			    	}
			    }, null, form);
	},

	/**
	 * 加载栏目
	 */
	LoadSubjectTree : function(treepanel){
	    var me = this;
	    treepanel.getRootNode().removeAll();
	    me.util.ExtAjaxRequest('B_Subject/get_QxSubjectCheck.do', {jsonData : Ext.encode({id : system.UtilStatic.subjectZTCode})},
	    function(response, options, respText){
	    	if(respText.data != ""){
	    		treepanel.getRootNode().appendChild(respText.data);
	            treepanel.expandAll();
	            treepanel.checkChosseValues();
	    	}
	    }, null, me.form);
	},
	/**
	 * 加载相关栏目
	 */
	LoadSubjectXGTree : function(treepanel){
	    var me = this;
	    treepanel.getRootNode().removeAll();
	    me.util.ExtAjaxRequest('B_Subject/get_QxSubjectCheck.do', {jsonData : Ext.encode({id : system.UtilStatic.subjectXGCode})},
	    function(response, options, respText){
	    	if(respText.data != ""){
	    		treepanel.getRootNode().appendChild(respText.data);
	            treepanel.expandAll();
	            treepanel.checkChosseValues();
	    	}
	    }, null, me.form);
	},
	/**
	 * 加载Wap栏目
	 */
	LoadSubjectWapTree : function(treepanel){
	    var me = this;
	    treepanel.getRootNode().removeAll();
	    me.util.ExtAjaxRequest('B_Subject/get_QxSubjectCheck.do', {jsonData : Ext.encode({id : system.UtilStatic.subjectWapCode})},
	    function(response, options, respText){
	    	if(respText.data != ""){
	    		treepanel.getRootNode().appendChild(respText.data);
	            treepanel.expandAll();
	            treepanel.checkChosseValues();
	    	}
	    }, null, me.form);
	},
	/**
	 * 加载Wap相关栏目
	 */
	LoadSubjectWapXGTree : function(treepanel){
	    var me = this;
	    treepanel.getRootNode().removeAll();
	    me.util.ExtAjaxRequest('B_Subject/get_QxSubjectCheck.do', {jsonData : Ext.encode({id : system.UtilStatic.subjectWapXGCode})},
	    function(response, options, respText){
	    	if(respText.data != ""){
	    		treepanel.getRootNode().appendChild(respText.data);
	            treepanel.expandAll();
	            treepanel.checkChosseValues();
	    	}
	    }, null, me.form);
	},
	
	/**
     * 高级查询
     */
	query : function(winForm, grid){
	    var win = Ext.create('system.bcms.article.biz.ArticleQuery');
        win.setTitle('文章高级查询');
        win.form = winForm;
        win.grid = grid;
        win.modal = true;
        win.show();
	},
	
	/**
     * 添加文本文章
     */
	addText : function(grid, isNew){
		var me = this;
		var model = Ext.create("system.bcms.article.biz.TextArticleEdit");
		model.setControl(grid);
		var tabID = 'nrwzbj';
		if (isNew && typeof(isNew)!="undefined"){
			var myDate = new Date();
			tabID += myDate.getTime();
		}
		me.util.addTab(tabID, "添加文章", model, "resources/admin/icon/vcard_add.png");
	},
	/**
     * 添加组图文章
     */
	addImage : function(grid, isNew){
		var me = this;
		var model = Ext.create("system.bcms.article.biz.ImageArticleEdit");
		model.setControl(grid);
		var tabID = 'nrztbj';
		if (isNew && typeof(isNew)!="undefined"){
			var myDate = new Date();
			tabID += myDate.getTime();
		}
		me.util.addTab(tabID, "添加组图", model, "resources/admin/icon/photo.png");
	},
	/**
     * 添加视频
     */
	addVideo : function(grid, isNew){
		var me = this;
		var model = Ext.create("system.bcms.article.biz.VideoArticleEdit");
		model.setControl(grid);
		var tabID = 'nrspbj';
		if (isNew && typeof(isNew)!="undefined"){
			var myDate = new Date();
			tabID += myDate.getTime();
		}
		me.util.addTab(tabID, "添加视频", model, "resources/admin/icon/film.png");
	},
	/**
     * 添加下载文件
     */
	addFile : function(grid, isNew){
		var me = this;
		var model = Ext.create("system.bcms.article.biz.FileArticleEdit");
		model.setControl(grid);
		var tabID = 'nrxzbj';
		if (isNew && typeof(isNew)!="undefined"){
			var myDate = new Date();
			tabID += myDate.getTime();
		}
		me.util.addTab(tabID, "添加下载文件", model, "resources/admin/icon/drive_web.png");
	},
	
	/**
     * 表单提交
     */
     submitArticle : function(formpanel, grid, treesubject, treesubjectxg, formWin){
        var me = this;
        if (formpanel.form.form.isValid()) {
        	var checkedNodes = treesubject.getChecked();
    		var sids = [];
    		for(var i=0 ; i < checkedNodes.length ; i++){
    			sids.push(checkedNodes[i].internalId)
    		}
    		checkedNodes = treesubjectxg.getChecked();
    		var sxgids = [];
    		for(var i=0 ; i < checkedNodes.length ; i++){
    			sxgids.push(checkedNodes[i].internalId)
    		}
    		var idMap = {};
    		idMap[system.UtilStatic.articleRelateLM] = sids;
    		idMap[system.UtilStatic.articleRelateXG] = sxgids;
            me.util.ExtFormSubmit('B_Article/save_Article.do' , formpanel.form.form , '正在提交数据,请稍候.....',
			function(form, action, respText){
            	if(grid != null){
            		grid.getStore().reload();//刷新管理界面的表格
            	}
            	// formpanel.idNo.setValue(action.result.id);
            	// formWin.loadObjectID(action.result.id);
				Ext.MessageBox.alert('提示', '保存成功！');
			},
			function (form, action){
				Ext.MessageBox.alert('异常', action.result.msg);
			},
			{jsonData : Ext.encode(idMap)});
        }
     },
 	
 	/**
      * Wap表单提交
      */
      submitArticleWap : function(formpanel, grid, treesubjectwap, treesubjectwapxg){
         var me = this;
         if (formpanel.form.isValid()) {
         	var checkedNodes = treesubjectwap.getChecked();
     		var sids = [];
     		for(var i=0 ; i < checkedNodes.length ; i++){
     			sids.push(checkedNodes[i].internalId)
     		}
    		checkedNodes = treesubjectwapxg.getChecked();
    		var sxgids = [];
    		for(var i=0 ; i < checkedNodes.length ; i++){
    			sxgids.push(checkedNodes[i].internalId)
    		}
     		var idMap = {};
     		idMap[system.UtilStatic.articleRelateWAP] = sids;
     		idMap[system.UtilStatic.articleRelateWAPXG] = sxgids;
             me.util.ExtFormSubmit('B_Article/save_WapArticle.do' , formpanel.form , '正在提交数据,请稍候.....',
 			function(form, action){
             	if(grid != null){
             		grid.getStore().reload();//刷新管理界面的表格
             	}
				Ext.MessageBox.alert('提示', '保存成功！');
 			},
 			function (form, action){
 				Ext.MessageBox.alert('异常', action.result.msg);
 			},
 			{jsonData : Ext.encode(idMap)});
         }
      },
     
     /**
      * 删除
      */
     del : function(grid){
    	 var me = this;
 		 // 获取选中的行
 	     var data = grid.getSelectionModel().getSelection();
         if (data.length == 0) {
             Ext.MessageBox.alert('提示', '请先选中要删除的记录！');
         }else {
        	 Ext.MessageBox.show({
        		 title : '询问',
        		 msg : '您确定要删除吗?',
 			     width : 250,
 				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                     if (btn == 'yes') {
                    	 var dir = new Array();　
                         Ext.Array.each(data, function(items) {
                             var id = items.data.id;
                             dir.push(id);
             			});
             			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('B_Article/del_Article_qx_wzsc.do' , param ,
                        function(response, options){
                        	grid.getStore().reload();
                        }, null, me.form);
                     }
                 }
             });
         }

 	},
 	
 	/**
     * 修改
     */
	editor : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的记录！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个记录！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            var type = data[0].data.type;
            switch (type) {
    		case system.UtilStatic.articleTypeWZ :
    			me.editorText(grid, id);
    			break;
    		case system.UtilStatic.articleTypeZT :
    			me.editorImage(grid, id);
    			break;
    		case system.UtilStatic.articleTypeSP :
    			me.editorVideo(grid, id);
    			break;
    		case system.UtilStatic.articleTypeWJ :
    			me.editorFile(grid, id);
    			break;
    		}
        }
	},
 	/**
     * 修改
     */
	editorText : function(grid, id){
		var me = this;
		var model = me.util.getTab("editnrwzbj");
		if (model == null){
			model = Ext.create("system.bcms.article.biz.TextArticleEdit");
			model.setControl(grid);
		}
        var pram = {id : id};
        var param = {jsonData : Ext.encode(pram)};
        me.util.formLoad(model.formArticle.form, 'B_Article/load_ArticleByid.do', param, null, 
        function(response, options, respText, data){
        	model.formArticleWap.loadObject(data.data, data[system.UtilStatic.articleRelateWAP], data[system.UtilStatic.articleRelateWAPXG]);
        	model.formArticle.loadObject(data[system.UtilStatic.articleRelateLM], data[system.UtilStatic.articleRelateXG]);
        	model.loadObject(data.data);
        	me.util.addTab("editnrwzbj" , "修改文章" , model , "resources/admin/icon/vcard_edit.png");
        });
	},
 	/**
     * 修改
     */
	editorImage : function(grid, id){
		var me = this;
		var model = me.util.getTab("editnrztbj");
		if (model == null){
			model = Ext.create("system.bcms.article.biz.ImageArticleEdit");
			model.setControl(grid);
		}
        var pram = {id : id};
        var param = {jsonData : Ext.encode(pram)};
        me.util.formLoad(model.formArticle.form, 'B_Article/load_ArticleByid.do', param, null, 
        function(response, options, respText, data){
        	model.formArticleWap.loadObject(data.data, data[system.UtilStatic.articleRelateWAP], data[system.UtilStatic.articleRelateWAPXG]);
        	model.formArticle.loadObject(data[system.UtilStatic.articleRelateLM], data[system.UtilStatic.articleRelateXG]);
        	model.loadObject(data.data);
        	me.util.addTab("editnrztbj" , "修改组图" , model , "resources/admin/icon/photo.png");
        });
	},
 	/**
     * 修改
     */
	editorVideo : function(grid, id){
		var me = this;
		var model = me.util.getTab("editnrspbj");
		if (model == null){
			model = Ext.create("system.bcms.article.biz.VideoArticleEdit");
			model.setControl(grid);
		}
        var pram = {id : id};
        var param = {jsonData : Ext.encode(pram)};
        me.util.formLoad(model.formArticle.form, 'B_Article/load_ArticleByid.do', param, null, 
        function(response, options, respText, data){
        	model.formArticleWap.loadObject(data.data, data[system.UtilStatic.articleRelateWAP], data[system.UtilStatic.articleRelateWAPXG]);
        	model.formArticle.loadObject(data[system.UtilStatic.articleRelateLM], data[system.UtilStatic.articleRelateXG]);
        	model.loadObject(data.data);
        	me.util.addTab("editnrspbj" , "修改视频" , model , "resources/admin/icon/film.png");
        });
	},
 	/**
     * 修改
     */
	editorFile : function(grid, id){
		var me = this;
		var model = me.util.getTab("editnrxzbj");
		if (model == null){
			model = Ext.create("system.bcms.article.biz.FileArticleEdit");
			model.setControl(grid);
		}
        var pram = {id : id};
        var param = {jsonData : Ext.encode(pram)};
        me.util.formLoad(model.formArticle.form, 'B_Article/load_ArticleByid.do', param, null, 
        function(response, options, respText, data){
        	model.formArticleWap.loadObject(data.data, data[system.UtilStatic.articleRelateWAP], data[system.UtilStatic.articleRelateWAPXG]);
        	model.formArticle.loadObject(data[system.UtilStatic.articleRelateLM], data[system.UtilStatic.articleRelateXG]);
        	model.loadObject(data.data);
        	me.util.addTab("editnrxzbj" , "修改下载" , model , "resources/admin/icon/drive_web.png");
        });
	},
 	
 	/**
     * 查看
     */
	view : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要查看的记录！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个记录！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            var type = data[0].data.type;
            switch (type) {
    		case system.UtilStatic.articleTypeWZ :
    			me.viewText(id);
    			break;
    		case system.UtilStatic.articleTypeZT :
    			me.viewImage(id);
    			break;
    		case system.UtilStatic.articleTypeSP :
    			me.viewVideo(id);
    			break;
    		case system.UtilStatic.articleTypeWJ :
    			me.viewFile(id);
    			break;
    		}
        }
	},
 	/**
     * 查看
     */
	viewText : function(id){
		var me = this;
        var model = Ext.create("system.bcms.article.biz.TextArticleEdit");
        model.viewObject();
        var pram = {id : id};
        var param = {jsonData : Ext.encode(pram)};
        me.util.formLoad(model.formArticle.form, 'B_Article/load_ArticleByid.do', param, null, 
        function(response, options, respText, data){
        	model.formArticleWap.loadObject(data.data, data[system.UtilStatic.articleRelateWAP], data[system.UtilStatic.articleRelateWAPXG]);
        	model.formArticle.loadObject(data[system.UtilStatic.articleRelateLM], data[system.UtilStatic.articleRelateXG]);
        	model.loadObject(data.data);
            me.util.addTab(id + "view", "查看文章", model, "resources/admin/icon/vcard_edit.png");
        });
	},
 	/**
     * 查看
     */
	viewImage : function(id){
		var me = this;
		var model = Ext.create("system.bcms.article.biz.ImageArticleEdit");
        model.viewObject();
        var pram = {id : id};
        var param = {jsonData : Ext.encode(pram)};
        me.util.formLoad(model.formArticle.form, 'B_Article/load_ArticleByid.do', param, null, 
        function(response, options, respText, data){
        	model.formArticleWap.loadObject(data.data, data[system.UtilStatic.articleRelateWAP], data[system.UtilStatic.articleRelateWAPXG]);
        	model.formArticle.loadObject(data[system.UtilStatic.articleRelateLM], data[system.UtilStatic.articleRelateXG]);
        	model.loadObject(data.data);
        	me.util.addTab(id + "view", "查看组图" , model , "resources/admin/icon/photo.png");
        });
	},
 	/**
     * 查看
     */
	viewVideo : function(grid, id){
		var me = this;
		var model = Ext.create("system.bcms.article.biz.VideoArticleEdit");
        model.viewObject();
        var pram = {id : id};
        var param = {jsonData : Ext.encode(pram)};
        me.util.formLoad(model.formArticle.form, 'B_Article/load_ArticleByid.do', param, null, 
        function(response, options, respText, data){
        	model.formArticleWap.loadObject(data.data, data[system.UtilStatic.articleRelateWAP], data[system.UtilStatic.articleRelateWAPXG]);
        	model.formArticle.loadObject(data[system.UtilStatic.articleRelateLM], data[system.UtilStatic.articleRelateXG]);
        	model.loadObject(data.data);
        	me.util.addTab(id + "view", "查看视频" , model , "resources/admin/icon/film.png");
        });
	},
 	/**
     * 查看
     */
	viewFile : function(grid, id){
		var me = this;
		var model = Ext.create("system.bcms.article.biz.FileArticleEdit");
        model.viewObject();
        var pram = {id : id};
        var param = {jsonData : Ext.encode(pram)};
        me.util.formLoad(model.formArticle.form, 'B_Article/load_ArticleByid.do', param, null, 
        function(response, options, respText, data){
        	model.formArticleWap.loadObject(data.data, data[system.UtilStatic.articleRelateWAP], data[system.UtilStatic.articleRelateWAPXG]);
        	model.formArticle.loadObject(data[system.UtilStatic.articleRelateLM], data[system.UtilStatic.articleRelateXG]);
        	model.loadObject(data.data);
        	me.util.addTab(id + "view", "查看下载" , model , "resources/admin/icon/drive_web.png");
        });
	},
 	
 	/**
     * 审核
     */
 	auditing : function(grid , status){
 		var me = this;
		 // 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要审核的记录！');
        }else {
       	 Ext.MessageBox.show({
       		 title : '询问',
       		 msg : '您确定要执行该操作吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes'){
	                   	var dir = new Array();　
	                    Ext.Array.each(data, function(items) {
	                            var id = items.data.id;
	                            dir.push(id);
	            		});
	                    var pram = {ids : dir , status : status};
	                    var param = {jsonData : Ext.encode(pram)};
	                    me.util.ExtAjaxRequest('B_Article/audit_Article_qx_wzsh.do' , param ,
	                    function(response, options){
	                    	grid.getStore().reload();
	                    });
                    }
                }
            });
        }
	},
 	
 	/**
     * 置顶
     */
	firstly : function(grid , status){
 		var me = this;
		 // 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的记录！');
        }else {
       	 Ext.MessageBox.show({
       		 title : '询问',
       		 msg : '您确定要执行该操作吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes'){
	                   	var dir = new Array();　
	                    Ext.Array.each(data, function(items) {
	                            var id = items.data.id;
	                            dir.push(id);
	            		});
	                    var pram = {ids : dir , status : status};
	                    var param = {jsonData : Ext.encode(pram)};
	                    me.util.ExtAjaxRequest('B_Article/firstly_Article_qx_wzbj.do' , param ,
	                    function(response, options){
	                    	grid.getStore().reload();
	                    });
                    }
                }
            });
        }
	},
 	
 	/**
     * 幻灯
     */
	slide : function(grid , status){
 		var me = this;
		 // 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的记录！');
        }else {
       	 Ext.MessageBox.show({
       		 title : '询问',
       		 msg : '您确定要执行该操作吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes'){
	                   	var dir = new Array();　
	                    Ext.Array.each(data, function(items) {
	                            var id = items.data.id;
	                            dir.push(id);
	            		});
	                    var pram = {ids : dir , status : status};
	                    var param = {jsonData : Ext.encode(pram)};
	                    me.util.ExtAjaxRequest('B_Article/slide_Article_qx_wzbj.do' , param ,
	                    function(response, options){
	                    	grid.getStore().reload();
	                    });
                    }
                }
            });
        }
	},

    /**
     * 推荐
     */
    recommend : function(grid , status){
        var me = this;
        // 获取选中的行
        var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的记录！');
        }else {
            Ext.MessageBox.show({
                title : '询问',
                msg : '您确定要执行该操作吗?',
                width : 250,
                buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes'){
                        var dir = new Array();
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
                        });
                        var pram = {ids : dir , status : status};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('B_Article/recommend_Article_qx_wzbj.do' , param ,
                            function(response, options){
                                grid.getStore().reload();
                            });
                    }
                }
            });
        }
    },
 	
 	/**
     * 更改文章类型
     */
	updateType : function(grid){
 		var me = this;
		 // 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要更改的记录！');
        }else {
        	var win = Ext.create('system.bcms.article.biz.ArticleUpdateType');
            win.modal = true;
            win.updateFun = function(id, text){
            	var dir = new Array();　
                Ext.Array.each(data, function(items) {
                        var id = items.data.id;
                        dir.push(id);
        		});
                var pram = {ids : dir, id : id, text : text};
                var param = {jsonData : Ext.encode(pram)};
                me.util.ExtAjaxRequest('B_Article/update_ArticleType_qx_wzbj.do' , param ,
                function(response, options){
                	grid.getStore().reload();
                });
            };
            win.show();
        }
	},
 	
 	/**
     * 更改文章栏目
     */
	updateSubject : function(grid){
 		var me = this;
		 // 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要更改的记录！');
        }else {
        	var win = Ext.create('system.bcms.article.biz.ArticleUpdateSubject');
            win.modal = true;
            win.updateFun = function(id, text){
            	var dir = new Array();　
                Ext.Array.each(data, function(items) {
                        var id = items.data.id;
                        dir.push(id);
        		});
                var pram = {ids : dir, id : id, text : text};
                var param = {jsonData : Ext.encode(pram)};
                me.util.ExtAjaxRequest('B_Article/update_ArticleSubject_qx_wzbj.do' , param ,
                function(response, options){
                	grid.getStore().reload();
                });
            };
            win.show();
        }
	},
	
	/**
     * 文章推送
     */
	pushArticle : function(grid){
 		var me = this;
		 // 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要推送的文章！');
        }else {
        	var win = Ext.create('system.bcms.article.biz.ArticlePushArticle');
            win.modal = true;
            win.updateFun = function(id){
            	var dir = new Array();　
                Ext.Array.each(data, function(items) {
                	var id = items.data.id;
                    dir.push(id);
        		});
                var pram = {ids : dir, id : id};
                var param = {jsonData : Ext.encode(pram)};
                me.util.ExtAjaxRequest('e_userinfo/push_articleinfo_userinfo.do', param ,
                function(response, options){
                	Ext.MessageBox.alert('提示', '文章推送成功！');
                });
            };
            win.show();
        }
	},
 	
 	/**
     * 栏目文章迁移
     */
	updateRemove : function(grid){
 		var me = this;
		var win = Ext.create('system.bcms.article.biz.ArticleUpdateRemove');
        win.modal = true;
        win.updateFun = function(idOld, idNew, txtNew){
            var pram = {oid : idOld, nid : idNew, ntxt : txtNew};
            var param = {jsonData : Ext.encode(pram)};
            me.util.ExtAjaxRequest('B_Subject/gllmqy_Subject_qx_lmbj.do' , param ,
            function(response, options){
                Ext.MessageBox.alert('提示', '栏目文章迁移成功！');
            	grid.getStore().reload();
            });
        };
        win.show();
	},
 	
 	/**
     * 预览
     */
	preview : function(id){
		var me = this;
		if (me.contentUrl == null || me.contentUrl == ''){
			me.util.ExtAjaxRequest('Sys_SystemParameters/get_systemarticleurl.do', null,
	        function(response, options, respText){
				me.contentUrl = respText.data;
				if (me.contentUrl != null && me.contentUrl != ''){
					window.open(me.contentUrl.replace('XXXXXX', id));
				}
	        });
		}else{
			window.open(me.contentUrl.replace('XXXXXX', id));
		}
	}
});