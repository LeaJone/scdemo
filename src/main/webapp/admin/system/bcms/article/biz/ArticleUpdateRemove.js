﻿/**
 * 栏目文章迁移
 * @author lw
 */
Ext.define('system.bcms.article.biz.ArticleUpdateRemove', {
	extend : 'Ext.window.Window',
	updateFun : null,
    title : '栏目文章迁移',
	util : Ext.create('system.util.util'),

	initComponent:function(){
	    var me = this;

	    var subjectOld = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '迁移栏目',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 300,
	    	rootText : '主栏目',
	    	rootId : 'FSDMAIN',
			rootVisible: false,
	        name : 'subjectid',
	        baseUrl:'B_Subject/load_AsyncSubjectTreeByPopdom.do',//访问路劲
	        allowBlank: false,
	        zCallback : function (value) {
	            var pram = {id : value.id};
	            var param = {jsonData : Ext.encode(pram)};
	            me.util.ExtAjaxRequest('B_Subject/gllmcx_Subject.do' , param ,
                function(response, options, respText){
              	  tipLabel.setText("栏目所属文章" + respText.article + "篇，栏目关联文章" + respText.relate + "篇");
                });
      	    }
	    });

		var tipLabel = Ext.create('Ext.form.Label',{
	    	text: '统计：',
	    	width: 300,
	    	style:{color:'red'}
	    });
		
	    var subjectNew = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '目标栏目',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 300,
			margin : '5 0 0 0',
	    	rootText : '主栏目',
	    	rootId : 'FSDMAIN',
			rootVisible: false,
	        name : 'subjectid',
	        baseUrl:'B_Subject/load_AsyncSubjectTreeByPopdom.do',//访问路劲
	        allowBlank: false
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [subjectOld, tipLabel, subjectNew]
        });
	        
	    Ext.apply(this,{
	        width:360,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '迁移',
			    iconCls : 'acceptIcon',
			    handler : function() {
			    	if (form.form.isValid()) {
			    		if (subjectOld.getValue() == subjectNew.getValue()){
			                Ext.MessageBox.alert('提示', '迁移栏目与目标栏目选择不能相同！');
			    			return;
			    		}
			    		var msg = '将栏目 “' + subjectOld.getRawValue() + 
			    		'” 下的所有文章及关联文章全部迁移至栏目 “' + 
			    		subjectNew.getRawValue() + '” 下。';
						Ext.MessageBox.show({
							 title : '询问',
							 msg : '您确定要执行栏目文章迁移操作吗？<br/>' + msg,
							 width : 350,
							 buttons : Ext.MessageBox.YESNO,
							 icon : Ext.MessageBox.QUESTION,
							 fn : function(btn) {
								 if (btn == 'yes'){
				    				if (me.updateFun != null){
				    					me.updateFun(subjectOld.getValue(), 
				    							subjectNew.getValue(), subjectNew.getRawValue());
				    				    me.close();
				    				}
								 }
							 }
						});
			    	}
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});