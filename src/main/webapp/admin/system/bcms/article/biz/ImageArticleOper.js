/**
 * 组图文章操作类
 * @author lw
 */

Ext.define('system.bcms.article.biz.ImageArticleOper', {

	form : null,
	util : Ext.create('system.util.util'),

	/**
     * 添加
     */
	add : function(grid, articleid){
	    var win = Ext.create('system.bcms.article.biz.ImageArticleItemEdit');
        win.setTitle('添加组图');
        win.setArticleID(articleid);
        win.grid = grid;
        win.modal = true;
        win.show();
	},
	
	/**
     * 表单提交
     */
     formSubmit : function(win, grid){
        var me = this;
        if (win.form.form.isValid()) {
            me.util.ExtFormSubmit('b_articleimage/save_articleimage_qx_nrztbj.do' , win.form.form , '正在提交数据,请稍候.....',
			function(form, action){
            	if(grid != null){
            		grid.getStore().reload();//刷新管理界面的表格
            	}
				Ext.MessageBox.show({
					title : '提示',
					msg : '保存成功!是否继续操作?',
					width : 250,
					buttons : Ext.MessageBox.YESNO,
					icon : Ext.MessageBox.QUESTION,
					fn : function(btn) {
						if(btn == 'no') {
						    win.close();
						}else if(btn == 'yes'){
							var obj = {
									id : null,
									sort : parseInt(win.sort.getValue()) + 1,
									imageurl : null,
									isaurl : null,
									aurl : null,
									title : null,
									content : null
								};
							win.form.form.setValues(obj);
						}
					}
				});
			});
        }
     },
     
     /**
      * 删除
      */
     del : function(grid){
    	 var me = this;
 		 // 获取选中的行
 	     var data = grid.getSelectionModel().getSelection();
         if (data.length == 0) {
             Ext.MessageBox.alert('提示', '请先选中要删除的项目！');
         }else {
        	 Ext.MessageBox.show({
        		 title : '询问',
        		 msg : '您确定要删除吗?',
 			     width : 250,
 				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                     if (btn == 'yes') {
                    	 var dir = new Array();　
                         Ext.Array.each(data, function(items) {
                             var id = items.data.id;
                             dir.push(id);
             			});
             			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('b_articleimage/del_articleimage_qx_nrztbj.do' , param ,
                        function(response, options){
                        	grid.getStore().reload();
                        }, null, me.form);
                     }
                 }
             });
         }

 	},

	/**
     * 是否启用停用
     */
	isEnabled : function(grid, isQT){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
		        msg : '您确定要修改图片状态吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
					    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
			            });
             			var pram = {ids : dir, isenabled : isQT};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest('b_articleimage/enabled_articleimage_qx_nrztbj.do' , param ,
                        function(response, options){
                        	grid.getStore().reload(); 
                        });
                    }
                }
            });
        }
	},
	
	/**
     * 修改组图元素
     */
	editor : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{
            // 先得到主键
            var id = data[0].data.id;
            var win = Ext.create('system.bcms.article.biz.ImageArticleItemEdit');
            win.setTitle('修改组图');
            win.modal = true;
            win.grid = grid;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.form.getForm() , 'b_articleimage/load_ObjectByid.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},
	
	/**
     * 查看组图元素
     */
	view : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{
            // 先得到主键
            var id = data[0].data.id;

            var win = Ext.create('system.bcms.article.biz.ImageArticleItemEdit');
            win.setTitle('查看组图');
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.form.getForm() , 'b_articleimage/load_ObjectByid.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	}
});