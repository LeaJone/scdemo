﻿/**
 * 机构添加界面
 * @author lw
 */
Ext.define('system.bcms.article.biz.ArticleQuery', {
	extend : 'Ext.window.Window',
	grid : null,
	form : null,
	oper : Ext.create('system.bcms.article.biz.ArticleOper'),
	initComponent:function(){
	    var me = this;
	    
	    var id = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '编号',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'id',
	    	maxLength: 32,
	    	maxLengthText: '编号不能超过32个字符'
	    });
	    
	    var title = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '标题',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'title',
	    	maxLength: 50,
	    	maxLengthText: '标题不能超过50个字符'
	    });
	    
	    var type = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'文章类型',
	        labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
    	    name : 'type',//提交到后台的参数名
	        key : 'wzlx'//参数
	    });
	    
	    var subjectid = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属栏目',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	rootText : '主栏目',
	    	rootId : 'FSDMAIN',
			rootVisible: false,
	        name : 'subjectid',
	        baseUrl:'B_Subject/load_AsyncSubjectTreeByPopdomQuery.do'//访问路劲
	    });
	    
		var branchid = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属机构',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	rootText : me.oper.util.getParams("company").name,
	    	rootId : me.oper.util.getParams("company").id,
			rootVisible: false,
	        name : 'branchid',
	        baseUrl:'A_Branch/load_AsyncBranchTreeByPopdomQuery.do'//访问路劲
	    });

	    var showtype = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'显示类型',
	        labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
    	    name : 'showtype',//提交到后台的参数名
	        key : 'wzxslx'
	    });

	    var auditing = Ext.create('system.widget.FsdComboBox',{
            fieldLabel:'审核状态',
	        labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
    	    name : 'auditing',//提交到后台的参数名
            store : [
                    ['true', '已审核'],
                    ['false', '未审核']
                ]
	    });
	    
	    checkboxgroup = new Ext.form.CheckboxGroup({
		    fieldLabel: '文章属性',
		    labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
		    items: [
		        {
				    name : 'firstly',    
				    boxLabel: '置顶',
				    inputValue: 'true'
			    }, 
			    {
					name : 'slide',
					boxLabel: '幻灯',
					inputValue: 'true'
				}, 
			    {
					name : 'ismobile',
					boxLabel: '手机发布',
					inputValue: 'true'
				}]
	    });
	    
	    var overdue = Ext.create('system.widget.FsdDateField',{
	    	fieldLabel: '过期时间',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'overdue'
	    });

	    var author = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '作者',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'author',
	    	maxLength: 50,
	    	maxLengthText: '作者不能超过50个字符'
	    });
	    
	    var source = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '来源',
	    	labelAlign:'right',
	    	labelWidth:70,
	        width : 340,
	    	name: 'source',
	    	maxLength: 50,
	    	maxLengthText: '来源不能超过50个字符'
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [id, title, type, subjectid, branchid, showtype, auditing,
			         checkboxgroup, overdue, author, source]
        });
	        
	    Ext.apply(this,{
	        width:410,
	        height:390,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        items :[form],
	        buttons : [{
			    text : '查询',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				if (me.form != null && me.grid != null){
    					me.form.queryObj = form.getForm().getValues();
    					me.grid.getStore().reload();
    				    me.close();
    				}
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});