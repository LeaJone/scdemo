﻿/**
 * 文本Wap文章编辑界面
 * @author lw
 */
Ext.define('system.bcms.article.biz.ArticleWapEditPanel', {
	extend : 'Ext.panel.Panel',
	oper : Ext.create('system.bcms.article.biz.ArticleOper'),
	
	grid : null,//管理表格
	formWin : null,//编辑窗体
	
	lstLM: null,
	lstXG:null,
	
	loadObject : function(obj, LM, XG){
		var me = this;
		
		me.lstLM = LM;
		me.lstXG = XG;
		
    	me.form.getForm().setValues(obj);
		me.msubjectid.setText(me.msubjectname.getValue());
	},
	
	viewObject : function(){
		var me = this;
		me.down('button[name=btnwapsave]').setVisible(false);
		me.down('button[name=btnwapcopy]').setVisible(false);
	},
	
	setNew : function(){
		var me = this;
		var obj = {
			id : null,
			ismobile : null,
			mimageurl : null,
			mimagedepict : null,
			misaurl : null,
			maurl : null,
			mabstracts : null,
			mcontent : null
		};
		me.form.getForm().setValues(obj);
		me.treesubjectwap.setChosseValues(null);
		me.treesubjectwapxg.setChosseValues(null);
	},
	
	initComponent : function() {
		var me = this;

		me.idNo = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '编号',
	    	name : 'id'
	    });

	    me.ismobile = Ext.create('system.widget.FsdCheckbox',{
	    	fieldLabel : '是否移动版',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	        width : 400,
	    	name : 'ismobile',
	    	inputValue : 'true'
	    });
	    me.ismobile.on('change', function (obj, newValue, oldValue, eOpts) {
	    	me.msubjectid.allowBlank = !newValue;
	    });
		
	    me.msubjectname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel : '所属Wap栏目名称',
	    	name : 'msubjectname'
	    });
	    me.msubjectid = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属Wap栏目',
	    	labelAlign : 'right',
	    	labelWidth : 80,
	    	rootText : 'Wap栏目',
	    	rootId : 'FSDMOBILE',
			rootVisible : false,
	        width : 400,
	        name : 'msubjectid',
	        baseUrl :'B_Subject/load_AsyncSubjectTreeByPopdom.do',//访问路劲
	        allowBlank : true,
	        blankText : '请选择所属Wap栏目',
	        hiddenName : 'msubjectname'//隐藏域Name
	    });
	    
	    var mimageurl = Ext.create('system.widget.FsdTextFileManage', {
	        width : 400,
	    	zName : 'mimageurl',
	    	zFieldLabel : 'Wap标题图片',
	    	zLabelWidth : 80,
	    	zIsShowButton : true,
	    	zIsGetButton : true,
	    	zIsUpButton : true,
	    	zContentObjFun : function(){
	    		return me.formWin.formArticle.content;
	    	},
	    	zFileUpPath : me.oper.util.getImagePath()
	    });
	    
	    me.mimagedepict = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: 'Wap图片描述',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 400,
	    	name: 'mimagedepict',
	    	maxLength: 500
	    });

	    me.maurl = Ext.create('system.widget.FsdTextCheckbox',{
	    	zNameCheckbox : 'misaurl',
	    	zNameText : 'maurl',
	    	zFieldLabel : '外链接',
	    	zLabelAlign : 'right',
	    	zLabelWidth : 80,
	    	zInputValue : 'true',
	        width : 400,
	        zMaxLength : 200
	    });
	    
	    me.mabstracts = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '简介',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 800,
	    	name: 'mabstracts',
	    	maxLength: 250,
	    	maxLengthText: '简介不能超过250个字符'
	    });
	    
	    me.mcontent = Ext.create('system.widget.FsdUeditor',{
	    	fieldLabel: '内容',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 820,
	        height : 400,
	    	name: 'mcontent',
	    	toolbars : [['source']]
	    });

	    /**
		 * 相关栏目面板
		 */
	    me.treesubjectwap = Ext.create("system.widget.FsdTreePanel", {
			title: "Wap栏目",
		    columnWidth : 0.5,
    		minWidth : 350,
    		border : true,
            rootVisible : false,//不显示根节点
            baseUrl :'B_Subject/get_QxSubjectCheck.do',
	        zTreeRoot : 'children',//一次性加载
            zTreeParams : {id : system.UtilStatic.subjectWapCode},
            tbar : [{
					xtype : "button",
					text : "展开",
					iconCls : 'expand-all',
					handler : function() {
						treesubjectwap.expandAll();
					}
				},{
					xtype : "button",
					text : "收缩",
					iconCls : 'collapse-all',
					handler : function() {
						treesubjectwap.collapseAll();
					}
				}]
    	});
	    
	    /**
		 * 相关栏目面板
		 */
		me.treesubjectwapxg = Ext.create("system.widget.FsdTreePanel", {
			title: "Wap相关栏目",
		    columnWidth : 0.5,
    		minWidth : 350,
    		border : true,
            rootVisible : false,//不显示根节点
            baseUrl :'B_Subject/get_QxSubjectCheck.do',
	        zTreeRoot : 'children',//一次性加载
            zTreeParams : {id : system.UtilStatic.subjectWapXGCode},
            tbar : [{
					xtype : "button",
					text : "展开",
					iconCls : 'expand-all',
					handler : function() {
						me.treesubjectwapxg.expandAll();
					}
				},{
					xtype : "button",
					text : "收缩",
					iconCls : 'collapse-all',
					handler : function() {
						me.treesubjectwapxg.collapseAll();
					}
				}]
    	});
	    
	    me.form = Ext.create('Ext.form.Panel',{
			border : false,
			padding : '10 0 30 20',
			items : [me.idNo, me.msubjectname,
            {
                layout : 'column',
                border : false,
    	        width : 800,
                items:[{
                    columnWidth : 0.5,
                    border:false,
                    items:[me.ismobile, mimageurl, me.mimagedepict]
                }, {
                    columnWidth : 0.5,
                    border:false,
                    items:[me.msubjectid, me.maurl]
                }]
			}, 
			me.mabstracts, me.mcontent,
			{
				xtype : 'fieldset',
                title : '关联信息',
                margin : '-50 0 0 0',
                layout : 'column',
                border : true,
    	        width : 900,
                items : [ me.treesubjectwap, me.treesubjectwapxg ]
			}]
	    });
	    
	    
	    me.treesubjectwap.getStore().on('load', function(s) {
	    	me.treesubjectwap.setChosseValues(me.lstLM);
		});
	    
	    me.treesubjectwapxg.getStore().on('load', function(s) {
	    	me.treesubjectwapxg.setChosseValues(me.lstXG);
		});
	    
	    me.on('boxready' , function(obj, width, height, eOpts){
        	me.msubjectid.setText(me.msubjectname.getValue());
	    });
	    
		Ext.apply(me, {
        	title: '移动信息发布',
			autoScroll : true,
			disabled : true,
	        tbar : [{
				xtype : "fsdbutton",
				text : "提交Wap信息",
	        	name : 'btnwapsave',
				iconCls : 'acceptIcon',
				handler : function(button) {
					me.oper.submitArticleWap(me.form, me.grid, me.treesubjectwap, me.treesubjectwapxg);
				}
			}, {
				xtype : "button",
				text : "拷贝过滤内容",
	        	name : 'btnwapcopy',
				iconCls : 'acceptIcon',
				handler : function(button) {
					if (me.mabstracts.getValue() == '')
						me.mabstracts.setValue(me.formWin.formArticle.abstracts.getValue());
					me.oper.copyText(me, me.formWin.formArticle.content, me.mcontent);
				}
			}, {
				xtype : "button",
				text : "拷贝原版内容",
	        	name : 'btnybcopy',
				iconCls : 'acceptIcon',
				handler : function(button) {
					if (me.mabstracts.getValue() == '')
						me.mabstracts.setValue(me.formWin.formArticle.abstracts.getValue());
					me.oper.copyTextYB(me, me.formWin.formArticle.content, me.mcontent);
				}
			}],
	        items :[me.form]
	    });
		me.callParent(arguments);
	}
});