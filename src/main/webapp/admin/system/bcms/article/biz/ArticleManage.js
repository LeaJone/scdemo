﻿/**
 * 文本文章管理界面
 * @author lumingbao
 */

Ext.define('system.bcms.article.biz.ArticleManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*' , 
	             'system.bcms.article.model.Extjs_Column_B_article',
	             'system.bcms.article.model.Extjs_Model_B_article',
	             'system.bcms.article.model.Extjs_Column_A_Employee',
	             'system.bcms.article.model.Extjs_Model_A_Employee'],
	header : false,
	border : 0,
	layout : 'fit',
	
	oper : Ext.create('system.bcms.article.biz.ArticleOper'),
	queryObj : null,
	
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},

	createContent : function(){
		var me = this;

		var queryID =  Ext.create("Ext.form.TextField" , {
			emptyText : '请输入文章ID',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						me.queryObj = {id : queryID.getValue(), title : queryText.getValue()};
						grid.getStore().loadPage(1);
					}
				}
			}
		});
		var queryText =  Ext.create("Ext.form.TextField" , {
			emptyText : '请输入文章标题',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						me.queryObj = {id : queryID.getValue(), title : queryText.getValue()};
						grid.getStore().loadPage(1);
					}
				}
			}
		});
		
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_b_article', // 显示列
			model: 'model_b_article',
			baseUrl : 'B_Article/load_Article.do',
			border : false,
			tbar : {
				enableOverflow: true,//控件过多，出现下拉按钮
				defaults: {
			        height : 73
				},
				items: [{
			        xtype: 'buttongroup',
			        columns: 2,
			        items: [{
					xtype : "fsdbutton",
					text : "添加文章",
					iconCls : 'articlewz',
					popedomCode : 'nrwzbj',
					handler : function(button) {
//						me.oper.addText(grid);
						me.oper.addText(grid, true);
					}
				}, {
					xtype : "fsdbutton",
					text : "添加组图",
					iconCls : 'articlezt',
					popedomCode : 'nrztbj',
					handler : function(button) {
//						me.oper.addImage(grid);
						me.oper.addImage(grid, true);
					}
				}, {
					xtype : "fsdbutton",
					text : "添加视频",
					iconCls : 'articlesp',
					popedomCode : 'nrspbj',
					handler : function(button) {
//						me.oper.addVideo(grid);
						me.oper.addVideo(grid, true);
					}
				}, {
					xtype : "fsdbutton",
					text : "添加下载",
					iconCls : 'articlewj',
					popedomCode : 'nrxzbj',
					handler : function(button) {
//						me.oper.addFile(grid);
						me.oper.addFile(grid, true);
					}
				}]}, {
			        xtype: 'buttongroup',
			        columns: 2,
			        items: [{
					xtype : "fsdbutton",
					text : "修改",
					iconCls : 'page_editIcon',
					popedomCode : 'wzbj',
					handler : function(button) {
						me.oper.editor(grid);
					}
				}, {
					text : '查看',
					iconCls : 'magnifierIcon',
					handler : function() {
						me.oper.view(grid);
					}
				}, {
					xtype : "fsdbutton",
					text : "删除",
					iconCls : 'page_deleteIcon',
					popedomCode : 'wzsc',
					handler : function(button) {
					    me.oper.del(grid);
					}
				}]}, {
			        xtype: 'buttongroup',
			        columns: 4,
			        items: [{
					xtype : "fsdbutton",
					text : "通过审核",
					iconCls : 'tickIcon',
					popedomCode : 'wzsh',
					handler : function(button) {
						me.oper.auditing(grid , 'true');
					}
				}, {
					xtype : "fsdbutton",
					text : "文章置顶",
					iconCls : 'tickIcon',
					popedomCode : 'wzzd',
					handler : function(button) {
						me.oper.firstly(grid , 'true');
					}
				}, {
					xtype : "fsdbutton",
					text : "图片幻灯",
					iconCls : 'tickIcon',
					popedomCode : 'wzhd',
					handler : function(button) {
						me.oper.slide(grid , 'true');
					}
				}, {
					xtype : "fsdbutton",
					text : "文章推荐",
					iconCls : 'tickIcon',
					popedomCode : 'wztj',
					handler : function(button) {
						me.oper.recommend(grid , 'true');
					}
				}, {
					xtype : "fsdbutton",
					text : "取消审核",
					iconCls : 'crossIcon',
					popedomCode : 'wzsh',
					handler : function(button) {
						me.oper.auditing(grid , 'false');
					}
				}, {
					xtype : "fsdbutton",
					text : "取消置顶",
					iconCls : 'crossIcon',
					popedomCode : 'wzzd',
					handler : function(button) {
						me.oper.firstly(grid , 'false');
					}
				}, {
					xtype : "fsdbutton",
					text : "取消幻灯",
					iconCls : 'crossIcon',
					popedomCode : 'wzhd',
					handler : function(button) {
						me.oper.slide(grid , 'false');
					}
				}, {
                        xtype : "fsdbutton",
                        text : "取消推荐",
                        iconCls : 'crossIcon',
                        popedomCode : 'wztj',
                        handler : function(button) {
                            me.oper.recommend(grid , 'false');
                        }
                }]}, {
			        xtype: 'buttongroup',
			        columns: 2,
			        items: [{
						xtype : "fsdbutton",
						text : "更改文章类型",
						iconCls : 'articlewz',
						popedomCode : 'wzbj',
						handler : function(button) {
							me.oper.updateType(grid);
						}
					}, {
						xtype : "fsdbutton",
						text : "栏目文章迁移",
						iconCls : 'arrow_redoIcon',
						popedomCode : 'wzbj',
						handler : function(button) {
							me.oper.updateRemove(grid);
						}
					}, {
						xtype : "fsdbutton",
						text : "更改所属栏目",
						iconCls : 'page_editIcon',
						popedomCode : 'wzbj',
						handler : function(button) {
							me.oper.updateSubject(grid);
						}
				}, {
					xtype : "fsdbutton",
					text : "文章推送",
					iconCls : 'up',
					popedomCode : 'wzbj',
					handler : function(button) {
						me.oper.pushArticle(grid);
					}
				}]}, {
			        xtype: 'buttongroup',
			        columns: 2,
			        items: [queryID, {
					text : '查询',
					iconCls : 'magnifierIcon',
					handler : function() {
						me.queryObj = {id : queryID.getValue(), title : queryText.getValue()};
						grid.getStore().loadPage(1);
					}
				}, queryText
				]}, {
			        xtype: 'buttongroup',
			        columns: 2,
			        items: [{
						text : '高级查询',
						iconCls : 'magnifierIcon',
			            rowspan: 2,
			            iconAlign: 'top',
						handler : function() {
							me.oper.query(me, grid);
						}
					}, {
					text : '刷新',
					iconCls : 'tbar_synchronizeIcon',
		            rowspan: 2,
		            iconAlign: 'top',
					handler : function() {
						grid.getStore().reload();
					}
				}]}
			]}			
		});
		
		grid.getStore().on('beforeload', function(s) {
			if (me.queryObj == null){
				me.queryObj = {};
			}
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(me.queryObj)});
		});
		
		grid.on('cellclick', function(thisObj, td, cellIndex, record, tr, rowIndex, e, eOpts) {
			if (cellIndex == 12){
				me.oper.preview(record.get('id'));
			}
		});
		
		return grid;
	}
});