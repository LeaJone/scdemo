﻿/**
 * 文章类型变更
 * @author lw
 */
Ext.define('system.bcms.article.biz.ArticleUpdateSubject', {
	extend : 'Ext.window.Window',
	updateFun : null,
    title : '文章栏目变更',

	initComponent:function(){
	    var me = this;

	    var subject = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属栏目',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 300,
	    	rootText : '主栏目',
	    	rootId : 'FSDMAIN',
			rootVisible: false,
	        name : 'subjectid',
	        baseUrl:'B_Subject/load_AsyncSubjectTreeByPopdom.do',//访问路劲
	        allowBlank: false,
	        blankText: '请选择所属栏目'
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [subject]
        });
	        
	    Ext.apply(this,{
	        width:360,
	        height:130,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '更改',
			    iconCls : 'acceptIcon',
			    handler : function() {
			    	if (form.form.isValid()) {
	    				if (me.updateFun != null){
	    					me.updateFun(subject.getValue(), subject.getRawValue());
	    				    me.close();
	    				}
			    	}
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});