﻿/**
 * 单个组图编辑界面
 * @author lw
 */
Ext.define('system.bcms.article.biz.ImageArticleItemEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	oper : Ext.create('system.bcms.article.biz.ImageArticleOper'),
	
	setArticleID : function(articleid){
		var me = this;
		me.articleid.setValue(articleid);
	},
	
	initComponent : function() {
		var me = this;
		
		me.articleid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel : '文章编号',
	    	name : 'articleid'
	    });
	    
	    me.sort = Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '排序编号',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 300,
	    	name: 'sort',
	    	maxLength: 3,
	    	minValue: 0,
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank: false,
	    	blankText: '请输入排序编号',
	    	maxLengthText: '排序编号不能超过3个字符'
	    });

	    var imageurl = Ext.create('system.widget.FsdTextFileManage', {
	        width : 300,
	    	zName : 'imageurl',
	    	zFieldLabel : '图片地址',
	    	zLabelWidth : 80,
	    	zIsShowButton : true,
	    	zIsUpButton : true,
	    	zAllowBlank : false,
	    	zIsReadOnly : false,
	    	zBlankText : '选择图片地址',
	    	zFileUpPath : me.oper.util.getImagePath()
	    });

	    var littleurl = Ext.create('system.widget.FsdTextFileManage', {
	        width : 300,
	    	zLabelWidth : 80,
	    	zName : 'littleurl',
	    	zFieldLabel : '缩略图地址',
	    	zIsShowButton : true,
	    	zIsUpButton : true,
	    	zFileUpPath : me.oper.util.getImagePath()
	    });

	    var aurl = Ext.create('system.widget.FsdTextCheckbox',{
	    	zNameCheckbox : 'isaurl',
	    	zNameText : 'aurl',
	    	zFieldLabel : '外链接',
	    	zLabelAlign : 'right',
	    	zLabelWidth : 80,
	        width : 300,
	        zMaxLength : 200
	    });
		
		var title = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '标题',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 600,
	    	name: 'title',
	    	maxLength: 100,
	    	allowBlank: false,
	    	blankText: '请输入标题',
	    	maxLengthText: '标题不能超过100个字符'
	    });
	    
	    var content = Ext.create('system.widget.FsdTextArea',{
	    	fieldLabel: '描述',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 600,
	        height : 200,
	    	name: 'content',
	    	maxLength: 1000,
	    	allowBlank: false,
	    	blankText: '请输入描述',
	    	maxLengthText: '描述不能超过1000个字符'
	    });

	    var status = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'状态',
	        labelAlign:'right',
	    	labelWidth : 80,
	        width : 300,
    	    name : 'status',//提交到后台的参数名
	        key : 'wzztzt',//参数
    	    hiddenName : 'statusname',//提交隐藏域Name
	    });
	    var statusname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '状态名',
	    	name: 'statusname'
	    });
	    
	    me.form = Ext.create('Ext.form.Panel',{
				xtype : 'form',
				header : false,// 是否显示标题栏
				autoScroll : true,
				border : false,
				padding : '10 0 20 20',
				items : [
				         {
                    name: "id",
                    xtype: "hidden"
                }, 
                me.articleid, statusname, 
                {
                    layout : 'column',
                    border : false,
        	        width : 600,
                    items:[{
                        columnWidth : 0.5,
                        border:false,
                        items:[imageurl, aurl]
                    }, {
                        columnWidth : 0.5,
                        border:false,
                        items:[status, me.sort]
                    }]
    			}, 
    			title, content
                ]
	    });
	    
		Ext.apply(this,{
	        width:670,
	        height:400,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        items :[me.form],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'nrwzbj',
	        	name : 'btnsave',
				text : "　提交　",
				iconCls : 'acceptIcon',
				handler : function(button) {
					me.oper.formSubmit(me, me.grid);
				}
			}, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
			}]
	    });
		this.callParent(arguments);
	}
});