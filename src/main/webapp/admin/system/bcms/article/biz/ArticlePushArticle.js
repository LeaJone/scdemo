﻿/**
 * 推送文章
 * @author Administrator
 */
Ext.define('system.bcms.article.biz.ArticlePushArticle', {
	extend : 'Ext.window.Window',
	updateFun : null,
    title : '文章推送',

	initComponent:function(){
	    var me = this;

	    me.realname = Ext.create('system.widget.FsdTextGrid',{	
        	zFieldLabel : '选择用户', 
	    	zLabelWidth : 80,
	    	width : 350,
		    zName : 'realname',
		    zColumn : 'column_a_employeexz', // 显示列
		    zModel : 'model_a_employee',
		    zBaseUrl : 'h_member/load_databycompany.do',
		    zAllowBlank : false,
		    zGridType : 'list',
			zGridWidth : 700,//弹出表格宽度
			zGridHeight : 600,//弹出表格高度
		    zIsText1 : true,
		    zTxtLabel1 : '用户名称',
		    zFunChoose : function(data){
		    	me.employeeid.setValue(data.id);
		    	me.realname.setValue(data.realname);
		    },
		    zFunQuery : function(txt1, txt2, txt3, txt4){
		    	return {fid : 'FSDMAIN', txt : txt1};
		    }	
	    });
	    me.employeeid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所选用户id',
	    	name: 'id'
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [me.realname]
        });
	        
	    Ext.apply(this,{
	        width:400,
	        height:130,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '推送',
			    iconCls : 'acceptIcon',
			    handler : function() {
			    	if (form.form.isValid()) {
	    				if (me.updateFun != null){
	    					me.updateFun(me.employeeid.getValue());
	    				    me.close();
	    				}
			    	}
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});