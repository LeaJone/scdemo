/**
 * 回收站操作类
 * @author lumingbao
 */

Ext.define('system.bcms.article.biz.RecycleOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 恢复文章
     */
    recoveryArticle : function(grid){
   	 var me = this;
		 // 获取选中的行
	     var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要恢复的项目！');
        }else {
       	 Ext.MessageBox.show({
       		 title : '询问',
       		 msg : '您确定要执行该操作吗?',
			     width : 250,
				 buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                   	 var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
            			});
            			var pram = {ids : dir};
                       var param = {jsonData : Ext.encode(pram)};
                       me.util.ExtAjaxRequest('B_Article/recovery_Article.do' , param ,
                       function(response, options){
                       	grid.getStore().reload();
                       });
                    }
                }
            });
        }

	},
	
	/**
     * 彻底删除文章
     */
    reldelete : function(grid){
   	 var me = this;
		 // 获取选中的行
	     var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要删除的项目！');
        }else {
       	 Ext.MessageBox.show({
       		 title : '询问',
       		 msg : '您确定要删除吗?',
			     width : 250,
				 buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
                   	 var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
            			});
            			var pram = {ids : dir};
                       var param = {jsonData : Ext.encode(pram)};
                       me.util.ExtAjaxRequest('B_Article/reldel_Article.do' , param ,
                       function(response, options){
                       	grid.getStore().reload();
                       });
                    }
                }
            });
        }
	}
});