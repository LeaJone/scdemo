﻿/**
 * 文章类型变更
 * @author lw
 */
Ext.define('system.bcms.article.biz.ArticleUpdateType', {
	extend : 'Ext.window.Window',
	updateFun : null,
    title : '文章类型变更',

	initComponent:function(){
	    var me = this;

	    var type = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'文章类型',
	        labelAlign:'right',
	    	labelWidth:60,
	        width : 300,
    	    name : 'type',//提交到后台的参数名
	        key : 'wzlx',//参数
	        allowBlank: false,
	        blankText: '请选择文章类型',
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [type]
        });
	        
	    Ext.apply(this,{
	        width:360,
	        height:130,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '更改',
			    iconCls : 'acceptIcon',
			    handler : function() {
			    	if (form.form.isValid()) {
	    				if (me.updateFun != null){
	    					me.updateFun(type.getValue(), type.getRawValue());
	    				    me.close();
	    				}
			    	}
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});