﻿/**
 * 文章回收站管理界面
 * @author lumingbao
 */

Ext.define('system.bcms.article.biz.RecycleManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*', 
	             'system.bcms.article.model.Extjs_Column_B_article',
	             'system.bcms.article.model.Extjs_Model_B_article'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.bcms.article.biz.RecycleOper'),
	
	createContent : function(){
		var me = this;
		
		var queryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入文章标题',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});
		
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_b_article', // 显示列
			model: 'model_b_article',
			baseUrl : 'B_Article/load_RecycleArticle.do',
			border : false,
			tbar : [{
				xtype : "fsdbutton",
				text : "还原",
				iconCls : 'arrow_redoIcon',
				handler : function(button) {
					me.oper.recoveryArticle(grid);
				}
			}, {
				xtype : "button",
				text : "彻底删除",
				iconCls : 'page_deleteIcon',
				handler : function(button) {
				    me.oper.reldelete(grid);
				}
			}, '->', queryText, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]
		});
		
		grid.getStore().on('beforeload', function(s) {
			var title =  queryText.getValue();
			var pram = {title : title};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		
		return grid;
	}
});