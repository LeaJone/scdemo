﻿/**
 *b_article Column
 *@author CodeSystem
 *文件名     Extjs_Column_B_article
 *Columns名  column_b_article
 */

Ext.define('column_b_article', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            width : 40
        }
        ,
        {
            header : '标题',
            dataIndex : 'showtitle',
            sortable : true,
            flex : 8
        }
        ,
        {
            header : '所属栏目',
            dataIndex : 'subjectname',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '所属机构',
            dataIndex : 'branchname',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '类型',
            dataIndex : 'typename',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '取图',
            dataIndex : 'imageurl1',
            sortable : true,
            flex : 1,
			renderer :function(value ,metaData ,record ){
				if(value != ''){
					value="<font style='color:blue;'>是</font>";
				}else{
					value='';
				}
				return value;
			}
        }
        ,
        {
            header : '审核',
            dataIndex : 'auditing',
            sortable : true,
            flex : 1,
			renderer :function(value ,metaData ,record ){
				if(value == 'true'){
					value="<font style='color:green;'>审</font>";
				}else{
					value="<font style='color:red;'>停</font>";
				}
				return value;
			}
        }
        ,
        {
            header : '置顶',
            dataIndex : 'firstly',
            sortable : true,
            flex : 1,
			renderer :function(value ,metaData ,record ){
				if(value == 'true'){
					value="<font style='color:blue;'>是</font>";
				}else{
					value="<font style='color:gray;'>否</font>";
				}
				return value;
			}
        }
        ,
        {
            header : '幻灯',
            dataIndex : 'slide',
            sortable : true,
            flex : 1,
			renderer :function(value ,metaData ,record ){
				if(value == 'true'){
					value="<font style='color:blue;'>是</font>";
				}else{
					value="<font style='color:gray;'>否</font>";
				}
				return value;
			}
        }
        ,
        {
            header : '外链接',
            dataIndex : 'isaurl',
            sortable : true,
            flex : 1,
			renderer :function(value ,metaData ,record ){
				if(value == 'true'){
					value="<font style='color:blue;'>是</font>";
				}else{
					value="<font style='color:gray;'>否</font>";
				}
				return value;
			}
        }
        ,
        {
            header : '推荐',
            dataIndex : 'recommend',
            sortable : true,
            flex : 1,
			renderer :function(value ,metaData ,record ){
				if(value == 'true'){
					value="<font style='color:blue;'>是</font>";
				}else{
					value="<font style='color:gray;'>否</font>";
				}
				return value;
			}
        }
      ,
      {
          header : '预览',
          dataIndex : 'id',
          sortable : true,
          flex : 1,
			renderer :function(value ,metaData ,record ){
				metaData.style='color:blue;text-decoration:underline;cursor:pointer;';//下划线
				value="<font style='color:blue;'>预览</font>";
				return value;
			}
      }
        ,
        {
            header : '操作人',
            dataIndex : 'addemployeename',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '新增时间',
            dataIndex : 'adddate',
            sortable : true,
            flex : 4,
			renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value);
			}
        }
    ]
});

Ext.define('column_n_article', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            align : 'center',
            width : 50
        }
        ,
        {
            header : '标题',
            dataIndex : 'title',
            sortable : true,
            align : 'center',
            flex : 1
        }]
});
