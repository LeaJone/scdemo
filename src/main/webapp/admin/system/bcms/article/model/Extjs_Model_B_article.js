﻿﻿/**
 *b_article Model
 *@author CodeSystem
 *文件名     Extjs_Model_B_article
 *Model名    model_b_article
 */

Ext.define('model_b_article', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'subjectid',
            type : 'string'
        }
        ,
        {
            name : 'subjectname',
            type : 'string'
        }
        ,
        {
            name : 'type',
            type : 'string'
        }
        ,
        {
            name : 'typename',
            type : 'string'
        }
        ,
        {
            name : 'title',
            type : 'string'
        }
        ,
        {
            name : 'showtitle',
            type : 'string'
        }
        ,
        {
            name : 'firstly',
            type : 'string'
        }
        ,
        {
            name : 'slide',
            type : 'string'
        }
        ,
        {
            name : 'aurl',
            type : 'string'
        }
        ,
        {
            name : 'isaurl',
            type : 'string'
        }
        ,
        {
            name : 'showtype',
            type : 'string'
        }
        ,
        {
            name : 'showtypename',
            type : 'string'
        }
        ,
        {
            name : 'imageurl1',
            type : 'string'
        }
        ,
        {
            name : 'auditing',
            type : 'string'
        }
        ,
        {
            name : 'recommend',
            type : 'string'
        }
        ,
        {
            name : 'msubjectid',
            type : 'string'
        }
        ,
        {
            name : 'msubjectname',
            type : 'string'
        }
        ,
        {
            name : 'branchid',
            type : 'string'
        }
        ,
        {
            name : 'branchname',
            type : 'string'
        }
        ,
        {
            name : 'adddate',
            type : 'string'
        }
        ,
        {
            name : 'addemployeeid',
            type : 'string'
        }
        ,
        {
            name : 'addemployeename',
            type : 'string'
        }
    ]
});

Ext.define('model_n_article', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'title',
            type : 'string'
        }
    ]
});
