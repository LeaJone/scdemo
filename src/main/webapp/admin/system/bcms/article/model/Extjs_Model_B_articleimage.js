﻿/**
 *b_articleimage Model
 *@author CodeSystem
 *文件名     Extjs_Model_B_articleimage
 *Model名    model_b_articleimage
 */

Ext.define('model_b_articleimage', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'articleid',
            type : 'string'
        }
        ,
        {
            name : 'title',
            type : 'string'
        }
        ,
        {
            name : 'imageurl',
            type : 'string'
        }
        ,
        {
            name : 'littleurl',
            type : 'string'
        }
        ,
        {
            name : 'aurl',
            type : 'string'
        }
        ,
        {
            name : 'isaurl',
            type : 'string'
        }
        ,
        {
            name : 'sort',
            type : 'int'
        }
        ,
        {
            name : 'status',
            type : 'string'
        }
        ,
        {
            name : 'statusname',
            type : 'string'
        }
        ,
        {
            name : 'adddate',
            type : 'string'
        }
        ,
        {
            name : 'addemployeeid',
            type : 'string'
        }
        ,
        {
            name : 'addemployeename',
            type : 'string'
        }
    ]
});
