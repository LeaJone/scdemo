﻿/**
 *E_UserInfo Column
 *@author CodeSystem
 *文件名     Extjs_Column_E_userinfo
 *Columns名  column_e_userinfo
 *xtype: 'textfield', 'numbercolumn', 'booleancolumn', 'datecolumn'   format : '0.00'
 *align: 'left', 'center', 'right'
 */

Ext.define('column_a_employeexz', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            align : 'center',
            width : 40
        }
        ,
        {
            header : '人员ID',
            dataIndex : 'id',
            sortable : true,
            align : 'center',
            flex : 1
        }
        ,
        {
            header : '真实姓名',
            dataIndex : 'realname',
            sortable : true,
            align : 'center',
            flex : 1
        }
        ,
        {
            header : '昵称',
            dataIndex : 'nickname',
            sortable : true,
            align : 'center',
            flex : 1
        }
        ,
        {
            header : '性别',
            dataIndex : 'sexname',
            sortable : true,
            align : 'center',
            flex : 0.5
        }
    ]
});
