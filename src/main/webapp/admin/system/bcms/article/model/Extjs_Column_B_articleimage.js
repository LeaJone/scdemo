﻿/**
 *b_articleimage Column
 *@author CodeSystem
 *文件名     Extjs_Column_B_articleimage
 *Columns名  column_b_articleimage
 */

Ext.define('column_b_articleimage', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            width : 40
        }
        ,
        {
            header : '图片地址',
            dataIndex : 'imageurl',
            sortable : true,
            flex : 4,
			renderer :function(value ,metaData ,record ){
				if(value != null && value != ''){
					var height = system.UtilStatic.getImageHeight(value, 150);
					if (height == 0)
						value = "<font style='color:red;'>图片未加载，刷新重试</font>";
					else
						value="<img width='150' height='" + height + "'  src='/FsdSoft" + value + "' />";
				}else{
					value = '暂无图片';
				}
				return value;
			}
        }
        ,
        {
            header : '图片标题',
            dataIndex : 'title',
            sortable : true,
            flex : 4
        }
        ,
        {
            header : '外链接',
            dataIndex : 'isaurl',
            sortable : true,
            flex : 1,
			renderer :function(value ,metaData ,record ){
				if(value == 'true'){
					value="<font style='color:blue;'>是</font>";
				}else{
					value="<font style='color:gray;'>否</font>";
				}
				return value;
			}
        }
        ,
        {
            header : '顺序号',
            dataIndex : 'sort',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '状态',
            dataIndex : 'statusname',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '操作人',
            dataIndex : 'addemployeename',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '新增时间',
            dataIndex : 'adddate',
            sortable : true,
            flex : 3,
			renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value);
			}
        }
    ]
});
