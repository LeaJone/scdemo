﻿/**
 *SubjectStat Model
 *@author CodeSystem
 *文件名     Extjs_Model_SubjectStat
 *Model名    model_subjectstat
 */

Ext.define('model_subjectstat', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'name',
            type : 'string'
        }
        ,
        {
            name : 'one',
            type : 'string'
        }
        ,
        {
            name : 'two',
            type : 'string'
        }
        ,
        {
            name : 'three',
            type : 'string'
        }
        ,
        {
            name : 'four',
            type : 'string'
        }
        ,
        {
            name : 'five',
            type : 'string'
        }
        ,
        {
            name : 'six',
            type : 'string'
        }
        ,
        {
            name : 'seven',
            type : 'string'
        }
        ,
        {
            name : 'eight',
            type : 'string'
        }
        ,
        {
            name : 'nine',
            type : 'string'
        }
        ,
        {
            name : 'ten',
            type : 'string'
        }
        ,
        {
            name : 'eleven',
            type : 'string'
        }
        ,
        {
            name : 'twelve',
            type : 'string'
        }
        ,
        {
            name : 'summation',
            type : 'string'
        }
    ]
});
