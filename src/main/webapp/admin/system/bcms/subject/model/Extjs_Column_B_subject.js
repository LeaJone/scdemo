﻿/**
 *b_subject Column
 *@author CodeSystem
 *文件名     Extjs_Column_B_subject
 *Columns名  column_b_subject
 *           column_b_subjectxg
 *           column_b_subjectzw
 *           column_b_subjectxz 弹出窗体查询
 */

Ext.define('column_b_subject', {
    columns: [
          {
              xtype : 'rownumberer',
              text : 'NO',
              sortable : true,
              width : 40
          }
          ,
        {
            header : '标题',
            dataIndex : 'title',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '所属栏目',
            dataIndex : 'parentname',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '图片地址',
            dataIndex : 'imageurl1',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '顺序号',
            dataIndex : 'sort',
            sortable : true,
            align : 'center',
            flex : 1
        }
        ,
        {
            header : '状态',
            dataIndex : 'statusname',
            sortable : true,
            align : 'center',
            flex : 1
        }
        ,
        {
            header : '公开目录',
            dataIndex : 'column1',
            sortable : true,
            align : 'center',
            flex : 1,
			renderer :function(value ,metaData ,record ){
				if(value == 'true'){
					//metaData.style='color:blue;text-decoration:underline;cursor:pointer;';下划线
					value="<font style='color:blue;'>是</font>";
				}else{
					value='';
				}
				return value;
			}
        }
        ,
        {
            header : '外链接',
            dataIndex : 'isaurl',
            sortable : true,
            align : 'center',
            flex : 1,
			renderer :function(value ,metaData ,record ){
				if(value == 'true'){
					//metaData.style='color:blue;text-decoration:underline;cursor:pointer;';下划线
					value="<font style='color:blue;'>启用</font>";
				}else{
					value="<font style='color:red;'></font>";
				}
				return value;
			}
        }
        ,
        {
            header : '单页栏目',
            dataIndex : 'issingle',
            sortable : true,
            align : 'center',
            flex : 1,
			renderer :function(value ,metaData ,record ){
				if(value == 'true'){
					//metaData.style='color:blue;text-decoration:underline;cursor:pointer;';下划线
					value="<font style='color:blue;'>启用</font>";
				}else{
					value="<font style='color:red;'></font>";
				}
				return value;
			}
        }
        ,
        {
            header : '文章发布',
            dataIndex : 'ispublish',
            sortable : true,
            align : 'center',
            flex : 1,
			renderer :function(value ,metaData ,record ){
				if(value == 'true'){
					//metaData.style='color:blue;text-decoration:underline;cursor:pointer;';下划线
					value="<font style='color:red;'>禁用发布</font>";
				}else{
					value="<font style='color:blue;'></font>";
				}
				return value;
			}
        }
        ,
        {
            header : '备注',
            dataIndex : 'remark',
            sortable : true,
            flex : 1
        }
    ]
});



Ext.define('column_b_subjectxg', {
    columns: [
        {
            header : '标题',
            dataIndex : 'title',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '所属栏目',
            dataIndex : 'parentname',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '顺序号',
            dataIndex : 'sort',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '备注',
            dataIndex : 'remark',
            sortable : true,
            flex : 2
        }
    ]
});



Ext.define('column_b_subjectzw', {
    columns: [
        {
            header : '标题',
            dataIndex : 'title',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '对应栏目',
            dataIndex : 'subjectname',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '所属栏目',
            dataIndex : 'parentname',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '顺序号',
            dataIndex : 'sort',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '外链接',
            dataIndex : 'isaurl',
            sortable : true,
            flex : 1,
			renderer :function(value ,metaData ,record ){
				if(value == 'true'){
					//metaData.style='color:blue;text-decoration:underline;cursor:pointer;';下划线
					value="<font style='color:blue;'>启用</font>";
				}else{
					value="<font style='color:red;'></font>";
				}
				return value;
			}
        }
        ,
        {
            header : '备注',
            dataIndex : 'remark',
            sortable : true,
            flex : 1
        }
    ]
});


Ext.define('column_b_subjectxz', {
    columns: [
          {
              xtype : 'rownumberer',
              text : 'NO',
              sortable : true,
              width : 30
          }
          ,
          {
              header : '标题',
              dataIndex : 'levelpath',
              sortable : true,
              width : '35%',
			  renderer :function(value ,metaData ,record){
				  var num = Number(value);
				  var title = '';
				  for ( var int = 1; int < num; int++) {
					  if (int == num - 1){
						  title += '　　|-';
					  }else{
						  title += '　　';
					  }
				  }
				  return title + record.data.title;
			  }
          }
          ,
          {
              header : '结构',
              dataIndex : 'column3',
              sortable : true,
              width : '65%'
          }
    ]
});
