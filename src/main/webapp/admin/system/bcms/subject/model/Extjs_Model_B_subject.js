﻿/**
 *b_subject Model
 *@author CodeSystem
 *文件名     Extjs_Model_B_subject
 *Model名    model_b_subject
 */

Ext.define('model_b_subject', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'subjectid',
            type : 'string'
        }
        ,
        {
            name : 'subjectname',
            type : 'string'
        }
        ,
        {
            name : 'parentid',
            type : 'string'
        }
        ,
        {
            name : 'parentname',
            type : 'string'
        }
        ,
        {
            name : 'title',
            type : 'string'
        }
        ,
        {
            name : 'imageurl1',
            type : 'string'
        }
        ,
        {
            name : 'sort',
            type : 'int'
        }
        ,
        {
            name : 'statusname',
            type : 'string'
        }
        ,
        {
            name : 'column1',
            type : 'string'
        }
        ,
        {
            name : 'column3',
            type : 'string'
        }
        ,
        {
            name : 'isaurl',
            type : 'string'
        }
        ,
        {
            name : 'ispublish',
            type : 'string'
        }
        ,
        {
            name : 'issingle',
            type : 'string'
        }
        ,
        {
            name : 'remark',
            type : 'string'
        }
        ,
        {
        	name : 'levelpath',
            type : 'string'
        }
    ]
});
