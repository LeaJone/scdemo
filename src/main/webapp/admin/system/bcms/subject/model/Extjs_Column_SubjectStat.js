﻿/**
 *SubjectStat Column
 *@author CodeSystem
 *文件名     Extjs_Column_SubjectStat
 *Columns名  column_subjectstat
 */

Ext.define('column_subjectstat', {
    columns: [
        {
            xtype : 'rownumberer',
            text : 'NO',
            sortable : true,
            width : 30
        }
        ,
        {
            header : '名称',
            dataIndex : 'name',
            sortable : true,
            flex : 8
        }
        ,
        {
            header : '一月',
            dataIndex : 'one',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '二月',
            dataIndex : 'two',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '三月',
            dataIndex : 'three',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '四月',
            dataIndex : 'four',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '五月',
            dataIndex : 'five',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '六月',
            dataIndex : 'six',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '七月',
            dataIndex : 'seven',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '八月',
            dataIndex : 'eight',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '九月',
            dataIndex : 'nine',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '十月',
            dataIndex : 'ten',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '十一月',
            dataIndex : 'eleven',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '十二月',
            dataIndex : 'twelve',
            sortable : true,
            flex : 2
        }
        ,
        {
            header : '年累计',
            dataIndex : 'summation',
            sortable : true,
            flex : 2
        }
    ]
});
