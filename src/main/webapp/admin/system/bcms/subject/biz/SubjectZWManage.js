﻿/**

 * 栏目管理界面
 * @author lumingbao
 */

Ext.define('system.bcms.subject.biz.SubjectZWManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*', 
	             'system.bcms.subject.model.Extjs_Column_B_subject',
	             'system.bcms.subject.model.Extjs_Model_B_subject',
	             'system.admin.tools.model.Extjs_Column_SortOperate',
		         'system.admin.tools.model.Extjs_Model_SortOperate'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.bcms.subject.biz.SubjectOper'),
	
	createContent : function(){
		var me = this;
		
		var queryid = 'FSDGOVERNMENT';
		
		/**
		 * 左边的树面板
		 */
		var treepanel = Ext.create("system.widget.FsdTreePanel", {
			icon : 'resources/admin/icon/chart_organisation.png',
			baseUrl : 'B_Subject/load_AsyncSubjectTreeQuery.do',
			title: '栏目结构',
			rootText : '政务栏目',
			rootId : 'FSDGOVERNMENT',
			region : 'west', // 设置方位
			collapsible : true,//折叠
			width: 182,
			split: true
		});
		
		
		var queryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入栏目名称',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});
		
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_b_subjectzw', // 显示列
			model: 'model_b_subject',
			baseUrl : 'B_Subject/load_data.do',
			tbar : [{
				xtype : "button",
				text : "添加",
				iconCls : 'page_addIcon',
				popedomCode : 'zwlmbj',
				handler : function(button) {
					me.oper.addGovernment(grid , treepanel);
				}
			}, {
				xtype : "button",
				text : "修改",
				iconCls : 'page_editIcon',
				popedomCode : 'zwlmbj',
				handler : function(button) {
					me.oper.editorGovernment(grid , treepanel);
				}
			}, {
				xtype : "button",
				text : "删除",
				iconCls : 'page_deleteIcon',
				popedomCode : 'zwlmsc',
				handler : function(button) {
				    me.oper.delGovernment(grid , treepanel);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.viewGovernment(grid);
				}
			}, {
				text : '排序',
				iconCls : 'up',
				handler : function() {
					var name =  queryText.getValue();
					var pram = {fid : queryid , name : name};
					me.oper.util.sortOperate(
							grid, 
							'column_title', 
							'model_sortoperate',
							grid.baseUrl,
							'B_Subject/sort_Subject_qx_zwlmbj.do',
							function(){return pram;},
				        	function(){treepanel.getStore().load();});
				}
			}, '-', {
				xtype : "fsdbutton",
				popedomCode : 'zwlmbj',
				text : "启用",
				iconCls : 'tickIcon',
				handler : function(button) {
				    me.oper.isEnabledGovernment(grid, true);
				}
			}, {
				xtype : "fsdbutton",
				popedomCode : 'zwlmbj',
				text : "停用",
				iconCls : 'crossIcon',
				handler : function(button) {
				    me.oper.isEnabledGovernment(grid, false);
				}
			}, '->', queryText , {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  queryText.getValue();
			var pram = {fid : queryid , name : name};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		var contentpanel = Ext.create("Ext.panel.Panel", {
			frame:true,
			split: true,
			region: 'center',
			layout: 'fit',
			title: '栏目信息',
			items: [grid]
		});

		var page1_jExtPanel1_obj = new Ext.panel.Panel({
			bodyPadding: 3,
			header : false,
			border : 0,
			split: true,
			layout: 'border',
			items: [treepanel,contentpanel]
		});
		
		treepanel.on('itemclick', function(view,record,item,index,e){
			queryid = record.raw.id;
			grid.getStore().loadPage(1);
		});
		return page1_jExtPanel1_obj;
	}
});