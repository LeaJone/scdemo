﻿/**
 * 发布统计 - 按栏目界面
 * @author lw
 */
Ext.define('system.bcms.subject.biz.SubjectStat', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'system.bcms.subject.model.Extjs_Column_SubjectStat', 
	             'system.bcms.subject.model.Extjs_Model_SubjectStat'],
	oper : Ext.create('system.bcms.subject.biz.SubjectOper'),
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
	queryObj : null,
    
	createTable : function() {
	    var me = this;
	    
	    var arrayYear = new Array();
	    var myDate = new Date();
	    var num = myDate.getFullYear();
	    for ( var i = num; i >= 2006; i--) {
	    	arrayYear. push([String(i), String(i) + '年']);
		}
	    var year = Ext.create('system.widget.FsdComboBox',{
            fieldLabel:'年份',
	        labelAlign:'right',
	    	labelWidth:60,
	        width : 150,
    	    name : 'auditing',//提交到后台的参数名
            store : arrayYear
	    });
	    var subject = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属栏目',
	    	labelAlign:'right',
	    	rootText : '所有栏目',
	    	rootId : 'FSDMAIN',
			rootVisible: true,
	    	labelWidth:80,
	        width : 250,
	        name : 'subjectid',
	        baseUrl:'B_Subject/load_AsyncSubjectTreeQuery.do',//访问路劲
	        allowBlank: false,
	        blankText: '请选择所属栏目'
	    });

		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPanel", {
			column : 'column_subjectstat', // 显示列
			model : 'model_subjectstat',
			baseUrl : 'B_Subject/stat_data.do',
			border : false,
			zAutoLoad : false,
			tbar : [year, subject, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}, '->', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			},{
				text : '下载',
				iconCls : 'articlewj',
				handler : function() {
					var y =  year.getValue();
					var sub =  subject.getValue();
					me.oper.download(grid,y,sub,me);
				}
			}]
		});
		grid.getStore().on('beforeload', function(s) {
			var y =  year.getValue();
			var sub =  subject.getValue();
			var pram = {year : y, subject : sub};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
	    year.select(String(num));
	    subject.setValue('FSDMAIN');

		return grid;
	}
});