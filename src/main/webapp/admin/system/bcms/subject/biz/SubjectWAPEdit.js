﻿/**
 * 栏目添加界面
 * @author lumingbao
 */
Ext.define('system.bcms.subject.biz.SubjectWAPEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	treepanel : null,
	oper : Ext.create('system.bcms.subject.biz.SubjectOper'),
	initComponent:function(){
	    var me = this;
	    
	    var gjid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '根级ID',
	    	name: 'baseid',
	    	value: 'FSDMOBILE'
	    });
	    
	    var sslmname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属栏目名称',
	    	name: 'parentname'
	    });
	    var sslm = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属栏目',
	    	labelAlign:'right',
	    	labelWidth:80,
	    	rootText : 'Wap栏目',
	    	rootId : 'FSDMOBILE',
	        width : 360,
	        name : 'parentid',
	        baseUrl:'B_Subject/load_AsyncSubjectTreeQuery.do',//访问路劲
	        allowBlank: false,
	        blankText: '请选择所属栏目',
	        hiddenName : 'parentname'//隐藏域Name
	    });
	    
	    var lmmc = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '栏目名称',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'title',
	    	maxLength: 20,
	    	allowBlank: false,
	    	blankText: '请输入栏目名称',
	    	maxLengthText: '栏目名称不能超过20个字符'
	    });

	    var lmtp1col = Ext.create('system.widget.FsdTextFileManage', {
	        width : 360,
	    	zName : 'imageurl1',
	    	zFieldLabel : '栏目图片',
	    	zLabelWidth : 80,
	    	zIsShowButton : true, 
	    	zIsUpButton : true,
	    	zFileType : 'image',
	    	zFileAutoName : false,
	    	zFileUpPath : me.oper.util.getSystemPath(),
	    	zManageType : 'manage'
	    });
	    
	    var lmtp2col = Ext.create('system.widget.FsdTextFileManage', {
	        width : 360,
	    	zName : 'imageurl2',
	    	zFieldLabel : '栏目图片2',
	    	zLabelWidth : 80,
	    	zIsShowButton : true, 
	    	zIsUpButton : true,
	    	zFileType : 'image',
	    	zFileAutoName : false,
	    	zFileUpPath : me.oper.util.getSystemPath(),
	    	zManageType : 'manage'
	    });
	    
	    var pxbh = new Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '排序编号',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'sort',
	    	maxLength: 3,
	    	minValue: 0,
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank: false,
	    	blankText: '请输入排序编号',
	    	maxLengthText: '排序编号不能超过3个字符'
	    });
	    
	    var iswlj = new Ext.create('system.widget.FsdCheckbox',{
	    	fieldLabel: '是否外连接',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'isaurl',
	    	inputValue : 'true'
	    });
	    iswlj.on('change', function (obj, newValue, oldValue, eOpts) {
    		wljurl.allowBlank = !newValue;
	    });
	    var wljurl = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '外连接',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'aurl',
	    	maxLength: 200,
	    	allowBlank: true,
	    	blankText: '请输入外连接'
	    });
	    
	    var isdylm = new Ext.create('system.widget.FsdCheckbox',{
	    	fieldLabel: '是否单页栏目',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'issingle',
	    	inputValue : 'true'
	    });
	    isdylm.on('change', function (obj, newValue, oldValue, eOpts) {
	    	dylm.allowBlank = !newValue;
	    });
	    var dylm = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '单页文章编号',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'singlearticleid',
	    	maxLength: 32,
	    	allowBlank: true,
	    	blankText: '请输入文章编号'
	    });
	    
	    var isfbwz = new Ext.create('system.widget.FsdCheckbox',{
	    	fieldLabel: '是否禁用发布',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'ispublish',
	    	inputValue : 'true'
	    });
	    
	    var beizhu = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'remark',
	    	maxLength: 50,
	    	maxLengthText: '备注不能超过50个字符'
	    });
	    
	    me.on('show' , function(){
	    	sslm.setText(sslmname.getValue());
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [{
                name: "id",
                xtype: "hidden"
            } , gjid, sslmname, sslm, lmmc, lmtp1col, pxbh,
            iswlj, wljurl, isdylm, dylm, isfbwz, beizhu]//lmtp2col , 
            
        });
	    
	    Ext.apply(this,{
	        width:450,
	        height:380,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
				popedomCode : 'waplmbj',
			    handler : function() {
    				me.oper.submitMobile(form, me.grid , me.treepanel);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});