/**
 * 栏目管理界面操作类
 * @author lumingbao
 */

Ext.define('system.bcms.subject.biz.SubjectOper', {

	form : null,
	util : Ext.create('system.util.util'),
	
	/**
     * 添加
     */
	addMain : function(grid , treepanel){
	    var win = Ext.create('system.bcms.subject.biz.SubjectEdit');
        win.setTitle('栏目添加');
        win.grid = grid;
        win.treepanel =  treepanel;
        win.modal = true;
        win.show();
	},
	addRelate : function(grid , treepanel){
	    var win = Ext.create('system.bcms.subject.biz.SubjectXGEdit');
        win.setTitle('相关栏目添加');
        win.grid = grid;
        win.treepanel =  treepanel;
        win.modal = true;
        win.show();
	},
	addMobile : function(grid , treepanel){
	    var win = Ext.create('system.bcms.subject.biz.SubjectWAPEdit');
        win.setTitle('Wap栏目添加');
        win.grid = grid;
        win.treepanel =  treepanel;
        win.modal = true;
        win.show();
	},
	addMobRelate : function(grid , treepanel){
	    var win = Ext.create('system.bcms.subject.biz.SubjectWAPXGEdit');
        win.setTitle('Wap相关栏目添加');
        win.grid = grid;
        win.treepanel =  treepanel;
        win.modal = true;
        win.show();
	},
	addGovernment : function(grid , treepanel){
	    var win = Ext.create('system.bcms.subject.biz.SubjectZWEdit');
        win.setTitle('政务栏目添加');
        win.grid = grid;
        win.treepanel =  treepanel;
        win.modal = true;
        win.show();
	},
	
	submitMain : function(formpanel , grid , treepanel){
		this.formSubmit(formpanel , grid, treepanel, 'B_Subject/save_Subject_qx_lmbj.do');
	},
	submitRelate : function(formpanel , grid , treepanel){
		this.formSubmit(formpanel , grid, treepanel, 'B_Subject/save_Subject_qx_xglmbj.do');
	},
	submitMobile : function(formpanel , grid , treepanel){
		this.formSubmit(formpanel , grid, treepanel, 'B_Subject/save_Subject_qx_waplmbj.do');
	},
	submitMobRelate : function(formpanel , grid , treepanel){
		this.formSubmit(formpanel , grid, treepanel, 'B_Subject/save_Subject_qx_wapxglmbj.do');
	},
	submitGovernment : function(formpanel , grid , treepanel){
		this.formSubmit(formpanel , grid, treepanel, 'B_Subject/save_Subject_qx_zwlmbj.do');
	},
	/**
     * 表单提交
     */
     formSubmit : function(formpanel , grid , treepanel, url){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit(url, formpanel.form, '正在提交数据,请稍候.....',
			function(form, action){
			    formpanel.up('window').close();
			    grid.getStore().reload();
			    treepanel.getStore().load();
				Ext.MessageBox.alert('提示', '保存成功！');
			});
        }
     },
     
     delMain : function(grid , treepanel){
  		this.del(grid, treepanel, 'B_Subject/del_Subject_qx_lmsc.do');
  	 },
  	 delRelate : function(grid , treepanel){
  		this.del(grid, treepanel, 'B_Subject/del_Subject_qx_xglmsc.do');
  	 },
  	 delMobile : function(grid , treepanel){
  		this.del(grid, treepanel, 'B_Subject/del_Subject_qx_waplmsc.do');
  	 },
  	 delMobRelate : function(grid , treepanel){
   		this.del(grid, treepanel, 'B_Subject/del_Subject_qx_wapxglmsc.do');
   	 },
  	 delGovernment : function(grid , treepanel){
  		this.del(grid, treepanel, 'B_Subject/del_Subject_qx_zwlmsc.do');
  	 },
     /**
      * 删除
      */
     del : function(grid , treepanel, url){
    	 var me = this;
 		 // 获取选中的行
 	     var data = grid.getSelectionModel().getSelection();
         if (data.length == 0) {
             Ext.MessageBox.alert('提示', '请先选中要删除的项目！');
         }else {
        	 Ext.MessageBox.show({
        		 title : '询问',
        		 msg : '您确定要删除吗?',
 			     width : 250,
 				 buttons : Ext.MessageBox.YESNO,
                 icon : Ext.MessageBox.QUESTION,
                 fn : function(btn) {
                     if (btn == 'yes') {
                    	 var dir = new Array();　
                         Ext.Array.each(data, function(items) {
                             var id = items.data.id;
                             dir.push(id);
             			});
             			var pram = {ids : dir};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest(url, param,
                        function(response, options){
                        	grid.getStore().reload();
            			    treepanel.getStore().load();
                        }, null, me.form);
                     }
                 }
             });
         }

 	},
 	
 	editorMain : function(grid , treepanel){
		this.editor(grid, treepanel, 'system.bcms.subject.biz.SubjectEdit', '栏目修改');
	},
	editorRelate : function(grid , treepanel){
		this.editor(grid, treepanel, 'system.bcms.subject.biz.SubjectXGEdit', '相关栏目修改');
	},
	editorMobile : function(grid , treepanel){
		this.editor(grid, treepanel, 'system.bcms.subject.biz.SubjectWAPEdit', 'Wap栏目修改');
	},
	editorMobRelate : function(grid , treepanel){
		this.editor(grid, treepanel, 'system.bcms.subject.biz.SubjectWAPXGEdit', 'Wap相关栏目修改');
	},
 	editorGovernment : function(grid , treepanel){
		this.editor(grid, treepanel, 'system.bcms.subject.biz.SubjectZWEdit', '政务栏目修改');
	},
 	/**
     * 修改
     */
	editor : function(grid, treepanel, url, title){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{

            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            
            var win = Ext.create(url);
            win.setTitle(title);
            win.modal = true;
            win.grid = grid;
            win.treepanel =  treepanel;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'B_Subject/load_SubjectByid.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},
 	
	viewMain : function(grid){
		this.view(grid, 'system.bcms.subject.biz.SubjectEdit', '栏目查看');
	},
	viewRelate : function(grid){
		this.view(grid, 'system.bcms.subject.biz.SubjectXGEdit', '相关栏目查看');
	},
	viewMobile : function(grid){
		this.view(grid, 'system.bcms.subject.biz.SubjectWAPEdit', 'Wap栏目查看');
	},
	viewMobRelate : function(grid){
		this.view(grid, 'system.bcms.subject.biz.SubjectWAPXGEdit', '相关栏目查看');
	},
	viewGovernment : function(grid){
		this.view(grid, 'system.bcms.subject.biz.SubjectZWEdit', '政务栏目查看');
	},
 	/**
     * 查看
     */
	view : function(grid, url, title){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{

            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            
            var win = Ext.create(url);
            win.setTitle(title);
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'B_Subject/load_SubjectByid.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},
	
	isEnabledMain : function(grid, isQT){
  		this.isEnabled(grid, isQT, 'B_Subject/enabled_Subject_qx_lmbj.do');
  	 },
  	isEnabledRelate : function(grid , isQT){
  		this.isEnabled(grid, isQT, 'B_Subject/enabled_Subject_qx_xglmbj.do');
  	 },
  	isEnabledMobile : function(grid , isQT){
  		this.isEnabled(grid, isQT, 'B_Subject/enabled_Subject_qx_waplmbj.do');
  	 },
  	isEnabledMobRelate : function(grid , isQT){
   		this.isEnabled(grid, isQT, 'B_Subject/enabled_Subject_qx_wapxglmbj.do');
   	 },
   	isEnabledGovernment : function(grid , isQT){
  		this.isEnabled(grid, isQT, 'B_Subject/enabled_Subject_qx_zwlmbj.do');
  	 },
	/**
     * 是否启用停用
     */
	isEnabled : function(grid, isQT, url){
	    var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请选择至少一条数据！');
        }else {
            Ext.MessageBox.show({
                title : '提示',
		        msg : '您确定要修改项目状态吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes') {
					    var dir = new Array();　
                        Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
			            });
             			var pram = {ids : dir, isenabled : isQT};
                        var param = {jsonData : Ext.encode(pram)};
                        me.util.ExtAjaxRequest(url, param,
                        function(response, options){
            				Ext.MessageBox.alert('提示', '项目状态修改成功！');
                        	grid.getStore().reload(); 
                        });
                    }
                }
            });
        }
	},
 	
 	/**
     * 公开栏目
     */
	gklm : function(grid , status){
 		var me = this;
		 // 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的记录！');
        }else {
       	 Ext.MessageBox.show({
       		 title : '询问',
       		 msg : '您确定要执行该操作吗?',
			    width : 250,
				buttons : Ext.MessageBox.YESNO,
                icon : Ext.MessageBox.QUESTION,
                fn : function(btn) {
                    if (btn == 'yes'){
	                   	var dir = new Array();　
	                    Ext.Array.each(data, function(items) {
	                            var id = items.data.id;
	                            dir.push(id);
	            		});
	                    var pram = {ids : dir , status : status};
	                    var param = {jsonData : Ext.encode(pram)};
	                    me.util.ExtAjaxRequest('B_Subject/gklm_Subject_qx_lmbj.do' , param ,
	                    function(response, options){
	                    	grid.getStore().reload();
	                    });
                    }
                }
            });
        }
	},
	
	/**
	 * 统计下载
	 */
	download : function(grid,year,subject,form){
		var me = this;
		me.form = form;
		//获取选中的行
		var data = grid.getSelectionModel().getSelection();
    	Ext.MessageBox.show({
   		title : '询问',
   		msg : '您确定要导出该统计报表吗?',
			width : 250,
			buttons : Ext.MessageBox.YESNO,
            icon : Ext.MessageBox.QUESTION,
            fn : function(btn) {
            	if (btn == 'yes') {
            		var dir = new Array();　
                    Ext.Array.each(data, function(items) {
                            var id = items.data.id;
                            dir.push(id);
            		});
                    var pram = {ids : dir, year : year, subject : subject};
                    var param = {jsonData : Ext.encode(pram)};
            		me.util.ExtAjaxRequest('B_Subject/download.do' , param ,
            		function(response, options){
            			var respText = Ext.JSON.decode(response.responseText);
            			window.open(respText.data.httpUrl);
            		}, null, me.form);
            	}
            }
    	});
	}
	
});