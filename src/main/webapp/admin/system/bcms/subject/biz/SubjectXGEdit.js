﻿/**
 * 栏目添加界面
 * @author lumingbao
 */
Ext.define('system.bcms.subject.biz.SubjectXGEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	treepanel : null,
	oper : Ext.create('system.bcms.subject.biz.SubjectOper'),
	initComponent:function(){
	    var me = this;
	    
	    var gjid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '根级ID',
	    	name: 'baseid',
	    	value: 'FSDRELATE'
	    });
	    
	    var sslmname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属栏目名称',
	    	name: 'parentname'
	    });
	    var sslm = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属栏目',
	    	labelAlign:'right',
	    	labelWidth:80,
	    	rootText : '相关栏目',
	    	rootId : 'FSDRELATE',
	        width : 360,
	        name : 'parentid',
	        baseUrl:'B_Subject/load_AsyncSubjectTreeQuery.do',//访问路劲
	        allowBlank: false,
	        blankText: '请选择所属栏目',
	        hiddenName : 'parentname'//隐藏域Name
	    });
	    
	    var lmmc = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '栏目名称',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'title',
	    	maxLength: 20,
	    	allowBlank: false,
	    	blankText: '请输入栏目名称',
	    	maxLengthText: '栏目名称不能超过20个字符'
	    });
	    
	    var pxbh = new Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '排序编号',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'sort',
	    	maxLength: 3,
	    	minValue: 0,
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank: false,
	    	blankText: '请输入排序编号',
	    	maxLengthText: '排序编号不能超过3个字符'
	    });
	    
	    var beizhu = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 360,
	    	name: 'remark',
	    	maxLength: 50,
	    	maxLengthText: '备注不能超过50个字符'
	    });
	    
	    me.on('show' , function(){
	    	sslm.setText(sslmname.getValue());
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [{
                name: "id",
                xtype: "hidden"
            } , gjid, sslmname, sslm, lmmc, pxbh, beizhu]
            
        });
	    
	    Ext.apply(this,{
	        width:450,
	        height:210,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
				popedomCode : 'xglmbj',
			    handler : function() {
    				me.oper.submitRelate(form, me.grid , me.treepanel);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});