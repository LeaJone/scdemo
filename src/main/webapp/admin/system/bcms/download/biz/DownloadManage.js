﻿/**
 * 角色管理界面
 * @author lw
 */
Ext.define('system.bcms.download.biz.DownloadManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*', 
	             'system.widget.FsdButton',
	             'system.bcms.download.model.Extjs_Column_B_download', 
	             'system.bcms.download.model.Extjs_Model_B_download'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [ me.createTable() ];
		me.callParent();
	},
	
    oper : Ext.create('system.bcms.download.biz.DownloadOper'),
    
	createTable : function() {
	    var me = this;

		/**
		 * 查询文本框
		 */
		var queryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入文件名',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});

		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_b_download', // 显示列
			model : 'model_b_download',
			baseUrl : 'b_download/load_pagedata.do',
			border : false,
			tbar : [{
				xtype : "fsdbutton",
				text : "添加",
				iconCls : 'page_addIcon',
				popedomCode : 'wdbj',
				handler : function() {
					me.oper.add(grid);
				}
			}, {
				xtype : "fsdbutton",
				text : "修改",
				iconCls : 'page_editIcon',
				popedomCode : 'wdbj',
				handler : function() {
					me.oper.editor(grid);
				}
			}, {
				xtype : "fsdbutton",
				text : "删除",
				iconCls : 'page_deleteIcon',
				popedomCode : 'wdsc',
				handler : function() {
				    me.oper.del(grid);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid);
				}
			}, '-', {
				xtype : "fsdbutton",
				text : "启用",
				iconCls : 'page_deleteIcon',
				popedomCode : 'wdbj',
				handler : function(button) {
				    me.oper.isEnabled(grid, true);
				}
			}, {
				xtype : "fsdbutton",
				text : "停用",
				iconCls : 'page_deleteIcon',
				popedomCode : 'wdbj',
				handler : function(button) {
				    me.oper.isEnabled(grid, false);
				}
			}, '->', queryText, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  queryText.getValue();
			var pram = {name : name};
	        var params = s.getProxy().extraParams;  
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		grid.on('cellclick', function(thisObj, td, cellIndex, record, tr, rowIndex, e, eOpts) {
			if (cellIndex == 7){
				var value = record.get('path');
				if (value != ''){
					window.open(value, record.get('name')); 
				}
			}
		});

		return grid;
	}
});