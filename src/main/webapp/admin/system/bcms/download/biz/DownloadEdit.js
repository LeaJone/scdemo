﻿/**
 * 角色添加界面
 * @author lw
 */
Ext.define('system.bcms.download.biz.DownloadEdit', {
	extend : 'Ext.window.Window',
	requires : ['system.widget.FsdTreeComboBox'],
	grid : null,
	oper : Ext.create('system.bcms.download.biz.DownloadOper'),
	initComponent:function(){
	    var me = this;

	    var companyid = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '单位编号',
	    	name: 'companyid',
	    	value : me.oper.util.getParams("company").id,
	    });
	    var type = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '类型',
	    	name: 'type',
	    	value : 'xzwjwd',
	    });
	    var typename = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '类型名称',
	    	name: 'typename',
	    	value : '文档类型',
	    });
	    
	    var name = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '文件名',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'name',
	    	maxLength: 100,
	    	allowBlank: false,
	    	blankText: '请输入文件名',
	    	maxLengthText: '文件名不能超过100个字符'
	    });
	    
	    var pathcol = Ext.create('system.widget.FsdTextFileManage', {
	        width : 340,
	    	zName : 'path',
	    	zFieldLabel : '文件路径',
	    	zLabelWidth : 60,
	    	zIsUpButton : true,
	    	zIsDownButton : true,
	    	zAllowBlank : false,
	    	zBlankText : '请选择输入文件路径',
	    	zFileType : 'file',
	    	zFileAutoName : false,
	    	zFileUpPath : me.oper.util.getFilePath(),
	    	zManageType : 'manage'
	    });
	    
	    var updated = Ext.create('system.widget.FsdDateField',{
	    	fieldLabel: '更新时间',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'updated',
	    	maxLength: 50,
	    	allowBlank: false,
	    	blankText: '请输入更新时间',
	    	maxLengthText: '更新时间不能超过50个字符'
	    });
	   
	    var remark = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '描述',
	    	labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
	    	name: 'remark',	    	
	    	maxLength: 200,
	    	allowBlank: true,
	    	blankText: '请输入描述',
	    	maxLengthText: '描述不能超过200个字符'
	    });

	    var status = Ext.create('system.widget.FsdComboBoxZD',{
            fieldLabel:'状态',
	        labelAlign:'right',
	    	labelWidth:60,
	        width : 340,
    	    name : 'status',//提交到后台的参数名
	        key : 'xzwjzt',//参数
    	    hiddenName : 'statusname'//提交隐藏域Name
	    });
	    var statusname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '状态名',
	    	name: 'statusname'
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [{
                name: "id",
                xtype: "hidden"
            }, companyid, type, typename, statusname, 
            name, pathcol, updated, remark, status]
        });
	        
	    Ext.apply(this,{
	        width:400,
	        height:250,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[form],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'wdbj',
	        	name : 'btnsave',
			    text : '　保存　',
			    iconCls : 'acceptIcon',
			    handler : function() {
    				me.oper.formSubmit(form, me.grid, true);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});