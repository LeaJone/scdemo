﻿/**
 *b_download Column
 *@author CodeSystem
 *文件名     Extjs_Column_B_download
 *Columns名  column_b_download
 */

Ext.define('column_b_download', {
    columns: [
              {
                  xtype : 'rownumberer',
                  text : 'NO',
                  sortable : true,
                  width : 30
              }
              ,
        {
            header : '文件名',
            dataIndex : 'name',
            sortable : true,
            flex : 3
        }
        ,
        {
            header : '更新时间',
            dataIndex : 'updated',
            sortable : true,
            flex : 2,
			renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value);
			}
        }
        ,
        {
            header : '描述',
            dataIndex : 'remark',
            sortable : true,
            flex : 4
        }
        ,
        {
            header : '状态',
            dataIndex : 'statusname',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '新建时间',
            dataIndex : 'adddate',
            sortable : true,
            flex : 2,
			renderer :function(value, metaData, record){
				return system.UtilStatic.formatDateTime(value);
			}
        }
        ,
        {
            header : '新建人',
            dataIndex : 'addemployeename',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '下载',
            dataIndex : 'path',
            sortable : true,
            flex : 1,
			renderer :function(value ,metaData ,record ){
				metaData.style='color:blue;text-decoration:underline;cursor:pointer;';//下划线
				value="<font style='color:blue;'>下载</font>";
				return value;
			}
        }
    ]
});
