/**
 *b_templatelist Column
 *@author CodeSystem
 *文件名     Extjs_Column_B_templatelist
 *Columns名  column_b_templatelist
 */

Ext.define('column_b_templatelist', {
    columns: [
              {
                  xtype : 'rownumberer',
                  text : 'NO',
                  sortable : true,
                  width : 30
              }
              ,
        {
            header : '名称',
            dataIndex : 'name',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '所属类型',
            dataIndex : 'templatetypename',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '显示标题',
            dataIndex : 'title',
            sortable : true,
            flex : 1
        },
        {
            header : '参数编码',
            dataIndex : 'code',
            sortable : true,
            flex : 1
        }
        ,
        {
        	header : '对应数据ID',
            dataIndex : 'belongsid',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '链接地址',
            dataIndex : 'urlpath',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '图片地址',
            dataIndex : 'imagepath',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '扩展参数1',
            dataIndex : 'parameter1',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '扩展参数2',
            dataIndex : 'parameter2',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '扩展参数3',
            dataIndex : 'parameter3',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '顺序号',
            dataIndex : 'sort',
            sortable : true,
            flex : 1
        }
        ,
        {
            header : '备注',
            dataIndex : 'remark',
            sortable : true,
            flex : 1
        }
    ]
});
