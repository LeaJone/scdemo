/**
 *b_templatelist Model
 *@author CodeSystem
 *文件名     Extjs_Model_B_templatelist
 *Model名    model_b_templatelist
 */

Ext.define('model_b_templatelist', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'templatetypeid',
            type : 'string'
        }
        ,
        {
            name : 'templatetypename',
            type : 'string'
        }
        ,
        {
        	name : 'code',
            type : 'string'
        }
        ,
        {
            name : 'name',
            type : 'string'
        }
        ,
        {
            name : 'title',
            type : 'string'
        }
        ,
        {
            name : 'belongsid',
            type : 'string'
        }
        ,
        {
            name : 'urlpath',
            type : 'string'
        }
        ,
        {
            name : 'imagepath',
            type : 'string'
        }
        ,
        {
            name : 'parameter1',
            type : 'string'
        }
        ,
        {
            name : 'parameter2',
            type : 'string'
        }
        ,
        {
            name : 'parameter3',
            type : 'string'
        }
        ,
        {
            name : 'sort',
            type : 'int'
        }
        ,
        {
            name : 'remark',
            type : 'string'
        }
    ]
});
