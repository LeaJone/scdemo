/**
 * 单位类型界面操作类
 */

Ext.define('system.bcms.templatelist.biz.TemplateListOper', {

	form : null,
	util : Ext.create('system.util.util'),

	/**
     * 表单提交
     */
     formSubmit : function(formpanel, grid){
        var me = this;
        if (formpanel.form.isValid()) {
            me.util.ExtFormSubmit('b_templatelist/save_TemplateList_qx_cslbbj.do', formpanel.form, '正在提交数据,请稍候.....',
			function(form, action){
			    formpanel.up('window').close();
			    grid.getStore().reload();
				Ext.MessageBox.alert('提示', '保存成功！');
			},
			function (form, action){
				Ext.MessageBox.alert('异常', action.result.msg);
			});
        }
     },

 	/**
     * 修改
     */
	editor : function(grid , treepanel){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if (data.length == 0) {
            Ext.MessageBox.alert('提示', '请先选中要修改的项目！');
        }else if(data.length > 1){
            Ext.MessageBox.alert('提示', '每次只能操作一个项目！');
        }else{

            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            
            var win = Ext.create('system.bcms.templatelist.biz.TemplateListEdit');
            win.setTitle('参数修改');
            win.modal = true;
            win.grid = grid;
            win.treepanel =  treepanel;
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'b_templatelist/load_TemplateListByid.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	},

 	/**
     * 查看
     */
	view : function(grid){
		var me = this;
		// 获取选中的行
	    var data = grid.getSelectionModel().getSelection();
        if(data.length > 0){
            // 先得到主键
            var id = data[0].data.id;
            //用的到的主键到后台查询，查询成功以后将相应的值放入文本框
            var win = Ext.create('system.bcms.templatelist.biz.TemplateListEdit');
            win.setTitle('参数查看');
            win.modal = true;
            win.down('button[name=btnsave]').setVisible(false);
            var pram = {id : id};
            var param = {jsonData : Ext.encode(pram)};
	        me.util.formLoad(win.down('form').getForm() , 'b_templatelist/load_TemplateListByid.do' , param , null , 
	        function(response, options, respText){
	            win.show();
	        });
        }
	}
});