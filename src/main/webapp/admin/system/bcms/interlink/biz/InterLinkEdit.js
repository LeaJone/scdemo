﻿/**
 * 友情链接添加界面
 * @author lw
 */
Ext.define('system.bcms.interlink.biz.InterLinkEdit', {
	extend : 'Ext.window.Window',
	grid : null,
	treepanel : null,
	oper : Ext.create('system.bcms.interlink.biz.InterLinkOper'),
	initComponent:function(){
	    var me = this;
	    
	    var parentname = Ext.create('Ext.form.field.Hidden',{
	    	fieldLabel: '所属链接名',
	    	name: 'parentname'
	    });
	    
	    var parentid = Ext.create('system.widget.FsdTreeComboBox',{
	    	fieldLabel : '所属链接',
	    	labelAlign:'right',
	    	labelWidth:80,
	    	rootText : '根链接',
	    	rootId : me.oper.util.getParams("company").id,
	        width : 340,
	        name : 'parentid',
	        baseUrl:'B_Interlink/load_AsyncInterlinkTree.do',//访问路劲	        
	        allowBlank: false,
	        blankText: '请选择所属链接',
	        hiddenName : 'parentname'//隐藏域Name
	    });
	    
	    var name = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '链接名称',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'name',
	    	maxLength: 100,
	    	allowBlank: false,
	    	blankText: '请输入链接名称',
	    	maxLengthText: '链接名称不能超过100个字符'
	    });
	   
	    var url = Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '链接地址',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'url',
	    	maxLength: 200,
	    	blankText: '请输入链接地址',
	    	maxLengthText: '链接地址不能超过200个字符'
	    });
	    
	    var imageurl1col = Ext.create('system.widget.FsdTextFileManage', {
	        width : 340,
	    	zName : 'imageurl1',
	    	zFieldLabel : '链接图片',
	    	zLabelWidth : 80,
	    	zIsShowButton : true, 
	    	zIsUpButton : true,
	    	zFileType : 'image',
	    	zFileAutoName : false,
	    	zFileUpPath : me.oper.util.getSystemPath(),
	    	zManageType : 'manage'
	    });
	    
	    var sort = new Ext.create('system.widget.FsdNumber',{
	    	fieldLabel: '排序编号',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'sort',
	    	maxLength: 3,
	    	minValue: 0,
	    	allowDecimals : false,//是否允许输入小数
	    	allowBlank: false,
	    	blankText: '请输入排序编号',
	    	maxLengthText: '排序编号不能超过3个字符'
	    });
	    
	    var remark = new Ext.create('system.widget.FsdTextField',{
	    	fieldLabel: '备注',
	    	labelAlign:'right',
	    	labelWidth:80,
	        width : 340,
	    	name: 'remark',
	    	maxLength: 50,
	    	maxLengthText: '备注不能超过50个字符'
	    });
	    
	    me.on('show' , function(){
	    	parentid.setText(parentname.getValue());
	    });
	    
	    var form = Ext.create('Ext.form.Panel',{
			xtype : 'form',
			defaultType : 'textfield',
			header : false,// 是否显示标题栏
			border : 0,
			padding : '20 0 0 20',
			items : [{
                name: "id",
                xtype: "hidden"
            }, parentname, parentid, name, url, imageurl1col, sort, remark]
        });
	    
	    Ext.apply(this,{
	        width:400,
	        collapsible : true,
	        bodyStyle : "background-color:#ffffff;",// 设置背景颜色 白色
	        resizable:false,// 改变大小
	        items :[ form ],
	        buttons : [{
				xtype : "fsdbutton",
				popedomCode : 'yqljbj',
	        	name : 'btnsave',
			    text : '保存',
			    iconCls : 'acceptIcon',
				popedomCode : 'yqljbj',
			    handler : function() {
    				me.oper.formSubmit(form, me.grid, me.treepanel);
			    }
		    }, {
			    text : '关闭',
			    iconCls : 'deleteIcon',
			    handler : function() {
			       me.close();
                }
		    }]
	    });
	    this.callParent(arguments);
	}
});