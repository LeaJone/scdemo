﻿/**
 * 机构管理界面
 * @author lw
 */

Ext.define('system.bcms.interlink.biz.InterLinkManage', {
	extend : 'system.widget.FsdFormPanel',
	requires : [ 'Ext.grid.*', 'Ext.toolbar.Paging', 'Ext.data.*', 'system.widget.FsdButton', 
	             'system.bcms.interlink.model.Extjs_Model_B_interlink',
	             'system.bcms.interlink.model.Extjs_Column_B_interlink'],
	header : false,
	border : 0,
	layout : 'fit',
	initComponent : function() {
		var me = this;
		me.oper.form = this;
		me.items = [me.createContent()];
		me.callParent();
	},
	
	oper : Ext.create('system.bcms.interlink.biz.InterLinkOper'),
	
	createContent : function(){
		var me = this;
		
		var queryid = me.oper.util.getParams("company").id;

		/**
		 * 查询文本框
		 */
		var queryText =  Ext.create("Ext.form.TextField" , {
			name : 'queryParam',
			emptyText : '请输入链接名称',
			enableKeyEvents : true,
			width : 130,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						grid.getStore().loadPage(1);
					}
				}
			}
		});
		
		/**
		 * 左边的树面板
		 */
		var treepanel = Ext.create("system.widget.FsdTreePanel", {
			icon : 'resources/admin/icon/chart_organisation.png',
			baseUrl : 'B_Interlink/load_AsyncInterlinkTree.do',
			title: '友情链接',
			rootText : '友情链接',
			rootId : me.oper.util.getParams("company").id,
			rootVisible: true,
			region : 'west', // 设置方位
			collapsible : true,//折叠
			width: 250,
			split: true
		});
		treepanel.on('itemclick', function(view,record,item,index,e){
			queryid = record.raw.id;
			grid.getStore().loadPage(1);
		});
		
		// 创建表格
		var grid = Ext.create("system.widget.FsdGridPagePanel", {
			column : 'column_b_interlink', // 显示列
			model: 'model_b_interlink',
			baseUrl : 'B_Interlink/load_data.do',
			tbar : [{
				xtype : "fsdbutton",
				text : "添加",
				iconCls : 'page_addIcon',
				popedomCode : 'yqljbj',
				handler : function(button) {
					me.oper.add(grid , treepanel);
				}
			}, {
				xtype : "fsdbutton",
				text : "修改",
				iconCls : 'page_editIcon',
				popedomCode : 'yqljbj',
				handler : function(button) {
					me.oper.editor(grid , treepanel);
				}
			}, {
				xtype : "fsdbutton",
				text : "删除",
				iconCls : 'page_deleteIcon',
				popedomCode : 'yqljsc',
				handler : function(button) {
				    me.oper.del(grid , treepanel);
				}
			}, {
				text : '查看',
				iconCls : 'magnifierIcon',
				handler : function() {
					me.oper.view(grid);
				}
			}, '-', {
				xtype : "fsdbutton",
				text : "审核通过",
				iconCls : 'tickIcon',
				popedomCode : 'yqljbj',
				handler : function(button) {
					me.oper.auditing(grid , 'true');
				}
			}, {
				xtype : "fsdbutton",
				text : "取消审核",
				iconCls : 'crossIcon',
				popedomCode : 'yqljbj',
				handler : function(button) {
					me.oper.auditing(grid , 'false');
				}
			}, '->', queryText, {
				text : '查询',
				iconCls : 'magnifierIcon',
				handler : function() {
					grid.getStore().loadPage(1);
				}
			}, '-', {
				text : '刷新',
				iconCls : 'tbar_synchronizeIcon',
				handler : function() {
					grid.getStore().reload();
				}
			}]			
		});
		grid.getStore().on('beforeload', function(s) {
			var name =  queryText.getValue();
			var pram = {fid : queryid , name : name};
	        var params = s.getProxy().extraParams;
	        Ext.apply(params,{jsonData : Ext.encode(pram)});
		});
		
		var contentpanel = Ext.create("Ext.panel.Panel", {
			frame:true,
			split: true,
			region: 'center',
			layout: 'fit',
			title: '机构信息',
			items: [grid]
		});

		var page1_jExtPanel1_obj = new Ext.panel.Panel({
			bodyPadding: 3,
			header : false,
			border : 0,
			split: true,
			layout: 'border',
			items: [treepanel,contentpanel]
		});
		
		return page1_jExtPanel1_obj;
	}
});