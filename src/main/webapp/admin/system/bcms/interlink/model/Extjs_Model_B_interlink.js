﻿/**
 *b_interlink Model
 *@author CodeSystem
 *文件名     Extjs_Model_B_interlink
 *Model名    model_b_interlink
 */

Ext.define('model_b_interlink', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name : 'id',
            type : 'string'
        }
        ,
        {
            name : 'companyid',
            type : 'string'
        }
        ,
        {
            name : 'parentid',
            type : 'string'
        }
        ,
        {
            name : 'parentname',
            type : 'string'
        }
        ,
        {
            name : 'name',
            type : 'string'
        }
        ,
        {
            name : 'url',
            type : 'string'
        }
        ,
        {
            name : 'imageurl1',
            type : 'string'
        }
        ,
        {
            name : 'sort',
            type : 'int'
        }
        ,
        {
            name : 'isleaf',
            type : 'string'
        }
        ,
        {
            name : 'auditing',
            type : 'string'
        }
        ,
        {
            name : 'remark',
            type : 'string'
        }
    ]
});
