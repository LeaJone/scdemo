﻿/**
 *b_interlink Column
 *@author CodeSystem
 *文件名     Extjs_Column_B_interlink
 *Columns名  column_b_interlink
 */

Ext.define('column_b_interlink', {
    columns: [
              {
                  xtype : 'rownumberer',
                  text : 'NO',
                  sortable : true,
                  width : 30
              }
              ,
		{
		    header : '链接名',
		    dataIndex : 'name'
		}
		,
		{
		    header : '所属链接',
		    dataIndex : 'parentname'
		}
		,
		{
		    header : '链接地址',
		    dataIndex : 'url'
		}
		,
		{
		    header : '链接图片',
		    dataIndex : 'imageurl1'
		}
		,
		{
		    header : '序号',
		    dataIndex : 'sort'
		}
		,
		{
		    header : '审核',
		    dataIndex : 'auditing',
			renderer :function(value ,metaData ,record ){
				if(value == 'true'){
					//metaData.style='color:blue;text-decoration:underline;cursor:pointer;';下划线
					value="<font style='color:blue;'>已审核</font>";
				}else{
					value="<font style='color:red;'>未审核</font>";
				}
				return value;
			}
		}
		,
		{
		    header : '备注',
		    dataIndex : 'remark'
		}
    ]
});
