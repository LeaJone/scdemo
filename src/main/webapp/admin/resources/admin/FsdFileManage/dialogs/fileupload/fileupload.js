	var swfu;
	var serverPathList = new Array();　//创建一个数组
	var fsd_config = new FSD_CONFIG();//配置
	window.onload = function() {
	    var api = frameElement.api, W = api.opener;
	    var jsondata = eval('('+api.data+')');
		var uploadpath = jsondata.path;
		var creatfolder = jsondata.creatfolder;
		var autoname = jsondata.autoName;
		var fileType = jsondata.fileType;
		var rootpath = "";
		if(api.get('fsdfilemanager') != null){
		    rootpath = api.get('fsdfilemanager').document.getElementById('rootpath').value;
		}
		var js=document.scripts;
		var jsPath;
		for(var i=js.length;i>0;i--){
		 if(js[i-1].src.indexOf("swfupload.js")>-1){
		   jsPath=js[i-1].src.substring(0,js[i-1].src.lastIndexOf("/")+1);
		 }
		}
		var dir = jsPath.split("/");
		var sb = "";
		for (var i = 0; i < dir.length; i++) {
			if(dir[i] != "" && dir[i] != "dialogs" && dir[i] != "swfupload"){
				sb += dir[i];
				if(i == 0){
					sb += "//";
				}else{
					sb += "/";
				}
				
			}
		}
		var file_types = "";
		if(fileType == "image"){
			file_types = "*.gif;*.jpg;*.jpeg;*.png;*.bmp;*.ico";
		}else if(fileType == "video"){
			file_types = "*.mp4;*.flv;*.wav;*.swf";
		}else{
			file_types = "*.zip;*.doc;*.docx;*.xls;*.xlsx;*.txt;*.rar;*.pdf;*.ppt;*.pptx;*.apk;";
		}
		var iswater = $("#iswater").val();
		$("#iswater").change(function(){
			iswater  = $(this).val();
			var postobj = { "iswater": iswater};
			swfu.setPostParams(postobj);
		});
		var settings = {
			flash_url : "../swfupload/swf/swfupload.swf",
			upload_url : sb + fsd_config.imageUrl +"?rootpath="+ rootpath +"&uploadpath="+uploadpath + "&creatfolder="+creatfolder+"&autoname="+autoname+"",
			file_size_limit : "1024 MB",
			file_types : file_types,
			file_types_description : "图片文件",
			file_upload_limit : 100,
			file_queue_limit : 0,
			use_query_string : true,
			post_params : {
				"iswater":iswater
			},
			custom_settings : {
				progressTarget : "fsUploadProgress",
				uploadButtonId : "btnUpload",
				cancelButtonId : "btnCancel"
			},
			debug : false,
			auto_upload : false,

			// Button settings
			button_image_url : "../swfupload/images/TestImageNoText_65x29.png",
			button_width : "75",
			button_height : "29",
			button_placeholder_id : "spanButtonPlaceHolder",
			button_text: '选择文件',
			button_text_style : ".btn-txt{font-family:'宋体'; font-size:12; color:#006699; }",
			button_text_left_padding : 7,
			button_text_top_padding : 5,

			// The event handler functions are defined in handlers.js
			file_queued_handler : fileQueued,
			file_queue_error_handler : fileQueueError,
			file_dialog_complete_handler : fileDialogComplete,
			upload_start_handler : uploadStart,
			upload_progress_handler : uploadProgress,
			upload_error_handler : uploadError,
			upload_success_handler : uploadSuccess,
			upload_complete_handler : uploadComplete,
			queue_complete_handler : queueComplete
		// Queue plugin event
		};

		swfu = new SWFUpload(settings);
	};
	
	function uploadSuccess(file, serverData) {
	    try{
		    var progress = new FileProgress(file, this.customSettings.progressTarget);
		    progress.setComplete();
		    progress.setStatus("上传成功!");
		    progress.toggleCancel(false);
		    var jsondata = eval('('+serverData+')');
            serverPathList.push(jsondata.serverurl);
	    } catch (ex) {
		    this.debug(ex);
	    }
    }
    
    function getServerPathList(){
        if(serverPathList != null){
           return serverPathList;
        }else{
            return null;
        }
    }