$(document).ready(function(){
	var api = frameElement.api, W = api.opener;
	var jsondata = eval('('+api.data+')');
	var htmlContent = jsondata.htmlContent;
	var imgUrlCut = jsondata.imgUrlCut;
	jQuery(htmlContent).appendTo("#editcontent");
	if (jQuery("#editcontent img").length == 0) {
		alert("您的文章内容中并没有插入图片。");
		api.close();
    }else{
    	var contentText = "";
        for (var i = 0; i < jQuery("#editcontent img").length; i++) {
            contentText = contentText + jQuery("#editcontent img")[i].src + ",";
        }
    	var newtitle = contentText.substring(0, contentText.length-1);
    	var result = newtitle.split(",");
    	for (var i = 0; i < result.length; i++) {
    		document.getElementById("imagecontent").innerHTML += "<img style='overflow:hidden;  margin:10px 0 0 0; padding:2px;' width='180' height='110' src=" + result[i] + " />";
    	}
    }
	
	//双击选中图片--缩略图
	$(".imagecontent img").dblclick(function(){
		var urllist = $(this).attr("src").split(imgUrlCut);
		$("#fullfilepath").val(imgUrlCut + urllist[1]);
		var api = frameElement.api, W = api.opener;
		api.close();
	});
});