<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*,java.io.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="org.json.*" %>
<%@ page import="net.sourceforge.pinyin4j.PinyinHelper" %>
<%@ page import="net.sourceforge.pinyin4j.format.HanyuPinyinCaseType" %>
<%@ page import="net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat" %>
<%@ page import="net.sourceforge.pinyin4j.format.HanyuPinyinToneType" %>

<%

String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String root = pageContext.getServletContext().getRealPath("/");

String qtrootpath = request.getParameter("qtrootpath");

String operatType = request.getParameter("operattype");//当前操作方式（1.显示 2.删除 3.新建文件夹 4.上传）
String url = "";
String oldurl = "";

if(!operatType.equals("4")){
	if(qtrootpath != null && !qtrootpath.equals("")){
		url = qtrootpath;
	}
	if(url != null && !url.equals("")){
		oldurl = url;
	}
}

if(operatType != null && !operatType.equals("") && operatType.equals("1")){
	String[] fileTypes = new String[]{".gif", ".jpg", ".jpeg", ".png", ".bmp", ".ico", ".mp4", ".flv", ".wav", ".swf", ".doc", ".docx", ".xls", ".xlsx", ".txt", ".rar", ".zip", ".pdf", ".ppt", ".pptx", ".apk"};//图片扩展名
	String showtype = request.getParameter("showtype");//显示方式
	String nowpath = request.getParameter("nowpath");//当前路径
	String previouspath = "";//上一级路劲
	if(nowpath != null && !nowpath.equals("") && !nowpath.equals(url)){
		String[] split = nowpath.split("/");
		StringBuffer sb = new StringBuffer();
		for(int i = 0 ; i < split.length ; i++){
			if(i != (split.length-1)){
				sb.append(split[i] + "/");
				
			}
		}
		previouspath = sb.toString();
		url = nowpath;
	}


	File file = new File(root + url);
	if(file == null || !file.isDirectory()){
		file.mkdirs();
	}else{
		List<Hashtable> fileList = new ArrayList<Hashtable>();
		JSONObject result = new JSONObject();
		String[] filelist = file.list();
		//得到所有的文件夹
	    for (int i = 0; i < filelist.length; i++) {
			File readfile = new File(root + url + "\\" + filelist[i]);
			if (readfile.isDirectory()) {
				Hashtable<String, Object> hash = new Hashtable<String, Object>();
				hash.put("filetype", "1");
				hash.put("filename", readfile.getName());
				hash.put("filesize", readfile.length());
				hash.put("filepath", url);
				hash.put("shortfilepath", url.replaceAll(oldurl, ""));
				hash.put("imagepath", basePath + "admin/resources/admin/FsdFileManage/skins/me/img/folder-64.gif");
				hash.put("datetime",  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(readfile.lastModified()));
				fileList.add(hash);
			}
	    }
	  //得到所有的文件
	    for (int i = 0; i < filelist.length; i++) {
			File readfile = new File(root + url + "\\" + filelist[i]);
			if (!readfile.isDirectory()) {
				
				String sname = readfile.getName().substring(readfile.getName().lastIndexOf("."));//获取后缀名
				for(int j = 0; j < fileTypes.length ; j++){
					if(sname.equals(fileTypes[j])){
						Hashtable<String, Object> hash = new Hashtable<String, Object>();
						hash.put("filetype", "2");
						hash.put("filename", readfile.getName());
						hash.put("filesize", readfile.length());
						hash.put("filepath", url + readfile.getName());
						hash.put("shortfilepath", (url + readfile.getName()).replaceAll(oldurl, ""));
						if(showtype.equals("1")){
							if(sname.equals(".gif") || sname.equals(".jpg") || sname.equals(".jpeg") || sname.equals(".png") || sname.equals(".bmp") || sname.equals(".ico")){
								hash.put("imagepath", basePath + url + readfile.getName());
							}else{
								hash.put("imagepath", basePath + "admin/resources/admin/FsdFileManage/skins/me/img/021.png");
							}
						}else if(showtype.equals("2")){
							hash.put("imagepath", basePath + "admin/resources/admin/FsdFileManage/skins/me/img/file-16.gif");
						}
						hash.put("datetime",  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(readfile.lastModified()));
						fileList.add(hash);
						break;
					}
				}
				
			}
	    }
	    result.put("rootpath", url);
	    result.put("previouspath", previouspath);
	    result.put("filelist", fileList);
		response.setContentType("application/json; charset=UTF-8");
		out.println(result.toString());
	}
}else if(operatType != null && !operatType.equals("") && operatType.equals("2")){
	JSONObject result = new JSONObject();
	String deletepath = root + request.getParameter("nowpath");
	File file = new File(deletepath);
	if(file != null && file.isDirectory()){
		//如果是文件夹
		if(file.listFiles().length > 0){
			result.put("result", "error");
			result.put("message", "该目录不为空，不能直接是删除！");
		}else{
			file.delete();
			result.put("result", "success");
			result.put("message", "删除成功！");
		}
	}else if(file != null && !file.isDirectory()){
		//如果是文件
		file.delete();
		result.put("result", "success");
		result.put("message", "删除成功！");
	}
	
	response.setContentType("application/json; charset=UTF-8");
	out.println(result.toString());
}else if(operatType != null && !operatType.equals("") && operatType.equals("3")){
	//新建文件夹
	JSONObject result = new JSONObject();
	String nowpath = request.getParameter("nowpath");
	String rootpath = request.getParameter("rootpath");
	String newfolder = request.getParameter("newfolder");
	newfolder = java.net.URLDecoder.decode(newfolder,"UTF-8");
	if(nowpath == null || nowpath.equals("")){
		if(rootpath != null && !rootpath.equals("")){
			nowpath = rootpath;
		}else{
			result.put("result", "error");
			result.put("message", "路径错误！");
		}
	}
	char[] newfolder_dir = newfolder.toCharArray();
	String newname = "";
	for(int i = 0 ; i < newfolder_dir.length ; i++){
		String zifu = HanyuToPinyin(String.valueOf(newfolder_dir[i]));
		if(zifu == null || zifu.equals("")){
			newname+= String.valueOf(newfolder_dir[i]);
		}else{
			newname+= zifu.toCharArray()[0];
		}
		
		
	}
	String fullpathh = root + nowpath + newname;
	File file = new File(fullpathh);

	if(file.exists()){
		result.put("result", "error");
		result.put("message", "该目录已存在！");
	}else{
		file.mkdir();
		result.put("result", "success");
		result.put("message", "目录创建成功！");
	}
	response.setContentType("application/json; charset=UTF-8");
	out.println(result.toString());
}
%>
<%!
private String HanyuToPinyin(String name){
	String pinyinName = "";
    char[] nameChar = name.toCharArray();
    HanyuPinyinOutputFormat defaultFormat =  new HanyuPinyinOutputFormat();
    defaultFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
    defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
    for (int i = 0; i < nameChar.length; i++) {
        if (nameChar[i] > 128) {
            try {
                pinyinName += PinyinHelper.toHanyuPinyinStringArray
                                       (nameChar[i], defaultFormat)[0];
            } catch (Exception e) {
                e.printStackTrace();
            }
        } 
    }
    return pinyinName;
}
%>