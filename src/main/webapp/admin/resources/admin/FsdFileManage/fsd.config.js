function FSD_CONFIG(){
    //文件管理根目录配置
    this.fileManagerRoot = "";
    //文件上传配置区
    //this.imageUrl = "net/file_upload_json.ashx";
    this.imageUrl = "jsp/file_upload_json.jsp";
    //文件管理配置区
    //this.imageManagerUrl = "net/file_manager_json.ashx";
    this.imageManagerUrl = "jsp/file_manager_json.jsp";
    //单独调用上传控件是否生成时间文件夹 boolean值  生成时间文件夹格式（2014/2014-05-05）
    //this.fileUploadCreateDateFolder = false ;
    //文件管理中调用上传控件是否生成时间文件夹 boolean值  生成时间文件夹格式（2014/2014-05-05）
    //this.fileManagerUploadCreateDateFolder = false ;
    
    //图片预览窗口配置
    this.imageViewWidth = "800px";
    this.imageViewHeight = "500px";
    
    //视频预览窗口配置
    this.videoViewWidth = "800px";
    this.videoViewHeight = "500px";
    
    //用于截取图片获取时的相对路径地址
    this.imgUrlCut = "uploadfiles/";
}