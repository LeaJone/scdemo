﻿<%@ webhandler Language="C#" class="FsdUpload" %>

using System;
using System.Collections;
using System.Web;
using System.IO;
using System.Globalization;
using LitJson;
using System.Web.SessionState;//第一步：导入此命名空间

public class FsdUpload : IHttpHandler, IRequiresSessionState
{

	public void ProcessRequest(HttpContext context)
	{
        
        string root = HttpRuntime.AppDomainAppPath.ToString();

        int cont = context.Request.Files.Count;
        if (cont == 0)
        {
            showError("请选择文件。", context);
            return;
        }

        string uploadpath = context.Request.QueryString["uploadpath"];
        if(uploadpath == null || "".Equals(uploadpath)){
            string rootpath = context.Request.QueryString["rootpath"];
            if(rootpath == null || "".Equals(rootpath)){
                showError("路径错误。", context);
                return;
            }else{
                uploadpath = rootpath;
            }
            
        }
        
        string savePath = root + uploadpath;

        if (!Directory.Exists(savePath))
        {
            showError("路径错误。", context);
            return;
        }

        Boolean creatfolder = Convert.ToBoolean(context.Request.QueryString["creatfolder"]);
        if (creatfolder)
        {
                string year = DateTime.Now.Year.ToString();
                string day = DateTime.Now.ToString("yyyy-MM-dd");
                if (!Directory.Exists(savePath + year + "/"))
                {
                    Directory.CreateDirectory(savePath + year);
                }

                if (!Directory.Exists(savePath + year + "/" + day + "/"))
                {
                    Directory.CreateDirectory(savePath + year + "/" + day + "/");
                }

                savePath = savePath + year + "/" + day + "/";
                uploadpath = uploadpath + year + "/" + day + "/";
        }
        
        

        HttpPostedFile imgFile = context.Request.Files["Filedata"];
        if (imgFile == null)
        {
            showError("请选择文件。" , context);
            return; 
        }
        string fileAllName = imgFile.FileName;
        string fileName = Path.GetFileNameWithoutExtension(fileAllName);
        string fileExt = Path.GetExtension(fileAllName).ToLower();
        string newname = "";
        string autoname = context.Request.QueryString["autoname"];
        if (autoname != null && !"".Equals(autoname) && "true".Equals(autoname))
        {
            DateTime time=DateTime.Now;
            newname = time.ToString("yyyyMMddhhmmssfff");
        }else{
            newname = DataProcessing.Public.Chinese2Spell.GetFirstPinYin(fileName.Trim());
        }
        imgFile.SaveAs(savePath + newname + fileExt);
        Hashtable hash = new Hashtable();
        hash["error"] = "0";
        hash["serverurl"] = uploadpath + newname + fileExt;
        hash["message"] = "上传成功";
        context.Response.AddHeader("Content-Type", "text/html; charset=UTF-8");
        context.Response.Write(JsonMapper.ToJson(hash));
        context.Response.End();

	}

    private void showError(string message, HttpContext context)
    {
        Hashtable hash = new Hashtable();
        hash["error"] = 1;
        hash["message"] = message;
        context.Response.AddHeader("Content-Type", "text/html; charset=UTF-8");
        context.Response.Write(JsonMapper.ToJson(hash));
        context.Response.End();
    }

	public bool IsReusable
	{
		get
		{
			return true;
		}
	}
}
