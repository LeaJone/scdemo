package com.fsd.core.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
/**
 *  Model基类
 */
@MappedSuperclass
public class BaseModel implements Serializable{

	private static final long serialVersionUID = 1L;
	
	Object object = null;
	List<Object> list = null;
	
	@Transient
	public Object getObject() {
		return object;
	}
	public void setObject(Object object) {
		this.object = object;
	}
	
	@Transient
	public List<Object> getList() {
		if (this.list == null){
			this.list = new ArrayList<Object>();
		}
		return list;
	}
	public void setList(List<Object> list) {
		this.list = list;
	}
}