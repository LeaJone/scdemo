package com.fsd.core.util;

import java.math.BigDecimal;

/**
 * 数学计算工具类
 * @author fsd
 */
public class MathsUtil {
	
	/**
	 * double保留几位小数点，四舍五入
	 * @param value double值
	 * @param bit 保留位数
	 * @return
	 */
	public static double formatDouble(double value, int bit){
		BigDecimal b = new BigDecimal(value);
		return b.setScale(bit, BigDecimal.ROUND_HALF_UP).doubleValue();
	}
	
	/**
	 * 得到最大值
	 * @param array
	 * @return
	 */
	public static int findMax(int[] arr){
		int max = Integer.MIN_VALUE;  
        for(int i = 0; i < arr.length; i++) {
            if(arr[i] > max)
                max = arr[i];
        }
        return max;
	}
	
	/**
	 * 得到最小值
	 * @param array
	 * @return
	 */
	public static int findMin(int[] arr){
		int min = Integer.MAX_VALUE;
        for(int i = 0; i < arr.length; i++) {
            if(arr[i] < min)
                min = arr[i];
        }
        return min;
	}
	
	/**
	 * 求和
	 * @param array
	 * @return
	 */
	public static int sum(int[] array){
		int sum = 0;
		for (int i = 0; i < array.length; i++) {
			sum = sum + array[i];
		}
		return sum;
	}
}
