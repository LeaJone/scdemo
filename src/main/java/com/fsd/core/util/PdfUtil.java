package com.fsd.core.util;

import java.io.*;
import java.util.*;
import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.*;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.usermodel.Range;

/**
 * PDF工具类
 */
public class PdfUtil {
    /**
     * 通过PDF模板创建PDF文件
     * @param data
     * @param templatePath
     * @param outPath
     */
    public static void  createPdfByTemplate(Map<String, Object> data, String templatePath, String outPath) throws Exception{
        FileOutputStream  out = new FileOutputStream(outPath);// 输出流
        PdfReader reader = new PdfReader(templatePath);// 读取pdf模板
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        PdfStamper stamper = new PdfStamper(reader, bos);
        BaseFont bf = BaseFont.createFont("c://WINDOWS//Fonts//simsun.TTC,0", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
        ArrayList<BaseFont> fontList = new ArrayList<>();
        fontList.add(bf);
        AcroFields form = stamper.getAcroFields();
        form.setSubstitutionFonts(fontList);
        for(String key : data.keySet()){
            form.setField(key, data.get(key).toString());
        }
//            //添加水印
//            String writeContent = "编号：123456";
//            PdfContentByte contentOver = stamper.getOverContent(1);
//            contentOver.beginText();
//            PdfGState gs = new PdfGState();
//            gs.setFillOpacity(0.8f);// 设置透明度
//            contentOver.setGState(gs);
//            BaseFont writeFront = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H",BaseFont.EMBEDDED);//字体实例，影响中文字体显示
//            contentOver.setFontAndSize(writeFront, 25);//字体样式和大小
//            contentOver.setColorFill(BaseColor.LIGHT_GRAY);
//            contentOver.showTextAligned(Element.ALIGN_LEFT, writeContent, 260, 780,0);//左边距、下边距
//            contentOver.endText();

        stamper.setFormFlattening(true);// 如果为false那么生成的PDF文件还能编辑，一定要设为true
        stamper.close();

        Document doc = new Document();
        PdfCopy copy = new PdfCopy(doc, out);
        doc.open();

        for (int i = 1; i <= reader.getNumberOfPages(); i++) {
            PdfImportedPage importPage = copy.getImportedPage(new PdfReader(bos.toByteArray()), i);
            copy.addPage(importPage);
        }
        doc.close();
        reader.close();
        out.close();
    }

    /**
     * 通过Word模板创建Word文件
     * @param data
     * @param templatePath
     * @param outPath
     * @throws Exception
     */
    public static void createWordByTemplate(Map<String, Object> data, String templatePath, String outPath) throws Exception{
        File file = new File(templatePath);
        InputStream inputStream = new FileInputStream(file);
        HWPFDocument document  = new HWPFDocument(inputStream);
        // 读取文本内容
        Range bodyRange = document.getRange();
        // 替换内容
        for (Map.Entry<String, Object> entry : data.entrySet()) {
            bodyRange.replaceText("${" + entry.getKey() + "}", entry.getValue().toString());
        }

        //导出到文件
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        document.write(byteArrayOutputStream);
        OutputStream outputStream = new FileOutputStream(outPath);
        outputStream.write(byteArrayOutputStream.toByteArray());
        outputStream.close();
    }
}