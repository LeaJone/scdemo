package com.fsd.core.util;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import org.icepdf.core.exceptions.PDFException;
import org.icepdf.core.exceptions.PDFSecurityException;
import org.icepdf.core.pobjects.Document;
import org.icepdf.core.pobjects.Page;
import org.icepdf.core.util.GraphicsRenderingHints;

/**
 * @Description
 * @Author 芦明宝
 * @Date 2019-05-10 11:33
 * @Version 1.0
 */
public class PDFtoPicture {
    public static void main(String[] args) throws PDFException, PDFSecurityException, IOException {
        try {
            String url = "C:\\Users\\lumin\\Desktop\\专家讲堂\\专家讲堂";
            File file = new File(url);
            String[] filelist = file.list();
            for (int i = 0; i < filelist.length; i++) {
                String fileName = filelist[i];
                System.out.println("正在处理：" + fileName);
                String pdfPath = url + "\\" + fileName;
                String imgPath = pdfPath.replaceAll(".pdf", ".jpg");
                pdf2multiImage(pdfPath, imgPath, 1000);
            }
            System.out.println("处理结束");
        }catch (Exception e){
            e.printStackTrace();
        }

    }

        /**
   *
   * @discription
   * @author zxl
   * @created 2018年9月21日 下午3:55:34
   * @param pdfFile
   *      pdf地址
   * @param outpath
   *      图片地址
   * @param maxPage
   *      最大页数
   * @throws PDFException
   * @throws PDFSecurityException
   * @throws IOException
   */
        public static void pdf2multiImage(String pdfFile, String outpath, int maxPage) throws PDFException, PDFSecurityException, IOException {
            InputStream is = new FileInputStream(pdfFile);
            List<BufferedImage> piclist = new ArrayList<BufferedImage>();
            Document document = new Document();
            document.setFile(pdfFile);
            float scale = 2.5f; // 缩放比例
            float rotation = 0f; // 旋转角度
            for (int i = 0; i < document.getNumberOfPages(); i++) {
                BufferedImage image = (BufferedImage) document.getPageImage(i, GraphicsRenderingHints.SCREEN, Page.BOUNDARY_CROPBOX, rotation, scale);
                piclist.add(image);
            }
            document.dispose();
            yPic(piclist, outpath);
            is.close();
        }

        /**
   *
   * @discription 因每个pdf页的宽度和长度不一样，故在此改动
   * @author zxl
   * @created 2018年9月21日 下午3:57:53
   * @param piclist
   * @param outPath
   * @throws IOException
   */
        public static void yPic(List<BufferedImage> piclist, String outPath) throws IOException {// 纵向处理图片
            if (piclist == null || piclist.size() <= 0) {
                System.out.println("图片数组为空");
                return;
            }

            int height = 0, // 总高度
            _width = 0, // 临时宽度
            maxWidth = 0, // 最大宽度
            _height = 0, // 临时的高度 , 或保存偏移高度
            __height = 0, // 临时的高度，主要保存每个高度
            picNum = piclist.size();// 图片的数量
            int[] heightArray = new int[picNum]; // 保存每个文件的高度
            int[] widthArray = new int[picNum]; // 保存每个文件的宽度
            BufferedImage buffer = null; // 保存图片流
            List<int[]> imgRGB = new ArrayList<int[]>(); // 保存所有的图片的RGB
            int[] _imgRGB; // 保存一张图片中的RGB数据
            for (int i = 0; i < picNum; i++) {
                buffer = piclist.get(i);
               if (buffer.getWidth() > maxWidth) {
                    // 获取最大宽度
                    maxWidth = buffer.getWidth();
               }
            }
              for (int i = 0; i < picNum; i++) {
                buffer = piclist.get(i);
                heightArray[i] = _height = buffer.getHeight();// 图片高度
                widthArray[i] = _width = buffer.getWidth();// 图片宽度
                height += _height; // 获取总高度
                _imgRGB = new int[_width * _height];// 从图片中读取RGB
                _imgRGB = buffer.getRGB(0, 0, _width, _height, _imgRGB, 0, _width);
                imgRGB.add(_imgRGB);
              }
            _height = 0; // 设置偏移高度为0
          // 生成新图片
          BufferedImage imageResult = new BufferedImage(_width, height, BufferedImage.TYPE_INT_RGB);
          for (int i = 0; i < picNum; i++) {
           __height = heightArray[i];
           _width = widthArray[i];
           // if (i != 0) _height += heightArray[i-1]; // 计算偏移高度 ，若高度一致，则把此行打开
           if (i != 0)
            _height += heightArray[i - 1]; // 计算偏移高度 ，因高度不一致，所以i-1
           // imageResult.setRGB(0, _height, widthArray[i], __height, imgRGB.get(i), 0, widthArray[i]); // 写入流中
           // 居中，前两个参数为起始的宽度、高度
           imageResult.setRGB((maxWidth - _width) / 2, _height, _width, __height, imgRGB.get(i), 0, _width); // 写入流中
          }
          File outFile = new File(outPath);
          ImageIO.write(imageResult, "jpg", outFile);// 写图片
 }
}
