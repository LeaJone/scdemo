package com.fsd.core.util;

import org.apache.shiro.crypto.hash.Md5Hash;

public class EncryptUtils {
	
	/**
	 * MD5加密
	 * @param source
	 * @return
	 */
	public static final String encryptMD5(String source) {
		if (source == null) {
			source = "";
		}
		Md5Hash md5 = new Md5Hash(source);
		return md5.toString();
	}
	
	/** 
     * 对密码进行md5加密,并返回密文和salt，包含在User对象中 
     * @param username 用户名 
     * @param password 密码 
     * @return 密文和salt 
     */ 
    public static String md5Password(String password, String salt){
    	return new Md5Hash(password,salt,2).toHex();
    }
}
