package com.fsd.core.util;

public class Config {

	/**
	 * 企业默认ID
	 */
	public static final String COMPANYID = "FSDCOMPANY";
	
	//----------------------------缓存数据参数------------------------
	/**
	 * 标记登录信息
	 */
	public static final String LOGININFO = "LoginInfo";

	/**
	 * 标记系统菜单的Map集合
	 */
	public static final String MENUMAP = "MenuMap";
	/**
	 * 标记系统参数的Map集合
	 */
	public static final String PARAMETERMAP = "ParameteMap";
	/**
	 * 标记部门的Map集合
	 */
	public static final String BRANCHMAP = "BranchMap";
	/**
	 * 标记部门的Map集合
	 */
	public static final String MAJORMAP = "MajorMap";
	/**
	 * 标记部门的Map集合
	 */
	public static final String DOMAINMAP = "DomainMap";
	/**
	 * 标记部门的Map集合
	 */
	public static final String PROFESSORMAP = "ProfessorMap";
	/**
	 * 标记栏目的Map集合
	 */
	public static final String SUBJECTMAP = "SubjectMap";
	/**
	 * 标记题库的Map集合
	 */
	public static final String BANKINFOMAP = "BankInfoMap";
	/**
	 * 标记验证码
	 */
	public static final String CAPTCHA = "captcha";
	
	/**
	 * 微信标记自动回复的Map集合
	 */
	public static final String E_MECHATMAP = "MeChatMap";
	
	/**
	 * 微信标记自动回复的Map集合
	 */
	public static final String E_AUTOREPLYMAP = "AutoReplyMap";
	
	/**
	 * 微信标记自动回复的Map集合
	 */
	public static final String E_SYSTEMCODEMAP = "SystemCodeMap";

	/**
	 * 微信推送模板的Map集合
	 */
	public static final String E_PUSHTEMPLATEMAP = "PushTemplateMap";
	
	//----------------------------系统参数------------------------
	/**
	 * 系统项目加载选择菜单组
	 */
	public static final String SITEMENU = "FSDSITEMENU";
	/**
	 * 网站前台浏览器标签显示标题名称
	 */
	public static final String SITETITLE = "FSDSITETITLE";
	/**
	 * 网站管理平台浏览器标签显示标题名称
	 */
	public static final String SITEADMINTITLE = "FSDSITEADMIN";
	/**
	 * 网站管理平台顶部显示图标和标题名称
	 */
	public static final String SITELOGOTITLE = "FSDSITELOGOTITLE";
	/**
	 * 网站前台和后台浏览器Tab标签上显示图标
	 */
	public static final String SITELOGOICON = "FSDSITELOGOICON";
	/**
	 * 网站前台访问地址根路径
	 */
	public static final String SITEURL = "FSDSITEURL";
	/**
	 * 网站管理平台访问地址根路径
	 */
	public static final String SITEMANAGEURL = "FSDSITEMANAGEURL";
	/**
	 * 网站前台内容页访问地址根路径
	 */
	public static final String SITEARTICLEURL = "FSDSITEARTICLEURL";
	/**
	 * 网站备份文件下载地址根路径
	 */
	public static final String FSDSITEDOWNLOADURL = "FSDSITEDOWNLOADURL";
	/**
	 * 密码过期时间
	 */
	public static final String PWDTIME = "FSDPWDTIME";
	/**
	 * 当前站点是否开启或关闭
	 */
	public static final String SITEENABLE = "FSDSITEENABLE";
	/**
	 * 任务计划是否开启或关闭
	 */
	public static final String JOBENABLE = "FSDJOBENABLE";
	
	
	//----------------------------系统日志类型------------------------
	/**
	 * 登录类型
	 */
	public static final String LOGTYPEDL = "xtrzdl";
	public static final String LOGTYPEDLNAME = "登录";
	/**
	 * 密码类型
	 */
	public static final String LOGTYPEMM = "xtrzmm";
	public static final String LOGTYPEMMNAME = "密码";
	/**
	 * 登录失败类型
	 */
	public static final String LOGTYPEDLSB = "xtrzdlsb";
	public static final String LOGTYPEDLSBNAME = "登录失败";
	/**
	 * 保存类型
	 */
	public static final String LOGTYPEBC = "xtrzbc";
	public static final String LOGTYPEBCNAME = "保存";
	/**
	 * 删除类型
	 */
	public static final String LOGTYPESC = "xtrzsc";
	public static final String LOGTYPESCNAME = "删除";
	/**
	 * 其他类型
	 */
	public static final String LOGTYPEQT = "xtrzqt";
	public static final String LOGTYPEQTNAME = "其他";
	/**
	 * 安全类型
	 */
	public static final String LOGTYPEAQ = "xtrzaq";
	public static final String LOGTYPEAQNAME = "安全";
	/**
	 * 敏感设置类型
	 */
	public static final String LOGTYPEMGSZ = "xtrzmgsz";
	public static final String LOGTYPEMGSZNAME = "敏感标志设置";
	/**
	 * 敏感保存类型
	 */
	public static final String LOGTYPEMGBC = "xtrzmgbc";
	public static final String LOGTYPEMGBCNAME = "敏感信息保存";
	/**
	 * 敏感查询类型
	 */
	public static final String LOGTYPEMGCX = "xtrzmgcx";
	public static final String LOGTYPEMGCXNAME = "敏感信息查询";
	
	

	//----------------------------权限类型参数------------------------
	/**
	 * 标记登录用户菜单权限
	 */
	public static final String POPEDOMMEUN = "qxlxcd";
	/**
	 * 标记登录用户栏目权限
	 */
	public static final String POPEDOMSUBJECT = "qxlxlm";
	/**
	 * 标记登录用户Wap栏目权限
	 */
	public static final String POPEDOMSUBJECTW = "qxlxlmw";
	/**
	 * 标记登录用户部门权限
	 */
	public static final String POPEDOMBRANCH = "qxlxjg";
	
	
	/**
	 * 权限类型为用户权限
	 */
	public static final String POPEDOMUSER = "qxlxyh";
	
	
	/**
	 * 权限-机构管理员
	 */
	public static final String POPEDOMjggly = "jggly";
	/**
	 * 权限-工作流机构审批
	 */
	public static final String POPEDOMgzljgsp = "gzljgsp";
	

	//----------------------------文章相关类型参数------------------------
	/**
	 * 文章相关类型栏目
	 */
	public static final String ARTICLERELATESUBJECT = "wzgllm";
	/**
	 * 文章相关类型相关栏目
	 */
	public static final String ARTICLERELATESUBJECTXG = "wzglxg";
	/**
	 * 文章相关类型Wap栏目
	 */
	public static final String ARTICLERELATESUBJECTWAP = "wzglwap";
	/**
	 * 文章相关类型Wap相关栏目
	 */
	public static final String ARTICLERELATESUBJECTWAPXG = "wzglwapxg";
	
	
	//----------------------------关键字类型参数------------------------
	/**
	 * 关键字敏感类型
	 */
	public static final String WORDTYPEBAD = "FSDBADWORD";
	/**
	 * 关键字关键类型
	 */
	public static final String WORDTYPEKEY = "FSDKEYWORD";
	

	//----------------------------微信类型参数------------------------
	/**
	 * 消息-系统类型
	 */
	public static final String WECHATMESSAGEXT = "e_wxxxxt";
	/**
	 * 消息-菜单类型
	 */
	public static final String WECHATMESSAGECD = "e_wxxxcd";
	/**
	 * 消息-关键字类型
	 */
	public static final String WECHATMESSAGEGJ = "e_wxxxgj";
	
	/**
	 * 回复-列表
	 */
	public static final String WECHATREPLYLB = "e_wxhflb";
	/**
	 * 回复-内容文本
	 */
	public static final String WECHATREPLYNR = "e_wxhfnr";
	
	/**
	 * 处理集合-系统类型
	 */
	public static final String WECHATPROCESSXT = "cljhxt";
	/**
	 * 处理集合-菜单类型
	 */
	public static final String WECHATPROCESSCD = "cljhcd";
	/**
	 * 处理集合-关键字全匹配类型
	 */
	public static final String WECHATPROCESSQP = "cljhqp";
	/**
	 * 处理集合-关键字模糊类型
	 */
	public static final String WECHATPROCESSMH = "cljhmh";
	
	//----------------------------微信系统消息类型------------------------
	/**
	 * 系统消息类型-默认
	 */
	public static final String WECHATMEGDEFAULT = "WECHATMEGDEFAULT";
	/**
	 * 系统消息类型-订阅
	 */
	public static final String WECHATMEGSUBSCRIBE = "WECHATMEGSUBSCRIBE";
}
