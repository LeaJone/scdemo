package com.fsd.core.util;

import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;
import com.fsd.core.common.BusinessException;

/** 
 * 过滤操作
 */
public class FilterUtil {
	
	/**
	 * 正则表达式（剔除特殊符号，只包含大小写字母，汉字，数字组合）
	 * @param value
	 * @throws BusinessException
	 */
	public static void matchPattern(String value) throws BusinessException{
		if(value != null && !value.equals("")) {
			boolean match = Pattern.matches("^[a-zA-Z0-9\u4e00-\u9fa5]*$", value);
			if(!match)
				match = Pattern.matches("^\\d{4}-\\d{1,2}-\\d{1,2}$", value);
				if(!match)
					throw new BusinessException("参数包含非法字符！");
		}
	}
	
    /**
     * 检查包换SqlHtml标记
     * @param str
     * @return
     */
    public static void checkSqlHtml(String str) throws BusinessException{
    	matchPattern(str);
    }
    
    public static void checkHtml(String str)  throws BusinessException{
    	String safe = cleanXSS(str);
    	if(!safe.equals(str)){
    		throw new BusinessException("参数包含非法字符！");
    	}
    }
    
    public static void checkSql(String str)  throws BusinessException{
    	String safe = StringEscapeUtils.escapeSql(str);
    	if(!safe.equals(str)){
    		throw new BusinessException("参数包含非法字符！");
    	}
    }
    
    private static String cleanXSS(String value) {
        //You'll need to remove the spaces from the html entities below
		value = value.replaceAll("<", "& lt;").replaceAll(">", "& gt;");
		value = value.replaceAll("\\(", "& #40;").replaceAll("\\)", "& #41;");
		value = value.replaceAll("'", "& #39;");
		value = value.replaceAll("eval\\((.*)\\)", "");
		value = value.replaceAll("[\\\"\\\'][\\s]*javascript:(.*)[\\\"\\\']", "\"\"");
		value = value.replaceAll("script", "");
		return value;
    }
    
    /**
     * 删除html标记
     * @param Htmlstring
     * @return
     */
    public static String deleteHtml(String Htmlstring)
    {
    	Htmlstring = Htmlstring.replaceAll("(?i)<script[^>]*?>.*?</script>", "");
        //删除HTML   
        Htmlstring = Htmlstring.replaceAll("(?i)<(.[^>]*)>", "");
        Htmlstring = Htmlstring.replaceAll("(?i)([\\r\n])[\\s]+", "");
        Htmlstring = Htmlstring.replaceAll("(?i)-->", "");
        Htmlstring = Htmlstring.replaceAll("(?i)<!--.*", "");
        Htmlstring = Htmlstring.replaceAll("(?i)&(quot|#34);", "\"");
        Htmlstring = Htmlstring.replaceAll("(?i)&(amp|#38);", "&");
        Htmlstring = Htmlstring.replaceAll("(?i)&(lt|#60);", "<");
        Htmlstring = Htmlstring.replaceAll("(?i)&(gt|#62);", ">");
        //Htmlstring = Htmlstring.replaceAll("(?i)&(nbsp|#160);", " ");
        Htmlstring = Htmlstring.replaceAll("(?i)&(iexcl|#161);", "\\xa1");
        Htmlstring = Htmlstring.replaceAll("(?i)&(cent|#162);", "\\xa2");
        Htmlstring = Htmlstring.replaceAll("(?i)&(pound|#163);", "\\xa3");
        Htmlstring = Htmlstring.replaceAll("(?i)&(copy|#169);", "\\xa9");
        Htmlstring = Htmlstring.replaceAll("(?i)&#(\\d+);", "");
        Htmlstring = Htmlstring.replaceAll("(?i)<img[^>]*>;", "");
        //Htmlstring.replaceAll("<", "");
        //Htmlstring.replaceAll(">", "");
        //Htmlstring.replaceAll("\r\n", "");
        return Htmlstring;
    }

    /**
     * 替换文章中的html标记为换行符
     * @param Htmlstring
     * @return
     */
    public static String deleteAriticleHtml(String Htmlstring)
    {
        //替换除<p>之外的所有标签，并将<p>改为<br/>
        Htmlstring = Htmlstring.replaceAll("(?i)<p (.[^>]*)>", "<P>");
        Htmlstring = Htmlstring.replaceAll("(?i)<[^p](.[^>]*)>", "");
        Htmlstring = Htmlstring.replaceAll("(?i)<p>", "<BR/>　　");
        return Htmlstring;
    }

    /**
     * 删除文章中的html标记
     * @param Htmlstring
     * @return
     */
    public static String deleteAllAriticleHtml(String Htmlstring)
    {
        //替换除<p>之外的所有标签，并将<p>改为<br/>
        Htmlstring = Htmlstring.replaceAll("(?i)<p (.[^>]*)>", "");
        Htmlstring = Htmlstring.replaceAll("(?i)<[^p](.[^>]*)>", "");
        Htmlstring = Htmlstring.replaceAll("(?i)<p>", "");
        return Htmlstring;
    }
    
    /**
     * 转换字符串，特殊字符
     * @param str
     * @return
     */
    public static String replaceString(String str)
    {
        str = str.replaceAll("'", "’");
        //str = str.replaceAll("-", "－");
        //str = str.replaceAll("=", "＝");
        str = str.replaceAll(";", "；");
        str = str.replaceAll("%", "％");
        str = str.replaceAll("(", "（");
        str = str.replaceAll(")", "）");
        str = str.replaceAll("<", "〈");
        str = str.replaceAll(">", "〉");
        str = str.replaceAll("+", "＋");
        return str.trim();
    }
}
