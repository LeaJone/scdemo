package com.fsd.core.util;

import org.springframework.web.multipart.MultipartFile;
import java.io.File;
import java.util.Random;

/**
 * 文件上传
 */
public class UploadUtil {
	
	public static String uploadFile(MultipartFile file, String path) throws Exception {
        String name = file.getOriginalFilename();
        String suffixName = name.substring(name.lastIndexOf("."));
        String  hash = Integer.toHexString(new Random().nextInt());
        String filename = hash + suffixName;
        File tempFile = new File(path, filename);
        if(!tempFile.getParentFile().exists()){
            tempFile.getParentFile().mkdirs();
        }
        if(tempFile.exists()){
            tempFile.delete();
        }
        tempFile.createNewFile();
        file.transferTo(tempFile);
	    return tempFile.getName();
    }

}
