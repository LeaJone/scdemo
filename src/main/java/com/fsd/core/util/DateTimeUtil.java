package com.fsd.core.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期时间工具类
 * @author fsd
 */
public class DateTimeUtil {
	
	private Calendar calendar;
	
	/**
	 * 构造函数
	 */
	public DateTimeUtil(){
		calendar = Calendar.getInstance();
	}
	
	/**
	 * 构造函数
	 * @param dateString 时间字符串
	 * @param dateStyle 时间字符串格式yyyyMMddHHmmss
	 * @throws ParseException
	 */
	public DateTimeUtil(String dateString, String dateStyle) throws ParseException{
		if(calendar == null){
			calendar = Calendar.getInstance();
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat(dateStyle);
		Date date = dateFormat.parse(dateString);
		calendar.setTime(date);
	}
	
	/**
	 * 时间字符串格式转换
	 * @param dateString 时间字符串
	 * @param oldStyle 时间本身格式
	 * @param newStyle 转换为新格式
	 * @return 返回新格式时间字符串
	 * @throws ParseException
	 */
	public static String getNewDateTimeStyle(String dateString, String oldStyle, String newStyle) throws ParseException{
		SimpleDateFormat dateFormat = new SimpleDateFormat(oldStyle);
		Date date = dateFormat.parse(dateString);
		dateFormat.applyPattern(newStyle);
		return dateFormat.format(date);
	}

	/**
	 * 检查时间长度是否为14位，不足14位自动补全
	 * @param strDate
	 * @return
	 */
	public static String checkDateTime14(String strDate){
		if (strDate.length() == 8){
			return strDate + "000000";
		}
		return strDate;
	}
   
	/**
	 * 时间加一个值，类型表示加在哪个部分
	 * @param strDate
	 * @param addType 1年，2月，6天，11小时，12分钟，13秒，14毫秒
	 * @param num
	 * @return
	 * @throws ParseException 
	 */
    public static String getDateTimeAddType(String strDate, int addType, int num) throws ParseException {
        strDate = checkDateTime14(strDate);
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
		Date date = format.parse(strDate);
        Calendar rightNow = Calendar.getInstance();
        rightNow.setTime(date);
        rightNow.add(addType, num);
        Date dt1=rightNow.getTime();
        return format.format(dt1);
    }
    
    /**
	 * 时间加一个值，类型表示加在哪个部分
	 * @param strDate
	 * @param style 日期字符串格式
	 * @param addType 1年，2月，6天，11小时，12分钟，13秒，14毫秒
	 * @param num
	 * @return
	 * @throws ParseException 
	 */
    public static String getDateTimeAddType(String strDate, String style, int addType, int num) throws ParseException {
        //strDate = checkDateTime14(strDate);
		SimpleDateFormat format = new SimpleDateFormat(style);
		Date date = format.parse(strDate);
        Calendar rightNow = Calendar.getInstance();
        rightNow.setTime(date);
        rightNow.add(addType, num);
        Date dt1=rightNow.getTime();
        return format.format(dt1);
    }
    
    /**
     *  日期字符串格式转换
     * @param dateString 需要转换的日期字符串
     * @param old_style 被转换日期字符串格式 例：yyyyMMddHHmmss
     * @param new_style 转换后的日期字串换格式 例：yyyyMMddHHmmss
     * @return
     * @throws Exception
     */
    public static String formatDate(String dateString, String old_style, String new_style) throws Exception{
    	if(dateString != null && !"".equals(dateString)){
    		SimpleDateFormat format = new SimpleDateFormat(old_style);
        	Date date = format.parse(dateString);
        	format =new SimpleDateFormat(new_style); 
        	return format.format(date);
    	}else{
    		return "";
    	}
    }
    
    /**
     * 获取当前时间月份最后一天，返回8位时间
     * @param dateString 字符串格式为：yyyyMMddHHmmss
     * @return
     * @throws ParseException
     */
    public static String getLastDayOfMonthTo8(String dateString) throws ParseException{
    	return getLastDayOfMonthTo8(dateString, "yyyyMMddHHmmss");
    }
    
    /**
     * 获取当前时间月份最后一天，返回8位时间
     * @param dateString
     * @param dateStyle 字符串格式 例：yyyyMMddHHmmss
     * @return
     * @throws ParseException
     */
    public static String getLastDayOfMonthTo8(String dateString, String dateStyle) throws ParseException{
    	SimpleDateFormat dateFormat = new SimpleDateFormat(dateStyle);
		Date date = dateFormat.parse(dateString);
        Calendar cDay1 = Calendar.getInstance();  
        cDay1.setTime(date);  
        int lastDay = cDay1.getActualMaximum(Calendar.DAY_OF_MONTH);
		return dateString.substring(0, 6) + String.valueOf(lastDay);
    }
    
    /**
     * 把long 类型的时间转换为自己想想要的日期字符串
     * @param dataString
     * @param style
     * @return
     * @throws Exception
     */
    public static String formatDate(long dataString, String style) throws Exception{
    	Calendar date = Calendar.getInstance();
		date.setTimeInMillis(dataString);
		SimpleDateFormat df = new SimpleDateFormat(style);
		return df.format(date.getTime());
    }
    /**
     * 获得年
     * @return
     */
    public int getYear(){
    	return calendar.get(Calendar.YEAR);  
    }
    
    /**
     * 获得月
     * @return
     */
    public int getMonth(){
    	return calendar.get(Calendar.MONTH) + 1;  
    }
    
    /**
     * 获得日
     * @return
     */
    public int getDay(){
    	return calendar.get(Calendar.DAY_OF_MONTH);  
    }
    
    /**
     * 获得时
     * @return
     */
    public int getHour(){
    	return calendar.get(Calendar.HOUR_OF_DAY);  
    }
    
    /**
     * 获得分
     * @return
     */
    public int getMinute(){
    	return calendar.get(Calendar.MINUTE);  
    }
    
    /**
     * 获得秒
     * @return
     */
    public int getSecond(){
    	return calendar.get(Calendar.SECOND);  
    }

	/**
	 * date2比date1多的天数
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static int differentDays(Date date1,Date date2) {
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date1);

		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(date2);
		int day1= cal1.get(Calendar.DAY_OF_YEAR);
		int day2 = cal2.get(Calendar.DAY_OF_YEAR);

		int year1 = cal1.get(Calendar.YEAR);
		int year2 = cal2.get(Calendar.YEAR);
		//同一年
		if(year1 != year2)
		{
			int timeDistance = 0 ;
			for(int i = year1 ; i < year2 ; i ++)
			{
				//闰年
				if(i%4==0 && i%100!=0 || i%400==0)
				{
					timeDistance += 366;
				}
				else    //不是闰年
				{
					timeDistance += 365;
				}
			}

			return timeDistance + (day2-day1) ;
		}
		else    //不同年
		{
			System.out.println("判断day2 - day1 : " + (day2-day1));
			return day2-day1;
		}
	}
}
