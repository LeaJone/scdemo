package com.fsd.core.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

/**
 * 序列化工具
 * @author lzfsd
 */
public class SerializeUtil {
	
	/**
	 * 将List序列化为文件
	 * @param list 要序列化的List
	 * @param dir 文件保存的路径
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public static String write(List list, String dir) throws Exception{
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(dir));
		oos.writeObject(list);
        oos.flush();
        oos.close();
        return "";
	}
	
	/**
	 * 读取序列化的文件
	 * @param dir 文件路径
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public static List read(String dir) throws Exception{
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(dir));
		List list = (List)ois.readObject();
		return list;
	}
}
