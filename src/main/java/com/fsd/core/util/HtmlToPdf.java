package com.fsd.core.util;

import java.io.File;

/**
 * @Description
 * @Author 芦明宝
 * @Date 2019-03-26 16:10
 * @Version 1.0
 */
public class HtmlToPdf {
    // wkhtmltopdf在系统中的路径
    private static final String toPdfTool = "wkhtmltopdf";

    /**
     * html转pdf
     *
     * @param srcPath
     *            html路径，可以是硬盘上的路径，也可以是网络路径
     * @param destPath
     *            pdf保存路径
     * @return 转换成功返回true
     */
    public static boolean convert(String srcPath, String destPath) {
        File file = new File(destPath);
        File parent = file.getParentFile();
        // 如果pdf保存路径不存在，则创建路径
        if (!parent.exists()) {
            parent.mkdirs();
        }
        StringBuilder cmd = new StringBuilder();
        cmd.append(toPdfTool);
        cmd.append(" ");
        cmd.append(" --margin-top 2cm ");// 设置页面上边距 (default 10mm)
        cmd.append(" --footer-center [page] ");//设置在中心位置的页脚内容
        cmd.append(" --page-size A4 ");//设置纸张大小
        cmd.append(" --footer-spacing 1 ");// (设置页脚和内容的距离
        cmd.append(srcPath);
        cmd.append(" ");
        cmd.append(destPath);

        boolean result = true;
        try {
            Process proc = Runtime.getRuntime().exec(cmd.toString());
            HtmlToPdfInterceptor error = new HtmlToPdfInterceptor(proc.getErrorStream());
            HtmlToPdfInterceptor output = new HtmlToPdfInterceptor(proc.getInputStream());
            error.start();
            output.start();
            proc.waitFor();
        } catch (Exception e) {
            result = false;
            e.printStackTrace();
        }

        return result;
    }

    public static void main(String[] args) {
        HtmlToPdf.convert("http://127.0.0.1:8080/admin/dcxm_sbs-4cacbc7b64644a5993d7029f266cf284.htm", "D:/baidu.pdf");
    }
}
