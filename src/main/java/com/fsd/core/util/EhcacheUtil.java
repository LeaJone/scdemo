package com.fsd.core.util;

import java.net.URL;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
/**
 * Ehcache缓存工具类
 * @author lumingbao
 *
 */
public class EhcacheUtil {
  
    private static final String path = "/ehcache.xml";
  
    private URL url;
  
    private CacheManager manager;
  
    private static EhcacheUtil ehCache;
  
    private EhcacheUtil(String path) {
        url = getClass().getResource(path);  
        manager = CacheManager.create(url);  
    }  
  
    public static EhcacheUtil getInstance() {
        if (ehCache== null) {  
            ehCache= new EhcacheUtil(path);  
        }  
        return ehCache;  
    }  
  
    public void put(String key, Object value) {
    	Cache cache = manager.getCache("baseCache");
        Element element = new Element(key, value);  
        cache.put(element);  
    }  
    
    public Object get(String key) {
    	Cache cache = manager.getCache("baseCache");
        Element element = cache.get(key);  
        return element == null ? null : element.getObjectValue();  
    }

    public void remove(String key) {
    	Cache cache = manager.getCache("baseCache");
        cache.remove(key);
    }

    public void removeAll(){
        Cache cache = manager.getCache("baseCache");
        cache.removeAll();
    }
}