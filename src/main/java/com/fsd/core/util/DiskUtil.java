package com.fsd.core.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import com.fsd.core.bean.FsdFile;
import com.fsd.core.common.BusinessException;

/**
 * 硬盘工具类
 * 读取硬盘目录等等
 * @author fsd
 */
public class DiskUtil {
	
	/**
	 * 加载制定目录下的所有文件（包括文件和文件夹）
	 * @param url
	 * @return
	 * @throws Exception
	 */
	public static List<FsdFile> getFileList(String url) throws Exception{
		File file = new File(url);
		File[] list = file.listFiles();
		List<FsdFile> fsffilelsit = new ArrayList<FsdFile>();
	    for (int i = 0; i < list.length; i++) {
	    	FsdFile fsdfile = new FsdFile();
	    	fsdfile.setName(list[i].getName());
	    	fsdfile.setSize(String.valueOf(MathsUtil.formatDouble(getDirSize(list[i]), 2)) + "KB");
    		fsdfile.setUpdateTime(DateTimeUtil.formatDate(list[i].lastModified(), "yyyy/MM/dd hh:mm:ss"));
    		fsdfile.setUrl(list[i].getPath().replaceAll("\\\\", "/"));
	    	if(list[i].isDirectory()){
	    		fsdfile.setType("0");//0：文件夹，1：文件
	    	}else{
	    		fsdfile.setType("1");//0：文件夹，1：文件
	    	}
	    	fsffilelsit.add(fsdfile);
	    }
	    return fsffilelsit;
	}
	
	/**
	 * 获得文件或文件夹的大小
	 * @param file
	 * @return
	 * @throws Exception
	 */
	public static double getDirSize(File file) throws Exception{
        //判断文件是否存在     
        if (file.exists()) {
            //如果是目录则递归计算其内容的总大小
            if(file.isDirectory()) {
                File[] children = file.listFiles();
                double size = 0;
                for (File f : children)
                    size += getDirSize(f);
                return size;
            }else {//如果是文件则直接返回其大小,以“KB”为单位
                double size = (double) file.length() / 1024;
                return size;
            }
        }else{
        	throw new BusinessException("文件不存在！");
        }
    }
	
	public static void main(String[] args) {
		try {
			List<FsdFile> list = getFileList("D:/手机备份");
			for (int i = 0; i < list.size(); i++) {
				System.out.println(list.get(i).getUrl() + "~~~" + list.get(i).getUpdateTime() + "```" + list.get(i).getSize());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
