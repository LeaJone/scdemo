package com.fsd.core.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 工具类 - 参数
 * @author lumingbao
 */

public class ParametersUtil {
	public static final Integer MAX_PAGE_SIZE = 500;// 每页最大记录数限制
	
	// 排序方式（递增、递减）
	public enum Order {
		asc, desc
	}

	private Map<String, Object> map = null;
	private int page = 1;// 当前页码
	private int limit = 20;// 每页记录数
	private String searchBy;// 查找字段
	private String keyword;// 查找关键字
	private String orderBy;// 排序字段
	private Order order;// 排序方式

	private int totalCount;// 总记录数
	private List<?> data;// 返回结果
	
	private String jsonData;
	
	public void addSendObject(String key, Object obj){
		if(map == null){
			map = new HashMap<String, Object>();
		}
		map.put(key, obj);
	}
	
	public <T> Map<String, Object> sendObject() {
		if (map == null){
			map = new HashMap<String, Object>();
			map.put("data", null);
		}
		map.put("success", true);
		return map;
	}
	public <T> Map<String, Object> sendObject(Object obj) {
		if (map == null)
			map = new HashMap<String, Object>();
		map.put("data", obj);
		map.put("success", true);
		return map;
	}
	
	public static <T> Map<String, Object> sendList(Object obj) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("data", obj);
		map.put("success", true);
		return map;
	}
	
	public static <T> Map<String, Object> sendTreeList(Object obj) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("children", obj);
		map.put("success", true);
		return map;
	}
	
	public static <T> Map<String, Object> sendErrorList() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("data", null);
		map.put("success", false);
		return map;
	}
	
	public static <T> Map<String, Object> sendList() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("data", null);
		map.put("success", true);
		return map;
	}
	
	// 获取总页数
	public int getPageCount() {
		int pageCount = totalCount / limit;
		if (totalCount % limit > 0) {
			pageCount++;
		}
		return pageCount;
	}

	public int getPage() {
		return page;
	}
	
	public void setPage(int page) {
		if(page < 1){
			page = 1;
		}
		this.page = page;
	}
	
	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		if (limit < 1) {
			limit = 1;
		} else if (limit > MAX_PAGE_SIZE) {
			limit = MAX_PAGE_SIZE;
		}
		this.limit = limit;
	}

	public String getSearchBy() {
		return searchBy;
	}

	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public List<?> getData() {
		return data;
	}

	public void setData(List<?> data) {
		this.data = data;
	}
}