package com.fsd.core.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.artofsolving.jodconverter.DocumentConverter;
import com.artofsolving.jodconverter.openoffice.connection.OpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.connection.SocketOpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.converter.OpenOfficeDocumentConverter;

/**
 * @Description
 * @Author 芦明宝
 * @Date 2019-03-29 23:20
 * @Version 1.0
 */
public class Doc2Html {

    /**
     * 将word文档转换成html文档
     * @param docFile
     *                需要转换的word文档
     * @param filepath
     *                转换之后html的存放路径
     * @return 转换之后的html文件
     */
    public static File convert(File docFile, String filepath) throws Exception{
        // 创建保存html的文件
        File htmlFile = new File(filepath + "/" + new Date().getTime() + ".html");

        try {
            // 这里是OpenOffice的安装目录
            String OpenOffice_HOME = "/opt/openoffice4/";
            // 启动OpenOffice的服务
            String command = OpenOffice_HOME  + "program" + File.separator + "soffice -headless -accept=\"socket,host=127.0.0.1,port=8100;urp;\" -nofirststartwizard";
            Process pro = Runtime.getRuntime().exec(command);
            // 创建Openoffice连接
            OpenOfficeConnection con = new SocketOpenOfficeConnection(8100);
            // 连接
            con.connect();
            // 创建转换器
            DocumentConverter converter = new OpenOfficeDocumentConverter(con);
            // 转换文档问html
            converter.convert(docFile, htmlFile);
            // 关闭openoffice连接
            con.disconnect();
            // 关闭OpenOffice服务的进程
            pro.destroy();
        } catch (Exception e) {
            System.out.println("获取OpenOffice连接失败...");
            e.printStackTrace();
        }

        return htmlFile;
    }

    /**
     * 将word转换成html文件，并且获取html文件代码。
     * @param docFile
     *                需要转换的文档
     * @param filepath
     *                文档中图片的保存位置
     * @return 转换成功的html代码
     */
    public static String toHtmlString(File docFile, String filepath, int clean_format, int text_indent, int clean_fontsize, int clean_font, int clean_image, int clean_a) throws Exception{
        // 判断上传的文件是否是受支持的类型
        String fileName = docFile.getName();
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
        if("doc".equals(suffix) || "docx".equals(suffix) || "wps".equals(suffix) || "wpt".equals(suffix)){
            // 将 wps office的 wps 和 wpt 转为 doc
            if("wps".equals(suffix)){
                String oldName = docFile.getName();
                String newName = oldName.replaceAll("wps", "doc");
                String oldPath = docFile.getPath();
                String newPath = oldPath.replaceAll(oldName, newName);
                docFile.renameTo(new File(newPath));
                docFile = new File(newPath);
            }else if("wpt".equals(suffix)){
                String oldName = docFile.getName();
                String newName = oldName.replaceAll("wpt", "doc");
                String oldPath = docFile.getPath();
                String newPath = oldPath.replaceAll(oldName, newName);
                docFile.renameTo(new File(newPath));
                docFile = new File(newPath);
            }
            // 转换word文档
            File htmlFile = convert(docFile, filepath);
            // 获取html文件流
            StringBuffer htmlSb = new StringBuffer();

            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(htmlFile), "UTF-8"));
            while (br.ready()) {
                htmlSb.append(br.readLine());
            }
            br.close();
            // 删除临时文件
            htmlFile.delete();

            // HTML文件字符串
            String htmlStr = htmlSb.toString();
            // 返回经过清洁的html文本
            return clearFormat(htmlStr, filepath, clean_format, text_indent, clean_fontsize, clean_font, clean_image, clean_a);
        }else{
            return "不支持导入该类型的文件";
        }
    }

    /**
     * 清除一些不需要的html标记
     * @param htmlStr
     *                带有复杂html标记的html语句
     * @return 去除了不需要html标记的语句
     */
    protected static String clearFormat(String htmlStr, String docImgPath, int clean_format, int text_indent, int clean_fontsize, int clean_font, int clean_image, int clean_a) throws Exception{
        // 获取body内容的正则
        String bodyReg = "<BODY .*</BODY>";
        Pattern bodyPattern = Pattern.compile(bodyReg);
        Matcher bodyMatcher = bodyPattern.matcher(htmlStr);
        if (bodyMatcher.find()) {
            // 获取BODY内容，并转化BODY标签为DIV
            htmlStr = bodyMatcher.group().replaceFirst("<BODY", "<DIV").replaceAll("</BODY>", "</DIV>");
        }
        // 调整图片地址
        htmlStr = htmlStr.replaceAll("<IMG SRC=\"", "<IMG SRC=\"" + docImgPath + "/");
        // 清除格式
        if(clean_format == 1){
            // 首行缩进
            if(text_indent == 1){
                // 把<P></P>转换成</div></div>并删除样式
                htmlStr = htmlStr.replaceAll("(<P)([^>]*)(>.*?)(<\\/P>)", "<div style='text-indent: 2em;'$3</div>");
            }else{
                // 把<P></P>转换成</div></div>并删除样式
                htmlStr = htmlStr.replaceAll("(<P)([^>]*)(>.*?)(<\\/P>)", "<div$3</div>");
            }

            // 删除不需要的标签
            htmlStr = htmlStr.replaceAll("<[/]?(font|FONT|h1|H1|h2|H2|h3|H3|h4|H4|h5|H5|h6|H6|strong|STRONG|span|SPAN|xml|XML|del|DEL|ins|INS|meta|META|b|B|[ovwxpOVWXP]:\\w+)[^>]*?>", "");
            // 删除不需要的属性
            htmlStr = htmlStr.replaceAll("<([^>]*)(?:lang|LANG|class|CLASS|style|STYLE|size|SIZE|face|FACE|[ovwxpOVWXP]:\\w+)=(?:'[^']*'|\"\"[^\"\"]*\"\"|[^>]+)([^>]*)>", "<$1$2>");
        }else{
            // 不清除格式
            // 首行缩进
            if(text_indent == 1){
                // 把<P></P>转换成</div></div>保留样式
                htmlStr = htmlStr.replaceAll("(<P)([^>]*>.*?)(<\\/P>)", "<div style='text-indent: 2em;'$2</div>");
            }
            // 清除字体
            if(clean_font == 1){
                htmlStr = htmlStr.replaceAll("<([^>]*)(?:face|FACE|[ovwxpOVWXP]:\\w+)=(?:'[^']*'|\"\"[^\"\"]*\"\"|[^>]+)([^>]*)>", "<$1$2>");
            }
            // 清除字号
            if(clean_fontsize == 1){
                htmlStr = htmlStr.replaceAll("<[/]?(font|FONT|h1|H1|h2|H2|h3|H3|h4|H4|h5|H5|h6|H6|strong|STRONG|b|B|[ovwxpOVWXP]:\\w+)[^>]*?>", "");
                htmlStr = htmlStr.replaceAll("<([^>]*)(?:size|SIZE|[ovwxpOVWXP]:\\w+)=(?:'[^']*'|\"\"[^\"\"]*\"\"|[^>]+)([^>]*)>", "<$1$2>");
            }
        }
        // 清除图片
        if(clean_image == 1){
            htmlStr = htmlStr.replaceAll("<[/]?(img|IMG|[ovwxpOVWXP]:\\w+)[^>]*?>", "");
        }
        // 清除超链接
        if(clean_a == 1){
            htmlStr = htmlStr.replaceAll("<[/]?(a|A|[ovwxpOVWXP]:\\w+)[^>]*?>", "");
        }
        return htmlStr;
    }
}