package com.fsd.core.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.ActivitiException;
import org.apache.log4j.Logger;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.bean.ExceptionInfo;
import com.fsd.core.bean.shiro.CaptchaException;
import com.fsd.core.common.BusinessException;

/**
 *  Controller基类
 */

public class BaseController{
	
	private static final Logger log = Logger.getLogger(BaseController.class);

	/** 基于@ExceptionHandler异常处理 */
	@ExceptionHandler
	public @ResponseBody Object resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
		ExceptionInfo exinfo = new ExceptionInfo();
		if(ex instanceof BusinessException) {
			exinfo.setIsException(true);
			exinfo.setType("bizexception");
			exinfo.setTitle("提示");
			exinfo.setMessage(ex.getMessage());
			exinfo.setDetails(ex.toString());
		}else if(ex instanceof ActivitiException) {
			exinfo.setIsException(true);
			exinfo.setType("bizexception");
			exinfo.setTitle("提示");
			exinfo.setMessage(ex.getCause().getCause().getMessage());
			exinfo.setDetails(ex.toString());
		}else if(ex instanceof IncorrectCredentialsException){
			exinfo.setIsException(true);
			exinfo.setType("bizexception");
			exinfo.setTitle("提示");
			exinfo.setDetails(ex.toString());
			exinfo.setMessage("认证失败，密码错误！");
		}else if(ex instanceof UnknownAccountException){
			exinfo.setIsException(true);
			exinfo.setType("bizexception");
			exinfo.setTitle("提示");
			exinfo.setDetails(ex.toString());
			exinfo.setMessage("认证失败，该用户不存在！");
		}else if(ex instanceof CaptchaException){
			exinfo.setIsException(true);
			exinfo.setType("bizexception");
			exinfo.setTitle("提示");
			exinfo.setDetails(ex.toString());
			exinfo.setMessage("认证失败，验证码错误！");
		}else if(ex instanceof ExcessiveAttemptsException){
			exinfo.setIsException(true);
			exinfo.setType("bizexception");
			exinfo.setTitle("提示");
			exinfo.setDetails(ex.toString());
			exinfo.setMessage("认证失败，密码多次错误，请10分钟后重试！");
		}else if(ex instanceof LockedAccountException){
			exinfo.setIsException(true);
			exinfo.setType("bizexception");
			exinfo.setTitle("提示");
			exinfo.setDetails(ex.toString());
			exinfo.setMessage("账号已被锁定,请联系管理员!");
		}else if(ex instanceof ActivitiException){
			exinfo.setIsException(true);
			exinfo.setType("bizexception");
			exinfo.setTitle("提示");
			exinfo.setMessage(ex.getMessage());
			exinfo.setDetails(ex.toString());
		}else {
			exinfo.setIsException(true);
			exinfo.setType("exception");
			exinfo.setTitle("异常");
			exinfo.setDetails(ex.toString());
			exinfo.setMessage(ex.toString());
			ex.printStackTrace();
		}
		return exinfo;
	}
}
