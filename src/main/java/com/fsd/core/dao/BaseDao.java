package com.fsd.core.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import com.fsd.core.util.ParametersUtil;

/**
 * Dao接口 - Dao基接口
 */

public interface BaseDao<T, PK extends Serializable> {

	/**
	 * 根据ID获取实体对象
	 * @param id 记录ID
	 * @return 实体对象
	 */
	public T get(PK id);
	/**
	 * 根据ID获取实体对象
	 * @param id 记录ID
	 * @return 实体对象
	 */
	public T load(PK id);
	/**
	 * 通过条件查询对象集合
	 * @param criteria
	 * @return
	 */
	public List<T> getList(Criteria criteria);
	/**
	 * 获取所有实体对象总数
	 * 
	 * @return 实体对象总数
	 */
	public Long getCount(String... condition);
	/**
	 * 获取所有实体对象总数
	 * 
	 * @return 实体对象总数
	 */
	public Long getCountEntity(String sql, Object... values);
	/**
	 * 保存实体对象
	 * 
	 * @param entity
	 *            对象
	 * @return ID
	 */
	public PK save(T entity);
	/**
	 * 更新实体对象
	 * 
	 * @param entity
	 *            对象
	 */
	public void update(T entity);

	/**
	 * 新增或更新对象
	 *
	 * @param entity
	 * 				对象
	 */
	public void saveOrUpdate(T entity);
	/**
	 * 删除实体对象
	 * 
	 * @param entity
	 *            对象
	 * @return
	 */
	public void delete(T entity);
	/**
	 * 根据ID删除实体对象
	 * 
	 * @param id
	 *            记录ID
	 */
	public void delete(PK id);
	/**
	 * 根据ID数组删除实体对象
	 * 
	 * @param ids
	 *            ID数组
	 */
	public void delete(PK[] ids);
	/**
	 * 根据hql语句获取所有实体对象集合
	 * @param hql 
	 * 				HQL语句
	 * @param values 
	 * 				可变参数
	 * @return
	 */
	public List<T> queryByHql(String hql, Object... values);
	/**
	 * 
	 * @param sql 
	 * 				SQL语句
	 * @param values 
	 * 				可变参数
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public List queryBySql(String sql, Object... values);
	/**
	 * 
	 * @param sql
	 * 				SQL语句
	 * @param entityClass
	 * 				返回的实体类型
	 * @param values
	 * 				可变参数
	 * @return
	 */
	public List<T> queryBySql1(String sql, Object... values);
	/**
	 * 执行非查询HQL语句
	 * 
	 * @param hql
	 *            HQL语句
	 */
	public int executeHql(String hql, Object... values);
	/**
	 * 执行非查询SQL语句
	 * 
	 * @param sql
	 *            SQL语句
	 */
	public int executeSql(String sql, Object... values);
	/**
	 * 刷新session
	 * 
	 */
	public void flush();
	/**
	 * 清除对象
	 * 
	 * @param object
	 *            需要清除的对象
	 */
	public void evict(Object object);
	/**
	 * 清除Session
	 * 
	 */
	public void clear();
	/**
	 * 根据Pager进行查询(提供分页、查找、排序功能)
	 * 
	 * @param pager
	 *            Pager对象
	 * 
	 * @return Pager对象
	 */
	public ParametersUtil findPager(ParametersUtil pager);
	/**
	 * 根据Pager、Criterion进行查询(提供分页、查找、排序功能)
	 * 
	 * @param pager
	 *            Pager对象
	 *            
	 * @param criterions
	 *            Criterion数组
	 * 
	 * @return Pager对象
	 */
	public ParametersUtil findPager(ParametersUtil pager, Criterion... criterions);
	/**
	 * 根据Pager、Criterion进行查询(提供分页、查找、排序功能)
	 * 
	 * @param pager
	 *            Pager对象
	 *            
	 * @param orders
	 *            Order数组
	 * 
	 * @return Pager对象
	 */
	public ParametersUtil findPager(ParametersUtil pager, Order... orders);
	/**
	 * 根据Pager、Criteria进行查询(提供分页、查找、排序功能)
	 * 
	 * @param pager
	 *            Pager对象
	 *            
	 * @param criteria
	 *            Criteria对象
	 * 
	 * @return Pager对象
	 */
	public ParametersUtil findPager(ParametersUtil pager, Criteria criteria);
	/**
	 * 获得32位的UUID
	 */
	public String getUUID();
	/**
	 * 创建Criteria对象
	 */
	public Criteria createCriteria();
	public ParametersUtil search(ParametersUtil pager, Map<String, Object> param, String... filed) throws Exception;
	/**
	 * 建立索引
	 */
	public void createIndex();
	
	/**
	 * 分页sql查询
	 * @param pager
	 * @param sqlTable 排序条件，例：tableName
	 * @param sqlOrder 排序条件，例：id asc, name desc
	 * @param sqlWhere 查询条件，例：id = ? and name = ?
	 * @param values 查询条件值
	 * @return
	 */
	public ParametersUtil queryBySqlPager(ParametersUtil pager, String sqlTable, String sqlOrder, String sqlWhere, Object... values);
}