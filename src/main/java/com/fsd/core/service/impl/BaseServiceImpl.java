package com.fsd.core.service.impl;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.springframework.transaction.annotation.Transactional;
import com.fsd.core.dao.BaseDao;
import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;
import com.fsd.core.util.SpringContextHolder;

/**
 * Service实现类 - Service实现类基类
 */

@Transactional
public class BaseServiceImpl<T, PK extends Serializable> implements BaseService<T, PK> {
	protected static final Log log = LogFactory.getLog(BaseServiceImpl.class);
	private BaseDao<T, PK> baseDao;

	public BaseDao<T, PK> getBaseDao() {
		return baseDao;
	}

	public void setBaseDao(BaseDao<T, PK> baseDao) {
		this.baseDao = baseDao;
	}
	
	@Transactional(readOnly = true)
	public T get(PK id) {
		return baseDao.get(id);
	}

	@Transactional(readOnly = true)
	public T load(PK id) {
		return baseDao.load(id);
	}

	@Transactional(readOnly = true)
	public List<T> getAllList() {
		return baseDao.getList(baseDao.createCriteria());
	}
	
	@Transactional(readOnly = true)
	public Long getTotalCount() {
		return baseDao.getCount();
	}

	@Transactional
	public PK save(T entity) {
		return baseDao.save(entity);
	}

	@Transactional
	public void update(T entity) {
		baseDao.update(entity);
	}

	@Transactional
	public void saveOrUpdate(T entity){
		baseDao.saveOrUpdate(entity);
	}
	
	@Transactional
	public void delete(T entity) {
		baseDao.delete(entity);
	}

	@Transactional
	public void delete(PK id) {
		baseDao.delete(id);
	}

	@Transactional
	public void delete(PK[] ids) {
		baseDao.delete(ids);
	}

	@Transactional(readOnly = true)
	public void flush() {
		baseDao.flush();
	}

	@Transactional(readOnly = true)
	public void evict(Object object) {
		baseDao.evict(object);
	}
	
	@Transactional(readOnly = true)
	public void clear() {
		baseDao.clear();
	}
	
	@Transactional(readOnly = true)
	public ParametersUtil findPager(ParametersUtil pager) {
		return baseDao.findPager(pager);
	}
	
	@Transactional(readOnly = true)
	public ParametersUtil findPager(ParametersUtil pager, Criterion... criterions) {
		return baseDao.findPager(pager, criterions);
	}
	
	@Transactional(readOnly = true)
	public ParametersUtil findPager(ParametersUtil pager, Order... orders) {
		return baseDao.findPager(pager, orders);
	}
	
	@Transactional(readOnly = true)
	public ParametersUtil findPager(ParametersUtil pager, Criteria criteria) {
		return baseDao.findPager(pager, criteria);
	}
	
	/**
	 * 获得Dao
	 * @return
	 */
	public BaseDao<T, PK> getDao(){
		return this.baseDao;
	}
	
	/**
	 * 获得32位的UUID
	 */
	public String getUUID(){
		return UUID.randomUUID().toString().trim().replaceAll("-", "");
	}
	
	/**
	 * 获得14位的时间
	 */
	public String getData(){
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
		return format.format(new Date());
	}
	
	/**
	 * 获得8位的时间
	 */
	public String getData1(){
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		return format.format(new Date());
	}
	
	/**
	 * 修改人员信息信息
	 */
	@SuppressWarnings("rawtypes")
	public void updateAEmployeeInfo(Map<String, String> moduleMap, String objSimpleName, Object object) throws Exception{
		if (moduleMap == null)
			return;
		//会员模块的调用
		if (moduleMap.containsKey("FSDMEMBERMODULE")){
			if (moduleMap.get("FSDMEMBERMODULE").equals("true")){
				BaseService operateClass = (BaseService) SpringContextHolder.getBean("Z_cxxxServiceImpl");
				if (operateClass != null){
					operateClass.updateAEmployeeInfo(objSimpleName, object);
				}
			}
		}
	}
	/**
	 * 修改人员信息子类重写方法
	 */
	public void updateAEmployeeInfo(String objSimpleName, Object object) throws Exception{
	}
	
	public ParametersUtil search(ParametersUtil pager, Map<String, Object> param, String... filed) throws Exception{
		return baseDao.search(pager, param, filed);
	}
	
	public void createIndex(){
		baseDao.createIndex();
	}
}