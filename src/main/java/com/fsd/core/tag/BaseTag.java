package com.fsd.core.tag;

import java.io.IOException;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import freemarker.core.Environment;
import freemarker.ext.servlet.HttpRequestHashModel;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;

public abstract class BaseTag implements TemplateDirectiveModel{
	
	protected static final Log log = LogFactory.getLog(BaseTag.class);
	
	@SuppressWarnings("rawtypes")
	protected abstract void service(Environment env, Map params,
			TemplateModel[] loopVars, TemplateDirectiveBody body)
			throws Exception;
	
	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body) throws TemplateException, IOException {
		// TODO Auto-generated method stub
		try {
			service(env, params, loopVars, body);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.debug("自定义Freemarker标签错误",e);
		}
	}
	
	/**
	 * 取标签所在请求页的request对象
	 * 
	 * @param env
	 * @return
	 * @throws TemplateModelException
	 */
	public HttpServletRequest getRequest(Environment env) throws TemplateModelException {
		HttpRequestHashModel requestParams = (HttpRequestHashModel) env.getGlobalVariable("Request");
		return requestParams.getRequest();
	}
}
