package com.fsd.core.wechat.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fsd.core.common.BusinessException;
import com.fsd.core.wechat.entity.menu.Button;
import com.fsd.core.wechat.entity.menu.Menu;
import com.fsd.core.wechat.util.Constants;
import com.fsd.core.wechat.util.WeixinUtil;

/**
 * 自定义菜单处理类
 */

public class MenuManager {

	private static Logger log = LoggerFactory.getLogger(MenuManager.class);

    public static void main(String[] args) {
        // 调用接口获取access_token  

		try {
	            // 调用接口创建菜单
	            WeixinUtil.createMenu(getMenu(), Constants.APPID, Constants.SECRET);
	  
	            // 判断菜单创建结果
//	            if (0 == result){
//	                log.info("菜单创建成功！");
//	                System.out.println("菜单创建成功！");
//	            }else{
//	                log.info("菜单创建失败，错误码：" + result);
//	                System.out.println("菜单创建失败，错误码：" + result);
//	            }
		} catch (BusinessException e) {
            log.info("菜单创建失败，错误码：" + e.getMessage());
            System.out.println("菜单创建失败，错误码：" + e.getMessage());
			e.printStackTrace();
		}
  
    }
  
    /** 
     * 组装菜单数据 
     * @return 
     */
    private static Menu getMenu() {
    	Button btn11 = new Button();
        btn11.setName("扫码带提示");
        btn11.setType("scancode_waitmsg");
        btn11.setKey("11");  
  
        Button btn12 = new Button();
        btn12.setName("公交查询");  
        btn12.setType("click");
        btn12.setKey("12");  
  
        Button btn13 = new Button();
        btn13.setName("周边搜索");
        btn13.setType("click");
        btn13.setKey("13");
  
        Button btn14 = new Button();  
        btn14.setName("历史上的今天");  
        btn14.setType("click");  
        btn14.setKey("14");  
  
        Button btn21 = new Button();  
        btn21.setName("歌曲点播");  
        btn21.setType("click");  
        btn21.setKey("21");  
  
        Button btn22 = new Button();  
        btn22.setName("经典游戏");  
        btn22.setType("click");  
        btn22.setKey("22");  
  
        Button btn23 = new Button();  
        btn23.setName("美女电台");  
        btn23.setType("click");  
        btn23.setKey("23");  
  
        Button btn24 = new Button();  
        btn24.setName("人脸识别");  
        btn24.setType("click");  
        btn24.setKey("24");  
  
        Button btn25 = new Button();  
        btn25.setName("聊天唠嗑");  
        btn25.setType("click");  
        btn25.setKey("25");  
  
        Button btn31 = new Button();  
        btn31.setName("Q友圈");  
        btn31.setType("click");  
        btn31.setKey("31");  
  
        Button btn32 = new Button();  
        btn32.setName("电影排行榜");  
        btn32.setType("click");  
        btn32.setKey("32");  
  
        Button btn33 = new Button();  
        btn33.setName("幽默笑话");  
        btn33.setType("click");
        btn33.setKey("33");
  
        Button mainBtn1 = new Button();  
        mainBtn1.setName("生活助手");
        mainBtn1.setSub_button(new Button[] { btn11, btn12, btn13, btn14 });  
  
        Button mainBtn2 = new Button();
        mainBtn2.setName("休闲驿站");
        mainBtn2.setSub_button(new Button[] { btn21, btn22, btn23, btn24, btn25 });  
  
        Button mainBtn3 = new Button();  
        mainBtn3.setName("更多体验");  
        mainBtn3.setSub_button(new Button[] { btn31, btn32, btn33 });  
  
        /** 
         * 这是公众号xiaoqrobot目前的菜单结构，每个一级菜单都有二级菜单项<br> 
         *  
         * 在某个一级菜单下没有二级菜单的情况，menu该如何定义呢？<br> 
         * 比如，第三个一级菜单项不是“更多体验”，而直接是“幽默笑话”，那么menu应该这样定义：<br> 
         * menu.setButton(new Button[] { mainBtn1, mainBtn2, btn33 }); 
         */  
        Menu menu = new Menu();  
        menu.setButton(new Button[] { mainBtn1, mainBtn2, mainBtn3 });  
  
        return menu;  
    }  
	
}
