package com.fsd.core.wechat.main;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.fsd.core.common.BusinessException;
import com.fsd.core.wechat.entity.autoReply.MessageResponse;
import com.fsd.core.wechat.util.MessageUtil;

/**
 * 核心请求处理类
 */

public abstract class CoreService<T> {
	private static final Logger log = Logger.getLogger(CoreService.class);
	
	// 默认返回的文本消息内容
	public String respContent = "请求处理异常，请稍候尝试！";
	
	/**
	 * 直接回复处理消息
	 * @param request
	 * @return
	 */
	public String processRequest(HttpServletRequest request, String message) {
		// xml请求解析
		// 调用消息工具类MessageUtil解析微信发来的xml格式的消息，解析的结果放在HashMap里；
		Map<String, String> requestMap = null;
		try {
			requestMap = MessageUtil.parseXml(request);
		} catch (Exception e1) {
			e1.printStackTrace();
			return null;
		}
		// 发送方帐号（open_id）
		String fromUserName = requestMap.get("FromUserName");
		// 公众帐号
		String toUserName = requestMap.get("ToUserName");
		
		return MessageResponse.getTextMessage(fromUserName , toUserName , message); 
	}
	
	/**
	 * 处理微信发来的请求
	 * @param request
	 * @return
	 */
	public String processRequest(T tObj, HttpServletRequest request) {
		String respMessage = null;

		// xml请求解析
		// 调用消息工具类MessageUtil解析微信发来的xml格式的消息，解析的结果放在HashMap里；
		Map<String, String> requestMap = null;
		try {
			requestMap = MessageUtil.parseXml(request);
		} catch (Exception e1) {
			e1.printStackTrace();
			return null;
		}
		// 发送方帐号（open_id）
		String fromUserName = requestMap.get("FromUserName");
		// 公众帐号
		String toUserName = requestMap.get("ToUserName");
		// 消息类型
		String msgType = requestMap.get("MsgType");
		// 消息内容
		String content = requestMap.get("Content");
		
		try {
			
			// 文本消息
			if(msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_TEXT)) {
				respMessage = textHandle(tObj, fromUserName, toUserName, content);
			}
			// 图片消息
			else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_IMAGE)) {
				respMessage = imageHandle(tObj, fromUserName, toUserName, content);
			}
			// 地理位置消息
			else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_LOCATION)) {
				respMessage = locationHandle(tObj, fromUserName, toUserName, content);
			}
			// 链接消息
			else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_LINK)) {
				respMessage = linkHandle(tObj, fromUserName, toUserName, content);
			}
			// 音频消息
			else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_VOICE)) {
				respMessage = voiceHandle(tObj, fromUserName, toUserName, content);
			}
			// 事件推送
			else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_EVENT)) {
				// 事件类型
				String eventType = requestMap.get("Event");
				// 订阅
				if (eventType.equals(MessageUtil.EVENT_TYPE_SUBSCRIBE)) {
					respMessage = subscribeHandle(tObj, fromUserName, toUserName);
				}
				// 取消订阅
				else if(eventType.equals(MessageUtil.EVENT_TYPE_UNSUBSCRIBE)){
					respMessage = unsubscribeHandle(tObj, fromUserName, toUserName);
				}
				// 自定义菜单点击事件
				else if(eventType.equals(MessageUtil.EVENT_TYPE_CLICK)){
					String eventKey = requestMap.get("EventKey");// 事件KEY值，与创建自定义菜单时指定的
					respMessage = clickHandle(tObj, fromUserName, toUserName, eventKey);
				}
				// 扫码事件
				else if(eventType.equals(MessageUtil.EVENT_TYPE_SCAN)){
					String eventKey = requestMap.get("EventKey");// 传过来的字符串类型参数，这里是用户编号
					respMessage = scanHandle(tObj, fromUserName, toUserName, eventKey);
				}
				else{
					respMessage = MessageResponse.getTextMessage(fromUserName , toUserName , "应用服务器未处理事件类型"); 
				}
			}
			// 其他
			else{
				respMessage = MessageResponse.getTextMessage(fromUserName , toUserName , "应用服务器未处理消息类型"); 
			}
		}catch (BusinessException be){
			log.error("微信-应用服务器处理异常!" + be.getMessage());
        	System.out.println("微信-应用服务器处理异常!" + be.getMessage());
        	for (int i = 0; i < be.getStackTrace().length; i++){
            	log.error(be.getStackTrace()[i]);
        	}
        	be.printStackTrace();
        	respMessage = MessageResponse.getTextMessage(fromUserName , toUserName , respContent + "自定义异常");
		} catch (Exception e) {
			log.error("微信-应用服务器未处理异常!" + e.getMessage());
        	System.out.println("微信-应用服务器未处理异常!" + e.getMessage());
        	for (int i = 0; i < e.getStackTrace().length; i++){
            	log.error(e.getStackTrace()[i]);
        	}
        	e.printStackTrace();
        	respMessage = MessageResponse.getTextMessage(fromUserName , toUserName , respContent + "系统异常");
		}

		return respMessage;
	}

	public abstract String textHandle(T tObj, String fromUserName, String toUserName, String content) throws Exception;
	public abstract String imageHandle(T tObj, String fromUserName, String toUserName, String content) throws Exception;
	public abstract String linkHandle(T tObj, String fromUserName, String toUserName, String content) throws Exception;
	public abstract String locationHandle(T tObj, String fromUserName, String toUserName, String content) throws Exception;
	public abstract String voiceHandle(T tObj, String fromUserName, String toUserName, String content) throws Exception;
	public abstract String subscribeHandle(T tObj, String fromUserName, String toUserName) throws Exception;
	public abstract String unsubscribeHandle(T tObj, String fromUserName, String toUserName) throws Exception;
	public abstract String clickHandle(T tObj, String fromUserName, String toUserName, String eventKey) throws Exception;
	public abstract String scanHandle(T tObj, String fromUserName, String toUserName, String scene_str) throws Exception;
	
}
