package com.fsd.core.wechat.util;

import java.util.Arrays;

/** 
 * 请求校验工具类 
 */

public class SignUtil {  

	
    /** 
     * 验证签名 
     * @param signature 
     * @param timestamp 
     * @param nonce 
     * @return 
     */  
    public static boolean checkSignatureTest(String signature, String timestamp, String nonce) {
        // 与接口配置信息中的Token要一致  
    	String[] arr = new String[] { Constants.TOKEN, timestamp, nonce };  
        // 将token、timestamp、nonce三个参数进行字典序排序  
        Arrays.sort(arr);
        String bigStr = arr[0] + arr[1] + arr[2];
        // SHA1加密  
        String digest = new SHA1().getDigestOfString(bigStr.getBytes()).toLowerCase();
        System.out.println("加密排序后的字符串："+digest);
        // 将sha1加密后的字符串可与signature对比，标识该请求来源于微信  
        return digest != null ? digest.equals(signature) : false;
    }
    
    /** 
     * 验证签名 
     * @param signature 
     * @param timestamp 
     * @param nonce 
     * @return 
     */  
    public static boolean checkSignature(String token, String signature, String timestamp, String nonce) {
        // 与接口配置信息中的Token要一致  
    	String[] arr = new String[] { token, timestamp, nonce };  
        // 将token、timestamp、nonce三个参数进行字典序排序  
        Arrays.sort(arr);
        String bigStr = arr[0] + arr[1] + arr[2];
        // SHA1加密  
        String digest = new SHA1().getDigestOfString(bigStr.getBytes()).toLowerCase();
        System.out.println("加密排序后的字符串："+digest);
        // 将sha1加密后的字符串可与signature对比，标识该请求来源于微信  
        return digest != null ? digest.equals(signature) : false;
    }
}  

