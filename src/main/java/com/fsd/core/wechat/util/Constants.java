package com.fsd.core.wechat.util;

/**
 * 常量
 */

public class Constants {
	/**
	 * APPID
	 */
	public static String APPID = "wx5f5a5d198c0627d3";
	/**
	 * SECRET
	 */
	public static String SECRET = "da58541f3e11eed6fd39dc53c5a56b98";
	/**
	 * 获取ACCESS_TOKEN接口 限200（次/天）
	 */
	public static String GET_ACCESSTOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";
	/**
	 * 菜单创建接口（POST） 限100（次/天）
	 */
	public static String MENU_CREATE_URL = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=ACCESS_TOKEN";
	/**
	 * 菜单创建接口（POST） 限100（次/天）
	 */
	public static String MENU_QUERY_URL = "https://api.weixin.qq.com/cgi-bin/menu/get?access_token=ACCESS_TOKEN";
	/**
	 * ACCESS_TOKEN有效时间(单位：ms)
	 */
	public static int EFFECTIVE_TIME = 700000;
	/**
	 * conf.properties文件路径
	 */
	public static String CONF_PROPERTIES_PATH = "src/conf/config.properties";
	/**
	 * 微信接入token ，用于验证微信接口
	 */
	public static String TOKEN = "lumingbao0228";
	/**
	 * 获得用户详情接口
	 */
	public static String GET_USERINFO_URL = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=ACCESS_TOKEN&openid=OPENID";
	/**
	 * 获取二维码ticket接口 限100000（次/天）
	 */
	public static String GET_TICKET_URL = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=ACCESS_TOKEN";
	/**
	 * 通过ticket换取二维码地址
	 */
	public static String SHOW_QRCODE_URL = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=TICKET";
	/**
	 * 模板消息接口
	 */
	public static String MESSAGE_TEMPLATE_URL = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN";
	/**
	 * 获得jsapi_ticket接口
	 */
	public static String GET_JSAPI_TICKET_URL = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=ACCESS_TOKEN&type=jsapi";
	/**
	 * 网页授权通过CODE换取ACCESSTOKEN接口
	 */
	public static String GET_OAUTH_ACCESSTOKEN_URL = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code";
	/**
	 * 缓存ACCESS_TOKEN 使用的KEY（获得的ACCESSTOKEN两小时有效，每天有次数限制，重复获取会导致之前的ACCESSTOKEN失效，为了保证业务，ACCESSTOKEN需要缓存起来，失效以后再去获取）
	 */
	public static String ACCESS_TOKEN = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=SECRET";
	/**
	 * 缓存JSAPI_TICKET 使用的KEY（有限时间为两个小时，每天有次数相纸，频繁访问会导致访问次数达到而影响正常业务，所以缓存起来）
	 */
	public static String JSAPI_TICKET = "JSAPI_TICKET";
	/**
	 * 缓存OAUTH_ACCESSTOKEN 使用的KEY
	 */
	public static String OAUTH_ACCESSTOKEN = "OAUTH_ACCESSTOKEN";
	/**
	 * 缓存ACCESS_TOKEN时间 使用的KEY
	 */
	public static String ACCESS_TOKEN_TIME = "ACCESS_TOKEN_TIME";
	/**
	 * 缓存JSAPI_TICKET时间使用的KEY
	 */
	public static String JSAPI_TICKET_TIME = "JSAPI_TICKET_TIME";
	/**
	 * 缓存OAUTH_ACCESSTOKEN时间使用的KEY
	 */
	public static String OAUTH_ACCESSTOKEN_TIME = "OAUTH_ACCESSTOKEN_TIME";
}
