package com.fsd.core.bean;

/**
 * 异常信息实体类
 * @author lumingbao
 */
public class ExceptionInfo {
	
	private Boolean success = false;
	private Boolean isException;//是否抛出异常，抛出异常则success为false，优先级大于success
	private Boolean isSessionOut;//session是否失效标志
	private Boolean isNoAuthority;//无权限标志
	private String type;//异常类型
	private String title;//异常标题
	private String message;//异常信息
	private String details;//异常详细信息
	
	public Boolean getSuccess() {
		return success;
	}
	public void setSuccess(Boolean success) {
		this.success = success;
	}
	public Boolean getIsException() {
		return isException;
	}
	public void setIsException(Boolean isException) {
		this.isException = isException;
	}
	public Boolean getIsSessionOut() {
		return isSessionOut;
	}
	public void setIsSessionOut(Boolean isSessionOut) {
		this.isSessionOut = isSessionOut;
	}
	public Boolean getIsNoAuthority() {
		return isNoAuthority;
	}
	public void setIsNoAuthority(Boolean isNoAuthority) {
		this.isNoAuthority = isNoAuthority;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
}
