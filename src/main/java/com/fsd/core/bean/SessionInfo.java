package com.fsd.core.bean;

/**
 * Session管理实体类
 * @author Administrator
 */
public class SessionInfo {
	
	public String id;
	public String username;
	public String ip;
	public String zhsj;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getZhsj() {
		return zhsj;
	}
	public void setZhsj(String zhsj) {
		this.zhsj = zhsj;
	}
}
