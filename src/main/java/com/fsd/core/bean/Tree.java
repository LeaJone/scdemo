package com.fsd.core.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Extjs Tree 实体类
 * @author lumingbao
 */

public class Tree {
    private String id;
    private String text;
    private String textold;//原值
    private boolean leaf;
    private String icon;
    private String module;
    private List<Tree> children;
    private int num = 0;
    private boolean expanded = false;
    private Object obj = null;
    private String IsDialog;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
		this.textold = text;
	}
	public void setTextNew(String text) {
		this.text = text;
	}
	public String getTextOld() {
		return textold;
	}
	public boolean isLeaf() {
		return leaf;
	}
	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public void setAddNum(int num) {
		this.num += num;
	}
	public List<Tree> getChildren() {
		if (this.children == null){
			this.children = new ArrayList<Tree>();
		}
		return children;
	}
	public void setChildren(List<Tree> children) {
		this.children = children;
	}
    
	public boolean isExpanded() {
		return expanded;
	}
	public void setExpanded(boolean expanded) {
		this.expanded = expanded;
	}
	
	public Object getObj() {
		return obj;
	}
	public void setObj(Object obj) {
		this.obj = obj;
	}
	public String getIsDialog() {
		return IsDialog;
	}
	public void setIsDialog(String isDialog) {
		IsDialog = isDialog;
	}
}
