package com.fsd.core.bean;

/**
 * Extjs Tree （带复选框）实体类
 * @author lumingbao
 */

public class CheckTree extends Tree{
	
	private boolean checked = false;

	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
}
