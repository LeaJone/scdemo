package com.fsd.core.bean;

/**
 * 文件实体类
 * @author fsd
 */
public class FsdFile {
	
	private String name; //名称
	private String size; //大小
	private String updateTime; //修改时间
	private String type; //类型
	private String url; //路劲
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
}
