package com.fsd.core.bean;

/**
 * Excel导入bean
 * @author lumigbao
 */
public class ImportExcel {
	
	private String sheet;
	private String ksh;
	private String ksl;
	private String hs;
	private String ls;
	private String excel;
	
	public String getSheet() {
		return sheet;
	}
	public void setSheet(String sheet) {
		this.sheet = sheet;
	}
	public String getKsh() {
		return ksh;
	}
	public void setKsh(String ksh) {
		this.ksh = ksh;
	}
	public String getKsl() {
		return ksl;
	}
	public void setKsl(String ksl) {
		this.ksl = ksl;
	}
	public String getHs() {
		return hs;
	}
	public void setHs(String hs) {
		this.hs = hs;
	}
	public String getLs() {
		return ls;
	}
	public void setLs(String ls) {
		this.ls = ls;
	}
	public String getExcel() {
		return excel;
	}
	public void setExcel(String excel) {
		this.excel = excel;
	}
}
