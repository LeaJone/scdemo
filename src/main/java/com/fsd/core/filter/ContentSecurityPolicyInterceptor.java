package com.fsd.core.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * 添加Content-Security-Policy头部信息
 * @author lumingbao
 */
public class ContentSecurityPolicyInterceptor extends HandlerInterceptorAdapter {
	
	//预处理
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		response.addHeader("Content-Security-Policy", "default-src 'unsafe-inline' 'unsafe-eval' 'self' http://dcs.conac.cn http://121.43.68.40 http://c.cnzz.com http://hzs17.cnzz.com http://hzs15.cnzz.com http://www.gsdlr.gov.cn http://v3.jiathis.com http://s.jiathis.com http://cnzz.mmstat.com http://pcookie.cnzz.com http://res.wx.qq.com/open/js/jweixin-1.2.0.js");
		return super.preHandle(request, response, handler);
	}
}
