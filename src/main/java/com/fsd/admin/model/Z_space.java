package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="z_space")
public class Z_space extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String f_name;
	private String f_abstract;
	private String f_vrurl;
	private String f_ds;
	private String f_rs;
	private String f_rzqy;
	private String f_zxwcxjj;
	private String f_scjj;
	private String f_schd;
	private String f_cyjs;
	private String f_zscq;
	private String f_adddate;
	private String f_addemployeeid;
	private String f_addemployeename;
	private String f_lastupdatedate;
	private String f_updateemployeeid;
	private String f_updateemployeename;
	private String f_deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getF_name() {
		return f_name;
	}
	public void setF_name(String f_name) {
		this.f_name = f_name;
	}
    
	public String getF_abstract() {
		return f_abstract;
	}
	public void setF_abstract(String f_abstract) {
		this.f_abstract = f_abstract;
	}
    
	public String getF_vrurl() {
		return f_vrurl;
	}
	public void setF_vrurl(String f_vrurl) {
		this.f_vrurl = f_vrurl;
	}
    
	public String getF_ds() {
		return f_ds;
	}
	public void setF_ds(String f_ds) {
		this.f_ds = f_ds;
	}
    
	public String getF_rs() {
		return f_rs;
	}
	public void setF_rs(String f_rs) {
		this.f_rs = f_rs;
	}
    
	public String getF_rzqy() {
		return f_rzqy;
	}
	public void setF_rzqy(String f_rzqy) {
		this.f_rzqy = f_rzqy;
	}
    
	public String getF_zxwcxjj() {
		return f_zxwcxjj;
	}
	public void setF_zxwcxjj(String f_zxwcxjj) {
		this.f_zxwcxjj = f_zxwcxjj;
	}
    
	public String getF_scjj() {
		return f_scjj;
	}
	public void setF_scjj(String f_scjj) {
		this.f_scjj = f_scjj;
	}
    
	public String getF_schd() {
		return f_schd;
	}
	public void setF_schd(String f_schd) {
		this.f_schd = f_schd;
	}
    
	public String getF_cyjs() {
		return f_cyjs;
	}
	public void setF_cyjs(String f_cyjs) {
		this.f_cyjs = f_cyjs;
	}
    
	public String getF_zscq() {
		return f_zscq;
	}
	public void setF_zscq(String f_zscq) {
		this.f_zscq = f_zscq;
	}
    
	public String getF_adddate() {
		return f_adddate;
	}
	public void setF_adddate(String f_adddate) {
		this.f_adddate = f_adddate;
	}
    
	public String getF_addemployeeid() {
		return f_addemployeeid;
	}
	public void setF_addemployeeid(String f_addemployeeid) {
		this.f_addemployeeid = f_addemployeeid;
	}
    
	public String getF_addemployeename() {
		return f_addemployeename;
	}
	public void setF_addemployeename(String f_addemployeename) {
		this.f_addemployeename = f_addemployeename;
	}
    
	public String getF_lastupdatedate() {
		return f_lastupdatedate;
	}
	public void setF_lastupdatedate(String f_lastupdatedate) {
		this.f_lastupdatedate = f_lastupdatedate;
	}
    
	public String getF_updateemployeeid() {
		return f_updateemployeeid;
	}
	public void setF_updateemployeeid(String f_updateemployeeid) {
		this.f_updateemployeeid = f_updateemployeeid;
	}
    
	public String getF_updateemployeename() {
		return f_updateemployeename;
	}
	public void setF_updateemployeename(String f_updateemployeename) {
		this.f_updateemployeename = f_updateemployeename;
	}
    
	public String getF_deleted() {
		return f_deleted;
	}
	public void setF_deleted(String f_deleted) {
		this.f_deleted = f_deleted;
	}
    
}
