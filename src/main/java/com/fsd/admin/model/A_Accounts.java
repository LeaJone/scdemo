package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="A_Accounts")
public class A_Accounts extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String belongsid;
	private String belongsname;
	private String bank;
	private String accounts;
	private String remark;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getBelongsid() {
		return belongsid;
	}
	public void setBelongsid(String belongsid) {
		this.belongsid = belongsid;
	}
    
	public String getBelongsname() {
		return belongsname;
	}
	public void setBelongsname(String belongsname) {
		this.belongsname = belongsname;
	}
    
	public String getBank() {
		return bank;
	}
	public void setBank(String bank) {
		this.bank = bank;
	}
    
	public String getAccounts() {
		return accounts;
	}
	public void setAccounts(String accounts) {
		this.accounts = accounts;
	}
    
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
    
}
