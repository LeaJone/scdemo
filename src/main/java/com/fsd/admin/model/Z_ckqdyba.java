package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="z_ckqdyba")
public class Z_ckqdyba extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String companyid;
	private String gwwh;
	private String ksmc;
	private String xkzh;
	private String yxqx;
	private String dyr;
	private String dyqr;
	private String dyqx;
	private String baqx;
	private String ggrq;
	private String jczt;
	private String remark;
	private String adddate;
	private String addemployeeid;
	private String addemployeename;
	private String updatedate;
	private String updateemployeeid;
	private String updateemployeename;
	private String deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getCompanyid() {
		return companyid;
	}
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
    
	public String getGwwh() {
		return gwwh;
	}
	public void setGwwh(String gwwh) {
		this.gwwh = gwwh;
	}
    
	public String getKsmc() {
		return ksmc;
	}
	public void setKsmc(String ksmc) {
		this.ksmc = ksmc;
	}
    
	public String getXkzh() {
		return xkzh;
	}
	public void setXkzh(String xkzh) {
		this.xkzh = xkzh;
	}
    
	public String getYxqx() {
		return yxqx;
	}
	public void setYxqx(String yxqx) {
		this.yxqx = yxqx;
	}
    
	public String getDyr() {
		return dyr;
	}
	public void setDyr(String dyr) {
		this.dyr = dyr;
	}
    
	public String getDyqr() {
		return dyqr;
	}
	public void setDyqr(String dyqr) {
		this.dyqr = dyqr;
	}
    
	public String getDyqx() {
		return dyqx;
	}
	public void setDyqx(String dyqx) {
		this.dyqx = dyqx;
	}
    
	public String getBaqx() {
		return baqx;
	}
	public void setBaqx(String baqx) {
		this.baqx = baqx;
	}
    
	public String getGgrq() {
		return ggrq;
	}
	public void setGgrq(String ggrq) {
		this.ggrq = ggrq;
	}
    
	public String getJczt() {
		return jczt;
	}
	public void setJczt(String jczt) {
		this.jczt = jczt;
	}
    
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
    
	public String getAdddate() {
		return adddate;
	}
	public void setAdddate(String adddate) {
		this.adddate = adddate;
	}
    
	public String getAddemployeeid() {
		return addemployeeid;
	}
	public void setAddemployeeid(String addemployeeid) {
		this.addemployeeid = addemployeeid;
	}
    
	public String getAddemployeename() {
		return addemployeename;
	}
	public void setAddemployeename(String addemployeename) {
		this.addemployeename = addemployeename;
	}
    
	public String getUpdatedate() {
		return updatedate;
	}
	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}
    
	public String getUpdateemployeeid() {
		return updateemployeeid;
	}
	public void setUpdateemployeeid(String updateemployeeid) {
		this.updateemployeeid = updateemployeeid;
	}
    
	public String getUpdateemployeename() {
		return updateemployeename;
	}
	public void setUpdateemployeename(String updateemployeename) {
		this.updateemployeename = updateemployeename;
	}
    
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
    
}
