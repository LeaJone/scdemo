package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

/**
 * 数据库备份
 * @author Administrator
 *
 */
@Entity
@Table(name = "Sys_Databackup")
public class Sys_Databackup extends BaseModel {
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String filename; // 文件名称
	private String filepath; // 文件路径
	private String filesize; // 文件大小 
	private String backupdate; // 备份时间
	private String backupemployeeid; // 备份用户ID
	private String backupemployeename; // 备份用户名称
	private String deleted; // 是否删除 0-未删除，1-删除
	
	@Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getFilepath() {
		return filepath;
	}
	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}
	public String getFilesize() {
		return filesize;
	}
	public void setFilesize(String filesize) {
		this.filesize = filesize;
	}
	public String getBackupdate() {
		return backupdate;
	}
	public void setBackupdate(String backupdate) {
		this.backupdate = backupdate;
	}
	public String getBackupemployeeid() {
		return backupemployeeid;
	}
	public void setBackupemployeeid(String backupemployeeid) {
		this.backupemployeeid = backupemployeeid;
	}
	public String getBackupemployeename() {
		return backupemployeename;
	}
	public void setBackupemployeename(String backupemployeename) {
		this.backupemployeename = backupemployeename;
	}
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
}
