package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

/**
 * 项目计划任务书
 * @author Administrator
 *
 */
@Entity
@Table(name="f_prospectus")
public class F_prospectus extends BaseModel {
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String f_projectid; //项目编号
	private String f_projectname; //项目名称
	private String f_projectcontent; //项目提交内容
	private String f_submituserid; //提交人编号
	private String f_submitusername; //提交人姓名
	private String f_submittime; //提交时间
	private String f_submitstatus; //提交状态
	private String f_accessory; //附件
	private String f_deleted; //删除标志
	
	@Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getF_projectid() {
		return f_projectid;
	}
	public void setF_projectid(String f_projectid) {
		this.f_projectid = f_projectid;
	}
	public String getF_projectname() {
		return f_projectname;
	}
	public void setF_projectname(String f_projectname) {
		this.f_projectname = f_projectname;
	}
	public String getF_projectcontent() {
		return f_projectcontent;
	}
	public void setF_projectcontent(String f_projectcontent) {
		this.f_projectcontent = f_projectcontent;
	}
	public String getF_submituserid() {
		return f_submituserid;
	}
	public void setF_submituserid(String f_submituserid) {
		this.f_submituserid = f_submituserid;
	}
	public String getF_submitusername() {
		return f_submitusername;
	}
	public void setF_submitusername(String f_submitusername) {
		this.f_submitusername = f_submitusername;
	}
	public String getF_submittime() {
		return f_submittime;
	}
	public void setF_submittime(String f_submittime) {
		this.f_submittime = f_submittime;
	}
	public String getF_submitstatus() {
		return f_submitstatus;
	}
	public void setF_submitstatus(String f_submitstatus) {
		this.f_submitstatus = f_submitstatus;
	}
	public String getF_accessory() {
		return f_accessory;
	}
	public void setF_accessory(String f_accessory) {
		this.f_accessory = f_accessory;
	}
	public String getF_deleted() {
		return f_deleted;
	}
	public void setF_deleted(String f_deleted) {
		this.f_deleted = f_deleted;
	}
}
