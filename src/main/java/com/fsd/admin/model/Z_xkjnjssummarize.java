package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="z_xkjnjssummarize")
public class Z_xkjnjssummarize extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String f_fid;
	private String f_joingroup;
	private String f_joinpeople;
	private String f_prizegroup;
	private String f_prizepeople;
	private String f_matchtime;
	private String f_matchdays;
	private String f_money;
	private String f_background;
	private String f_implementcourse;
	private String f_matchcourse;
	private String f_achievement;
	private String f_summarize;
	private String f_question;
	private String f_innovate;
	private String f_attachment1;
	private String f_attachment2;
	private String f_attachment3;
	private String f_attachment4;
	private String f_attachment5;
	private String f_statusid;
	private String f_statusname;
	private String f_adddate;
	private String f_addemployeeid;
	private String f_addemployeename;
	private String f_lastupdatedate;
	private String f_updateemployeeid;
	private String f_updateemployeename;
	private String f_deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getF_fid() {
		return f_fid;
	}
	public void setF_fid(String f_fid) {
		this.f_fid = f_fid;
	}
    
	public String getF_joingroup() {
		return f_joingroup;
	}
	public void setF_joingroup(String f_joingroup) {
		this.f_joingroup = f_joingroup;
	}
    
	public String getF_joinpeople() {
		return f_joinpeople;
	}
	public void setF_joinpeople(String f_joinpeople) {
		this.f_joinpeople = f_joinpeople;
	}
    
	public String getF_prizegroup() {
		return f_prizegroup;
	}
	public void setF_prizegroup(String f_prizegroup) {
		this.f_prizegroup = f_prizegroup;
	}
    
	public String getF_prizepeople() {
		return f_prizepeople;
	}
	public void setF_prizepeople(String f_prizepeople) {
		this.f_prizepeople = f_prizepeople;
	}
    
	public String getF_matchtime() {
		return f_matchtime;
	}
	public void setF_matchtime(String f_matchtime) {
		this.f_matchtime = f_matchtime;
	}
    
	public String getF_matchdays() {
		return f_matchdays;
	}
	public void setF_matchdays(String f_matchdays) {
		this.f_matchdays = f_matchdays;
	}
    
	public String getF_money() {
		return f_money;
	}
	public void setF_money(String f_money) {
		this.f_money = f_money;
	}
    
	public String getF_background() {
		return f_background;
	}
	public void setF_background(String f_background) {
		this.f_background = f_background;
	}
    
	public String getF_implementcourse() {
		return f_implementcourse;
	}
	public void setF_implementcourse(String f_implementcourse) {
		this.f_implementcourse = f_implementcourse;
	}
    
	public String getF_matchcourse() {
		return f_matchcourse;
	}
	public void setF_matchcourse(String f_matchcourse) {
		this.f_matchcourse = f_matchcourse;
	}
    
	public String getF_achievement() {
		return f_achievement;
	}
	public void setF_achievement(String f_achievement) {
		this.f_achievement = f_achievement;
	}
    
	public String getF_summarize() {
		return f_summarize;
	}
	public void setF_summarize(String f_summarize) {
		this.f_summarize = f_summarize;
	}
    
	public String getF_question() {
		return f_question;
	}
	public void setF_question(String f_question) {
		this.f_question = f_question;
	}
    
	public String getF_innovate() {
		return f_innovate;
	}
	public void setF_innovate(String f_innovate) {
		this.f_innovate = f_innovate;
	}
    
	public String getF_attachment1() {
		return f_attachment1;
	}
	public void setF_attachment1(String f_attachment1) {
		this.f_attachment1 = f_attachment1;
	}
    
	public String getF_attachment2() {
		return f_attachment2;
	}
	public void setF_attachment2(String f_attachment2) {
		this.f_attachment2 = f_attachment2;
	}
    
	public String getF_attachment3() {
		return f_attachment3;
	}
	public void setF_attachment3(String f_attachment3) {
		this.f_attachment3 = f_attachment3;
	}
    
	public String getF_attachment4() {
		return f_attachment4;
	}
	public void setF_attachment4(String f_attachment4) {
		this.f_attachment4 = f_attachment4;
	}
    
	public String getF_attachment5() {
		return f_attachment5;
	}
	public void setF_attachment5(String f_attachment5) {
		this.f_attachment5 = f_attachment5;
	}
    
	public String getF_adddate() {
		return f_adddate;
	}
	public void setF_adddate(String f_adddate) {
		this.f_adddate = f_adddate;
	}

	public String getF_statusid() {
		return f_statusid;
	}

	public void setF_statusid(String f_statusid) {
		this.f_statusid = f_statusid;
	}

	public String getF_statusname() {
		return f_statusname;
	}

	public void setF_statusname(String f_statusname) {
		this.f_statusname = f_statusname;
	}

	public String getF_addemployeeid() {
		return f_addemployeeid;
	}
	public void setF_addemployeeid(String f_addemployeeid) {
		this.f_addemployeeid = f_addemployeeid;
	}
    
	public String getF_addemployeename() {
		return f_addemployeename;
	}
	public void setF_addemployeename(String f_addemployeename) {
		this.f_addemployeename = f_addemployeename;
	}
    
	public String getF_lastupdatedate() {
		return f_lastupdatedate;
	}
	public void setF_lastupdatedate(String f_lastupdatedate) {
		this.f_lastupdatedate = f_lastupdatedate;
	}
    
	public String getF_updateemployeeid() {
		return f_updateemployeeid;
	}
	public void setF_updateemployeeid(String f_updateemployeeid) {
		this.f_updateemployeeid = f_updateemployeeid;
	}
    
	public String getF_updateemployeename() {
		return f_updateemployeename;
	}
	public void setF_updateemployeename(String f_updateemployeename) {
		this.f_updateemployeename = f_updateemployeename;
	}
    
	public String getF_deleted() {
		return f_deleted;
	}
	public void setF_deleted(String f_deleted) {
		this.f_deleted = f_deleted;
	}
    
}
