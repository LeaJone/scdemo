package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

/**
 * 课程信息
 * @author Administrator
 *
 */
@Entity
@Table(name="t_course")
public class T_course extends BaseModel{
    private static final long serialVersionUID = 1L;
    
	private String id;
	private String f_coursetypeid; //课程分类编号
	private String f_coursetypename; //课程分类名称
	private String f_title; //课程标题
	private String f_abstract; //课程简介
	private Long f_required; //是否必修
	private Long f_score; //课程分数
	private String f_imageurl; //图片路径
	private String f_flow; //课程流程
	private Long f_order; //排序编号
	private String f_islink;//是否外链
	private String f_link;//连接地址
	private String f_state; //状态
	private String f_adduser; //添加人员
	private String f_adddate; //添加时间
	private String f_updateuser; //修改人员
	private String f_updatedate; //修改时间
	private Long f_deleted; //删除标志
    private Long f_recommend; //是否推荐
    private String f_expirationtime; //课程到期时间
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getF_coursetypeid() {
		return f_coursetypeid;
	}
	public void setF_coursetypeid(String f_coursetypeid) {
		this.f_coursetypeid = f_coursetypeid;
	}
    
	public String getF_title() {
		return f_title;
	}
	public void setF_title(String f_title) {
		this.f_title = f_title;
	}
    
	public String getF_abstract() {
		return f_abstract;
	}
	public void setF_abstract(String f_abstract) {
		this.f_abstract = f_abstract;
	}
    
	public Long getF_required() {
		return f_required;
	}
	public void setF_required(Long f_required) {
		this.f_required = f_required;
	}
    
	public Long getF_score() {
		return f_score;
	}
	public void setF_score(Long f_score) {
		this.f_score = f_score;
	}
    
	public String getF_imageurl() {
		return f_imageurl;
	}
	public void setF_imageurl(String f_imageurl) {
		this.f_imageurl = f_imageurl;
	}
    
	public String getF_flow() {
		return f_flow;
	}
	public void setF_flow(String f_flow) {
		this.f_flow = f_flow;
	}
    
	public Long getF_order() {
		return f_order;
	}
	public void setF_order(Long f_order) {
		this.f_order = f_order;
	}

	public String getF_islink() {
		return f_islink;
	}
	public void setF_islink(String f_islink) {
		this.f_islink = f_islink;
	}

	public String getF_link() {
		return f_link;
	}
	public void setF_link(String f_link) {
		this.f_link = f_link;
	}

	public String getF_state() {
		return f_state;
	}
	public void setF_state(String f_state) {
		this.f_state = f_state;
	}
    
	public String getF_adduser() {
		return f_adduser;
	}
	public void setF_adduser(String f_adduser) {
		this.f_adduser = f_adduser;
	}
    
	public String getF_adddate() {
		return f_adddate;
	}
	public void setF_adddate(String f_adddate) {
		this.f_adddate = f_adddate;
	}
    
	public String getF_updateuser() {
		return f_updateuser;
	}
	public void setF_updateuser(String f_updateuser) {
		this.f_updateuser = f_updateuser;
	}
    
	public String getF_updatedate() {
		return f_updatedate;
	}
	public void setF_updatedate(String f_updatedate) {
		this.f_updatedate = f_updatedate;
	}
    
	public Long getF_deleted() {
		return f_deleted;
	}
	public void setF_deleted(Long f_deleted) {
		this.f_deleted = f_deleted;
	}
	public Long getF_recommend() {
		return f_recommend;
	}
	public void setF_recommend(Long f_recommend) {
		this.f_recommend = f_recommend;
	}
	public String getF_expirationtime() {
		return f_expirationtime;
	}
	public void setF_expirationtime(String f_expirationtime) {
		this.f_expirationtime = f_expirationtime;
	}
	public String getF_coursetypename() {
		return f_coursetypename;
	}
	public void setF_coursetypename(String f_coursetypename) {
		this.f_coursetypename = f_coursetypename;
	}
    
}
