package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="z_tkq")
public class Z_tkq extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String companyid;
	private String xkzh;
	private String xmmc;
	private String sqr;
	private String kcdw;
	private String kckzbm;
	private String kckzcm;
	private String yxqq;
	private String yxqz;
	private String zmj;
	private String djq;
	private String djz;
	private String bwq;
	private String bwz;
	private String remark;
	private String adddate;
	private String addemployeeid;
	private String addemployeename;
	private String updatedate;
	private String updateemployeeid;
	private String updateemployeename;
	private String deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getCompanyid() {
		return companyid;
	}
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
    
	public String getXkzh() {
		return xkzh;
	}
	public void setXkzh(String xkzh) {
		this.xkzh = xkzh;
	}
    
	public String getXmmc() {
		return xmmc;
	}
	public void setXmmc(String xmmc) {
		this.xmmc = xmmc;
	}
    
	public String getSqr() {
		return sqr;
	}
	public void setSqr(String sqr) {
		this.sqr = sqr;
	}
    
	public String getKcdw() {
		return kcdw;
	}
	public void setKcdw(String kcdw) {
		this.kcdw = kcdw;
	}
    
	public String getKckzbm() {
		return kckzbm;
	}
	public void setKckzbm(String kckzbm) {
		this.kckzbm = kckzbm;
	}
    
	public String getKckzcm() {
		return kckzcm;
	}
	public void setKckzcm(String kckzcm) {
		this.kckzcm = kckzcm;
	}
    
	public String getYxqq() {
		return yxqq;
	}
	public void setYxqq(String yxqq) {
		this.yxqq = yxqq;
	}
    
	public String getYxqz() {
		return yxqz;
	}
	public void setYxqz(String yxqz) {
		this.yxqz = yxqz;
	}
    
	public String getZmj() {
		return zmj;
	}
	public void setZmj(String zmj) {
		this.zmj = zmj;
	}
    
	public String getDjq() {
		return djq;
	}
	public void setDjq(String djq) {
		this.djq = djq;
	}
    
	public String getDjz() {
		return djz;
	}
	public void setDjz(String djz) {
		this.djz = djz;
	}
    
	public String getBwq() {
		return bwq;
	}
	public void setBwq(String bwq) {
		this.bwq = bwq;
	}
    
	public String getBwz() {
		return bwz;
	}
	public void setBwz(String bwz) {
		this.bwz = bwz;
	}
    
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
    
	public String getAdddate() {
		return adddate;
	}
	public void setAdddate(String adddate) {
		this.adddate = adddate;
	}
    
	public String getAddemployeeid() {
		return addemployeeid;
	}
	public void setAddemployeeid(String addemployeeid) {
		this.addemployeeid = addemployeeid;
	}
    
	public String getAddemployeename() {
		return addemployeename;
	}
	public void setAddemployeename(String addemployeename) {
		this.addemployeename = addemployeename;
	}
    
	public String getUpdatedate() {
		return updatedate;
	}
	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}
    
	public String getUpdateemployeeid() {
		return updateemployeeid;
	}
	public void setUpdateemployeeid(String updateemployeeid) {
		this.updateemployeeid = updateemployeeid;
	}
    
	public String getUpdateemployeename() {
		return updateemployeename;
	}
	public void setUpdateemployeename(String updateemployeename) {
		this.updateemployeename = updateemployeename;
	}
    
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
    
}
