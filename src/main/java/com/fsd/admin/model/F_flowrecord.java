package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="f_flowrecord")
public class F_flowrecord extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String cmpanyid;
	private String projectid;
	private String projectcode;
	private String projectname;
	private String operatepath;
	private String operatename;
	private String entitypath;
	private String entityname;
	private String windowpath;
	private String windowname;
	private String dataobjectid;
	private String dataobjectname;
	private String nodeid;
	private String nodecode;
	private String nodename;
	private String nodetype;
	private String nodetypename;
	private String previousid;
	private String previousname;
	private String nextid;
	private String nextname;
	private String nexttype;
	private String nexttypename;
	private String falseid;
	private String falsename;
	private String isextract;
	private String isdeliver;
	private String isrefusal;
	private String isreturn;
	private String flowobjectid;
	private String nextrecordid;
	private String dealtstatus;
	private String dealtstatusname;
	private String startdate;
	private String enddate;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getCmpanyid() {
		return cmpanyid;
	}
	public void setCmpanyid(String cmpanyid) {
		this.cmpanyid = cmpanyid;
	}
    
	public String getProjectid() {
		return projectid;
	}
	public void setProjectid(String projectid) {
		this.projectid = projectid;
	}
    
	public String getProjectcode() {
		return projectcode;
	}
	public void setProjectcode(String projectcode) {
		this.projectcode = projectcode;
	}
    
	public String getProjectname() {
		return projectname;
	}
	public void setProjectname(String projectname) {
		this.projectname = projectname;
	}
    
	public String getOperatepath() {
		return operatepath;
	}
	public void setOperatepath(String operatepath) {
		this.operatepath = operatepath;
	}
    
	public String getOperatename() {
		return operatename;
	}
	public void setOperatename(String operatename) {
		this.operatename = operatename;
	}
    
	public String getEntitypath() {
		return entitypath;
	}
	public void setEntitypath(String entitypath) {
		this.entitypath = entitypath;
	}
    
	public String getEntityname() {
		return entityname;
	}
	public void setEntityname(String entityname) {
		this.entityname = entityname;
	}
    
	public String getWindowpath() {
		return windowpath;
	}
	public void setWindowpath(String windowpath) {
		this.windowpath = windowpath;
	}
    
	public String getWindowname() {
		return windowname;
	}
	public void setWindowname(String windowname) {
		this.windowname = windowname;
	}
    
	public String getDataobjectid() {
		return dataobjectid;
	}
	public void setDataobjectid(String dataobjectid) {
		this.dataobjectid = dataobjectid;
	}
    
	public String getDataobjectname() {
		return dataobjectname;
	}
	public void setDataobjectname(String dataobjectname) {
		this.dataobjectname = dataobjectname;
	}
    
	public String getNodeid() {
		return nodeid;
	}
	public void setNodeid(String nodeid) {
		this.nodeid = nodeid;
	}
    
	public String getNodename() {
		return nodename;
	}
	public void setNodename(String nodename) {
		this.nodename = nodename;
	}
    
	public String getNodetype() {
		return nodetype;
	}
	public void setNodetype(String nodetype) {
		this.nodetype = nodetype;
	}
    
	public String getNodetypename() {
		return nodetypename;
	}
	public void setNodetypename(String nodetypename) {
		this.nodetypename = nodetypename;
	}
    
	public String getPreviousid() {
		return previousid;
	}
	public void setPreviousid(String previousid) {
		this.previousid = previousid;
	}
    
	public String getPreviousname() {
		return previousname;
	}
	public void setPreviousname(String previousname) {
		this.previousname = previousname;
	}
    
	public String getNextid() {
		return nextid;
	}
	public void setNextid(String nextid) {
		this.nextid = nextid;
	}
    
	public String getNextname() {
		return nextname;
	}
	public void setNextname(String nextname) {
		this.nextname = nextname;
	}
    
	public String getNexttype() {
		return nexttype;
	}
	public void setNexttype(String nexttype) {
		this.nexttype = nexttype;
	}
	
	public String getNexttypename() {
		return nexttypename;
	}
	public void setNexttypename(String nexttypename) {
		this.nexttypename = nexttypename;
	}
    
	public String getFalseid() {
		return falseid;
	}
	public void setFalseid(String falseid) {
		this.falseid = falseid;
	}
    
	public String getFalsename() {
		return falsename;
	}
	public void setFalsename(String falsename) {
		this.falsename = falsename;
	}
    
	public String getFlowobjectid() {
		return flowobjectid;
	}
	public void setFlowobjectid(String flowobjectid) {
		this.flowobjectid = flowobjectid;
	}
    
	public String getNextrecordid() {
		return nextrecordid;
	}
	public void setNextrecordid(String nextrecordid) {
		this.nextrecordid = nextrecordid;
	}
	
	public String getDealtstatus() {
		return dealtstatus;
	}
	public void setDealtstatus(String dealtstatus) {
		this.dealtstatus = dealtstatus;
	}
    
	public String getDealtstatusname() {
		return dealtstatusname;
	}
	public void setDealtstatusname(String dealtstatusname) {
		this.dealtstatusname = dealtstatusname;
	}
    
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
    
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	
	public String getIsextract() {
		return isextract;
	}
	public void setIsextract(String isextract) {
		this.isextract = isextract;
	}
	
	public String getNodecode() {
		return nodecode;
	}
	public void setNodecode(String nodecode) {
		this.nodecode = nodecode;
	}
	
	public String getIsdeliver() {
		return isdeliver;
	}
	public void setIsdeliver(String isdeliver) {
		this.isdeliver = isdeliver;
	}
	
	public String getIsrefusal() {
		return isrefusal;
	}
	public void setIsrefusal(String isrefusal) {
		this.isrefusal = isrefusal;
	}
	
	public String getIsreturn() {
		return isreturn;
	}
	public void setIsreturn(String isreturn) {
		this.isreturn = isreturn;
	}
}
