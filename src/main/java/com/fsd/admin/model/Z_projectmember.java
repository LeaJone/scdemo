package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="z_projectmember")
public class Z_projectmember extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String f_projectid;
	private String f_memberid;
	private String f_studentnumber;
	private String f_membername;
	private String f_memberphone;
	private String f_memberclass;
	private String f_membercollege;
	private String f_memberresponsibility;
	private String f_adddate;
	private String f_addemployeeid;
	private String f_addemployeename;
	private String f_lastupdatedate;
	private String f_updateemployeeid;
	private String f_updateemployeename;
	private String f_deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getF_projectid() {
		return f_projectid;
	}
	public void setF_projectid(String f_projectid) {
		this.f_projectid = f_projectid;
	}
    
	public String getF_memberid() {
		return f_memberid;
	}
	public void setF_memberid(String f_memberid) {
		this.f_memberid = f_memberid;
	}
    
	public String getF_studentnumber() {
		return f_studentnumber;
	}
	public void setF_studentnumber(String f_studentnumber) {
		this.f_studentnumber = f_studentnumber;
	}
    
	public String getF_membername() {
		return f_membername;
	}
	public void setF_membername(String f_membername) {
		this.f_membername = f_membername;
	}
    
	public String getF_memberphone() {
		return f_memberphone;
	}
	public void setF_memberphone(String f_memberphone) {
		this.f_memberphone = f_memberphone;
	}
    
	public String getF_memberclass() {
		return f_memberclass;
	}
	public void setF_memberclass(String f_memberclass) {
		this.f_memberclass = f_memberclass;
	}
    
	public String getF_membercollege() {
		return f_membercollege;
	}
	public void setF_membercollege(String f_membercollege) {
		this.f_membercollege = f_membercollege;
	}
    
	public String getF_memberresponsibility() {
		return f_memberresponsibility;
	}
	public void setF_memberresponsibility(String f_memberresponsibility) {
		this.f_memberresponsibility = f_memberresponsibility;
	}
    
	public String getF_adddate() {
		return f_adddate;
	}
	public void setF_adddate(String f_adddate) {
		this.f_adddate = f_adddate;
	}
    
	public String getF_addemployeeid() {
		return f_addemployeeid;
	}
	public void setF_addemployeeid(String f_addemployeeid) {
		this.f_addemployeeid = f_addemployeeid;
	}
    
	public String getF_addemployeename() {
		return f_addemployeename;
	}
	public void setF_addemployeename(String f_addemployeename) {
		this.f_addemployeename = f_addemployeename;
	}
    
	public String getF_lastupdatedate() {
		return f_lastupdatedate;
	}
	public void setF_lastupdatedate(String f_lastupdatedate) {
		this.f_lastupdatedate = f_lastupdatedate;
	}
    
	public String getF_updateemployeeid() {
		return f_updateemployeeid;
	}
	public void setF_updateemployeeid(String f_updateemployeeid) {
		this.f_updateemployeeid = f_updateemployeeid;
	}
    
	public String getF_updateemployeename() {
		return f_updateemployeename;
	}
	public void setF_updateemployeename(String f_updateemployeename) {
		this.f_updateemployeename = f_updateemployeename;
	}
    
	public String getF_deleted() {
		return f_deleted;
	}
	public void setF_deleted(String f_deleted) {
		this.f_deleted = f_deleted;
	}
    
}
