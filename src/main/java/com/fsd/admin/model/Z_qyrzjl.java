package com.fsd.admin.model;

import com.fsd.core.model.BaseModel;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="z_qyrzjl")
public class Z_qyrzjl extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String qyid;
	private String qymc;
	private String time;
	private String type;
	private String remark;

    
    @Id
	public String getId() { return id; }
	public void setId(String id) {
		this.id = id;
	}

	public String getQyid() {
		return qyid;
	}

	public void setQyid(String qyid) {
		this.qyid = qyid;
	}

	public String getQymc() {
		return qymc;
	}

	public void setQymc(String qymc) {
		this.qymc = qymc;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}
