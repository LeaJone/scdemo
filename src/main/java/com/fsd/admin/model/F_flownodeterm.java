package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="f_flownodeterm")
public class F_flownodeterm extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String companyid;
	private String nodeid;
	private String nodename;
	private String propertyname;
	private String propertydepict;
	private String propertytype;
	private String propertytypename;
	private String operator;
	private String operatorname;
	private String valuewaycode;
	private String valuewayname;
	private String valueinfo;
	private String valuedepict;
	private Long sort;
	private String adddate;
	private String addemployeeid;
	private String addemployeename;
	private String updatedate;
	private String updateemployeeid;
	private String updateemployeename;
	private String deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getCompanyid() {
		return companyid;
	}
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
    
	public String getNodeid() {
		return nodeid;
	}
	public void setNodeid(String nodeid) {
		this.nodeid = nodeid;
	}
    
	public String getNodename() {
		return nodename;
	}
	public void setNodename(String nodename) {
		this.nodename = nodename;
	}
    
	public String getPropertyname() {
		return propertyname;
	}
	public void setPropertyname(String propertyname) {
		this.propertyname = propertyname;
	}
    
	public String getPropertydepict() {
		return propertydepict;
	}
	public void setPropertydepict(String propertydepict) {
		this.propertydepict = propertydepict;
	}
    
	public String getPropertytype() {
		return propertytype;
	}
	public void setPropertytype(String propertytype) {
		this.propertytype = propertytype;
	}
    
	public String getPropertytypename() {
		return propertytypename;
	}
	public void setPropertytypename(String propertytypename) {
		this.propertytypename = propertytypename;
	}
    
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
    
	public String getOperatorname() {
		return operatorname;
	}
	public void setOperatorname(String operatorname) {
		this.operatorname = operatorname;
	}
    
	public String getValuewaycode() {
		return valuewaycode;
	}
	public void setValuewaycode(String valuewaycode) {
		this.valuewaycode = valuewaycode;
	}
	
	public String getValuewayname() {
		return valuewayname;
	}
	public void setValuewayname(String valuewayname) {
		this.valuewayname = valuewayname;
	}
	
	public String getValueinfo() {
		return valueinfo;
	}
	public void setValueinfo(String valueinfo) {
		this.valueinfo = valueinfo;
	}
    
	public String getValuedepict() {
		return valuedepict;
	}
	public void setValuedepict(String valuedepict) {
		this.valuedepict = valuedepict;
	}
    
	public Long getSort() {
		return sort;
	}
	public void setSort(Long sort) {
		this.sort = sort;
	}
    
	public String getAdddate() {
		return adddate;
	}
	public void setAdddate(String adddate) {
		this.adddate = adddate;
	}
    
	public String getAddemployeeid() {
		return addemployeeid;
	}
	public void setAddemployeeid(String addemployeeid) {
		this.addemployeeid = addemployeeid;
	}
    
	public String getAddemployeename() {
		return addemployeename;
	}
	public void setAddemployeename(String addemployeename) {
		this.addemployeename = addemployeename;
	}
    
	public String getUpdatedate() {
		return updatedate;
	}
	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}
    
	public String getUpdateemployeeid() {
		return updateemployeeid;
	}
	public void setUpdateemployeeid(String updateemployeeid) {
		this.updateemployeeid = updateemployeeid;
	}
    
	public String getUpdateemployeename() {
		return updateemployeename;
	}
	public void setUpdateemployeename(String updateemployeename) {
		this.updateemployeename = updateemployeename;
	}
    
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
    
}
