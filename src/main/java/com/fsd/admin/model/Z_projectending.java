package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="z_projectending")
public class Z_projectending extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String f_projectid;
	private String f_projectname;
	private String f_executeid;
	private String f_executename;
	private String f_background;
	private String f_progressachievement;
	private String f_achievement;
	private String f_moneyuse;
	private String f_adddate;
	private String f_addemployeeid;
	private String f_addemployeename;
	private String f_lastupdatedate;
	private String f_updateemployeeid;
	private String f_updateemployeename;
	private String f_deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getF_projectid() {
		return f_projectid;
	}
	public void setF_projectid(String f_projectid) {
		this.f_projectid = f_projectid;
	}
    
	public String getF_projectname() {
		return f_projectname;
	}
	public void setF_projectname(String f_projectname) {
		this.f_projectname = f_projectname;
	}
    
	public String getF_executeid() {
		return f_executeid;
	}
	public void setF_executeid(String f_executeid) {
		this.f_executeid = f_executeid;
	}
    
	public String getF_executename() {
		return f_executename;
	}
	public void setF_executename(String f_executename) {
		this.f_executename = f_executename;
	}
    
	public String getF_background() {
		return f_background;
	}
	public void setF_background(String f_background) {
		this.f_background = f_background;
	}
    
	public String getF_progressachievement() {
		return f_progressachievement;
	}
	public void setF_progressachievement(String f_progressachievement) {
		this.f_progressachievement = f_progressachievement;
	}
    
	public String getF_achievement() {
		return f_achievement;
	}
	public void setF_achievement(String f_achievement) {
		this.f_achievement = f_achievement;
	}
    
	public String getF_moneyuse() {
		return f_moneyuse;
	}
	public void setF_moneyuse(String f_moneyuse) {
		this.f_moneyuse = f_moneyuse;
	}
    
	public String getF_adddate() {
		return f_adddate;
	}
	public void setF_adddate(String f_adddate) {
		this.f_adddate = f_adddate;
	}
    
	public String getF_addemployeeid() {
		return f_addemployeeid;
	}
	public void setF_addemployeeid(String f_addemployeeid) {
		this.f_addemployeeid = f_addemployeeid;
	}
    
	public String getF_addemployeename() {
		return f_addemployeename;
	}
	public void setF_addemployeename(String f_addemployeename) {
		this.f_addemployeename = f_addemployeename;
	}
    
	public String getF_lastupdatedate() {
		return f_lastupdatedate;
	}
	public void setF_lastupdatedate(String f_lastupdatedate) {
		this.f_lastupdatedate = f_lastupdatedate;
	}
    
	public String getF_updateemployeeid() {
		return f_updateemployeeid;
	}
	public void setF_updateemployeeid(String f_updateemployeeid) {
		this.f_updateemployeeid = f_updateemployeeid;
	}
    
	public String getF_updateemployeename() {
		return f_updateemployeename;
	}
	public void setF_updateemployeename(String f_updateemployeename) {
		this.f_updateemployeename = f_updateemployeename;
	}
    
	public String getF_deleted() {
		return f_deleted;
	}
	public void setF_deleted(String f_deleted) {
		this.f_deleted = f_deleted;
	}
    
}
