package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="f_flowproject")
public class F_flowproject extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String companyid;
	private String typeid;
	private String typename;
	private String code;
	private String name;
	private String describe;
	private String operatepath;
	private String operatename;
	private String entitypath;
	private String entityname;
	private String windowpath;
	private String windowname;
	private String status;
	private String statusname;
	private String acceptcode;
	private String acceptname;
	private String x;
	private String y;
	private String width;
	private String height;
	private Long sort;
	private String adddate;
	private String addemployeeid;
	private String addemployeename;
	private String updatedate;
	private String updateemployeeid;
	private String updateemployeename;
	private String deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getCompanyid() {
		return companyid;
	}
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
    
	public Long getSort() {
		return sort;
	}
	public void setSort(Long sort) {
		this.sort = sort;
	}
	public String getTypeid() {
		return typeid;
	}
	public void setTypeid(String typeid) {
		this.typeid = typeid;
	}
    
	public String getTypename() {
		return typename;
	}
	public void setTypename(String typename) {
		this.typename = typename;
	}
	public String getAdddate() {
		return adddate;
	}
	public void setAdddate(String adddate) {
		this.adddate = adddate;
	}
	public String getAddemployeeid() {
		return addemployeeid;
	}
	public void setAddemployeeid(String addemployeeid) {
		this.addemployeeid = addemployeeid;
	}
	public String getAddemployeename() {
		return addemployeename;
	}
	public void setAddemployeename(String addemployeename) {
		this.addemployeename = addemployeename;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
    
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
    
	public String getDescribe() {
		return describe;
	}
	public void setDescribe(String describe) {
		this.describe = describe;
	}
    
	public String getOperatepath() {
		return operatepath;
	}
	public void setOperatepath(String operatepath) {
		this.operatepath = operatepath;
	}
    
	public String getOperatename() {
		return operatename;
	}
	public void setOperatename(String operatename) {
		this.operatename = operatename;
	}
    
	public String getEntitypath() {
		return entitypath;
	}
	public void setEntitypath(String entitypath) {
		this.entitypath = entitypath;
	}
    
	public String getEntityname() {
		return entityname;
	}
	public void setEntityname(String entityname) {
		this.entityname = entityname;
	}
    
	public String getWindowpath() {
		return windowpath;
	}
	public void setWindowpath(String windowpath) {
		this.windowpath = windowpath;
	}
    
	public String getWindowname() {
		return windowname;
	}
	public void setWindowname(String windowname) {
		this.windowname = windowname;
	}
	
	public String getX() {
		return x;
	}
	public void setX(String x) {
		this.x = x;
	}
	
	public String getY() {
		return y;
	}
	public void setY(String y) {
		this.y = y;
	}
	
	public String getWidth() {
		return width;
	}
	public void setWidth(String width) {
		this.width = width;
	}
	
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getStatusname() {
		return statusname;
	}
	public void setStatusname(String statusname) {
		this.statusname = statusname;
	}
	
	public String getAcceptcode() {
		return acceptcode;
	}
	public void setAcceptcode(String acceptcode) {
		this.acceptcode = acceptcode;
	}
	
	public String getAcceptname() {
		return acceptname;
	}
	public void setAcceptname(String acceptname) {
		this.acceptname = acceptname;
	}
	
	public String getUpdatedate() {
		return updatedate;
	}
	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}
	
	public String getUpdateemployeeid() {
		return updateemployeeid;
	}
	public void setUpdateemployeeid(String updateemployeeid) {
		this.updateemployeeid = updateemployeeid;
	}
	
	public String getUpdateemployeename() {
		return updateemployeename;
	}
	public void setUpdateemployeename(String updateemployeename) {
		this.updateemployeename = updateemployeename;
	}
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
    
}
