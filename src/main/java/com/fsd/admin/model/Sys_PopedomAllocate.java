package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="Sys_PopedomAllocate")
public class Sys_PopedomAllocate extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String popedomid;
	private String popedomtype;
	private String belongid;
    
	@Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPopedomid() {
		return popedomid;
	}
	public void setPopedomid(String popedomid) {
		this.popedomid = popedomid;
	}
	public String getPopedomtype() {
		return popedomtype;
	}
	public void setPopedomtype(String popedomtype) {
		this.popedomtype = popedomtype;
	}
	public String getBelongid() {
		return belongid;
	}
	public void setBelongid(String belongid) {
		this.belongid = belongid;
	}
}
