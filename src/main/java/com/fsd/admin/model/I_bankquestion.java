package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="i_bankquestion")
public class I_bankquestion extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String companyid;
	private String bankinfoid;
	private String bankinfoname;
	private String typecode;
	private String typename;
	private String levelcode;
	private String levelname;
	private String keynotecode;
	private String keynotename;
	private String questiontypeid;
	private String questiontypename;
	private String auditing;
	private String problem;
	private String content;
	private String keynote;
	private String parse;
	private Long sort;
	private String remark;
	private String adddate;
	private String addemployeeid;
	private String addemployeename;
	private String updatedate;
	private String updateemployeeid;
	private String updateemployeename;
	private String deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getCompanyid() {
		return companyid;
	}
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
	
	public String getAuditing() {
		return auditing;
	}
	public void setAuditing(String auditing) {
		this.auditing = auditing;
	}
    
	public String getBankinfoid() {
		return bankinfoid;
	}
	public void setBankinfoid(String bankinfoid) {
		this.bankinfoid = bankinfoid;
	}
    
	public String getBankinfoname() {
		return bankinfoname;
	}
	public void setBankinfoname(String bankinfoname) {
		this.bankinfoname = bankinfoname;
	}
    
	public String getTypecode() {
		return typecode;
	}
	public void setTypecode(String typecode) {
		this.typecode = typecode;
	}
    
	public String getTypename() {
		return typename;
	}
	public void setTypename(String typename) {
		this.typename = typename;
	}
    
	public String getLevelcode() {
		return levelcode;
	}
	public void setLevelcode(String levelcode) {
		this.levelcode = levelcode;
	}
    
	public String getLevelname() {
		return levelname;
	}
	public void setLevelname(String levelname) {
		this.levelname = levelname;
	}
    
	public String getKeynotecode() {
		return keynotecode;
	}
	public void setKeynotecode(String keynotecode) {
		this.keynotecode = keynotecode;
	}
	
	public String getKeynotename() {
		return keynotename;
	}
	public void setKeynotename(String keynotename) {
		this.keynotename = keynotename;
	}
    
	public String getQuestiontypeid() {
		return questiontypeid;
	}
	public void setQuestiontypeid(String questiontypeid) {
		this.questiontypeid = questiontypeid;
	}
    
	public String getQuestiontypename() {
		return questiontypename;
	}
	public void setQuestiontypename(String questiontypename) {
		this.questiontypename = questiontypename;
	}
    
	public String getProblem() {
		return problem;
	}
	public void setProblem(String problem) {
		this.problem = problem;
	}
    
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
    
	public String getKeynote() {
		return keynote;
	}
	public void setKeynote(String keynote) {
		this.keynote = keynote;
	}
    
	public String getParse() {
		return parse;
	}
	public void setParse(String parse) {
		this.parse = parse;
	}
    
	public Long getSort() {
		return sort;
	}
	public void setSort(Long sort) {
		this.sort = sort;
	}
    
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
    
	public String getAdddate() {
		return adddate;
	}
	public void setAdddate(String adddate) {
		this.adddate = adddate;
	}
    
	public String getAddemployeeid() {
		return addemployeeid;
	}
	public void setAddemployeeid(String addemployeeid) {
		this.addemployeeid = addemployeeid;
	}
    
	public String getAddemployeename() {
		return addemployeename;
	}
	public void setAddemployeename(String addemployeename) {
		this.addemployeename = addemployeename;
	}
    
	public String getUpdatedate() {
		return updatedate;
	}
	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}
    
	public String getUpdateemployeeid() {
		return updateemployeeid;
	}
	public void setUpdateemployeeid(String updateemployeeid) {
		this.updateemployeeid = updateemployeeid;
	}
    
	public String getUpdateemployeename() {
		return updateemployeename;
	}
	public void setUpdateemployeename(String updateemployeename) {
		this.updateemployeename = updateemployeename;
	}
    
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
    
}
