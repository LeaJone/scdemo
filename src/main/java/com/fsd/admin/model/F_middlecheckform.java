package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="f_middlecheckform")
public class F_middlecheckform extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String f_projectid;
	private String f_projectname;
	private String f_projectprogress;
	private String f_projectphaseresult;
	private String f_mainproblem;
	private String f_resourceuse;
	private String f_projectfunds;
	private String f_existproblem;
	private String f_accessory;
	private String f_submituserid;
	private String f_submitusername;
	private String f_submittime;
	private String f_submitstatus;
	private String f_deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getF_projectid() {
		return f_projectid;
	}
	public void setF_projectid(String f_projectid) {
		this.f_projectid = f_projectid;
	}
    
	public String getF_projectname() {
		return f_projectname;
	}
	public void setF_projectname(String f_projectname) {
		this.f_projectname = f_projectname;
	}
    
	public String getF_projectprogress() {
		return f_projectprogress;
	}
	public void setF_projectprogress(String f_projectprogress) {
		this.f_projectprogress = f_projectprogress;
	}
    
	public String getF_projectphaseresult() {
		return f_projectphaseresult;
	}
	public void setF_projectphaseresult(String f_projectphaseresult) {
		this.f_projectphaseresult = f_projectphaseresult;
	}
    
	public String getF_mainproblem() {
		return f_mainproblem;
	}
	public void setF_mainproblem(String f_mainproblem) {
		this.f_mainproblem = f_mainproblem;
	}
    
	public String getF_resourceuse() {
		return f_resourceuse;
	}
	public void setF_resourceuse(String f_resourceuse) {
		this.f_resourceuse = f_resourceuse;
	}
    
	public String getF_projectfunds() {
		return f_projectfunds;
	}
	public void setF_projectfunds(String f_projectfunds) {
		this.f_projectfunds = f_projectfunds;
	}
    
	public String getF_existproblem() {
		return f_existproblem;
	}
	public void setF_existproblem(String f_existproblem) {
		this.f_existproblem = f_existproblem;
	}
    
	public String getF_accessory() {
		return f_accessory;
	}
	public void setF_accessory(String f_accessory) {
		this.f_accessory = f_accessory;
	}
    
	public String getF_submituserid() {
		return f_submituserid;
	}
	public void setF_submituserid(String f_submituserid) {
		this.f_submituserid = f_submituserid;
	}
    
	public String getF_submitusername() {
		return f_submitusername;
	}
	public void setF_submitusername(String f_submitusername) {
		this.f_submitusername = f_submitusername;
	}
    
	public String getF_submittime() {
		return f_submittime;
	}
	public void setF_submittime(String f_submittime) {
		this.f_submittime = f_submittime;
	}
    
	public String getF_submitstatus() {
		return f_submitstatus;
	}
	public void setF_submitstatus(String f_submitstatus) {
		this.f_submitstatus = f_submitstatus;
	}
    
	public String getF_deleted() {
		return f_deleted;
	}
	public void setF_deleted(String f_deleted) {
		this.f_deleted = f_deleted;
	}
    
}
