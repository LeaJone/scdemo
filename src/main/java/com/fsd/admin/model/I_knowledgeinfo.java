package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="i_knowledgeinfo")
public class I_knowledgeinfo extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String companyid;
	private String bankinfoid;
	private String bankinfoname;
	private String levelcode;
	private String levelname;
	private String keynotecode;
	private String keynotename;
	private String questiontypeid;
	private String questiontypename;
	private String title;
	private String subtitle;
	private String showtitle;
	private String abstracts;
	private String content;
	private String keynote;
	private String parse;
	private String aurl;
	private String isaurl;
	private String source;
	private String sourceurl;
	private String sourceisurl;
	private Long sort;
	private String remark;
	private String imageurl1;
	private String imageurl2;
	private String videourl;
	private String voiceurl;
	private String statuscode;
	private String statusname;
	private String auditing;
	private String auditingdate;
	private String auditingempid;
	private String auditingempname;
	private String adddate;
	private String addemployeeid;
	private String addemployeename;
	private String updatedate;
	private String updateemployeeid;
	private String updateemployeename;
	private String deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getCompanyid() {
		return companyid;
	}
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
    
	public String getBankinfoid() {
		return bankinfoid;
	}
	public void setBankinfoid(String bankinfoid) {
		this.bankinfoid = bankinfoid;
	}
    
	public String getBankinfoname() {
		return bankinfoname;
	}
	public void setBankinfoname(String bankinfoname) {
		this.bankinfoname = bankinfoname;
	}
    
	public String getLevelcode() {
		return levelcode;
	}
	public void setLevelcode(String levelcode) {
		this.levelcode = levelcode;
	}
    
	public String getLevelname() {
		return levelname;
	}
	public void setLevelname(String levelname) {
		this.levelname = levelname;
	}
    
	public String getKeynotecode() {
		return keynotecode;
	}
	public void setKeynotecode(String keynotecode) {
		this.keynotecode = keynotecode;
	}
	
	public String getKeynotename() {
		return keynotename;
	}
	public void setKeynotename(String keynotename) {
		this.keynotename = keynotename;
	}
	
	public String getQuestiontypeid() {
		return questiontypeid;
	}
	public void setQuestiontypeid(String questiontypeid) {
		this.questiontypeid = questiontypeid;
	}
    
	public String getQuestiontypename() {
		return questiontypename;
	}
	public void setQuestiontypename(String questiontypename) {
		this.questiontypename = questiontypename;
	}
    
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
    
	public String getSubtitle() {
		return subtitle;
	}
	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
    
	public String getShowtitle() {
		return showtitle;
	}
	public void setShowtitle(String showtitle) {
		this.showtitle = showtitle;
	}
    
	public String getAbstracts() {
		return abstracts;
	}
	public void setAbstracts(String abstracts) {
		this.abstracts = abstracts;
	}
    
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
    
	public String getKeynote() {
		return keynote;
	}
	public void setKeynote(String keynote) {
		this.keynote = keynote;
	}
    
	public String getParse() {
		return parse;
	}
	public void setParse(String parse) {
		this.parse = parse;
	}
    
	public String getAurl() {
		return aurl;
	}
	public void setAurl(String aurl) {
		this.aurl = aurl;
	}
    
	public String getIsaurl() {
		return isaurl;
	}
	public void setIsaurl(String isaurl) {
		this.isaurl = isaurl;
	}
    
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
    
	public String getSourceurl() {
		return sourceurl;
	}
	public void setSourceurl(String sourceurl) {
		this.sourceurl = sourceurl;
	}
    
	public String getSourceisurl() {
		return sourceisurl;
	}
	public void setSourceisurl(String sourceisurl) {
		this.sourceisurl = sourceisurl;
	}
    
	public Long getSort() {
		return sort;
	}
	public void setSort(Long sort) {
		this.sort = sort;
	}
    
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
    
	public String getImageurl1() {
		return imageurl1;
	}
	public void setImageurl1(String imageurl1) {
		this.imageurl1 = imageurl1;
	}
    
	public String getImageurl2() {
		return imageurl2;
	}
	public void setImageurl2(String imageurl2) {
		this.imageurl2 = imageurl2;
	}
    
	public String getVideourl() {
		return videourl;
	}
	public void setVideourl(String videourl) {
		this.videourl = videourl;
	}
    
	public String getVoiceurl() {
		return voiceurl;
	}
	public void setVoiceurl(String voiceurl) {
		this.voiceurl = voiceurl;
	}
    
	public String getStatuscode() {
		return statuscode;
	}
	public void setStatuscode(String statuscode) {
		this.statuscode = statuscode;
	}
    
	public String getStatusname() {
		return statusname;
	}
	public void setStatusname(String statusname) {
		this.statusname = statusname;
	}
    
	public String getAuditing() {
		return auditing;
	}
	public void setAuditing(String auditing) {
		this.auditing = auditing;
	}
    
	public String getAuditingdate() {
		return auditingdate;
	}
	public void setAuditingdate(String auditingdate) {
		this.auditingdate = auditingdate;
	}
	
	public String getAuditingempid() {
		return auditingempid;
	}
	public void setAuditingempid(String auditingempid) {
		this.auditingempid = auditingempid;
	}
	
	public String getAuditingempname() {
		return auditingempname;
	}
	public void setAuditingempname(String auditingempname) {
		this.auditingempname = auditingempname;
	}
    
	public String getAdddate() {
		return adddate;
	}
	public void setAdddate(String adddate) {
		this.adddate = adddate;
	}
    
	public String getAddemployeeid() {
		return addemployeeid;
	}
	public void setAddemployeeid(String addemployeeid) {
		this.addemployeeid = addemployeeid;
	}
    
	public String getAddemployeename() {
		return addemployeename;
	}
	public void setAddemployeename(String addemployeename) {
		this.addemployeename = addemployeename;
	}
    
	public String getUpdatedate() {
		return updatedate;
	}
	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}
    
	public String getUpdateemployeeid() {
		return updateemployeeid;
	}
	public void setUpdateemployeeid(String updateemployeeid) {
		this.updateemployeeid = updateemployeeid;
	}
    
	public String getUpdateemployeename() {
		return updateemployeename;
	}
	public void setUpdateemployeename(String updateemployeename) {
		this.updateemployeename = updateemployeename;
	}
    
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
    
}
