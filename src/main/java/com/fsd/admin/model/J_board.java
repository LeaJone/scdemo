package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="j_board")
public class J_board extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String companyid;
	private String baseid;
	private String parentid;
	private String parentname;
	private String title;
	private String description;
	private String remark;
	private String imageurl;
	private String isleaf;
	private String ispublish;
	private Long sort;
	private String adddate;
	private String addemployeeid;
	private String addemployeename;
	private String updatedate;
	private String updateemployeeid;
	private String updateemployeename;
	private String deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getCompanyid() {
		return companyid;
	}
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
    
	public String getBaseid() {
		return baseid;
	}
	public void setBaseid(String baseid) {
		this.baseid = baseid;
	}
    
	public String getParentid() {
		return parentid;
	}
	public void setParentid(String parentid) {
		this.parentid = parentid;
	}
    
	public String getParentname() {
		return parentname;
	}
	public void setParentname(String parentname) {
		this.parentname = parentname;
	}
    
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
    
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getImageurl() {
		return imageurl;
	}
	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}
    
	public Long getSort() {
		return sort;
	}
	public void setSort(Long sort) {
		this.sort = sort;
	}
    
	public String getIspublish() {
		return ispublish;
	}
	public void setIspublish(String ispublish) {
		this.ispublish = ispublish;
	}

	public String getIsleaf() {
		return isleaf;
	}
	public void setIsleaf(String isleaf) {
		this.isleaf = isleaf;
	}
    
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
    
	public String getAdddate() {
		return adddate;
	}
	public void setAdddate(String adddate) {
		this.adddate = adddate;
	}
    
	public String getAddemployeeid() {
		return addemployeeid;
	}
	public void setAddemployeeid(String addemployeeid) {
		this.addemployeeid = addemployeeid;
	}
    
	public String getAddemployeename() {
		return addemployeename;
	}
	public void setAddemployeename(String addemployeename) {
		this.addemployeename = addemployeename;
	}
    
	public String getUpdatedate() {
		return updatedate;
	}
	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}
    
	public String getUpdateemployeeid() {
		return updateemployeeid;
	}
	public void setUpdateemployeeid(String updateemployeeid) {
		this.updateemployeeid = updateemployeeid;
	}
    
	public String getUpdateemployeename() {
		return updateemployeename;
	}
	public void setUpdateemployeename(String updateemployeename) {
		this.updateemployeename = updateemployeename;
	}
    
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
    
}
