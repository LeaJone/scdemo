package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="A_Employee")
public class A_Employee extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String companyid;
	private String companyname;
	private String branchid;
	private String branchname; // 专业 
	private String code; //学号（工号）-登录账号
	private String loginname;  // 登陆名
	private String password; // 登陆密码
	private String passwordtime;
	private String nickname; 
	private String realname; //个人姓名
	private String symbol;
	private String usertypeid;   
	private String usertypename; // 用户类型（学生、教师、专家）
	private String areatypeid;
	private String areatypename; 
	private String jobtypeid;
	private String jobtypename; //职务
	private String sex;   
	private String regtime;
	private String idcard; //身份证号
	private String sexname;   //性别
	private String birthday;
	private String birthyear;
	private String birthmonth;
	private String birthdays;
	private String natives;
	private String workaddress;  //工作单位
	private String homeaddress;
	private String marital;
	private String maritalname;
	private String iswechatbind;
	private String iswechatpush;
	private String qq;
	private String qualification;
	private String qualificationname;
	private String grade;
	private String gradename;
	private String position;
	private String positionname;
	private String address;
	private String postcode;
	private String email;  // 电子邮箱
	private String blood;
	private String bloodname;
	private String childnumber;
	private String bodyheight;
	private String weight;
	private String duty;
	private String mobile;  //手机号
	private String mobile1;
	private String mobile2;
	private String telephone;
	private String roomnumber;
	private String abstracts;
	private String type;
	private String typename;
	private String status;
	private String statusname;
	private String remark;
	private String imagelogo; //用户头像
	private String image1;
	private String image2;
	private Long sort;
	private String adddate;
	private String addemployeeid;
	private String addemployeename;
	private String updatedate;
	private String updateemployeeid;
	private String updateemployeename;
	private String deleted;
	private String registerclassid;
	private String registerclass; //班级 - 职称
	private String academyid; 
	private String academyname; //院系 
	private String xueliid;
	private String xueliname; //学历
	private String xueweiid;
	private String xueweiname; //学位
	private String resourceid;
	private String resourcename; //专家来源
	private String studymajorid;
	private String studymajorname; // 所学专业（专家）
	private String busymajorid;
	private String busymajorname; // 从事专业
	private String departmentadmin;//部门管理员
	private boolean isadmin = false;

    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getCompanyid() {
		return companyid;
	}
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
    
	public String getCompanyname() {
		return companyname;
	}
	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}
    
	public String getBranchid() {
		return branchid;
	}
	public void setBranchid(String branchid) {
		this.branchid = branchid;
	}
    
	public String getBranchname() {
		return branchname;
	}
	public void setBranchname(String branchname) {
		this.branchname = branchname;
	}
    
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
    
	public String getLoginname() {
		return loginname;
	}
	public void setLoginname(String loginname) {
		this.loginname = loginname;
	}
    
	public String getPasswordtime() {
		return passwordtime;
	}
	public void setPasswordtime(String passwordtime) {
		this.passwordtime = passwordtime;
	}
    
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
    
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	
	public String getRealname() {
		return realname;
	}
	public void setRealname(String realname) {
		this.realname = realname;
	}
    
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
    
	public String getUsertypeid() {
		return usertypeid;
	}
	public void setUsertypeid(String usertypeid) {
		this.usertypeid = usertypeid;
	}
    
	public String getUsertypename() {
		return usertypename;
	}
	public void setUsertypename(String usertypename) {
		this.usertypename = usertypename;
	}
    
	public String getAreatypeid() {
		return areatypeid;
	}
	public void setAreatypeid(String areatypeid) {
		this.areatypeid = areatypeid;
	}
    
	public String getAreatypename() {
		return areatypename;
	}
	public void setAreatypename(String areatypename) {
		this.areatypename = areatypename;
	}
    
	public String getJobtypeid() {
		return jobtypeid;
	}
	public void setJobtypeid(String jobtypeid) {
		this.jobtypeid = jobtypeid;
	}
    
	public String getJobtypename() {
		return jobtypename;
	}
	public void setJobtypename(String jobtypename) {
		this.jobtypename = jobtypename;
	}
    
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
    
	public String getSexname() {
		return sexname;
	}
	public void setSexname(String sexname) {
		this.sexname = sexname;
	}
    
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
    
	public String getNatives() {
		return natives;
	}
	public void setNatives(String natives) {
		this.natives = natives;
	}
    
	public String getMarital() {
		return marital;
	}
	public void setMarital(String marital) {
		this.marital = marital;
	}
    
	public String getMaritalname() {
		return maritalname;
	}
	public void setMaritalname(String maritalname) {
		this.maritalname = maritalname;
	}
    
	public String getIswechatbind() {
		return iswechatbind;
	}
	public void setIswechatbind(String iswechatbind) {
		this.iswechatbind = iswechatbind;
	}
	public String getIswechatpush() {
		return iswechatpush;
	}
	public void setIswechatpush(String iswechatpush) {
		this.iswechatpush = iswechatpush;
	}
	public String getQq() {
		return qq;
	}
	public void setQq(String qq) {
		this.qq = qq;
	}
    
	public String getQualification() {
		return qualification;
	}
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
    
	public String getQualificationname() {
		return qualificationname;
	}
	public void setQualificationname(String qualificationname) {
		this.qualificationname = qualificationname;
	}
    
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
    
	public String getGradename() {
		return gradename;
	}
	public void setGradename(String gradename) {
		this.gradename = gradename;
	}
    
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
    
	public String getPositionname() {
		return positionname;
	}
	public void setPositionname(String positionname) {
		this.positionname = positionname;
	}
    
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
    
	public String getPostcode() {
		return postcode;
	}
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
    
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
    
	public String getBlood() {
		return blood;
	}
	public void setBlood(String blood) {
		this.blood = blood;
	}
	
	public String getBloodname() {
		return bloodname;
	}
	public void setBloodname(String bloodname) {
		this.bloodname = bloodname;
	}
	
	public String getChildnumber() {
		return childnumber;
	}
	public void setChildnumber(String childnumber) {
		this.childnumber = childnumber;
	}
	
	public String getBodyheight() {
		return bodyheight;
	}
	public void setBodyheight(String bodyheight) {
		this.bodyheight = bodyheight;
	}
	
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	
	public String getDuty() {
		return duty;
	}
	public void setDuty(String duty) {
		this.duty = duty;
	}
	
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	public String getMobile1() {
		return mobile1;
	}
	public void setMobile1(String mobile1) {
		this.mobile1 = mobile1;
	}
	
	public String getMobile2() {
		return mobile2;
	}
	public void setMobile2(String mobile2) {
		this.mobile2 = mobile2;
	}
	
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	
	public String getRoomnumber() {
		return roomnumber;
	}
	public void setRoomnumber(String roomnumber) {
		this.roomnumber = roomnumber;
	}
	
	public String getAbstracts() {
		return abstracts;
	}
	public void setAbstracts(String abstracts) {
		this.abstracts = abstracts;
	}
    
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
    
	public String getTypename() {
		return typename;
	}
	public void setTypename(String typename) {
		this.typename = typename;
	}
    
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
    
	public String getStatusname() {
		return statusname;
	}
	public void setStatusname(String statusname) {
		this.statusname = statusname;
	}
    
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
    
	public String getImagelogo() {
		return imagelogo;
	}
	public void setImagelogo(String imagelogo) {
		this.imagelogo = imagelogo;
	}
    
	public String getImage1() {
		return image1;
	}
	public void setImage1(String image1) {
		this.image1 = image1;
	}
    
	public String getImage2() {
		return image2;
	}
	public void setImage2(String image2) {
		this.image2 = image2;
	}
    
	public Long getSort() {
		return sort;
	}
	public void setSort(Long sort) {
		this.sort = sort;
	}
    
	public String getAdddate() {
		return adddate;
	}
	public void setAdddate(String adddate) {
		this.adddate = adddate;
	}
    
	public String getAddemployeeid() {
		return addemployeeid;
	}
	public void setAddemployeeid(String addemployeeid) {
		this.addemployeeid = addemployeeid;
	}
    
	public String getAddemployeename() {
		return addemployeename;
	}
	public void setAddemployeename(String addemployeename) {
		this.addemployeename = addemployeename;
	}
    
	public String getUpdatedate() {
		return updatedate;
	}
	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}
    
	public String getUpdateemployeeid() {
		return updateemployeeid;
	}
	public void setUpdateemployeeid(String updateemployeeid) {
		this.updateemployeeid = updateemployeeid;
	}
    
	public String getUpdateemployeename() {
		return updateemployeename;
	}
	public void setUpdateemployeename(String updateemployeename) {
		this.updateemployeename = updateemployeename;
	}
    
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
	
	public String getRegtime() {
		return regtime;
	}
	public void setRegtime(String regtime) {
		this.regtime = regtime;
	}
	public String getIdcard() {
		return idcard;
	}
	public void setIdcard(String idcard) {
		this.idcard = idcard;
	}
	public String getBirthyear() {
		return birthyear;
	}
	public void setBirthyear(String birthyear) {
		this.birthyear = birthyear;
	}
	public String getBirthmonth() {
		return birthmonth;
	}
	public void setBirthmonth(String birthmonth) {
		this.birthmonth = birthmonth;
	}
	public String getBirthdays() {
		return birthdays;
	}
	public void setBirthdays(String birthdays) {
		this.birthdays = birthdays;
	}
	public String getWorkaddress() {
		return workaddress;
	}
	public void setWorkaddress(String workaddress) {
		this.workaddress = workaddress;
	}
	public String getHomeaddress() {
		return homeaddress;
	}
	public void setHomeaddress(String homeaddress) {
		this.homeaddress = homeaddress;
	}
	public String getRegisterclass() {
		return registerclass;
	}
	public void setRegisterclass(String registerclass) {
		this.registerclass = registerclass;
	}
	public String getAcademyid() {
		return academyid;
	}
	public void setAcademyid(String academyid) {
		this.academyid = academyid;
	}
	public String getAcademyname() {
		return academyname;
	}
	public void setAcademyname(String academyname) {
		this.academyname = academyname;
	}
	public String getRegisterclassid() {
		return registerclassid;
	}
	public void setRegisterclassid(String registerclassid) {
		this.registerclassid = registerclassid;
	}
	public String getXueliid() {
		return xueliid;
	}
	public void setXueliid(String xueliid) {
		this.xueliid = xueliid;
	}
	public String getXueliname() {
		return xueliname;
	}
	public void setXueliname(String xueliname) {
		this.xueliname = xueliname;
	}
	public String getXueweiid() {
		return xueweiid;
	}
	public void setXueweiid(String xueweiid) {
		this.xueweiid = xueweiid;
	}
	public String getXueweiname() {
		return xueweiname;
	}
	public void setXueweiname(String xueweiname) {
		this.xueweiname = xueweiname;
	}
	public String getResourceid() {
		return resourceid;
	}
	public void setResourceid(String resourceid) {
		this.resourceid = resourceid;
	}
	public String getResourcename() {
		return resourcename;
	}
	public void setResourcename(String resourcename) {
		this.resourcename = resourcename;
	}
	public String getStudymajorid() {
		return studymajorid;
	}
	public void setStudymajorid(String studymajorid) {
		this.studymajorid = studymajorid;
	}
	public String getStudymajorname() {
		return studymajorname;
	}
	public void setStudymajorname(String studymajorname) {
		this.studymajorname = studymajorname;
	}
	public String getBusymajorid() {
		return busymajorid;
	}
	public void setBusymajorid(String busymajorid) {
		this.busymajorid = busymajorid;
	}
	public String getBusymajorname() {
		return busymajorname;
	}
	public void setBusymajorname(String busymajorname) {
		this.busymajorname = busymajorname;
	}
	public String getDepartmentadmin() {
		return departmentadmin;
	}
	public void setDepartmentadmin(String departmentadmin) {
		this.departmentadmin = departmentadmin;
	}

	@Transient
	public boolean isIsadmin() {
		return isadmin;
	}
	public void setIsadmin(boolean isadmin) {
		this.isadmin = isadmin;
	}
}
