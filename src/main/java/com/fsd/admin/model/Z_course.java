package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="z_course")
public class Z_course extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String f_typeid;
	private String f_typename;
	private String f_propertyid;
	private String f_propertyname;
	private String f_collegeid;
	private String f_collegename;
	private String f_teacherid;
	private String f_name;
	private String f_teacher;
	private String f_statusid;
	private String f_statusname;//审核状态
	private String f_reason;//审核拒绝理由
	private String f_score;
	private String f_period;
	private String f_state;//启用状态
	private String f_count;
	private String f_platformcode;
	private String f_platform;
	private String f_url;
	private String f_pdfurl;
	private String f_site;
	private String f_wayid;
	private String f_wayname;
	private String f_xingqi;
	private String f_jieci;
	private String f_semester;
	private String f_remark;
	private String f_abstract;
	private String f_adddate;
	private String f_addemployeeid;
	private String f_addemployeename;
	private String f_lastupdatedate;
	private String f_updateemployeeid;
	private String f_updateemployeename;
	private String f_deleted;
	private Integer f_sort;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getF_typeid() {
		return f_typeid;
	}
	public void setF_typeid(String f_typeid) {
		this.f_typeid = f_typeid;
	}
    
	public String getF_typename() {
		return f_typename;
	}
	public void setF_typename(String f_typename) {
		this.f_typename = f_typename;
	}
	
	public String getF_propertyid() {
		return f_propertyid;
	}
	public void setF_propertyid(String f_propertyid) {
		this.f_propertyid = f_propertyid;
	}
	public String getF_propertyname() {
		return f_propertyname;
	}
	public void setF_propertyname(String f_propertyname) {
		this.f_propertyname = f_propertyname;
	}
	public String getF_collegeid() {
		return f_collegeid;
	}
	public void setF_collegeid(String f_collegeid) {
		this.f_collegeid = f_collegeid;
	}

	public String getF_collegename() {
		return f_collegename;
	}
	public void setF_collegename(String f_collegename) {
		this.f_collegename = f_collegename;
	}

	public String getF_teacherid() {
		return f_teacherid;
	}
	public void setF_teacherid(String f_teacherid) {
		this.f_teacherid = f_teacherid;
	}

	public String getF_name() {
		return f_name;
	}
	public void setF_name(String f_name) {
		this.f_name = f_name;
	}
    
	public String getF_teacher() {
		return f_teacher;
	}
	public void setF_teacher(String f_teacher) {
		this.f_teacher = f_teacher;
	}
    
	public String getF_statusid() {
		return f_statusid;
	}
	public void setF_statusid(String f_statusid) {
		this.f_statusid = f_statusid;
	}
	public String getF_statusname() {
		return f_statusname;
	}
	public void setF_statusname(String f_statusname) {
		this.f_statusname = f_statusname;
	}
	
	public String getF_reason() {
		return f_reason;
	}
	public void setF_reason(String f_reason) {
		this.f_reason = f_reason;
	}
	public String getF_score() {
		return f_score;
	}
	public void setF_score(String f_score) {
		this.f_score = f_score;
	}
    
	public String getF_period() {
		return f_period;
	}
	public void setF_period(String f_period) {
		this.f_period = f_period;
	}
    
	public String getF_state() {
		return f_state;
	}
	public void setF_state(String f_state) {
		this.f_state = f_state;
	}
	
	public String getF_count() {
		return f_count;
	}
	public void setF_count(String f_count) {
		this.f_count = f_count;
	}
    
	public String getF_platformcode() {
		return f_platformcode;
	}
	public void setF_platformcode(String f_platformcode) {
		this.f_platformcode = f_platformcode;
	}
	public String getF_platform() {
		return f_platform;
	}
	public void setF_platform(String f_platform) {
		this.f_platform = f_platform;
	}
    
	public String getF_url() {
		return f_url;
	}
	public void setF_url(String f_url) {
		this.f_url = f_url;
	}
    
	public String getF_pdfurl() {
		return f_pdfurl;
	}
	public void setF_pdfurl(String f_pdfurl) {
		this.f_pdfurl = f_pdfurl;
	}
	public String getF_site() {
		return f_site;
	}
	public void setF_site(String f_site) {
		this.f_site = f_site;
	}
	
	public String getF_wayid() {
		return f_wayid;
	}
	public void setF_wayid(String f_wayid) {
		this.f_wayid = f_wayid;
	}
	public String getF_wayname() {
		return f_wayname;
	}
	public void setF_wayname(String f_wayname) {
		this.f_wayname = f_wayname;
	}

	public String getF_xingqi() {
		return f_xingqi;
	}
	public void setF_xingqi(String f_xingqi) {
		this.f_xingqi = f_xingqi;
	}

	public String getF_jieci() {
		return f_jieci;
	}
	public void setF_jieci(String f_jieci) {
		this.f_jieci = f_jieci;
	}

	public String getF_semester() {
		return f_semester;
	}
	public void setF_semester(String f_semester) {
		this.f_semester = f_semester;
	}

	public String getF_remark() {
		return f_remark;
	}
	public void setF_remark(String f_remark) {
		this.f_remark = f_remark;
	}

	public String getF_abstract() {
		return f_abstract;
	}
	public void setF_abstract(String f_abstract) {
		this.f_abstract = f_abstract;
	}
    
	public String getF_adddate() {
		return f_adddate;
	}
	public void setF_adddate(String f_adddate) {
		this.f_adddate = f_adddate;
	}
    
	public String getF_addemployeeid() {
		return f_addemployeeid;
	}
	public void setF_addemployeeid(String f_addemployeeid) {
		this.f_addemployeeid = f_addemployeeid;
	}
    
	public String getF_addemployeename() {
		return f_addemployeename;
	}
	public void setF_addemployeename(String f_addemployeename) {
		this.f_addemployeename = f_addemployeename;
	}
    
	public String getF_lastupdatedate() {
		return f_lastupdatedate;
	}
	public void setF_lastupdatedate(String f_lastupdatedate) {
		this.f_lastupdatedate = f_lastupdatedate;
	}
    
	public String getF_updateemployeeid() {
		return f_updateemployeeid;
	}
	public void setF_updateemployeeid(String f_updateemployeeid) {
		this.f_updateemployeeid = f_updateemployeeid;
	}
    
	public String getF_updateemployeename() {
		return f_updateemployeename;
	}
	public void setF_updateemployeename(String f_updateemployeename) {
		this.f_updateemployeename = f_updateemployeename;
	}
    
	public String getF_deleted() {
		return f_deleted;
	}
	public void setF_deleted(String f_deleted) {
		this.f_deleted = f_deleted;
	}
	public Integer getF_sort() {
		return f_sort;
	}
	public void setF_sort(Integer f_sort) {
		this.f_sort = f_sort;
	}
}
