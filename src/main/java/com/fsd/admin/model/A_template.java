package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="a_template")
public class A_template extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String name;
	private String code;
	private String paht;
	private String imageurl1;
	private String imageurl2;
	private String imageurl3;
	private String imageurl4;
	private String imageurl5;
	private String imageurl6;
	private String imageurl7;
	private String imageurl8;
	private String imageurl9;
	private String imageurl10;
	private String adddate;
	private String addemployeeid;
	private String addemployeename;
	private String updatedate;
	private String updateemployeeid;
	private String updateemployeename;
	private String deleted;
	private String remark;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
    
	public String getPaht() {
		return paht;
	}
	public void setPaht(String paht) {
		this.paht = paht;
	}
    
	public String getImageurl1() {
		return imageurl1;
	}
	public void setImageurl1(String imageurl1) {
		this.imageurl1 = imageurl1;
	}
    
	public String getImageurl2() {
		return imageurl2;
	}
	public void setImageurl2(String imageurl2) {
		this.imageurl2 = imageurl2;
	}
    
	public String getImageurl3() {
		return imageurl3;
	}
	public void setImageurl3(String imageurl3) {
		this.imageurl3 = imageurl3;
	}
    
	public String getImageurl4() {
		return imageurl4;
	}
	public void setImageurl4(String imageurl4) {
		this.imageurl4 = imageurl4;
	}
    
	public String getImageurl5() {
		return imageurl5;
	}
	public void setImageurl5(String imageurl5) {
		this.imageurl5 = imageurl5;
	}
    
	public String getImageurl6() {
		return imageurl6;
	}
	public void setImageurl6(String imageurl6) {
		this.imageurl6 = imageurl6;
	}
    
	public String getImageurl7() {
		return imageurl7;
	}
	public void setImageurl7(String imageurl7) {
		this.imageurl7 = imageurl7;
	}
    
	public String getImageurl8() {
		return imageurl8;
	}
	public void setImageurl8(String imageurl8) {
		this.imageurl8 = imageurl8;
	}
    
	public String getImageurl9() {
		return imageurl9;
	}
	public void setImageurl9(String imageurl9) {
		this.imageurl9 = imageurl9;
	}
    
	public String getImageurl10() {
		return imageurl10;
	}
	public void setImageurl10(String imageurl10) {
		this.imageurl10 = imageurl10;
	}
    
	public String getAdddate() {
		return adddate;
	}
	public void setAdddate(String adddate) {
		this.adddate = adddate;
	}
    
	public String getAddemployeeid() {
		return addemployeeid;
	}
	public void setAddemployeeid(String addemployeeid) {
		this.addemployeeid = addemployeeid;
	}
    
	public String getAddemployeename() {
		return addemployeename;
	}
	public void setAddemployeename(String addemployeename) {
		this.addemployeename = addemployeename;
	}
    
	public String getUpdatedate() {
		return updatedate;
	}
	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}
    
	public String getUpdateemployeeid() {
		return updateemployeeid;
	}
	public void setUpdateemployeeid(String updateemployeeid) {
		this.updateemployeeid = updateemployeeid;
	}
    
	public String getUpdateemployeename() {
		return updateemployeename;
	}
	public void setUpdateemployeename(String updateemployeename) {
		this.updateemployeename = updateemployeename;
	}
    
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}

	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
}
