package com.fsd.admin.model;

import com.fsd.core.model.BaseModel;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="j_reply")
public class J_reply extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String boardid;
	private String boardtitle;
	private String postsid;
	private String poststitle;
	private String isscreen;
	private String content;
	private String retime;
	private String reuserid;
	private String reusername;
	private String deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getBoardid() {
		return boardid;
	}

	public void setBoardid(String boardid) {
		this.boardid = boardid;
	}

	public String getBoardtitle() {
		return boardtitle;
	}

	public void setBoardtitle(String boardtitle) {
		this.boardtitle = boardtitle;
	}

	public String getPostsid() {
		return postsid;
	}

	public void setPostsid(String postsid) {
		this.postsid = postsid;
	}

	public String getPoststitle() {
		return poststitle;
	}

	public void setPoststitle(String poststitle) {
		this.poststitle = poststitle;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getIsscreen() {
		return isscreen;
	}

	public void setIsscreen(String isscreen) {
		this.isscreen = isscreen;
	}

	public String getRetime() {
		return retime;
	}

	public void setRetime(String retime) {
		this.retime = retime;
	}

	public String getReuserid() {
		return reuserid;
	}

	public void setReuserid(String reuserid) {
		this.reuserid = reuserid;
	}

	public String getReusername() {
		return reusername;
	}

	public void setReusername(String reusername) {
		this.reusername = reusername;
	}

	public String getDeleted() {
		return deleted;
	}

	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
}
