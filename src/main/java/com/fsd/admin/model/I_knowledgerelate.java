package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="i_knowledgerelate")
public class I_knowledgerelate extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String type;
	private String belongsid;
	private String relateid;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
    
	public String getBelongsid() {
		return belongsid;
	}
	public void setBelongsid(String belongsid) {
		this.belongsid = belongsid;
	}
    
	public String getRelateid() {
		return relateid;
	}
	public void setRelateid(String relateid) {
		this.relateid = relateid;
	}
    
}
