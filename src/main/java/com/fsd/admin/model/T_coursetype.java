package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import com.fsd.core.model.BaseModel;

/**
 * 课程分类
 * @author Administrator
 */
@Entity
@Table(name="t_coursetype")
public class T_coursetype extends BaseModel{
    private static final long serialVersionUID = 1L;
    
	private String id;
	private String f_parentid; //父级编号
	private Long f_leaf; //叶子节点
	private String f_title; //类型标题
	private String f_imageurl; //预览图路径
	private Long f_order; //排序编号
	private String f_state; //状态
	private String f_adduser; //添加人员
	private String f_adddate; //添加日期
	private String f_updateuser; //修改人员
	private String f_updatedate; //修改日期
	private Long f_deleted; //删除标志
	private String f_parentname;
	
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getF_parentid() {
		return f_parentid;
	}
	public void setF_parentid(String f_parentid) {
		this.f_parentid = f_parentid;
	}
    
	public Long getF_leaf() {
		return f_leaf;
	}
	public void setF_leaf(Long f_leaf) {
		this.f_leaf = f_leaf;
	}
    
	public String getF_title() {
		return f_title;
	}
	public void setF_title(String f_title) {
		this.f_title = f_title;
	}

	public String getF_imageurl() {
		return f_imageurl;
	}
	public void setF_imageurl(String f_imageurl) {
		this.f_imageurl = f_imageurl;
	}

	public Long getF_order() {
		return f_order;
	}
	public void setF_order(Long f_order) {
		this.f_order = f_order;
	}
    
	public String getF_state() {
		return f_state;
	}
	public void setF_state(String f_state) {
		this.f_state = f_state;
	}
    
	public String getF_adduser() {
		return f_adduser;
	}
	public void setF_adduser(String f_adduser) {
		this.f_adduser = f_adduser;
	}
    
	public String getF_adddate() {
		return f_adddate;
	}
	public void setF_adddate(String f_adddate) {
		this.f_adddate = f_adddate;
	}
    
	public String getF_updateuser() {
		return f_updateuser;
	}
	public void setF_updateuser(String f_updateuser) {
		this.f_updateuser = f_updateuser;
	}
    
	public String getF_updatedate() {
		return f_updatedate;
	}
	public void setF_updatedate(String f_updatedate) {
		this.f_updatedate = f_updatedate;
	}
    
	public Long getF_deleted() {
		return f_deleted;
	}
	public void setF_deleted(Long f_deleted) {
		this.f_deleted = f_deleted;
	}

	@Transient
	public String getF_parentname() {
		return f_parentname;
	}
	public void setF_parentname(String f_parentname) {
		this.f_parentname = f_parentname;
	}
}
