package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="i_answerquestion")
public class I_answerquestion extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String companyid;
	private String bankquestionid;
	private String testinfoid;
	private String testquestionid;
	private String answerinfoid;
	private String employeeid;
	private String answerdate;
	private double score;
	private String scorewaycode;
	private String scorewayname;
	private String isright;
	private String inspectemplid;
	private String inspectemplname;
	private String inspectdate;
	private String inspectannotation;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getCompanyid() {
		return companyid;
	}
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
    
	public String getBankquestionid() {
		return bankquestionid;
	}
	public void setBankquestionid(String bankquestionid) {
		this.bankquestionid = bankquestionid;
	}
    
	public String getTestinfoid() {
		return testinfoid;
	}
	public void setTestinfoid(String testinfoid) {
		this.testinfoid = testinfoid;
	}
    
	public String getTestquestionid() {
		return testquestionid;
	}
	public void setTestquestionid(String testquestionid) {
		this.testquestionid = testquestionid;
	}
    
	public String getAnswerinfoid() {
		return answerinfoid;
	}
	public void setAnswerinfoid(String answerinfoid) {
		this.answerinfoid = answerinfoid;
	}
    
	public String getEmployeeid() {
		return employeeid;
	}
	public void setEmployeeid(String employeeid) {
		this.employeeid = employeeid;
	}
    
	public String getAnswerdate() {
		return answerdate;
	}
	public void setAnswerdate(String answerdate) {
		this.answerdate = answerdate;
	}
    
	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}
    
	public String getScorewaycode() {
		return scorewaycode;
	}
	public void setScorewaycode(String scorewaycode) {
		this.scorewaycode = scorewaycode;
	}
    
	public String getScorewayname() {
		return scorewayname;
	}
	public void setScorewayname(String scorewayname) {
		this.scorewayname = scorewayname;
	}
    
	public String getIsright() {
		return isright;
	}
	public void setIsright(String isright) {
		this.isright = isright;
	}
    
	public String getInspectemplid() {
		return inspectemplid;
	}
	public void setInspectemplid(String inspectemplid) {
		this.inspectemplid = inspectemplid;
	}
    
	public String getInspectemplname() {
		return inspectemplname;
	}
	public void setInspectemplname(String inspectemplname) {
		this.inspectemplname = inspectemplname;
	}
    
	public String getInspectdate() {
		return inspectdate;
	}
	public void setInspectdate(String inspectdate) {
		this.inspectdate = inspectdate;
	}
    
	public String getInspectannotation() {
		return inspectannotation;
	}
	public void setInspectannotation(String inspectannotation) {
		this.inspectannotation = inspectannotation;
	}
    
}
