package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fsd.core.model.BaseModel;

@Entity
@Table(name = "b_templatelist")
public class B_templatelist extends BaseModel {
	private static final long serialVersionUID = 1L;
	private String id;
	private String companyid;
	private String templatetypeid;
	private String templatetypename;
	private String templatetypecode;
	private String code;
	private String name;
	private String title;
	private String belongsid;
	private String imagepath;
	private String urlpath;
	private String parameter1;
	private String parameter2;
	private String parameter3;
	private String remark;
	private Long sort;
	private String adddate;
	private String addemployeeid;
	private String addemployeename;
	private String updatedate;
	private String updateemployeeid;
	private String updateemployeename;
	private String deleted;

	@Id
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCompanyid() {
		return companyid;
	}

	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}

	public String getTemplatetypeid() {
		return templatetypeid;
	}

	public void setTemplatetypeid(String templatetypeid) {
		this.templatetypeid = templatetypeid;
	}

	public String getTemplatetypename() {
		return templatetypename;
	}

	public void setTemplatetypename(String templatetypename) {
		this.templatetypename = templatetypename;
	}

	public String getTemplatetypecode() {
		return templatetypecode;
	}

	public void setTemplatetypecode(String templatetypecode) {
		this.templatetypecode = templatetypecode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBelongsid() {
		return belongsid;
	}

	public void setBelongsid(String belongsid) {
		this.belongsid = belongsid;
	}

	public String getImagepath() {
		return imagepath;
	}

	public void setImagepath(String imagepath) {
		this.imagepath = imagepath;
	}

	public String getUrlpath() {
		return urlpath;
	}

	public void setUrlpath(String urlpath) {
		this.urlpath = urlpath;
	}

	public String getParameter1() {
		return parameter1;
	}

	public void setParameter1(String parameter1) {
		this.parameter1 = parameter1;
	}

	public String getParameter2() {
		return parameter2;
	}

	public void setParameter2(String parameter2) {
		this.parameter2 = parameter2;
	}

	public String getParameter3() {
		return parameter3;
	}

	public void setParameter3(String parameter3) {
		this.parameter3 = parameter3;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getSort() {
		return sort;
	}

	public void setSort(Long sort) {
		this.sort = sort;
	}

	public String getAdddate() {
		return adddate;
	}

	public void setAdddate(String adddate) {
		this.adddate = adddate;
	}

	public String getAddemployeeid() {
		return addemployeeid;
	}

	public void setAddemployeeid(String addemployeeid) {
		this.addemployeeid = addemployeeid;
	}

	public String getAddemployeename() {
		return addemployeename;
	}

	public void setAddemployeename(String addemployeename) {
		this.addemployeename = addemployeename;
	}

	public String getUpdatedate() {
		return updatedate;
	}

	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}

	public String getUpdateemployeeid() {
		return updateemployeeid;
	}

	public void setUpdateemployeeid(String updateemployeeid) {
		this.updateemployeeid = updateemployeeid;
	}

	public String getUpdateemployeename() {
		return updateemployeename;
	}

	public void setUpdateemployeename(String updateemployeename) {
		this.updateemployeename = updateemployeename;
	}

	public String getDeleted() {
		return deleted;
	}

	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
}
