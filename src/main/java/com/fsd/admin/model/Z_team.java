package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

/**
 * 团队
 * @author Administrator
 *
 */
@Entity
@Table(name="z_team")
public class Z_team extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String f_name; //团队名称
	private String f_abstract; //团队简介
	private String f_leaderno; //负责人学号
	private String f_leaderid; //负责人编号
	private String f_leadername; //负责人姓名
	private String f_leaderphone; //负责人电话
	private String f_member; //团队成员
	private String f_projects; //团队项目
	private String f_creatorid; //创建人编号
	private String f_creatorname; //创建人姓名
	private String f_adddate; //创建时间
	private String f_photo1; //团队照片1
	private String f_photo2; //团队照片2
	private String f_deltet; //删除标志
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getF_name() {
		return f_name;
	}
	public void setF_name(String f_name) {
		this.f_name = f_name;
	}
    
	public String getF_abstract() {
		return f_abstract;
	}
	public void setF_abstract(String f_abstract) {
		this.f_abstract = f_abstract;
	}
    
	public String getF_leaderno() {
		return f_leaderno;
	}
	public void setF_leaderno(String f_leaderno) {
		this.f_leaderno = f_leaderno;
	}
    
	public String getF_leaderid() {
		return f_leaderid;
	}
	public void setF_leaderid(String f_leaderid) {
		this.f_leaderid = f_leaderid;
	}
    
	public String getF_leadername() {
		return f_leadername;
	}
	public void setF_leadername(String f_leadername) {
		this.f_leadername = f_leadername;
	}

	public String getF_leaderphone() {
		return f_leaderphone;
	}
	public void setF_leaderphone(String f_leaderphone) {
		this.f_leaderphone = f_leaderphone;
	}

	public String getF_member() {
		return f_member;
	}
	public void setF_member(String f_member) {
		this.f_member = f_member;
	}

	public String getF_projects() {
		return f_projects;
	}
	public void setF_projects(String f_projects) {
		this.f_projects = f_projects;
	}

	public String getF_creatorid() {
		return f_creatorid;
	}
	public void setF_creatorid(String f_creatorid) {
		this.f_creatorid = f_creatorid;
	}
    
	public String getF_creatorname() {
		return f_creatorname;
	}
	public void setF_creatorname(String f_creatorname) {
		this.f_creatorname = f_creatorname;
	}
    
	public String getF_adddate() {
		return f_adddate;
	}
	public void setF_adddate(String f_adddate) {
		this.f_adddate = f_adddate;
	}
    
	public String getF_photo1() {
		return f_photo1;
	}
	public void setF_photo1(String f_photo1) {
		this.f_photo1 = f_photo1;
	}
    
	public String getF_photo2() {
		return f_photo2;
	}
	public void setF_photo2(String f_photo2) {
		this.f_photo2 = f_photo2;
	}
    
	public String getF_deltet() {
		return f_deltet;
	}
	public void setF_deltet(String f_deltet) {
		this.f_deltet = f_deltet;
	}
}
