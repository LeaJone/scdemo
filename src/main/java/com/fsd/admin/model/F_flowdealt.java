package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="f_flowdealt")
public class F_flowdealt extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String cmpanyid;
	private String flowobjectid;
	private String recordid;
	private String termid;
	private String propertyname;
	private String propertydepict;
	private String propertytype;
	private String propertytypename;
	private String operator;
	private String operatorname;
	private String valuewaycode;
	private String valuewayname;
	private String valueinfo;
	private String valuedepict;
	private String dealtresults;
	private String dealtresultsname;
	private String opiniondepict;
	private Long sort;
	private String dealtdate;
	private String objectjson;
    
	@Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getCmpanyid() {
		return cmpanyid;
	}
	public void setCmpanyid(String cmpanyid) {
		this.cmpanyid = cmpanyid;
	}
    
	public String getFlowobjectid() {
		return flowobjectid;
	}
	public void setFlowobjectid(String flowobjectid) {
		this.flowobjectid = flowobjectid;
	}
	
	public String getRecordid() {
		return recordid;
	}
	public void setRecordid(String recordid) {
		this.recordid = recordid;
	}
    
	public String getTermid() {
		return termid;
	}
	public void setTermid(String termid) {
		this.termid = termid;
	}
    
	public String getPropertyname() {
		return propertyname;
	}
	public void setPropertyname(String propertyname) {
		this.propertyname = propertyname;
	}
    
	public String getPropertydepict() {
		return propertydepict;
	}
	public void setPropertydepict(String propertydepict) {
		this.propertydepict = propertydepict;
	}
    
	public String getPropertytype() {
		return propertytype;
	}
	public void setPropertytype(String propertytype) {
		this.propertytype = propertytype;
	}
    
	public String getPropertytypename() {
		return propertytypename;
	}
	public void setPropertytypename(String propertytypename) {
		this.propertytypename = propertytypename;
	}
    
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
    
	public String getOperatorname() {
		return operatorname;
	}
	public void setOperatorname(String operatorname) {
		this.operatorname = operatorname;
	}
    
	public String getValuewaycode() {
		return valuewaycode;
	}
	public void setValuewaycode(String valuewaycode) {
		this.valuewaycode = valuewaycode;
	}
	
	public String getValuewayname() {
		return valuewayname;
	}
	public void setValuewayname(String valuewayname) {
		this.valuewayname = valuewayname;
	}
    
	public String getValueinfo() {
		return valueinfo;
	}
	public void setValueinfo(String valueinfo) {
		this.valueinfo = valueinfo;
	}
    
	public String getValuedepict() {
		return valuedepict;
	}
	public void setValuedepict(String valuedepict) {
		this.valuedepict = valuedepict;
	}
    
	public String getDealtresults() {
		return dealtresults;
	}
	public void setDealtresults(String dealtresults) {
		this.dealtresults = dealtresults;
	}
    
	public String getDealtresultsname() {
		return dealtresultsname;
	}
	public void setDealtresultsname(String dealtresultsname) {
		this.dealtresultsname = dealtresultsname;
	}
    
	public String getOpiniondepict() {
		return opiniondepict;
	}
	public void setOpiniondepict(String opiniondepict) {
		this.opiniondepict = opiniondepict;
	}
    
	public Long getSort() {
		return sort;
	}
	public void setSort(Long sort) {
		this.sort = sort;
	}
	
	public String getDealtdate() {
		return dealtdate;
	}
	public void setDealtdate(String dealtdate) {
		this.dealtdate = dealtdate;
	}
    
	public String getObjectjson() {
		return objectjson;
	}
	public void setObjectjson(String objectjson) {
		this.objectjson = objectjson;
	}
    
}
