package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="A_Company")
public class A_Company extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String parentid;
	private String parentname;
	private String code;
	private String name;
	private String symbol;
	private String defaulttypeid;
	private String defaulttypename;
	private String areatypeid;
	private String areatypename;
	private String professiontypeid;
	private String professiontypename;
	private String postcode;
	private String address;
	private String freightaddress;
	private String corporation;
	private String irdnumber;
	private String coverage;
	private String accountmethod;
	private String againstbillday;
	private String paidbillday;
	private String accountdeadline;
	private String doingstatus;
	private Long debtmoney;
	private String website;
	private String email;
	private String status;
	private String statusname;
	private String remark;
	private String imagelogo;
	private String image1;
	private String image2;
	private Long sort;
	private String adddate;
	private String addemployeeid;
	private String addemployeename;
	private String updatedate;
	private String updateemployeeid;
	private String updateemployeename;
	private String deleted;
	private String templateid;
	private String templatename;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getParentid() {
		return parentid;
	}
	public void setParentid(String parentid) {
		this.parentid = parentid;
	}
    
	public String getParentname() {
		return parentname;
	}
	public void setParentname(String parentname) {
		this.parentname = parentname;
	}
    
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
    
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
    
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
    
	public String getDefaulttypeid() {
		return defaulttypeid;
	}
	public void setDefaulttypeid(String defaulttypeid) {
		this.defaulttypeid = defaulttypeid;
	}
    
	public String getDefaulttypename() {
		return defaulttypename;
	}
	public void setDefaulttypename(String defaulttypename) {
		this.defaulttypename = defaulttypename;
	}
    
	public String getAreatypeid() {
		return areatypeid;
	}
	public void setAreatypeid(String areatypeid) {
		this.areatypeid = areatypeid;
	}
    
	public String getAreatypename() {
		return areatypename;
	}
	public void setAreatypename(String areatypename) {
		this.areatypename = areatypename;
	}
    
	public String getProfessiontypeid() {
		return professiontypeid;
	}
	public void setProfessiontypeid(String professiontypeid) {
		this.professiontypeid = professiontypeid;
	}
    
	public String getProfessiontypename() {
		return professiontypename;
	}
	public void setProfessiontypename(String professiontypename) {
		this.professiontypename = professiontypename;
	}
    
	public String getPostcode() {
		return postcode;
	}
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
    
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
    
	public String getFreightaddress() {
		return freightaddress;
	}
	public void setFreightaddress(String freightaddress) {
		this.freightaddress = freightaddress;
	}
    
	public String getCorporation() {
		return corporation;
	}
	public void setCorporation(String corporation) {
		this.corporation = corporation;
	}
    
	public String getIrdnumber() {
		return irdnumber;
	}
	public void setIrdnumber(String irdnumber) {
		this.irdnumber = irdnumber;
	}
    
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
    
	public String getAccountmethod() {
		return accountmethod;
	}
	public void setAccountmethod(String accountmethod) {
		this.accountmethod = accountmethod;
	}
    
	public String getAgainstbillday() {
		return againstbillday;
	}
	public void setAgainstbillday(String againstbillday) {
		this.againstbillday = againstbillday;
	}
    
	public String getPaidbillday() {
		return paidbillday;
	}
	public void setPaidbillday(String paidbillday) {
		this.paidbillday = paidbillday;
	}
    
	public String getAccountdeadline() {
		return accountdeadline;
	}
	public void setAccountdeadline(String accountdeadline) {
		this.accountdeadline = accountdeadline;
	}
    
	public String getDoingstatus() {
		return doingstatus;
	}
	public void setDoingstatus(String doingstatus) {
		this.doingstatus = doingstatus;
	}
    
	public Long getDebtmoney() {
		return debtmoney;
	}
	public void setDebtmoney(Long debtmoney) {
		this.debtmoney = debtmoney;
	}
    
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
    
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
    
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
    
	public String getStatusname() {
		return statusname;
	}
	public void setStatusname(String statusname) {
		this.statusname = statusname;
	}
    
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
    
	public String getImagelogo() {
		return imagelogo;
	}
	public void setImagelogo(String imagelogo) {
		this.imagelogo = imagelogo;
	}
    
	public String getImage1() {
		return image1;
	}
	public void setImage1(String image1) {
		this.image1 = image1;
	}
    
	public String getImage2() {
		return image2;
	}
	public void setImage2(String image2) {
		this.image2 = image2;
	}
    
	public Long getSort() {
		return sort;
	}
	public void setSort(Long sort) {
		this.sort = sort;
	}
    
	public String getAdddate() {
		return adddate;
	}
	public void setAdddate(String adddate) {
		this.adddate = adddate;
	}
    
	public String getAddemployeeid() {
		return addemployeeid;
	}
	public void setAddemployeeid(String addemployeeid) {
		this.addemployeeid = addemployeeid;
	}
    
	public String getAddemployeename() {
		return addemployeename;
	}
	public void setAddemployeename(String addemployeename) {
		this.addemployeename = addemployeename;
	}
    
	public String getUpdatedate() {
		return updatedate;
	}
	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}
    
	public String getUpdateemployeeid() {
		return updateemployeeid;
	}
	public void setUpdateemployeeid(String updateemployeeid) {
		this.updateemployeeid = updateemployeeid;
	}
    
	public String getUpdateemployeename() {
		return updateemployeename;
	}
	public void setUpdateemployeename(String updateemployeename) {
		this.updateemployeename = updateemployeename;
	}
    
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}

	public String getTemplateid() {
		return templateid;
	}
	public void setTemplateid(String templateid) {
		this.templateid = templateid;
	}

	public String getTemplatename() {
		return templatename;
	}
	public void setTemplatename(String templatename) {
		this.templatename = templatename;
	}
}
