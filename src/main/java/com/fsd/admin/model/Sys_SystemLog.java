package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="Sys_SystemLog")
public class Sys_SystemLog extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String companyid;
	private String logtime;
	private String employeeid;
	private String employeename;
	private String logtype;
	private String logtypename;
	private String logdepict;
	private String remark;
    
	@Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getEmployeename() {
		return employeename;
	}
	public void setEmployeename(String employeename) {
		this.employeename = employeename;
	}
	public String getLogtypename() {
		return logtypename;
	}
	public void setLogtypename(String logtypename) {
		this.logtypename = logtypename;
	}
	public String getCompanyid() {
		return companyid;
	}
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
	public String getLogtime() {
		return logtime;
	}
	public void setLogtime(String logtime) {
		this.logtime = logtime;
	}
	public String getEmployeeid() {
		return employeeid;
	}
	public void setEmployeeid(String employeeid) {
		this.employeeid = employeeid;
	}
	public String getLogtype() {
		return logtype;
	}
	public void setLogtype(String logtype) {
		this.logtype = logtype;
	}
	public String getLogdepict() {
		return logdepict;
	}
	public void setLogdepict(String logdepict) {
		this.logdepict = logdepict;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
}
