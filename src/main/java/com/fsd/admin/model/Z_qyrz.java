package com.fsd.admin.model;

import com.fsd.core.model.BaseModel;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="z_qyrz")
public class Z_qyrz extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String qymc;
	private String qyxz_code;
	private String qyxz_name;
	private String qyyx;
	private String frdb;
	private String frsfzh;
	private String frlxdh;
	private String jyfw;
	private String zycp;
	private String zcsj;
	private String zcdd;
	private String zczb;
	private String yyzz;
	private String subtime;
	private String status;
	private String deleted;
	private String rowid;

    
    @Id
	public String getId() { return id; }
	public void setId(String id) {
		this.id = id;
	}

	public String getQymc() {
		return qymc;
	}

	public void setQymc(String qymc) {
		this.qymc = qymc;
	}

	public String getQyxz_code() {
		return qyxz_code;
	}

	public void setQyxz_code(String qyxz_code) {
		this.qyxz_code = qyxz_code;
	}

	public String getQyxz_name() { return qyxz_name; }

	public void setQyxz_name(String qyxz_name) {
		this.qyxz_name = qyxz_name;
	}

	public String getQyyx() { return qyyx; }

	public void setQyyx(String qyyx) {
		this.qyyx = qyyx;
	}

	public String getFrdb() {
		return frdb;
	}

	public void setFrdb(String frdb) {
		this.frdb = frdb;
	}

	public String getFrsfzh() {
		return frsfzh;
	}

	public void setFrsfzh(String frsfzh) {
		this.frsfzh = frsfzh;
	}

	public String getFrlxdh() {
		return frlxdh;
	}

	public void setFrlxdh(String frlxdh) {
		this.frlxdh = frlxdh;
	}

	public String getJyfw() {
		return jyfw;
	}

	public void setJyfw(String jyfw) {
		this.jyfw = jyfw;
	}

	public String getZycp() {
		return zycp;
	}

	public void setZycp(String zycp) {
		this.zycp = zycp;
	}

	public String getZcsj() {
		return zcsj;
	}

	public void setZcsj(String zcsj) {
		this.zcsj = zcsj;
	}

	public String getZcdd() {
		return zcdd;
	}

	public void setZcdd(String zcdd) {
		this.zcdd = zcdd;
	}

	public String getZczb() {
		return zczb;
	}

	public void setZczb(String zczb) {
		this.zczb = zczb;
	}

	public String getYyzz() {
		return yyzz;
	}

	public void setYyzz(String yyzz) {
		this.yyzz = yyzz;
	}

	public String getSubtime() {
		return subtime;
	}

	public void setSubtime(String subtime) {
		this.subtime = subtime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDeleted() {
		return deleted;
	}

	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}

    public String getRowid() {
        return rowid;
    }

    public void setRowid(String rowid) {
        this.rowid = rowid;
    }
}
