package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="f_flownode")
public class F_flownode extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String companyid;
	private String projectid;
	private String projectname;
	private String nodetype;
	private String nodetypename;
	private String code;
	private String name;
	private String previousid;
	private String previousname;
	private String nextid;
	private String nextname;
	private String nexttype;
	private String nexttypename;
	private String falseid;
	private String falsename;
	private String isdeliver;
	private String isextract;
	private String isrefusal;
	private String isreturn;
	private String x;
	private String y;
	private String width;
	private String height;
	private String adddate;
	private String addemployeeid;
	private String addemployeename;
	private String updatedate;
	private String updateemployeeid;
	private String updateemployeename;
	private String deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getCompanyid() {
		return companyid;
	}
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
    
	public String getProjectid() {
		return projectid;
	}
	public void setProjectid(String projectid) {
		this.projectid = projectid;
	}
    
	public String getProjectname() {
		return projectname;
	}
	public void setProjectname(String projectname) {
		this.projectname = projectname;
	}
    
	public String getNodetype() {
		return nodetype;
	}
	public void setNodetype(String nodetype) {
		this.nodetype = nodetype;
	}
    
	public String getNodetypename() {
		return nodetypename;
	}
	public void setNodetypename(String nodetypename) {
		this.nodetypename = nodetypename;
	}
    
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
    
	public String getPreviousid() {
		return previousid;
	}
	public void setPreviousid(String previousid) {
		this.previousid = previousid;
	}
    
	public String getPreviousname() {
		return previousname;
	}
	public void setPreviousname(String previousname) {
		this.previousname = previousname;
	}
    
	public String getNextid() {
		return nextid;
	}
	public void setNextid(String nextid) {
		this.nextid = nextid;
	}
    
	public String getNextname() {
		return nextname;
	}
	public void setNextname(String nextname) {
		this.nextname = nextname;
	}
    
	public String getNexttype() {
		return nexttype;
	}
	public void setNexttype(String nexttype) {
		this.nexttype = nexttype;
	}
	
	public String getNexttypename() {
		return nexttypename;
	}
	public void setNexttypename(String nexttypename) {
		this.nexttypename = nexttypename;
	}
	
	public String getFalseid() {
		return falseid;
	}
	public void setFalseid(String falseid) {
		this.falseid = falseid;
	}
    
	public String getFalsename() {
		return falsename;
	}
	public void setFalsename(String falsename) {
		this.falsename = falsename;
	}
	
	public String getIsdeliver() {
		return isdeliver;
	}
	public void setIsdeliver(String isdeliver) {
		this.isdeliver = isdeliver;
	}
	
	public String getIsextract() {
		return isextract;
	}
	public void setIsextract(String isextract) {
		this.isextract = isextract;
	}
	
	public String getIsrefusal() {
		return isrefusal;
	}
	public void setIsrefusal(String isrefusal) {
		this.isrefusal = isrefusal;
	}
	
	public String getIsreturn() {
		return isreturn;
	}
	public void setIsreturn(String isreturn) {
		this.isreturn = isreturn;
	}
	
	public String getX() {
		return x;
	}
	public void setX(String x) {
		this.x = x;
	}
	
	public String getY() {
		return y;
	}
	public void setY(String y) {
		this.y = y;
	}
	
	public String getWidth() {
		return width;
	}
	public void setWidth(String width) {
		this.width = width;
	}
	
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
    
	public String getAdddate() {
		return adddate;
	}
	public void setAdddate(String adddate) {
		this.adddate = adddate;
	}
    
	public String getAddemployeeid() {
		return addemployeeid;
	}
	public void setAddemployeeid(String addemployeeid) {
		this.addemployeeid = addemployeeid;
	}
    
	public String getAddemployeename() {
		return addemployeename;
	}
	public void setAddemployeename(String addemployeename) {
		this.addemployeename = addemployeename;
	}
    
	public String getUpdatedate() {
		return updatedate;
	}
	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}
    
	public String getUpdateemployeeid() {
		return updateemployeeid;
	}
	public void setUpdateemployeeid(String updateemployeeid) {
		this.updateemployeeid = updateemployeeid;
	}
    
	public String getUpdateemployeename() {
		return updateemployeename;
	}
	public void setUpdateemployeename(String updateemployeename) {
		this.updateemployeename = updateemployeename;
	}
    
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
    
}
