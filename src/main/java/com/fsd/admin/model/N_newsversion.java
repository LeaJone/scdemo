package com.fsd.admin.model;

import com.fsd.core.model.BaseModel;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="n_newsversion")
public class N_newsversion extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String version;
	private String iosurl;
	private String androidurl;
	private String pubdate;
	private String remark;

	@Id
	public String getId() { return id; }

	public void setId(String id) { this.id = id; }

	public String getVersion() { return version; }

	public void setVersion(String version) { this.version = version; }

	public String getIosurl() { return iosurl; }

	public void setIosurl(String iosurl) { this.iosurl = iosurl; }

	public String getAndroidurl() { return androidurl; }

	public void setAndroidurl(String androidurl) { this.androidurl = androidurl; }

	public String getPubdate() { return pubdate; }

	public void setPubdate(String pubdate) { this.pubdate = pubdate; }

	public String getRemark() { return remark; }

	public void setRemark(String remark) { this.remark = remark; }
    
}
