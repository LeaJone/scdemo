package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="z_service")
public class Z_service extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String f_typeid;
	private String f_typename;
	private String f_contacts;
	private String f_phone;
	private String f_describe;
	private String f_stateid;
	private String f_statename;
	private String f_isreply;
	private String f_isreplycontent;
	private String f_isreplytime;
	private String f_adddate;
	private String f_addemployeeid;
	private String f_addemployeename;
	private String f_lastupdatedate;
	private String f_updateemployeeid;
	private String f_updateemployeename;
	private String f_deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getF_typeid() {
		return f_typeid;
	}
	public void setF_typeid(String f_typeid) {
		this.f_typeid = f_typeid;
	}
    
	public String getF_typename() {
		return f_typename;
	}
	public void setF_typename(String f_typename) {
		this.f_typename = f_typename;
	}
    
	public String getF_contacts() {
		return f_contacts;
	}
	public void setF_contacts(String f_contacts) {
		this.f_contacts = f_contacts;
	}
    
	public String getF_phone() {
		return f_phone;
	}
	public void setF_phone(String f_phone) {
		this.f_phone = f_phone;
	}
    
	public String getF_describe() {
		return f_describe;
	}
	public void setF_describe(String f_describe) {
		this.f_describe = f_describe;
	}
    
	public String getF_stateid() {
		return f_stateid;
	}
	public void setF_stateid(String f_stateid) {
		this.f_stateid = f_stateid;
	}
    
	public String getF_statename() {
		return f_statename;
	}
	public void setF_statename(String f_statename) {
		this.f_statename = f_statename;
	}
    
	public String getF_isreply() {
		return f_isreply;
	}
	public void setF_isreply(String f_isreply) {
		this.f_isreply = f_isreply;
	}
    
	public String getF_isreplycontent() {
		return f_isreplycontent;
	}
	public void setF_isreplycontent(String f_isreplycontent) {
		this.f_isreplycontent = f_isreplycontent;
	}
    
	public String getF_isreplytime() {
		return f_isreplytime;
	}
	public void setF_isreplytime(String f_isreplytime) {
		this.f_isreplytime = f_isreplytime;
	}
    
	public String getF_adddate() {
		return f_adddate;
	}
	public void setF_adddate(String f_adddate) {
		this.f_adddate = f_adddate;
	}
    
	public String getF_addemployeeid() {
		return f_addemployeeid;
	}
	public void setF_addemployeeid(String f_addemployeeid) {
		this.f_addemployeeid = f_addemployeeid;
	}
    
	public String getF_addemployeename() {
		return f_addemployeename;
	}
	public void setF_addemployeename(String f_addemployeename) {
		this.f_addemployeename = f_addemployeename;
	}
    
	public String getF_lastupdatedate() {
		return f_lastupdatedate;
	}
	public void setF_lastupdatedate(String f_lastupdatedate) {
		this.f_lastupdatedate = f_lastupdatedate;
	}
    
	public String getF_updateemployeeid() {
		return f_updateemployeeid;
	}
	public void setF_updateemployeeid(String f_updateemployeeid) {
		this.f_updateemployeeid = f_updateemployeeid;
	}
    
	public String getF_updateemployeename() {
		return f_updateemployeename;
	}
	public void setF_updateemployeename(String f_updateemployeename) {
		this.f_updateemployeename = f_updateemployeename;
	}
    
	public String getF_deleted() {
		return f_deleted;
	}
	public void setF_deleted(String f_deleted) {
		this.f_deleted = f_deleted;
	}
    
}
