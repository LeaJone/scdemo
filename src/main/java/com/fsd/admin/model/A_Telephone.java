package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="A_Telephone")
public class A_Telephone extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String belongsid;
	private String belongsname;
	private String depict;
	private String telephone;
	private String remark;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getBelongsid() {
		return belongsid;
	}
	public void setBelongsid(String belongsid) {
		this.belongsid = belongsid;
	}
    
	public String getBelongsname() {
		return belongsname;
	}
	public void setBelongsname(String belongsname) {
		this.belongsname = belongsname;
	}
    
	public String getDepict() {
		return depict;
	}
	public void setDepict(String depict) {
		this.depict = depict;
	}
    
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
    
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
    
}
