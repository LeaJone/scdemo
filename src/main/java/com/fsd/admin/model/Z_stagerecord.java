package com.fsd.admin.model;

import com.fsd.core.model.BaseModel;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="z_stagerecord")
public class Z_stagerecord extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String f_code;
	private String f_name;
	private String f_subtime;
	private String f_user;
	private String f_content;
	private String f_file;
	private String f_delete;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getF_code() {
		return f_code;
	}

	public void setF_code(String f_code) {
		this.f_code = f_code;
	}

	public String getF_name() {
		return f_name;
	}

	public void setF_name(String f_name) {
		this.f_name = f_name;
	}

	public String getF_subtime() {
		return f_subtime;
	}

	public void setF_subtime(String f_subtime) {
		this.f_subtime = f_subtime;
	}

	public String getF_user() {
		return f_user;
	}

	public void setF_user(String f_user) {
		this.f_user = f_user;
	}

	public String getF_content() {
		return f_content;
	}

	public void setF_content(String f_content) {
		this.f_content = f_content;
	}

	public String getF_file() {
		return f_file;
	}

	public void setF_file(String f_file) {
		this.f_file = f_file;
	}

	public String getF_delete() {
		return f_delete;
	}

	public void setF_delete(String f_delete) {
		this.f_delete = f_delete;
	}
	
}
