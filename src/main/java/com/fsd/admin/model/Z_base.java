package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="z_base")
public class Z_base extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String f_name;
	private String f_fid;
	private String f_fname;
	private String f_collegeid;
	private String f_collegename;
	private String f_address;
	private String f_contact;
	private String f_phone;
	private String f_floorspace;
	private String f_website;
	private String f_abstract;
	private String f_projects;
	private String f_achievement;
	private String f_imgurl;
	private String f_bigimgurl;
	private String f_isleaf;
	private String f_adddate;
	private String f_addemployeeid;
	private String f_addemployeename;
	private String f_lastupdatedate;
	private String f_updateemployeeid;
	private String f_updateemployeename;
	private String f_deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getF_name() {
		return f_name;
	}
	public void setF_name(String f_name) {
		this.f_name = f_name;
	}

	public String getF_fid() {
		return f_fid;
	}
	public void setF_fid(String f_fid) {
		this.f_fid = f_fid;
	}

	public String getF_fname() {
		return f_fname;
	}
	public void setF_fname(String f_fname) {
		this.f_fname = f_fname;
	}

	public String getF_collegeid() {
		return f_collegeid;
	}
	public void setF_collegeid(String f_collegeid) {
		this.f_collegeid = f_collegeid;
	}

	public String getF_collegename() {
		return f_collegename;
	}
	public void setF_collegename(String f_collegename) {
		this.f_collegename = f_collegename;
	}

	public String getF_address() {
		return f_address;
	}
	public void setF_address(String f_address) {
		this.f_address = f_address;
	}
    
	public String getF_contact() {
		return f_contact;
	}
	public void setF_contact(String f_contact) {
		this.f_contact = f_contact;
	}
    
	public String getF_phone() {
		return f_phone;
	}
	public void setF_phone(String f_phone) {
		this.f_phone = f_phone;
	}
    
	public String getF_floorspace() {
		return f_floorspace;
	}
	public void setF_floorspace(String f_floorspace) {
		this.f_floorspace = f_floorspace;
	}
    
	public String getF_website() {
		return f_website;
	}
	public void setF_website(String f_website) {
		this.f_website = f_website;
	}
    
	public String getF_abstract() {
		return f_abstract;
	}
	public void setF_abstract(String f_abstract) {
		this.f_abstract = f_abstract;
	}

	public String getF_projects() {
		return f_projects;
	}
	public void setF_projects(String f_projects) {
		this.f_projects = f_projects;
	}

	public String getF_achievement() {
		return f_achievement;
	}
	public void setF_achievement(String f_achievement) {
		this.f_achievement = f_achievement;
	}

	public String getF_imgurl() {
		return f_imgurl;
	}
	public void setF_imgurl(String f_imgurl) {
		this.f_imgurl = f_imgurl;
	}

	public String getF_bigimgurl() {
		return f_bigimgurl;
	}
	public void setF_bigimgurl(String f_bigimgurl) {
		this.f_bigimgurl = f_bigimgurl;
	}

	public String getF_isleaf() {
		return f_isleaf;
	}
	public void setF_isleaf(String f_isleaf) {
		this.f_isleaf = f_isleaf;
	}

	public String getF_adddate() {
		return f_adddate;
	}
	public void setF_adddate(String f_adddate) {
		this.f_adddate = f_adddate;
	}
    
	public String getF_addemployeeid() {
		return f_addemployeeid;
	}
	public void setF_addemployeeid(String f_addemployeeid) {
		this.f_addemployeeid = f_addemployeeid;
	}
    
	public String getF_addemployeename() {
		return f_addemployeename;
	}
	public void setF_addemployeename(String f_addemployeename) {
		this.f_addemployeename = f_addemployeename;
	}
    
	public String getF_lastupdatedate() {
		return f_lastupdatedate;
	}
	public void setF_lastupdatedate(String f_lastupdatedate) {
		this.f_lastupdatedate = f_lastupdatedate;
	}
    
	public String getF_updateemployeeid() {
		return f_updateemployeeid;
	}
	public void setF_updateemployeeid(String f_updateemployeeid) {
		this.f_updateemployeeid = f_updateemployeeid;
	}
    
	public String getF_updateemployeename() {
		return f_updateemployeename;
	}
	public void setF_updateemployeename(String f_updateemployeename) {
		this.f_updateemployeename = f_updateemployeename;
	}
    
	public String getF_deleted() {
		return f_deleted;
	}
	public void setF_deleted(String f_deleted) {
		this.f_deleted = f_deleted;
	}
    
}
