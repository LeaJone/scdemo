package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="z_ysqgk")
public class Z_ysqgk extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String companyid;
	private String flowstatus;
	private String flowstatusname;
	private String xm;
	private String gzdw;
	private String zjmc;
	private String zjhm;
	private String txdz;
	private String yzbm;
	private String lxdh;
	private String dzyx1;
	private String mc;
	private String zzjgdm;
	private String yyzzxx;
	private String frdb;
	private String lxrxm;
	private String lxrdh;
	private String cz;
	private String lxdz;
	private String dzyx2;
	private String sqrqm;
	private String sqsj;
	private String sxxxnrms;
	private String sxxxyt;
	private String sxxxsqh;
	private String qtts;
	private String sfsqjmfy;
	private String sxxxzdgyfs;
	private String hqxxfs;
	private String qtfs;
	private String qureycode;
	private String auditing;
	private String selected;
	private String isreply;
	private String atitle;
	private String acontent;
	private String abranchid;
	private String abranchname;
	private String adddate;
	private String addemployeeid;
	private String addemployeename;
	private String updatedate;
	private String updateemployeeid;
	private String updateemployeename;
	private String deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getCompanyid() {
		return companyid;
	}
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
    
	public String getFlowstatus() {
		return flowstatus;
	}
	public void setFlowstatus(String flowstatus) {
		this.flowstatus = flowstatus;
	}
	
	public String getFlowstatusname() {
		return flowstatusname;
	}
	public void setFlowstatusname(String flowstatusname) {
		this.flowstatusname = flowstatusname;
	}
    
	public String getXm() {
		return xm;
	}
	public void setXm(String xm) {
		this.xm = xm;
	}
    
	public String getGzdw() {
		return gzdw;
	}
	public void setGzdw(String gzdw) {
		this.gzdw = gzdw;
	}
    
	public String getZjmc() {
		return zjmc;
	}
	public void setZjmc(String zjmc) {
		this.zjmc = zjmc;
	}
    
	public String getZjhm() {
		return zjhm;
	}
	public void setZjhm(String zjhm) {
		this.zjhm = zjhm;
	}
    
	public String getTxdz() {
		return txdz;
	}
	public void setTxdz(String txdz) {
		this.txdz = txdz;
	}
    
	public String getYzbm() {
		return yzbm;
	}
	public void setYzbm(String yzbm) {
		this.yzbm = yzbm;
	}
    
	public String getLxdh() {
		return lxdh;
	}
	public void setLxdh(String lxdh) {
		this.lxdh = lxdh;
	}
    
	public String getDzyx1() {
		return dzyx1;
	}
	public void setDzyx1(String dzyx1) {
		this.dzyx1 = dzyx1;
	}
    
	public String getMc() {
		return mc;
	}
	public void setMc(String mc) {
		this.mc = mc;
	}
    
	public String getZzjgdm() {
		return zzjgdm;
	}
	public void setZzjgdm(String zzjgdm) {
		this.zzjgdm = zzjgdm;
	}
    
	public String getYyzzxx() {
		return yyzzxx;
	}
	public void setYyzzxx(String yyzzxx) {
		this.yyzzxx = yyzzxx;
	}
    
	public String getFrdb() {
		return frdb;
	}
	public void setFrdb(String frdb) {
		this.frdb = frdb;
	}
    
	public String getLxrxm() {
		return lxrxm;
	}
	public void setLxrxm(String lxrxm) {
		this.lxrxm = lxrxm;
	}
    
	public String getLxrdh() {
		return lxrdh;
	}
	public void setLxrdh(String lxrdh) {
		this.lxrdh = lxrdh;
	}
    
	public String getCz() {
		return cz;
	}
	public void setCz(String cz) {
		this.cz = cz;
	}
    
	public String getLxdz() {
		return lxdz;
	}
	public void setLxdz(String lxdz) {
		this.lxdz = lxdz;
	}
    
	public String getDzyx2() {
		return dzyx2;
	}
	public void setDzyx2(String dzyx2) {
		this.dzyx2 = dzyx2;
	}
    
	public String getSqrqm() {
		return sqrqm;
	}
	public void setSqrqm(String sqrqm) {
		this.sqrqm = sqrqm;
	}
    
	public String getSqsj() {
		return sqsj;
	}
	public void setSqsj(String sqsj) {
		this.sqsj = sqsj;
	}
    
	public String getSxxxnrms() {
		return sxxxnrms;
	}
	public void setSxxxnrms(String sxxxnrms) {
		this.sxxxnrms = sxxxnrms;
	}
    
	public String getSxxxyt() {
		return sxxxyt;
	}
	public void setSxxxyt(String sxxxyt) {
		this.sxxxyt = sxxxyt;
	}
    
	public String getSxxxsqh() {
		return sxxxsqh;
	}
	public void setSxxxsqh(String sxxxsqh) {
		this.sxxxsqh = sxxxsqh;
	}
    
	public String getQtts() {
		return qtts;
	}
	public void setQtts(String qtts) {
		this.qtts = qtts;
	}
    
	public String getSfsqjmfy() {
		return sfsqjmfy;
	}
	public void setSfsqjmfy(String sfsqjmfy) {
		this.sfsqjmfy = sfsqjmfy;
	}
    
	public String getSxxxzdgyfs() {
		return sxxxzdgyfs;
	}
	public void setSxxxzdgyfs(String sxxxzdgyfs) {
		this.sxxxzdgyfs = sxxxzdgyfs;
	}
    
	public String getHqxxfs() {
		return hqxxfs;
	}
	public void setHqxxfs(String hqxxfs) {
		this.hqxxfs = hqxxfs;
	}
    
	public String getQtfs() {
		return qtfs;
	}
	public void setQtfs(String qtfs) {
		this.qtfs = qtfs;
	}
    
	public String getQureycode() {
		return qureycode;
	}
	public void setQureycode(String qureycode) {
		this.qureycode = qureycode;
	}
    
	public String getAuditing() {
		return auditing;
	}
	public void setAuditing(String auditing) {
		this.auditing = auditing;
	}
    
	public String getSelected() {
		return selected;
	}
	public void setSelected(String selected) {
		this.selected = selected;
	}
    
	public String getIsreply() {
		return isreply;
	}
	public void setIsreply(String isreply) {
		this.isreply = isreply;
	}
    
	public String getAtitle() {
		return atitle;
	}
	public void setAtitle(String atitle) {
		this.atitle = atitle;
	}
    
	public String getAcontent() {
		return acontent;
	}
	public void setAcontent(String acontent) {
		this.acontent = acontent;
	}
    
	public String getAbranchid() {
		return abranchid;
	}
	public void setAbranchid(String abranchid) {
		this.abranchid = abranchid;
	}
    
	public String getAbranchname() {
		return abranchname;
	}
	public void setAbranchname(String abranchname) {
		this.abranchname = abranchname;
	}
    
	public String getAdddate() {
		return adddate;
	}
	public void setAdddate(String adddate) {
		this.adddate = adddate;
	}
    
	public String getAddemployeeid() {
		return addemployeeid;
	}
	public void setAddemployeeid(String addemployeeid) {
		this.addemployeeid = addemployeeid;
	}
    
	public String getAddemployeename() {
		return addemployeename;
	}
	public void setAddemployeename(String addemployeename) {
		this.addemployeename = addemployeename;
	}
    
	public String getUpdatedate() {
		return updatedate;
	}
	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}
    
	public String getUpdateemployeeid() {
		return updateemployeeid;
	}
	public void setUpdateemployeeid(String updateemployeeid) {
		this.updateemployeeid = updateemployeeid;
	}
    
	public String getUpdateemployeename() {
		return updateemployeename;
	}
	public void setUpdateemployeename(String updateemployeename) {
		this.updateemployeename = updateemployeename;
	}
    
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
    
}
