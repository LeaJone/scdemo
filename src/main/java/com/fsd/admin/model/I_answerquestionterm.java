package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="i_answerquestionterm")
public class I_answerquestionterm extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String companyid;
	private String bankquestionid;
	private String bankquestermid;
	private String testinfoid;
	private String testquestionid;
	private String testquestermid;
	private String answerinfoid;
	private String answerquestionid;
	private String employeeid;
	private double score;
	private String answerresult;
	private String answerdescribe;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getCompanyid() {
		return companyid;
	}
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
    
	public String getBankquestionid() {
		return bankquestionid;
	}
	public void setBankquestionid(String bankquestionid) {
		this.bankquestionid = bankquestionid;
	}
    
	public String getBankquestermid() {
		return bankquestermid;
	}
	public void setBankquestermid(String bankquestermid) {
		this.bankquestermid = bankquestermid;
	}
    
	public String getTestinfoid() {
		return testinfoid;
	}
	public void setTestinfoid(String testinfoid) {
		this.testinfoid = testinfoid;
	}
    
	public String getTestquestionid() {
		return testquestionid;
	}
	public void setTestquestionid(String testquestionid) {
		this.testquestionid = testquestionid;
	}
    
	public String getTestquestermid() {
		return testquestermid;
	}
	public void setTestquestermid(String testquestermid) {
		this.testquestermid = testquestermid;
	}
    
	public String getAnswerinfoid() {
		return answerinfoid;
	}
	public void setAnswerinfoid(String answerinfoid) {
		this.answerinfoid = answerinfoid;
	}
    
	public String getAnswerquestionid() {
		return answerquestionid;
	}
	public void setAnswerquestionid(String answerquestionid) {
		this.answerquestionid = answerquestionid;
	}
    
	public String getEmployeeid() {
		return employeeid;
	}
	public void setEmployeeid(String employeeid) {
		this.employeeid = employeeid;
	}
    
	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}
    
	public String getAnswerresult() {
		return answerresult;
	}
	public void setAnswerresult(String answerresult) {
		this.answerresult = answerresult;
	}
    
	public String getAnswerdescribe() {
		return answerdescribe;
	}
	public void setAnswerdescribe(String answerdescribe) {
		this.answerdescribe = answerdescribe;
	}
    
}
