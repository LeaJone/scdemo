package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="f_flowobject")
public class F_flowobject extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String companyid;
	private String typeid;
	private String projectid;
	private String projectcode;
	private String projectname;
	private String nodeid;
	private String nodename;
	private String recordid;
	private String dataobjectid;
	private String dataobjectname;
	private String datatypeid;
	private String datatypename;
	private String code;
	private String branchid;
	private String branchname;
	private String employeeid;
	private String employeename;
	private String datadepict;
	private String applydate;
	private String flowstatus;
	private String flowstatusname;
	private String windowname;
	private String windowpath;
	private String objectjson;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getCompanyid() {
		return companyid;
	}
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
    
	public String getTypeid() {
		return typeid;
	}
	public void setTypeid(String typeid) {
		this.typeid = typeid;
	}
	
	public String getProjectid() {
		return projectid;
	}
	public void setProjectid(String projectid) {
		this.projectid = projectid;
	}
    
	public String getProjectcode() {
		return projectcode;
	}
	public void setProjectcode(String projectcode) {
		this.projectcode = projectcode;
	}
    
	public String getProjectname() {
		return projectname;
	}
	public void setProjectname(String projectname) {
		this.projectname = projectname;
	}
    
	public String getNodeid() {
		return nodeid;
	}
	public void setNodeid(String nodeid) {
		this.nodeid = nodeid;
	}
    
	public String getNodename() {
		return nodename;
	}
	public void setNodename(String nodename) {
		this.nodename = nodename;
	}
    
	public String getRecordid() {
		return recordid;
	}
	public void setRecordid(String recordid) {
		this.recordid = recordid;
	}
    
	public String getDataobjectid() {
		return dataobjectid;
	}
	public void setDataobjectid(String dataobjectid) {
		this.dataobjectid = dataobjectid;
	}
    
	public String getDataobjectname() {
		return dataobjectname;
	}
	public void setDataobjectname(String dataobjectname) {
		this.dataobjectname = dataobjectname;
	}
    
	public String getDatatypeid() {
		return datatypeid;
	}
	public void setDatatypeid(String datatypeid) {
		this.datatypeid = datatypeid;
	}
	
	public String getDatatypename() {
		return datatypename;
	}
	public void setDatatypename(String datatypename) {
		this.datatypename = datatypename;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
    
	public String getBranchid() {
		return branchid;
	}
	public void setBranchid(String branchid) {
		this.branchid = branchid;
	}
    
	public String getBranchname() {
		return branchname;
	}
	public void setBranchname(String branchname) {
		this.branchname = branchname;
	}
    
	public String getEmployeeid() {
		return employeeid;
	}
	public void setEmployeeid(String employeeid) {
		this.employeeid = employeeid;
	}
    
	public String getEmployeename() {
		return employeename;
	}
	public void setEmployeename(String employeename) {
		this.employeename = employeename;
	}
    
	public String getDatadepict() {
		return datadepict;
	}
	public void setDatadepict(String datadepict) {
		this.datadepict = datadepict;
	}
    
	public String getApplydate() {
		return applydate;
	}
	public void setApplydate(String applydate) {
		this.applydate = applydate;
	}
    
	public String getFlowstatus() {
		return flowstatus;
	}
	public void setFlowstatus(String flowstatus) {
		this.flowstatus = flowstatus;
	}
    
	public String getFlowstatusname() {
		return flowstatusname;
	}
	public void setFlowstatusname(String flowstatusname) {
		this.flowstatusname = flowstatusname;
	}
    
	public String getWindowname() {
		return windowname;
	}
	public void setWindowname(String windowname) {
		this.windowname = windowname;
	}
	
	public String getWindowpath() {
		return windowpath;
	}
	public void setWindowpath(String windowpath) {
		this.windowpath = windowpath;
	}
	
	public String getObjectjson() {
		return objectjson;
	}
	public void setObjectjson(String objectjson) {
		this.objectjson = objectjson;
	}
    
}
