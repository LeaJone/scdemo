package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fsd.core.model.BaseModel;

@Entity
@Table(name="z_project")
public class Z_project extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String f_code;
	private String f_name;
	private String f_typeid;
	private String f_typename;
	private String f_typeid1;
	private String f_typename1;
	private String f_typeid2;
	private String f_typename2;
	private String f_typeid3;
	private String f_typename3;
	private String f_sourceid;
	private String f_sourcename;
	private String f_teamid;
	private String f_teamname;
	private String f_teacherid;
	private String f_teachername;
	private String f_teacherduty;
	private String f_teacherunit;
	private String f_teacherphone;
	private String f_teacheremail;
	private String f_teacherid2;
	private String f_teachername2;
	private String f_teacherduty2;
	private String f_teacherunit2;
	private String f_teacherphone2;
	private String f_teacheremail2;
	private String f_outteacherid;
	private String f_outteachername;
	private String f_outteacherduty;
	private String f_outteacherunit;
	private String f_outteacherphone;
	private String f_outteacheremail;
	private String f_outteacherid2;
	private String f_outteachername2;
	private String f_outteacherduty2;
	private String f_outteacherunit2;
	private String f_outteacherphone2;
	private String f_outteacheremail2;
	private String f_studentnumber;
	private String f_leaderid;
	private String f_leadername;
	private String f_leaderresponsibility;
	private String f_leaderphone;
	private String f_leaderclass;
	private String f_leadercollege;
	private String f_email;
	private String f_starttime;
	private String f_stoptime;
	private String f_statusid;
	private String f_statusname;
	private String f_adddate;
	private String f_resultid;
	private String f_resultname;
	private String f_rateid;
	private String f_ratename;
	private String f_abstract;
	private String f_gist;
	private String f_scheme;
	private String f_achievement;
	private String f_trait;
	private String f_feature;
	private String f_schedule;
	private String f_pdf;
	private String f_classifyid;
	private String f_classifyname;
	private String f_delete;
	/**
	 * 提交次数
	 */
	private String f_subnum;
	/**
	 * 是否提交中期检查
	 */
	private String f_issubzqjc;
	private String f_money_zjf;
	private String f_money_czbk;
	private String f_money_xxbk;
	private String f_money_jtf;
	private String f_money_jtfbz;
	private String f_money_tszlf;
	private String f_money_tszlfbz;
	private String f_money_ysf;
	private String f_money_ysfbz;
	private String f_money_bgyp;
	private String f_money_bgypbz;
	private String f_money_hyf;
	private String f_money_hyfbz;
	private String f_money_syclf;
	private String f_money_syclfbz;
	private String f_money_qt;
	private String f_money_qtbz;
	private String f_money_fj;
	private String f_addemployeeid;
	private String f_addemployeename;
	private String f_isschool;
	private String f_isprovince;
	private String f_iscountry;
	private String f_schoolmoney;
	private String f_provincemoney;
	private String f_countrymoney;
	private String f_netscore;
	private String f_luyanscore;
	private String f_branchid;
	private String f_ismiddle;
	private String f_ismiddletypeid;
	private String f_ismiddletypename;
	private String f_isending;
	private String f_endingmbid;
	private String f_endingmbname;
	private String f_endingcjpdid;
	private String f_endingcjpdname;
	private String f_schooltypeid;
	private String f_schooltypename;

	@Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getF_code() {
		return f_code;
	}
	public void setF_code(String f_code) {
		this.f_code = f_code;
	}
    
	public String getF_name() {
		return f_name;
	}
	public void setF_name(String f_name) {
		this.f_name = f_name;
	}
    
	public String getF_typeid() {
		return f_typeid;
	}
	public void setF_typeid(String f_typeid) {
		this.f_typeid = f_typeid;
	}
    
	public String getF_typename() {
		return f_typename;
	}
	public void setF_typename(String f_typename) {
		this.f_typename = f_typename;
	}
    
	public String getF_typeid1() {
		return f_typeid1;
	}
	public void setF_typeid1(String f_typeid1) {
		this.f_typeid1 = f_typeid1;
	}
    
	public String getF_typename1() {
		return f_typename1;
	}
	public void setF_typename1(String f_typename1) {
		this.f_typename1 = f_typename1;
	}
    
	public String getF_typeid2() {
		return f_typeid2;
	}
	public void setF_typeid2(String f_typeid2) {
		this.f_typeid2 = f_typeid2;
	}
    
	public String getF_typename2() {
		return f_typename2;
	}
	public void setF_typename2(String f_typename2) {
		this.f_typename2 = f_typename2;
	}
    
	public String getF_typeid3() {
		return f_typeid3;
	}
	public void setF_typeid3(String f_typeid3) {
		this.f_typeid3 = f_typeid3;
	}
    
	public String getF_typename3() {
		return f_typename3;
	}
	public void setF_typename3(String f_typename3) {
		this.f_typename3 = f_typename3;
	}
    
	public String getF_sourceid() {
		return f_sourceid;
	}
	public void setF_sourceid(String f_sourceid) {
		this.f_sourceid = f_sourceid;
	}
    
	public String getF_sourcename() {
		return f_sourcename;
	}
	public void setF_sourcename(String f_sourcename) {
		this.f_sourcename = f_sourcename;
	}
    
	public String getF_teamid() {
		return f_teamid;
	}
	public void setF_teamid(String f_teamid) {
		this.f_teamid = f_teamid;
	}

	public String getF_teamname() {
		return f_teamname;
	}
	public void setF_teamname(String f_teamname) {
		this.f_teamname = f_teamname;
	}
    
	public String getF_teacherid() {
		return f_teacherid;
	}
	public void setF_teacherid(String f_teacherid) {
		this.f_teacherid = f_teacherid;
	}
    
	public String getF_teachername() {
		return f_teachername;
	}
	public void setF_teachername(String f_teachername) {
		this.f_teachername = f_teachername;
	}

	public String getF_teacherduty() {
		return f_teacherduty;
	}
	public void setF_teacherduty(String f_teacherduty) {
		this.f_teacherduty = f_teacherduty;
	}

	public String getF_teacherunit() {
		return f_teacherunit;
	}
	public void setF_teacherunit(String f_teacherunit) {
		this.f_teacherunit = f_teacherunit;
	}

	public String getF_teacherphone() {
		return f_teacherphone;
	}
	public void setF_teacherphone(String f_teacherphone) {
		this.f_teacherphone = f_teacherphone;
	}

	public String getF_teacheremail() {
		return f_teacheremail;
	}
	public void setF_teacheremail(String f_teacheremail) {
		this.f_teacheremail = f_teacheremail;
	}

	public String getF_teacherid2() {
		return f_teacherid2;
	}
	public void setF_teacherid2(String f_teacherid2) {
		this.f_teacherid2 = f_teacherid2;
	}

	public String getF_teachername2() {
		return f_teachername2;
	}
	public void setF_teachername2(String f_teachername2) {
		this.f_teachername2 = f_teachername2;
	}

	public String getF_teacherduty2() {
		return f_teacherduty2;
	}

	public void setF_teacherduty2(String f_teacherduty2) {
		this.f_teacherduty2 = f_teacherduty2;
	}

	public String getF_teacherunit2() {
		return f_teacherunit2;
	}

	public void setF_teacherunit2(String f_teacherunit2) {
		this.f_teacherunit2 = f_teacherunit2;
	}

	public String getF_teacherphone2() {
		return f_teacherphone2;
	}

	public void setF_teacherphone2(String f_teacherphone2) {
		this.f_teacherphone2 = f_teacherphone2;
	}

	public String getF_teacheremail2() {
		return f_teacheremail2;
	}

	public void setF_teacheremail2(String f_teacheremail2) {
		this.f_teacheremail2 = f_teacheremail2;
	}

	public String getF_outteacherid() {
		return f_outteacherid;
	}
	public void setF_outteacherid(String f_outteacherid) {
		this.f_outteacherid = f_outteacherid;
	}

	public String getF_outteachername() {
		return f_outteachername;
	}
	public void setF_outteachername(String f_outteachername) {
		this.f_outteachername = f_outteachername;
	}

	public String getF_outteacherduty() {
		return f_outteacherduty;
	}

	public void setF_outteacherduty(String f_outteacherduty) {
		this.f_outteacherduty = f_outteacherduty;
	}

	public String getF_outteacherunit() {
		return f_outteacherunit;
	}

	public void setF_outteacherunit(String f_outteacherunit) {
		this.f_outteacherunit = f_outteacherunit;
	}

	public String getF_outteacherphone() {
		return f_outteacherphone;
	}

	public void setF_outteacherphone(String f_outteacherphone) {
		this.f_outteacherphone = f_outteacherphone;
	}

	public String getF_outteacheremail() {
		return f_outteacheremail;
	}

	public void setF_outteacheremail(String f_outteacheremail) {
		this.f_outteacheremail = f_outteacheremail;
	}

	public String getF_outteacherid2() {
		return f_outteacherid2;
	}

	public void setF_outteacherid2(String f_outteacherid2) {
		this.f_outteacherid2 = f_outteacherid2;
	}

	public String getF_outteachername2() {
		return f_outteachername2;
	}

	public void setF_outteachername2(String f_outteachername2) {
		this.f_outteachername2 = f_outteachername2;
	}

	public String getF_outteacherduty2() {
		return f_outteacherduty2;
	}

	public void setF_outteacherduty2(String f_outteacherduty2) {
		this.f_outteacherduty2 = f_outteacherduty2;
	}

	public String getF_outteacherunit2() {
		return f_outteacherunit2;
	}

	public void setF_outteacherunit2(String f_outteacherunit2) {
		this.f_outteacherunit2 = f_outteacherunit2;
	}

	public String getF_outteacherphone2() {
		return f_outteacherphone2;
	}

	public void setF_outteacherphone2(String f_outteacherphone2) {
		this.f_outteacherphone2 = f_outteacherphone2;
	}

	public String getF_outteacheremail2() {
		return f_outteacheremail2;
	}

	public void setF_outteacheremail2(String f_outteacheremail2) {
		this.f_outteacheremail2 = f_outteacheremail2;
	}

	public String getF_studentnumber() {
		return f_studentnumber;
	}
	public void setF_studentnumber(String f_studentnumber) {
		this.f_studentnumber = f_studentnumber;
	}
    
	public String getF_leaderid() {
		return f_leaderid;
	}
	public void setF_leaderid(String f_leaderid) {
		this.f_leaderid = f_leaderid;
	}
    
	public String getF_leadername() {
		return f_leadername;
	}
	public void setF_leadername(String f_leadername) {
		this.f_leadername = f_leadername;
	}

	public String getF_leaderresponsibility() {
		return f_leaderresponsibility;
	}
	public void setF_leaderresponsibility(String f_leaderresponsibility) {
		this.f_leaderresponsibility = f_leaderresponsibility;
	}

	public String getF_leaderphone() {
		return f_leaderphone;
	}
	public void setF_leaderphone(String f_leaderphone) {
		this.f_leaderphone = f_leaderphone;
	}

	public String getF_leaderclass() {
		return f_leaderclass;
	}
	public void setF_leaderclass(String f_leaderclass) {
		this.f_leaderclass = f_leaderclass;
	}

	public String getF_leadercollege() {
		return f_leadercollege;
	}
	public void setF_leadercollege(String f_leadercollege) {
		this.f_leadercollege = f_leadercollege;
	}

	public String getF_email() {
		return f_email;
	}
	public void setF_email(String f_email) {
		this.f_email = f_email;
	}
    
	public String getF_starttime() {
		return f_starttime;
	}
	public void setF_starttime(String f_starttime) {
		this.f_starttime = f_starttime;
	}
    
	public String getF_stoptime() {
		return f_stoptime;
	}
	public void setF_stoptime(String f_stoptime) {
		this.f_stoptime = f_stoptime;
	}
    
	public String getF_statusid() {
		return f_statusid;
	}
	public void setF_statusid(String f_statusid) {
		this.f_statusid = f_statusid;
	}
    
	public String getF_statusname() {
		return f_statusname;
	}
	public void setF_statusname(String f_statusname) {
		this.f_statusname = f_statusname;
	}
    
	public String getF_adddate() {
		return f_adddate;
	}
	public void setF_adddate(String f_adddate) {
		this.f_adddate = f_adddate;
	}
    
	public String getF_resultid() {
		return f_resultid;
	}
	public void setF_resultid(String f_resultid) {
		this.f_resultid = f_resultid;
	}
    
	public String getF_resultname() {
		return f_resultname;
	}
	public void setF_resultname(String f_resultname) {
		this.f_resultname = f_resultname;
	}

	public String getF_rateid() {
		return f_rateid;
	}
	public void setF_rateid(String f_rateid) {
		this.f_rateid = f_rateid;
	}

	public String getF_ratename() {
		return f_ratename;
	}
	public void setF_ratename(String f_ratename) {
		this.f_ratename = f_ratename;
	}

	public String getF_abstract() {
		return f_abstract;
	}
	public void setF_abstract(String f_abstract) {
		this.f_abstract = f_abstract;
	}

	public String getF_gist() {
		return f_gist;
	}
	public void setF_gist(String f_gist) {
		this.f_gist = f_gist;
	}
    
	public String getF_scheme() {
		return f_scheme;
	}
	public void setF_scheme(String f_scheme) {
		this.f_scheme = f_scheme;
	}
    
	public String getF_achievement() {
		return f_achievement;
	}
	public void setF_achievement(String f_achievement) {
		this.f_achievement = f_achievement;
	}
    
	public String getF_trait() {
		return f_trait;
	}
	public void setF_trait(String f_trait) {
		this.f_trait = f_trait;
	}

	public String getF_feature() {
		return f_feature;
	}
	public void setF_feature(String f_feature) {
		this.f_feature = f_feature;
	}

	public String getF_schedule() {
		return f_schedule;
	}
	public void setF_schedule(String f_schedule) {
		this.f_schedule = f_schedule;
	}

	public String getF_pdf() {
		return f_pdf;
	}
	public void setF_pdf(String f_pdf) {
		this.f_pdf = f_pdf;
	}
    
	public String getF_delete() {
		return f_delete;
	}
	public void setF_delete(String f_delete) {
		this.f_delete = f_delete;
	}

	public String getF_money_zjf() {
		return f_money_zjf;
	}
	public void setF_money_zjf(String f_money_zjf) {
		this.f_money_zjf = f_money_zjf;
	}

	public String getF_money_czbk() {
		return f_money_czbk;
	}
	public void setF_money_czbk(String f_money_czbk) {
		this.f_money_czbk = f_money_czbk;
	}

	public String getF_money_xxbk() {
		return f_money_xxbk;
	}
	public void setF_money_xxbk(String f_money_xxbk) {
		this.f_money_xxbk = f_money_xxbk;
	}

	public String getF_money_jtf() {
		return f_money_jtf;
	}
	public void setF_money_jtf(String f_money_jtf) {
		this.f_money_jtf = f_money_jtf;
	}

	public String getF_money_jtfbz() {
		return f_money_jtfbz;
	}
	public void setF_money_jtfbz(String f_money_jtfbz) {
		this.f_money_jtfbz = f_money_jtfbz;
	}

	public String getF_money_tszlf() {
		return f_money_tszlf;
	}
	public void setF_money_tszlf(String f_money_tszlf) {
		this.f_money_tszlf = f_money_tszlf;
	}

	public String getF_money_tszlfbz() {
		return f_money_tszlfbz;
	}
	public void setF_money_tszlfbz(String f_money_tszlfbz) {
		this.f_money_tszlfbz = f_money_tszlfbz;
	}

	public String getF_money_ysf() {
		return f_money_ysf;
	}
	public void setF_money_ysf(String f_money_ysf) {
		this.f_money_ysf = f_money_ysf;
	}

	public String getF_money_ysfbz() {
		return f_money_ysfbz;
	}
	public void setF_money_ysfbz(String f_money_ysfbz) {
		this.f_money_ysfbz = f_money_ysfbz;
	}

	public String getF_money_bgyp() {
		return f_money_bgyp;
	}
	public void setF_money_bgyp(String f_money_bgyp) {
		this.f_money_bgyp = f_money_bgyp;
	}

	public String getF_money_bgypbz() {
		return f_money_bgypbz;
	}
	public void setF_money_bgypbz(String f_money_bgypbz) {
		this.f_money_bgypbz = f_money_bgypbz;
	}

	public String getF_money_hyf() {
		return f_money_hyf;
	}
	public void setF_money_hyf(String f_money_hyf) {
		this.f_money_hyf = f_money_hyf;
	}

	public String getF_money_hyfbz() {
		return f_money_hyfbz;
	}
	public void setF_money_hyfbz(String f_money_hyfbz) {
		this.f_money_hyfbz = f_money_hyfbz;
	}

	public String getF_money_syclf() {
		return f_money_syclf;
	}
	public void setF_money_syclf(String f_money_syclf) {
		this.f_money_syclf = f_money_syclf;
	}

	public String getF_money_syclfbz() {
		return f_money_syclfbz;
	}
	public void setF_money_syclfbz(String f_money_syclfbz) {
		this.f_money_syclfbz = f_money_syclfbz;
	}

	public String getF_money_qt() {
		return f_money_qt;
	}
	public void setF_money_qt(String f_money_qt) {
		this.f_money_qt = f_money_qt;
	}

	public String getF_money_qtbz() {
		return f_money_qtbz;
	}
	public void setF_money_qtbz(String f_money_qtbz) {
		this.f_money_qtbz = f_money_qtbz;
	}

	public String getF_money_fj() {
		return f_money_fj;
	}

	public void setF_money_fj(String f_money_fj) {
		this.f_money_fj = f_money_fj;
	}

	@Transient
	public String getF_subnum() {
		return f_subnum;
	}

	public void setF_subnum(String f_subnum) {
		this.f_subnum = f_subnum;
	}

	@Transient
	public String getF_issubzqjc() {
		return f_issubzqjc;
	}
	public void setF_issubzqjc(String f_issubzqjc) {
		this.f_issubzqjc = f_issubzqjc;
	}

	public String getF_addemployeeid() {
		return f_addemployeeid;
	}
	public void setF_addemployeeid(String f_addemployeeid) {
		this.f_addemployeeid = f_addemployeeid;
	}

	public String getF_addemployeename() {
		return f_addemployeename;
	}
	public void setF_addemployeename(String f_addemployeename) {
		this.f_addemployeename = f_addemployeename;
	}

	public String getF_classifyid() {
		return f_classifyid;
	}
	public void setF_classifyid(String f_classifyid) {
		this.f_classifyid = f_classifyid;
	}

	public String getF_classifyname() {
		return f_classifyname;
	}
	public void setF_classifyname(String f_classifyname) {
		this.f_classifyname = f_classifyname;
	}

	public String getF_isschool() {
		return f_isschool;
	}
	public void setF_isschool(String f_isschool) {
		this.f_isschool = f_isschool;
	}

	public String getF_isprovince() {
		return f_isprovince;
	}
	public void setF_isprovince(String f_isprovince) {
		this.f_isprovince = f_isprovince;
	}

	public String getF_iscountry() {
		return f_iscountry;
	}
	public void setF_iscountry(String f_iscountry) {
		this.f_iscountry = f_iscountry;
	}

	public String getF_schoolmoney() {
		return f_schoolmoney;
	}
	public void setF_schoolmoney(String f_schoolmoney) {
		this.f_schoolmoney = f_schoolmoney;
	}

	public String getF_provincemoney() {
		return f_provincemoney;
	}
	public void setF_provincemoney(String f_provincemoney) {
		this.f_provincemoney = f_provincemoney;
	}

	public String getF_countrymoney() {
		return f_countrymoney;
	}
	public void setF_countrymoney(String f_countrymoney) {
		this.f_countrymoney = f_countrymoney;
	}

	public String getF_netscore() {
		return f_netscore;
	}
	public void setF_netscore(String f_netscore) {
		this.f_netscore = f_netscore;
	}

	public String getF_luyanscore() {
		return f_luyanscore;
	}
	public void setF_luyanscore(String f_luyanscore) {
		this.f_luyanscore = f_luyanscore;
	}

	public String getF_branchid() {
		return f_branchid;
	}
	public void setF_branchid(String f_branchid) {
		this.f_branchid = f_branchid;
	}

	public String getF_ismiddle() {
		return f_ismiddle;
	}
	public void setF_ismiddle(String f_ismiddle) {
		this.f_ismiddle = f_ismiddle;
	}

	public String getF_ismiddletypeid() {
		return f_ismiddletypeid;
	}
	public void setF_ismiddletypeid(String f_ismiddletypeid) {
		this.f_ismiddletypeid = f_ismiddletypeid;
	}

	public String getF_ismiddletypename() {
		return f_ismiddletypename;
	}
	public void setF_ismiddletypename(String f_ismiddletypename) {
		this.f_ismiddletypename = f_ismiddletypename;
	}

	public String getF_endingmbid() {
		return f_endingmbid;
	}
	public void setF_endingmbid(String f_endingmbid) {
		this.f_endingmbid = f_endingmbid;
	}

	public String getF_endingmbname() {
		return f_endingmbname;
	}
	public void setF_endingmbname(String f_endingmbname) {
		this.f_endingmbname = f_endingmbname;
	}

	public String getF_endingcjpdid() {
		return f_endingcjpdid;
	}
	public void setF_endingcjpdid(String f_endingcjpdid) {
		this.f_endingcjpdid = f_endingcjpdid;
	}

	public String getF_endingcjpdname() {
		return f_endingcjpdname;
	}
	public void setF_endingcjpdname(String f_endingcjpdname) {
		this.f_endingcjpdname = f_endingcjpdname;
	}

	public String getF_isending() {
		return f_isending;
	}
	public void setF_isending(String f_isending) {
		this.f_isending = f_isending;
	}

	public String getF_schooltypeid() {
		return f_schooltypeid;
	}
	public void setF_schooltypeid(String f_schooltypeid) {
		this.f_schooltypeid = f_schooltypeid;
	}

	public String getF_schooltypename() {
		return f_schooltypename;
	}
	public void setF_schooltypename(String f_schooltypename) {
		this.f_schooltypename = f_schooltypename;
	}
}
