package com.fsd.admin.model;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fsd.core.model.BaseModel;

/**
 * 团队成员关系
 * @author Administrator
 *
 */
@Entity
@Table(name="z_teamanduser")
public class Z_teamanduser extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String f_teamid; //团队编号
	private String f_userid; //成员编号
	private String username; //成员姓名
	private String usernumber; //成员学号
	private String useracademy; //成员院系
	private String userbranch; //成员专业
	private String usersex; //成员性别
	private String f_delete; //删除标志
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getF_teamid() {
		return f_teamid;
	}
	public void setF_teamid(String f_teamid) {
		this.f_teamid = f_teamid;
	}
    
	public String getF_userid() {
		return f_userid;
	}
	public void setF_userid(String f_userid) {
		this.f_userid = f_userid;
	}
    
	public String getF_delete() {
		return f_delete;
	}
	public void setF_delete(String f_delete) {
		this.f_delete = f_delete;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUseracademy() {
		return useracademy;
	}
	public void setUseracademy(String useracademy) {
		this.useracademy = useracademy;
	}
	public String getUserbranch() {
		return userbranch;
	}
	public void setUserbranch(String userbranch) {
		this.userbranch = userbranch;
	}
	public String getUsersex() {
		return usersex;
	}
	public void setUsersex(String usersex) {
		this.usersex = usersex;
	}

	public String getUsernumber() {
		return usernumber;
	}
	public void setUsernumber(String usernumber) {
		this.usernumber = usernumber;
	}
}
