package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="i_answerinfo")
public class I_answerinfo extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String companyid;
	private String testinfoid;
	private String testinfoname;
	private String employeeid;
	private String employeename;
	private String answerdate;
	private Long answertime;
	private String submitdate;
	private double totalscore;
	private String statuscode;
	private String statusname;
	private String inspectemplid;
	private String inspectemplname;
	private String inspectdate;
	private String inspectannotation;
	private String deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getCompanyid() {
		return companyid;
	}
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
    
	public String getTestinfoid() {
		return testinfoid;
	}
	public void setTestinfoid(String testinfoid) {
		this.testinfoid = testinfoid;
	}
    
	public String getTestinfoname() {
		return testinfoname;
	}
	public void setTestinfoname(String testinfoname) {
		this.testinfoname = testinfoname;
	}
    
	public String getEmployeeid() {
		return employeeid;
	}
	public void setEmployeeid(String employeeid) {
		this.employeeid = employeeid;
	}
    
	public String getEmployeename() {
		return employeename;
	}
	public void setEmployeename(String employeename) {
		this.employeename = employeename;
	}
    
	public String getAnswerdate() {
		return answerdate;
	}
	public void setAnswerdate(String answerdate) {
		this.answerdate = answerdate;
	}
    
	public Long getAnswertime() {
		return answertime;
	}
	public void setAnswertime(Long answertime) {
		this.answertime = answertime;
	}
    
	public String getSubmitdate() {
		return submitdate;
	}
	public void setSubmitdate(String submitdate) {
		this.submitdate = submitdate;
	}
    
	public double getTotalscore() {
		return totalscore;
	}
	public void setTotalscore(double totalscore) {
		this.totalscore = totalscore;
	}
    
	public String getStatuscode() {
		return statuscode;
	}
	public void setStatuscode(String statuscode) {
		this.statuscode = statuscode;
	}
    
	public String getStatusname() {
		return statusname;
	}
	public void setStatusname(String statusname) {
		this.statusname = statusname;
	}
    
	public String getInspectemplid() {
		return inspectemplid;
	}
	public void setInspectemplid(String inspectemplid) {
		this.inspectemplid = inspectemplid;
	}
    
	public String getInspectemplname() {
		return inspectemplname;
	}
	public void setInspectemplname(String inspectemplname) {
		this.inspectemplname = inspectemplname;
	}
    
	public String getInspectdate() {
		return inspectdate;
	}
	public void setInspectdate(String inspectdate) {
		this.inspectdate = inspectdate;
	}
    
	public String getInspectannotation() {
		return inspectannotation;
	}
	public void setInspectannotation(String inspectannotation) {
		this.inspectannotation = inspectannotation;
	}
    
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
    
}
