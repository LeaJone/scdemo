package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="i_testinfo")
public class I_testinfo extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String companyid;
	private String kindcode;
	private String kindname;
	private String bankinfoid;
	private String bankinfoname;
	private String name;
	private String symbol;
	private String description;
	private String stratdate;
	private String enddate;
	private Long answertime;
	private double totalscore;
	private Long testnumber;
	private String remark;
	private String status;
	private String statusname;
	private String adddate;
	private String addemployeeid;
	private String addemployeename;
	private String updatedate;
	private String updateemployeeid;
	private String updateemployeename;
	private String deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getCompanyid() {
		return companyid;
	}
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
    
	public String getKindcode() {
		return kindcode;
	}
	public void setKindcode(String kindcode) {
		this.kindcode = kindcode;
	}
    
	public String getKindname() {
		return kindname;
	}
	public void setKindname(String kindname) {
		this.kindname = kindname;
	}
    
	public String getBankinfoid() {
		return bankinfoid;
	}
	public void setBankinfoid(String bankinfoid) {
		this.bankinfoid = bankinfoid;
	}
    
	public String getBankinfoname() {
		return bankinfoname;
	}
	public void setBankinfoname(String bankinfoname) {
		this.bankinfoname = bankinfoname;
	}
    
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
    
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
    
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
    
	public String getStratdate() {
		return stratdate;
	}
	public void setStratdate(String stratdate) {
		this.stratdate = stratdate;
	}
    
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
    
	public Long getAnswertime() {
		return answertime;
	}
	public void setAnswertime(Long answertime) {
		this.answertime = answertime;
	}
    
	public double getTotalscore() {
		return totalscore;
	}
	public void setTotalscore(double totalscore) {
		this.totalscore = totalscore;
	}
    
	public Long getTestnumber() {
		return testnumber;
	}
	public void setTestnumber(Long testnumber) {
		this.testnumber = testnumber;
	}
    
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
    
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
    
	public String getStatusname() {
		return statusname;
	}
	public void setStatusname(String statusname) {
		this.statusname = statusname;
	}
    
	public String getAdddate() {
		return adddate;
	}
	public void setAdddate(String adddate) {
		this.adddate = adddate;
	}
    
	public String getAddemployeeid() {
		return addemployeeid;
	}
	public void setAddemployeeid(String addemployeeid) {
		this.addemployeeid = addemployeeid;
	}
    
	public String getAddemployeename() {
		return addemployeename;
	}
	public void setAddemployeename(String addemployeename) {
		this.addemployeename = addemployeename;
	}
    
	public String getUpdatedate() {
		return updatedate;
	}
	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}
    
	public String getUpdateemployeeid() {
		return updateemployeeid;
	}
	public void setUpdateemployeeid(String updateemployeeid) {
		this.updateemployeeid = updateemployeeid;
	}
    
	public String getUpdateemployeename() {
		return updateemployeename;
	}
	public void setUpdateemployeename(String updateemployeename) {
		this.updateemployeename = updateemployeename;
	}
    
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
    
}
