package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="z_eduteacher")
public class Z_eduteacher extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String f_candidateid;
	private String f_candidatename;
	private String f_course;
	private String f_abstract;
	private String f_tips;
	private String f_imgurl;
	private String f_baseid;
	private String f_basename;
	private String f_pdfurl;
	private String f_adddate;
	private String f_addemployeeid;
	private String f_addemployeename;
	private String f_lastupdatedate;
	private String f_updateemployeeid;
	private String f_updateemployeename;
	private String f_deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getF_candidateid() {
		return f_candidateid;
	}
	public void setF_candidateid(String f_candidateid) {
		this.f_candidateid = f_candidateid;
	}
    
	public String getF_candidatename() {
		return f_candidatename;
	}
	public void setF_candidatename(String f_candidatename) {
		this.f_candidatename = f_candidatename;
	}
    
	public String getF_course() {
		return f_course;
	}
	public void setF_course(String f_course) {
		this.f_course = f_course;
	}
    
	public String getF_abstract() {
		return f_abstract;
	}
	public void setF_abstract(String f_abstract) {
		this.f_abstract = f_abstract;
	}
    
	public String getF_tips() {
		return f_tips;
	}
	public void setF_tips(String f_tips) {
		this.f_tips = f_tips;
	}
    
	public String getF_imgurl() {
		return f_imgurl;
	}
	public void setF_imgurl(String f_imgurl) {
		this.f_imgurl = f_imgurl;
	}
    
	public String getF_baseid() {
		return f_baseid;
	}
	public void setF_baseid(String f_baseid) {
		this.f_baseid = f_baseid;
	}
    
	public String getF_basename() {
		return f_basename;
	}
	public void setF_basename(String f_basename) {
		this.f_basename = f_basename;
	}

	public String getF_pdfurl() {
		return f_pdfurl;
	}
	public void setF_pdfurl(String f_pdfurl) {
		this.f_pdfurl = f_pdfurl;
	}

	public String getF_adddate() {
		return f_adddate;
	}
	public void setF_adddate(String f_adddate) {
		this.f_adddate = f_adddate;
	}
    
	public String getF_addemployeeid() {
		return f_addemployeeid;
	}
	public void setF_addemployeeid(String f_addemployeeid) {
		this.f_addemployeeid = f_addemployeeid;
	}
    
	public String getF_addemployeename() {
		return f_addemployeename;
	}
	public void setF_addemployeename(String f_addemployeename) {
		this.f_addemployeename = f_addemployeename;
	}
    
	public String getF_lastupdatedate() {
		return f_lastupdatedate;
	}
	public void setF_lastupdatedate(String f_lastupdatedate) {
		this.f_lastupdatedate = f_lastupdatedate;
	}
    
	public String getF_updateemployeeid() {
		return f_updateemployeeid;
	}
	public void setF_updateemployeeid(String f_updateemployeeid) {
		this.f_updateemployeeid = f_updateemployeeid;
	}
    
	public String getF_updateemployeename() {
		return f_updateemployeename;
	}
	public void setF_updateemployeename(String f_updateemployeename) {
		this.f_updateemployeename = f_updateemployeename;
	}
    
	public String getF_deleted() {
		return f_deleted;
	}
	public void setF_deleted(String f_deleted) {
		this.f_deleted = f_deleted;
	}
    
}
