package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="f_form")
public class F_form extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String f_name;
	private String f_displayname;
	private String f_type;
	private String f_originalhtml;
	private String f_parsehtml;
	private Long f_fieldnum;
	private String f_wordtemplate;
	private String f_status;
	private String f_description;
	private String f_adddate;
	private String f_addemployeeid;
	private String f_addemployeename;
	private String f_lastupdatedate;
	private String f_updateemployeeid;
	private String f_updateemployeename;
	private String f_deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getF_name() {
		return f_name;
	}
	public void setF_name(String f_name) {
		this.f_name = f_name;
	}
    
	public String getF_displayname() {
		return f_displayname;
	}
	public void setF_displayname(String f_displayname) {
		this.f_displayname = f_displayname;
	}
    
	public String getF_type() {
		return f_type;
	}
	public void setF_type(String f_type) {
		this.f_type = f_type;
	}
    
	public String getF_originalhtml() {
		return f_originalhtml;
	}
	public void setF_originalhtml(String f_originalhtml) {
		this.f_originalhtml = f_originalhtml;
	}
    
	public String getF_parsehtml() {
		return f_parsehtml;
	}
	public void setF_parsehtml(String f_parsehtml) {
		this.f_parsehtml = f_parsehtml;
	}
    
	public Long getF_fieldnum() {
		return f_fieldnum;
	}
	public void setF_fieldnum(Long f_fieldnum) {
		this.f_fieldnum = f_fieldnum;
	}

	public String getF_wordtemplate() {
		return f_wordtemplate;
	}
	public void setF_wordtemplate(String f_wordtemplate) {
		this.f_wordtemplate = f_wordtemplate;
	}

	public String getF_status() {
		return f_status;
	}
	public void setF_status(String f_status) {
		this.f_status = f_status;
	}

	public String getF_description() {
		return f_description;
	}
	public void setF_description(String f_description) {
		this.f_description = f_description;
	}
    
	public String getF_adddate() {
		return f_adddate;
	}
	public void setF_adddate(String f_adddate) {
		this.f_adddate = f_adddate;
	}
    
	public String getF_addemployeeid() {
		return f_addemployeeid;
	}
	public void setF_addemployeeid(String f_addemployeeid) {
		this.f_addemployeeid = f_addemployeeid;
	}
    
	public String getF_addemployeename() {
		return f_addemployeename;
	}
	public void setF_addemployeename(String f_addemployeename) {
		this.f_addemployeename = f_addemployeename;
	}
    
	public String getF_lastupdatedate() {
		return f_lastupdatedate;
	}
	public void setF_lastupdatedate(String f_lastupdatedate) {
		this.f_lastupdatedate = f_lastupdatedate;
	}
    
	public String getF_updateemployeeid() {
		return f_updateemployeeid;
	}
	public void setF_updateemployeeid(String f_updateemployeeid) {
		this.f_updateemployeeid = f_updateemployeeid;
	}
    
	public String getF_updateemployeename() {
		return f_updateemployeename;
	}
	public void setF_updateemployeename(String f_updateemployeename) {
		this.f_updateemployeename = f_updateemployeename;
	}
    
	public String getF_deleted() {
		return f_deleted;
	}
	public void setF_deleted(String f_deleted) {
		this.f_deleted = f_deleted;
	}
    
}
