package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="e_pushtemplates")
public class E_pushtemplates extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String companyid;
	private String wechatid;
	private String code;
	private String name;
	private String typecode;
	private String typename;
	private String urlpath;
	private String templateid;
	private String details;
	private String statecode;
	private String statename;
	private String remark;
	private String adddate;
	private String addemployeeid;
	private String addemployeename;
	private String updatedate;
	private String updateemployeeid;
	private String updateemployeename;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getCompanyid() {
		return companyid;
	}
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
    
	public String getWechatid() {
		return wechatid;
	}
	public void setWechatid(String wechatid) {
		this.wechatid = wechatid;
	}
    
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
    
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
    
	public String getTypecode() {
		return typecode;
	}
	public void setTypecode(String typecode) {
		this.typecode = typecode;
	}
	public String getTypename() {
		return typename;
	}
	public void setTypename(String typename) {
		this.typename = typename;
	}
	public String getUrlpath() {
		return urlpath;
	}
	public void setUrlpath(String urlpath) {
		this.urlpath = urlpath;
	}
	public String getTemplateid() {
		return templateid;
	}
	public void setTemplateid(String templateid) {
		this.templateid = templateid;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}

	
	public String getStatecode() {
		return statecode;
	}
	public void setStatecode(String statecode) {
		this.statecode = statecode;
	}
	public String getStatename() {
		return statename;
	}
	public void setStatename(String statename) {
		this.statename = statename;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
    
	public String getAdddate() {
		return adddate;
	}
	public void setAdddate(String adddate) {
		this.adddate = adddate;
	}
    
	public String getAddemployeeid() {
		return addemployeeid;
	}
	public void setAddemployeeid(String addemployeeid) {
		this.addemployeeid = addemployeeid;
	}
    
	public String getAddemployeename() {
		return addemployeename;
	}
	public void setAddemployeename(String addemployeename) {
		this.addemployeename = addemployeename;
	}
    
	public String getUpdatedate() {
		return updatedate;
	}
	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}
    
	public String getUpdateemployeeid() {
		return updateemployeeid;
	}
	public void setUpdateemployeeid(String updateemployeeid) {
		this.updateemployeeid = updateemployeeid;
	}
    
	public String getUpdateemployeename() {
		return updateemployeename;
	}
	public void setUpdateemployeename(String updateemployeename) {
		this.updateemployeename = updateemployeename;
	}
    
}
