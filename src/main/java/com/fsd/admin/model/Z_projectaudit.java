package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="z_projectaudit")
public class Z_projectaudit extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String f_projectid;
	private String f_audit1;
	private String f_audit2;
	private String f_audit3;
	private String f_remark3;
	private String f_audit4;
	private String f_remark4;
	private String f_audit5;
	private String f_remark5;
	private String f_audit6;
	private String f_remark6;
	private String f_audit7;
	private String f_remark7;
	private String f_audit8;
	private String f_remark8;
	private String f_audit9;
	private String f_remark9;
	private String f_audit10;
	private String f_remark10;
	private String f_remark2;
	private String f_remark1;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getF_projectid() {
		return f_projectid;
	}
	public void setF_projectid(String f_projectid) {
		this.f_projectid = f_projectid;
	}
    
	public String getF_audit1() {
		return f_audit1;
	}
	public void setF_audit1(String f_audit1) {
		this.f_audit1 = f_audit1;
	}
    
	public String getF_audit2() {
		return f_audit2;
	}
	public void setF_audit2(String f_audit2) {
		this.f_audit2 = f_audit2;
	}
    
	public String getF_audit3() {
		return f_audit3;
	}
	public void setF_audit3(String f_audit3) {
		this.f_audit3 = f_audit3;
	}
    
	public String getF_remark3() {
		return f_remark3;
	}
	public void setF_remark3(String f_remark3) {
		this.f_remark3 = f_remark3;
	}
    
	public String getF_audit4() {
		return f_audit4;
	}
	public void setF_audit4(String f_audit4) {
		this.f_audit4 = f_audit4;
	}
    
	public String getF_remark4() {
		return f_remark4;
	}
	public void setF_remark4(String f_remark4) {
		this.f_remark4 = f_remark4;
	}
    
	public String getF_audit5() {
		return f_audit5;
	}
	public void setF_audit5(String f_audit5) {
		this.f_audit5 = f_audit5;
	}
    
	public String getF_remark5() {
		return f_remark5;
	}
	public void setF_remark5(String f_remark5) {
		this.f_remark5 = f_remark5;
	}
    
	public String getF_audit6() {
		return f_audit6;
	}
	public void setF_audit6(String f_audit6) {
		this.f_audit6 = f_audit6;
	}
    
	public String getF_remark6() {
		return f_remark6;
	}
	public void setF_remark6(String f_remark6) {
		this.f_remark6 = f_remark6;
	}
    
	public String getF_audit7() {
		return f_audit7;
	}
	public void setF_audit7(String f_audit7) {
		this.f_audit7 = f_audit7;
	}
    
	public String getF_remark7() {
		return f_remark7;
	}
	public void setF_remark7(String f_remark7) {
		this.f_remark7 = f_remark7;
	}
    
	public String getF_audit8() {
		return f_audit8;
	}
	public void setF_audit8(String f_audit8) {
		this.f_audit8 = f_audit8;
	}
    
	public String getF_remark8() {
		return f_remark8;
	}
	public void setF_remark8(String f_remark8) {
		this.f_remark8 = f_remark8;
	}
    
	public String getF_audit9() {
		return f_audit9;
	}
	public void setF_audit9(String f_audit9) {
		this.f_audit9 = f_audit9;
	}
    
	public String getF_remark9() {
		return f_remark9;
	}
	public void setF_remark9(String f_remark9) {
		this.f_remark9 = f_remark9;
	}
    
	public String getF_audit10() {
		return f_audit10;
	}
	public void setF_audit10(String f_audit10) {
		this.f_audit10 = f_audit10;
	}
    
	public String getF_remark10() {
		return f_remark10;
	}
	public void setF_remark10(String f_remark10) {
		this.f_remark10 = f_remark10;
	}
    
	public String getF_remark2() {
		return f_remark2;
	}
	public void setF_remark2(String f_remark2) {
		this.f_remark2 = f_remark2;
	}
    
	public String getF_remark1() {
		return f_remark1;
	}
	public void setF_remark1(String f_remark1) {
		this.f_remark1 = f_remark1;
	}
    
}
