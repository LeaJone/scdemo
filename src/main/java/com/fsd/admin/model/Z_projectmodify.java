package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="z_projectmodify")
public class Z_projectmodify extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String f_modifytypeid;
	private String f_modifytypename;
	private String f_modifyinfo;
	private String f_modifyreason;
	private String f_projectid;
	private String f_projectname;
	private String f_adddate;
	private String f_addemployeeid;
	private String f_addemployeename;
	private String f_lastupdatedate;
	private String f_updateemployeeid;
	private String f_updateemployeename;
	private String f_deleted;

	@Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getF_modifytypeid() {
		return f_modifytypeid;
	}
	public void setF_modifytypeid(String f_modifytypeid) {
		this.f_modifytypeid = f_modifytypeid;
	}
    
	public String getF_modifytypename() {
		return f_modifytypename;
	}
	public void setF_modifytypename(String f_modifytypename) {
		this.f_modifytypename = f_modifytypename;
	}
    
	public String getF_modifyinfo() {
		return f_modifyinfo;
	}
	public void setF_modifyinfo(String f_modifyinfo) {
		this.f_modifyinfo = f_modifyinfo;
	}
    
	public String getF_modifyreason() {
		return f_modifyreason;
	}
	public void setF_modifyreason(String f_modifyreason) {
		this.f_modifyreason = f_modifyreason;
	}
    
	public String getF_projectid() {
		return f_projectid;
	}
	public void setF_projectid(String f_projectid) {
		this.f_projectid = f_projectid;
	}
    
	public String getF_projectname() {
		return f_projectname;
	}
	public void setF_projectname(String f_projectname) {
		this.f_projectname = f_projectname;
	}
    
	public String getF_adddate() {
		return f_adddate;
	}
	public void setF_adddate(String f_adddate) {
		this.f_adddate = f_adddate;
	}
    
	public String getF_addemployeeid() {
		return f_addemployeeid;
	}
	public void setF_addemployeeid(String f_addemployeeid) {
		this.f_addemployeeid = f_addemployeeid;
	}
    
	public String getF_addemployeename() {
		return f_addemployeename;
	}
	public void setF_addemployeename(String f_addemployeename) {
		this.f_addemployeename = f_addemployeename;
	}
    
	public String getF_lastupdatedate() {
		return f_lastupdatedate;
	}
	public void setF_lastupdatedate(String f_lastupdatedate) {
		this.f_lastupdatedate = f_lastupdatedate;
	}
    
	public String getF_updateemployeeid() {
		return f_updateemployeeid;
	}
	public void setF_updateemployeeid(String f_updateemployeeid) {
		this.f_updateemployeeid = f_updateemployeeid;
	}
    
	public String getF_updateemployeename() {
		return f_updateemployeename;
	}
	public void setF_updateemployeename(String f_updateemployeename) {
		this.f_updateemployeename = f_updateemployeename;
	}
    
	public String getF_deleted() {
		return f_deleted;
	}
	public void setF_deleted(String f_deleted) {
		this.f_deleted = f_deleted;
	}
    
}
