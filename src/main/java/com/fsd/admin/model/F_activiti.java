package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="f_activiti")
public class F_activiti extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String f_modelid;
	private String f_name;
	private String f_key;
	private String f_description;
	private int f_version;
	private int f_deploystate;
	private String f_adddate;
	private String f_addemployeeid;
	private String f_addemployeename;
	private String f_lastupdatedate;
	private String f_updateemployeeid;
	private String f_updateemployeename;
	private String f_deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getF_name() {
		return f_name;
	}
	public void setF_name(String f_name) {
		this.f_name = f_name;
	}

	public String getF_modelid() {
		return f_modelid;
	}
	public void setF_modelid(String f_modelid) {
		this.f_modelid = f_modelid;
	}

	public String getF_key() {
		return f_key;
	}
	public void setF_key(String f_key) {
		this.f_key = f_key;
	}
    
	public String getF_description() {
		return f_description;
	}
	public void setF_description(String f_description) {
		this.f_description = f_description;
	}
    
	public int getF_version() {
		return f_version;
	}
	public void setF_version(int f_version) {
		this.f_version = f_version;
	}

	public int getF_deploystate() {
		return f_deploystate;
	}
	public void setF_deploystate(int f_deploystate) {
		this.f_deploystate = f_deploystate;
	}

	public String getF_adddate() {
		return f_adddate;
	}
	public void setF_adddate(String f_adddate) {
		this.f_adddate = f_adddate;
	}
    
	public String getF_addemployeeid() {
		return f_addemployeeid;
	}
	public void setF_addemployeeid(String f_addemployeeid) {
		this.f_addemployeeid = f_addemployeeid;
	}
    
	public String getF_addemployeename() {
		return f_addemployeename;
	}
	public void setF_addemployeename(String f_addemployeename) {
		this.f_addemployeename = f_addemployeename;
	}
    
	public String getF_lastupdatedate() {
		return f_lastupdatedate;
	}
	public void setF_lastupdatedate(String f_lastupdatedate) {
		this.f_lastupdatedate = f_lastupdatedate;
	}
    
	public String getF_updateemployeeid() {
		return f_updateemployeeid;
	}
	public void setF_updateemployeeid(String f_updateemployeeid) {
		this.f_updateemployeeid = f_updateemployeeid;
	}
    
	public String getF_updateemployeename() {
		return f_updateemployeename;
	}
	public void setF_updateemployeename(String f_updateemployeename) {
		this.f_updateemployeename = f_updateemployeename;
	}
    
	public String getF_deleted() {
		return f_deleted;
	}
	public void setF_deleted(String f_deleted) {
		this.f_deleted = f_deleted;
	}
    
}
