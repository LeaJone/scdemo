package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="z_projectsite")
public class Z_projectsite extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String f_fid;
	private String f_applypdfurl;
	private String f_abstract;
	private String f_teammien;
	private String f_achievement;
	private String f_adviser;
	private String f_videourl;
	private String f_materials;
	private String f_adddate;
	private String f_addemployeeid;
	private String f_addemployeename;
	private String f_lastupdatedate;
	private String f_updateemployeeid;
	private String f_updateemployeename;
	private String f_deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getF_fid() {
		return f_fid;
	}
	public void setF_fid(String f_fid) {
		this.f_fid = f_fid;
	}

	public String getF_applypdfurl() {
		return f_applypdfurl;
	}
	public void setF_applypdfurl(String f_applypdfurl) {
		this.f_applypdfurl = f_applypdfurl;
	}
    
	public String getF_abstract() {
		return f_abstract;
	}
	public void setF_abstract(String f_abstract) {
		this.f_abstract = f_abstract;
	}
    
	public String getF_teammien() {
		return f_teammien;
	}
	public void setF_teammien(String f_teammien) {
		this.f_teammien = f_teammien;
	}
    
	public String getF_achievement() {
		return f_achievement;
	}
	public void setF_achievement(String f_achievement) {
		this.f_achievement = f_achievement;
	}
    
	public String getF_adviser() {
		return f_adviser;
	}
	public void setF_adviser(String f_adviser) {
		this.f_adviser = f_adviser;
	}
    
	public String getF_videourl() {
		return f_videourl;
	}
	public void setF_videourl(String f_videourl) {
		this.f_videourl = f_videourl;
	}

	public String getF_materials() {
		return f_materials;
	}
	public void setF_materials(String f_materials) {
		this.f_materials = f_materials;
	}

	public String getF_adddate() {
		return f_adddate;
	}
	public void setF_adddate(String f_adddate) {
		this.f_adddate = f_adddate;
	}
    
	public String getF_addemployeeid() {
		return f_addemployeeid;
	}
	public void setF_addemployeeid(String f_addemployeeid) {
		this.f_addemployeeid = f_addemployeeid;
	}
    
	public String getF_addemployeename() {
		return f_addemployeename;
	}
	public void setF_addemployeename(String f_addemployeename) {
		this.f_addemployeename = f_addemployeename;
	}
    
	public String getF_lastupdatedate() {
		return f_lastupdatedate;
	}
	public void setF_lastupdatedate(String f_lastupdatedate) {
		this.f_lastupdatedate = f_lastupdatedate;
	}
    
	public String getF_updateemployeeid() {
		return f_updateemployeeid;
	}
	public void setF_updateemployeeid(String f_updateemployeeid) {
		this.f_updateemployeeid = f_updateemployeeid;
	}
    
	public String getF_updateemployeename() {
		return f_updateemployeename;
	}
	public void setF_updateemployeename(String f_updateemployeename) {
		this.f_updateemployeename = f_updateemployeename;
	}
    
	public String getF_deleted() {
		return f_deleted;
	}
	public void setF_deleted(String f_deleted) {
		this.f_deleted = f_deleted;
	}
    
}
