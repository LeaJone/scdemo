package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="z_qdnrb")
public class Z_qdnrb extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String companyid;
	private String parentid;
	private String bt;
	private String lbid;
	private String lbmc;
	private String cbjg;
	private String ssyj;
	private String sszt;
	private String zrsx;
	private String zzqk;
	private String bz;
	private Long sort;
	private String auditing;
	private String adddate;
	private String addemployeeid;
	private String addemployeename;
	private String updatedate;
	private String updateemployeeid;
	private String updateemployeename;
	private String deleted;
	private String isaurl;
	private String aurl;
	private String isaurlstuff;
	private String aurlstuff;
	private String isaurlguides;
	private String aurlguides;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getCompanyid() {
		return companyid;
	}
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
    
	public String getParentid() {
		return parentid;
	}
	public void setParentid(String parentid) {
		this.parentid = parentid;
	}
    
	public String getBt() {
		return bt;
	}
	public void setBt(String bt) {
		this.bt = bt;
	}
    
	public String getLbid() {
		return lbid;
	}
	public void setLbid(String lbid) {
		this.lbid = lbid;
	}
    
	public String getLbmc() {
		return lbmc;
	}
	public void setLbmc(String lbmc) {
		this.lbmc = lbmc;
	}
    
	public String getCbjg() {
		return cbjg;
	}
	public void setCbjg(String cbjg) {
		this.cbjg = cbjg;
	}
    
	public String getSsyj() {
		return ssyj;
	}
	public void setSsyj(String ssyj) {
		this.ssyj = ssyj;
	}
    
	public String getSszt() {
		return sszt;
	}
	public void setSszt(String sszt) {
		this.sszt = sszt;
	}
    
	public String getZrsx() {
		return zrsx;
	}
	public void setZrsx(String zrsx) {
		this.zrsx = zrsx;
	}
    
	public String getZzqk() {
		return zzqk;
	}
	public void setZzqk(String zzqk) {
		this.zzqk = zzqk;
	}
    
	public String getBz() {
		return bz;
	}
	public void setBz(String bz) {
		this.bz = bz;
	}
    
	public Long getSort() {
		return sort;
	}
	public void setSort(Long sort) {
		this.sort = sort;
	}
    
	public String getAuditing() {
		return auditing;
	}
	public void setAuditing(String auditing) {
		this.auditing = auditing;
	}
    
	public String getAdddate() {
		return adddate;
	}
	public void setAdddate(String adddate) {
		this.adddate = adddate;
	}
    
	public String getAddemployeeid() {
		return addemployeeid;
	}
	public void setAddemployeeid(String addemployeeid) {
		this.addemployeeid = addemployeeid;
	}
    
	public String getAddemployeename() {
		return addemployeename;
	}
	public void setAddemployeename(String addemployeename) {
		this.addemployeename = addemployeename;
	}
    
	public String getUpdatedate() {
		return updatedate;
	}
	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}
    
	public String getUpdateemployeeid() {
		return updateemployeeid;
	}
	public void setUpdateemployeeid(String updateemployeeid) {
		this.updateemployeeid = updateemployeeid;
	}
    
	public String getUpdateemployeename() {
		return updateemployeename;
	}
	public void setUpdateemployeename(String updateemployeename) {
		this.updateemployeename = updateemployeename;
	}
    
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
    
	public String getIsaurl() {
		return isaurl;
	}
	public void setIsaurl(String isaurl) {
		this.isaurl = isaurl;
	}
    
	public String getAurl() {
		return aurl;
	}
	public void setAurl(String aurl) {
		this.aurl = aurl;
	}
	public String getIsaurlstuff() {
		return isaurlstuff;
	}
	public void setIsaurlstuff(String isaurlstuff) {
		this.isaurlstuff = isaurlstuff;
	}
	public String getAurlstuff() {
		return aurlstuff;
	}
	public void setAurlstuff(String aurlstuff) {
		this.aurlstuff = aurlstuff;
	}
	public String getIsaurlguides() {
		return isaurlguides;
	}
	public void setIsaurlguides(String isaurlguides) {
		this.isaurlguides = isaurlguides;
	}
	public String getAurlguides() {
		return aurlguides;
	}
	public void setAurlguides(String aurlguides) {
		this.aurlguides = aurlguides;
	}
    
}
