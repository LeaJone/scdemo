package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

/**
 * 课时信息(课程内容)
 * @author Administrator
 *
 */
@Entity
@Table(name="t_coursecontent")
public class T_coursecontent extends BaseModel{
    private static final long serialVersionUID = 1L;
    
	private String id;
	private String t_courseinfoid; //课程编号
	private String t_courseinfoname; //课程名称
	private String f_title; //课时题目
	private String f_fileurl; //文件路径
	private String f_videourl; //视频路径
	private String f_content; //课时内容
	private Long f_order; //排序编号
	private String f_state; //状态
	private String f_adduser; //添加人员
	private String f_adddate; //添加时间
	private String f_updateuser; //修改人员
	private String f_updatedate; //修改时间
	private Long f_deleted; //删除标志
	private String f_accessoryurl; //附件路径
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getT_courseinfoid() {
		return t_courseinfoid;
	}
	public void setT_courseinfoid(String t_courseinfoid) {
		this.t_courseinfoid = t_courseinfoid;
	}
    
	public String getF_title() {
		return f_title;
	}
	public void setF_title(String f_title) {
		this.f_title = f_title;
	}
    
	public String getF_fileurl() {
		return f_fileurl;
	}
	public void setF_fileurl(String f_fileurl) {
		this.f_fileurl = f_fileurl;
	}
    
	public String getF_videourl() {
		return f_videourl;
	}
	public void setF_videourl(String f_videourl) {
		this.f_videourl = f_videourl;
	}
    
	public String getF_content() {
		return f_content;
	}
	public void setF_content(String f_content) {
		this.f_content = f_content;
	}
    
	public Long getF_order() {
		return f_order;
	}
	public void setF_order(Long f_order) {
		this.f_order = f_order;
	}
    
	public String getF_state() {
		return f_state;
	}
	public void setF_state(String f_state) {
		this.f_state = f_state;
	}
    
	public String getF_adduser() {
		return f_adduser;
	}
	public void setF_adduser(String f_adduser) {
		this.f_adduser = f_adduser;
	}
    
	public String getF_adddate() {
		return f_adddate;
	}
	public void setF_adddate(String f_adddate) {
		this.f_adddate = f_adddate;
	}
    
	public String getF_updateuser() {
		return f_updateuser;
	}
	public void setF_updateuser(String f_updateuser) {
		this.f_updateuser = f_updateuser;
	}
    
	public String getF_updatedate() {
		return f_updatedate;
	}
	public void setF_updatedate(String f_updatedate) {
		this.f_updatedate = f_updatedate;
	}
    
	public Long getF_deleted() {
		return f_deleted;
	}
	public void setF_deleted(Long f_deleted) {
		this.f_deleted = f_deleted;
	}
	public String getF_accessoryurl() {
		return f_accessoryurl;
	}
	public void setF_accessoryurl(String f_accessoryurl) {
		this.f_accessoryurl = f_accessoryurl;
	}
	public String getT_courseinfoname() {
		return t_courseinfoname;
	}
	public void setT_courseinfoname(String t_courseinfoname) {
		this.t_courseinfoname = t_courseinfoname;
	}
	
}
