package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="f_formfield")
public class F_formfield extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String f_formid;
	private String f_tablename;
	private String f_name;
	private String f_plugins;
	private String f_title;
	private String f_type;
	private String f_flow;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getF_formid() {
		return f_formid;
	}
	public void setF_formid(String f_formid) {
		this.f_formid = f_formid;
	}

	public String getF_tablename() {
		return f_tablename;
	}
	public void setF_tablename(String f_tablename) {
		this.f_tablename = f_tablename;
	}

	public String getF_name() {
		return f_name;
	}
	public void setF_name(String f_name) {
		this.f_name = f_name;
	}
    
	public String getF_plugins() {
		return f_plugins;
	}
	public void setF_plugins(String f_plugins) {
		this.f_plugins = f_plugins;
	}
    
	public String getF_title() {
		return f_title;
	}
	public void setF_title(String f_title) {
		this.f_title = f_title;
	}
    
	public String getF_type() {
		return f_type;
	}
	public void setF_type(String f_type) {
		this.f_type = f_type;
	}
    
	public String getF_flow() {
		return f_flow;
	}
	public void setF_flow(String f_flow) {
		this.f_flow = f_flow;
	}
    
}
