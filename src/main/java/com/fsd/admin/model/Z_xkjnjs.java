package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="z_xkjnjs")
public class Z_xkjnjs extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String f_branchid;
	private String f_code;
	private String f_name;
	private String f_gamename;
	private String f_gamelevelid;
	private String f_gamelevelname;
	private String f_gametypeid;
	private String f_gametypename;
	private String f_hostunit;
	private String f_undertaker;
	private String f_jslyxx;
	private String f_profession;
	private String f_formid;
	private String f_formname;
	private String f_gametime;
	private String f_site;
	private String f_baseid;
	private String f_basename;
	private String f_baseleaderid;
	private String f_baseleadername;
	private String f_applyformoney;
	private String f_matingmoney;
	private String f_matingmoneysource;
	private String f_group;
	private String f_people;
	private String f_leaderid;
	private String f_leadername;
	private String f_leaderphone;
	private String f_leadertitle;
	private String f_leaderbranch;
	private String f_leaderresponsibility;
	private String f_abstract;
	private String f_scheme;
	private String f_condition;
	private String f_train;
	private String f_pdfurl;
	private String f_statusid;
	private String f_statusname;
	private String f_issummarize;
	private String f_adddate;
	private String f_addemployeeid;
	private String f_addemployeename;
	private String f_lastupdatedate;
	private String f_updateemployeeid;
	private String f_updateemployeename;
	private String f_deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getF_branchid() {
		return f_branchid;
	}
	public void setF_branchid(String f_branchid) {
		this.f_branchid = f_branchid;
	}

	public String getF_code() {
		return f_code;
	}
	public void setF_code(String f_code) {
		this.f_code = f_code;
	}

	public String getF_name() {
		return f_name;
	}
	public void setF_name(String f_name) {
		this.f_name = f_name;
	}
    
	public String getF_gamename() {
		return f_gamename;
	}
	public void setF_gamename(String f_gamename) {
		this.f_gamename = f_gamename;
	}
    
	public String getF_gamelevelid() {
		return f_gamelevelid;
	}
	public void setF_gamelevelid(String f_gamelevelid) {
		this.f_gamelevelid = f_gamelevelid;
	}
    
	public String getF_gamelevelname() {
		return f_gamelevelname;
	}
	public void setF_gamelevelname(String f_gamelevelname) {
		this.f_gamelevelname = f_gamelevelname;
	}
    
	public String getF_gametypeid() {
		return f_gametypeid;
	}
	public void setF_gametypeid(String f_gametypeid) {
		this.f_gametypeid = f_gametypeid;
	}
    
	public String getF_gametypename() {
		return f_gametypename;
	}
	public void setF_gametypename(String f_gametypename) {
		this.f_gametypename = f_gametypename;
	}
    
	public String getF_hostunit() {
		return f_hostunit;
	}
	public void setF_hostunit(String f_hostunit) {
		this.f_hostunit = f_hostunit;
	}
    
	public String getF_undertaker() {
		return f_undertaker;
	}
	public void setF_undertaker(String f_undertaker) {
		this.f_undertaker = f_undertaker;
	}

	public String getF_jslyxx() {
		return f_jslyxx;
	}
	public void setF_jslyxx(String f_jslyxx) {
		this.f_jslyxx = f_jslyxx;
	}

	public String getF_profession() {
		return f_profession;
	}
	public void setF_profession(String f_profession) {
		this.f_profession = f_profession;
	}
    
	public String getF_formid() {
		return f_formid;
	}
	public void setF_formid(String f_formid) {
		this.f_formid = f_formid;
	}
    
	public String getF_formname() {
		return f_formname;
	}
	public void setF_formname(String f_formname) {
		this.f_formname = f_formname;
	}
    
	public String getF_gametime() {
		return f_gametime;
	}
	public void setF_gametime(String f_gametime) {
		this.f_gametime = f_gametime;
	}
    
	public String getF_site() {
		return f_site;
	}
	public void setF_site(String f_site) {
		this.f_site = f_site;
	}
    
	public String getF_baseid() {
		return f_baseid;
	}
	public void setF_baseid(String f_baseid) {
		this.f_baseid = f_baseid;
	}
    
	public String getF_basename() {
		return f_basename;
	}
	public void setF_basename(String f_basename) {
		this.f_basename = f_basename;
	}
    
	public String getF_baseleaderid() {
		return f_baseleaderid;
	}
	public void setF_baseleaderid(String f_baseleaderid) {
		this.f_baseleaderid = f_baseleaderid;
	}
    
	public String getF_baseleadername() {
		return f_baseleadername;
	}
	public void setF_baseleadername(String f_baseleadername) {
		this.f_baseleadername = f_baseleadername;
	}
    
	public String getF_applyformoney() {
		return f_applyformoney;
	}
	public void setF_applyformoney(String f_applyformoney) {
		this.f_applyformoney = f_applyformoney;
	}
    
	public String getF_matingmoney() {
		return f_matingmoney;
	}
	public void setF_matingmoney(String f_matingmoney) {
		this.f_matingmoney = f_matingmoney;
	}
    
	public String getF_matingmoneysource() {
		return f_matingmoneysource;
	}
	public void setF_matingmoneysource(String f_matingmoneysource) {
		this.f_matingmoneysource = f_matingmoneysource;
	}
    
	public String getF_group() {
		return f_group;
	}
	public void setF_group(String f_group) {
		this.f_group = f_group;
	}
    
	public String getF_people() {
		return f_people;
	}
	public void setF_people(String f_people) {
		this.f_people = f_people;
	}
    
	public String getF_leaderid() {
		return f_leaderid;
	}
	public void setF_leaderid(String f_leaderid) {
		this.f_leaderid = f_leaderid;
	}
    
	public String getF_leadername() {
		return f_leadername;
	}
	public void setF_leadername(String f_leadername) {
		this.f_leadername = f_leadername;
	}

	public String getF_leaderphone() {
		return f_leaderphone;
	}

	public void setF_leaderphone(String f_leaderphone) {
		this.f_leaderphone = f_leaderphone;
	}

	public String getF_leadertitle() {
		return f_leadertitle;
	}

	public void setF_leadertitle(String f_leadertitle) {
		this.f_leadertitle = f_leadertitle;
	}

	public String getF_leaderbranch() {
		return f_leaderbranch;
	}

	public void setF_leaderbranch(String f_leaderbranch) {
		this.f_leaderbranch = f_leaderbranch;
	}

	public String getF_leaderresponsibility() {
		return f_leaderresponsibility;
	}

	public void setF_leaderresponsibility(String f_leaderresponsibility) {
		this.f_leaderresponsibility = f_leaderresponsibility;
	}
	public String getF_abstract() {
		return f_abstract;
	}
	public void setF_abstract(String f_abstract) {
		this.f_abstract = f_abstract;
	}
    
	public String getF_scheme() {
		return f_scheme;
	}
	public void setF_scheme(String f_scheme) {
		this.f_scheme = f_scheme;
	}
    
	public String getF_condition() {
		return f_condition;
	}
	public void setF_condition(String f_condition) {
		this.f_condition = f_condition;
	}
    
	public String getF_train() {
		return f_train;
	}
	public void setF_train(String f_train) {
		this.f_train = f_train;
	}

	public String getF_pdfurl() {
		return f_pdfurl;
	}
	public void setF_pdfurl(String f_pdfurl) {
		this.f_pdfurl = f_pdfurl;
	}

	public String getF_statusid() {
		return f_statusid;
	}
	public void setF_statusid(String f_statusid) {
		this.f_statusid = f_statusid;
	}

	public String getF_statusname() {
		return f_statusname;
	}
	public void setF_statusname(String f_statusname) {
		this.f_statusname = f_statusname;
	}

	public String getF_issummarize() {
		return f_issummarize;
	}
	public void setF_issummarize(String f_issummarize) {
		this.f_issummarize = f_issummarize;
	}

	public String getF_adddate() {
		return f_adddate;
	}
	public void setF_adddate(String f_adddate) {
		this.f_adddate = f_adddate;
	}
    
	public String getF_addemployeeid() {
		return f_addemployeeid;
	}
	public void setF_addemployeeid(String f_addemployeeid) {
		this.f_addemployeeid = f_addemployeeid;
	}
    
	public String getF_addemployeename() {
		return f_addemployeename;
	}
	public void setF_addemployeename(String f_addemployeename) {
		this.f_addemployeename = f_addemployeename;
	}
    
	public String getF_lastupdatedate() {
		return f_lastupdatedate;
	}
	public void setF_lastupdatedate(String f_lastupdatedate) {
		this.f_lastupdatedate = f_lastupdatedate;
	}
    
	public String getF_updateemployeeid() {
		return f_updateemployeeid;
	}
	public void setF_updateemployeeid(String f_updateemployeeid) {
		this.f_updateemployeeid = f_updateemployeeid;
	}
    
	public String getF_updateemployeename() {
		return f_updateemployeename;
	}
	public void setF_updateemployeename(String f_updateemployeename) {
		this.f_updateemployeename = f_updateemployeename;
	}
    
	public String getF_deleted() {
		return f_deleted;
	}
	public void setF_deleted(String f_deleted) {
		this.f_deleted = f_deleted;
	}
    
}
