package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="z_jsydsp")
public class Z_jsydsp extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String companyid;
	private String pzwh;
	private String xmmc;
	private String xmwz;
	private String pzsj;
	private String pznf;
	private String pzmj;
	private String ydmj;
	private Long sort;
	private String remark;
	private String adddate;
	private String addemployeeid;
	private String addemployeename;
	private String updatedate;
	private String updateemployeeid;
	private String updateemployeename;
	private String deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getCompanyid() {
		return companyid;
	}
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
    
	public String getPzwh() {
		return pzwh;
	}
	public void setPzwh(String pzwh) {
		this.pzwh = pzwh;
	}
    
	public String getXmmc() {
		return xmmc;
	}
	public void setXmmc(String xmmc) {
		this.xmmc = xmmc;
	}
    
	public String getXmwz() {
		return xmwz;
	}
	public void setXmwz(String xmwz) {
		this.xmwz = xmwz;
	}
    
	public String getPzsj() {
		return pzsj;
	}
	public void setPzsj(String pzsj) {
		this.pzsj = pzsj;
	}
    
	public String getPznf() {
		return pznf;
	}
	public void setPznf(String pznf) {
		this.pznf = pznf;
	}
    
	public String getPzmj() {
		return pzmj;
	}
	public void setPzmj(String pzmj) {
		this.pzmj = pzmj;
	}
    
	public String getYdmj() {
		return ydmj;
	}
	public void setYdmj(String ydmj) {
		this.ydmj = ydmj;
	}
    
	public Long getSort() {
		return sort;
	}
	public void setSort(Long sort) {
		this.sort = sort;
	}
    
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
    
	public String getAdddate() {
		return adddate;
	}
	public void setAdddate(String adddate) {
		this.adddate = adddate;
	}
    
	public String getAddemployeeid() {
		return addemployeeid;
	}
	public void setAddemployeeid(String addemployeeid) {
		this.addemployeeid = addemployeeid;
	}
    
	public String getAddemployeename() {
		return addemployeename;
	}
	public void setAddemployeename(String addemployeename) {
		this.addemployeename = addemployeename;
	}
    
	public String getUpdatedate() {
		return updatedate;
	}
	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}
    
	public String getUpdateemployeeid() {
		return updateemployeeid;
	}
	public void setUpdateemployeeid(String updateemployeeid) {
		this.updateemployeeid = updateemployeeid;
	}
    
	public String getUpdateemployeename() {
		return updateemployeename;
	}
	public void setUpdateemployeename(String updateemployeename) {
		this.updateemployeename = updateemployeename;
	}
    
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
    
}
