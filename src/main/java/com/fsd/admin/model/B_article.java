package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.DocumentId;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Store;
import org.wltea.analyzer.lucene.IKAnalyzer;
import com.fsd.core.model.BaseModel;

@Indexed(index="article") @Analyzer(impl = IKAnalyzer.class)
@Entity
@Table(name="b_article")
public class B_article extends BaseModel{
    private static final long serialVersionUID = 1L;
    
    @DocumentId()
	private String id;
    @Field(store = Store.YES, index = Index.YES)
	private String companyid;
	private String subjectid;
	private String subjectname;
	private String type;
	private String typename;
	@Field(store = Store.YES, index = Index.YES)
	private String title;
	@Field(store = Store.YES, index = Index.YES)
	private String subtitle;
	private String showtitle;
	private String titlecolor;
	private String recommend;
	private String firstly;
	private String video;
	private String roll;
	private String headline;
	private String hot;
	private String slide;
	private String comments;
	private String keyword;
	private String aurl;
	private String isaurl;
	private String author;
	private String source;
	private String showtype;
	private String showtypename;
	private String overdue;
	@Field(store = Store.YES, index = Index.YES)
	private String abstracts;
	private String content;
	private String imageurl1;
	private String imageurl2;
	private String videourl;
	private String voiceurl;
	private Long wordnum;
	private Long imagenum;
	private Long sort;
	@Field(store = Store.YES, index = Index.YES)
	private String auditing;
	private String auditingdate;
	private String auditingempid;
	private String auditingempname;
	private String ismobile;
	private String msubjectid;
	private String msubjectname;
	private String mimageurl;
	private String mimagedepict;
	private String misaurl;
	private String maurl;
	private String mcontent;
	private String mabstracts;
	private String remark;
	private String branchid;
	private String branchname;
	private String adddate;
	private String addemployeeid;
	private String addemployeename;
	private String updatedate;
	private String updateemployeeid;
	private String updateemployeename;
	@Field(store = Store.YES, index = Index.YES)
	private String deleted;
	private String ispush;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getCompanyid() {
		return companyid;
	}
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
    
	public String getSubjectid() {
		return subjectid;
	}
	public void setSubjectid(String subjectid) {
		this.subjectid = subjectid;
	}
    
	public String getSubjectname() {
		return subjectname;
	}
	public void setSubjectname(String subjectname) {
		this.subjectname = subjectname;
	}
    
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
    
	public String getTypename() {
		return typename;
	}
	public void setTypename(String typename) {
		this.typename = typename;
	}
    
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
    
	public String getSubtitle() {
		return subtitle;
	}
	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
    
	public String getShowtitle() {
		return showtitle;
	}
	public void setShowtitle(String showtitle) {
		this.showtitle = showtitle;
	}
    
	public String getTitlecolor() {
		return titlecolor;
	}
	public void setTitlecolor(String titlecolor) {
		this.titlecolor = titlecolor;
	}
    
	public String getRecommend() {
		return recommend;
	}
	public void setRecommend(String recommend) {
		this.recommend = recommend;
	}
    
	public String getFirstly() {
		return firstly;
	}
	public void setFirstly(String firstly) {
		this.firstly = firstly;
	}
    
	public String getVideo() {
		return video;
	}
	public void setVideo(String video) {
		this.video = video;
	}
    
	public String getRoll() {
		return roll;
	}
	public void setRoll(String roll) {
		this.roll = roll;
	}
    
	public String getHeadline() {
		return headline;
	}
	public void setHeadline(String headline) {
		this.headline = headline;
	}
    
	public String getHot() {
		return hot;
	}
	public void setHot(String hot) {
		this.hot = hot;
	}
    
	public String getSlide() {
		return slide;
	}
	public void setSlide(String slide) {
		this.slide = slide;
	}
    
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
    
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
    
	public String getAurl() {
		return aurl;
	}
	public void setAurl(String aurl) {
		this.aurl = aurl;
	}
    
	public String getIsaurl() {
		return isaurl;
	}
	public void setIsaurl(String isaurl) {
		this.isaurl = isaurl;
	}
    
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
    
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
    
	public String getShowtype() {
		return showtype;
	}
	public void setShowtype(String showtype) {
		this.showtype = showtype;
	}
    
	public String getShowtypename() {
		return showtypename;
	}
	public void setShowtypename(String showtypename) {
		this.showtypename = showtypename;
	}
    
	public String getOverdue() {
		return overdue;
	}
	public void setOverdue(String overdue) {
		this.overdue = overdue;
	}
    
	public String getAbstracts() {
		return abstracts;
	}
	public void setAbstracts(String abstracts) {
		this.abstracts = abstracts;
	}
    
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
    
	public String getImageurl1() {
		return imageurl1;
	}
	public void setImageurl1(String imageurl1) {
		this.imageurl1 = imageurl1;
	}
    
	public String getImageurl2() {
		return imageurl2;
	}
	public void setImageurl2(String imageurl2) {
		this.imageurl2 = imageurl2;
	}
    
	public String getVideourl() {
		return videourl;
	}
	public void setVideourl(String videourl) {
		this.videourl = videourl;
	}
    
	public String getVoiceurl() {
		return voiceurl;
	}
	public void setVoiceurl(String voiceurl) {
		this.voiceurl = voiceurl;
	}
	
	public Long getWordnum() {
		return wordnum;
	}
	public void setWordnum(Long wordnum) {
		this.wordnum = wordnum;
	}
    
	public Long getImagenum() {
		return imagenum;
	}
	public void setImagenum(Long imagenum) {
		this.imagenum = imagenum;
	}
    
	public Long getSort() {
		return sort;
	}
	public void setSort(Long sort) {
		this.sort = sort;
	}
    
	public String getAuditing() {
		return auditing;
	}
	public void setAuditing(String auditing) {
		this.auditing = auditing;
	}
    
	public String getAuditingdate() {
		return auditingdate;
	}
	public void setAuditingdate(String auditingdate) {
		this.auditingdate = auditingdate;
	}
	
	public String getAuditingempid() {
		return auditingempid;
	}
	public void setAuditingempid(String auditingempid) {
		this.auditingempid = auditingempid;
	}
	
	public String getAuditingempname() {
		return auditingempname;
	}
	public void setAuditingempname(String auditingempname) {
		this.auditingempname = auditingempname;
	}
	
	public String getIsmobile() {
		return ismobile;
	}
	public void setIsmobile(String ismobile) {
		this.ismobile = ismobile;
	}
    
	public String getMsubjectid() {
		return msubjectid;
	}
	public void setMsubjectid(String msubjectid) {
		this.msubjectid = msubjectid;
	}
    
	public String getMsubjectname() {
		return msubjectname;
	}
	public void setMsubjectname(String msubjectname) {
		this.msubjectname = msubjectname;
	}
    
	public String getMimageurl() {
		return mimageurl;
	}
	public void setMimageurl(String mimageurl) {
		this.mimageurl = mimageurl;
	}
    
	public String getMimagedepict() {
		return mimagedepict;
	}
	public void setMimagedepict(String mimagedepict) {
		this.mimagedepict = mimagedepict;
	}
    
	public String getMisaurl() {
		return misaurl;
	}
	public void setMisaurl(String misaurl) {
		this.misaurl = misaurl;
	}
    
	public String getMaurl() {
		return maurl;
	}
	public void setMaurl(String maurl) {
		this.maurl = maurl;
	}
    
	public String getMcontent() {
		return mcontent;
	}
	public void setMcontent(String mcontent) {
		this.mcontent = mcontent;
	}
    
	public String getMabstracts() {
		return mabstracts;
	}
	public void setMabstracts(String mabstracts) {
		this.mabstracts = mabstracts;
	}
    
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	public String getBranchid() {
		return branchid;
	}
	public void setBranchid(String branchid) {
		this.branchid = branchid;
	}
    
	public String getBranchname() {
		return branchname;
	}
	public void setBranchname(String branchname) {
		this.branchname = branchname;
	}
    
	public String getAdddate() {
		return adddate;
	}
	public void setAdddate(String adddate) {
		this.adddate = adddate;
	}
    
	public String getAddemployeeid() {
		return addemployeeid;
	}
	public void setAddemployeeid(String addemployeeid) {
		this.addemployeeid = addemployeeid;
	}
    
	public String getAddemployeename() {
		return addemployeename;
	}
	public void setAddemployeename(String addemployeename) {
		this.addemployeename = addemployeename;
	}
    
	public String getUpdatedate() {
		return updatedate;
	}
	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}
    
	public String getUpdateemployeeid() {
		return updateemployeeid;
	}
	public void setUpdateemployeeid(String updateemployeeid) {
		this.updateemployeeid = updateemployeeid;
	}
    
	public String getUpdateemployeename() {
		return updateemployeename;
	}
	public void setUpdateemployeename(String updateemployeename) {
		this.updateemployeename = updateemployeename;
	}
    
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}

	@Transient
	public String getIspush() {
		return ispush;
	}
	public void setIspush(String ispush) {
		this.ispush = ispush;
	}
}
