package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="f_flowtype")
public class F_flowtype extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String companyid;
	private String name;
	private String typecode;
	private String typename;
	private String dictionarykey;
	private String structurecode;
	private String structureurl;
	private Long sort;
	private String deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getCompanyid() {
		return companyid;
	}
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
    
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
    
	public String getTypecode() {
		return typecode;
	}
	public void setTypecode(String typecode) {
		this.typecode = typecode;
	}
    
	public String getTypename() {
		return typename;
	}
	public void setTypename(String typename) {
		this.typename = typename;
	}
    
	public String getDictionarykey() {
		return dictionarykey;
	}
	public void setDictionarykey(String dictionarykey) {
		this.dictionarykey = dictionarykey;
	}
    
	public String getStructurecode() {
		return structurecode;
	}
	public void setStructurecode(String structurecode) {
		this.structurecode = structurecode;
	}
    
	public String getStructureurl() {
		return structureurl;
	}
	public void setStructureurl(String structureurl) {
		this.structureurl = structureurl;
	}
    
	public Long getSort() {
		return sort;
	}
	public void setSort(Long sort) {
		this.sort = sort;
	}
    
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
    
}
