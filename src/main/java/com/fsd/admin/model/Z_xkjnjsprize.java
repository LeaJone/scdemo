package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="z_xkjnjsprize")
public class Z_xkjnjsprize extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String f_projectid;
	private String f_fid;
	private String f_name;
	private String f_typeid;
	private String f_typename;
	private String f_levelid;
	private String f_levelname;
	private String f_teacher;
	private String f_student;
	private String f_remark;
	private String f_adddate;
	private String f_addemployeeid;
	private String f_addemployeename;
	private String f_lastupdatedate;
	private String f_updateemployeeid;
	private String f_updateemployeename;
	private String f_deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getF_projectid() {
		return f_projectid;
	}
	public void setF_projectid(String f_projectid) {
		this.f_projectid = f_projectid;
	}
    
	public String getF_fid() {
		return f_fid;
	}
	public void setF_fid(String f_fid) {
		this.f_fid = f_fid;
	}
    
	public String getF_name() {
		return f_name;
	}
	public void setF_name(String f_name) {
		this.f_name = f_name;
	}
    
	public String getF_typeid() {
		return f_typeid;
	}
	public void setF_typeid(String f_typeid) {
		this.f_typeid = f_typeid;
	}
    
	public String getF_typename() {
		return f_typename;
	}
	public void setF_typename(String f_typename) {
		this.f_typename = f_typename;
	}
    
	public String getF_levelid() {
		return f_levelid;
	}
	public void setF_levelid(String f_levelid) {
		this.f_levelid = f_levelid;
	}
    
	public String getF_levelname() {
		return f_levelname;
	}
	public void setF_levelname(String f_levelname) {
		this.f_levelname = f_levelname;
	}
    
	public String getF_teacher() {
		return f_teacher;
	}
	public void setF_teacher(String f_teacher) {
		this.f_teacher = f_teacher;
	}
    
	public String getF_student() {
		return f_student;
	}
	public void setF_student(String f_student) {
		this.f_student = f_student;
	}
    
	public String getF_remark() {
		return f_remark;
	}
	public void setF_remark(String f_remark) {
		this.f_remark = f_remark;
	}
    
	public String getF_adddate() {
		return f_adddate;
	}
	public void setF_adddate(String f_adddate) {
		this.f_adddate = f_adddate;
	}
    
	public String getF_addemployeeid() {
		return f_addemployeeid;
	}
	public void setF_addemployeeid(String f_addemployeeid) {
		this.f_addemployeeid = f_addemployeeid;
	}
    
	public String getF_addemployeename() {
		return f_addemployeename;
	}
	public void setF_addemployeename(String f_addemployeename) {
		this.f_addemployeename = f_addemployeename;
	}
    
	public String getF_lastupdatedate() {
		return f_lastupdatedate;
	}
	public void setF_lastupdatedate(String f_lastupdatedate) {
		this.f_lastupdatedate = f_lastupdatedate;
	}
    
	public String getF_updateemployeeid() {
		return f_updateemployeeid;
	}
	public void setF_updateemployeeid(String f_updateemployeeid) {
		this.f_updateemployeeid = f_updateemployeeid;
	}
    
	public String getF_updateemployeename() {
		return f_updateemployeename;
	}
	public void setF_updateemployeename(String f_updateemployeename) {
		this.f_updateemployeename = f_updateemployeename;
	}
    
	public String getF_deleted() {
		return f_deleted;
	}
	public void setF_deleted(String f_deleted) {
		this.f_deleted = f_deleted;
	}
    
}
