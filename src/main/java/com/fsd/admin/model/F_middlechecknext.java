package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="f_middlechecknext")
public class F_middlechecknext extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String f_projectid;
	private String f_projectname;
	private String f_fid;
	private String f_starttime;
	private String f_stoptime;
	private String f_content;
	private String f_adddate;
	private String f_addemployeeid;
	private String f_addemployeename;
	private String f_lastupdatedate;
	private String f_updateemployeeid;
	private String f_updateemployeename;
	private String f_deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getF_projectid() {
		return f_projectid;
	}
	public void setF_projectid(String f_projectid) {
		this.f_projectid = f_projectid;
	}
    
	public String getF_projectname() {
		return f_projectname;
	}
	public void setF_projectname(String f_projectname) {
		this.f_projectname = f_projectname;
	}
    
	public String getF_fid() {
		return f_fid;
	}
	public void setF_fid(String f_fid) {
		this.f_fid = f_fid;
	}
    
	public String getF_starttime() {
		return f_starttime;
	}
	public void setF_starttime(String f_starttime) {
		this.f_starttime = f_starttime;
	}
    
	public String getF_stoptime() {
		return f_stoptime;
	}
	public void setF_stoptime(String f_stoptime) {
		this.f_stoptime = f_stoptime;
	}
    
	public String getF_content() {
		return f_content;
	}
	public void setF_content(String f_content) {
		this.f_content = f_content;
	}
    
	public String getF_adddate() {
		return f_adddate;
	}
	public void setF_adddate(String f_adddate) {
		this.f_adddate = f_adddate;
	}
    
	public String getF_addemployeeid() {
		return f_addemployeeid;
	}
	public void setF_addemployeeid(String f_addemployeeid) {
		this.f_addemployeeid = f_addemployeeid;
	}
    
	public String getF_addemployeename() {
		return f_addemployeename;
	}
	public void setF_addemployeename(String f_addemployeename) {
		this.f_addemployeename = f_addemployeename;
	}
    
	public String getF_lastupdatedate() {
		return f_lastupdatedate;
	}
	public void setF_lastupdatedate(String f_lastupdatedate) {
		this.f_lastupdatedate = f_lastupdatedate;
	}
    
	public String getF_updateemployeeid() {
		return f_updateemployeeid;
	}
	public void setF_updateemployeeid(String f_updateemployeeid) {
		this.f_updateemployeeid = f_updateemployeeid;
	}
    
	public String getF_updateemployeename() {
		return f_updateemployeename;
	}
	public void setF_updateemployeename(String f_updateemployeename) {
		this.f_updateemployeename = f_updateemployeename;
	}
    
	public String getF_deleted() {
		return f_deleted;
	}
	public void setF_deleted(String f_deleted) {
		this.f_deleted = f_deleted;
	}
    
}
