package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="z_articlefz")
public class Z_articlefz extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String companyid;
	private String articleid;
	private String mc;
	private String syh;
	private String wh;
	private String fwsj;
	private String fwjg;
	private String ztfl;
	private String zpfl;
	private String tzfl;
	private String gkxs;
	private String gkfs;
	private String gkfw;
	private String yxq;
	private String fznr;
	private String adddate;
	private String addemployeeid;
	private String addemployeename;
	private String updatedate;
	private String updateemployeeid;
	private String updateemployeename;
	private String deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getCompanyid() {
		return companyid;
	}
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
    
	public String getArticleid() {
		return articleid;
	}
	public void setArticleid(String articleid) {
		this.articleid = articleid;
	}
    
	public String getMc() {
		return mc;
	}
	public void setMc(String mc) {
		this.mc = mc;
	}
    
	public String getSyh() {
		return syh;
	}
	public void setSyh(String syh) {
		this.syh = syh;
	}
    
	public String getWh() {
		return wh;
	}
	public void setWh(String wh) {
		this.wh = wh;
	}
    
	public String getFwsj() {
		return fwsj;
	}
	public void setFwsj(String fwsj) {
		this.fwsj = fwsj;
	}
    
	public String getFwjg() {
		return fwjg;
	}
	public void setFwjg(String fwjg) {
		this.fwjg = fwjg;
	}
    
	public String getZtfl() {
		return ztfl;
	}
	public void setZtfl(String ztfl) {
		this.ztfl = ztfl;
	}
    
	public String getZpfl() {
		return zpfl;
	}
	public void setZpfl(String zpfl) {
		this.zpfl = zpfl;
	}
    
	public String getTzfl() {
		return tzfl;
	}
	public void setTzfl(String tzfl) {
		this.tzfl = tzfl;
	}
    
	public String getGkxs() {
		return gkxs;
	}
	public void setGkxs(String gkxs) {
		this.gkxs = gkxs;
	}
    
	public String getGkfs() {
		return gkfs;
	}
	public void setGkfs(String gkfs) {
		this.gkfs = gkfs;
	}
    
	public String getGkfw() {
		return gkfw;
	}
	public void setGkfw(String gkfw) {
		this.gkfw = gkfw;
	}
    
	public String getYxq() {
		return yxq;
	}
	public void setYxq(String yxq) {
		this.yxq = yxq;
	}
    
	public String getFznr() {
		return fznr;
	}
	public void setFznr(String fznr) {
		this.fznr = fznr;
	}
    
	public String getAdddate() {
		return adddate;
	}
	public void setAdddate(String adddate) {
		this.adddate = adddate;
	}
    
	public String getAddemployeeid() {
		return addemployeeid;
	}
	public void setAddemployeeid(String addemployeeid) {
		this.addemployeeid = addemployeeid;
	}
    
	public String getAddemployeename() {
		return addemployeename;
	}
	public void setAddemployeename(String addemployeename) {
		this.addemployeename = addemployeename;
	}
    
	public String getUpdatedate() {
		return updatedate;
	}
	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}
    
	public String getUpdateemployeeid() {
		return updateemployeeid;
	}
	public void setUpdateemployeeid(String updateemployeeid) {
		this.updateemployeeid = updateemployeeid;
	}
    
	public String getUpdateemployeename() {
		return updateemployeename;
	}
	public void setUpdateemployeename(String updateemployeename) {
		this.updateemployeename = updateemployeename;
	}
    
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
    
}
