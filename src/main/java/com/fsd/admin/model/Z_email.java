package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

/**
 * 站内信-实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="z_email")
public class Z_email extends BaseModel {

	private static final long serialVersionUID = 1L;
	
	private String id;
	private String messagetitle; //消息标题
	private String messagetext; //消息内容
	private String savetime; //保存时间
	private String pushstatusid; //true - 已发送  ， false - 未发送
	private String pushstatus; //发送状态
	private String sendid; //发送人id
	private String sendname; //发送人name
	private String senddate; //发送时间
	private String recid; //接收人id
	private String recname; //接收人name
	private String readstatusid; // true - 已阅读， false - 未阅读
	private String readstatus; //阅读状态
	private String deleted; //删除标志（0 - 未删除，1 - 已删除）
	private String isstar; //是否星标邮件（true - 是， false - 否）
	
	@Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMessagetitle() {
		return messagetitle;
	}
	public void setMessagetitle(String messagetitle) {
		this.messagetitle = messagetitle;
	}
	public String getMessagetext() {
		return messagetext;
	}
	public void setMessagetext(String messagetext) {
		this.messagetext = messagetext;
	}
	public String getSavetime() {
		return savetime;
	}
	public void setSavetime(String savetime) {
		this.savetime = savetime;
	}
	public String getPushstatusid() {
		return pushstatusid;
	}
	public void setPushstatusid(String pushstatusid) {
		this.pushstatusid = pushstatusid;
	}
	public String getPushstatus() {
		return pushstatus;
	}
	public void setPushstatus(String pushstatus) {
		this.pushstatus = pushstatus;
	}
	public String getSendid() {
		return sendid;
	}
	public void setSendid(String sendid) {
		this.sendid = sendid;
	}
	public String getSendname() {
		return sendname;
	}
	public void setSendname(String sendname) {
		this.sendname = sendname;
	}
	public String getSenddate() {
		return senddate;
	}
	public void setSenddate(String senddate) {
		this.senddate = senddate;
	}
	public String getRecid() {
		return recid;
	}
	public void setRecid(String recid) {
		this.recid = recid;
	}
	public String getRecname() {
		return recname;
	}
	public void setRecname(String recname) {
		this.recname = recname;
	}
	public String getReadstatusid() {
		return readstatusid;
	}
	public void setReadstatusid(String readstatusid) {
		this.readstatusid = readstatusid;
	}
	public String getReadstatus() {
		return readstatus;
	}
	public void setReadstatus(String readstatus) {
		this.readstatus = readstatus;
	}
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
	public String getIsstar() {
		return isstar;
	}
	public void setIsstar(String isstar) {
		this.isstar = isstar;
	} 
}
