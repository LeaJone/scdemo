package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="b_articlerelate")
public class B_articlerelate extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String articleid;
	private String joinid;
	private String jointype;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getArticleid() {
		return articleid;
	}
	public void setArticleid(String articleid) {
		this.articleid = articleid;
	}
    
	public String getJoinid() {
		return joinid;
	}
	public void setJoinid(String joinid) {
		this.joinid = joinid;
	}
    
	public String getJointype() {
		return jointype;
	}
	public void setJointype(String jointype) {
		this.jointype = jointype;
	}
    
}
