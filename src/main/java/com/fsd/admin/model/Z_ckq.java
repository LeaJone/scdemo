package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="z_ckq")
public class Z_ckq extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String companyid;
	private String xkzh;
	private String sqr;
	private String ksmc;
	private String kczkz;
	private String sjgm;
	private String gmdw;
	private String kcfs;
	private String kqmj;
	private String yxqx;
	private String yxqq;
	private String yxqz;
	private String szxzqdm;
	private String szxzqmc;
	private String gdzb;
	private String remark;
	private String adddate;
	private String addemployeeid;
	private String addemployeename;
	private String updatedate;
	private String updateemployeeid;
	private String updateemployeename;
	private String deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getCompanyid() {
		return companyid;
	}
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
    
	public String getXkzh() {
		return xkzh;
	}
	public void setXkzh(String xkzh) {
		this.xkzh = xkzh;
	}
    
	public String getSqr() {
		return sqr;
	}
	public void setSqr(String sqr) {
		this.sqr = sqr;
	}
    
	public String getKsmc() {
		return ksmc;
	}
	public void setKsmc(String ksmc) {
		this.ksmc = ksmc;
	}
    
	public String getKczkz() {
		return kczkz;
	}
	public void setKczkz(String kczkz) {
		this.kczkz = kczkz;
	}
    
	public String getSjgm() {
		return sjgm;
	}
	public void setSjgm(String sjgm) {
		this.sjgm = sjgm;
	}
    
	public String getGmdw() {
		return gmdw;
	}
	public void setGmdw(String gmdw) {
		this.gmdw = gmdw;
	}
    
	public String getKcfs() {
		return kcfs;
	}
	public void setKcfs(String kcfs) {
		this.kcfs = kcfs;
	}
    
	public String getKqmj() {
		return kqmj;
	}
	public void setKqmj(String kqmj) {
		this.kqmj = kqmj;
	}
    
	public String getYxqx() {
		return yxqx;
	}
	public void setYxqx(String yxqx) {
		this.yxqx = yxqx;
	}
    
	public String getYxqq() {
		return yxqq;
	}
	public void setYxqq(String yxqq) {
		this.yxqq = yxqq;
	}
    
	public String getYxqz() {
		return yxqz;
	}
	public void setYxqz(String yxqz) {
		this.yxqz = yxqz;
	}
    
	public String getSzxzqdm() {
		return szxzqdm;
	}
	public void setSzxzqdm(String szxzqdm) {
		this.szxzqdm = szxzqdm;
	}
    
	public String getSzxzqmc() {
		return szxzqmc;
	}
	public void setSzxzqmc(String szxzqmc) {
		this.szxzqmc = szxzqmc;
	}
    
	public String getGdzb() {
		return gdzb;
	}
	public void setGdzb(String gdzb) {
		this.gdzb = gdzb;
	}
    
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
    
	public String getAdddate() {
		return adddate;
	}
	public void setAdddate(String adddate) {
		this.adddate = adddate;
	}
    
	public String getAddemployeeid() {
		return addemployeeid;
	}
	public void setAddemployeeid(String addemployeeid) {
		this.addemployeeid = addemployeeid;
	}
    
	public String getAddemployeename() {
		return addemployeename;
	}
	public void setAddemployeename(String addemployeename) {
		this.addemployeename = addemployeename;
	}
    
	public String getUpdatedate() {
		return updatedate;
	}
	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}
    
	public String getUpdateemployeeid() {
		return updateemployeeid;
	}
	public void setUpdateemployeeid(String updateemployeeid) {
		this.updateemployeeid = updateemployeeid;
	}
    
	public String getUpdateemployeename() {
		return updateemployeename;
	}
	public void setUpdateemployeename(String updateemployeename) {
		this.updateemployeename = updateemployeename;
	}
    
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
    
}
