package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="j_posts")
public class J_posts extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String boardid;
	private String boardtitle;
	private String title;
	private String content;
	private String firstly;
	private String pubtime;
	private String pubuserid;
	private String pubusername;
	private String isscreen;
	private String retime;
	private String reusername;
	private String iscomment;
	private String sort;
	private String deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getBoardid() {
		return boardid;
	}

	public void setBoardid(String boardid) {
		this.boardid = boardid;
	}

	public String getBoardtitle() {
		return boardtitle;
	}

	public void setBoardtitle(String boardtitle) {
		this.boardtitle = boardtitle;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getFirstly() {
		return firstly;
	}

	public void setFirstly(String firstly) {
		this.firstly = firstly;
	}

	public String getPubtime() {
		return pubtime;
	}

	public void setPubtime(String pubtime) {
		this.pubtime = pubtime;
	}

	public String getPubuserid() {
		return pubuserid;
	}

	public void setPubuserid(String pubuserid) {
		this.pubuserid = pubuserid;
	}

	public String getPubusername() {
		return pubusername;
	}

	public void setPubusername(String pubusername) {
		this.pubusername = pubusername;
	}

	public String getRetime() {
		return retime;
	}

	public void setRetime(String retime) {
		this.retime = retime;
	}

	public String getReusername() {
		return reusername;
	}

	public void setReusername(String reusername) {
		this.reusername = reusername;
	}

	public String getIscomment() {
		return iscomment;
	}

	public void setIscomment(String iscomment) {
		this.iscomment = iscomment;
	}

	public String getIsscreen() {
		return isscreen;
	}

	public void setIsscreen(String isscreen) {
		this.isscreen = isscreen;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getDeleted() {
		return deleted;
	}

	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
}
