package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="z_xkjnjsteacher")
public class Z_xkjnjsteacher extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String f_fid;
	private String f_teacherid;
	private String f_teachername;
	private String f_teachersex;
	private String f_teachertitle;
	private String f_teacherphone;
	private String f_leaderresponsibility;
	private String f_adddate;
	private String f_addemployeeid;
	private String f_addemployeename;
	private String f_lastupdatedate;
	private String f_updateemployeeid;
	private String f_updateemployeename;
	private String f_deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getF_fid() {
		return f_fid;
	}
	public void setF_fid(String f_fid) {
		this.f_fid = f_fid;
	}
    
	public String getF_teacherid() {
		return f_teacherid;
	}
	public void setF_teacherid(String f_teacherid) {
		this.f_teacherid = f_teacherid;
	}
    
	public String getF_teachername() {
		return f_teachername;
	}
	public void setF_teachername(String f_teachername) {
		this.f_teachername = f_teachername;
	}
    
	public String getF_teachersex() {
		return f_teachersex;
	}
	public void setF_teachersex(String f_teachersex) {
		this.f_teachersex = f_teachersex;
	}
    
	public String getF_teachertitle() {
		return f_teachertitle;
	}
	public void setF_teachertitle(String f_teachertitle) {
		this.f_teachertitle = f_teachertitle;
	}
    
	public String getF_teacherphone() {
		return f_teacherphone;
	}
	public void setF_teacherphone(String f_teacherphone) {
		this.f_teacherphone = f_teacherphone;
	}
    
	public String getF_leaderresponsibility() {
		return f_leaderresponsibility;
	}
	public void setF_leaderresponsibility(String f_leaderresponsibility) {
		this.f_leaderresponsibility = f_leaderresponsibility;
	}
    
	public String getF_adddate() {
		return f_adddate;
	}
	public void setF_adddate(String f_adddate) {
		this.f_adddate = f_adddate;
	}
    
	public String getF_addemployeeid() {
		return f_addemployeeid;
	}
	public void setF_addemployeeid(String f_addemployeeid) {
		this.f_addemployeeid = f_addemployeeid;
	}
    
	public String getF_addemployeename() {
		return f_addemployeename;
	}
	public void setF_addemployeename(String f_addemployeename) {
		this.f_addemployeename = f_addemployeename;
	}
    
	public String getF_lastupdatedate() {
		return f_lastupdatedate;
	}
	public void setF_lastupdatedate(String f_lastupdatedate) {
		this.f_lastupdatedate = f_lastupdatedate;
	}
    
	public String getF_updateemployeeid() {
		return f_updateemployeeid;
	}
	public void setF_updateemployeeid(String f_updateemployeeid) {
		this.f_updateemployeeid = f_updateemployeeid;
	}
    
	public String getF_updateemployeename() {
		return f_updateemployeename;
	}
	public void setF_updateemployeename(String f_updateemployeename) {
		this.f_updateemployeename = f_updateemployeename;
	}
    
	public String getF_deleted() {
		return f_deleted;
	}
	public void setF_deleted(String f_deleted) {
		this.f_deleted = f_deleted;
	}
    
}
