package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="Sys_SystemMenu")
public class Sys_SystemMenu extends BaseModel{
    private static final long serialVersionUID = 1L;
    private String id;
	private String code;
	private String companyid;
	private String parentcode;
	private String basecode;
	private String showname;
	private String tiptext;
	private String iconpath;
	private Long sort;
	private String formname;
	private String isshow;
	private String isdialog;
	private String isuse;
	private String ispopedom;
	private String isexpand;
    
	@Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCompanyid() {
		return companyid;
	}
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getParentcode() {
		return parentcode;
	}
	public void setParentcode(String parentcode) {
		this.parentcode = parentcode;
	}
	public String getBasecode() {
		return basecode;
	}
	public void setBasecode(String basecode) {
		this.basecode = basecode;
	}
	public String getShowname() {
		return showname;
	}
	public void setShowname(String showname) {
		this.showname = showname;
	}
	public String getTiptext() {
		return tiptext;
	}
	public void setTiptext(String tiptext) {
		this.tiptext = tiptext;
	}
	public String getIconpath() {
		return iconpath;
	}
	public void setIconpath(String iconpath) {
		this.iconpath = iconpath;
	}
	public Long getSort() {
		return sort;
	}
	public void setSort(Long sort) {
		this.sort = sort;
	}
	public String getFormname() {
		return formname;
	}
	public void setFormname(String formname) {
		this.formname = formname;
	}
	public String getIsshow() {
		return isshow;
	}
	public void setIsshow(String isshow) {
		this.isshow = isshow;
	}
	public String getIsdialog() {
		return isdialog;
	}
	public void setIsdialog(String isdialog) {
		this.isdialog = isdialog;
	}
	public String getIsuse() {
		return isuse;
	}
	public void setIsuse(String isuse) {
		this.isuse = isuse;
	}
	public String getIspopedom() {
		return ispopedom;
	}
	public void setIspopedom(String ispopedom) {
		this.ispopedom = ispopedom;
	}
	public String getIsexpand() {
		return isexpand;
	}
	public void setIsexpand(String isexpand) {
		this.isexpand = isexpand;
	}
}
