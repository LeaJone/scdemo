package com.fsd.admin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fsd.core.model.BaseModel;

@Entity
@Table(name="z_tutor")
public class Z_tutor extends BaseModel{
    private static final long serialVersionUID = 1L;
	private String id;
	private String f_baseid;
	private String f_typecode;
	private String f_typename;
	private String f_name;
	private Long f_age;
	private String f_sexid;
	private String f_sexname;
	private String f_educationid;
	private String f_educationname;
	private String f_degree;
	private String f_professionalTitle;
	private String f_specialityid;
	private String f_specialityname;
	private String f_helpspeciality;
	private String f_resume;
	private String f_reason;
	private String f_courses;
	private String f_direction;
	private String f_workunit;
	private String f_remark;
	private String f_practice;
	private String f_imgurl;
	private String f_isfriend;
	private Long f_sort;
	private String f_adddate;
	private String f_statusid;
	private String f_statusname;
	private String f_addemployeeid;
	private String f_addemployeename;
	private String f_lastupdatedate;
	private String f_updateemployeeid;
	private String f_updateemployeename;
	private String f_deleted;
    
    @Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
    
	public String getF_baseid() {
		return f_baseid;
	}
	public void setF_baseid(String f_baseid) {
		this.f_baseid = f_baseid;
	}

	public String getF_typecode() {
		return f_typecode;
	}
	public void setF_typecode(String f_typecode) {
		this.f_typecode = f_typecode;
	}

	public String getF_typename() {
		return f_typename;
	}
	public void setF_typename(String f_typename) {
		this.f_typename = f_typename;
	}

	public String getF_name() {
		return f_name;
	}
	public void setF_name(String f_name) {
		this.f_name = f_name;
	}
    
	public Long getF_age() {
		return f_age;
	}
	public void setF_age(Long f_age) {
		this.f_age = f_age;
	}
    
	public String getF_sexid() {
		return f_sexid;
	}
	public void setF_sexid(String f_sexid) {
		this.f_sexid = f_sexid;
	}
    
	public String getF_sexname() {
		return f_sexname;
	}
	public void setF_sexname(String f_sexname) {
		this.f_sexname = f_sexname;
	}
    
	public String getF_educationid() {
		return f_educationid;
	}
	public void setF_educationid(String f_educationid) {
		this.f_educationid = f_educationid;
	}
    
	public String getF_educationname() {
		return f_educationname;
	}
	public void setF_educationname(String f_educationname) {
		this.f_educationname = f_educationname;
	}

	public String getF_degree() {
		return f_degree;
	}
	public void setF_degree(String f_degree) {
		this.f_degree = f_degree;
	}

	public String getF_professionalTitle() {
		return f_professionalTitle;
	}
	public void setF_professionalTitle(String f_professionalTitle) {
		this.f_professionalTitle = f_professionalTitle;
	}

	public String getF_specialityid() {
		return f_specialityid;
	}
	public void setF_specialityid(String f_specialityid) {
		this.f_specialityid = f_specialityid;
	}
    
	public String getF_specialityname() {
		return f_specialityname;
	}
	public void setF_specialityname(String f_specialityname) {
		this.f_specialityname = f_specialityname;
	}
    
	public String getF_helpspeciality() {
		return f_helpspeciality;
	}
	public void setF_helpspeciality(String f_helpspeciality) {
		this.f_helpspeciality = f_helpspeciality;
	}
    
	public String getF_resume() {
		return f_resume;
	}
	public void setF_resume(String f_resume) {
		this.f_resume = f_resume;
	}
	
	public String getF_reason() {
		return f_reason;
	}
	public void setF_reason(String f_reason) {
		this.f_reason = f_reason;
	}
	public String getF_courses() {
		return f_courses;
	}
	public void setF_courses(String f_courses) {
		this.f_courses = f_courses;
	}

	public String getF_direction() {
		return f_direction;
	}
	public void setF_direction(String f_direction) {
		this.f_direction = f_direction;
	}

	public String getF_workunit() {
		return f_workunit;
	}
	public void setF_workunit(String f_workunit) {
		this.f_workunit = f_workunit;
	}

	public String getF_remark() {
		return f_remark;
	}
	public void setF_remark(String f_remark) {
		this.f_remark = f_remark;
	}

	public String getF_practice() {
		return f_practice;
	}
	public void setF_practice(String f_practice) {
		this.f_practice = f_practice;
	}

	public String getF_imgurl() {
		return f_imgurl;
	}
	public void setF_imgurl(String f_imgurl) {
		this.f_imgurl = f_imgurl;
	}

	public String getF_isfriend() {
		return f_isfriend;
	}
	public void setF_isfriend(String f_isfriend) {
		this.f_isfriend = f_isfriend;
	}

	public Long getF_sort() {
		return f_sort;
	}
	public void setF_sort(Long f_sort) {
		this.f_sort = f_sort;
	}

	public String getF_adddate() {
		return f_adddate;
	}
	public void setF_adddate(String f_adddate) {
		this.f_adddate = f_adddate;
	}
    
	public String getF_statusid() {
		return f_statusid;
	}
	public void setF_statusid(String f_statusid) {
		this.f_statusid = f_statusid;
	}
	public String getF_statusname() {
		return f_statusname;
	}
	public void setF_statusname(String f_statusname) {
		this.f_statusname = f_statusname;
	}
	public String getF_addemployeeid() {
		return f_addemployeeid;
	}
	public void setF_addemployeeid(String f_addemployeeid) {
		this.f_addemployeeid = f_addemployeeid;
	}
    
	public String getF_addemployeename() {
		return f_addemployeename;
	}
	public void setF_addemployeename(String f_addemployeename) {
		this.f_addemployeename = f_addemployeename;
	}
    
	public String getF_lastupdatedate() {
		return f_lastupdatedate;
	}
	public void setF_lastupdatedate(String f_lastupdatedate) {
		this.f_lastupdatedate = f_lastupdatedate;
	}
    
	public String getF_updateemployeeid() {
		return f_updateemployeeid;
	}
	public void setF_updateemployeeid(String f_updateemployeeid) {
		this.f_updateemployeeid = f_updateemployeeid;
	}
    
	public String getF_updateemployeename() {
		return f_updateemployeename;
	}
	public void setF_updateemployeename(String f_updateemployeename) {
		this.f_updateemployeename = f_updateemployeename;
	}
    
	public String getF_deleted() {
		return f_deleted;
	}
	public void setF_deleted(String f_deleted) {
		this.f_deleted = f_deleted;
	}
    
}
