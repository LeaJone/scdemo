package com.fsd.admin.entity;

import java.util.List;
import com.fsd.admin.model.E_Autoreplylist;

public class E_autoreplyEntity{
    private static final long serialVersionUID = 1L;
    
	private String id;
	private String messagetype;
	private String keyword;
	private String replytype;
	private String content;
	
	private String key;
	private List<E_Autoreplylist> autoreplylist;
    
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMessagetype() {
		return messagetype;
	}
	public void setMessagetype(String messagetype) {
		this.messagetype = messagetype;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getReplytype() {
		return replytype;
	}
	public void setReplytype(String replytype) {
		this.replytype = replytype;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public List<E_Autoreplylist> getAutoreplylist() {
		return autoreplylist;
	}
	public void setAutoreplylist(List<E_Autoreplylist> autoreplylist) {
		this.autoreplylist = autoreplylist;
	}
}
