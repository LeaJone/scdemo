package com.fsd.admin.entity.activiti;

import java.util.List;
import java.util.Map;

/**
 * Description: Activiti 任务实体
 * @author: lumin
 * @date: 2018-08-23 16:53
 **/
public class ActivitiTask {

    /**
     * 任务id
     */
    private String id;
    /**
     * 任务名称
     */
    private String name;
    /**
     * 任务办理人
     */
    private String assignee;
    /**
     * 任务实例id
     */
    private String processInstanceId;
    /**
     * 任务创建时间
     */
    private String createTime;
    /**
     * 流程状态
     */
    private String state;
    /**
     * 任务表单
     */
    private Map<String, Object> formMap;
    /**
     * 历史表单
     */
    private List<Map<String, Object>> finishedFormList;
    /**
     *  下一节点信息
     */
    private List<Map<String, Object>> nextNodeList;
    private String column1;
    private String column2;
    private String column3;
    private String column4;
    private String column5;
    private String column6;
    private String column7;
    private String column8;
    private String column9;
    private String column10;


    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getAssignee() {
        return assignee;
    }
    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public String getProcessInstanceId() {
        return processInstanceId;
    }
    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    public String getCreateTime() {
        return createTime;
    }
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }

    public Map<String, Object> getFormMap() {
        return formMap;
    }
    public void setFormMap(Map<String, Object> formMap) {
        this.formMap = formMap;
    }

    public List<Map<String, Object>> getFinishedFormList() {
        return finishedFormList;
    }
    public void setFinishedFormList(List<Map<String, Object>> finishedFormList) {
        this.finishedFormList = finishedFormList;
    }

    public List<Map<String, Object>> getNextNodeList() {
        return nextNodeList;
    }
    public void setNextNodeList(List<Map<String, Object>> nextNodeList) {
        this.nextNodeList = nextNodeList;
    }

    public String getColumn1() {
        return column1;
    }
    public void setColumn1(String column1) {
        this.column1 = column1;
    }

    public String getColumn2() {
        return column2;
    }
    public void setColumn2(String column2) {
        this.column2 = column2;
    }

    public String getColumn3() {
        return column3;
    }
    public void setColumn3(String column3) {
        this.column3 = column3;
    }

    public String getColumn4() {
        return column4;
    }
    public void setColumn4(String column4) {
        this.column4 = column4;
    }

    public String getColumn5() {
        return column5;
    }
    public void setColumn5(String column5) {
        this.column5 = column5;
    }

    public String getColumn6() {
        return column6;
    }
    public void setColumn6(String column6) {
        this.column6 = column6;
    }

    public String getColumn7() {
        return column7;
    }
    public void setColumn7(String column7) {
        this.column7 = column7;
    }

    public String getColumn8() {
        return column8;
    }
    public void setColumn8(String column8) {
        this.column8 = column8;
    }

    public String getColumn9() {
        return column9;
    }
    public void setColumn9(String column9) {
        this.column9 = column9;
    }

    public String getColumn10() {
        return column10;
    }
    public void setColumn10(String column10) {
        this.column10 = column10;
    }
}