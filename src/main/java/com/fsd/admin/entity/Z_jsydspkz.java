package com.fsd.admin.entity;

/**
 * 建设用地审批扩展
 */
public class Z_jsydspkz {
	
	private String wlyd;
	private String jsyd;
	private String yd;
	private String gd;
	private String xj;
	private String zzyd;
	private String sfyd;
	private String gkccyd;
	private String ggglfwyd;
	private String jtysyd;
	private String slss;
	private String nyyd;
	private String qt;
	
	public String getWlyd() {
		return wlyd;
	}
	public void setWlyd(String wlyd) {
		this.wlyd = wlyd;
	}
	public String getJsyd() {
		return jsyd;
	}
	public void setJsyd(String jsyd) {
		this.jsyd = jsyd;
	}
	public String getYd() {
		return yd;
	}
	public void setYd(String yd) {
		this.yd = yd;
	}
	public String getGd() {
		return gd;
	}
	public void setGd(String gd) {
		this.gd = gd;
	}
	public String getXj() {
		return xj;
	}
	public void setXj(String xj) {
		this.xj = xj;
	}
	public String getZzyd() {
		return zzyd;
	}
	public void setZzyd(String zzyd) {
		this.zzyd = zzyd;
	}
	public String getSfyd() {
		return sfyd;
	}
	public void setSfyd(String sfyd) {
		this.sfyd = sfyd;
	}
	public String getGkccyd() {
		return gkccyd;
	}
	public void setGkccyd(String gkccyd) {
		this.gkccyd = gkccyd;
	}
	public String getGgglfwyd() {
		return ggglfwyd;
	}
	public void setGgglfwyd(String ggglfwyd) {
		this.ggglfwyd = ggglfwyd;
	}
	public String getJtysyd() {
		return jtysyd;
	}
	public void setJtysyd(String jtysyd) {
		this.jtysyd = jtysyd;
	}
	public String getSlss() {
		return slss;
	}
	public void setSlss(String slss) {
		this.slss = slss;
	}
	public String getNyyd() {
		return nyyd;
	}
	public void setNyyd(String nyyd) {
		this.nyyd = nyyd;
	}
	public String getQt() {
		return qt;
	}
	public void setQt(String qt) {
		this.qt = qt;
	}
}
