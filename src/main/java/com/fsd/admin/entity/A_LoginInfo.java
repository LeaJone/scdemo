package com.fsd.admin.entity;

import java.util.List;
import java.util.Map;

import com.fsd.admin.model.A_Company;
import com.fsd.admin.model.A_Employee;

public class A_LoginInfo {
    private static final long serialVersionUID = 1L;
    
    private A_Employee employee;
    private A_Company company;
    private Map<String, List<String>> popedomMap;
	private boolean isadmin = false;

	public boolean isIsadmin() {
		return isadmin;
	}
    
	public A_Employee getEmployee() {
		return employee;
	}
	public void setEmployee(A_Employee employee) {
		this.employee = employee;
		this.isadmin = employee.isIsadmin();
	}
	
	public A_Company getCompany() {
		return company;
	}
	public void setCompany(A_Company company) {
		this.company = company;
	}
	
	public Map<String, List<String>> getPopedomMap() {
		return popedomMap;
	}
	public void setPopedomMap(Map<String, List<String>> popedomMap) {
		this.popedomMap = popedomMap;
	}

}
