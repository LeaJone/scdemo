package com.fsd.admin.dao;

import com.fsd.admin.model.E_Autoreply;
import com.fsd.core.dao.BaseDao;

public interface E_AutoreplyDao extends BaseDao<E_Autoreply, String> {

}
