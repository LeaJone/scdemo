package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.Z_projectgroup;

public interface Z_projectgroupDao extends BaseDao<Z_projectgroup, String>{
    
}

