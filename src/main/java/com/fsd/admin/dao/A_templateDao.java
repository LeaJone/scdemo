package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.A_template;

public interface A_templateDao extends BaseDao<A_template, String>{
    
}

