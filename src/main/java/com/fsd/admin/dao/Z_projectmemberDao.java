package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.Z_projectmember;

public interface Z_projectmemberDao extends BaseDao<Z_projectmember, String>{
    
}

