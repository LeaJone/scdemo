package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.B_wordtype;

public interface B_wordtypeDao extends BaseDao<B_wordtype, String>{
    
}

