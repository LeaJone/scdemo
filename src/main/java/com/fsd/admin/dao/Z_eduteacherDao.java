package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.Z_eduteacher;

public interface Z_eduteacherDao extends BaseDao<Z_eduteacher, String>{
    
}

