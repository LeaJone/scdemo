package com.fsd.admin.dao;

import com.fsd.admin.model.T_course;
import com.fsd.core.dao.BaseDao;

/**
 * 课程信息Dao接口
 * @author Administrator
 *
 */
public interface T_courseDao extends BaseDao<T_course, String>{
    
}

