package com.fsd.admin.dao;

import com.fsd.admin.model.Z_stagerecord;
import com.fsd.core.dao.BaseDao;

public interface Z_stagerecordDao extends BaseDao<Z_stagerecord, String>{
    
}

