package com.fsd.admin.dao;

import com.fsd.admin.model.E_Systemcode;
import com.fsd.core.dao.BaseDao;

public interface E_SystemcodeDao extends BaseDao<E_Systemcode, String> {

}
