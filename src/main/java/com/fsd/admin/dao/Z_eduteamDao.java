package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.Z_eduteam;

public interface Z_eduteamDao extends BaseDao<Z_eduteam, String>{
    
}

