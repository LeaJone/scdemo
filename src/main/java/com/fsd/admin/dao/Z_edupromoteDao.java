package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.Z_edupromote;

public interface Z_edupromoteDao extends BaseDao<Z_edupromote, String>{
    
}

