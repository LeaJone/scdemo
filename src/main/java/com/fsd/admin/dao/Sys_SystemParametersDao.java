package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.Sys_SystemParameters;

public interface Sys_SystemParametersDao extends BaseDao<Sys_SystemParameters, String>{
    
}

