package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.H_relation;

public interface H_relationDao extends BaseDao<H_relation, String>{
    
}

