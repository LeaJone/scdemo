package com.fsd.admin.dao;

import com.fsd.admin.model.J_reply;
import com.fsd.core.dao.BaseDao;

public interface J_replyDao extends BaseDao<J_reply, String>{
    
}

