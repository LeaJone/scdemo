package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.Sys_SystemDictionary;

public interface Sys_SystemDictionaryDao extends BaseDao<Sys_SystemDictionary, String>{
    
}

