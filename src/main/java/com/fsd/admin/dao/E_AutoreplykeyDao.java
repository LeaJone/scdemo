package com.fsd.admin.dao;

import com.fsd.admin.model.E_Autoreplykey;
import com.fsd.core.dao.BaseDao;

public interface E_AutoreplykeyDao extends BaseDao<E_Autoreplykey, String> {

}
