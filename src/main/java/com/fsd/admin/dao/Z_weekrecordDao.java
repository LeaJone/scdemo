package com.fsd.admin.dao;

import com.fsd.admin.model.Z_weekrecord;
import com.fsd.core.dao.BaseDao;

public interface Z_weekrecordDao extends BaseDao<Z_weekrecord, String>{
    
}

