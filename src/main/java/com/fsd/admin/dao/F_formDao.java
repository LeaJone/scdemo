package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.F_form;

public interface F_formDao extends BaseDao<F_form, String>{
    
}

