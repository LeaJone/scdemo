package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.F_flowobject;

public interface F_flowobjectDao extends BaseDao<F_flowobject, String>{
    
}

