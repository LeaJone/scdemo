package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.Z_projectsite;

public interface Z_projectsiteDao extends BaseDao<Z_projectsite, String>{
    
}

