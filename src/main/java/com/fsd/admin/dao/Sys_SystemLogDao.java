package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.Sys_SystemLog;

public interface Sys_SystemLogDao extends BaseDao<Sys_SystemLog, String>{
    
}

