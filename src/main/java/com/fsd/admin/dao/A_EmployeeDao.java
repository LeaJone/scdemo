package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.A_Employee;

public interface A_EmployeeDao extends BaseDao<A_Employee, String>{
    
}

