package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.A_Company;

public interface A_CompanyDao extends BaseDao<A_Company, String>{
    
}

