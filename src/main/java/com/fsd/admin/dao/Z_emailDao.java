package com.fsd.admin.dao;

import com.fsd.admin.model.Z_email;
import com.fsd.core.dao.BaseDao;

/**
 * 站内信 - dao
 * @author Administrator
 *
 */
public interface Z_emailDao extends BaseDao<Z_email, String> {

}
