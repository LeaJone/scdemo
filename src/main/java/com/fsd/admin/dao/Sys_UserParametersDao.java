package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.Sys_UserParameters;

public interface Sys_UserParametersDao extends BaseDao<Sys_UserParameters, String>{
    
}

