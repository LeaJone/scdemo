package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.Sys_SystemMenu;

public interface Sys_SystemMenuDao extends BaseDao<Sys_SystemMenu, String>{
    
}

