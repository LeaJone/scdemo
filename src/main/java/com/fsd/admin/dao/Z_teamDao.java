package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.Z_team;

public interface Z_teamDao extends BaseDao<Z_team, String>{
    
}

