package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.Sys_PopedomGroup;

public interface Sys_PopedomGroupDao extends BaseDao<Sys_PopedomGroup, String>{
    
}

