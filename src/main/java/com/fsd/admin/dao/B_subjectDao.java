package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.B_subject;

public interface B_subjectDao extends BaseDao<B_subject, String>{
    
}

