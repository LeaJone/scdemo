package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.F_flowrecord;

public interface F_flowrecordDao extends BaseDao<F_flowrecord, String>{
    
}

