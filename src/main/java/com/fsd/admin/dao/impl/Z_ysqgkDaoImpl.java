package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_ysqgkDao;
import com.fsd.admin.model.Z_ysqgk;

@Repository("z_ysqgkDaoImpl")
public class Z_ysqgkDaoImpl extends BaseDaoImpl<Z_ysqgk, String> implements Z_ysqgkDao {
    
}

