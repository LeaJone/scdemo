package com.fsd.admin.dao.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Sys_SystemParametersDao;
import com.fsd.admin.model.Sys_SystemParameters;

@Repository("Sys_SystemParametersDaoImpl")
public class Sys_SystemParametersDaoImpl extends BaseDaoImpl<Sys_SystemParameters, String> implements Sys_SystemParametersDao {

	private static Log log = LogFactory.getLog(Sys_SystemParametersDaoImpl.class);

}

