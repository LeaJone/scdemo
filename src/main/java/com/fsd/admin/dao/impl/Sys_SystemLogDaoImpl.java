package com.fsd.admin.dao.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Sys_SystemLogDao;
import com.fsd.admin.model.Sys_SystemLog;

@Repository("Sys_SystemLogDaoImpl")
public class Sys_SystemLogDaoImpl extends BaseDaoImpl<Sys_SystemLog, String> implements Sys_SystemLogDao {

	private static Log log = LogFactory.getLog(Sys_SystemLogDaoImpl.class);

}

