package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_projectmaterialDao;
import com.fsd.admin.model.Z_projectmaterial;

@Repository("z_projectmaterialDaoImpl")
public class Z_projectmaterialDaoImpl extends BaseDaoImpl<Z_projectmaterial, String> implements Z_projectmaterialDao {
    
}

