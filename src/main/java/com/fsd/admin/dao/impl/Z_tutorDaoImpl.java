package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_tutorDao;
import com.fsd.admin.model.Z_tutor;

@Repository("z_tutorDaoImpl")
public class Z_tutorDaoImpl extends BaseDaoImpl<Z_tutor, String> implements Z_tutorDao {
    
}

