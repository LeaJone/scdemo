package com.fsd.admin.dao.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Sys_PopedomAllocateDao;
import com.fsd.admin.model.Sys_PopedomAllocate;

@Repository("Sys_PopedomAllocateDaoImpl")
public class Sys_PopedomAllocateDaoImpl extends BaseDaoImpl<Sys_PopedomAllocate, String> implements Sys_PopedomAllocateDao {

	private static Log log = LogFactory.getLog(Sys_PopedomAllocateDaoImpl.class);

}

