package com.fsd.admin.dao.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Sys_SystemDictionaryDao;
import com.fsd.admin.model.Sys_SystemDictionary;

@Repository("Sys_SystemDictionaryDaoImpl")
public class Sys_SystemDictionaryDaoImpl extends BaseDaoImpl<Sys_SystemDictionary, String> implements Sys_SystemDictionaryDao {

	private static Log log = LogFactory.getLog(Sys_SystemDictionaryDaoImpl.class);

}

