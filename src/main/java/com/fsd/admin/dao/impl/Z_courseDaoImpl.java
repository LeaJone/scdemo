package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_courseDao;
import com.fsd.admin.model.Z_course;

@Repository("z_courseDaoImpl")
public class Z_courseDaoImpl extends BaseDaoImpl<Z_course, String> implements Z_courseDao {
    
}

