package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_projectendingDao;
import com.fsd.admin.model.Z_projectending;

@Repository("z_projectendingDaoImpl")
public class Z_projectendingDaoImpl extends BaseDaoImpl<Z_projectending, String> implements Z_projectendingDao {
    
}

