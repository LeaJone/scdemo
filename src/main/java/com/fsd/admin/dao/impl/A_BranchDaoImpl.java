package com.fsd.admin.dao.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.A_BranchDao;
import com.fsd.admin.model.A_Branch;

@Repository("A_BranchDaoImpl")
public class A_BranchDaoImpl extends BaseDaoImpl<A_Branch, String> implements A_BranchDao {

	private static Log log = LogFactory.getLog(A_BranchDaoImpl.class);

}

