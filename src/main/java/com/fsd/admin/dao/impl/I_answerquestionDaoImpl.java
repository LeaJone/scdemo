package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.I_answerquestionDao;
import com.fsd.admin.model.I_answerquestion;

@Repository("i_answerquestionDaoImpl")
public class I_answerquestionDaoImpl extends BaseDaoImpl<I_answerquestion, String> implements I_answerquestionDao {
    
}

