package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_eduteamDao;
import com.fsd.admin.model.Z_eduteam;

@Repository("z_eduteamDaoImpl")
public class Z_eduteamDaoImpl extends BaseDaoImpl<Z_eduteam, String> implements Z_eduteamDao {
    
}

