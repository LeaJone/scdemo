package com.fsd.admin.dao.impl;

import com.fsd.admin.dao.z_domainDao;
import com.fsd.admin.model.z_domain;
import com.fsd.core.dao.impl.BaseDaoImpl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

@Repository("z_domainDaoImpl")
public class z_domainDaoImpl extends BaseDaoImpl<z_domain, String> implements z_domainDao {

	private static Log log = LogFactory.getLog(z_domainDaoImpl.class);

}

