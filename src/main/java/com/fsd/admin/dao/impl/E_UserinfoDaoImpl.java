package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.admin.dao.E_UserinfoDao;
import com.fsd.admin.model.E_userinfo;
import com.fsd.core.dao.impl.BaseDaoImpl;

@Repository("e_userinfoDaoImpl")
public class E_UserinfoDaoImpl extends BaseDaoImpl<E_userinfo, String> implements E_UserinfoDao {
	
}
