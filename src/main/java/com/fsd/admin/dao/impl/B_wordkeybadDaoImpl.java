package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.B_wordkeybadDao;
import com.fsd.admin.model.B_wordkeybad;

@Repository("b_wordkeybadDaoImpl")
public class B_wordkeybadDaoImpl extends BaseDaoImpl<B_wordkeybad, String> implements B_wordkeybadDao {
    
}

