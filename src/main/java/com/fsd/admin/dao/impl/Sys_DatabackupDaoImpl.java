package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.admin.dao.Sys_DatabackupDao;
import com.fsd.admin.model.Sys_Databackup;
import com.fsd.core.dao.impl.BaseDaoImpl;

/**
 * 数据备份Dao-实现类
 * @author Administrator
 *
 */
@Repository("sys_databackupDaoImpl")
public class Sys_DatabackupDaoImpl extends BaseDaoImpl<Sys_Databackup, String> implements Sys_DatabackupDao {

}
