package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.A_templateDao;
import com.fsd.admin.model.A_template;

@Repository("a_templateDaoImpl")
public class A_templateDaoImpl extends BaseDaoImpl<A_template, String> implements A_templateDao {
    
}

