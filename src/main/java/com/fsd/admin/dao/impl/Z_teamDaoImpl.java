package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_teamDao;
import com.fsd.admin.model.Z_team;

@Repository("z_teamDaoImpl")
public class Z_teamDaoImpl extends BaseDaoImpl<Z_team, String> implements Z_teamDao {
    
}

