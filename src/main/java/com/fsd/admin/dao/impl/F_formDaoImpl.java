package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.F_formDao;
import com.fsd.admin.model.F_form;

@Repository("f_formDaoImpl")
public class F_formDaoImpl extends BaseDaoImpl<F_form, String> implements F_formDao {
    
}

