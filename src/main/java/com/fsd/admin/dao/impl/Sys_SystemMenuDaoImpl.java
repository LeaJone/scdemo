package com.fsd.admin.dao.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Sys_SystemMenuDao;
import com.fsd.admin.model.Sys_SystemMenu;

@Repository("Sys_SystemMenuDaoImpl")
public class Sys_SystemMenuDaoImpl extends BaseDaoImpl<Sys_SystemMenu, String> implements Sys_SystemMenuDao {

	private static Log log = LogFactory.getLog(Sys_SystemMenuDaoImpl.class);

}

