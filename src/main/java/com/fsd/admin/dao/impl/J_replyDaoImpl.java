package com.fsd.admin.dao.impl;

import com.fsd.admin.dao.J_replyDao;
import com.fsd.admin.model.J_reply;
import com.fsd.core.dao.impl.BaseDaoImpl;
import org.springframework.stereotype.Repository;

@Repository("j_replyDaoImpl")
public class J_replyDaoImpl extends BaseDaoImpl<J_reply, String> implements J_replyDao {
    
}

