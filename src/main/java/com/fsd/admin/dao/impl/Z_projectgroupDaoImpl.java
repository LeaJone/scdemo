package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_projectgroupDao;
import com.fsd.admin.model.Z_projectgroup;

@Repository("z_projectgroupDaoImpl")
public class Z_projectgroupDaoImpl extends BaseDaoImpl<Z_projectgroup, String> implements Z_projectgroupDao {
    
}

