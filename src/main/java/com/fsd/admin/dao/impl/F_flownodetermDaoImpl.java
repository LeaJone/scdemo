package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.F_flownodetermDao;
import com.fsd.admin.model.F_flownodeterm;

@Repository("f_flownodetermDaoImpl")
public class F_flownodetermDaoImpl extends BaseDaoImpl<F_flownodeterm, String> implements F_flownodetermDao {
    
}

