package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_qdnrbDao;
import com.fsd.admin.model.Z_qdnrb;

@Repository("z_qdnrbDaoImpl")
public class Z_qdnrbDaoImpl extends BaseDaoImpl<Z_qdnrb, String> implements Z_qdnrbDao {
    
}

