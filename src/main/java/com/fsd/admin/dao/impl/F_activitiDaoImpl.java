package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.F_activitiDao;
import com.fsd.admin.model.F_activiti;

@Repository("f_activitiDaoImpl")
public class F_activitiDaoImpl extends BaseDaoImpl<F_activiti, String> implements F_activitiDao {
    
}

