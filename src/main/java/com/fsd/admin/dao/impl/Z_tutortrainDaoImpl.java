package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_tutortrainDao;
import com.fsd.admin.model.Z_tutortrain;

@Repository("z_tutortrainDaoImpl")
public class Z_tutortrainDaoImpl extends BaseDaoImpl<Z_tutortrain, String> implements Z_tutortrainDao {
    
}

