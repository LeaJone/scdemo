package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.I_bankinfoDao;
import com.fsd.admin.model.I_bankinfo;

@Repository("i_bankinfoDaoImpl")
public class I_bankinfoDaoImpl extends BaseDaoImpl<I_bankinfo, String> implements I_bankinfoDao {
    
}

