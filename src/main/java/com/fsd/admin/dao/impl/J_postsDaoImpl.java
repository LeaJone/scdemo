package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.J_postsDao;
import com.fsd.admin.model.J_posts;

@Repository("j_postsDaoImpl")
public class J_postsDaoImpl extends BaseDaoImpl<J_posts, String> implements J_postsDao {
    
}

