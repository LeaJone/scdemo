package com.fsd.admin.dao.impl;

import com.fsd.admin.dao.N_pushpublishDao;
import com.fsd.admin.model.N_pushpublish;
import com.fsd.core.dao.impl.BaseDaoImpl;
import org.springframework.stereotype.Repository;

@Repository("n_pushpublishDaoImpl")
public class N_pushpublishDaoImpl extends BaseDaoImpl<N_pushpublish, String> implements N_pushpublishDao {
    
}

