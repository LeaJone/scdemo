package com.fsd.admin.dao.impl;

import com.fsd.admin.dao.Z_weekrecordDao;
import com.fsd.admin.model.Z_weekrecord;
import com.fsd.core.dao.impl.BaseDaoImpl;
import org.springframework.stereotype.Repository;

@Repository("z_weekrecordDaoImpl")
public class Z_weekrecordDaoImpl extends BaseDaoImpl<Z_weekrecord, String> implements Z_weekrecordDao {
    
}

