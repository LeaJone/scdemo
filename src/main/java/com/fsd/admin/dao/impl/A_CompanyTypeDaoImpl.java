package com.fsd.admin.dao.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.A_CompanyTypeDao;
import com.fsd.admin.model.A_CompanyType;

@Repository("A_CompanyTypeDaoImpl")
public class A_CompanyTypeDaoImpl extends BaseDaoImpl<A_CompanyType, String> implements A_CompanyTypeDao {

	private static Log log = LogFactory.getLog(A_CompanyTypeDaoImpl.class);

}

