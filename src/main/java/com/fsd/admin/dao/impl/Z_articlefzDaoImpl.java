package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_articlefzDao;
import com.fsd.admin.model.Z_articlefz;

@Repository("z_articlefzDaoImpl")
public class Z_articlefzDaoImpl extends BaseDaoImpl<Z_articlefz, String> implements Z_articlefzDao {
    
}

