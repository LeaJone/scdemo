package com.fsd.admin.dao.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.F_middlecheckformDao;
import com.fsd.admin.model.F_middlecheckform;

@Repository("f_middlecheckformDaoImpl")
public class F_middlecheckformDaoImpl extends BaseDaoImpl<F_middlecheckform, String> implements F_middlecheckformDao {

	private static Log log = LogFactory.getLog(F_middlecheckformDaoImpl.class);

}

