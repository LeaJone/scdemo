package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_qdflbDao;
import com.fsd.admin.model.Z_qdflb;

@Repository("z_qdflbDaoImpl")
public class Z_qdflbDaoImpl extends BaseDaoImpl<Z_qdflb, String> implements Z_qdflbDao {
    
}

