package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_eduprojectDao;
import com.fsd.admin.model.Z_eduproject;

@Repository("z_eduprojectDaoImpl")
public class Z_eduprojectDaoImpl extends BaseDaoImpl<Z_eduproject, String> implements Z_eduprojectDao {
    
}

