package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.F_flowtypeDao;
import com.fsd.admin.model.F_flowtype;

@Repository("f_flowtypeDaoImpl")
public class F_flowtypeDaoImpl extends BaseDaoImpl<F_flowtype, String> implements F_flowtypeDao {
    
}

