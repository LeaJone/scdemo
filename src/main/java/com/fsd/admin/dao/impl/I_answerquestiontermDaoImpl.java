package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.I_answerquestiontermDao;
import com.fsd.admin.model.I_answerquestionterm;

@Repository("i_answerquestiontermDaoImpl")
public class I_answerquestiontermDaoImpl extends BaseDaoImpl<I_answerquestionterm, String> implements I_answerquestiontermDao {
    
}

