package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.B_parametertypeDao;
import com.fsd.admin.model.B_parametertype;

@Repository("b_parametertypeDaoImpl")
public class B_parametertypeDaoImpl extends BaseDaoImpl<B_parametertype, String> implements B_parametertypeDao {
    
}

