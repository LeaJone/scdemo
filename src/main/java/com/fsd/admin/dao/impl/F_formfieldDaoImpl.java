package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.F_formfieldDao;
import com.fsd.admin.model.F_formfield;

@Repository("f_formfieldDaoImpl")
public class F_formfieldDaoImpl extends BaseDaoImpl<F_formfield, String> implements F_formfieldDao {
    
}

