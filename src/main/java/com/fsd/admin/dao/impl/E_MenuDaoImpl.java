package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.admin.dao.E_MenuDao;
import com.fsd.admin.model.E_Menu;
import com.fsd.core.dao.impl.BaseDaoImpl;

/**
 * 微信菜单管理Dao-实现类
 * @author Administrator
 *
 */

@Repository("e_menuDaoImpl")
public class E_MenuDaoImpl extends BaseDaoImpl<E_Menu, String> implements E_MenuDao {

}
