package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_projectmemberDao;
import com.fsd.admin.model.Z_projectmember;

@Repository("z_projectmemberDaoImpl")
public class Z_projectmemberDaoImpl extends BaseDaoImpl<Z_projectmember, String> implements Z_projectmemberDao {
    
}

