package com.fsd.admin.dao.impl;

import com.fsd.admin.dao.Z_stagerecordDao;
import com.fsd.admin.model.Z_stagerecord;
import com.fsd.core.dao.impl.BaseDaoImpl;
import org.springframework.stereotype.Repository;

@Repository("z_stagerecordDaoImpl")
public class Z_stagerecordDaoImpl extends BaseDaoImpl<Z_stagerecord, String> implements Z_stagerecordDao {
    
}

