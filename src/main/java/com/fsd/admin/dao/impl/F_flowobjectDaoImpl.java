package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.F_flowobjectDao;
import com.fsd.admin.model.F_flowobject;

@Repository("f_flowobjectDaoImpl")
public class F_flowobjectDaoImpl extends BaseDaoImpl<F_flowobject, String> implements F_flowobjectDao {
    
}

