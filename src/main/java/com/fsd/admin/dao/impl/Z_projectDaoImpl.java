package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_projectDao;
import com.fsd.admin.model.Z_project;

@Repository("z_projectDaoImpl")
public class Z_projectDaoImpl extends BaseDaoImpl<Z_project, String> implements Z_projectDao {
    
}

