package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.A_notifyDao;
import com.fsd.admin.model.A_notify;

@Repository("A_notifyDaoImpl")
public class A_notifyDaoImpl extends BaseDaoImpl<A_notify, String> implements A_notifyDao {
    
}

