package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.I_testinfoDao;
import com.fsd.admin.model.I_testinfo;

@Repository("i_testinfoDaoImpl")
public class I_testinfoDaoImpl extends BaseDaoImpl<I_testinfo, String> implements I_testinfoDao {
    
}

