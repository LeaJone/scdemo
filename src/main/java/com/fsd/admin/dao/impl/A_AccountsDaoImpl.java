package com.fsd.admin.dao.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.A_AccountsDao;
import com.fsd.admin.model.A_Accounts;

@Repository("A_AccountsDaoImpl")
public class A_AccountsDaoImpl extends BaseDaoImpl<A_Accounts, String> implements A_AccountsDao {

	private static Log log = LogFactory.getLog(A_AccountsDaoImpl.class);

}

