package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_projectbillDao;
import com.fsd.admin.model.Z_projectbill;

@Repository("z_projectbillDaoImpl")
public class Z_projectbillDaoImpl extends BaseDaoImpl<Z_projectbill, String> implements Z_projectbillDao {
    
}

