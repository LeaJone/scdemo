package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_tkqDao;
import com.fsd.admin.model.Z_tkq;

@Repository("z_tkqDaoImpl")
public class Z_tkqDaoImpl extends BaseDaoImpl<Z_tkq, String> implements Z_tkqDao {
    
}

