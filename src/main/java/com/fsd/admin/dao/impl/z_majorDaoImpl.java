package com.fsd.admin.dao.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.z_majorDao;
import com.fsd.admin.model.z_major;

@Repository("z_majorDaoImpl")
public class z_majorDaoImpl extends BaseDaoImpl<z_major, String> implements z_majorDao {

	private static Log log = LogFactory.getLog(z_majorDaoImpl.class);

}

