package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.admin.dao.E_WechatDao;
import com.fsd.admin.model.E_wechat;
import com.fsd.core.dao.impl.BaseDaoImpl;

/**
 * 微信账号管理Dao-实现类
 * @author Administrator
 *
 */

@Repository("e_wechatDaoImpl")
public class E_WechatDaoImpl extends BaseDaoImpl<E_wechat, String> implements E_WechatDao {

}
