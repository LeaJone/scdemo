package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.admin.dao.E_AutoreplykeyDao;
import com.fsd.admin.model.E_Autoreplykey;
import com.fsd.core.dao.impl.BaseDaoImpl;

@Repository("e_autoreplykeyDaoImpl")
public class E_AutoreplykeyDaoImpl extends BaseDaoImpl<E_Autoreplykey, String> implements E_AutoreplykeyDao {

}
