package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_competitiondeclareDao;
import com.fsd.admin.model.Z_competitiondeclare;

@Repository("z_competitiondeclareDaoImpl")
public class Z_competitiondeclareDaoImpl extends BaseDaoImpl<Z_competitiondeclare, String> implements Z_competitiondeclareDao {
    
}

