package com.fsd.admin.dao.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.H_qualifiedDao;
import com.fsd.admin.model.H_qualified;

@Repository("h_qualifiedDaoImpl")
public class H_qualifiedDaoImpl extends BaseDaoImpl<H_qualified, String> implements H_qualifiedDao {

	private static Log log = LogFactory.getLog(H_qualifiedDaoImpl.class);

}

