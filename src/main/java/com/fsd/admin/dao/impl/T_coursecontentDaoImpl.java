package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.admin.dao.T_coursecontentDao;
import com.fsd.admin.model.T_coursecontent;
import com.fsd.core.dao.impl.BaseDaoImpl;

/**
 * 课时信息Dao实现类
 * @author Administrator
 *
 */
@Repository("t_coursecontentDaoImpl")
public class T_coursecontentDaoImpl extends BaseDaoImpl<T_coursecontent, String> implements T_coursecontentDao {
    
}

