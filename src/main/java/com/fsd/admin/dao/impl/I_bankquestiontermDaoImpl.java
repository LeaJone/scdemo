package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.I_bankquestiontermDao;
import com.fsd.admin.model.I_bankquestionterm;

@Repository("i_bankquestiontermDaoImpl")
public class I_bankquestiontermDaoImpl extends BaseDaoImpl<I_bankquestionterm, String> implements I_bankquestiontermDao {
    
}

