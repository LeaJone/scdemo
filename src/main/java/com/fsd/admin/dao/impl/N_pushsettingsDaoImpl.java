package com.fsd.admin.dao.impl;

import com.fsd.admin.dao.N_pushsettingsDao;
import com.fsd.admin.model.N_pushsettings;
import com.fsd.core.dao.impl.BaseDaoImpl;
import org.springframework.stereotype.Repository;

@Repository("n_pushsettingsDaoImpl")
public class N_pushsettingsDaoImpl extends BaseDaoImpl<N_pushsettings, String> implements N_pushsettingsDao {
    
}

