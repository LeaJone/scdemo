package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;

import com.fsd.admin.dao.I_knowledgerelateDao;
import com.fsd.admin.model.I_knowledgerelate;
import com.fsd.core.dao.impl.BaseDaoImpl;

@Repository("i_knowledgerelateDaoImpl")
public class I_knowledgerelateDaoImpl extends BaseDaoImpl<I_knowledgerelate, String> implements I_knowledgerelateDao{

}
