package com.fsd.admin.dao.impl;

import com.fsd.admin.dao.Z_qycgDao;
import com.fsd.admin.model.Z_qycg;
import com.fsd.core.dao.impl.BaseDaoImpl;
import org.springframework.stereotype.Repository;

@Repository("z_qycgDaoImpl")
public class Z_qycgDaoImpl extends BaseDaoImpl<Z_qycg, String> implements Z_qycgDao {
    
}

