package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.I_testquestiontermDao;
import com.fsd.admin.model.I_testquestionterm;

@Repository("i_testquestiontermDaoImpl")
public class I_testquestiontermDaoImpl extends BaseDaoImpl<I_testquestionterm, String> implements I_testquestiontermDao {
    
}

