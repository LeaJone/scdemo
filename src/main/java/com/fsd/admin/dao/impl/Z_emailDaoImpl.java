package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;

import com.fsd.admin.dao.Z_emailDao;
import com.fsd.admin.model.Z_email;
import com.fsd.core.dao.impl.BaseDaoImpl;

/**
 * 站内信 - Dao实现类
 * @author Administrator
 *
 */
@Repository("z_emailDaoImpl")
public class Z_emailDaoImpl extends BaseDaoImpl<Z_email, String> implements Z_emailDao {

}
