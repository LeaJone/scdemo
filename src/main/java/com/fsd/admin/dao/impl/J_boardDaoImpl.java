package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.J_boardDao;
import com.fsd.admin.model.J_board;

@Repository("j_boardDaoImpl")
public class J_boardDaoImpl extends BaseDaoImpl<J_board, String> implements J_boardDao {
    
}

