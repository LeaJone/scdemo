package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;

import com.fsd.admin.dao.B_templatelistDao;
import com.fsd.admin.model.B_templatelist;
import com.fsd.core.dao.impl.BaseDaoImpl;

@Repository("b_templatelistDaoImpl")
public class B_templatelistDaoImpl extends BaseDaoImpl<B_templatelist, String> implements B_templatelistDao{

}
