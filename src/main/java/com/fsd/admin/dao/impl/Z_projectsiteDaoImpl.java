package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_projectsiteDao;
import com.fsd.admin.model.Z_projectsite;

@Repository("z_projectsiteDaoImpl")
public class Z_projectsiteDaoImpl extends BaseDaoImpl<Z_projectsite, String> implements Z_projectsiteDao {
    
}

