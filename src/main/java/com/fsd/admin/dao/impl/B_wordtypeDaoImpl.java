package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.B_wordtypeDao;
import com.fsd.admin.model.B_wordtype;

@Repository("b_wordtypeDaoImpl")
public class B_wordtypeDaoImpl extends BaseDaoImpl<B_wordtype, String> implements B_wordtypeDao {
    
}

