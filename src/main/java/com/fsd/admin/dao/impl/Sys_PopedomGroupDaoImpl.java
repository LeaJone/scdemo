package com.fsd.admin.dao.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Sys_PopedomGroupDao;
import com.fsd.admin.model.Sys_PopedomGroup;

@Repository("Sys_PopedomGroupDaoImpl")
public class Sys_PopedomGroupDaoImpl extends BaseDaoImpl<Sys_PopedomGroup, String> implements Sys_PopedomGroupDao {

	private static Log log = LogFactory.getLog(Sys_PopedomGroupDaoImpl.class);

}

