package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.B_downloadDao;
import com.fsd.admin.model.B_download;

@Repository("b_downloadDaoImpl")
public class B_downloadDaoImpl extends BaseDaoImpl<B_download, String> implements B_downloadDao {
    
}

