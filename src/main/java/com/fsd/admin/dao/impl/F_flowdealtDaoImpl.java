package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.F_flowdealtDao;
import com.fsd.admin.model.F_flowdealt;

@Repository("f_flowdealtDaoImpl")
public class F_flowdealtDaoImpl extends BaseDaoImpl<F_flowdealt, String> implements F_flowdealtDao {
    
}

