package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.B_articlerelateDao;
import com.fsd.admin.model.B_articlerelate;

@Repository("b_articlerelateDaoImpl")
public class B_articlerelateDaoImpl extends BaseDaoImpl<B_articlerelate, String> implements B_articlerelateDao {
    
}

