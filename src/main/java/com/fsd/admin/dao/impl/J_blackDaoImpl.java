package com.fsd.admin.dao.impl;

import com.fsd.admin.dao.J_blackDao;
import com.fsd.admin.model.J_black;
import com.fsd.core.dao.impl.BaseDaoImpl;
import org.springframework.stereotype.Repository;

@Repository("j_blackDaoImpl")
public class J_blackDaoImpl extends BaseDaoImpl<J_black, String> implements J_blackDao {
    
}

