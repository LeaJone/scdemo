package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;

import com.fsd.admin.dao.B_templatetypeDao;
import com.fsd.admin.model.B_templatetype;
import com.fsd.core.dao.impl.BaseDaoImpl;

@Repository("b_templatetypeDaoImpl")
public class B_templatetypeDaoImpl extends BaseDaoImpl<B_templatetype, String> implements B_templatetypeDao {

}
