package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.B_leavewordDao;
import com.fsd.admin.model.B_leaveword;

@Repository("b_leavewordDaoImpl")
public class B_leavewordDaoImpl extends BaseDaoImpl<B_leaveword, String> implements B_leavewordDao {
    
}

