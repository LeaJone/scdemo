package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.I_bankquestiontypeDao;
import com.fsd.admin.model.I_bankquestiontype;

@Repository("i_bankquestiontypeDaoImpl")
public class I_bankquestiontypeDaoImpl extends BaseDaoImpl<I_bankquestiontype, String> implements I_bankquestiontypeDao {
    
}

