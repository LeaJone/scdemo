package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.B_praiseDao;
import com.fsd.admin.model.B_praise;

@Repository("b_praiseDaoImpl")
public class B_praiseDaoImpl extends BaseDaoImpl<B_praise, String> implements B_praiseDao {
    
}

