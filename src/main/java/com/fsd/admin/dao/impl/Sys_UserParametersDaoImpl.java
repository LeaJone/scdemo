package com.fsd.admin.dao.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Sys_UserParametersDao;
import com.fsd.admin.model.Sys_UserParameters;

@Repository("Sys_UserParametersDaoImpl")
public class Sys_UserParametersDaoImpl extends BaseDaoImpl<Sys_UserParameters, String> implements Sys_UserParametersDao {

	private static Log log = LogFactory.getLog(Sys_UserParametersDaoImpl.class);

}

