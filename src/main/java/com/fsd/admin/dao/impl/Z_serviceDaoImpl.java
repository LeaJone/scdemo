package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_serviceDao;
import com.fsd.admin.model.Z_service;

@Repository("z_serviceDaoImpl")
public class Z_serviceDaoImpl extends BaseDaoImpl<Z_service, String> implements Z_serviceDao {
    
}

