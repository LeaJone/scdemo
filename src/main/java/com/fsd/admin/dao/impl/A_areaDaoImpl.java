package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.A_areaDao;
import com.fsd.admin.model.A_area;

@Repository("a_areaDaoImpl")
public class A_areaDaoImpl extends BaseDaoImpl<A_area, String> implements A_areaDao {
    
}

