package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.I_testquestionDao;
import com.fsd.admin.model.I_testquestion;

@Repository("i_testquestionDaoImpl")
public class I_testquestionDaoImpl extends BaseDaoImpl<I_testquestion, String> implements I_testquestionDao {
    
}

