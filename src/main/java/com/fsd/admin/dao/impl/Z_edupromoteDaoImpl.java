package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_edupromoteDao;
import com.fsd.admin.model.Z_edupromote;

@Repository("z_edupromoteDaoImpl")
public class Z_edupromoteDaoImpl extends BaseDaoImpl<Z_edupromote, String> implements Z_edupromoteDao {
    
}

