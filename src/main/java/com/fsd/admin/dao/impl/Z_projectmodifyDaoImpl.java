package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_projectmodifyDao;
import com.fsd.admin.model.Z_projectmodify;

@Repository("z_projectmodifyDaoImpl")
public class Z_projectmodifyDaoImpl extends BaseDaoImpl<Z_projectmodify, String> implements Z_projectmodifyDao {
    
}

