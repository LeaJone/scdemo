package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.F_middlechecknextDao;
import com.fsd.admin.model.F_middlechecknext;

@Repository("f_middlechecknextDaoImpl")
public class F_middlechecknextDaoImpl extends BaseDaoImpl<F_middlechecknext, String> implements F_middlechecknextDao {
    
}

