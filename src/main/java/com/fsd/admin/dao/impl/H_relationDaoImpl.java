package com.fsd.admin.dao.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.H_relationDao;
import com.fsd.admin.model.H_relation;

@Repository("h_relationDaoImpl")
public class H_relationDaoImpl extends BaseDaoImpl<H_relation, String> implements H_relationDao {

	private static Log log = LogFactory.getLog(H_relationDaoImpl.class);

}

