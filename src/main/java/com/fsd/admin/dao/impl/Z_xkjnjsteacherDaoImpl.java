package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_xkjnjsteacherDao;
import com.fsd.admin.model.Z_xkjnjsteacher;

@Repository("z_xkjnjsteacherDaoImpl")
public class Z_xkjnjsteacherDaoImpl extends BaseDaoImpl<Z_xkjnjsteacher, String> implements Z_xkjnjsteacherDao {
    
}

