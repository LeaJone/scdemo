package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.B_subjectcheckDao;
import com.fsd.admin.model.B_subjectcheck;

@Repository("b_subjectcheckDaoImpl")
public class B_subjectcheckDaoImpl extends BaseDaoImpl<B_subjectcheck, String> implements B_subjectcheckDao {
    
}

