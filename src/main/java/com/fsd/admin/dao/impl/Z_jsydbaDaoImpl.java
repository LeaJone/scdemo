package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_jsydbaDao;
import com.fsd.admin.model.Z_jsydba;

@Repository("z_jsydbaDaoImpl")
public class Z_jsydbaDaoImpl extends BaseDaoImpl<Z_jsydba, String> implements Z_jsydbaDao {
    
}

