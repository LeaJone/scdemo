package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.B_opennumberDao;
import com.fsd.admin.model.B_opennumber;

@Repository("b_opennumberDaoImpl")
public class B_opennumberDaoImpl extends BaseDaoImpl<B_opennumber, String> implements B_opennumberDao {
    
}

