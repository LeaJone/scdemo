package com.fsd.admin.dao.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.A_EmployeeDao;
import com.fsd.admin.model.A_Employee;

@Repository("A_EmployeeDaoImpl")
public class A_EmployeeDaoImpl extends BaseDaoImpl<A_Employee, String> implements A_EmployeeDao {

	private static Log log = LogFactory.getLog(A_EmployeeDaoImpl.class);

}

