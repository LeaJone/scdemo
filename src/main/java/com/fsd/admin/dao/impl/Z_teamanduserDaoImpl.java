package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_teamanduserDao;
import com.fsd.admin.model.Z_teamanduser;

@Repository("z_teamanduserDaoImpl")
public class Z_teamanduserDaoImpl extends BaseDaoImpl<Z_teamanduser, String> implements Z_teamanduserDao {
    
}

