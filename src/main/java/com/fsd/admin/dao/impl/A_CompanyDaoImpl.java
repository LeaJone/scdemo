package com.fsd.admin.dao.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.A_CompanyDao;
import com.fsd.admin.model.A_Company;

@Repository("A_CompanyDaoImpl")
public class A_CompanyDaoImpl extends BaseDaoImpl<A_Company, String> implements A_CompanyDao {

	private static Log log = LogFactory.getLog(A_CompanyDaoImpl.class);

}

