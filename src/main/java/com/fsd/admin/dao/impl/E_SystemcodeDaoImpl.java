package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.admin.dao.E_SystemcodeDao;
import com.fsd.admin.model.E_Systemcode;
import com.fsd.core.dao.impl.BaseDaoImpl;

@Repository("e_systemcodeDaoImpl")
public class E_SystemcodeDaoImpl extends BaseDaoImpl<E_Systemcode, String> implements E_SystemcodeDao {

}
