package com.fsd.admin.dao.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Sys_SystemMenuPopupDao;
import com.fsd.admin.model.Sys_SystemMenuPopup;

@Repository("Sys_SystemMenuPopupDaoImpl")
public class Sys_SystemMenuPopupDaoImpl extends BaseDaoImpl<Sys_SystemMenuPopup, String> implements Sys_SystemMenuPopupDao {

	private static Log log = LogFactory.getLog(Sys_SystemMenuPopupDaoImpl.class);

}

