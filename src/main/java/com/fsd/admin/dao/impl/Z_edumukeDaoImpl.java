package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_edumukeDao;
import com.fsd.admin.model.Z_edumuke;

@Repository("z_edumukeDaoImpl")
public class Z_edumukeDaoImpl extends BaseDaoImpl<Z_edumuke, String> implements Z_edumukeDao {
    
}

