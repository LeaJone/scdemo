package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_eduprofessionDao;
import com.fsd.admin.model.Z_eduprofession;

@Repository("z_eduprofessionDaoImpl")
public class Z_eduprofessionDaoImpl extends BaseDaoImpl<Z_eduprofession, String> implements Z_eduprofessionDao {
    
}

