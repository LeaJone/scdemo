package com.fsd.admin.dao.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.z_professorDao;
import com.fsd.admin.model.z_professor;

@Repository("z_professorDaoImpl")
public class z_professorDaoImpl extends BaseDaoImpl<z_professor, String> implements z_professorDao {

	private static Log log = LogFactory.getLog(z_professorDaoImpl.class);

}

