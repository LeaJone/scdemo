package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.B_parameterlistDao;
import com.fsd.admin.model.B_parameterlist;

@Repository("b_parameterlistDaoImpl")
public class B_parameterlistDaoImpl extends BaseDaoImpl<B_parameterlist, String> implements B_parameterlistDao {
    
}

