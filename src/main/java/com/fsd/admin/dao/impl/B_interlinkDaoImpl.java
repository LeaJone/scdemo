package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.B_interlinkDao;
import com.fsd.admin.model.B_interlink;

@Repository("b_interlinkDaoImpl")
public class B_interlinkDaoImpl extends BaseDaoImpl<B_interlink, String> implements B_interlinkDao {
    
}

