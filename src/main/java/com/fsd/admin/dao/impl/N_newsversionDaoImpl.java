package com.fsd.admin.dao.impl;

import com.fsd.admin.dao.N_newsversionDao;
import com.fsd.admin.model.N_newsversion;
import com.fsd.core.dao.impl.BaseDaoImpl;
import org.springframework.stereotype.Repository;

@Repository("n_newsversionDaoImpl")
public class N_newsversionDaoImpl extends BaseDaoImpl<N_newsversion, String> implements N_newsversionDao {
    
}

