package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.B_articleDao;
import com.fsd.admin.model.B_article;

@Repository("b_articleDaoImpl")
public class B_articleDaoImpl extends BaseDaoImpl<B_article, String> implements B_articleDao {
    
}

