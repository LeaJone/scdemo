package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_jsydspDao;
import com.fsd.admin.model.Z_jsydsp;

@Repository("z_jsydspDaoImpl")
public class Z_jsydspDaoImpl extends BaseDaoImpl<Z_jsydsp, String> implements Z_jsydspDao {
    
}

