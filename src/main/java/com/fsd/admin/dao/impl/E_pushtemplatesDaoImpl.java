package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.E_pushtemplatesDao;
import com.fsd.admin.model.E_pushtemplates;

@Repository("e_pushtemplatesDaoImpl")
public class E_pushtemplatesDaoImpl extends BaseDaoImpl<E_pushtemplates, String> implements E_pushtemplatesDao {
    
}

