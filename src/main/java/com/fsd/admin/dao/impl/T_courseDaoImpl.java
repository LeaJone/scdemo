package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.admin.dao.T_courseDao;
import com.fsd.admin.model.T_course;
import com.fsd.core.dao.impl.BaseDaoImpl;

/**
 * 课程信息Dao实现类
 * @author Administrator
 *
 */
@Repository("t_courseDaoImpl")
public class T_courseDaoImpl extends BaseDaoImpl<T_course, String> implements T_courseDao {
    
}

