package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.I_knowledgeinfoDao;
import com.fsd.admin.model.I_knowledgeinfo;

@Repository("i_knowledgeinfoDaoImpl")
public class I_knowledgeinfoDaoImpl extends BaseDaoImpl<I_knowledgeinfo, String> implements I_knowledgeinfoDao {
    
}

