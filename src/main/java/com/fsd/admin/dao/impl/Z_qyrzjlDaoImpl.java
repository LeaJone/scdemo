package com.fsd.admin.dao.impl;

import com.fsd.admin.dao.Z_qyrzjlDao;
import com.fsd.admin.model.Z_qyrzjl;
import com.fsd.core.dao.impl.BaseDaoImpl;
import org.springframework.stereotype.Repository;

@Repository("z_qyrzjlDaoImpl")
public class Z_qyrzjlDaoImpl extends BaseDaoImpl<Z_qyrzjl, String> implements Z_qyrzjlDao {
    
}

