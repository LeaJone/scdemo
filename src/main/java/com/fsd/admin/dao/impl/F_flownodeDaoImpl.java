package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.F_flownodeDao;
import com.fsd.admin.model.F_flownode;

@Repository("f_flownodeDaoImpl")
public class F_flownodeDaoImpl extends BaseDaoImpl<F_flownode, String> implements F_flownodeDao {
    
}

