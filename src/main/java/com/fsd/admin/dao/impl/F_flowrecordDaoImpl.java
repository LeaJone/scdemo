package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.F_flowrecordDao;
import com.fsd.admin.model.F_flowrecord;

@Repository("f_flowrecordDaoImpl")
public class F_flowrecordDaoImpl extends BaseDaoImpl<F_flowrecord, String> implements F_flowrecordDao {
    
}

