package com.fsd.admin.dao.impl;

import com.fsd.admin.dao.Z_teamcgDao;
import com.fsd.admin.model.Z_teamcg;
import com.fsd.core.dao.impl.BaseDaoImpl;
import org.springframework.stereotype.Repository;

@Repository("z_teamcgDaoImpl")
public class Z_teamcgDaoImpl extends BaseDaoImpl<Z_teamcg, String> implements Z_teamcgDao {
    
}

