package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.B_subjectDao;
import com.fsd.admin.model.B_subject;

@Repository("b_subjectDaoImpl")
public class B_subjectDaoImpl extends BaseDaoImpl<B_subject, String> implements B_subjectDao {
    
}

