package com.fsd.admin.dao.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.F_prospectusDao;
import com.fsd.admin.model.F_prospectus;

/**
 * 计划任务书Dao实现类
 * @author Administrator
 *
 */
@Repository("f_prospectusDaoImpl")
public class F_prospectusDaoImpl extends BaseDaoImpl<F_prospectus, String> implements F_prospectusDao {

	private static Log log = LogFactory.getLog(F_prospectusDaoImpl.class);

}

