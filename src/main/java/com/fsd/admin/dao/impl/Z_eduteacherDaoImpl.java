package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_eduteacherDao;
import com.fsd.admin.model.Z_eduteacher;

@Repository("z_eduteacherDaoImpl")
public class Z_eduteacherDaoImpl extends BaseDaoImpl<Z_eduteacher, String> implements Z_eduteacherDao {
    
}

