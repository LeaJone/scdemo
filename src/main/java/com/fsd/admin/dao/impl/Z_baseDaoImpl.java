package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_baseDao;
import com.fsd.admin.model.Z_base;

@Repository("z_baseDaoImpl")
public class Z_baseDaoImpl extends BaseDaoImpl<Z_base, String> implements Z_baseDao {
    
}

