package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.admin.dao.E_AutoreplylistDao;
import com.fsd.admin.model.E_Autoreplylist;
import com.fsd.core.dao.impl.BaseDaoImpl;

@Repository("e_autoreplylistDaoImpl")
public class E_AutoreplylistDaoImpl extends BaseDaoImpl<E_Autoreplylist, String> implements E_AutoreplylistDao {
	
}
