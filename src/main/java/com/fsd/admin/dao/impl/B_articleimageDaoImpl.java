package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.B_articleimageDao;
import com.fsd.admin.model.B_articleimage;

@Repository("b_articleimageDaoImpl")
public class B_articleimageDaoImpl extends BaseDaoImpl<B_articleimage, String> implements B_articleimageDao {
    
}

