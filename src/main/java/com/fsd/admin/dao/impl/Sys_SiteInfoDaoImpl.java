package com.fsd.admin.dao.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Sys_SiteInfoDao;
import com.fsd.admin.model.Sys_SiteInfo;

@Repository("Sys_SiteInfoDaoImpl")
public class Sys_SiteInfoDaoImpl extends BaseDaoImpl<Sys_SiteInfo, String> implements Sys_SiteInfoDao {

	private static Log log = LogFactory.getLog(Sys_SiteInfoDaoImpl.class);

}

