package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_honorDao;
import com.fsd.admin.model.Z_honor;

@Repository("z_honorDaoImpl")
public class Z_honorDaoImpl extends BaseDaoImpl<Z_honor, String> implements Z_honorDao {
    
}

