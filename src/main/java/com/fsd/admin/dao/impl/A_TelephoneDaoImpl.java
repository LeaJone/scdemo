package com.fsd.admin.dao.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.A_TelephoneDao;
import com.fsd.admin.model.A_Telephone;

@Repository("A_TelephoneDaoImpl")
public class A_TelephoneDaoImpl extends BaseDaoImpl<A_Telephone, String> implements A_TelephoneDao {

	private static Log log = LogFactory.getLog(A_TelephoneDaoImpl.class);

}

