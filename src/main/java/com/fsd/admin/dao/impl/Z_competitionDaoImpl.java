package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_competitionDao;
import com.fsd.admin.model.Z_competition;

@Repository("z_competitionDaoImpl")
public class Z_competitionDaoImpl extends BaseDaoImpl<Z_competition, String> implements Z_competitionDao {
    
}

