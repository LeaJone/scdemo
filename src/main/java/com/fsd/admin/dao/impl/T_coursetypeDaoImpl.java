package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.admin.dao.T_coursetypeDao;
import com.fsd.admin.model.T_coursetype;
import com.fsd.core.dao.impl.BaseDaoImpl;

/**
 * 课程分类Dao实现类  
 * @author Administrator
 *
 */
@Repository("t_coursetypeDaoImpl")
public class T_coursetypeDaoImpl extends BaseDaoImpl<T_coursetype, String> implements T_coursetypeDao {
    
}

