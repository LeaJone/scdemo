package com.fsd.admin.dao.impl;

import com.fsd.admin.dao.Sys_seoDao;
import com.fsd.admin.model.Sys_seo;
import com.fsd.core.dao.impl.BaseDaoImpl;
import org.springframework.stereotype.Repository;

@Repository("sys_seoDaoImpl")
public class Sys_seoDaoImpl extends BaseDaoImpl<Sys_seo, String> implements Sys_seoDao {
    
}

