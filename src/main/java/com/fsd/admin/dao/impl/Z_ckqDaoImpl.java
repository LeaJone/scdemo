package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_ckqDao;
import com.fsd.admin.model.Z_ckq;

@Repository("z_ckqDaoImpl")
public class Z_ckqDaoImpl extends BaseDaoImpl<Z_ckq, String> implements Z_ckqDao {
    
}

