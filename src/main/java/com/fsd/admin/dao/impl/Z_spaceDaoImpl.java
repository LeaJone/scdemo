package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_spaceDao;
import com.fsd.admin.model.Z_space;

@Repository("z_spaceDaoImpl")
public class Z_spaceDaoImpl extends BaseDaoImpl<Z_space, String> implements Z_spaceDao {
    
}

