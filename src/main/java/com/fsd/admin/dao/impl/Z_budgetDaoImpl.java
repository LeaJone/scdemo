package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_budgetDao;
import com.fsd.admin.model.Z_budget;

@Repository("z_budgetDaoImpl")
public class Z_budgetDaoImpl extends BaseDaoImpl<Z_budget, String> implements Z_budgetDao {
    
}

