package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.admin.dao.E_AutoreplyDao;
import com.fsd.admin.model.E_Autoreply;
import com.fsd.core.dao.impl.BaseDaoImpl;

@Repository("e_autoreplyDaoImpl")
public class E_AutoreplyDaoImpl extends BaseDaoImpl<E_Autoreply, String> implements E_AutoreplyDao {

}
