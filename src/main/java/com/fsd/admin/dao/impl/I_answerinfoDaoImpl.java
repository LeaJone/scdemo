package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.I_answerinfoDao;
import com.fsd.admin.model.I_answerinfo;

@Repository("i_answerinfoDaoImpl")
public class I_answerinfoDaoImpl extends BaseDaoImpl<I_answerinfo, String> implements I_answerinfoDao {
    
}

