package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.F_flowprojectDao;
import com.fsd.admin.model.F_flowproject;

@Repository("f_flowprojectDaoImpl")
public class F_flowprojectDaoImpl extends BaseDaoImpl<F_flowproject, String> implements F_flowprojectDao {
    
}

