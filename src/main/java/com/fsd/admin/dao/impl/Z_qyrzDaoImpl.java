package com.fsd.admin.dao.impl;

import com.fsd.admin.dao.Z_qyrzDao;
import com.fsd.admin.model.Z_qyrz;
import com.fsd.core.dao.impl.BaseDaoImpl;
import org.springframework.stereotype.Repository;

@Repository("z_qyrzDaoImpl")
public class Z_qyrzDaoImpl extends BaseDaoImpl<Z_qyrz, String> implements Z_qyrzDao {
    
}

