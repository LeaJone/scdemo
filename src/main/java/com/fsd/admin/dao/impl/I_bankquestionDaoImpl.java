package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.I_bankquestionDao;
import com.fsd.admin.model.I_bankquestion;

@Repository("i_bankquestionDaoImpl")
public class I_bankquestionDaoImpl extends BaseDaoImpl<I_bankquestion, String> implements I_bankquestionDao {
    
}

