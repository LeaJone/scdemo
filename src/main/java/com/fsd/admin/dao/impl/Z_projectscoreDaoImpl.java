package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_projectscoreDao;
import com.fsd.admin.model.Z_projectscore;

@Repository("z_projectscoreDaoImpl")
public class Z_projectscoreDaoImpl extends BaseDaoImpl<Z_projectscore, String> implements Z_projectscoreDao {
    
}

