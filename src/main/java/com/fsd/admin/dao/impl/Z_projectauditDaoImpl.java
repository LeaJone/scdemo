package com.fsd.admin.dao.impl;

import org.springframework.stereotype.Repository;
import com.fsd.core.dao.impl.BaseDaoImpl;
import com.fsd.admin.dao.Z_projectauditDao;
import com.fsd.admin.model.Z_projectaudit;

@Repository("z_projectauditDaoImpl")
public class Z_projectauditDaoImpl extends BaseDaoImpl<Z_projectaudit, String> implements Z_projectauditDao {
    
}

