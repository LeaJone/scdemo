package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.A_Telephone;

public interface A_TelephoneDao extends BaseDao<A_Telephone, String>{
    
}

