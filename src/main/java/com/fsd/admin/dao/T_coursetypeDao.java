package com.fsd.admin.dao;

import com.fsd.admin.model.T_coursetype;
import com.fsd.core.dao.BaseDao;

/** 
 * 课程类型Dao接口
 * @author Administrator
 *
 */
public interface T_coursetypeDao extends BaseDao<T_coursetype, String>{
    
}

