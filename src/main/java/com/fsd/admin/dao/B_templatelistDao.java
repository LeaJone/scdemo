package com.fsd.admin.dao;

import com.fsd.admin.model.B_templatelist;
import com.fsd.core.dao.BaseDao;

public interface B_templatelistDao extends BaseDao<B_templatelist, String>{

}
