package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.Z_competition;

public interface Z_competitionDao extends BaseDao<Z_competition, String>{
    
}

