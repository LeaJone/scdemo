package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.B_parameterlist;

public interface B_parameterlistDao extends BaseDao<B_parameterlist, String>{
    
}

