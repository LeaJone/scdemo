package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.Z_eduproject;

public interface Z_eduprojectDao extends BaseDao<Z_eduproject, String>{
    
}

