package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.F_flowtype;

public interface F_flowtypeDao extends BaseDao<F_flowtype, String>{
    
}

