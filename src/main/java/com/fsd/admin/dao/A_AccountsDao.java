package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.A_Accounts;

public interface A_AccountsDao extends BaseDao<A_Accounts, String>{
    
}

