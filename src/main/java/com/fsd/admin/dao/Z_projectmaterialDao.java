package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.Z_projectmaterial;

public interface Z_projectmaterialDao extends BaseDao<Z_projectmaterial, String>{
    
}

