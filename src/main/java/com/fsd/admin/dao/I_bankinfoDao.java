package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.I_bankinfo;

public interface I_bankinfoDao extends BaseDao<I_bankinfo, String>{
    
}

