package com.fsd.admin.dao;

import com.fsd.admin.model.T_coursecontent;
import com.fsd.core.dao.BaseDao;

/**
 * 课时信息Dao接口
 * @author Administrator
 *
 */
public interface T_coursecontentDao extends BaseDao<T_coursecontent, String>{
    
}

