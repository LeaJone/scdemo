package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.I_testinfo;

public interface I_testinfoDao extends BaseDao<I_testinfo, String>{
    
}

