package com.fsd.admin.dao;

import com.fsd.admin.model.Sys_seo;
import com.fsd.core.dao.BaseDao;

public interface Sys_seoDao extends BaseDao<Sys_seo, String> {
    
}

