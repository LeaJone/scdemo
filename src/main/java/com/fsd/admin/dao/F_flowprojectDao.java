package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.F_flowproject;

public interface F_flowprojectDao extends BaseDao<F_flowproject, String>{
    
}

