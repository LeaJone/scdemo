package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.Z_service;

public interface Z_serviceDao extends BaseDao<Z_service, String>{
    
}

