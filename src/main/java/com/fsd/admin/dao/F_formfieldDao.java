package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.F_formfield;

public interface F_formfieldDao extends BaseDao<F_formfield, String>{
    
}

