package com.fsd.admin.dao;

import com.fsd.admin.model.I_knowledgerelate;
import com.fsd.core.dao.BaseDao;

public interface I_knowledgerelateDao extends BaseDao<I_knowledgerelate, String>{

}
