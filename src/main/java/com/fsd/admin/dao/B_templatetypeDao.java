package com.fsd.admin.dao;

import com.fsd.admin.model.B_templatetype;
import com.fsd.core.dao.BaseDao;

public interface B_templatetypeDao extends BaseDao<B_templatetype, String>{

}
