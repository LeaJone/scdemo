package com.fsd.admin.dao;

import com.fsd.admin.model.N_pushsettings;
import com.fsd.core.dao.BaseDao;

public interface N_pushsettingsDao extends BaseDao<N_pushsettings, String>{
    
}

