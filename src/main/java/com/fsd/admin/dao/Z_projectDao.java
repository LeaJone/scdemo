package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.Z_project;

public interface Z_projectDao extends BaseDao<Z_project, String>{
    
}

