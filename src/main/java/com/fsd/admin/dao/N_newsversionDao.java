package com.fsd.admin.dao;

import com.fsd.admin.model.N_newsversion;
import com.fsd.core.dao.BaseDao;

public interface N_newsversionDao extends BaseDao<N_newsversion, String>{
    
}

