package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.Sys_SystemMenuPopup;

public interface Sys_SystemMenuPopupDao extends BaseDao<Sys_SystemMenuPopup, String>{
    
}

