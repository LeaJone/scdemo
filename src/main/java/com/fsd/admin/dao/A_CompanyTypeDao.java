package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.A_CompanyType;

public interface A_CompanyTypeDao extends BaseDao<A_CompanyType, String>{
    
}

