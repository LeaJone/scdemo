package com.fsd.admin.dao;

import com.fsd.admin.model.Sys_Databackup;
import com.fsd.core.dao.BaseDao;

public interface Sys_DatabackupDao extends BaseDao<Sys_Databackup, String> {

}
