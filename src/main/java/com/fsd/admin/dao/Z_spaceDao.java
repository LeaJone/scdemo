package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.Z_space;

public interface Z_spaceDao extends BaseDao<Z_space, String>{
    
}

