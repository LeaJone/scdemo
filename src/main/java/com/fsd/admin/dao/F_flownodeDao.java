package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.F_flownode;

public interface F_flownodeDao extends BaseDao<F_flownode, String>{
    
}

