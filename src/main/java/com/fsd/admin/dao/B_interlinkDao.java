package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.B_interlink;

public interface B_interlinkDao extends BaseDao<B_interlink, String>{
    
}

