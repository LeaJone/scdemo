package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.Sys_SiteInfo;

public interface Sys_SiteInfoDao extends BaseDao<Sys_SiteInfo, String>{
    
}

