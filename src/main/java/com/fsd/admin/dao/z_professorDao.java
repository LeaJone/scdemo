package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.z_professor;

public interface z_professorDao extends BaseDao<z_professor, String>{
    
}

