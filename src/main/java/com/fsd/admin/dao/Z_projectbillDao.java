package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.Z_projectbill;

public interface Z_projectbillDao extends BaseDao<Z_projectbill, String>{
    
}

