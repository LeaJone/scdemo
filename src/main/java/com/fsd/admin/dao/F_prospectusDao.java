package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.F_prospectus;

public interface F_prospectusDao extends BaseDao<F_prospectus, String>{
    
}

