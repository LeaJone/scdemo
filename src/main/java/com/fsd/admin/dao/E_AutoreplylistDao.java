package com.fsd.admin.dao;

import com.fsd.admin.model.E_Autoreplylist;
import com.fsd.core.dao.BaseDao;

public interface E_AutoreplylistDao extends BaseDao<E_Autoreplylist, String> {

}
