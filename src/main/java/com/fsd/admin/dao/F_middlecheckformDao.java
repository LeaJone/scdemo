package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.F_middlecheckform;

public interface F_middlecheckformDao extends BaseDao<F_middlecheckform, String>{
    
}

