package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.I_answerinfo;

public interface I_answerinfoDao extends BaseDao<I_answerinfo, String>{
    
}

