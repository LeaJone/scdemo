package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.I_bankquestiontype;

public interface I_bankquestiontypeDao extends BaseDao<I_bankquestiontype, String>{
    
}

