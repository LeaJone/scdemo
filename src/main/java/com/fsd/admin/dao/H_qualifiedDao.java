package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.H_qualified;

public interface H_qualifiedDao extends BaseDao<H_qualified, String>{
    
}

