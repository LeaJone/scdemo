package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.I_knowledgeinfo;

public interface I_knowledgeinfoDao extends BaseDao<I_knowledgeinfo, String>{
    
}

