package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.A_area;

public interface A_areaDao extends BaseDao<A_area, String>{
    
}

