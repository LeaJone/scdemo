package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.B_article;

public interface B_articleDao extends BaseDao<B_article, String>{
    
}

