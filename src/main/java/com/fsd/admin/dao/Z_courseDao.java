package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.Z_course;

public interface Z_courseDao extends BaseDao<Z_course, String>{
    
}

