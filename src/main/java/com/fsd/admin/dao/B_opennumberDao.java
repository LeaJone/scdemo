package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.B_opennumber;

public interface B_opennumberDao extends BaseDao<B_opennumber, String>{
    
}

