package com.fsd.admin.dao;

import com.fsd.admin.model.E_userinfo;
import com.fsd.core.dao.BaseDao;

public interface E_UserinfoDao extends BaseDao<E_userinfo, String> {

}
