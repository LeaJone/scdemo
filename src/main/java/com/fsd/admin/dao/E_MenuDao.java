package com.fsd.admin.dao;

import com.fsd.admin.model.E_Menu;
import com.fsd.core.dao.BaseDao;

public interface E_MenuDao extends BaseDao<E_Menu, String> {

}
