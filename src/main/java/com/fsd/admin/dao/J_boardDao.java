package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.J_board;

public interface J_boardDao extends BaseDao<J_board, String>{
    
}

