package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.A_Branch;

public interface A_BranchDao extends BaseDao<A_Branch, String>{
    
}

