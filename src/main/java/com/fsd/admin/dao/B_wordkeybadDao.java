package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.B_wordkeybad;

public interface B_wordkeybadDao extends BaseDao<B_wordkeybad, String>{
    
}

