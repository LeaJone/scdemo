package com.fsd.admin.dao;

import com.fsd.core.dao.BaseDao;
import com.fsd.admin.model.Sys_PopedomAllocate;

public interface Sys_PopedomAllocateDao extends BaseDao<Sys_PopedomAllocate, String>{
    
}

