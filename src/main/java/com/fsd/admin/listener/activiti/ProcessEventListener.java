package com.fsd.admin.listener.activiti;

import com.fsd.admin.model.A_Employee;
import com.fsd.admin.service.A_EmployeeService;
import com.fsd.admin.service.E_pushtemplatesService;
import com.fsd.admin.service.Sys_SystemParametersService;
import com.fsd.admin.service.Z_emailService;
import com.fsd.admin.service.impl.Sys_SystemParametersServiceImpl;
import com.fsd.core.util.SpringContextHolder;
import com.fsd.core.wechat.entity.message.resp.TemplateMessage;
import org.activiti.engine.delegate.event.ActivitiEntityEvent;
import org.activiti.engine.delegate.event.ActivitiEvent;
import org.activiti.engine.delegate.event.ActivitiEventListener;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.impl.persistence.entity.TaskEntity;

/**
 * ================================
 * 流程事件监听
 * @Author: lumingbao
 * @Date 2019-01-04 14:49
 * ================================
 */
public class ProcessEventListener implements ActivitiEventListener {
    @Override
    public void onEvent(ActivitiEvent event) {
        A_EmployeeService employeeService = (A_EmployeeService) SpringContextHolder.getBean("A_EmployeeServiceImpl");
        Sys_SystemParametersService systemParametersService = (Sys_SystemParametersService) SpringContextHolder.getBean("Sys_SystemParametersServiceImpl");
        E_pushtemplatesService pushtemplatesService = (E_pushtemplatesService) SpringContextHolder.getBean("e_pushtemplatesServiceImpl");
        Z_emailService emailService = (Z_emailService) SpringContextHolder.getBean("z_emailServiceImpl");

        switch (event.getType()) {
            case ACTIVITY_COMPENSATE:
                // 一个节点将要被补偿。事件包含了将要执行补偿的节点id。
                break;
            case ACTIVITY_COMPLETED:
                // 一个节点成功结束
                break;
            case ACTIVITY_ERROR_RECEIVED:
                // 一个节点收到了一个错误事件。在节点实际处理错误之前触发。 事件的activityId对应着处理错误的节点。 这个事件后续会是ACTIVITY_SIGNALLED或ACTIVITY_COMPLETE， 如果错误发送成功的话。
                break;
            case ACTIVITY_MESSAGE_RECEIVED:
                // 一个节点收到了一个消息。在节点收到消息之前触发。收到后，会触发ACTIVITY_SIGNAL或ACTIVITY_STARTED，这会根据节点的类型（边界事件，事件子流程开始事件）
                break;
            case ACTIVITY_SIGNALED:
                // 一个节点收到了一个信号
                break;
            case ACTIVITY_STARTED:
                // 一个节点开始执行
                break;
            case CUSTOM:
                break;
            case ENGINE_CLOSED:
                // 监听器监听的流程引擎已经关闭，不再接受API调用。
                break;
            case ENGINE_CREATED:
                // 监听器监听的流程引擎已经创建完毕，并准备好接受API调用。
                break;
            case ENTITY_ACTIVATED:
                // 激活了已存在的实体，实体包含在事件中。会被ProcessDefinitions, ProcessInstances 和 Tasks抛出。
                break;
            case ENTITY_CREATED:
                // 创建了一个新实体。实体包含在事件中。
                break;
            case ENTITY_DELETED:
                // 删除了已存在的实体。实体包含在事件中
                break;
            case ENTITY_INITIALIZED:
                // 创建了一个新实体，初始化也完成了。如果这个实体的创建会包含子实体的创建，这个事件会在子实体都创建/初始化完成后被触发，这是与ENTITY_CREATED的区别。
                break;
            case ENTITY_SUSPENDED:
                // 暂停了已存在的实体。实体包含在事件中。会被ProcessDefinitions, ProcessInstances 和 Tasks抛出。
                break;
            case ENTITY_UPDATED:
                // 更新了已存在的实体。实体包含在事件中。
                break;
            case JOB_EXECUTION_FAILURE:
                // 作业执行失败。作业和异常信息包含在事件中。
                break;
            case JOB_EXECUTION_SUCCESS:
                // 作业执行成功。job包含在事件中。
                break;
            case JOB_RETRIES_DECREMENTED:
                // 因为作业执行失败，导致重试次数减少。作业包含在事件中。
                break;
            case MEMBERSHIPS_DELETED:
                // 所有成员被从一个组中删除。在成员删除之前触发这个事件，所以他们都是可以访问的。 因为性能方面的考虑，不会为每个成员触发单独的MEMBERSHIP_DELETED事件。
                break;
            case MEMBERSHIP_CREATED:
                // 用户被添加到一个组里。事件包含了用户和组的id。
                break;
            case MEMBERSHIP_DELETED:
                // 用户被从一个组中删除。事件包含了用户和组的id。
                break;
            case TASK_ASSIGNED:
                // 任务被分配给了一个人员。事件包含任务。
                break;
            case TASK_COMPLETED:
                // 任务被完成了。它会在ENTITY_DELETE事件之前触发。当任务是流程一部分时，事件会在流程继续运行之前， 后续事件将是ACTIVITY_COMPLETE，对应着完成任务的节点。
                try{
                    ActivitiEntityEvent entityEvent = (ActivitiEntityEvent) event;
                    TaskEntity taskEntity = (TaskEntity) entityEvent.getEntity();
                    HistoricProcessInstance historicProcessInstance = event.getEngineServices().getHistoryService().createHistoricProcessInstanceQuery().processInstanceId(taskEntity.getProcessInstanceId()).singleResult();
                    if(historicProcessInstance != null) {
                        String userid = historicProcessInstance.getStartUserId();
                        System.out.println("流程发起人：" + userid);
                        A_Employee employee = employeeService.get(userid);
                        // 发送微信通知
                        if(employee.getIswechatbind() != null && "true".equals(employee.getIswechatbind())){
                            String wechatApiId = systemParametersService.getParameterValueByCode(Sys_SystemParametersServiceImpl.WECHATMEMBER, employee.getCompanyid());
                            TemplateMessage message = new TemplateMessage();
                            message.setItem("first",  "恭喜，您的申请有了新的进展", "#000000");
                            message.setItem("keyword1", employee.getRealname(), "#000000");
                            message.setItem("keyword2", taskEntity.getName(), "#000000");
                            message.setItem("keyword3", "完成", "#000000");
                            message.setItem("remark", "兰州工业学院创新创业学院", "#1111EE");
                            pushtemplatesService.sendTemplateMessage(wechatApiId, "lcsptx", employee, message);
                        }
                        // 发送站内信通知
                        StringBuffer sb = new StringBuffer();
                        sb.append("申请人：" + employee.getRealname());
                        sb.append("<br />");
                        sb.append("审批类型：" + taskEntity.getName());
                        sb.append("<br />");
                        sb.append("审批状态：完成");
                        sb.append("<br />");
                        sb.append("兰州工业学院创新创业学院");
                        emailService.saveMessage("恭喜，您的申请有了新的进展。", sb.toString(), employee);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            case TIMER_FIRED:
                // 触发了定时器。job包含在事件中。
                break;
            case UNCAUGHT_BPMN_ERROR:
                break;
            case VARIABLE_CREATED:
                break;
            case VARIABLE_DELETED:
                break;
            case VARIABLE_UPDATED:
                break;
            default:
                break;
        }
    }

    @Override
    public boolean isFailOnException() {
        return false;
    }
}
