package com.fsd.admin.listener.activiti;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * ================================
 * @Description: 填充到期日任务监听器
 * @Author: lumingbao
 * @Date 2019-01-03 17:09
 * ================================
 */
public class DueDateTaskListener implements TaskListener{

    @Override
    public void notify(DelegateTask delegateTask) {
        try {
            if(delegateTask.getDueDate() != null){
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(delegateTask.getDueDate());
                int day = calendar.get(Calendar.YEAR);
                System.out.println("超时时间为：" + day);
                calendar.setTime(delegateTask.getCreateTime());
                calendar.add(Calendar.DAY_OF_MONTH, day);
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                System.out.println("具体到期时间为：" +formatter.format(calendar.getTime()));
                delegateTask.setDueDate(calendar.getTime());
            }
        }catch (Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }
}
