package com.fsd.admin.listener.activiti;

import com.fsd.admin.model.A_Employee;
import com.fsd.admin.service.A_EmployeeService;
import com.fsd.admin.service.E_pushtemplatesService;
import com.fsd.admin.service.Sys_SystemParametersService;
import com.fsd.admin.service.Z_emailService;
import com.fsd.admin.service.impl.Sys_SystemParametersServiceImpl;
import com.fsd.core.util.SpringContextHolder;
import com.fsd.core.wechat.entity.message.resp.TemplateMessage;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import java.text.SimpleDateFormat;

/**
 * ================================
 * 用户给用户发送任务待办提醒
 * @Author: lumingbao
 * @Date 2019-01-04 13:10
 * ================================
 */
public class TaskMessageWarnTaskListener implements TaskListener {

    @Override
    public void notify(DelegateTask delegateTask) {
        try{
            A_EmployeeService employeeService = (A_EmployeeService) SpringContextHolder.getBean("A_EmployeeServiceImpl");
            Sys_SystemParametersService systemParametersService = (Sys_SystemParametersService) SpringContextHolder.getBean("Sys_SystemParametersServiceImpl");
            E_pushtemplatesService pushtemplatesService = (E_pushtemplatesService) SpringContextHolder.getBean("e_pushtemplatesServiceImpl");
            Z_emailService emailService = (Z_emailService) SpringContextHolder.getBean("z_emailServiceImpl");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            if(delegateTask.getAssignee().length() == 10 || delegateTask.getAssignee().length() == 12 ){
                A_Employee employee = employeeService.get(delegateTask.getAssignee());
                // 发送微信通知
                if(employee.getIswechatbind() != null && "true".equals(employee.getIswechatbind())){
                    String wechatApiId = systemParametersService.getParameterValueByCode(Sys_SystemParametersServiceImpl.WECHATMEMBER, employee.getCompanyid());
                    TemplateMessage message = new TemplateMessage();
                    message.setItem("first", "尊敬的" + employee.getRealname() + "，您有新的任务，请及时处理", "#000000");
                    message.setItem("keyword1", delegateTask.getName(), "#000000");
                    message.setItem("keyword2", formatter.format(delegateTask.getCreateTime()), "#000000");
                    message.setItem("remark", "兰州工业学院创新创业学院", "#1111EE");
                    pushtemplatesService.sendTemplateMessage(wechatApiId, "lcdbtx", employee, message);
                }
                // 发送站内信通知
                StringBuffer sb = new StringBuffer();
                sb.append("任务名称：" + delegateTask.getName());
                sb.append("<br />");
                sb.append("提交时间：" + formatter.format(delegateTask.getCreateTime()));
                sb.append("<br />");
                sb.append("兰州工业学院创新创业学院");
                emailService.saveMessage("您有新的任务，请及时处理。", sb.toString(), employee);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
