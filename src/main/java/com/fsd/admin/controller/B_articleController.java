package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fsd.core.util.Config;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.B_article;
import com.fsd.admin.service.B_articleService;
import com.fsd.admin.service.B_wordkeybadService;

@Controller
@RequestMapping("/admin/B_Article")
public class B_articleController extends AdminController{
	
	private static final Logger log = Logger.getLogger(B_articleController.class);
    
    @Resource(name = "b_articleServiceImpl")
	private B_articleService objectService;
    
    @Resource(name = "b_wordkeybadServiceImpl")
	private B_wordkeybadService wordService;

    
    /**
     * 获得文章内容
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/get_ArticleMContent.do")
    public @ResponseBody Object getArticleMContent(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getArticleMContent(parameters, "copygl"));
    }
    /**
     * 获得文章内容
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/get_ArticleMContentYB.do")
    public @ResponseBody Object getArticleMContentYB(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getArticleMContent(parameters, "coopyyb"));
    }

    /**
     * 加载文本文章数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_Article.do")
    public @ResponseBody Object loadArticle(ParametersUtil parameters , HttpServletRequest request) throws Exception{
    	return objectService.getArticlePageList(parameters , this.getLoginInfo(request.getSession()));
    }
    
    /**
     * 加载已删除文章数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_RecycleArticle.do")
    public @ResponseBody Object loadRecycleArticle(ParametersUtil parameters , HttpServletRequest request) throws Exception{
    	return objectService.getRecycleArticlePageList(parameters , this.getLoginUser(request.getSession()));
    }
    
    /**
     * 保存文章
     * @param obj
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_Article.do")
    public @ResponseBody Object saveArticle(B_article obj, ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	System.out.println(obj.getContent());
    	wordService.checkTextByWordType(Config.WORDTYPEBAD, "内容", obj.getContent(), this.getLoginUser(request.getSession()));
    	ParametersUtil util = objectService.saveArticle(obj, parameters, this.getLoginUser(request.getSession()));
    	return util.sendObject();
    }
    
    /**
     * 保存文章Wap信息
     * @param obj
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_WapArticle.do")
    public @ResponseBody Object saveArticleWap(B_article obj, ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	wordService.checkTextByWordType(Config.WORDTYPEBAD, "", obj.getMcontent(), this.getLoginUser(request.getSession()));
    	objectService.saveArticleWap(obj, parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_ArticleByid.do")
    public @ResponseBody Object loadArticleByid(ParametersUtil parameters) throws Exception{
    	return objectService.getArticleById(parameters).sendObject();
    }
    
    /**
     * 删除文章
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_Article_qx_wzsc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 审核文章
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/audit_Article_qx_wzsh.do")
    public @ResponseBody Object auditObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.updateAuditObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 置顶文章
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/firstly_Article_qx_wzbj.do")
    public @ResponseBody Object firstlyObject(ParametersUtil parameters) throws Exception{
    	objectService.updateFirstlyObject(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 幻灯文章
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/slide_Article_qx_wzbj.do")
    public @ResponseBody Object slideObject(ParametersUtil parameters) throws Exception{
    	objectService.updateSlideObject(parameters);
    	return ParametersUtil.sendList(null);
    }

    /**
     * 推荐文章
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/recommend_Article_qx_wzbj.do")
    public @ResponseBody Object recommendObject(ParametersUtil parameters) throws Exception{
        objectService.updateRecommendObject(parameters);
        return ParametersUtil.sendList(null);
    }
    
    /**
     * 热点文章
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/hot_Article_qx_wzbj.do")
    public @ResponseBody Object hotObject(ParametersUtil parameters) throws Exception{
    	objectService.updateHotObject(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 更改文章类型
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/update_ArticleType_qx_wzbj.do")
    public @ResponseBody Object updateObjectType(ParametersUtil parameters) throws Exception{
    	objectService.updateObjectType(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 更改文章栏目
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/update_ArticleSubject_qx_wzbj.do")
    public @ResponseBody Object updateObjectSubject(ParametersUtil parameters) throws Exception{
    	objectService.updateObjectSubject(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 恢复文章
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/recovery_Article.do")
    public @ResponseBody Object recoveryObject(ParametersUtil parameters) throws Exception{
    	objectService.updateObjectDeleted(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 彻底删除文章
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/reldel_Article.do")
    public @ResponseBody Object reldelObject(ParametersUtil parameters) throws Exception{
    	objectService.delRealyObject(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 附件转换为flash文件
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/zhuanhuan_ArticleFlash.do")
    public @ResponseBody Object zhuanHuan(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	String rootPath = this.getRootPaht(request.getSession());
    	String url = objectService.zhuanHuanFlash(parameters, rootPath);
    	return ParametersUtil.sendList(url);
    }
}