package com.fsd.admin.controller;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_template;
import com.fsd.admin.service.A_templateService;

@Controller
@RequestMapping("/admin/a_template")
public class A_templateController extends AdminController{
	
	private static final Logger log = Logger.getLogger(A_templateController.class);
    
    @Autowired
	private A_templateService templateService;

    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
        return templateService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagefiledata.do")
    public @ResponseBody Object loadPageFileData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return templateService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_a_template_qx_a_templatebj.do")
    public @ResponseBody Object saveObject(A_template obj, HttpServletRequest request) throws Exception{
        templateService.save(obj, this.getLoginUser(request.getSession()), this.getRootPaht(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_a_template_qx_a_templatesc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
        templateService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_a_templatebyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception {
        return ParametersUtil.sendList(templateService.getObjectById(parameters));
    }
}