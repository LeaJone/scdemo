package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fsd.admin.model.B_leaveword;
import com.fsd.admin.service.B_leavewordService;
import com.fsd.core.util.ParametersUtil;

@Controller
@RequestMapping("/admin/b_leaveword")
public class B_leavewordController extends AdminController{
	
	private static final Logger log = Logger.getLogger(B_leavewordController.class);
    
    @Resource(name = "b_leavewordServiceImpl")
	private B_leavewordService objectService;
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 加载回收站分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedatarecycle.do")
    public @ResponseBody Object loadPageDataRecycle(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageListRecycle(parameters, this.getLoginUser(request.getSession()));
    }

    /**
     * 加载分页数据，加载所属回复人数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedatabyemployee.do")
    public @ResponseBody Object loadPageDataByEmployee(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageListByEmployee(parameters, this.getLoginInfo(request.getSession()));
    }
    
    /**
     * 保存对象
     * @param Object
     * @return
     * @throws Exception
     */
    @RequestMapping({
    	"/insert_LeaveWord.do", 
    	"/save_LeaveWord_qx_ptlyhf.do", 
    	"/save_LeaveWord_qx_zxzxhf.do", 
    	"/save_LeaveWord_qx_ldxxhf.do", 
    	"/save_LeaveWord_qx_xfxxhf.do", 
    	"/save_LeaveWord_qx_jbxxhf.do", 
    	"/save_LeaveWord_qx_jjjchf.do", 
    	"/save_LeaveWord_qx_gkyjhf.do"})
    public @ResponseBody Object saveObject(B_leaveword Object, HttpServletRequest request) throws Exception{
    	objectService.saveObject(Object, this.getLoginInfo(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_LeaveWordByid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_LeaveWord_qx_jssc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 恢复对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/recycle_LeaveWord.do")
    public @ResponseBody Object recycleObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.recycleObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 审核
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping({
    	"/audit_LeaveWord_qx_ptlysh.do", 
    	"/audit_LeaveWord_qx_zxzxsh.do", 
    	"/audit_LeaveWord_qx_ldxxsh.do", 
    	"/audit_LeaveWord_qx_xfxxsh.do", 
    	"/audit_LeaveWord_qx_jbxxsh.do", 
    	"/audit_LeaveWord_qx_jjjcsh.do", 
    	"/audit_LeaveWord_qx_gkyjsh.do"})
    public @ResponseBody Object auditObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.auditObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 选登
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping({
    	"/audit_LeaveWord_qx_ptlyxd.do", 
    	"/audit_LeaveWord_qx_zxzxxd.do", 
    	"/audit_LeaveWord_qx_ldxxxd.do", 
    	"/audit_LeaveWord_qx_xfxxxd.do", 
    	"/audit_LeaveWord_qx_jbxxxd.do", 
    	"/audit_LeaveWord_qx_jjjcxd.do", 
    	"/audit_LeaveWord_qx_gkyjxd.do"})
    public @ResponseBody Object chooseObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.chooseObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 处理未进入流程的信箱数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/dealt_LeaveWord.do")
    public @ResponseBody Object saveDealtObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.saveDealtObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 敏感标志
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/audit_LeaveWordMgbz.do")
    public @ResponseBody Object flageObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.flageObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
}