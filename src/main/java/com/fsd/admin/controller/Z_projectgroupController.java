package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.Z_projectgroup;
import com.fsd.admin.service.Z_projectgroupService;

@Controller
@RequestMapping("/admin/z_projectgroup")
public class Z_projectgroupController extends AdminController{
	
	private static final Logger log = Logger.getLogger(Z_projectgroupController.class);
    
    @Resource(name = "z_projectgroupServiceImpl")
	private Z_projectgroupService objectService;
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }

    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata1.do")
    public @ResponseBody Object loadPageData1(ParametersUtil parameters, HttpServletRequest request) throws Exception{
        return objectService.getObjectPageList1(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_z_projectgroup_qx_z_projectgroupbj.do")
    public @ResponseBody Object saveObject(Z_projectgroup obj, HttpServletRequest request) throws Exception{
    	objectService.save(obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_z_projectgroup_qx_z_projectgroupsc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_z_projectgroupbyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }

    /**
     * 保存项目分组
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_z_projectgroup.do")
    public @ResponseBody Object saveProjectGroup(ParametersUtil parameters) throws Exception{
        objectService.saveProjectGroup(parameters);
        return ParametersUtil.sendList();
    }
}