package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fsd.admin.model.B_article;
import com.fsd.admin.model.Z_articlefz;
import com.fsd.admin.service.B_wordkeybadService;
import com.fsd.admin.service.Z_zwftglService;
import com.fsd.core.util.Config;
import com.fsd.core.util.ParametersUtil;

@Controller
@RequestMapping("/admin/z_zwftgl")
public class Z_zwftglController extends AdminController {
	
	private static final Logger log = Logger.getLogger(Z_zwftglController.class);
	
	@Resource(name = "Z_zwftglServiceImpl")
	private Z_zwftglService objectService;
	
	@Resource(name = "b_wordkeybadServiceImpl")
	private B_wordkeybadService wordService;
	
	/**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters) throws Exception{
    	return objectService.getObjectPageList(parameters);
    }
    
    /**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_z_zwftgl_qx_z_zwftglbj.do")
    public @ResponseBody Object saveObject(Z_articlefz obj, HttpServletRequest request,  B_article article, ParametersUtil parameters) throws Exception{
    	ParametersUtil util = objectService.save(obj, this.getLoginUser(request.getSession()), article, parameters);
    	return util.sendObject();
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_z_zwftgl_qx_z_zwftglsc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_z_zwftglbyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return objectService.getObjectById(parameters).sendObject();
    }
    
    /**
     * 修改状态
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/enabled_qdnrb_qx.do")
    public @ResponseBody Object enabledObject(ParametersUtil parameters) throws Exception{
    	objectService.updateEnabledObject(parameters);
    	return ParametersUtil.sendList(null);
    }
}
