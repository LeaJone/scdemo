package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fsd.admin.model.A_notify;
import com.fsd.admin.service.A_notifyService;
import com.fsd.core.util.ParametersUtil;

@Controller
@RequestMapping("/admin/a_notify")
public class A_notifyController extends AdminController{
	
	private static final Logger log = Logger.getLogger(A_notifyController.class);
    
    @Resource(name = "A_notifyServiceImpl")
	private A_notifyService objectService;
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 加载启用数据
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_dataqy.do")
    public @ResponseBody Object loadDataQY(HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectListQY(this.getLoginUser(request.getSession())));
    }
    
    /**
     * 保存对象
     * @param obj
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_Notify_qx_tzbj.do")
    public @ResponseBody Object saveObject(A_notify obj, HttpServletRequest request) throws Exception{
    	objectService.saveObject(obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_NotifyByid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_Notify_qx_tzsc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters) throws Exception{
    	objectService.delObject(parameters);
    	return ParametersUtil.sendList(null);
    }

    /**
     * 修改对象置顶状态
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/firstly_Notify_qx_tzbj.do")
    public @ResponseBody Object firstlyObject(ParametersUtil parameters) throws Exception{
    	objectService.updateFirstlyObject(parameters);
    	return ParametersUtil.sendList(null);
    }

    /**
     * 修改对象状态
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/enabled_Notify_qx_tzbj.do")
    public @ResponseBody Object enabledObject(ParametersUtil parameters) throws Exception{
    	objectService.updateEnabledObject(parameters);
    	return ParametersUtil.sendList(null);
    }
}