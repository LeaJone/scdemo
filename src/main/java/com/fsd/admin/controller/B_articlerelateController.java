package com.fsd.admin.controller;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.fsd.admin.service.B_articlerelateService;

@Controller
@RequestMapping("/admin/b_articlerelate")
public class B_articlerelateController extends AdminController{
	
	private static final Logger log = Logger.getLogger(B_articlerelateController.class);
    
    @Resource(name = "b_articlerelateServiceImpl")
	private B_articlerelateService objectService;
    
}