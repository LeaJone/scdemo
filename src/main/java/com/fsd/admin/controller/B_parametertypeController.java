package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fsd.admin.model.B_parametertype;
import com.fsd.admin.service.B_parametertypeService;
import com.fsd.core.util.ParametersUtil;

@Controller
@RequestMapping("/admin/b_parametertype")
public class B_parametertypeController extends AdminController{
	
	private static final Logger log = Logger.getLogger(B_parametertypeController.class);
    
    @Resource(name = "b_parametertypeServiceImpl")
	private B_parametertypeService objectService;

    /**
     * 加载数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_data.do")
    public @ResponseBody Object loadData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 加载对象树
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_ParameterTypeTree.do")
    public @ResponseBody Object getObjectTree(String node, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectTree(node, this.getLoginUser(request.getSession())));
    }
    
    /**
     * 异步加载对象树
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_AsyncParameterTypeTree.do")
    public @ResponseBody Object getAsyncObjectTree(String node, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getAsyncObjectTree(node, this.getLoginUser(request.getSession())));
    }
    
    /**
     * 保存对象
     * @param obj
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_ParameterType_qx_cslxbj.do")
    public @ResponseBody Object saveObject(B_parametertype obj , HttpServletRequest request) throws Exception{
    	objectService.saveObject(obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_ParameterType_qx_cslxsc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters) throws Exception{
    	objectService.delObject(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_ParameterTypeByid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
    
    /**
     * Excel导出
     * @param parameters
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/download.do")
    public @ResponseBody Object download(HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.download(this.getLoginUser(request.getSession()), this.getRootPaht(request.getSession()), this.getHttpRootPath(request)));
    }
}