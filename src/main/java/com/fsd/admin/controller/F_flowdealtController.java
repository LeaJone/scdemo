package com.fsd.admin.controller;

import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.fsd.admin.service.F_flowdealtService;

@Controller
@RequestMapping("/admin/f_flowdealt")
public class F_flowdealtController extends AdminController{
	
	private static final Logger log = Logger.getLogger(F_flowdealtController.class);
    
    @Resource(name = "f_flowdealtServiceImpl")
	private F_flowdealtService objectService;
    
}