package com.fsd.admin.controller;

import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.fsd.admin.service.A_AccountsService;

@Controller
@RequestMapping("/admin/A_Accounts")
public class A_AccountsController extends AdminController{
	
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger(A_AccountsController.class);
    
    @Resource(name = "A_AccountsServiceImpl")
	private A_AccountsService objectService;
    
}