package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.B_subject;
import com.fsd.admin.service.B_subjectService;
import com.fsd.admin.service.Z_fbtjService;

@Controller
@RequestMapping("/admin/Z_fbtj")
public class Z_fbtjController extends AdminController{
	
	private static final Logger log = Logger.getLogger(Z_fbtjController.class);
    
    @Resource(name = "z_fbtjServiceImpl")
	private Z_fbtjService objectService;
    
    
    /**
     * 查询统计数据(单位)
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/stat_data.do")
    public @ResponseBody Object statData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectStat(parameters, this.getLoginUser(request.getSession())));
    }
    
    /**
     * 查询统计数据(作者)
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/stat_dataAuthor.do")
    public @ResponseBody Object statDataAuthor(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectStatAuthor(parameters, this.getLoginUser(request.getSession())));
    }
    
    /**
     * 查询统计数据(登记部门)
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/stat_dataFb.do")
    public @ResponseBody Object statDataFb(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectStatFb(parameters, this.getLoginUser(request.getSession())));
    }
    
    /**
     * 下载(部门)
     * @param parameters
     * @param request
     * @return
     * @throws Exception
     */
    
    @RequestMapping(value = "/download.do")
    public @ResponseBody Object download(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.download(parameters, this.getLoginUser(request.getSession()), this.getRootPaht(request.getSession()), this.getHttpRootPath(request)));
    }
    
    /**
     * 下载(作者)
     * @param parameters
     * @param request
     * @return
     * @throws Exception
     */
    
    @RequestMapping(value = "/downloadAuthor.do")
    public @ResponseBody Object downloadAuthor(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.downloadAuthor(parameters, this.getLoginUser(request.getSession()), this.getRootPaht(request.getSession()), this.getHttpRootPath(request)));
    }
    
    /**
     * 下载(发布登记部门)
     * @param parameters
     * @param request
     * @return
     * @throws Exception
     */
    
    @RequestMapping(value = "/downloadFb.do")
    public @ResponseBody Object downloadFb(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.downloadFb(parameters, this.getLoginUser(request.getSession()), this.getRootPaht(request.getSession()), this.getHttpRootPath(request)));
    }
    
    /**
     * 生成厅机关文章
     * @throws Exception
     */
    @RequestMapping(value = "/save_ScWzTjg.do")
    public @ResponseBody Object saveScWzTjg(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.saveScWzTjg(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 生成直属事业单位文章
     * @throws Exception
     */
    @RequestMapping(value = "/save_ScWzSydw.do")
    public @ResponseBody Object saveScWzSydw(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.saveScWzSydw(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
}