package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fsd.admin.service.F_flowobjectService;
import com.fsd.core.util.ParametersUtil;

@Controller
@RequestMapping("/admin/f_flowobject")
public class F_flowobjectController extends AdminController{
	
	private static final Logger log = Logger.getLogger(F_flowobjectController.class);
    
    @Resource(name = "f_flowobjectServiceImpl")
	private F_flowobjectService objectService;

    /**
     * 加载分页数据，申请所有数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pageManage.do")
    public @ResponseBody Object loadPageManage(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageListByManage(parameters, this.getLoginInfo(request.getSession()));
    }
    
    /**
     * 加载分页数据，提取核数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pageExtract.do")
    public @ResponseBody Object loadPageExtract(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageListByExtract(parameters, this.getLoginInfo(request.getSession()));
    }
    
    /**
     * 加载分页数据，释放核数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pageRelease.do")
    public @ResponseBody Object loadPageRelease(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageListByRelease(parameters, this.getLoginInfo(request.getSession()));
    }
    
    /**
     * 加载分页数据，待审核数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagePending.do")
    public @ResponseBody Object loadPagePending(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageListByPending(parameters, this.getLoginInfo(request.getSession()));
    }

    /**
     * 加载分页数据，可撤回数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pageRecall.do")
    public @ResponseBody Object loadPageRecall(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageListByRecall(parameters, this.getLoginInfo(request.getSession()));
    }

    /**
     * 加载分页数据，处理过数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagePassed.do")
    public @ResponseBody Object loadPagePassed(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageListByPassed(parameters, this.getLoginInfo(request.getSession()));
    }

    /**
     * 加载分页数据，本人申请数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pageSelf.do")
    public @ResponseBody Object loadPageSelf(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageListBySelf(parameters, this.getLoginInfo(request.getSession()));
    }

    /**
     * 获取数据对象，根据审批记录ID
     * @param parameters
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_DataObjectByRecordID.do")
    public @ResponseBody ParametersUtil getDataObjectByRecordID(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getDataObjectByRecordID(parameters, this.getLoginInfo(request.getSession()));
    }
}