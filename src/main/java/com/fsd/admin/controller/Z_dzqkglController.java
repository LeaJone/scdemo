package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.B_article;
import com.fsd.admin.service.B_articleService;
import com.fsd.admin.service.Z_dzqkglService;

@Controller
@RequestMapping("/admin/z_dzqkgl")
public class Z_dzqkglController extends AdminController{
	
	private static final Logger log = Logger.getLogger(Z_dzqkglController.class);
    
    @Resource(name = "z_dzqkglServiceImpl")
	private Z_dzqkglService objectService;
    @Resource(name = "b_articleServiceImpl")
	private B_articleService articleService;
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters,this.getLoginUser(request.getSession()));
    }
    
    /**
     * 编辑对象
     * @param obj
     * @param parameters
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_Article.do")
    public @ResponseBody Object saveArticle(B_article obj, ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	ParametersUtil util = articleService.saveArticle(obj, parameters, this.getLoginUser(request.getSession()));
    	return util.sendObject();
    }
    
    /**
     * 删除文章
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_Article.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	articleService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_ArticleByid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return articleService.getArticleById(parameters).sendObject();
    }
    
    /**
     * 审核文章
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/audit_Article.do")
    public @ResponseBody Object auditObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	articleService.updateAuditObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
}