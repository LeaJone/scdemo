package com.fsd.admin.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.B_subjectcheck;
import com.fsd.admin.service.B_subjectcheckService;

@Controller
@RequestMapping("/admin/b_subjectcheck")
public class B_subjectcheckController extends AdminController{
	
	private static final Logger log = Logger.getLogger(B_subjectcheckController.class);
    
    @Resource(name = "b_subjectcheckServiceImpl")
	private B_subjectcheckService objectService;

	
    //---------------------栏目未更新-------------------------//
    /**
     * 栏目未更新，获取栏目树
     * @param parameters
     * @param request
     * @return
     * @throws Exception
     */
	@RequestMapping(value = "/get_B_subjectcheckIds.do")
	public @ResponseBody Object getB_subjectcheckIds(ParametersUtil parameters, HttpServletRequest request) throws Exception{
		return ParametersUtil.sendTreeList(objectService.getObjectCheck(parameters, this.getLoginUser(request.getSession()), this.getLoginUserPopedom(request.getSession())));
	}
	 
	/**
	 * 栏目未更新，保存栏目选择
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/save_B_subject.do")
	public @ResponseBody Object save_B_subject(ParametersUtil parameters) throws Exception{
		objectService.save_Subject(parameters);
		return ParametersUtil.sendList();
	}
 
	/**
	 * 栏目未更新，查询报表，检查结果
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getJcjgMap.do")
	public @ResponseBody Object getJcjgMap(ParametersUtil parameters, HttpServletRequest request) throws Exception{
	 	List<Map<String,String>> list = objectService.getJcjgMap(parameters, this.getLoginUser(request.getSession()));
		return list;
	}
	
	
	//---------------------栏目关联人员，或管理人员-------------------------//
	
	/**
     * 验证加载权限栏目
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/get_QxSubjectCheck.do")
    public @ResponseBody Object getQxObjectCheck(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendTreeList(objectService.getQxObjectCheck(parameters, this.getLoginUser(request.getSession()), this.getLoginUserPopedom(request.getSession())));
    }
    /**
	 * 保存栏目关联人员，或管理人员
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/save_B_subjectCheck.do")
	public @ResponseBody Object save_SubjectEmployee(ParametersUtil parameters) throws Exception{
		objectService.save_SubjectEmployee(parameters);
		return ParametersUtil.sendList();
	}
	
	//---------------------原始方法-------------------------//
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_b_subjectcheck_qx_b_subjectcheckbj.do")
    public @ResponseBody Object saveObject(B_subjectcheck obj, HttpServletRequest request) throws Exception{
    	objectService.save(obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_b_subjectcheck_qx_b_subjectchecksc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_b_subjectcheckbyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
    
    @RequestMapping(value = "/get_checkByRoleIds.do")
    public @ResponseBody Object getCheckByRoleIds(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getCheckByRoleIds(parameters));
    }
    
}