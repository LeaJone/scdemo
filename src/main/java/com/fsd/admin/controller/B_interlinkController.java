package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.B_interlink;
import com.fsd.admin.service.B_interlinkService;

@Controller
@RequestMapping("/admin/B_Interlink")
public class B_interlinkController extends AdminController{
	
	private static final Logger log = Logger.getLogger(B_interlinkController.class);
    
    @Resource(name = "b_interlinkServiceImpl")
	private B_interlinkService objectService;

    /**
     * 加载数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_data.do")
    public @ResponseBody Object loadData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 加载对象树
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_InterlinkTree.do")
    public @ResponseBody Object getObjectTree(String node) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectTree(node));
    }
    
    /**
     * 异步加载对象树
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_AsyncInterlinkTree.do")
    public @ResponseBody Object getAsyncObjectTree(String node) throws Exception{
    	return ParametersUtil.sendList(objectService.getAsyncObjectTree(node));
    }
    
    /**
     * 保存对象
     * @param obj
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_Interlink_qx_yqljbj.do")
    public @ResponseBody Object saveObject(B_interlink obj , HttpServletRequest request) throws Exception{
    	objectService.saveObject(obj , this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_Interlink_qx_yqljsc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters) throws Exception{
    	objectService.delObject(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 审核
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/audit_Interlink_qx_yqljbj.do")
    public @ResponseBody Object auditObject(ParametersUtil parameters) throws Exception{
    	objectService.auditObject(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_InterlinkByid.do")
    public @ResponseBody Object loadObjectById(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
}