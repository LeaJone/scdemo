package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fsd.admin.service.F_flowrecordService;
import com.fsd.core.util.ParametersUtil;

@Controller
@RequestMapping("/admin/f_flowrecord")
public class F_flowrecordController extends AdminController{
	
	private static final Logger log = Logger.getLogger(F_flowrecordController.class);
    
    @Resource(name = "f_flowrecordServiceImpl")
	private F_flowrecordService objectService;
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_RecordByid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_RecordDealtByid.do")
    public @ResponseBody Object loadRecordDealtByid(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getRecordDealtByid(parameters, this.getLoginInfo(request.getSession())).sendObject();
    }
    
    /**
     * 修改记录的提取状态
     * @param parameters
     * @throws Exception
     */
    @RequestMapping(value = "/extract_Record.do")
    public @ResponseBody Object extractObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.updateDealtStatusExtractByID(parameters, this.getLoginInfo(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 修改记录的释放状态
     * @param parameters
     * @throws Exception
     */
    @RequestMapping(value = "/release_Record.do")
    public @ResponseBody Object releaseObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.updateDealtStatusReleaseByID(parameters, this.getLoginInfo(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 保存处理结果
     * @param parameters
     * @throws Exception
     */
    @RequestMapping(value = "/save_DealtResults.do")
    public @ResponseBody Object saveDealtResults(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.saveDealtResults(parameters, this.getLoginInfo(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
}