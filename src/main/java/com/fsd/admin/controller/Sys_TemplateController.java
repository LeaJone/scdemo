package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.admin.service.Sys_TemplateService;
import com.fsd.core.util.ParametersUtil;

@Controller
@RequestMapping("/admin/Sys_Template")
public class Sys_TemplateController extends AdminController{
	
	private static final Logger log = Logger.getLogger(Sys_TemplateController.class);
    
    @Resource(name = "Sys_TemplateServiceImpl")
	private Sys_TemplateService objectService;
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()), this.getRootPaht(request.getSession())));
    }
    
    /**
     * 重命名
     * @param url
     * @param name
     * @param parameters
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_rename.do")
    public @ResponseBody Object saveRename(String url, String name, ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.renameFile(this.getRootPaht(request.getSession()), url, name);
    	return ParametersUtil.sendList();
    }
    
    /**
     * 删除文件
     * @param parameters
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_files.do")
    public @ResponseBody Object delFile(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delFiles(parameters, this.getRootPaht(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 加载文件内容
     * @param parameters
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_filecontent.do")
    public @ResponseBody Object loadFileContent(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getFileContent(parameters, this.getRootPaht(request.getSession())));
    }
    
    /**
     * 保存文件内容
     * @param url
     * @param content
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_filecontent.do")
    public @ResponseBody Object saveContent(String url, String content, HttpServletRequest request) throws Exception{
    	objectService.saveFile(url, content, this.getRootPaht(request.getSession()));
    	return ParametersUtil.sendList();
    }
}