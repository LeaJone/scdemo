package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.Z_space;
import com.fsd.admin.service.Z_spaceService;

@Controller
@RequestMapping("/admin/z_space")
public class Z_spaceController extends AdminController{
	
	private static final Logger log = Logger.getLogger(Z_spaceController.class);
    
    @Resource(name = "z_spaceServiceImpl")
	private Z_spaceService objectService;
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_z_space_qx_z_spacebj.do")
    public @ResponseBody Object saveObject(Z_space obj, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.save(obj, this.getLoginUser(request.getSession())));
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_z_space_qx_z_spacesc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_z_spacebyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }

    /**
     * 获得空间树结构
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_spacheTree.do")
    public @ResponseBody Object getTree(HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.getSpaceTree());
    }
}