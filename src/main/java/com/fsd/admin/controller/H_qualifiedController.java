package com.fsd.admin.controller;

import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.fsd.admin.service.H_qualifiedService;

@Controller
@RequestMapping("/admin/h_qualified")
public class H_qualifiedController extends AdminController{
	
	private static final Logger log = Logger.getLogger(H_qualifiedController.class);
    
    @Resource(name = "h_qualifiedServiceImpl")
	private H_qualifiedService objectService;
}