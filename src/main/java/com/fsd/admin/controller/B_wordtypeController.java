package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.B_wordtype;
import com.fsd.admin.service.B_wordtypeService;

@Controller
@RequestMapping("/admin/b_wordtype")
public class B_wordtypeController extends AdminController{
	
	private static final Logger log = Logger.getLogger(B_wordtypeController.class);
    
    @Resource(name = "b_wordtypeServiceImpl")
	private B_wordtypeService objectService;

    /**
     * 加载数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_data.do")
    public @ResponseBody Object loadData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 加载对象树
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_WordTypeTree.do")
    public @ResponseBody Object getObjectTree(String node, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectTree(node, this.getLoginUser(request.getSession())));
    }
    
    /**
     * 异步加载对象树
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_AsyncWordTypeTree.do")
    public @ResponseBody Object getAsyncObjectTree(String node, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getAsyncObjectTree(node, this.getLoginUser(request.getSession())));
    }
    
    /**
     * 保存对象
     * @param obj
     * @return
     * @throws Exception
     */
    @RequestMapping({
    	"/save_WordType_qx_gjclxbj.do", 
    	"/save_WordType_qx_mgclxbj.do"})
    public @ResponseBody Object saveObject(B_wordtype obj , HttpServletRequest request) throws Exception{
    	objectService.saveObject(obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping({
    	"/del_WordType_qx_gjclxsc.do", 
    	"/del_WordType_qx_mgclxsc.do"})
    public @ResponseBody Object delObject(ParametersUtil parameters) throws Exception{
    	objectService.delObject(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_WordTypeByid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
    
}