package com.fsd.admin.controller;

import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.fsd.admin.service.Sys_UserParametersService;

@Controller
@RequestMapping("/admin/Sys_UserParameters")
public class Sys_UserParametersController extends AdminController{
	
	private static final Logger log = Logger.getLogger(Sys_UserParametersController.class);
    
    @Resource(name = "Sys_UserParametersServiceImpl")
	private Sys_UserParametersService objectService;
    
}