package com.fsd.admin.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.Sys_SystemDictionary;
import com.fsd.admin.service.Sys_SystemDictionaryService;
import com.google.gson.Gson;

@Controller
@RequestMapping("/admin/Sys_SystemDictionary")
public class Sys_SystemDictionaryController extends AdminController{
	
	private static final Logger log = Logger.getLogger(Sys_SystemDictionaryController.class);
    
    @Resource(name = "Sys_SystemDictionaryServiceImpl")
	private Sys_SystemDictionaryService objectService;

    /**
     * 加载数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_sysdict_key.do")
    public @ResponseBody Object getObjectListByKey(ParametersUtil parameters) throws Exception{
    	if(parameters.getJsonData() != null && !"".equals(parameters.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
			if(objectMap.get("key") != null && !"".equals(objectMap.get("key"))){
				List<Sys_SystemDictionary> list = objectService.getObjectListByKey(objectMap.get("key").toString());
				List<Sys_SystemDictionary> lst = new ArrayList<Sys_SystemDictionary>();
				Sys_SystemDictionary empty = null;
				if (objectMap.get("isempty") != null && "true".equals(objectMap.get("isempty").toString())){
					empty = new Sys_SystemDictionary();
					empty.setCode("empty");
					empty.setName("　");
				}
				boolean isAdd = true;
				if (objectMap.get("yescodes") != null && !"".equals(objectMap.get("yescodes"))){
					ArrayList<String> idList = (ArrayList<String>) objectMap.get("yescodes");
					if (idList != null && idList.size() > 0){
						for (Sys_SystemDictionary obj : list) {
							if (idList.contains(obj.getCode())){
								isAdd = false;
								lst.add(obj);
							}
						}
					}
				}else if (objectMap.get("notcodes") != null && !"".equals(objectMap.get("notcodes"))){
					ArrayList<String> idList = (ArrayList<String>) objectMap.get("notcodes");
					if (idList != null && idList.size() > 0){
						for (Sys_SystemDictionary obj : list) {
							if (idList.contains(obj.getCode())){
								continue;
							}else{
								isAdd = false;
								lst.add(obj);
							}
						}
					}
				}
				if (isAdd){
					lst.addAll(list);
				}
				if (empty != null){
					lst.add(0, empty);
				}
				return lst;
			}
		}
    	return null;
    }

	/**
	 * 加载字典类型分页列表
	 * @param parameters
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/get_dictionaryTypePageList.do")
	public @ResponseBody Object getDictionaryTypePageList(ParametersUtil parameters, HttpServletRequest request) throws Exception{
		return objectService.getDictionaryTypePageList(parameters, this.getLoginUser(request.getSession()));
	}

	/**
	 * 加载字典分页列表
	 * @param parameters
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/get_dictionaryPageList.do")
	public @ResponseBody Object getDictionaryPageList(ParametersUtil parameters, HttpServletRequest request) throws Exception{
		return objectService.getDictionaryPageList(parameters, this.getLoginUser(request.getSession()));
	}

	/**
	 * 编辑字典类型
	 * @param obj
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/save_sys_systemdictionaryType.do")
	public @ResponseBody Object saveType(Sys_SystemDictionary obj, HttpServletRequest request) throws Exception{
		objectService.saveDictionaryType(obj, this.getLoginUser(request.getSession()));
		return ParametersUtil.sendList();
	}

	/**
	 * 编辑字典
	 * @param obj
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/save_sys_systemdictionary.do")
	public @ResponseBody Object saveObject(Sys_SystemDictionary obj, HttpServletRequest request) throws Exception{
		objectService.saveDictionary(obj, this.getLoginUser(request.getSession()));
		return ParametersUtil.sendList();
	}

	/**
	 * 删除字典类型
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/del_sys_systemdictionaryType.do")
	public @ResponseBody Object delType(ParametersUtil parameters, HttpServletRequest request) throws Exception{
		objectService.delDictionaryType(parameters, this.getLoginUser(request.getSession()));
		return ParametersUtil.sendList();
	}

	/**
	 * 删除字典
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/del_sys_systemdictionary.do")
	public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
		objectService.delDictionary(parameters, this.getLoginUser(request.getSession()));
		return ParametersUtil.sendList();
	}

	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/load_sys_systemdictionaryById.do")
	public @ResponseBody Object loadTypeByid(ParametersUtil parameters) throws Exception{
		return ParametersUtil.sendList(objectService.getObjectById(parameters));
	}
}