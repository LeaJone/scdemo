package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.fsd.core.bean.ImportExcel;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fsd.admin.model.Z_ckq;
import com.fsd.admin.service.Z_ckqService;
import com.fsd.core.util.ParametersUtil;

@Controller
@RequestMapping("/admin/z_ckq")
public class Z_ckqController extends AdminController{
	
	private static final Logger log = Logger.getLogger(Z_ckqController.class);
    
    @Resource(name = "z_ckqServiceImpl")
	private Z_ckqService objectService;
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_ckq_qx_ckqbj.do")
    public @ResponseBody Object saveObject(Z_ckq obj, HttpServletRequest request) throws Exception{
    	objectService.save(obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_ckq_qx_ckqsc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_ckqbyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
    
    /**
     * 导入Excel
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/import_ckq_qx_ckqdr.do")
    public @ResponseBody Object importObject(ImportExcel obj, HttpServletRequest request) throws Exception{
    	objectService.saveImportObject(obj, this.getLoginUser(request.getSession()), request.getSession().getServletContext().getRealPath("/"));
    	return ParametersUtil.sendList();
    }
    
}