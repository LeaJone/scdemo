package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.I_bankinfo;
import com.fsd.admin.service.I_bankinfoService;

@Controller
@RequestMapping("/admin/i_bankinfo")
public class I_bankinfoController extends AdminController{
	
	private static final Logger log = Logger.getLogger(I_bankinfoController.class);
    
    @Resource(name = "i_bankinfoServiceImpl")
	private I_bankinfoService objectService;
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 异步加载栏目树
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_AsyncObjectTree.do")
    public @ResponseBody Object getAsyncObjectTree(String node, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getAsyncObjectTree(node, this.getLoginUser(request.getSession())));
    }
    
    /**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping({
    	"/save_i_bankinfo_qx_zskflbj.do", 
    	"/save_i_bankinfo_qx_tmkflbj.do", 
    	"/save_i_bankinfo_qx_tjkflbj.do", 
    	"/save_i_bankinfo_qx_wjkflbj.do", 
    	"/save_i_bankinfo_qx_tpkflbj.do"})
    public @ResponseBody Object saveObject(I_bankinfo obj, HttpServletRequest request) throws Exception{
    	objectService.save(obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping({
    	"/del_i_bankinfo_qx_zskflsc.do", 
    	"/del_i_bankinfo_qx_tmkflsc.do", 
    	"/del_i_bankinfo_qx_tjkflsc.do", 
    	"/del_i_bankinfo_qx_wjkflsc.do", 
    	"/del_i_bankinfo_qx_tpkflsc.do"})
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_i_bankinfobyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
    
    /**
     * 顺序排序
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/sort_i_bankinfo.do")
    public @ResponseBody Object sortObject(ParametersUtil parameters) throws Exception{
    	objectService.updateSortObject(parameters);
    	return ParametersUtil.sendList(null);
    }
    
}