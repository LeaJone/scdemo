package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.z_major;
import com.fsd.admin.service.z_majorService;

@Controller
@RequestMapping("/admin/z_major")
public class z_majorController extends AdminController{
	
	private static final Logger log = Logger.getLogger(z_majorController.class);
    
    @Resource(name = "z_majorServiceImpl")
	private z_majorService objectService;

    /**
     * 验证加载权限栏目
     * @author lw
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/get_QxMajorCheck.do")
    public @ResponseBody Object getQxObjectCheck(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendTreeList(objectService.getQxObjectCheck(parameters, this.getLoginUser(request.getSession()), this.getLoginUserPopedom(request.getSession())));
    }
    
    /**
     * 加载权限栏目
     * @author lw
     * @return
     * @throws Exception
     */
    @RequestMapping({
    	"/get_AllQxMajorCheck.do", 
    	"/get_AllQxMajorWapCheck.do"})
    public @ResponseBody Object getAllQxObjectCheck(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendTreeList(objectService.getAllQxObjectCheck(parameters, this.getLoginUser(request.getSession())));
    }
    
    /**
     * 加载数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_data.do")
    public @ResponseBody Object loadData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 异步加载栏目树
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_AsyncMajorTree.do")
    public @ResponseBody Object getAsyncObjectTree(String node, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getAsyncObjectTree(node, false, this.getLoginUser(request.getSession())));
    }
    /**
     * 异步加载栏目树，查询使用
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_AsyncMajorTreeQuery.do")
    public @ResponseBody Object getAsyncObjectTreeQuery(String node, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getAsyncObjectTree(node, true, this.getLoginUser(request.getSession())));
    }
    
    /**
     * 异步加载栏目树
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_AsyncMajorTreeByPopdom.do")
    public @ResponseBody Object getAsyncObjectTreeByPopdom(String node, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getAsyncObjectTreeByPopdom(node, false, 
    			this.getLoginUser(request.getSession()), this.getLoginUserPopedom(request.getSession())));
    }
    /**
     * 异步加载栏目树，查询使用
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_AsyncMajorTreeByPopdomQuery.do")
    public @ResponseBody Object getAsyncObjectTreeByPopdomQuery(String node, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getAsyncObjectTreeByPopdom(node, true, 
    			this.getLoginUser(request.getSession()), this.getLoginUserPopedom(request.getSession())));
    }
    
    /**
     * 一次性加载机构树
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_AllMajorTreeByPopdom.do")
    public @ResponseBody Object getAllObjectTreeByPopdom(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendTreeList(objectService.getAllObjectTreeByPopdom(parameters, false, this.getLoginInfo(request.getSession())));
    }
    /**
     * 一次性加载机构树，查询使用
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_AllMajorTreeByPopdomQuery.do")
    public @ResponseBody Object getAllObjectTreeByPopdomQuery(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendTreeList(objectService.getAllObjectTreeByPopdom(parameters, true, this.getLoginInfo(request.getSession())));
    }
    
    /**
     * 查询窗口表格显示
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_AllMajorListByPopdom.do")
    public @ResponseBody Object getAllObjectListByPopdom(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getAllObjectListByPopdom(parameters, false, this.getLoginInfo(request.getSession())));
    }
    
    /**
     * 保存栏目
     * @param obj
     * @return
     * @throws Exception
     */
    @RequestMapping({
    	"/save_Major_qx_lmbj.do", 
    	"/save_Major_qx_xglmbj.do", 
    	"/save_Major_qx_waplmbj.do", 
    	"/save_Major_qx_wapxglmbj.do", 
    	"/save_Major_qx_zwlmbj.do"})
    public @ResponseBody Object saveObject(z_major obj , HttpServletRequest request) throws Exception{
    	objectService.saveObject(obj , this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_MajorByid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
    
    /**
     * 删除栏目
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping({
    	"/del_Major_qx_lmsc.do", 
    	"/del_Major_qx_xglmsc.do", 
    	"/del_Major_qx_waplmsc.do", 
    	"/del_Major_qx_wapxglmsc.do", 
    	"/del_Major_qx_zwlmsc.do"})
    public @ResponseBody Object delObject(ParametersUtil parameters) throws Exception{
    	objectService.delObject(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 查询统计数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/stat_data.do")
    public @ResponseBody Object statData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(
    			objectService.getObjectStat(parameters, this.getLoginUser(request.getSession())));
    }
    
    /**
     * 顺序排序
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping({
    	"/sort_Major_qx_lmbj.do", 
    	"/sort_Major_qx_xglmbj.do", 
    	"/sort_Major_qx_waplmbj.do",
    	"/sort_Major_qx_wapxglmbj.do",  
    	"/sort_Major_qx_zwlmbj.do"})
    public @ResponseBody Object sortObject(ParametersUtil parameters) throws Exception{
    	objectService.updateSortObject(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 修改状态
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping({
    	"/enabled_Major_qx_lmbj.do", 
    	"/enabled_Major_qx_xglmbj.do", 
    	"/enabled_Major_qx_waplmbj.do",
    	"/enabled_Major_qx_wapxglmbj.do",  
    	"/enabled_Major_qx_zwlmbj.do"})
    public @ResponseBody Object enabledObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.updateEnabledObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 公开栏目
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/gklm_Major_qx_lmbj.do")
    public @ResponseBody Object slideObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.updateGklmObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 栏目文章关联查询
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/gllmcx_Major.do")
    public @ResponseBody Object getObjectRelateArticleNum(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectRelateArticleNum(parameters, this.getLoginUser(request.getSession())).sendObject();
    }
    
    /**
     * 栏目文章及关联文章迁移
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/gllmqy_Major_qx_lmbj.do")
    public @ResponseBody Object moveObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.updateObjectMoveRelateArticle(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * Excel导出
     * @param parameters
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/download.do")
    public @ResponseBody Object download(ParametersUtil parameters,HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.download(parameters, this.getLoginUser(request.getSession()), this.getRootPaht(request.getSession()), this.getHttpRootPath(request)));
    }
}