package com.fsd.admin.controller;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.admin.model.T_coursetype;
import com.fsd.admin.service.T_coursetypeService;
import com.fsd.core.util.ParametersUtil;

/**
 * 课程分类Controller
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/admin/t_coursetype")
public class T_coursetypeController extends AdminController{
	
	private static final Logger log = Logger.getLogger(T_coursetypeController.class);
    
    @Autowired
	private T_coursetypeService objectService;
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 异步加载对象树
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_AsyncAreaTree.do")
    public @ResponseBody Object getAsyncObjectTree(String node) throws Exception{
    	return ParametersUtil.sendList(objectService.getAsyncObjectTree(node));
    }
    
    /**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_t_coursetype_qx_t_coursetypebj.do")
    public @ResponseBody Object saveObject(T_coursetype obj, HttpServletRequest request) throws Exception{
    	objectService.save(obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_t_coursetype_qx_t_coursetypesc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_t_coursetypebyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
    
    /**
     * 修改启用状态
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/enabled_coursetype_qx_coursetypebj.do")
    public @ResponseBody Object enabledObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.updateEnabledObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
}