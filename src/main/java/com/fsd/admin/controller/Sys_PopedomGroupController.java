package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.Sys_PopedomGroup;
import com.fsd.admin.service.Sys_PopedomGroupService;

@Controller
@RequestMapping("/admin/Sys_PopedomGroup")
public class Sys_PopedomGroupController extends AdminController{
	
	private static final Logger log = Logger.getLogger(Sys_PopedomGroupController.class);
    
    @Resource(name = "Sys_PopedomGroupServiceImpl")
	private Sys_PopedomGroupService objectService;
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 加载所有数据
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_data.do")
    public @ResponseBody Object loadData(HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectList(this.getLoginUser(request.getSession())));
    }
    
    /**
     * 加载所有数据
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_datauser.do")
    public @ResponseBody Object loadDataEmployee(HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectListEmployee(this.getLoginUser(request.getSession())));
    }
    
    /**
     * 保存对象
     * @param Object
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_Popedomgroup_qx_jsbj.do")
    public @ResponseBody Object saveObject(Sys_PopedomGroup Object, HttpServletRequest request) throws Exception{
    	objectService.saveObject(Object, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_PopedomgroupByid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_Popedomgroup_qx_jssc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters) throws Exception{
    	objectService.delObject(parameters);
    	return ParametersUtil.sendList(null);
    }
}