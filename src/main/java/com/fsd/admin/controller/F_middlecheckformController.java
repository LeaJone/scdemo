package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.admin.model.F_middlecheckform;
import com.fsd.admin.service.F_middlecheckformService;
import com.fsd.core.util.ParametersUtil;

/**
 * 项目中期检查计划书- controller
 * @author lumingbao
 */
@Controller
@RequestMapping("/admin/f_middlecheckform")
public class F_middlecheckformController extends AdminController {
    
    @Resource(name = "f_middlecheckformServiceImpl")
	private F_middlecheckformService objectService;
    
    /**
     * 提交中期检查
     * @param obj
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_middlecheckform.do")
    public @ResponseBody Object saveObject(F_middlecheckform obj, HttpServletRequest request) throws Exception{
    	objectService.saveMiddlecheckform(obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }

    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_MiddlecheckformById.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
        return ParametersUtil.sendList(objectService.getMiddlecheckformById(parameters));
    }

    /**
     * 根据项目ID加载中期检查信息
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_MiddlecheckformByProjectId.do")
    public @ResponseBody Object loadObjectByProjectid(ParametersUtil parameters) throws Exception{
        return ParametersUtil.sendList(objectService.saveOrgetMiddlecheckformByProjectId(parameters));
    }
}
