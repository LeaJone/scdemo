package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fsd.admin.model.F_prospectus;
import com.fsd.admin.model.Z_email;
import com.fsd.admin.service.F_prospectusService;
import com.fsd.core.util.ParametersUtil;

/**
 * 项目计划任务书 - controller
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/admin/f_prospectus")
public class F_prospectusController extends AdminController {

	private static final Logger log = Logger.getLogger(F_prospectusController.class);
    
    @Resource(name = "f_prospectusServiceImpl")
	private F_prospectusService objectService;
    
    /**
     * 保存执行计划书
     * @param obj
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_prospectus_first.do")
    public @ResponseBody Object saveObjectFirst(F_prospectus obj, HttpServletRequest request) throws Exception{
    	objectService.saveProspectusFirst(obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 提交执行计划书
     * @param obj
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_prospectus.do")
    public @ResponseBody Object saveObject(F_prospectus obj, HttpServletRequest request) throws Exception{
    	objectService.saveProspectus(obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 加载执行计划书
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_PageData.do")
    public @ResponseBody Object loadProspectus(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getProspectusPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 删除执行计划任务书
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/deleted_prospectus.do")
    public @ResponseBody Object deletedObject(ParametersUtil parameters) throws Exception{
    	objectService.deletedProspectus(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_ProspectusById.do")
    public @ResponseBody Object loadProspectusByid(ParametersUtil parameters) throws Exception{
    	return objectService.getProspectusById(parameters).sendObject();
    }
 
}
