package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.admin.model.E_Menu;
import com.fsd.admin.service.E_MenuService;
import com.fsd.core.util.ParametersUtil;

/**
 * 微信菜单Controller
 * @author Administrator
 *
 */

@Controller
@RequestMapping("/admin/e_menu")
public class E_MenuController extends AdminController {
	
	private static final Logger log = Logger.getLogger(E_MenuController.class);
	
	@Resource(name = "e_menuServiceImpl")
	private E_MenuService objectService;
	
	/**
     * 加载数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_data.do")
    public @ResponseBody Object loadData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 异步加载对象树
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_AsyncMenuTree.do")
    public @ResponseBody Object getAsyncObjectTree(String node, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getAsyncObjectTree(node, this.getLoginUser(request.getSession())));
    }
    
    /**
     * 一次性加载机构树
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_AllMenuTree.do")
    public @ResponseBody Object getAllObjectTree(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendTreeList(objectService.getAllObjectTree(parameters, this.getLoginInfo(request.getSession())));
    }
    
    /**
     * 保存对象
     * @param obj
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_Menu_qx_wxcdbj.do")
    public @ResponseBody Object saveObject(E_Menu obj , HttpServletRequest request) throws Exception{
    	objectService.saveObject(obj , this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_MenuByid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_Menu_qx_wxcdsc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters) throws Exception{
    	objectService.delObject(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 顺序排序
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/sort_Menu_qx_wxcdbj.do")
    public @ResponseBody Object sortObject(ParametersUtil parameters) throws Exception{
    	objectService.updateSortObject(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    
    /**
     * 加载微信平台菜单
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_WeChatMenu.do")
    public @ResponseBody Object getWeChatMenu(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendTreeList(objectService.getWeChatMenu(parameters));
    }
    
    /**
     * 同步微信平台菜单
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/synchro_WeChatMenu.do")
    public @ResponseBody Object synchroUserMenu(ParametersUtil parameters) throws Exception{
    	objectService.synchroUserMenu(parameters);
    	return ParametersUtil.sendList(null);
    }
}
