package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.Config;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.B_article;
import com.fsd.admin.service.B_wordkeybadService;
import com.fsd.admin.service.Z_ldxxglService;

@Controller
@RequestMapping("/admin/z_ldxxgl")
public class Z_ldxxglController extends AdminController{
	
	private static final Logger log = Logger.getLogger(Z_ldxxglController.class);
    
    @Resource(name = "z_ldxxglServiceImpl")
	private Z_ldxxglService objectService;
    
    @Resource(name = "b_wordkeybadServiceImpl")
	private B_wordkeybadService wordService;
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters,this.getLoginUser(request.getSession()));
    }
    
    /**
     * 编辑对象
     * @param obj
     * @param parameters
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_Lsxx.do")
    public @ResponseBody Object saveArticle(B_article obj, String fznr, ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	wordService.checkTextByWordType(Config.WORDTYPEBAD, null, obj.getContent(), this.getLoginUser(request.getSession()));
    	ParametersUtil util = objectService.save(obj, fznr, parameters, this.getLoginUser(request.getSession()));
    	return util.sendObject();
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_ldxx.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_ldxxbyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return objectService.getObjectById(parameters).sendObject();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/saveLoad_ldxxbyid.do")
    public @ResponseBody Object saveLoadObjectByid(ParametersUtil parameters) throws Exception{
    	return objectService.saveGetObjectById(parameters).sendObject();
    }
    
    /**
     * 修改对象状态
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/enabled_ldxx.do")
    public @ResponseBody Object enabledObject(ParametersUtil parameters) throws Exception{
    	objectService.updateEnabledObject(parameters);
    	return ParametersUtil.sendList(null);
    }
}