package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.service.H_relationService;

@Controller
@RequestMapping("/admin/h_relation")
public class H_relationController extends AdminController{
	
	private static final Logger log = Logger.getLogger(H_relationController.class);
    
    @Resource(name = "h_relationServiceImpl")
	private H_relationService objectService;

    
    /**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_qx_hyzcglzc.do")
    public @ResponseBody Object saveObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.saveObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/savelist_qx_hyzcglzc.do")
    public @ResponseBody Object saveObjectList(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.saveObjectList(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_qx_hyzcglzx.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
}