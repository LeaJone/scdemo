package com.fsd.admin.controller;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.Z_team;
import com.fsd.admin.service.Z_teamService;

@Controller
@RequestMapping("/admin/z_team")
public class Z_teamController extends AdminController{
	
	private static final Logger log = Logger.getLogger(Z_teamController.class);
    
    @Autowired
	private Z_teamService objectService;
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 加载列表数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_listdata.do")
    public @ResponseBody Object loadListData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getTeamList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_z_team_qx_z_teambj.do")
    public @ResponseBody Object saveObject(Z_team obj, HttpServletRequest request) throws Exception{
    	objectService.save(obj, this.getLoginUser(request.getSession()));
        return ParametersUtil.sendList();
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_z_team_qx_z_teamsc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_z_teambyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
    
}