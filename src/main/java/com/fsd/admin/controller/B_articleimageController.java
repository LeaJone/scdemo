package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.B_articleimage;
import com.fsd.admin.service.B_articleimageService;

@Controller
@RequestMapping("/admin/b_articleimage")
public class B_articleimageController extends AdminController{
	
	private static final Logger log = Logger.getLogger(B_articleimageController.class);
    
    @Resource(name = "b_articleimageServiceImpl")
	private B_articleimageService objectService;
    
    /**
     * 根据文章ID加载数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_DataByArticleId.do")
    public @ResponseBody Object loadObjectByFid(ParametersUtil parameters , HttpServletRequest request) throws Exception{
    	return objectService.getObjectListByArticleId(parameters , this.getLoginUser(request.getSession()));
    }
    
    /**
     * 保存组图
     * @param obj
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_articleimage_qx_nrztbj.do")
    public @ResponseBody Object saveObject(B_articleimage obj , HttpServletRequest request) throws Exception{
    	objectService.saveObject(obj , this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_ObjectByid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_articleimage_qx_nrztbj.do")
    public @ResponseBody Object delObject(ParametersUtil parameters) throws Exception{
    	objectService.delObject(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 修改状态
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/enabled_articleimage_qx_nrztbj.do")
    public @ResponseBody Object enabledObject(ParametersUtil parameters) throws Exception{
    	objectService.updateEnabledObject(parameters);
    	return ParametersUtil.sendList(null);
    }
}