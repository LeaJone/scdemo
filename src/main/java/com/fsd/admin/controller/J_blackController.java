package com.fsd.admin.controller;

import com.fsd.admin.service.J_blackService;
import com.fsd.core.util.ParametersUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/admin/j_black")
public class J_blackController extends AdminController{
    
	@Autowired
	private J_blackService objectService;
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_j_blackbyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }

    /**
     * 帖子禁言（包括历史消息）
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/shutup_j_black_qx_lttzjyyes.do")
    public @ResponseBody Object saveObjectByYes(ParametersUtil parameters) throws Exception{
        objectService.saveObject(parameters, "true");
        return ParametersUtil.sendList();
    }

    /**
     * 帖子禁言（只禁言发帖跟回复）
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/shutup_j_black_qx_lttzjyno.do")
    public @ResponseBody Object saveObjectByNo(ParametersUtil parameters) throws Exception{
        objectService.saveObject(parameters, "false");
        return ParametersUtil.sendList();
    }

    /**
     * 回复禁言（包括历史消息）
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/shutup_j_black_qx_lthfjyyes.do")
    public @ResponseBody Object saveObjectHfByYes(ParametersUtil parameters) throws Exception{
        objectService.saveHfObject(parameters, "true");
        return ParametersUtil.sendList();
    }

    /**
     * 回复禁言（只禁言发帖跟回复）
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/shutup_j_black_qx_lthfjyno.do")
    public @ResponseBody Object saveObjectHfByNo(ParametersUtil parameters) throws Exception{
        objectService.saveHfObject(parameters, "false");
        return ParametersUtil.sendList();
    }

    /**
     * 解除禁言（包括历史消息）
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/shutup_j_black_qx_ltjyjcyes.do")
    public @ResponseBody Object delObjectByYes(ParametersUtil parameters) throws Exception{
        objectService.delObject(parameters, "true");
        return ParametersUtil.sendList();
    }

    /**
     * 解除禁言（只解除发帖跟回复权限）
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/shutup_j_black_qx_ltjyjcno.do")
    public @ResponseBody Object delObjectByNo(ParametersUtil parameters) throws Exception{
        objectService.delObject(parameters, "false");
        return ParametersUtil.sendList();
    }
    
}