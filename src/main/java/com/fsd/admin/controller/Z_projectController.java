package com.fsd.admin.controller;

import javax.servlet.http.HttpServletRequest;

import com.fsd.core.util.HtmlToPdf;
import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.Z_project;
import com.fsd.admin.service.Z_projectService;

@Controller
@RequestMapping("/admin/z_project")
public class Z_projectController extends AdminController{
    
    @Autowired
	private Z_projectService objectService;
    
    /**
     * 加载申报分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_applypagedata.do")
    public @ResponseBody Object loadApplyPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectApplyPageList(parameters, this.getLoginUser(request.getSession()));
    }

    /**
     * 中期检查分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_middlepagedata.do")
    public @ResponseBody Object loadMiddlePageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
        return objectService.getObjectMiddlePageList(parameters, this.getLoginUser(request.getSession()));
    }

    /**
     * 加载申报分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
        return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedataprospectus.do")
    public @ResponseBody Object loadPageDataProspectus(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageListProspectus(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_project_qx_projectbj.do")
    public @ResponseBody Object saveObject(ParametersUtil parameters, Z_project obj, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.save(this.getRootPaht(request.getSession()), parameters, obj, this.getLoginUser(request.getSession())));
    }

    /**
     * 提交项目基本信息
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_project_info.do")
    public @ResponseBody Object saveProjectInfo(ParametersUtil parameters, Z_project obj, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.saveInfo(parameters, obj, this.getLoginUser(request.getSession())));
    }

    /**
     * 提交项目负责人信息
     * @param parameters
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_project_leader.do")
    public @ResponseBody Object saveProjectLeader(ParametersUtil parameters, Z_project obj, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.saveLeader(parameters, obj, this.getLoginUser(request.getSession())));
    }

    /**
     * 提交第一指导老师
     * @param parameters
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_project_teacher.do")
    public @ResponseBody Object saveProjectTeacher(ParametersUtil parameters, Z_project obj, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.saveTeacher(parameters, obj, this.getLoginUser(request.getSession())));
    }

    /**
     * 提交第二指导老师
     * @param parameters
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_project_teacher2.do")
    public @ResponseBody Object saveProjectTeacher2(ParametersUtil parameters, Z_project obj, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.saveTeacher2(parameters, obj, this.getLoginUser(request.getSession())));
    }

    /**
     * 提交校外第一指导老师
     * @param parameters
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_project_outteacher.do")
    public @ResponseBody Object saveProjectOutTeacher(ParametersUtil parameters, Z_project obj, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.saveOutTeacher(parameters, obj, this.getLoginUser(request.getSession())));
    }

    /**
     * 提交校外第二指导老师
     * @param parameters
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_project_outteacher2.do")
    public @ResponseBody Object saveProjectOutTeacher2(ParametersUtil parameters, Z_project obj, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.saveOutTeacher2(parameters, obj, this.getLoginUser(request.getSession())));
    }

    /**
     * 提交项目简介
     * @param parameters
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_project_abstract.do")
    public @ResponseBody Object saveProjectAbstract(ParametersUtil parameters, Z_project obj, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.saveAbstract(parameters, obj, this.getLoginUser(request.getSession())));
    }

    /**
     * 提交申请理由
     * @param parameters
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_project_gist.do")
    public @ResponseBody Object saveProjectGist(ParametersUtil parameters, Z_project obj, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.saveGist(parameters, obj, this.getLoginUser(request.getSession())));
    }

    /**
     * 提交项目方案
     * @param parameters
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_project_scheme.do")
    public @ResponseBody Object saveProjectScheme(ParametersUtil parameters, Z_project obj, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.saveScheme(parameters, obj, this.getLoginUser(request.getSession())));
    }

    /**
     * 提交特色与创新点
     * @param parameters
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_project_feature.do")
    public @ResponseBody Object saveProjectFeature(ParametersUtil parameters, Z_project obj, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.saveFeature(parameters, obj, this.getLoginUser(request.getSession())));
    }

    /**
     * 项目进度安排
     * @param parameters
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_project_schedule.do")
    public @ResponseBody Object saveProjectSchedule(ParametersUtil parameters, Z_project obj, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.saveSchedule(parameters, obj, this.getLoginUser(request.getSession())));
    }

    /**
     * 提交项目
     * @param parameters
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_project_submit.do")
    public @ResponseBody Object saveProjectSubmit(ParametersUtil parameters, HttpServletRequest request) throws Exception{
        objectService.saveProjectSubmit(this.getRootPaht(request.getSession()), parameters, this.getLoginUser(request.getSession()));
        return ParametersUtil.sendList();
    }

    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_project_qx_projectsc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_z_projectbyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }


    /**
     * 项目立项
     * @param parameters
     * @param request
     * @param projectids
     * @param typeid
     * @param money
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_project_approval.do")
    public @ResponseBody Object saveProjectApproval(ParametersUtil parameters, HttpServletRequest request, String projectids, String typeid, String money, String f_schooltypeid, String f_schooltypename) throws Exception{
        objectService.updateProjectApproval(projectids, typeid, money, f_schooltypeid, f_schooltypename);
        return ParametersUtil.sendList();
    }

    /**
     * 项目中期检查
     * @param parameters
     * @param request
     * @param projectids
     * @param typeid
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_project_middle.do")
    public @ResponseBody Object saveProjectMiddle(ParametersUtil parameters, HttpServletRequest request, String projectids, String typeid, String typename) throws Exception{
        objectService.updateProjectMiddle(projectids, typeid, typename);
        return ParametersUtil.sendList();
    }

    /**
     * 项目结题结果
     * @param parameters
     * @param request
     * @param projectids
     * @param ddmbid
     * @param ddmbname
     * @param cjpdid
     * @param cjpdname
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_project_ending.do")
    public @ResponseBody Object saveProjectEnding(ParametersUtil parameters, HttpServletRequest request, String projectids, String ddmbid, String ddmbname, String cjpdid, String cjpdname) throws Exception{
        objectService.updateProjectEnding(projectids, ddmbid, ddmbname, cjpdid, cjpdname);
        return ParametersUtil.sendList();
    }

    /**
     * 更新项目路演分数
     * @param parameters
     * @param request
     * @param projectid
     * @param f_score
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/update_luyanscore.do")
    public @ResponseBody Object updateLuyanScore(ParametersUtil parameters, HttpServletRequest request, String projectid, String f_score) throws Exception{
        objectService.updateLuyanScore(projectid, f_score);
        return ParametersUtil.sendList();
    }

    /**
     * 导入项目路演分数
     * @param parameters
     * @param request
     * @param f_url
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/import_luyanscore.do")
    public @ResponseBody Object importLuyanScore(ParametersUtil parameters, HttpServletRequest request, String f_url) throws Exception{
        objectService.updateImportLuyanScore(this.getRootPaht(request.getSession()), f_url);
        return ParametersUtil.sendList();
    }

    /**
     * 下载项目申报书
     * @param request
     * @param projectid
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/download_projectApplyBook.do")
    public @ResponseBody Object downloadProjectApplyBook(HttpServletRequest request, String projectid) throws Exception{
        String url = this.getHttpRootPath(request);
        String rootPaht = this.getRootPaht(request.getSession());
        String filePdfPath = rootPaht + "uploadfiles/projectfile/"+ projectid +".pdf";
        HtmlToPdf.convert(url + "admin/dcxm_sbs-"+ projectid +".htm", filePdfPath);
        return ParametersUtil.sendList("uploadfiles/projectfile/"+ projectid +".pdf");
    }

    /**
     * 下载项目中期检查表
     * @param request
     * @param projectid
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/download_projectMiddleBook.do")
    public @ResponseBody Object downloadProjectMiddleBook(HttpServletRequest request, String projectid) throws Exception{
        String url = this.getHttpRootPath(request);
        String rootPaht = this.getRootPaht(request.getSession());
        String filePdfPath = rootPaht + "uploadfiles/projectfile/"+ projectid +".pdf";
        HtmlToPdf.convert(url + "admin/dcxm_zqjc-"+ projectid +".htm", filePdfPath);
        return ParametersUtil.sendList("uploadfiles/projectfile/"+ projectid +".pdf");
    }


    /**
     * 下载项目结项书
     * @param request
     * @param projectid
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/download_projectEndingBook.do")
    public @ResponseBody Object downloadProjectEndingBook(HttpServletRequest request, String projectid) throws Exception{
        String url = this.getHttpRootPath(request);
        String rootPaht = this.getRootPaht(request.getSession());
        String filePdfPath = rootPaht + "uploadfiles/projectfile/"+ projectid +".pdf";
        HtmlToPdf.convert(url + "admin/dcxm_jxs-"+ projectid +".htm", filePdfPath);
        return ParametersUtil.sendList("uploadfiles/projectfile/"+ projectid +".pdf");
    }

    /**
     * 下载项目变更申请表
     * @param request
     * @param projectid
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/download_projectModifyBook.do")
    public @ResponseBody Object downloadProjectModifyBook(HttpServletRequest request, String id, String projectid) throws Exception{
        String url = this.getHttpRootPath(request);
        String rootPaht = this.getRootPaht(request.getSession());
        String filePdfPath = rootPaht + "uploadfiles/projectfile/"+ projectid +".pdf";
        HtmlToPdf.convert(url + "admin/dcxm_xmbg-"+ projectid +"-"+ id +".htm", filePdfPath);
        return ParametersUtil.sendList("uploadfiles/projectfile/"+ projectid +".pdf");
    }

    /**
     * 根据项目ID加载项目当前审核节点信息
     * @param request
     * @param projectid
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/get_projectAuditStep.do")
    public @ResponseBody Object getProjectAuditStep(HttpServletRequest request, String projectid) throws Exception{
        return ParametersUtil.sendList(objectService.getProjectAuditStep(projectid));
    }
}