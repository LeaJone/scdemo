package com.fsd.admin.controller;

import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.fsd.admin.service.B_opennumberService;

@Controller
@RequestMapping("/admin/b_opennumber")
public class B_opennumberController extends AdminController{
	
	private static final Logger log = Logger.getLogger(B_opennumberController.class);
    
    @Resource(name = "b_opennumberServiceImpl")
	private B_opennumberService objectService;
    
}