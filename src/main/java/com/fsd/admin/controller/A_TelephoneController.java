package com.fsd.admin.controller;

import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.fsd.admin.service.A_TelephoneService;

@Controller
@RequestMapping("/admin/A_Telephone")
public class A_TelephoneController extends AdminController{
	
	private static final Logger log = Logger.getLogger(A_TelephoneController.class);
    
    @Resource(name = "A_TelephoneServiceImpl")
	private A_TelephoneService objectService;
    
}