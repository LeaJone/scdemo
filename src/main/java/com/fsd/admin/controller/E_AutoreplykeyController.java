package com.fsd.admin.controller;

import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.fsd.admin.service.E_AutoreplykeyService;

@Controller
@RequestMapping("/admin/e_autoreplykey")
public class E_AutoreplykeyController extends AdminController {

	private static final Logger log = Logger.getLogger(E_AutoreplykeyController.class);
    
    @Resource(name = "e_autoreplykeyServiceImpl")
	private E_AutoreplykeyService objectService;
}
