package com.fsd.admin.controller;

import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.controller.BaseController;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.service.Sys_PopedomAllocateService;

@Controller
@RequestMapping("/admin/Sys_PopedomAllocate")
public class Sys_PopedomAllocateController extends BaseController{
	
	private static final Logger log = Logger.getLogger(Sys_PopedomAllocateController.class);
    
    @Resource(name = "Sys_PopedomAllocateServiceImpl")
	private Sys_PopedomAllocateService objectService;
    
    
    /**
     * 保存角色权限
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_RolePopdom.do")
    public @ResponseBody Object saveRolePopedom(ParametersUtil parameters) throws Exception{
    	objectService.saveRolePopedom(parameters);
    	return ParametersUtil.sendList();
    }
    
    /**
     * 保存人员权限
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_UserPopdom.do")
    public @ResponseBody Object saveUserPopedom(ParametersUtil parameters) throws Exception{
    	objectService.saveUserPopedom(parameters);
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据人员ID集合获取所属角色ID集合
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/get_RoleIdsByUserIds.do")
    public @ResponseBody Object getRoleIdsByUserIds(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getRoleIdsByUserIds(parameters));
    }
    
    /**
     * 根据角色ID集合获取所属权限ID集合
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/get_PopedomIdsByRoleIds.do")
    public @ResponseBody Object getPopedomIdsByRoleIds(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getPopedomIdsByRoleIds(parameters));
    }
}