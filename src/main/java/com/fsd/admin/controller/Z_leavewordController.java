package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fsd.admin.model.B_leaveword;
import com.fsd.admin.service.B_leavewordService;
import com.fsd.core.util.ParametersUtil;

@Controller
@RequestMapping("/admin/z_leaveword")
public class Z_leavewordController extends AdminController{
	
	private static final Logger log = Logger.getLogger(Z_leavewordController.class);
    
    @Resource(name = "b_leavewordServiceImpl")
	private B_leavewordService objectService;
        
    /**
     * 保存对象
     * @param Object
     * @return
     * @throws Exception
     */
    @RequestMapping({
    	"/save_dzxx_qx_myzjhf.do", 
    	"/save_dzxx_qx_zxzxhf.do", 
    	"/save_dzxx_qx_xfxxhf.do", 
    	"/save_dzxx_qx_xcxxhf.do", 
    	"/save_dzxx_qx_tdwfjbhf.do", 
    	"/save_dzxx_qx_kcwfjbhf.do", 
    	"/save_dzxx_qx_ldxxhf.do",  
    	"/save_dzxx_qx_gkyjhf.do",
    	"/save_dzxx_qx_slwthf.do",
    	"/save_dzxx_qx_qzyjxhf.do"})
    public @ResponseBody Object saveObject(B_leaveword Object, HttpServletRequest request) throws Exception{
    	objectService.saveObject(Object, this.getLoginInfo(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping({
    	"/del_dzxx_qx_myzjsc.do", 
    	"/del_dzxx_qx_zxzxsc.do", 
    	"/del_dzxx_qx_xfxxsc.do", 
    	"/del_dzxx_qx_xcxxsc.do", 
    	"/del_dzxx_qx_tdwfjbsc.do", 
    	"/del_dzxx_qx_kcwfjbsc.do", 
    	"/del_dzxx_qx_ldxxsc.do",  
    	"/del_dzxx_qx_gkyjsc.do",  
    	"/del_dzxx_qx_slwtsc.do",  
    	"/del_dzxx_qx_qzyjxsc.do"})
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 审核
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping({
    	"/audit_dzxx_qx_myzjsh.do", 
    	"/audit_dzxx_qx_zxzxsh.do", 
    	"/audit_dzxx_qx_xfxxsh.do", 
    	"/audit_dzxx_qx_xcxxsh.do", 
    	"/audit_dzxx_qx_tdwfjbsh.do", 
    	"/audit_dzxx_qx_kcwfjbsh.do", 
    	"/audit_dzxx_qx_ldxxsh.do",  
    	"/audit_dzxx_qx_gkyjsh.do",  
    	"/audit_dzxx_qx_slwtsh.do",  
    	"/audit_dzxx_qx_qzyjxsh.do"})
    public @ResponseBody Object auditObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.auditObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 选登
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping({
    	"/choose_dzxx_qx_myzjxd.do", 
    	"/choose_dzxx_qx_zxzxxd.do", 
    	"/choose_dzxx_qx_xfxxxd.do", 
    	"/choose_dzxx_qx_xcxxxd.do", 
    	"/choose_dzxx_qx_tdwfjbxd.do", 
    	"/choose_dzxx_qx_kcwfjbxd.do", 
    	"/choose_dzxx_qx_ldxxxd.do",  
    	"/choose_dzxx_qx_gkyjxd.do",  
    	"/choose_dzxx_qx_slwtxd.do",  
    	"/choose_dzxx_qx_qzyjxxd.do"})
    public @ResponseBody Object chooseObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.chooseObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 变更回复人
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/hfrbg_dzxx_qx_dzxxbgr.do")
    public @ResponseBody Object hfrbgObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.hfrbgObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 敏感标志
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/audit_LeaveWordMgbz.do")
    public @ResponseBody Object flageObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.flageObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
}