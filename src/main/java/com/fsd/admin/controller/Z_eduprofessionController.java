package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.Z_edumuke;
import com.fsd.admin.model.Z_eduprofession;
import com.fsd.admin.service.Z_eduprofessionService;

@Controller
@RequestMapping("/admin/z_eduprofession")
public class Z_eduprofessionController extends AdminController{
	
	private static final Logger log = Logger.getLogger(Z_eduprofessionController.class);
    
    @Resource(name = "z_eduprofessionServiceImpl")
	private Z_eduprofessionService objectService;
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_z_eduprofession_qx_z_eduprofessionbj.do")
    public @ResponseBody Object saveObject(Z_eduprofession obj, HttpServletRequest request) throws Exception{
    	objectService.save(this.getRootPaht(request.getSession()),obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_z_eduprofession_qx_z_eduprofessionsc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_z_eduprofessionbyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
    
    /**
	 * 上传申请文件
	 * @param obj
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/eduprofession_fileupload.do")
	public @ResponseBody Object uploadUrl(Z_eduprofession obj, HttpServletRequest request) throws Exception{
		objectService.uploadFile(obj, this.getLoginUser(request.getSession()));
		return ParametersUtil.sendList();
	}
}