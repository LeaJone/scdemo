package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fsd.admin.service.Sys_SystemLogService;
import com.fsd.core.util.ParametersUtil;

@Controller
@RequestMapping("/admin/Sys_SystemLog")
public class Sys_SystemLogController extends AdminController{
	
	private static final Logger log = Logger.getLogger(Sys_SystemLogController.class);
    
    @Resource(name = "Sys_SystemLogServiceImpl")
	private Sys_SystemLogService objectService;
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    /**
     * 加载分页数据，普通操作
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedatacz.do")
    public @ResponseBody Object loadPageDataCZ(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageListCZ(parameters, this.getLoginUser(request.getSession()));
    }
    /**
     * 加载分页数据，安全操作
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedataaq.do")
    public @ResponseBody Object loadPageDataAQ(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageListAQ(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_SystemLogByid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }

    /**
     * Excel导出
     * @param parameters
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/download_log.do")
    public @ResponseBody Object download(ParametersUtil parameters, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.download(parameters, this.getLoginUser(request.getSession()), this.getRootPaht(request.getSession()), this.getHttpRootPath(request)));
    }
    
}