package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.I_knowledgeinfo;
import com.fsd.admin.service.I_knowledgeinfoService;

@Controller
@RequestMapping("/admin/i_knowledgeinfo")
public class I_knowledgeinfoController extends AdminController{
	
	private static final Logger log = Logger.getLogger(I_knowledgeinfoController.class);
    
    @Resource(name = "i_knowledgeinfoServiceImpl")
	private I_knowledgeinfoService objectService;
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_i_knowledgeinfo_qx_i_knowledgeinfobj.do")
    public @ResponseBody Object saveObject(I_knowledgeinfo obj, HttpServletRequest request) throws Exception{
    	return objectService.save(obj, this.getLoginUser(request.getSession())).sendObject();
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_i_knowledgeinfo_qx_i_knowledgeinfosc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_i_knowledgeinfobyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
    
    /**
     * 修改状态
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/enabled_qdnrb_qx.do")
    public @ResponseBody Object enabledObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.updateEnabledObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 顺序排序
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/sort_i_knowledgeinfo.do")
    public @ResponseBody Object sortObject(ParametersUtil parameters) throws Exception{
    	objectService.updateSortObject(parameters);
    	return ParametersUtil.sendList(null);
    }
    
}