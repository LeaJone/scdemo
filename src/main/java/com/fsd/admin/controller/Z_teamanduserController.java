package com.fsd.admin.controller;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.Z_teamanduser;
import com.fsd.admin.service.Z_teamanduserService;

@Controller
@RequestMapping("/admin/z_teamanduser")
public class Z_teamanduserController extends AdminController{
	
	private static final Logger log = Logger.getLogger(Z_teamanduserController.class);
    
    @Autowired
	private Z_teamanduserService objectService;
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_z_teamanduser_qx_z_teamanduserbj.do")
    public @ResponseBody Object saveObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	ParametersUtil util = objectService.save(parameters, this.getLoginUser(request.getSession()));
    	return util.sendObject();
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_z_teamanduser_qx_z_teamandusersc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_z_teamanduserbyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
    
    /**
     * 加载列表数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_listdata.do")
    public @ResponseBody Object loadlistData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getTeamanduserList(parameters, this.getLoginUser(request.getSession()));
    }

    /**
     * 加载列表数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_teamuserbyid.do")
    public @ResponseBody Object loadteambyidData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
        return objectService.getTeamById(parameters, this.getLoginUser(request.getSession()));
    }
    
}