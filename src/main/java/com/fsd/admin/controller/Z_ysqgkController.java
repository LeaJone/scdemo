package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_ysqgk;
import com.fsd.admin.service.Z_ysqgkService;
import com.fsd.core.util.ParametersUtil;

@Controller
@RequestMapping("/admin/z_ysqgk")
public class Z_ysqgkController extends AdminController{
	
	private static final Logger log = Logger.getLogger(Z_ysqgkController.class);
    
    @Resource(name = "z_ysqgkServiceImpl")
	private Z_ysqgkService objectService;
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }

    /**
     * 加载分页数据，加载所属回复人数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedatabyemployee.do")
    public @ResponseBody Object loadPageDataByEmployee(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageListByEmployee(parameters, this.getLoginInfo(request.getSession()));
    }
    
    /**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_ysqgk_qx_ysqgkbj.do")
    public @ResponseBody Object saveObject(Z_ysqgk obj, HttpServletRequest request) throws Exception{
    	objectService.save(obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_ysqgk_qx_ysqgksc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_ysqgkbyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
    
    /**
     * 修改状态
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/enabled_ysqgk_qx_ysqgksh.do")
    public @ResponseBody Object enabledObject(ParametersUtil parameters) throws Exception{
    	objectService.updateEnabledObject(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 提交回复
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/huifu_ysqgk_qx_ysqgkhf.do")
    public @ResponseBody Object huifuObject(Z_ysqgk obj, HttpServletRequest request) throws Exception{
    	objectService.saveHuifu(obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 处理未进入流程的信箱数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/dealt_ysqgk.do")
    public @ResponseBody Object saveDealtObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.saveDealtObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
}