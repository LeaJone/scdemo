package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.admin.controller.AdminController;
import com.fsd.admin.model.E_userinfo;
import com.fsd.admin.service.E_userinfoService;
import com.fsd.core.util.ParametersUtil;

@Controller
@RequestMapping("/admin/e_userinfo")
public class E_UserinfoController extends AdminController {

	private static final Logger log = Logger.getLogger(E_UserinfoController.class);
    
    @Resource(name = "e_userinfoServiceImpl")
	private E_userinfoService objectService;
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_e_userinfo_qx_e_userinfobj.do")
    public @ResponseBody Object saveObject(E_userinfo obj, HttpServletRequest request) throws Exception{
    	objectService.save(obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_e_userinfo_qx_e_userinfosc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_e_userinfobyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
     
    /**
     * 根据EmployeeID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_e_userinfobyemployeeid.do")
    public @ResponseBody Object loadObjectByEmployeeid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectByEmployeeId(parameters));
    }

    
    /**
     * 获得微信绑定二维码
     * @param parameters
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/get_weichatQRcode.do")
    public @ResponseBody Object bindWchat(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	String url = objectService.getWechatQRcode(parameters);
    	return ParametersUtil.sendList(url);
    }

    /**
     * 解除微信绑定
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_userinfobind.do")
    public @ResponseBody Object delUserInfoBind(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delUserInfoBind(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 为用户推送消息
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/push_articleinfo_userinfo.do")
    public @ResponseBody Object pushArticleinfoToUserInfo(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.pushArticleinfoToUserinfo(parameters);
    	return ParametersUtil.sendList();
    }
    
}
