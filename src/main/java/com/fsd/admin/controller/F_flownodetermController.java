package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fsd.admin.model.F_flownodeterm;
import com.fsd.admin.service.F_flownodetermService;
import com.fsd.core.util.ParametersUtil;

@Controller
@RequestMapping("/admin/f_flownodeterm")
public class F_flownodetermController extends AdminController{
	
	private static final Logger log = Logger.getLogger(F_flownodetermController.class);
    
    @Resource(name = "f_flownodetermServiceImpl")
	private F_flownodetermService objectService;

    /**
     * 根据节点对象ID加载对象集合
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_ObjectListByNodeID.do")
    public @ResponseBody Object loadObjectListByNodeID(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectListByNodeID(parameters, this.getLoginUser(request.getSession())));
    }
    
    /**
     * 保存对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_flownodeterm_qx_lcxmbj.do")
    public @ResponseBody Object saveObject(F_flownodeterm obj, HttpServletRequest request) throws Exception{
    	objectService.save(obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_flownodeterm_qx_lcxmsc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_flownodetermbyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
    
    /**
     * 顺序排序
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/sort_flownodeterm_qx_lcxmbj.do")
    public @ResponseBody Object sortObject(ParametersUtil parameters) throws Exception{
    	objectService.updateSortObject(parameters);
    	return ParametersUtil.sendList(null);
    }
}