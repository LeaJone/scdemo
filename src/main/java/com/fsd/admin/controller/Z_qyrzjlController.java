package com.fsd.admin.controller;

import com.fsd.admin.model.Z_qyrzjl;
import com.fsd.admin.service.Z_qyrzjlService;
import com.fsd.core.util.ParametersUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/admin/z_qyrzjl")
public class Z_qyrzjlController extends AdminController{
    
    @Resource(name = "z_qyrzjlServiceImpl")
	private Z_qyrzjlService objectService;
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }

    /**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_qyrzjl_qx_qyrzjlbj.do")
    public @ResponseBody Object saveObject(Z_qyrzjl obj, HttpServletRequest request) throws Exception{
        objectService.save(obj, this.getLoginUser(request.getSession()));
        return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_qyrzjlbyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }

    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_qyrzjl.do")
    public @ResponseBody Object loadObject(ParametersUtil parameters) throws Exception{
        return ParametersUtil.sendList(objectService.getObject(parameters));
    }

}