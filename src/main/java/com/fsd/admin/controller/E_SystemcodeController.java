package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.admin.service.E_SystemcodeService;
import com.fsd.core.util.ParametersUtil;

@Controller
@RequestMapping("/admin/e_systemcode")
public class E_SystemcodeController extends AdminController {

	private static final Logger log = Logger.getLogger(E_SystemcodeController.class);
    
    @Resource(name = "e_systemcodeServiceImpl")
	private E_SystemcodeService objectService;

    /**
     * 加载数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_data.do")
    public @ResponseBody Object loadData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectListByCompanyID(this.getLoginUser(request.getSession())));
    }
}
