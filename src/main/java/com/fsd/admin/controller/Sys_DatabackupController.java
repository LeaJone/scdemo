package com.fsd.admin.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.admin.model.Sys_Databackup;
import com.fsd.admin.service.Sys_DatabackupService;
import com.fsd.core.util.ParametersUtil;

/**
 * 数据备份—Controller
 * @author Administrator
 *
 */
@Controller
@RequestMapping("admin/sys_databackup")
public class Sys_DatabackupController extends AdminController {
	
	@Resource(name = "sys_databackupServiceImpl")
	private Sys_DatabackupService databackupService;
	
	/**
     * 加载备份文件数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadArticle(ParametersUtil parameters , HttpServletRequest request) throws Exception{
    	return databackupService.getDataPageList(parameters);
    }
    
    /**
     * 删除备份数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_databackup.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	databackupService.delObject(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 下载备份数据文件
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/download_databackup.do")
    public @ResponseBody Object downloadObject(ParametersUtil parameters, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	Sys_Databackup databackup = databackupService.getObjectById(parameters);
    	String path = databackup.getFilepath() + "/" + databackup.getFilename();
    	File file = new File(path);
        FileInputStream fis = new FileInputStream(file);
        byte[] b = new byte[1024];
        int len = 0;
        String newpath = request.getSession().getServletContext().getRealPath("uploadfiles/databackup");
        File saveDir = new File(newpath);
    	if(!saveDir.exists()){  
    	    saveDir.mkdir();  
    	}  
        FileOutputStream fos = new FileOutputStream(newpath + "/" + databackup.getFilename());
        while((len = fis.read(b)) != -1){
            fos.write(b, 0, len);
        }
        fos.close();
        fis.close();
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 查看备份数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/view_databackup.do")
    public @ResponseBody Object viewObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(databackupService.getObjectById(parameters));
    }
    
    /**
     * 恢复备份数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/recovery_databackup.do")
    public @ResponseBody Object recoveryObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	databackupService.recoveryData(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 备份数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/add_databackup.do")
    public @ResponseBody Object addObject(HttpServletRequest request) throws Exception{
    	databackupService.saveDatabackup(this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
}
