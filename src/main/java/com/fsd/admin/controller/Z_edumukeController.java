package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.Z_course;
import com.fsd.admin.model.Z_edumuke;
import com.fsd.admin.service.Z_edumukeService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/admin/z_edumuke")
public class Z_edumukeController extends AdminController{

	private static final Logger log = Logger.getLogger(Z_edumukeController.class);

	@Resource(name = "z_edumukeServiceImpl")
	private Z_edumukeService objectService;

	/**
	 * 加载分页数据
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/load_pagedata.do")
	public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
		return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
	}

	/**
	 * 编辑对象
	 * @param obj
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/save_z_edumuke_qx_z_edumukebj.do")
	public @ResponseBody Object saveObject(Z_edumuke obj, HttpServletRequest request) throws Exception{
		objectService.save(this.getRootPaht(request.getSession()), obj, this.getLoginUser(request.getSession()));
		return ParametersUtil.sendList();
	}

	/**
	 * 删除对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/del_z_edumuke_qx_z_edumukesc.do")
	public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
		objectService.delObject(parameters, this.getLoginUser(request.getSession()));
		return ParametersUtil.sendList();
	}

	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/load_z_edumukebyid.do")
	public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
		return ParametersUtil.sendList(objectService.getObjectById(parameters));
	}

	/**
	 * 上传申请文件
	 * @param obj
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/edumuke_fileupload.do")
	public @ResponseBody Object uploadUrl(Z_edumuke obj, HttpServletRequest request) throws Exception{
		objectService.uploadFile(obj, this.getLoginUser(request.getSession()));
		return ParametersUtil.sendList();
	}
	
	/**
	 * 上传申请文件
	 * @param obj
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/downExcel_all.do")
	public @ResponseBody Object getExcel(Z_edumuke obj, HttpServletRequest request) throws Exception{
		objectService.downExcel(this.getLoginUser(request.getSession()));
		return ParametersUtil.sendList();
	}
}