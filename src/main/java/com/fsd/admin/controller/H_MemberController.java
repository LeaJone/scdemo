package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.service.H_MemberService;

@Controller
@RequestMapping("/admin/h_member")
public class H_MemberController extends AdminController{
	
	private static final Logger log = Logger.getLogger(H_MemberController.class);
    
    @Resource(name = "h_memberServiceImpl")
	private H_MemberService objectService;


	//============================  注册管理  ============================
	/**
	 * 加载分页数据，查询所属注册会员
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_databyregister.do")
    public @ResponseBody Object loadDataByRegister(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageListByRegister(parameters, this.getLoginUser(request.getSession()));
    }


	//============================  会员管理  ============================
	/**
	 * 加载分页数据，查询删除会员
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_recycledata.do")
    public @ResponseBody Object loadDataRecycle(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageListRecycle(parameters, this.getLoginUser(request.getSession()));
    }
    
	/**
	 * 加载分页数据，查询所有会员
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_databyall.do")
    public @ResponseBody Object loadDataByAll(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageListByAll(parameters, this.getLoginUser(request.getSession()));
    }

	/**
	 * 加载分页数据，查询所属管理会员
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_databycompany.do")
    public @ResponseBody Object loadDataByCompany(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageListByCompany(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 保存对象
     * @param obj
     * @return
     * @throws Exception
     */
    @RequestMapping({
    	"/save_Member_qx_hyglbj.do", 
    	"/save_Member_qx_hyglqbbj.do"})
    public @ResponseBody Object saveObject(A_Employee obj , HttpServletRequest request) throws Exception{
    	objectService.saveObject(obj , this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 保存对象
     * @param obj
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_MemberSelf.do")
    public @ResponseBody Object saveObjectSelf(A_Employee obj , HttpServletRequest request) throws Exception{
    	objectService.saveObjectSelf(obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 发送充值消息测试
     * @param parameters
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/test_sendMessageRecharge.do")
    public @ResponseBody Object sendMessageRecharge(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.sendMessageRecharge(parameters);
    	return ParametersUtil.sendList();
    }
    
    /**
     * 发送消费消息测试
     * @param parameters
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/test_sendMessagePay.do")
    public @ResponseBody Object sendMessagePay(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.sendMessagePay(parameters);
    	return ParametersUtil.sendList();
    }
    
    /**
     * 发送预约消息测试
     * @param parameters
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/test_sendMessageReserve.do")
    public @ResponseBody Object sendMessageReserve(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.sendMessageReserve(parameters);
    	return ParametersUtil.sendList();
    }
}