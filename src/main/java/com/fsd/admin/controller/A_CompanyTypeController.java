package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_CompanyType;
import com.fsd.admin.service.A_CompanyTypeService;

@Controller
@RequestMapping("/admin/A_CompanyType")
public class A_CompanyTypeController extends AdminController{
	
	private static final Logger log = Logger.getLogger(A_CompanyTypeController.class);
    
    @Resource(name = "A_CompanyTypeServiceImpl")
	private A_CompanyTypeService objectService;

    /**
     * 加载数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_data.do")
    public @ResponseBody Object loadData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 加载对象树
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_CompanyTypeTree.do")
    public @ResponseBody Object getObjectTree(String node, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectTree(node, this.getLoginUser(request.getSession())));
    }
    
    /**
     * 异步加载对象树
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_AsyncCompanyTypeTree.do")
    public @ResponseBody Object getAsyncObjectTree(String node, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getAsyncObjectTree(node, this.getLoginUser(request.getSession())));
    }
    
    /**
     * 保存对象
     * @param obj
     * @return
     * @throws Exception
     */
    @RequestMapping({
    	"/save_CompanyType_qx_mrlbbj.do", 
    	"/save_CompanyType_qx_dylbbj.do", 
    	"/save_CompanyType_qx_hylbbj.do", 
    	"/save_CompanyType_qx_rylbbj.do", 
    	"/save_CompanyType_qx_zwlbbj.do"})
    public @ResponseBody Object saveObject(A_CompanyType obj, HttpServletRequest request) throws Exception{
    	objectService.saveObject(obj , this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping({
    	"/del_CompanyType_qx_mrlbsc.do", 
    	"/del_CompanyType_qx_dylbsc.do", 
    	"/del_CompanyType_qx_hylbsc.do", 
    	"/del_CompanyType_qx_rylbsc.do", 
    	"/del_CompanyType_qx_zwlbsc.do"})
    public @ResponseBody Object delObject(ParametersUtil parameters) throws Exception{
    	objectService.delObject(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_CompanyTypeByid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
}