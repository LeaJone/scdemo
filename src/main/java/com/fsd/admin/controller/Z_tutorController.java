package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.Z_course;
import com.fsd.admin.model.Z_tutor;
import com.fsd.admin.service.Z_tutorService;

@Controller
@RequestMapping("/admin/z_tutor")
public class Z_tutorController extends AdminController{

	private static final Logger log = Logger.getLogger(Z_tutorController.class);

	@Resource(name = "z_tutorServiceImpl")
	private Z_tutorService objectService;

	/**
	 * 加载分页数据
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/load_pagedata.do")
	public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
		return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
	}

	/**
	 * 编辑对象
	 * @param obj
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/save_z_tutor_qx_z_tutorbj.do")
	public @ResponseBody Object saveObject(Z_tutor obj, HttpServletRequest request) throws Exception{
		return ParametersUtil.sendList(objectService.save(obj, this.getLoginUser(request.getSession())));
	}

	/**
	 * 删除对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/del_z_tutor_qx_z_tutorsc.do")
	public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
		objectService.delObject(parameters, this.getLoginUser(request.getSession()));
		return ParametersUtil.sendList();
	}

	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/load_z_tutorbyid.do")
	public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
		return ParametersUtil.sendList(objectService.getObjectById(parameters));
	}
	
	/**
     * 修改审核状态
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/checked_tutor_bj.do")
    public @ResponseBody Object checkedObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.updateCheckedObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
	
	/**
	 * 添加拒绝原因
	 * @param obj
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/save_reason.do")
	public @ResponseBody Object saveReason(Z_tutor obj, HttpServletRequest request) throws Exception{
		objectService.saveMessage(obj, this.getLoginUser(request.getSession()));
		return ParametersUtil.sendList();
	}

}