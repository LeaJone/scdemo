package com.fsd.admin.controller;

import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.fsd.admin.service.Sys_SiteInfoService;

@Controller
@RequestMapping("/admin/Sys_SiteInfo")
public class Sys_SiteInfoController extends AdminController{
	
	private static final Logger log = Logger.getLogger(Sys_SiteInfoController.class);
    
    @Resource(name = "Sys_SiteInfoServiceImpl")
	private Sys_SiteInfoService objectService;
    
}