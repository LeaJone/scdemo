package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.Z_projectsite;
import com.fsd.admin.service.Z_projectsiteService;

@Controller
@RequestMapping("/admin/z_projectsite")
public class Z_projectsiteController extends AdminController{
	
	private static final Logger log = Logger.getLogger(Z_projectsiteController.class);
    
    @Resource(name = "z_projectsiteServiceImpl")
	private Z_projectsiteService objectService;
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }

    /**
     * 提交项目基本信息
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_project_info.do")
    public @ResponseBody Object saveProjectInfo(ParametersUtil parameters, Z_projectsite obj, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.saveInfo(parameters, this.getRootPaht(request.getSession()), obj, this.getLoginUser(request.getSession())));
    }

    /**
     * 提交项目简介信息
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_abstract_info.do")
    public @ResponseBody Object saveAbstract(ParametersUtil parameters, Z_projectsite obj, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.saveAbstract(parameters, obj, this.getLoginUser(request.getSession())));
    }

    /**
     * 提交项目团队风采信息
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_teammien_info.do")
    public @ResponseBody Object saveTeammien(ParametersUtil parameters, Z_projectsite obj, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.saveTeammien(parameters, obj, this.getLoginUser(request.getSession())));
    }

    /**
     * 提交项目成果展示信息
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_achievement_info.do")
    public @ResponseBody Object saveAchievement(ParametersUtil parameters, Z_projectsite obj, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.saveAchievement(parameters, obj, this.getLoginUser(request.getSession())));
    }

    /**
     * 提交项目指导老师信息
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_adviser_info.do")
    public @ResponseBody Object saveAdviser(ParametersUtil parameters, Z_projectsite obj, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.saveAdviser(parameters, obj, this.getLoginUser(request.getSession())));
    }

    /**
     * 提交项目支撑材料信息
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_materials_info.do")
    public @ResponseBody Object saveMaterials(ParametersUtil parameters, Z_projectsite obj, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.saveMaterials(parameters, obj, this.getLoginUser(request.getSession())));
    }
    
    /**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_z_projectsite_qx_z_projectsitebj.do")
    public @ResponseBody Object saveObject(Z_projectsite obj, HttpServletRequest request) throws Exception{
    	objectService.save(obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_z_projectsite_qx_z_projectsitesc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_z_projectsitebyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }

    /**
     * 根据FID加载对象
     * @param fid
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_z_projectsitebyfid.do")
    public @ResponseBody Object loadObjectByFid(String fid) throws Exception{
        return ParametersUtil.sendList(objectService.getObjectByFid(fid));
    }


    /**
     * 根据FID加载对象
     * @param fid
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_z_projectsiteview.do")
    public @ResponseBody Object viewSite(String fid) throws Exception{
        Z_projectsite projectsite = objectService.getObjectByFid(fid);
        if(projectsite != null){
            return ParametersUtil.sendList(projectsite);
        }else{
            return ParametersUtil.sendList();
        }
    }
}