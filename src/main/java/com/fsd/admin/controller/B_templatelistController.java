package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fsd.admin.model.B_templatelist;
import com.fsd.admin.service.B_templatelistService;
import com.fsd.core.util.ParametersUtil;

@Controller
@RequestMapping("/admin/b_templatelist")
public class B_templatelistController extends AdminController{
	
	private static final Logger log = Logger.getLogger(B_templatelistController.class);
    
    @Resource(name = "b_templatelistServiceImpl")
	private B_templatelistService objectService;
    
    /**
     * 加载数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_data.do")
    public @ResponseBody Object loadData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 保存对象
     * @param obj
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_TemplateList_qx_cslbbj.do")
    public @ResponseBody Object saveObject(B_templatelist obj , HttpServletRequest request) throws Exception{
    	objectService.saveObject(obj , this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
   
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_TemplateListByid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
}
