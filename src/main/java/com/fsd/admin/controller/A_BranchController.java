package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Branch;
import com.fsd.admin.service.A_BranchService;

@Controller
@RequestMapping("/admin/A_Branch")
public class A_BranchController extends AdminController{
	
	private static final Logger log = Logger.getLogger(A_BranchController.class);
    
    @Resource(name = "A_BranchServiceImpl")
	private A_BranchService objectService;

    /**
     * 加载权限菜单
     * @author lw
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/get_AllQxBranchCheck.do")
    public @ResponseBody Object getAllQxObjectCheck(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getAllQxObjectCheck(parameters, this.getLoginUser(request.getSession())));
    }
    
    /**
     * 加载数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_data.do")
    public @ResponseBody Object loadData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 异步加载机构树
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_AsyncBranchTree.do")
    public @ResponseBody Object getAsyncObjectTree(String node, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getAsyncObjectTree(node, false, this.getLoginUser(request.getSession())));
    }
    /**
     * 异步加载机构树，查询使用
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_AsyncBranchTreeQuery.do")
    public @ResponseBody Object getAsyncObjectTreeQuery(String node, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getAsyncObjectTree(node, true, this.getLoginUser(request.getSession())));
    }
    
    /**
     * 异步加载机构树，根据权限加载
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_AsyncBranchTreeByPopdom.do")
    public @ResponseBody Object getAsyncObjectTreeByPopdom(String node, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getAsyncObjectTreeByPopdom(node, false, this.getLoginUser(request.getSession()), this.getLoginUserPopedom(request.getSession())));
    }
    /**
     * 异步加载机构树，根据权限加载，查询使用
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_AsyncBranchTreeByPopdomQuery.do")
    public @ResponseBody Object getAsyncObjectTreeByPopdomQuery(String node, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getAsyncObjectTreeByPopdom(node, true, this.getLoginUser(request.getSession()), this.getLoginUserPopedom(request.getSession())));
    }
    
    /**
     * 一次性加载机构树
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_AllBranchTreeByPopdom.do")
    public @ResponseBody Object getAllObjectTreeByPopdom(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendTreeList(objectService.getAllObjectTreeByPopdom(parameters, false, this.getLoginInfo(request.getSession())));
    }
    /**
     * 一次性加载机构树，查询使用
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_AllBranchTreeByPopdomQuery.do")
    public @ResponseBody Object getAllObjectTreeByPopdomQuery(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendTreeList(objectService.getAllObjectTreeByPopdom(parameters, true, this.getLoginInfo(request.getSession())));
    }
    
    /**
     * 保存机构
     * @param obj
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_Branch_qx_jgbj.do")
    public @ResponseBody Object saveObject(A_Branch obj, HttpServletRequest request) throws Exception{
    	objectService.saveObject(obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 删除机构
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_Branch_qx_jgsc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters) throws Exception{
    	objectService.delObject(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_BranchByid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
    
    /**
     * 修改状态
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/enabled_Branch_qx_jgbj.do")
    public @ResponseBody Object enabledObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.updateEnabledObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }

    /**
     * 院系数据和统一身份认证同步
     * @param parameters
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/sync_Branch.do")
    public @ResponseBody Object syncBranch(ParametersUtil parameters, HttpServletRequest request) throws Exception{
        objectService.updateSyncBranch(this.getLoginUser(request.getSession()));
        return ParametersUtil.sendList();
    }
}