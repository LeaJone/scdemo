package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Company;
import com.fsd.admin.service.A_CompanyService;

@Controller
@RequestMapping("/admin/A_Company")
public class A_CompanyController extends AdminController{
	
	private static final Logger log = Logger.getLogger(A_CompanyController.class);
    
    @Resource(name = "A_CompanyServiceImpl")
	private A_CompanyService objectService;

    /**
     * 站群专用跳转站点查询方法
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_alldatabysitechange.do")
    public @ResponseBody Object loadAllDataBySiteChange(ParametersUtil parameters) throws Exception{
    	return objectService.getObjectListBySiteChange(parameters);
    }

    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters) throws Exception{
    	return objectService.getObjectPageList(parameters);
    }

    /**
     * 保存对象
     * @param object
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_Company_qx_dwbj.do")
    public @ResponseBody Object saveObject(A_Company object , HttpServletRequest request) throws Exception{
    	objectService.saveObject(object, this.getLoginUser(request.getSession()), this.getRootPaht(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_CompanyById.do")
    public @ResponseBody Object loadObjectById(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
    
    /**
     * 删除机构
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_Company_qx_dwsc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters) throws Exception{
    	objectService.delObject(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 修改状态
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/enabled_Company_qx_dwbj.do")
    public @ResponseBody Object enabledObject(ParametersUtil parameters) throws Exception{
    	objectService.updateEnabledObject(parameters);
    	return ParametersUtil.sendList(null);
    }
}