package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.B_wordkeybad;
import com.fsd.admin.service.B_wordkeybadService;

@Controller
@RequestMapping("/admin/b_wordkeybad")
public class B_wordkeybadController extends AdminController{
	
	private static final Logger log = Logger.getLogger(B_wordkeybadController.class);
    
    @Resource(name = "b_wordkeybadServiceImpl")
	private B_wordkeybadService objectService;

    /**
     * 加载数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_data.do")
    public @ResponseBody Object loadData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 保存对象
     * @param obj
     * @return
     * @throws Exception
     */
    @RequestMapping({
    	"/save_WordKeyBad_qx_gjcbj.do", 
    	"/save_WordKeyBad_qx_mgcbj.do"})
    public @ResponseBody Object saveObject(B_wordkeybad obj , HttpServletRequest request) throws Exception{
    	objectService.saveObject(obj , this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping({
    	"/del_WordKeyBad_qx_gjcsc.do", 
    	"/del_WordKeyBad_qx_mgcsc.do"})
    public @ResponseBody Object delObject(ParametersUtil parameters) throws Exception{
    	objectService.delObject(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_WordKeyBadByid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }

    /**
     * 修改对象状态
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping({
    	"/enabled_WordKeyBad_qx_gjcbj.do", 
    	"/enabled_WordKeyBad_qx_mgcbj.do"})
    public @ResponseBody Object enabledObject(ParametersUtil parameters) throws Exception{
    	objectService.updateEnabledObject(parameters);
    	return ParametersUtil.sendList(null);
    }
}