package com.fsd.admin.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fsd.admin.model.*;
import com.fsd.admin.service.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.fsd.admin.util.WordUtils;
import com.fsd.core.util.Config;
import com.fsd.core.util.ParametersUtil;

@Controller
@RequestMapping("/admin")
public class PageController extends AdminController{
	
	private static final Logger log = Logger.getLogger(PageController.class);
	
    @Resource(name = "Sys_SystemParametersServiceImpl")
	private Sys_SystemParametersService sysparaService;
    
    //用户
    @Resource(name = "A_EmployeeServiceImpl")
	private A_EmployeeService employeeService; 
    
    //邮件
    @Resource(name = "z_emailServiceImpl")
	private Z_emailService emailService;

	@Autowired
	private Z_projectService projectService;

	@Autowired
	private Z_projectmemberService projectmemberService;

	@Autowired
	private Z_budgetService budgetService;

	@Autowired
	private F_middlecheckformService middlecheckformService;

	@Autowired
	private F_middlechecknextService middlechecknextService;

	@Autowired
	private Z_projectbillService projectbillService;

	@Autowired
	private Z_projectendingService projectendingService;

	@Autowired
	private Z_projectmodifyService projectmodifyService;
    
    @RequestMapping("/")
    public void toLoginPage(HttpServletRequest request, HttpServletResponse response) throws Exception{
    	response.sendRedirect("admin/index.htm");
    }
    
	@RequestMapping("/index.htm")
	public ModelAndView toIndexPage(HttpServletRequest request, HttpServletResponse response) throws Exception {
		A_Employee loginuser = this.getLoginUser(request.getSession());
		String icopath = sysparaService.getParameterValueByCode(Config.SITELOGOICON, loginuser.getCompanyid());
		if (icopath == null || icopath.equals(""))
			icopath = "favicon.ico";
		String pagetitle = sysparaService.getParameterValueByCode(Config.SITEADMINTITLE, loginuser.getCompanyid());
		if (pagetitle == null || pagetitle.equals(""))
			pagetitle = "福顺达内容管理系统";
		ModelAndView mav = new ModelAndView();
		mav.addObject("icopath", icopath);
		mav.addObject("pagetitle", pagetitle);
		if (loginuser.getLoginname().indexOf("admin") == 0){
			mav.setViewName("admin/index");
		}else{
			String gqsj = sysparaService.getParameterValueByCode(Config.PWDTIME, "FSDCOMPANY");
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
			String nowdate = format.format(new Date());
			if("3980e4044675f6339248ee0c735c7d72".equals(loginuser.getPassword())){
				mav.setViewName("admin/init");
				mav.addObject("msg", "初始化密码");
			}else{
				mav.setViewName("admin/index");
			}
//			else if (loginuser.getPasswordtime() == null || "".equals(loginuser.getPasswordtime())){
//				mav.setViewName("admin/init");
//				mav.addObject("msg", "密码强制过期");
//			}else if (gqsj.compareTo(loginuser.getPasswordtime()) > 0 && nowdate.compareTo(gqsj) > 0){
//				mav.setViewName("admin/init");
//				mav.addObject("msg", "密码强制过期");
//			}
//			else{
//				mav.setViewName("admin/index");
//			}
		}
		return mav;
	}
	
	/**
	 * 用户管理个人信息导出
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/gyxyscEmployee-{id}.htm")
    public void toZxbmPage(HttpServletRequest request, HttpServletResponse response, @PathVariable("id") String id) throws Exception{
		Map<String, Object> map = new HashMap<String, Object>();  
		A_Employee employee = employeeService.getUserinfoById(id);
		if(employee.getRealname() != null) {
			map.put("realname", employee.getRealname() );
		}else {
			map.put("realname", " ");
		}
		
		if(employee.getSexname() != null) {
			map.put("sexname", employee.getSexname());
		}else {
			map.put("sexname", " ");
		}
		
		if(employee.getCode() != null) {
			map.put("code", employee.getCode());
		}else {
			map.put("code", " ");
		}
		
		if(employee.getAcademyname() != null) {
			map.put("academyname", employee.getAcademyname());
		}else {
			map.put("academyname", " ");
		}
		
		if(employee.getBranchname() != null) {
			map.put("branchname", employee.getBranchname());
		}else {
			map.put("branchname", " ");
		}
		
		if(employee.getRegisterclass() != null) {
			map.put("registerclass", employee.getRegisterclass());
		}else {
			map.put("registerclass", " ");
		}
		
		if(employee.getUsertypename() != null) {
			map.put("usertypename", employee.getUsertypename());
		}else {
			map.put("usertypename", " ");
		}
		
		if(employee.getMobile() != null) {
			map.put("mobile", employee.getMobile());
		}else {
			map.put("mobile", " ");
		}
		
		if(employee.getEmail() != null) {
			map.put("email", employee.getEmail());
		}else {
			map.put("email", " ");
		}
		
        if(employee.getRealname() != null && !employee.getRealname().equals("")) {
        	WordUtils.exportMillCertificateWord(request, response, map, employee.getRealname()); // 文件名称
        }
	}

	/**
	 * 大创项目统计报表
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/report_dcxm.htm")
	public ModelAndView toReportDcxmPage(HttpServletRequest request, HttpServletResponse response, String start, String end) throws Exception{
		ModelAndView mav = new ModelAndView();
		mav.addObject("start", start);
		mav.addObject("end", end);
		mav.setViewName("admin/report/report_dcxm");
		return mav;
	}

	/**
	 * 大创项目省级统计报表
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/report_dcxmsj.htm")
	public ModelAndView toReportDcxmSjPage(HttpServletRequest request, HttpServletResponse response, String start, String end) throws Exception{
		ModelAndView mav = new ModelAndView();
		mav.addObject("start", start);
		mav.addObject("end", end);
		mav.setViewName("admin/report/report_dcxmsj");
		return mav;
	}

	/**
	 * 教改项目统计报表
	 * @param parameters
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/report_jgxm.htm")
	public ModelAndView toReportJgxmPage(ParametersUtil parameters,HttpServletRequest request, HttpServletResponse response, String start, String end) throws Exception{
		ModelAndView mav = new ModelAndView();
		mav.addObject("start", start);
		mav.addObject("end", end);
		mav.setViewName("admin/report/report_jgxm");
		return mav;
	}


	/**
	 * 项目分组统计报表
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/report_xmfz.htm")
	public ModelAndView toReportXmfzPage(HttpServletRequest request, HttpServletResponse response, String groupid) throws Exception{
		ModelAndView mav = new ModelAndView();
		mav.addObject("groupid", groupid);
		mav.setViewName("admin/report/report_xmfz");
		return mav;
	}

	/**
	 * 大创项目-申报书
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/dcxm_sbs-{id}.htm")
	public ModelAndView toDcxmSbsPage(HttpServletRequest request, HttpServletResponse response, @PathVariable("id") String id) throws Exception{
		ModelAndView mav = new ModelAndView();
		Z_project project = projectService.get(id);
		List<Z_projectmember> list = projectmemberService.getListByProjectId(id);
		List<Z_budget> budgetList = budgetService.getListByFid(id);
		double count = 0;
		if(budgetList != null){
			for (Z_budget z_budget : budgetList) {
				count = count +  Double.valueOf(z_budget.getF_money());
			}
		}
		mav.addObject("project", project);
		mav.addObject("members", list);
		mav.addObject("budgetList", budgetList);
		mav.addObject("count", count);
		mav.setViewName("admin/table/dcxm_sbs");
		return mav;
	}

	/**
	 * 大创项目-项目变更
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/dcxm_xmbg-{id}-{modifyid}.htm")
	public ModelAndView toDcxmXmbgPage(HttpServletRequest request, HttpServletResponse response, @PathVariable("id") String id, @PathVariable("modifyid") String modifyid) throws Exception{
		ModelAndView mav = new ModelAndView();
		Z_project project = projectService.get(id);
		Z_projectmodify projectmodify = projectmodifyService.get(modifyid);
		mav.addObject("project", project);
		mav.addObject("projectmodify", projectmodify);
		mav.setViewName("admin/table/dcxm_xmbg");
		return mav;
	}

	/**
	 * 大创项目-中期检查
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/dcxm_zqjc-{id}.htm")
	public ModelAndView toDcxmZqjcPage(HttpServletRequest request, HttpServletResponse response, @PathVariable("id") String id) throws Exception{
		ModelAndView mav = new ModelAndView();
		Z_project project = projectService.get(id);
		F_middlecheckform middlecheckform = middlecheckformService.getObjectByProjectId(id);
		List<F_middlechecknext> nextList = middlechecknextService.getListByProjectId(id);
		List<Z_projectbill> billList = projectbillService.getListByProjectId(id);
		List<Z_projectmember> memberList = projectmemberService.getListByProjectId(id);

		mav.addObject("project", project);
		mav.addObject("middlecheckform", middlecheckform);
		mav.addObject("nextList", nextList);
		mav.addObject("billList", billList);
		mav.addObject("members", memberList);
		mav.setViewName("admin/table/dcxm_zqjc");
		return mav;
	}

	/**
	 * 大创项目-结项书
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/dcxm_jxs-{id}.htm")
	public ModelAndView toDcxmJxsPage(HttpServletRequest request, HttpServletResponse response, @PathVariable("id") String id) throws Exception{
		ModelAndView mav = new ModelAndView();
		Z_project project = projectService.get(id);
		List<Z_projectmember> memberList = projectmemberService.getListByProjectId(id);
		Z_projectending projectending = projectendingService.getObjectByProjectId(id);
		mav.addObject("project", project);
		mav.addObject("projectending", projectending);
		mav.addObject("members", memberList);
		mav.setViewName("admin/table/dcxm_jxs");
		return mav;
	}

	@Autowired
	private Z_xkjnjsService xkjnjsService;

	@Autowired
	private Z_xkjnjsteacherService xkjnjsteacherService;

	/**
	 * 学科技能竞赛-申报书
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/xkjnjs_sbs-{id}.htm")
	public ModelAndView toXkjnjsSbsPage(HttpServletRequest request, HttpServletResponse response, @PathVariable("id") String id) throws Exception{
		ModelAndView mav = new ModelAndView();
		Z_xkjnjs xkjnjs = xkjnjsService.get(id);
		List<Z_xkjnjsteacher> teacherList = xkjnjsteacherService.getListByFid(id);
		List<Z_budget> budgetList = budgetService.getListByFid(id);
		mav.addObject("xkjnjs",xkjnjs);
		mav.addObject("teacherList",teacherList);
		mav.addObject("budgetList",budgetList);
		double total = 0;
		for (Z_budget z_budget : budgetList) {
			total = total + Integer.valueOf(z_budget.getF_money());
		}
		mav.addObject("total",total);
		mav.setViewName("admin/table/xkjnjs_sbs");
		return mav;
	}

	/**
	 * 学科技能竞赛-总结报告
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/xkjnjs_zjbg-{id}.htm")
	public ModelAndView toXkjnjsZjbgPage(HttpServletRequest request, HttpServletResponse response, @PathVariable("id") String id) throws Exception{
		ModelAndView mav = new ModelAndView();
		mav.setViewName("admin/table/xkjnjs_zjbg");
		return mav;
	}

	@Autowired
	private F_activitiService activitiService;

	@RequestMapping("/console.htm")
	public ModelAndView toConsolePage(HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mav = new ModelAndView();
		ParametersUtil parametersUtil = new ParametersUtil();
		parametersUtil = activitiService.getTaskPageList(parametersUtil, this.getLoginUser(request.getSession()));
		if(parametersUtil != null && parametersUtil.getData() != null && parametersUtil.getData().size() > 0){
			mav.addObject("tasknum", parametersUtil.getData().size());
		}else{
			mav.addObject("tasknum", 0);
		}
		int emainnum = emailService.getCountById(this.getLoginUser(request.getSession()).getId());
		mav.addObject("emainnum", emainnum);

		int projectnum = projectService.getProjectCountByUser(this.getLoginUser(request.getSession()));
		mav.addObject("projectnum", projectnum);

		mav.setViewName("admin/console");
		return mav;
	}
}