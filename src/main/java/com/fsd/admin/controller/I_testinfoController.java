package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.I_testinfo;
import com.fsd.admin.service.I_testinfoService;

@Controller
@RequestMapping("/admin/i_testinfo")
public class I_testinfoController extends AdminController{
	
	private static final Logger log = Logger.getLogger(I_testinfoController.class);
    
    @Resource(name = "i_testinfoServiceImpl")
	private I_testinfoService objectService;
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedataall.do")
    public @ResponseBody Object loadPageDataAll(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageListAll(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_i_testinfo_qx_i_testinfobj.do")
    public @ResponseBody Object saveObject(I_testinfo obj, HttpServletRequest request) throws Exception{
    	objectService.save(obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_i_testinfo_qx_i_testinfosc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_i_testinfobyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
    
    /**
     * 加载题卷树
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_i_testinfotree.do")
    public @ResponseBody Object getTestInfoTree(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getTestInfoTree(parameters, this.getLoginUser(request.getSession()));
    }
}