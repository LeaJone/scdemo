package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fsd.admin.model.F_flownode;
import com.fsd.admin.service.F_flownodeService;
import com.fsd.core.util.ParametersUtil;

@Controller
@RequestMapping("/admin/f_flownode")
public class F_flownodeController extends AdminController{
	
	private static final Logger log = Logger.getLogger(F_flownodeController.class);
    
    @Resource(name = "f_flownodeServiceImpl")
	private F_flownodeService objectService;
    
    /**
     * 根据项目ID加载数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_databyprojectid.do")
    public @ResponseBody Object loadDataByProjectID(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectListByProjectID(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 保存对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_flownode_qx_lcxmbj.do")
    public @ResponseBody Object saveObject(F_flownode obj, HttpServletRequest request) throws Exception{
    	objectService.save(obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_flownode_qx_lcxmsc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_flownodebyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
}