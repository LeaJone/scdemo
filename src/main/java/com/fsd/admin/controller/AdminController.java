package com.fsd.admin.controller;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

import com.fsd.admin.entity.A_LoginInfo;
import com.fsd.admin.model.A_Company;
import com.fsd.admin.model.A_Employee;
import com.fsd.core.controller.BaseController;
import com.fsd.core.util.Config;

/**
 *  业务Controller基类
 */

public class AdminController  extends BaseController{
	
	private static final Logger log = Logger.getLogger(AdminController.class);

	/**
	 * 获得当前登录用户
	 */
	public A_LoginInfo getLoginInfo(HttpSession session){
		 return ((A_LoginInfo)session.getAttribute(Config.LOGININFO));
	}

	/**
	 * 获得当前登录用户
	 */
	public A_Employee getLoginUser(HttpSession session){
		if(session.getAttribute(Config.LOGININFO) != null){
			return ((A_LoginInfo)session.getAttribute(Config.LOGININFO)).getEmployee();
		}else{
			return null;
		}
	}
	
	/**
	 * 设置当前登录用户
	 */
	public void setLoginUser(HttpSession session, A_Employee employee){
		if(session.getAttribute(Config.LOGININFO) != null){
			A_LoginInfo user = (A_LoginInfo) session.getAttribute(Config.LOGININFO);
			user.setEmployee(employee);
			session.setAttribute(Config.LOGININFO, user);
		}
	}
	
	/**
	 * 获得当前登录用户所属单位
	 */
	public A_Company getLoginUserCompany(HttpSession session){
		return ((A_LoginInfo)session.getAttribute(Config.LOGININFO)).getCompany();
	}
	
	/**
	 * 获得当前登录用户权限
	 */
	public Map<String, List<String>> getLoginUserPopedom(HttpSession session){
		return ((A_LoginInfo)session.getAttribute(Config.LOGININFO)).getPopedomMap();
	}
	
	/**
	 * 获得Tomcat容器根目录
	 * @param session
	 * @return
	 */
	public String getRootPaht(HttpSession session){
		String rootPaht = session.getServletContext().getRealPath("/");
		return rootPaht.replaceAll("\\\\", "/");
	}
	
	/**
	 * 获得HTTP目录
	 * @param request
	 * @return
	 */
	public String getHttpRootPath(HttpServletRequest request){
		String path = request.getContextPath();
		return request.getScheme() + "://"+ request.getServerName() + ":" + request.getServerPort()+ path + "/";
	}
	
	/**
	 * 获得临时文件url
	 * @param request
	 * @return
	 */
	public String getTempUrl(HttpServletRequest request){
		return "uploadfiles/temp/" + UUID.randomUUID().toString()+".zip";
	}
}
