package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.Z_projectbill;
import com.fsd.admin.service.Z_projectbillService;

@Controller
@RequestMapping("/admin/z_projectbill")
public class Z_projectbillController extends AdminController{
	
	private static final Logger log = Logger.getLogger(Z_projectbillController.class);
    
    @Resource(name = "z_projectbillServiceImpl")
	private Z_projectbillService objectService;
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }

    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata1.do")
    public @ResponseBody Object loadPageData1(ParametersUtil parameters, HttpServletRequest request) throws Exception{
        return objectService.getObjectPageList1(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_z_projectbill_qx_z_projectbillbj.do")
    public @ResponseBody Object saveObject(Z_projectbill obj, HttpServletRequest request) throws Exception{
    	objectService.save(obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_z_projectbill_qx_z_projectbillsc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_z_projectbillbyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }

    /**
     * 修改审批状态
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/enabled_bill.do")
    public @ResponseBody Object enabledObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
        objectService.updateEnabledObject(parameters);
        return ParametersUtil.sendList(null);
    }
}