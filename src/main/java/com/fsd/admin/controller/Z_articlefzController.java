package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fsd.core.util.Config;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.B_article;
import com.fsd.admin.model.Z_articlefz;
import com.fsd.admin.service.Z_articlefzService;

@Controller
@RequestMapping("/admin/z_articlefz")
public class Z_articlefzController extends AdminController{
	
	private static final Logger log = Logger.getLogger(Z_articlefzController.class);
    
    @Resource(name = "z_articlefzServiceImpl")
	private Z_articlefzService objectService;
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters) throws Exception{
    	return objectService.getObjectPageList(parameters);
    }
    
    /**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_z_articlefz_qx_z_articlefzbj.do")
    public @ResponseBody Object saveObject(Z_articlefz obj, HttpServletRequest request,  B_article article, ParametersUtil parameters) throws Exception{
    	ParametersUtil util = objectService.save(obj, this.getLoginUser(request.getSession()), article, parameters);
    	return util.sendObject();
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_z_articlefz_qx_z_articlefzsc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_z_articlefzbyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return objectService.getObjectById(parameters).sendObject();
    }
    
    /**
     * 修改状态
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/enabled_qdnrb_qx.do")
    public @ResponseBody Object enabledObject(ParametersUtil parameters) throws Exception{
    	objectService.updateEnabledObject(parameters);
    	return ParametersUtil.sendList(null);
    }
}