package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fsd.admin.model.A_area;
import com.fsd.admin.service.A_areaService;
import com.fsd.core.util.ParametersUtil;

@Controller
@RequestMapping("/admin/a_area")
public class A_areaController extends AdminController{
	
	private static final Logger log = Logger.getLogger(A_areaController.class);
    
    @Resource(name = "a_areaServiceImpl")
	private A_areaService objectService;
    
    /**
     * 加载数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_data.do")
    public @ResponseBody Object loadData(ParametersUtil parameters) throws Exception{
    	return objectService.getObjectPageList(parameters);
    }
    
    /**
     * 加载对象树
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_AreaTree.do")
    public @ResponseBody Object getObjectTree(String node) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectTree(node));
    }
    
    /**
     * 异步加载对象树
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_AsyncAreaTree.do")
    public @ResponseBody Object getAsyncObjectTree(String node) throws Exception{
    	return ParametersUtil.sendList(objectService.getAsyncObjectTree(node));
    }
    
    /**
     * 保存对象
     * @param obj
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_Area_qx_dylbbj.do")
    public @ResponseBody Object saveObject(A_area obj , HttpServletRequest request) throws Exception{
    	objectService.saveObject(obj , this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_Area_qx_dylbsc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters) throws Exception{
    	objectService.delObject(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_AreaByid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
}