package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.Z_course;
import com.fsd.admin.service.Z_courseService;

@Controller
@RequestMapping("/admin/z_course")
public class Z_courseController extends AdminController{
	
	private static final Logger log = Logger.getLogger(Z_courseController.class);
    
    @Resource(name = "z_courseServiceImpl")
	private Z_courseService objectService;
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
            * 加载个人分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata_my.do")
    public @ResponseBody Object loadPageDataMy(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectMyPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_z_course_qx_z_coursebj.do")
    public @ResponseBody Object saveObject(Z_course obj, HttpServletRequest request) throws Exception{
    	objectService.save(this.getRootPaht(request.getSession()),obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
   
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_z_course_qx_z_coursesc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象    修改功能
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_z_coursebyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
    
    /**
     * 修改启用状态
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/enabled_course_qx_coursebj.do")
    public @ResponseBody Object enabledObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.updateEnabledObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 修改审核状态
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/checked_course_qx_coursebj.do")
    public @ResponseBody Object checkedObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.updateCheckedObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
             * 添加拒绝原因
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_reason.do")
    public @ResponseBody Object saveReason(Z_course obj, HttpServletRequest request) throws Exception{
    	objectService.saveMessage(obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }

    /**
     * 导入课程
     * @param parameters
     * @param request
     * @param f_url
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/import_course.do")
    public @ResponseBody Object importLuyanScore(ParametersUtil parameters, HttpServletRequest request, String f_url) throws Exception{
        objectService.updateImportCourse(this.getRootPaht(request.getSession()), f_url);
        return ParametersUtil.sendList();
    }
}