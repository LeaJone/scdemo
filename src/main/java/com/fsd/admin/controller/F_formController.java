package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.F_form;
import com.fsd.admin.service.F_formService;

@Controller
@RequestMapping("/admin/f_form")
public class F_formController extends AdminController{
	
	private static final Logger log = Logger.getLogger(F_formController.class);
    
    @Resource(name = "f_formServiceImpl")
	private F_formService objectService;
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_f_form_qx_f_formbj.do")
    public @ResponseBody Object saveObject(F_form obj, HttpServletRequest request) throws Exception{
    	objectService.save(obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_f_form_qx_f_formsc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_f_formbyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }

    /**
     * 根据taskid和formid加载相应的表对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_f_objForm.do")
    public @ResponseBody Object loadObjectForm(ParametersUtil parameters) throws Exception{
        return ParametersUtil.sendList(objectService.getObjectForm(parameters));
    }

    /**
     * 修改状态
     * @param parameters
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/enabled_form_qx_formbj.do")
    public @ResponseBody Object enabledObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
        objectService.updateEnabledObject(parameters, this.getLoginUser(request.getSession()));
        return ParametersUtil.sendList(null);
    }

    /**
     * 保存表单设计
     * @param parameters
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/form_savedesign.do")
    public @ResponseBody Object saveFormDesign(ParametersUtil parameters, HttpServletRequest request) throws Exception{
        objectService.saveFormDesign(parameters, this.getLoginUser(request.getSession()));
        return ParametersUtil.sendList(null);
    }

    /**
     * 保存表单数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_form.do")
    public @ResponseBody Object saveForm(ParametersUtil parameters) throws Exception{
        objectService.saveForm(parameters);
        return ParametersUtil.sendList(null);
    }

    /**
     * 加载表单数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_form.do")
    public @ResponseBody Object loadForm(ParametersUtil parameters) throws Exception{
        return ParametersUtil.sendList(objectService.getObjectForm(parameters));
    }

    /**
     * Word导出
     * @param parameters
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/export_form.do")
    public @ResponseBody Object export(ParametersUtil parameters, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.exportForm(parameters, this.getRootPaht(request.getSession()), this.getHttpRootPath(request)));
    }
}