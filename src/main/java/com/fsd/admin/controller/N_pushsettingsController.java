package com.fsd.admin.controller;

import com.fsd.admin.model.N_pushsettings;
import com.fsd.admin.service.N_pushsettingsService;
import com.fsd.core.util.ParametersUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/admin/n_pushsettings")
public class N_pushsettingsController extends AdminController{
    
    @Resource(name = "n_pushsettingsServiceImpl")
	private N_pushsettingsService objectService;
    
    /**
     * 加载数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_data.do")
    public @ResponseBody Object loadData(ParametersUtil parameters) throws Exception{
    	return objectService.getObjectPageList(parameters);
    }
    
    /**
     * 保存对象
     * @param obj
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_pushsettings_qx_tsszbj.do")
    public @ResponseBody Object saveObject(N_pushsettings obj , HttpServletRequest request) throws Exception{
    	objectService.saveObject(obj , this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_pushsettings_qx_tsszsc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters) throws Exception{
    	objectService.delObject(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pushsettingsById.do")
    public @ResponseBody Object loadObjectById(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
}