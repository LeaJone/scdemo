package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.fsd.core.util.HtmlToPdf;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.Z_xkjnjs;
import com.fsd.admin.service.Z_xkjnjsService;

@Controller
@RequestMapping("/admin/z_xkjnjs")
public class Z_xkjnjsController extends AdminController{
	
	private static final Logger log = Logger.getLogger(Z_xkjnjsController.class);
    
    @Resource(name = "z_xkjnjsServiceImpl")
	private Z_xkjnjsService objectService;
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }

    /**
     * 提交基本信息
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_xkjnjs_info.do")
    public @ResponseBody Object saveProjectInfo(ParametersUtil parameters, Z_xkjnjs obj, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.saveInfo(parameters, obj, this.getLoginUser(request.getSession())));
    }

    /**
     * 提交负责人信息
     * @param parameters
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_xkjnjs_leader.do")
    public @ResponseBody Object saveProjectLeader(ParametersUtil parameters, Z_xkjnjs obj, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.saveLeader(parameters, obj, this.getLoginUser(request.getSession())));
    }

    /**
     * 提交竞赛内容简介信息
     * @param parameters
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_xkjnjs_abstract.do")
    public @ResponseBody Object saveProjectAbstract(ParametersUtil parameters, Z_xkjnjs obj, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.saveAbstract(parameters, obj, this.getLoginUser(request.getSession())));
    }

    /**
     * 提交竞赛实施方案及预期目标
     * @param parameters
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_xkjnjs_scheme.do")
    public @ResponseBody Object saveProjectScheme(ParametersUtil parameters, Z_xkjnjs obj, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.saveScheme(parameters, obj, this.getLoginUser(request.getSession())));
    }

    /**
     * 提交配套条件信息
     * @param parameters
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_xkjnjs_condition.do")
    public @ResponseBody Object saveProjectcondition(ParametersUtil parameters, Z_xkjnjs obj, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.saveCondition(parameters, obj, this.getLoginUser(request.getSession())));
    }

    /**
     * 提交方案信息
     * @param parameters
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_xkjnjs_train.do")
    public @ResponseBody Object saveProjecttrain(ParametersUtil parameters, Z_xkjnjs obj, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.saveTrain(parameters, obj, this.getLoginUser(request.getSession())));
    }

    /**
     * 提交项目
     * @param parameters
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_xkjnjs_submit.do")
    public @ResponseBody Object saveProjectSubmit(ParametersUtil parameters, HttpServletRequest request) throws Exception{
        objectService.saveProjectSubmit(this.getRootPaht(request.getSession()), parameters, this.getLoginUser(request.getSession()));
        return ParametersUtil.sendList();
    }
    
    /**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_z_xkjnjs_qx_z_xkjnjsbj.do")
    public @ResponseBody Object saveObject(ParametersUtil parameters, Z_xkjnjs obj, HttpServletRequest request) throws Exception{
    	objectService.save(this.getRootPaht(request.getSession()), parameters, obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_z_xkjnjs_qx_z_xkjnjssc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_z_xkjnjsbyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }

    /**
     * 下载项目申报书
     * @param request
     * @param projectid
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/download_projectApplyBook.do")
    public @ResponseBody Object downloadProjectApplyBook(HttpServletRequest request, String projectid) throws Exception{
        String url = this.getHttpRootPath(request);
        String rootPaht = this.getRootPaht(request.getSession());
        String filePdfPath = rootPaht + "uploadfiles/projectfile/"+ projectid +".pdf";
        HtmlToPdf.convert(url + "admin/xkjnjs_sbs-"+ projectid +".htm", filePdfPath);
        return ParametersUtil.sendList("uploadfiles/projectfile/"+ projectid +".pdf");
    }

    /**
     * 根据项目ID加载项目当前审核节点信息
     * @param request
     * @param projectid
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/get_projectAuditStep.do")
    public @ResponseBody Object getProjectAuditStep(HttpServletRequest request, String projectid) throws Exception{
        return ParametersUtil.sendList(objectService.getProjectAuditStep(projectid));
    }
}