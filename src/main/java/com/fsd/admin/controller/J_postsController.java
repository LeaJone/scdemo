package com.fsd.admin.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.J_posts;
import com.fsd.admin.model.Z_team;
import com.fsd.admin.service.J_postsService;

@Controller
@RequestMapping("/admin/j_posts")
public class J_postsController extends AdminController{
    
	@Autowired
	private J_postsService objectService;
	
	/**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_z_post_qx_z_teambj.do")
    public @ResponseBody Object saveObject(J_posts obj, HttpServletRequest request) throws Exception{
    	objectService.save(obj, this.getLoginUser(request.getSession()));
        return ParametersUtil.sendList();
    }
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_j_posts_qx_lttzsc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_j_postsbyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
    
    /**
     * 置顶文章
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/firstly_j_posts_qx_lttzbj.do")
    public @ResponseBody Object firstlyObject(ParametersUtil parameters) throws Exception{
    	objectService.updateFirstlyObject(parameters);
    	return ParametersUtil.sendList(null);
    }
    
}