package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.Z_base;
import com.fsd.admin.service.Z_baseService;

@Controller
@RequestMapping("/admin/z_base")
public class Z_baseController extends AdminController{
	
	private static final Logger log = Logger.getLogger(Z_baseController.class);
    
    @Resource(name = "z_baseServiceImpl")
	private Z_baseService objectService;
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_z_base_qx_z_basebj.do")
    public @ResponseBody Object saveObject(Z_base obj, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.save(obj, this.getLoginUser(request.getSession())));
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_z_base_qx_z_basesc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_z_basebyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }

    /**
     * 异步加载基地树
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_AsyncBaseTreeQuery.do")
    public @ResponseBody Object getAsyncObjectTreeQuery(String node, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.getAsyncObjectTree(node, this.getLoginUser(request.getSession())));
    }
}