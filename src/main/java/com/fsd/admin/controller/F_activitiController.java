package com.fsd.admin.controller;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.F_activiti;
import com.fsd.admin.service.F_activitiService;

@Controller
@RequestMapping("/admin/f_activiti")
public class F_activitiController extends AdminController{
	
	private static final Logger log = Logger.getLogger(F_activitiController.class);
    
    @Autowired
	private F_activitiService objectService;
    
    /**
     * 加载流程设计分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_designpagedata.do")
    public @ResponseBody Object loadDesignPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getDesignPageList(parameters, this.getLoginUser(request.getSession()));
    }

    /**
     * 加载流程定义列表
     * @param parameters
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_processDefinitionPageData.do")
    public @ResponseBody Object loadProcessDefinitionPageList(ParametersUtil parameters, HttpServletRequest request) throws Exception{
        return objectService.getProcessDefinitionPageList(parameters, this.getLoginUser(request.getSession()));
    }

    /**
     * 加载流程实例列表
     * @param parameters
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_processInstancePageData.do")
    public @ResponseBody Object loadProcessInstancePageList(ParametersUtil parameters, HttpServletRequest request) throws Exception{
        return objectService.getProcessInstancePageList(parameters, this.getLoginUser(request.getSession()));
    }

    /**
     * 编辑流程
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_f_activiti_qx_f_activitibj.do")
    public @ResponseBody Object saveObject(F_activiti obj, HttpServletRequest request) throws Exception{
    	objectService.save(obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }

    /**
     * 发布流程
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/deploy_f_activiti.do")
    public @ResponseBody Object deployObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
        objectService.deployObject(parameters, this.getLoginUser(request.getSession()));
        return ParametersUtil.sendList();
    }
    
    /**
     * 删除模型
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_f_activitiModel")
    public @ResponseBody Object deleteModel(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.deleteModel(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }

    /**
     * 删除部署
     * @param parameters
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_f_activitiDeployment.do")
    public @ResponseBody Object deleteDeployment(ParametersUtil parameters, HttpServletRequest request) throws Exception{
        objectService.deleteDeployment(parameters, this.getLoginUser(request.getSession()));
        return ParametersUtil.sendList();
    }

    /**
     * 加载登陆用户任务分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_taskpagedata.do")
    public @ResponseBody Object loadTaskPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
        return objectService.getTaskPageList(parameters, this.getLoginUser(request.getSession()));
    }

    /**
     * 任务处理
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/audit_task.do")
    public @ResponseBody Object taskAudit(HttpServletRequest request, String taskid, String state, String message) throws Exception{
        objectService.disposeTask(taskid, state, message);
        return ParametersUtil.sendList();
    }

    /**
     * 任务处理
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/audit_choosetask.do")
    public @ResponseBody Object chooseTaskAudit(HttpServletRequest request, String taskid, String ids) throws Exception{
        objectService.disposeChooseTask(taskid, ids);
        return ParametersUtil.sendList();
    }

    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_f_activitibyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }

    /**
     * 根据任务ID加载流程历史审核信息
     * @param parameters
     * @throws Exception
     */
    @RequestMapping(value = "/load_historyListByTaskId.do")
    public @ResponseBody Object getHistoryListByTaskId(ParametersUtil parameters) throws Exception{
        return ParametersUtil.sendList(objectService.getHistoryListByTaskId(parameters));
    }
}