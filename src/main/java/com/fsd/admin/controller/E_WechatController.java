package com.fsd.admin.controller;

import java.io.File;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.admin.model.E_wechat;
import com.fsd.admin.service.E_WechatService;
import com.fsd.core.util.ParametersUtil;

/**
 * 微信账号管理Controller
 * @author Administrator
 *
 */

@Controller
@RequestMapping("/admin/e_wechat")
public class E_WechatController extends AdminController {
	
	private static final Logger log = Logger.getLogger(E_WechatController.class);
    
    @Resource(name = "e_wechatServiceImpl")
	private E_WechatService objectService;	
	
    
    /**
     * 加载数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_data.do")
    public @ResponseBody Object loadData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectList(parameters, this.getLoginUser(request.getSession())));
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_WeChatByid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }

    /**
     * 保存对象
     * @param obj
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_WeChat_qx_wxzhbj.do")
    public @ResponseBody Object saveObject(E_wechat obj , HttpServletRequest request) throws Exception{
    	objectService.saveObject(obj , this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_WeChat_qx_wxzhsc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters) throws Exception{
    	objectService.delObject(parameters);
    	return ParametersUtil.sendList(null);
    }
}
