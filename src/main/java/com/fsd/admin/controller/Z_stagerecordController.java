package com.fsd.admin.controller;

import com.fsd.admin.model.Z_stagerecord;
import com.fsd.admin.service.Z_stagerecordService;
import com.fsd.core.util.ParametersUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/admin/z_stagerecord")
public class Z_stagerecordController extends AdminController{
    
    @Autowired
	private Z_stagerecordService objectService;

    /**
     * 加载项目分页数据
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_projectpagedata.do")
    public @ResponseBody Object loadProjectPageData(ParametersUtil param, HttpServletRequest request) throws Exception{
        return objectService.getProjectPageList(param, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_stagerecord_qx_stagerecordbj.do")
    public @ResponseBody Object saveObject(Z_stagerecord obj, HttpServletRequest request) throws Exception{
    	objectService.save(obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_stagerecord_qx_stagerecordsc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_z_stagerecordbyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
    
}