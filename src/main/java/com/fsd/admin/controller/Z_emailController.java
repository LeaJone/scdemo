package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.admin.model.Z_email;
import com.fsd.admin.service.Z_emailService;
import com.fsd.core.util.ParametersUtil;

/**
 * 站内信- controller
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/admin/z_email")
public class Z_emailController extends AdminController {

	private static final Logger log = Logger.getLogger(Z_emailController.class);
    
    @Resource(name = "z_emailServiceImpl")
	private Z_emailService objectService;
    
    /**
     * 保存邮件
     * @param obj
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_email_first.do")
    public @ResponseBody Object saveObjectFirst(Z_email obj , HttpServletRequest request) throws Exception{
    	objectService.saveEmailFirst(obj , this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 发送邮件
     * @param obj
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_email.do")
    public @ResponseBody Object saveObject(Z_email obj , HttpServletRequest request) throws Exception{
    	objectService.saveEmail(obj , this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 加载已删除邮件数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_RecycleEmail.do")
    public @ResponseBody Object loadRecycleEmail(ParametersUtil parameters , HttpServletRequest request) throws Exception{
    	return objectService.getRecycleEmailPageList(parameters , this.getLoginUser(request.getSession()));
    }
    
    /**
     * 加载草稿箱邮件数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_DraftEmail.do")
    public @ResponseBody Object loadDraftEmail(ParametersUtil parameters , HttpServletRequest request) throws Exception{
    	return objectService.getDraftEmailPageList(parameters , this.getLoginUser(request.getSession()));
    }
    
    /**
     * 加载星标邮件数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_StarEmail.do")
    public @ResponseBody Object loadStarEmail(ParametersUtil parameters , HttpServletRequest request) throws Exception{
    	return objectService.getStarEmailPageList(parameters , this.getLoginUser(request.getSession()));
    }
    
    /**
     * 加载已发送邮件数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_SendEmail.do")
    public @ResponseBody Object loadSendEmail(ParametersUtil parameters , HttpServletRequest request) throws Exception{
    	return objectService.getSendEmailPageList(parameters , this.getLoginUser(request.getSession()));
    }
    
    /**
     * 加载收件箱邮件数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_InboxEmail.do")
    public @ResponseBody Object loadInboxEmail(ParametersUtil parameters , HttpServletRequest request) throws Exception{
    	return objectService.getInboxEmailPageList(parameters , this.getLoginUser(request.getSession()));
    }
    
    /**
     * 恢复邮件 
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/recovery_email.do")
    public @ResponseBody Object recoveryObject(ParametersUtil parameters) throws Exception{
    	objectService.updateObjectDeleted(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 彻底删除邮件
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/reldel_email.do")
    public @ResponseBody Object reldelObject(ParametersUtil parameters) throws Exception{
    	objectService.delRealyObject(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 标为已读未读
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/enabled_email_star.do")
    public @ResponseBody Object enabledObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.updateEnabledObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 删除邮件 
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/deleted_email.do")
    public @ResponseBody Object deletedObject(ParametersUtil parameters) throws Exception{
    	objectService.deletedEmail(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 取消星标 
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/nostar_email.do")
    public @ResponseBody Object nostarObject(ParametersUtil parameters) throws Exception{
    	objectService.updatenostarEmail(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 星标邮件 
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/star_email.do")
    public @ResponseBody Object starObject(ParametersUtil parameters) throws Exception{
    	objectService.updatestarEmail(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_EmailByid.do")
    public @ResponseBody Object loadArticleByid(ParametersUtil parameters) throws Exception{
    	objectService.updateEmailToRead(parameters);
    	return objectService.getEmailById(parameters).sendObject();
    }

    /**
     * 根据id加载用户未读邮件数量
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_EmailNotreadByid.do")
    public @ResponseBody Object getEmailNotreadByid(HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.getCountById(this.getLoginUser(request.getSession()).getId()));
    }
}
