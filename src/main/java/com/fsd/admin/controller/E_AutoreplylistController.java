package com.fsd.admin.controller;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fsd.admin.service.E_AutoreplylistService;

@Controller
@RequestMapping("")
public class E_AutoreplylistController extends AdminController {
	
private static final Logger log = Logger.getLogger(E_AutoreplylistController.class);
    
    @Resource(name = "e_autoreplylistServiceImpl")
	private E_AutoreplylistService objectService;
}
