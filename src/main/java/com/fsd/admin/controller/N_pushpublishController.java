package com.fsd.admin.controller;

import com.fsd.admin.model.N_pushpublish;
import com.fsd.admin.service.N_pushpublishService;
import com.fsd.core.util.ParametersUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/admin/n_pushpublish")
public class N_pushpublishController extends AdminController{
    
    @Resource(name = "n_pushpublishServiceImpl")
	private N_pushpublishService objectService;
    
    /**
     * 保存对象
     * @param obj
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_pushpublish_qx_tsfbbj.do")
    public @ResponseBody Object saveObject(N_pushpublish obj , HttpServletRequest request) throws Exception{
    	objectService.saveObject(obj , this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
}