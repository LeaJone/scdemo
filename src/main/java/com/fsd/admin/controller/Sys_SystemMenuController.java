package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.service.Sys_SystemMenuService;

@Controller
@RequestMapping("/admin/Sys_SystemMenu")
public class Sys_SystemMenuController extends AdminController{
	
	private static final Logger log = Logger.getLogger(Sys_SystemMenuController.class);
    
    @Resource(name = "Sys_SystemMenuServiceImpl")
	private Sys_SystemMenuService objectService;

    /**
     * 加载权限菜单
     * @author lw
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/get_AllQxMenuCheck.do")
    public @ResponseBody Object getAllQxMenuCheck(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getAllQxMenuCheck(parameters, this.getLoginUser(request.getSession())));
    }

    /**
     * 加载权限菜单
     * @author lumingbao
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/get_AllQxMenuGroup.do")
    public @ResponseBody Object getAllQxMenuByGroup(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getAllQxMenuByGroup(parameters, this.getLoginUser(request.getSession())));
    }
    
    /**
     * 根据用户ID加载快捷菜单
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/get_userKjcd.do")
    public @ResponseBody Object getKjMenu(HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getKjMenu(this.getLoginUser(request.getSession()).getId()));
    }
}