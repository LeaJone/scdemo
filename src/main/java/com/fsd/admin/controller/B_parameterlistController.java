package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fsd.admin.model.B_parameterlist;
import com.fsd.admin.service.B_parameterlistService;
import com.fsd.core.util.ParametersUtil;

@Controller
@RequestMapping("/admin/b_parameterlist")
public class B_parameterlistController extends AdminController{
	
	private static final Logger log = Logger.getLogger(B_parameterlistController.class);
    
    @Resource(name = "b_parameterlistServiceImpl")
	private B_parameterlistService objectService;

    /**
     * 加载数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_data.do")
    public @ResponseBody Object loadData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 保存对象
     * @param obj
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_ParameterList_qx_cslbbj.do")
    public @ResponseBody Object saveObject(B_parameterlist obj , HttpServletRequest request) throws Exception{
    	objectService.saveObject(obj , this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_ParameterList_qx_cslbsc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters) throws Exception{
    	objectService.delObject(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_ParameterListByid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }

    /**
     * 修改对象状态
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/enabled_ParameterList_qx_cslbbj.do")
    public @ResponseBody Object enabledObject(ParametersUtil parameters) throws Exception{
    	objectService.updateEnabledObject(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * Excel导出
     * @param parameters
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/download.do")
    public @ResponseBody Object download(HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.download(this.getLoginUser(request.getSession()), this.getRootPaht(request.getSession()), this.getHttpRootPath(request)));
    }
    
    /**
     * 顺序排序
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/sort_parameterlist.do")
    public @ResponseBody Object sortObject(ParametersUtil parameters) throws Exception{
    	objectService.updateSortObject(parameters);
    	return ParametersUtil.sendList(null);
    }
}