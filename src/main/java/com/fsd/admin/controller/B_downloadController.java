package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.B_download;
import com.fsd.admin.service.B_downloadService;

@Controller
@RequestMapping("/admin/b_download")
public class B_downloadController extends AdminController{
	
	private static final Logger log = Logger.getLogger(B_downloadController.class);
    
    @Resource(name = "b_downloadServiceImpl")
	private B_downloadService objectService;

    /**
     * 保存附件
     * @param obj
     * @return
     * @throws Exception
     */
    @RequestMapping({
    	"/save_Object.do",
    	"/save_Download_qx_wdbj.do"})
    public @ResponseBody Object saveObject(B_download obj , HttpServletRequest request) throws Exception{
    	objectService.saveObject(obj , this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 根据文章ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_ObjectByArticleId.do")
    public @ResponseBody Object loadObjectByArticleId(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectByArticleId(parameters));
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping({
    	"/del_object_qx_wzsc.do",
    	"/del_Download_qx_wdsc.do"})
    public @ResponseBody Object delObject(ParametersUtil parameters) throws Exception{
    	objectService.delObject(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    

    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_DownloadByid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }

    /**
     * 修改对象状态
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/enabled_Download_qx_wdbj.do")
    public @ResponseBody Object enabledObject(ParametersUtil parameters) throws Exception{
    	objectService.updateEnabledObject(parameters);
    	return ParametersUtil.sendList(null);
    }
}