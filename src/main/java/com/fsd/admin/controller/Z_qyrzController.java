package com.fsd.admin.controller;

import com.fsd.admin.model.Z_qyrz;
import com.fsd.admin.service.Z_qyrzService;
import com.fsd.core.util.ParametersUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/admin/z_qyrz")
public class Z_qyrzController extends AdminController{
    
    @Resource(name = "z_qyrzServiceImpl")
	private Z_qyrzService objectService;
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_qyrz_qx_qyrzbj.do")
    public @ResponseBody Object saveObject(Z_qyrz obj, HttpServletRequest request) throws Exception{
    	objectService.save(obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_qyrz_qx_qyrzsc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_qyrzbyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }

    /**
     * 审核对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/audit_qyrz_qx_qyrzsh.do")
    public @ResponseBody Object auditObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
        objectService.updateAuditObject(parameters, this.getLoginUser(request.getSession()));
        return ParametersUtil.sendList(null);
    }
}