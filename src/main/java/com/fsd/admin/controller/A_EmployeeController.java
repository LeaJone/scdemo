package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.entity.A_LoginInfo;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.service.A_EmployeeService;

@Controller
@RequestMapping("/admin/A_Employee")
public class A_EmployeeController extends AdminController{
	
	private static final Logger log = Logger.getLogger(A_EmployeeController.class);
    
    @Resource(name = "A_EmployeeServiceImpl")
	private A_EmployeeService objectService;

    /**
     * 加载数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_data.do")
    public @ResponseBody Object loadData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
        A_LoginInfo loginInfo = this.getLoginInfo(request.getSession());
        return objectService.getObjectPageList(parameters, loginInfo);
    }

    /**
     * 加载数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_teamuserdata.do")
    public @ResponseBody Object loadTeamuserData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
        A_LoginInfo loginInfo = this.getLoginInfo(request.getSession());
        return objectService.getTeamuserPageList(parameters, loginInfo);
    }
    
    /**
     * 加载数据(加载所有用户)
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_data1.do")
    public @ResponseBody Object loadData1(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	A_LoginInfo loginInfo = this.getLoginInfo(request.getSession());
    	return objectService.getObjectPageList1(parameters, loginInfo);
    }
    
    /**
     * 加载数据(加载所有用户)
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_data2.do")
    public @ResponseBody Object loadData2(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	A_LoginInfo loginInfo = this.getLoginInfo(request.getSession());
    	return objectService.getObjectAllList(parameters, loginInfo);
    }
    
    /**
     * 加载数据(专家)
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_zhuanjia.do")
    public @ResponseBody Object loadZhuanjia(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	A_LoginInfo loginInfo = this.getLoginInfo(request.getSession());
    	return objectService.getZhuanjiaPageList(parameters, loginInfo);
    }
    
    /**
     * 加载数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_datapageall.do")
    public @ResponseBody Object loadDataPageAll(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageAllList(parameters, this.getLoginUser(request.getSession()));
    }

    /**
     * 加载数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_dataall.do")
    public @ResponseBody Object loadDataAll(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectAllList(parameters, this.getLoginInfo(request.getSession())));
    }
    
    /**
     * 加载数据，根据部门加载
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_dataByBranchID.do")
    public @ResponseBody Object loadDataByBranchID(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	A_LoginInfo loginInfo = this.getLoginInfo(request.getSession());
    	return ParametersUtil.sendList(objectService.getObjectListByBranchID(parameters, loginInfo));
    }
    
    /**
     * 保存对象
     * @param obj
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_Employee_qx_rybj.do")
    public @ResponseBody Object saveObject(A_Employee obj , HttpServletRequest request) throws Exception{
    	objectService.saveObject(obj , this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 保存对象(专家)
     * @param obj
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_Employee_qx_zhuanjia.do")
    public @ResponseBody Object savezhuanjia(ParametersUtil parameters, A_Employee obj , HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.saveZhuanjia(parameters, obj, this.getLoginUser(request.getSession())));
    }
    
    /**
     * 用户信息批量导入
     * @param obj
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_Employee_xx_xxdr.do")
    public @ResponseBody Object saveXxdr(A_Employee obj , HttpServletRequest request) throws Exception{
    	objectService.saveXxdr(obj , this.getLoginUser(request.getSession()), this.getRootPaht(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 保存对象
     * @param obj
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_EmployeeSelf.do")
    public @ResponseBody Object saveObjectSelf(A_Employee obj , HttpServletRequest request) throws Exception{
    	objectService.saveObjectSelf(obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping({
    	"/del_Employee_qx_rysc.do", 
    	"/del_Member_qx_hyglsc.do", 
    	"/del_Member_qx_hyglqbsc.do"})
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 重置用户密码
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping({
    	"/pwd_Employee_qx_rybj.do", 
    	"/pwd_Member_qx_hyglbj.do", 
    	"/pwd_Member_qx_hyglqbbj.do"})
    public @ResponseBody Object pwdObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.updatePwdObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 修改人员状态
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping({
    	"/enabled_Employee_qx_rybj.do", 
    	"/enabled_Member_qx_hyglbj.do", 
    	"/enabled_Member_qx_hyglqbbj.do"})
    public @ResponseBody Object enabledObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.updateEnabledObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 修改人员状态
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping({
    	"/ispush_Employee_qx_rybj.do", 
    	"/ispush_Member_qx_hyglbj.do", 
    	"/ispush_Member_qx_hyglqbbj.do"})
    public @ResponseBody Object ispushObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.updateIsWeChatPushObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_EmployeeByid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
    
    /**
     * 修改用户密码
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/pwdedit_Employee.do")
    public @ResponseBody Object pwdEdit(HttpServletRequest request) throws Exception{
    	String old_pwd = request.getParameter("oldpwd").toString();
    	String new_pwd1 = request.getParameter("newpwd1").toString();
    	String new_pwd2 = request.getParameter("newpwd2").toString();
    	this.setLoginUser(request.getSession(), objectService.updateUserPwd(old_pwd, new_pwd1, new_pwd2, this.getLoginUser(request.getSession())));;
    	return ParametersUtil.sendList(null);
    }
    

	//============================  会员信息  ============================
	/**
	 * 加载分页数据，查询会员
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_datahy.do")
    public @ResponseBody Object loadDataHY(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageListHY(parameters, this.getLoginUser(request.getSession()));
    }

    /**
     * 根据Type加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/loadDataByType.do")
    public @ResponseBody Object loadDataByType(ParametersUtil parameters, HttpServletRequest request) throws Exception{
        A_LoginInfo loginInfo = this.getLoginInfo(request.getSession());
        return objectService.getObjectPageListByType(parameters, loginInfo);
    }

    /**
     * 用户数据和统一身份认证同步
     * @param parameters
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/sync_Employee.do")
    public @ResponseBody Object syncEmployee(ParametersUtil parameters, HttpServletRequest request) throws Exception{
        objectService.updateSyncEmployee(this.getLoginUser(request.getSession()));
        return ParametersUtil.sendList();
    }
}