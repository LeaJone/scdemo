package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.Z_projectending;
import com.fsd.admin.service.Z_projectendingService;

@Controller
@RequestMapping("/admin/z_projectending")
public class Z_projectendingController extends AdminController{
	
	private static final Logger log = Logger.getLogger(Z_projectendingController.class);
    
    @Resource(name = "z_projectendingServiceImpl")
	private Z_projectendingService objectService;
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_z_projectending_qx_z_projectendingbj.do")
    public @ResponseBody Object saveObject(Z_projectending obj, HttpServletRequest request) throws Exception{
    	objectService.save(obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_z_projectending_qx_z_projectendingsc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_z_projectendingbyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }

    /**
     * 根据项目ID加载结项信息
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_z_projectendingbyProjectid.do")
    public @ResponseBody Object loadObjectByProjectId(ParametersUtil parameters) throws Exception{
        return ParametersUtil.sendList(objectService.saveOrGetProjectEndingByProjectId(parameters));
    }
}