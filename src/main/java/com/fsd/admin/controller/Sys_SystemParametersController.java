package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fsd.admin.model.Sys_SystemParameters;
import com.fsd.admin.service.Sys_SystemParametersService;
import com.fsd.core.util.Config;
import com.fsd.core.util.ParametersUtil;

@Controller
@RequestMapping("/admin/Sys_SystemParameters")
public class Sys_SystemParametersController extends AdminController{
	
	private static final Logger log = Logger.getLogger(Sys_SystemParametersController.class);
    
    @Resource(name = "Sys_SystemParametersServiceImpl")
	private Sys_SystemParametersService objectService;

    /**
     * 获取系统参数值
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/get_systemlogotitle.do")
    public @ResponseBody Object getSystemLogoTitle(HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getParameterValueByCode(
    			Config.SITELOGOTITLE, this.getLoginUser(request.getSession()).getCompanyid()));
    }
    /**
     * 网站前台内容页访问地址根路径
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/get_systemarticleurl.do")
    public @ResponseBody Object getSystemArticleUrl(HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getParameterValueByCode(
    			Config.SITEARTICLEURL, this.getLoginUser(request.getSession()).getCompanyid()));
    }
    
    /**
     * 网站备份文件下载地址根路径
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/get_systemdatabackupdownloadurl.do")
    public @ResponseBody Object getSystemDatabackupDownloadUrl(HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getParameterValueByCode(
    			Config.FSDSITEDOWNLOADURL, this.getLoginUser(request.getSession()).getCompanyid()));
    }
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 加载所有数据
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_data.do")
    public @ResponseBody Object loadData(HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectList(this.getLoginUser(request.getSession())));
    }
    
    /**
     * 保存对象
     * @param Object
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_SystemParameters.do")
    public @ResponseBody Object saveObject(Sys_SystemParameters Object, HttpServletRequest request) throws Exception{
    	objectService.saveObject(Object, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_SystemParametersByid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_SystemParameters.do")
    public @ResponseBody Object delObject(ParametersUtil parameters) throws Exception{
    	objectService.delObject(parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * Excel导出
     * @param parameters
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/download.do")
    public @ResponseBody Object download(HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.download(this.getLoginUser(request.getSession()), this.getRootPaht(request.getSession()), this.getHttpRootPath(request)));
    }
    
}