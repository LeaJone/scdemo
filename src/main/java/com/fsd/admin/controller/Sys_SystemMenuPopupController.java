package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.admin.service.Sys_SystemMenuPopupService;
import com.fsd.core.util.ParametersUtil;

@Controller
@RequestMapping("/admin/Sys_SystemMenuPopup")
public class Sys_SystemMenuPopupController extends AdminController{
	
	private static final Logger log = Logger.getLogger(Sys_SystemMenuPopupController.class);
    
    @Resource(name = "Sys_SystemMenuPopupServiceImpl")
	private Sys_SystemMenuPopupService objectService;
    
    /**
     * 保存用户快捷菜单
     * @param parameters
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_systemMenuPopup.do")
    public @ResponseBody Object saveObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.saveObject(this.getLoginUser(request.getSession()), parameters);
    	return ParametersUtil.sendList(null);
    }
    
    /**
     * 根据用户ID加载用户的所有快捷菜单
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/get_systemMenuPopupByUserid.do")
    public @ResponseBody Object getObjectByUserId(HttpServletRequest request) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectListByEmployeeid(this.getLoginUser(request.getSession()).getId()));
    }
}