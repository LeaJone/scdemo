package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import com.fsd.core.bean.ImportExcel;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.admin.model.Z_jsydba;
import com.fsd.admin.service.Z_jsydbaService;
import com.fsd.core.util.ParametersUtil;

@Controller
@RequestMapping("/admin/z_jsydba")
public class Z_jsydbaController extends AdminController{
	
	private static final Logger log = Logger.getLogger(Z_jsydbaController.class);
    
    @Resource(name = "z_jsydbaServiceImpl")
	private Z_jsydbaService objectService;
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }
    
    /**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_jsydba_qx_jsydbabj.do")
    public @ResponseBody Object saveObject(Z_jsydba obj, HttpServletRequest request) throws Exception{
    	objectService.save(obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_jsydba_qx_jsydbasc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_jsydbabyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
    
    /**
     * 导入Excel
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/import_jsydba_qx_jsydbadr.do")
    public @ResponseBody Object importObject(ImportExcel obj, HttpServletRequest request) throws Exception{
    	objectService.saveImportObject(obj, this.getLoginUser(request.getSession()), request.getSession().getServletContext().getRealPath("/"));
    	return ParametersUtil.sendList();
    }
}