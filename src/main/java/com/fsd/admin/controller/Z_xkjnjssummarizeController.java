package com.fsd.admin.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.Z_xkjnjssummarize;
import com.fsd.admin.service.Z_xkjnjssummarizeService;

@Controller
@RequestMapping("/admin/z_xkjnjssummarize")
public class Z_xkjnjssummarizeController extends AdminController{
	
	private static final Logger log = Logger.getLogger(Z_xkjnjssummarizeController.class);
    
    @Resource(name = "z_xkjnjssummarizeServiceImpl")
	private Z_xkjnjssummarizeService objectService;
    
    /**
     * 加载分页数据
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_pagedata.do")
    public @ResponseBody Object loadPageData(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	return objectService.getObjectPageList(parameters, this.getLoginUser(request.getSession()));
    }

    /**
     * 提交基本信息
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_xkjnjszjbg_info.do")
    public @ResponseBody Object saveProjectInfo(ParametersUtil parameters, Z_xkjnjssummarize obj, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.saveInfo(parameters, obj, this.getLoginUser(request.getSession())));
    }

    /**
     * 保存项目背景信息
     * @param parameters
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_xkjnjssummarize_background.do")
    public @ResponseBody Object saveBackground(ParametersUtil parameters, Z_xkjnjssummarize obj, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.saveBackground(parameters, obj));
    }

    /**
     * 保存项目实施过程信息
     * @param parameters
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_xkjnjssummarize_implementcourse.do")
    public @ResponseBody Object saveImplementcourse(ParametersUtil parameters, Z_xkjnjssummarize obj, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.saveImplementcourse(parameters, obj));
    }

    /**
     * 保存项目竞赛过程信息
     * @param parameters
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_xkjnjssummarize_matchcourse.do")
    public @ResponseBody Object saveMatchcourse(ParametersUtil parameters, Z_xkjnjssummarize obj, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.saveMatchcourse(parameters, obj));
    }

    /**
     * 保存项目竞赛成果信息
     * @param parameters
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_xkjnjssummarize_achievement.do")
    public @ResponseBody Object saveAchievement(ParametersUtil parameters, Z_xkjnjssummarize obj, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.saveAchievement(parameters, obj));
    }

    /**
     * 保存项目不足与改进信息
     * @param parameters
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_xkjnjssummarize_summarize.do")
    public @ResponseBody Object saveSummarize(ParametersUtil parameters, Z_xkjnjssummarize obj, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.saveSummarize(parameters, obj));
    }

    /**
     * 保存项目中遇到的问题信息
     * @param parameters
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_xkjnjssummarize_question.do")
    public @ResponseBody Object saveQuestion(ParametersUtil parameters, Z_xkjnjssummarize obj, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.saveQuestion(parameters, obj));
    }

    /**
     * 保存项目创新信息
     * @param parameters
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_xkjnjssummarize_innovate.do")
    public @ResponseBody Object saveInnovate(ParametersUtil parameters, Z_xkjnjssummarize obj, HttpServletRequest request) throws Exception{
        return ParametersUtil.sendList(objectService.saveInnovate(parameters, obj));
    }
    
    /**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_z_xkjnjssummarize_qx_z_xkjnjssummarizebj.do")
    public @ResponseBody Object saveObject(Z_xkjnjssummarize obj, HttpServletRequest request) throws Exception{
    	objectService.save(obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 删除对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_z_xkjnjssummarize_qx_z_xkjnjssummarizesc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters, HttpServletRequest request) throws Exception{
    	objectService.delObject(parameters, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_z_xkjnjssummarizebyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }

    /**
     * 根据FID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_z_xkjnjssummarizebyfid.do")
    public @ResponseBody Object loadObjectByFid(ParametersUtil parameters) throws Exception{
        return ParametersUtil.sendList(objectService.getObjectByFid(parameters));
    }

}