package com.fsd.admin.controller;

import com.fsd.admin.model.Sys_seo;
import com.fsd.admin.service.Sys_seoService;
import com.fsd.core.util.ParametersUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/admin/sys_seo")
public class Sys_seoController extends AdminController {
    
    @Autowired
	private Sys_seoService objectService;
    
    /**
     * 编辑对象
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/save_seo_qx_seobj.do")
    public @ResponseBody Object saveObject(Sys_seo obj, HttpServletRequest request) throws Exception{
    	objectService.save(obj, this.getLoginUser(request.getSession()));
    	return ParametersUtil.sendList();
    }
    
    /**
     * 重置对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/del_seo_qx_seosc.do")
    public @ResponseBody Object delObject(ParametersUtil parameters) throws Exception{
    	objectService.delObject(parameters);
    	return ParametersUtil.sendList();
    }
    
    /**
     * 根据ID加载对象
     * @param parameters
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/load_seobyid.do")
    public @ResponseBody Object loadObjectByid(ParametersUtil parameters) throws Exception{
    	return ParametersUtil.sendList(objectService.getObjectById(parameters));
    }
    
}