package com.fsd.admin.service;

import java.util.List;

import com.fsd.admin.model.E_Autoreplylist;
import com.fsd.core.service.BaseService;

public interface E_AutoreplylistService extends BaseService<E_Autoreplylist, String> {

	/**
	 * 彻底删除对象
	 * @param autoReplyID
	 */
	void delObjectByAutoReplyID(String autoReplyID) throws Exception;
	
	/**
	 * 加载所需集合
	 * @return
	 * @throws Exception
	 */
	List<E_Autoreplylist> getObjectListByAutoReplyID(String autoReplyID) throws Exception;
	
	/**
	 * 加载微信号所需集合
	 * @return
	 * @throws Exception
	 */
	List<E_Autoreplylist> getObjectListByWeChatID(String weChatID) throws Exception;
}
