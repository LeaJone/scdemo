package com.fsd.admin.service;

import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.entity.A_LoginInfo;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.B_article;

public interface B_articleService extends BaseService<B_article, String>{
	/**
     * 获得文章内容
     * @param parameters
     * @return
     * @throws Exception
     */
	public Object getArticleMContent(ParametersUtil param, String type) throws Exception;
	/**
	 * 加载文本文章分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getArticlePageList(ParametersUtil param, A_LoginInfo loginInfo) throws Exception;
    /**
	 * 加载已删除文章分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getRecycleArticlePageList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 保存文章
	 */
	public ParametersUtil saveArticle(B_article obj, ParametersUtil parameters, A_Employee user) throws Exception;
	/**
	 * 保存移动文章
	 */
	public void saveArticleWap(B_article obj, ParametersUtil parameters, A_Employee user) throws Exception;
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getArticleById(ParametersUtil parameters) throws Exception;
	/**
	 * 删除对象
	 * @param parameters
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
	/**
	 * 审核对象
	 * @param parameters
	 */
	public void updateAuditObject(ParametersUtil parameters, A_Employee employee) throws Exception;
	/**
	 * 置顶对象
	 * @param parameters
	 */
	public void updateFirstlyObject(ParametersUtil parameters) throws Exception;
	/**
	 * 幻灯对象
	 * @param parameters
	 */
	public void updateSlideObject(ParametersUtil parameters) throws Exception;
	/**
	 * 推荐对象
	 * @param parameters
	 */
	public void updateRecommendObject(ParametersUtil parameters) throws Exception;
	/**
	 * 热门对象
	 * @param parameters
	 */
	public void updateHotObject(ParametersUtil parameters) throws Exception;
	/**
	 * 更改文章类型
	 * @param parameters
	 */
	public void updateObjectType(ParametersUtil parameters) throws Exception;
	/**
	 * 更改文章栏目
	 * @param parameters
	 */
	public void updateObjectSubject(ParametersUtil parameters) throws Exception;
	/**
	 * 恢复对象
	 * @param parameters
	 */
	public void updateObjectDeleted(ParametersUtil parameters) throws Exception;
	/**
	 * 彻底对象
	 * @param parameters
	 */
	public void delRealyObject(ParametersUtil parameters) throws Exception;
	/**
	 * 获得栏目下最后一篇文章
	 * @param subjectId
	 * @return
	 * @throws Exception
	 */
	public B_article getFinally(String subjectId) throws Exception;
	/**
	 * 转换文件为SWF文件
	 * @param parameters
	 * @param rootPath
	 * @return
	 * @throws Exception
	 */
	public String zhuanHuanFlash(ParametersUtil parameters, String rootPath) throws Exception;
	/**
	 * 统计前一日文章的发布量,并推送数据至微信
	 */
	void saveCountNumPerDay(B_article article) throws Exception;

}
