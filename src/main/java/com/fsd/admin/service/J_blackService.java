package com.fsd.admin.service;

import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.J_black;
import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;

public interface J_blackService extends BaseService<J_black, String>{
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	void delObject(ParametersUtil parameters, String isscreen) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	J_black getObjectById(ParametersUtil parameters) throws Exception;
	/**
	 * 添加黑名单（帖子）
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	void saveObject(ParametersUtil parameters, String isscreen) throws Exception;
	/**
	 * 添加黑名单（回复）
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	void saveHfObject(ParametersUtil parameters, String isscreen) throws Exception;
	/**
	 * 根据ID加载黑名单
	 * @return
	 * @throws Exception
	 */
	J_black getBlackByUserId(String userid) throws Exception;
}
