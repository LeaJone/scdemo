package com.fsd.admin.service;

import java.util.List;
import com.fsd.core.bean.CheckTree;
import com.fsd.core.bean.Tree;
import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Sys_SystemMenu;
import com.fsd.admin.model.Sys_SystemMenuPopup;

public interface Sys_SystemMenuService extends BaseService<Sys_SystemMenu, String>{
	
	/**
	 * 获取用户菜单
	 * @param user
	 * @return
	 */
	public List<Tree> getUserMenu(A_Employee user, List<String>popedomList) throws Exception;
	
	/**
	 * 获取带复选框的用户菜单
	 * @param user
	 * @return
	 */
	public List<CheckTree> getUserCheckMenu(A_Employee user, List<String> popedomList) throws Exception;
	
	/**
	 * 获取权限树
	 * @param user
	 * @return
	 */
	public List<CheckTree> getAllQxMenuCheck(ParametersUtil parameters, A_Employee employee) throws Exception;

	/**
	 * 获取权限树
	 * @param user
	 * @return
	 */
	public List<CheckTree> getAllQxMenuByGroup(ParametersUtil parameters, A_Employee employee) throws Exception;
	/**
	 * 获得用户的快捷菜单
	 * @param Employeeid
	 * @return
	 * @throws Exception
	 */
	public List<Sys_SystemMenu> getKjMenu(String Employeeid) throws Exception;
}
