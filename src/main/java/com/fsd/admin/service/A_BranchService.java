package com.fsd.admin.service;

import java.util.List;
import java.util.Map;

import com.fsd.core.bean.CheckTree;
import com.fsd.core.bean.Tree;
import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.entity.A_LoginInfo;
import com.fsd.admin.model.A_Branch;
import com.fsd.admin.model.A_Employee;

public interface A_BranchService extends BaseService<A_Branch, String>{
	/**
	 * 获取栏目结构
	 * @param id
	 * @return
	 */
	public A_Branch getObjectStructByID(String id, String companyid);
	
	/**
	 * 获取权限树
	 * @param user
	 * @return
	 */
	public List<CheckTree> getAllQxObjectCheck(ParametersUtil parameters, A_Employee employee) throws Exception;
	
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
	
	/**
	 * 异步加载部门树
	 * @return
	 * @throws Exception
	 */
	public List<Tree> getAsyncObjectTree(String fid, boolean isQuery, A_Employee employee) throws Exception;
	
	/**
	 * 异步加载部门树，根据权限加载
	 * @return
	 * @throws Exception
	 */
	public List<Tree> getAsyncObjectTreeByPopdom(String fid, boolean isQuery, A_Employee employee, Map<String, List<String>> popdomMap) throws Exception;

	/**
	 * 一次性加载部门树，根据权限加载
	 * @return
	 * @throws Exception
	 */
	public List<Tree> getAllObjectTreeByPopdom(ParametersUtil param, boolean isQuery, A_LoginInfo login) throws Exception;
	
	/**
	 * 保存对象（添加、修改）
	 * @param obj
	 */
	public void saveObject(A_Branch obj , A_Employee a_employee) throws Exception;
	
	/**
	 * 删除对象
	 * @param parameters
	 */
	public void delObject(ParametersUtil parameters) throws Exception;
	
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public A_Branch getObjectById(ParametersUtil parameters) throws Exception;
	/**
	 * 加载部门数据列表
	 * @return
	 * @throws Exception
	 */
	public List<A_Branch> getObjectList() throws Exception;
	
	/**
	 * 获取部门及包含下级所有部门ID集合
	 * @return
	 * @throws Exception
	 */
	public List<String> getObjectStructIDList(String id, String companyID) throws Exception;
	
	/**
	 * 修改状态
	 * @param parameters
	 */
	public void updateEnabledObject(ParametersUtil parameters, A_Employee employee) throws Exception;
	/**
	 * 院系数据和统一身份认证同步
	 * @throws Exception
	 */
	public void updateSyncBranch(A_Employee employee) throws Exception;
}
