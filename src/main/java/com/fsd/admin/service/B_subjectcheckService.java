package com.fsd.admin.service;

import com.fsd.core.util.ParametersUtil;
import com.fsd.core.bean.CheckTree;
import com.fsd.core.service.BaseService;

import java.util.List;
import java.util.Map;

import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.B_subjectcheck;

public interface B_subjectcheckService extends BaseService<B_subjectcheck, String>{
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public void save(B_subjectcheck obj, A_Employee employee) throws Exception;
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public B_subjectcheck getObjectById(ParametersUtil parameters) throws Exception;
	
	
	//---------------------栏目未更新-------------------------//
	/**
	 * 加载树
	 * @param parameters
	 * @param employee
	 * @param popdomMap
	 * @return
	 * @throws Exception
	 */
	public List<CheckTree> getObjectCheck(ParametersUtil parameters, A_Employee employee, 
			Map<String, List<String>> popdomMap) throws Exception;
	/**
	 * 栏目未更新，保存栏目选择
	 * @param parameters
	 * @throws Exception
	 */
	public void save_Subject(ParametersUtil parameters) throws Exception;
	/**
	 * 栏目未更新，检查更新结果
	 * @param parameters
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	public List<Map<String,String>> getJcjgMap(ParametersUtil parameters, A_Employee employee) throws Exception;
	
	
	//---------------------栏目关联人员，或管理人员-------------------------//
	/**
	 * 保存栏目关联人员
	 * @param parameters
	 */
	public void save_SubjectEmployee(ParametersUtil parameters) throws Exception;
	/**
	 * 获取权限树
	 * @param user
	 * @return
	 */
	public List<CheckTree> getQxObjectCheck(ParametersUtil parameters, A_Employee employee, Map<String, List<String>> popdomMap) throws Exception;
	/**
	 * 根据角色ID集合获取所属权限ID集合
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public Map<String, List<String>> getCheckByRoleIds(ParametersUtil parameters) throws Exception;
 
}
