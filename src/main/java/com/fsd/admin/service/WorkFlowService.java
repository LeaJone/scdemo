package com.fsd.admin.service;

import java.util.List;

import com.fsd.admin.entity.A_LoginInfo;
import com.fsd.admin.model.F_flowdealt;
import com.fsd.admin.model.F_flownodeterm;
import com.fsd.admin.model.F_flowrecord;


public interface WorkFlowService {
	
	/**
	 * 退回到流程申请人调用
	 * @param record
	 * @param dealt
	 * @param dataStatus
	 * @param dataStatusName
	 * @param loginInfo
	 * @throws Exception
	 */
	public void saveWorkFlowBackOriginal(F_flowrecord record, F_flowdealt dealt, 
			String dataStatus, String dataStatusName, A_LoginInfo loginInfo) throws Exception;
	
	/**
	 * 撤回到流程申请人调用
	 * @param record
	 * @param dealt
	 * @param dataStatus
	 * @param dataStatusName
	 * @param loginInfo
	 * @throws Exception
	 */
	public void saveWorkFlowRecallOriginal(F_flowrecord record, F_flowdealt dealt, 
			String dataStatus, String dataStatusName, A_LoginInfo loginInfo) throws Exception;

	/**
	 * 开始流程调用数据对象处理
	 * @param dataobjectid
	 * @param dataStatus
	 * @param dataStatusName
	 * @param loginInfo
	 * @throws Exception
	 */
	public void saveWorkFlowStart(String dataobjectid, String dataStatus, String dataStatusName, A_LoginInfo loginInfo) throws Exception;
	
	/**
	 * 终止流程调用数据对象处理
	 * @param record
	 * @param dealt
	 * @param dataStatus
	 * @param dataStatusName
	 * @param loginInfo
	 * @throws Exception
	 */
	public void saveWorkFlowStop(F_flowrecord record, F_flowdealt dealt, 
			String dataStatus, String dataStatusName, A_LoginInfo loginInfo) throws Exception;
	
	/**
	 * 完成流程调用数据对象处理
	 * @param record
	 * @param dealt
	 * @param dataStatus
	 * @param dataStatusName
	 * @param loginInfo
	 * @throws Exception
	 */
	public void saveWorkFlowFinish(F_flowrecord record, F_flowdealt dealt, 
			String dataStatus, String dataStatusName, A_LoginInfo loginInfo) throws Exception;
	
	/**
	 * 保存当前流程节点数据对象处理
	 * @param record
	 * @param dealt
	 * @param dataStatus
	 * @param dataStatusName
	 * @param loginInfo
	 * @throws Exception
	 */
	public void saveWorkFlowNodeDealt(F_flowrecord record, F_flowdealt dealt, 
			String dataStatus, String dataStatusName, A_LoginInfo loginInfo) throws Exception;
	
	/**
	 * 自动流程节点数据对象处理
	 * @param record
	 * @param dealt
	 * @param term
	 * @param dataStatus
	 * @param dataStatusName
	 * @param loginInfo
	 * @throws Exception
	 */
	public boolean saveWorkFlowNodeAuto(F_flowrecord record, F_flowdealt dealt, 
			String dataStatus, String dataStatusName, A_LoginInfo loginInfo) throws Exception;
	
	/**
	 * 自动流程节点程序接口获取数据
	 * @param record
	 * @param dealt
	 * @param term
	 * @param dataStatus
	 * @param dataStatusName
	 * @param loginInfo
	 * @throws Exception
	 */
	public String saveWorkFlowNodeAutoValue(F_flowrecord record, F_flowdealt dealt, 
			String dataStatus, String dataStatusName, A_LoginInfo loginInfo) throws Exception;
	
	/**
	 * 自动流程节点数据选择单一对象处理
	 * @param record
	 * @param dealt
	 * @param dataStatus
	 * @param dataStatusName
	 * @param loginInfo
	 * @return
	 * @throws Exception
	 */
	public boolean saveWorkFlowNodeAutoChooseSingle(F_flowrecord record, F_flowdealt dealt, 
			String dataStatus, String dataStatusName, A_LoginInfo loginInfo) throws Exception;
	
	/**
	 * 自动流程节点数据选择多对象处理
	 * @param record
	 * @param dealtList
	 * @param dataStatus
	 * @param dataStatusName
	 * @param loginInfo
	 * @return
	 * @throws Exception
	 */
	public boolean saveWorkFlowNodeAutoChooseMulti(F_flowrecord record, List<F_flowdealt> dealtList, 
			String dataStatus, String dataStatusName, A_LoginInfo loginInfo) throws Exception;
}
