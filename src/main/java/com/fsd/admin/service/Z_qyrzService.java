package com.fsd.admin.service;

import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_qyrz;
import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;

public interface Z_qyrzService extends BaseService<Z_qyrz, String>{
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	void save(Z_qyrz obj, A_Employee employee) throws Exception;
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	Z_qyrz getObjectById(ParametersUtil parameters) throws Exception;
	/**
	 * 审核对象
	 * @param parameters
	 */
	void updateAuditObject(ParametersUtil parameters, A_Employee employee) throws Exception;
}
