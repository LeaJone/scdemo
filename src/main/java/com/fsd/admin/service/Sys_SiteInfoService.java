package com.fsd.admin.service;

import com.fsd.core.service.BaseService;
import com.fsd.admin.model.Sys_SiteInfo;

public interface Sys_SiteInfoService extends BaseService<Sys_SiteInfo, String>{
  	    
}
