package com.fsd.admin.service;

import java.util.List;

import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.F_flownodeterm;

public interface F_flownodetermService extends BaseService<F_flownodeterm, String>{

	/**
	 * 根据节点对象ID加载对象集合
	 * @param previousID
	 * @return
	 * @throws Exception
	 */
	public List<F_flownodeterm> getObjectListByNodeID(String nodeID)  throws Exception;

	/**
	 * 根据节点对象ID加载对象集合
	 * @param previousID
	 * @return
	 * @throws Exception
	 */
	public List<F_flownodeterm> getObjectListByNodeID(ParametersUtil param, A_Employee employee)  throws Exception;

    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public void save(F_flownodeterm obj, A_Employee employee) throws Exception;
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public F_flownodeterm getObjectById(ParametersUtil parameters) throws Exception;

	/**
	 * 顺序排序
	 * @param parameters
	 */
	public void updateSortObject(ParametersUtil parameters) throws Exception;
}
