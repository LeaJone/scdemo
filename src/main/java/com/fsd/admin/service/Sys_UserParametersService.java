package com.fsd.admin.service;

import com.fsd.core.service.BaseService;
import com.fsd.admin.model.Sys_UserParameters;

public interface Sys_UserParametersService extends BaseService<Sys_UserParameters, String>{
  	    
}
