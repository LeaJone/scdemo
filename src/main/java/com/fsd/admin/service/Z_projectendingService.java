package com.fsd.admin.service;

import com.fsd.core.util.ParametersUtil;
import com.fsd.core.service.BaseService;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_projectending;

public interface Z_projectendingService extends BaseService<Z_projectending, String>{
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	void save(Z_projectending obj, A_Employee employee) throws Exception;
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	Z_projectending getObjectById(ParametersUtil parameters) throws Exception;
	/**
	 * 根据项目ID加载项目结题信息
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	Z_projectending saveOrGetProjectEndingByProjectId(ParametersUtil parameters) throws Exception;
	/**
	 * 根据项目ID加载结项信息
	 * @param projectid
	 * @return
	 * @throws Exception
	 */
	Z_projectending getObjectByProjectId(String projectid) throws Exception;
}
