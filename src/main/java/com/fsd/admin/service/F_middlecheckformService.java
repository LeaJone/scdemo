package com.fsd.admin.service;

import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.F_middlecheckform;

public interface F_middlecheckformService extends BaseService<F_middlecheckform, String>{
	/**
	 * 提交执行计划书
	 * @param obj
	 */
	void saveMiddlecheckform(F_middlecheckform obj, A_Employee employee) throws Exception;
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	F_middlecheckform getMiddlecheckformById(ParametersUtil parameters) throws Exception;
	/**
	 * 根据项目ID加载项目中期检查信息
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	F_middlecheckform saveOrgetMiddlecheckformByProjectId(ParametersUtil parameters) throws Exception;
	/**
	 * 根据项目ID加载中期检查
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	F_middlecheckform getObjectByProjectId(String projectId) throws Exception;
}
