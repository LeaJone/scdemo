package com.fsd.admin.service;

import com.fsd.core.util.ParametersUtil;
import com.fsd.core.bean.Tree;
import com.fsd.core.service.BaseService;

import java.util.List;

import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.T_course;
import com.fsd.admin.model.T_coursetype;

/**
 * 课程信息Service接口
 * @author Administrator
 *
 */
public interface T_courseService extends BaseService<T_course, String>{
    
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
    
	/**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public void save(T_course obj, A_Employee employee) throws Exception;
    
	/**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
    
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public T_course getObjectById(ParametersUtil parameters) throws Exception;
	
	/**
	 * 修改启用&停用状态
	 * @param parameters
	 * @param employee
	 * @throws Exception
	 */
	public void updateEnabledObject(ParametersUtil parameters, A_Employee employee) throws Exception;
	
	/**
	 * 异步加载对象树
	 * @return
	 * @throws Exception
	 */
	public List<Tree> getAsyncObjectTree(String fid) throws Exception;
	
}
