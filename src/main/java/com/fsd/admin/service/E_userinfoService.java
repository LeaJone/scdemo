package com.fsd.admin.service;

import com.fsd.core.util.ParametersUtil;
import com.fsd.core.bean.Tree;
import com.fsd.core.service.BaseService;

import java.util.List;
import java.util.Map;

import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.E_userinfo;
import com.fsd.admin.model.E_wechat;

public interface E_userinfoService extends BaseService<E_userinfo, String>{
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public void save(E_userinfo obj, A_Employee employee) throws Exception;
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public E_userinfo getObjectById(ParametersUtil parameters) throws Exception;
    /**
	 * 根据EmployeeID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public E_userinfo getObjectByEmployeeId(ParametersUtil parameters) throws Exception;
	/**
	 * 根据用户ID加载绑定的微信信息
	 * @param employeeId
	 * @return
	 * @throws Exception
	 */
	public E_userinfo getObjectByEmployeeId(String employeeId) throws Exception;
	/**
	 * 根据openid加载绑定微信列表
	 * @param openid
	 * @return
	 * @throws Exception
	 */
	public E_userinfo getObjectByOpenId(String openid) throws Exception;

	/**
	 * 根据微信openid 添加微信用户信息
	 * @param wechat 系统微信配置
	 * @param openid 微信用户openid
	 * @return
	 * @throws Exception
	 */
	public E_userinfo saveObject(E_wechat wechat, String openid) throws Exception;
	
	/**
	 * 获得用户绑定二维码
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public String getWechatQRcode(ParametersUtil param) throws Exception;

	/**
	 * 扫码根据参数绑定用户
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public String saveUserInfoBindByQRcode(E_wechat tObj, String fromUserName, String employeeid) throws Exception;
	
	/**
	 * 删除微信绑定
	 * @param employeeid 用户编号
	 * @return
	 * @throws Exception
	 */
	public void delUserInfoBind(ParametersUtil param, A_Employee employee) throws Exception;
	
	/**
	 * 为用户推送消息
	 * @param parameters
	 */
	public void pushArticleinfoToUserinfo(ParametersUtil parameters) throws Exception;
	
}
