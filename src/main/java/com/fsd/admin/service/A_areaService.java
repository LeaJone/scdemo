package com.fsd.admin.service;

import java.util.List;

import com.fsd.core.bean.Tree;
import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.A_area;

public interface A_areaService extends BaseService<A_area, String>{
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param) throws Exception;
	
	/**
	 * 加载类别树
	 * @return
	 * @throws Exception
	 */
	public List<Tree> getObjectTree(String fid) throws Exception;

	/**
	 * 异步加载对象树
	 * @param node
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public List<Tree> getAsyncObjectTree(String fid) throws Exception;
	
	/**
	 * 保存对象（添加、修改）
	 * @param obj
	 */
	public void saveObject(A_area obj , A_Employee employee) throws Exception;
	
	/**
	 * 删除对象
	 * @param parameters
	 */
	public void delObject(ParametersUtil parameters) throws Exception;
	
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public A_area getObjectById(ParametersUtil parameters) throws Exception;
}
