package com.fsd.admin.service;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.entity.A_LoginInfo;
import com.fsd.admin.model.F_flowdealt;
import com.fsd.admin.model.F_flownode;
import com.fsd.admin.model.F_flowobject;
import com.fsd.admin.model.F_flowproject;
import com.fsd.admin.model.F_flowrecord;

public interface F_flowrecordService extends BaseService<F_flowrecord, String>{

	/**
	 * 获取流程记录中，提取审核的流程对象ID集合
	 * @param loginInfo
	 * @return
	 * @throws Exception
	 */
	public List<String> getFlowObjectIDListByExtract(A_LoginInfo loginInfo) throws Exception;

	/**
	 * 获取流程记录中，释放审核的流程对象ID集合
	 * @param loginInfo
	 * @return
	 * @throws Exception
	 */
	public List<String> getFlowObjectIDListByRelease(A_LoginInfo loginInfo) throws Exception;

	/**
	 * 获取流程记录中，审批中的流程对象ID集合
	 * @param loginInfo
	 * @return
	 * @throws Exception
	 */
	public List<String> getFlowObjectIDListByPending(A_LoginInfo loginInfo) throws Exception;

	/**
	 * 获取流程记录中，可撤回的流程对象ID集合
	 * @param loginInfo
	 * @return
	 * @throws Exception
	 */
	public List<String> getFlowObjectIDListByRecall(A_LoginInfo loginInfo) throws Exception;

	/**
	 * 获取流程记录中，处理过的流程对象ID集合
	 * @param loginInfo
	 * @return
	 * @throws Exception
	 */
	public List<String> getFlowObjectIDListByPassed(A_LoginInfo loginInfo) throws Exception;
	
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public F_flowrecord getObjectById(ParametersUtil parameters) throws Exception;
	
	/**
	 * 根据nextRecordID加载对象
	 * @param nextRecordID
	 * @return
	 * @throws Exception
	 */
	public F_flowrecord getObjectByNextRecordId(String nextRecordID) throws Exception;

	/**
	 * 根据ID加载记录对象、处理对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getRecordDealtByid(ParametersUtil parameters, A_LoginInfo loginInfo) throws Exception;

	/**
	 * 修改流程记录状态，待审核提取操作
	 * @param loginInfo
	 * @throws Exception
	 */
	public void updateDealtStatusExtractByID(ParametersUtil param, A_LoginInfo loginInfo) throws Exception;

	/**
	 * 修改流程记录状态，审核中释放操作
	 * @param loginInfo
	 * @throws Exception
	 */
	public void updateDealtStatusReleaseByID(ParametersUtil param, A_LoginInfo loginInfo) throws Exception;

	/**
	 * 流程记录保存，及保存处理记录
	 * @param flowRecordID
	 * @param flowObj
	 * @param project
	 * @param node
	 * @param loginInfo
	 * @throws Exception
	 */
	public void saveRecordAndDealt(String flowRecordID, F_flowobject flowObj, 
			F_flowproject project, F_flownode node, A_LoginInfo loginInfo) throws Exception;	
	/**
	 * 流程记录保存，及保存处理记录
	 * @param flowRecordID
	 * @param flowObj
	 * @param project
	 * @param node
	 * @param termIDList
	 * @param zjBraID
	 * @param zjBraName
	 * @param zjEmpID
	 * @param zjEmpName
	 * @param loginInfo
	 * @throws Exception
	 */
	public void saveRecordAndDealt(String flowRecordID, F_flowobject flowObj, 
			F_flowproject project, F_flownode node, ArrayList<String> termIDList, 
			String zjBraID, String zjBraName, String zjEmpID, String zjEmpName,
			A_LoginInfo loginInfo) throws Exception;

	/**
	 * 保存处理结果
	 * @param param
	 * @param loginInfo
	 * @throws Exception
	 */
	public void saveDealtResults(ParametersUtil param, A_LoginInfo loginInfo) throws Exception;
	
	/**
	 * 执行自动节点流程处理
	 * @param flowRecordID
	 * @param flowObj
	 * @param project
	 * @param loginInfo
	 * @param operateClass
	 * @param node
	 * @param nextTermIDList
	 * @param jObj
	 * @return
	 * @throws Exception
	 */
	public F_flownode executeAutoNode(StringBuilder flowRecordID, F_flowobject flowObj, F_flowproject project, 
			A_LoginInfo loginInfo, WorkFlowService operateClass, F_flownode node, 
			ArrayList<String> nextTermIDList, JSONObject jObj) throws Exception;
}
