package com.fsd.admin.service;

import java.util.List;
import java.util.Map;

import com.fsd.core.bean.FsdFile;
import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Sys_SystemLog;

public interface Sys_TemplateService extends BaseService<Sys_SystemLog, String>{
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<FsdFile> getObjectPageList(ParametersUtil param, A_Employee employee, String rootPaht) throws Exception;
	/**
	 * 加载文件内容
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public Map<String, String> getFileContent(ParametersUtil parameters, String rootPath) throws Exception;
	/**
	 * 重命名文件
	 * @param rootPath
	 * @param url
	 * @param name
	 * @throws Exception
	 */
	public void renameFile(String rootPath, String url, String name) throws Exception;
	/**
	 * 删除文件
	 * @param parameters
	 * @param rootPath
	 * @throws Exception
	 */
	public void delFiles(ParametersUtil parameters, String rootPath) throws Exception;
	/**
	 * 提交文件内容
	 * @param url
	 * @param content
	 * @param rootPath
	 * @throws Exception
	 */
	public void saveFile(String url, String content, String rootPath) throws Exception;
	
}
