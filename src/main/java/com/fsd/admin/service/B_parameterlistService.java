package com.fsd.admin.service;

import java.util.Map;

import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.B_parameterlist;

public interface B_parameterlistService extends BaseService<B_parameterlist, String>{
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
	
	/**
	 * 保存对象（添加、修改）
	 * @param obj
	 */
	public void saveObject(B_parameterlist obj , A_Employee employee) throws Exception;
	
	/**
	 * 删除对象
	 * @param parameters
	 */
	public void delObject(ParametersUtil parameters) throws Exception;
	
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public B_parameterlist getObjectById(ParametersUtil parameters) throws Exception;

	/**
	 * 修改对象状态
	 * @param parameters
	 */
	public void updateEnabledObject(ParametersUtil parameters) throws Exception;
	
	/**
	 * Excel导出
	 * @param employee
	 * @param rootPath
	 * @param httpRootPath
	 * @return
	 * @throws Exception
	 */
	public Map<String, String> download(A_Employee employee, String rootPath, String httpRootPath) throws Exception;
	
	/**
	 * 顺序排序
	 * @param parameters
	 */
	public void updateSortObject(ParametersUtil parameters) throws Exception;

}
