package com.fsd.admin.service;

import com.fsd.admin.entity.E_autoreplyEntity;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.E_Autoreply;
import com.fsd.admin.model.E_wechat;
import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;

public interface E_AutoreplyService extends BaseService<E_Autoreply, String> {

	/**
	 * 文本消息处理消息
	 * @throws Exception 
	 */
	E_autoreplyEntity getReplyTextMessage(String wechatid, String message) throws Exception;
	
	/**
	 * 事件消息处理消息
	 * @throws Exception 
	 */
	E_autoreplyEntity getReplyClickMessage(String wechatid, String code) throws Exception;
	
	/**
	 * 系统消息处理消息
	 * @throws Exception 
	 */
	E_autoreplyEntity getReplySystemMessage(String wechatid, String code) throws Exception;
	
	/**
	 * 保存对象（添加、修改）
	 * @param obj
	 * @param a_employee
	 * @param a_company
	 * @throws Exception
	 */
	void saveObject(E_Autoreply obj, ParametersUtil param, A_Employee employee) throws Exception;
	
	/**
	 * 彻底删除对象
	 * @param parameters
	 */
	void delObject(ParametersUtil parameters) throws Exception;
	
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
	
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getObjectById(ParametersUtil parameters) throws Exception;
	
	/**
	 * 修改对象状态
	 * @param parameters
	 */
	void updateEnabledObject(ParametersUtil parameters) throws Exception;
	
	/**
	 * 顺序排序
	 * @param parameters
	 */
	void updateSortObject(ParametersUtil parameters) throws Exception;
	
	/**
	 * 微信账号初始化，添加自动回复系统值
	 * @param wechat
	 */
	void saveInitSystemAutoReply(E_wechat wechat) throws Exception;
}
