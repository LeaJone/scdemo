package com.fsd.admin.service;

import java.util.List;

import com.fsd.core.util.ParametersUtil;
import com.fsd.core.bean.Tree;
import com.fsd.core.service.BaseService;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.J_board;

public interface J_boardService extends BaseService<J_board, String>{
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;/**
	 * 异步加载对象树
	 * @return
	 * @throws Exception
	 */
	List<Tree> getAsyncObjectTree(String fid, A_Employee employee) throws Exception;
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	void saveObject(J_board obj, A_Employee employee) throws Exception;
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	J_board getObjectById(ParametersUtil parameters) throws Exception;
	/**
	 * 顺序排序
	 * @param parameters
	 */
	void updateSortObject(ParametersUtil parameters) throws Exception;
}
