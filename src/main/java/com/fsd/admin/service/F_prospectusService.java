package com.fsd.admin.service;

import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.F_prospectus;

public interface F_prospectusService extends BaseService<F_prospectus, String>{
	/**
	 * 加载执行计划书
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getProspectusPageList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 删除执行计划书
	 * @param parameters
	 */
	void deletedProspectus(ParametersUtil parameters) throws Exception;
	/**
	 * 提交执行计划书
	 * @param obj
	 */
	void saveProspectus(F_prospectus obj, A_Employee employee) throws Exception;
	/**
	 * 保存执行计划书
	 * @param obj
	 */
	void saveProspectusFirst(F_prospectus obj, A_Employee employee) throws Exception;
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getProspectusById(ParametersUtil parameters) throws Exception;
}
