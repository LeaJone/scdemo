package com.fsd.admin.service;

import com.fsd.core.bean.ImportExcel;
import com.fsd.core.util.ParametersUtil;
import com.fsd.core.service.BaseService;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_tkq;

public interface Z_tkqService extends BaseService<Z_tkq, String>{
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public void save(Z_tkq obj, A_Employee employee) throws Exception;
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public Z_tkq getObjectById(ParametersUtil parameters) throws Exception;
	/**
	 * 导入Excel
	 * @param obj
	 * @param loginUser
	 * @param realPath
	 * @throws Exception
	 */
	public void saveImportObject(ImportExcel obj, A_Employee loginUser, String realPath) throws Exception;
}
