package com.fsd.admin.service;

import java.util.List;

import com.fsd.core.util.ParametersUtil;
import com.fsd.core.bean.Tree;
import com.fsd.core.service.BaseService;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_qdnrb;

public interface Z_qdnrbService extends BaseService<Z_qdnrb, String>{
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public void save(Z_qdnrb obj, A_Employee employee) throws Exception;
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public Z_qdnrb getObjectById(ParametersUtil parameters) throws Exception;
	
	/**
	 * 修改状态
	 * @param parameters
	 */
	public void updateEnabledObject(ParametersUtil parameters) throws Exception;
}
