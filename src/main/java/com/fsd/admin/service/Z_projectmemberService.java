package com.fsd.admin.service;

import com.fsd.core.util.ParametersUtil;
import com.fsd.core.service.BaseService;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_projectmember;

import java.util.List;

public interface Z_projectmemberService extends BaseService<Z_projectmember, String>{
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	void save(Z_projectmember obj, A_Employee employee) throws Exception;
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	Z_projectmember getObjectById(ParametersUtil parameters) throws Exception;
	/**
	 * 根据项目编码加载项目成员列表
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	List<Z_projectmember> getListByProjectId(String projectId) throws Exception;
	/**
	 * 根据项目ID删除项目成员
	 * @param projectId
	 * @throws Exception
	 */
	void delObjectByProjectId(String projectId) throws Exception;
}
