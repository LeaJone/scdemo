package com.fsd.admin.service;

import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_qyrz;
import com.fsd.admin.model.Z_qyrzjl;
import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;

public interface Z_qyrzjlService extends BaseService<Z_qyrzjl, String>{
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	void save(Z_qyrzjl obj, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	Z_qyrzjl getObjectById(ParametersUtil parameters) throws Exception;
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	Z_qyrzjl getObject(ParametersUtil parameters) throws Exception;

	/**
	 * 根据条件查找对象
	 * @return
	 * @throws Exception
	 */
	boolean getObjectByQyid(String id, String type) throws Exception;
}
