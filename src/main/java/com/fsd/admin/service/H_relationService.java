package com.fsd.admin.service;

import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;

import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.H_relation;

public interface H_relationService extends BaseService<H_relation, String>{
	
	/**
	 * 保存对象添加
	 * @param 
	 */
	public void saveObject(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 保存对象添加
	 * @param branch
	 */
	public void saveObjectList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 保存对象添加
	 * @param branch
	 */
	public void saveObject(String employeeid, A_Employee employee) throws Exception;
	/**
	 * 保存对象添加
	 * @param branch
	 */
	public void saveObject(A_Employee emplObject, A_Employee employee) throws Exception;
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
}
