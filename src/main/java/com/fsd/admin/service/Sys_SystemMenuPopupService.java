package com.fsd.admin.service;

import java.util.List;
import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Sys_SystemMenu;
import com.fsd.admin.model.Sys_SystemMenuPopup;

public interface Sys_SystemMenuPopupService extends BaseService<Sys_SystemMenuPopup, String>{
	/**
	 * 保存用户快捷菜单
	 * @param employee
	 * @param parameters
	 * @throws Exception
	 */
	public void saveObject(A_Employee employee, ParametersUtil parameters) throws Exception;
	/**
	 * 根据用户ID加载用户的快捷菜单列表
	 * @param Employeeid
	 * @return
	 */
	public List<Sys_SystemMenuPopup> getObjectListByEmployeeid(String Employeeid) throws Exception;
}
