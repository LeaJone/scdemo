package com.fsd.admin.service;

import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Sys_SystemLog;

import java.util.Map;

public interface Sys_SystemLogService extends BaseService<Sys_SystemLog, String>{
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 加载分页数据，普通操作
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageListCZ(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 加载分页数据，安全操作
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageListAQ(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 保存角色
	 * @throws Exception
	 */
	public void saveObject(String logType, String logDepict, A_Employee employee, Class clazz) throws Exception;
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public Sys_SystemLog getObjectById(ParametersUtil parameters)  throws Exception;
	/**
	 * Excel导出
	 * @param employee
	 * @param rootPath
	 * @param httpRootPath
	 * @return
	 * @throws Exception
	 */
	public Map<String, String> download(ParametersUtil param, A_Employee employee, String rootPath, String httpRootPath) throws Exception;
}
