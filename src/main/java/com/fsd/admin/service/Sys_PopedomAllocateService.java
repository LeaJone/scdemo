package com.fsd.admin.service;

import java.util.List;
import java.util.Map;
import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Sys_PopedomAllocate;

public interface Sys_PopedomAllocateService extends BaseService<Sys_PopedomAllocate, String>{
	/**
	 * 通过sql查询所需集合
	 * @param sql
	 * @return
	 */
	List<Sys_PopedomAllocate> getObjectBySql(String sql);
	/**
	 * 获取用户权限组ID集合
	 * @param user
	 * @return
	 * @throws Exception
	 */
	List<String> getUserPopedomGroupIds(A_Employee user) throws Exception;
	/**
	 * 获取用户权限集合
	 * @param user
	 * @return
	 * @throws Exception
	 */
	Map<String, List<String>> getUserPopedomMap(A_Employee user) throws Exception;
	/**
	 * 保存角色权限信息
	 * @param parameters
	 * @throws Exception
	 */
	void saveRolePopedom(ParametersUtil parameters) throws Exception;
	/**
	 * 保存人员权限信息
	 * @param parameters
	 * @throws Exception
	 */
	void saveUserPopedom(ParametersUtil parameters) throws Exception;
	/**
	 * 根据人员ID集合获取所属角色ID集合
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	List<String> getRoleIdsByUserIds(ParametersUtil parameters) throws Exception;
	/**
	 * 根据角色ID集合获取所属权限ID集合
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	Map<String, List<String>> getPopedomIdsByRoleIds(ParametersUtil parameters) throws Exception;
	/**
	 * 根据权限ID加载权限管理信息
	 * @param popedomId
	 * @return
	 * @throws Exception
	 */
	Sys_PopedomAllocate getObjectByPopedomIdAndType(String popedomId, String type) throws Exception;
}
