package com.fsd.admin.service;

import java.util.List;

import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.I_knowledgerelate;
import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;

public interface I_knowledgerelateService extends BaseService<I_knowledgerelate, String>{
	
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
	
	/**
	 * 加载不分页数据
	 * @return
	 * @throws Exception
	 */
	public List getObjectList(ParametersUtil param) throws Exception;
	
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public void save(ParametersUtil parameters, A_Employee employee) throws Exception;
	
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
	
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public I_knowledgerelate getObjectById(ParametersUtil parameters) throws Exception;
}
