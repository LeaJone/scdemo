package com.fsd.admin.service;

import java.util.List;

import com.fsd.core.bean.Tree;
import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.B_interlink;

public interface B_interlinkService extends BaseService<B_interlink, String>{

	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
	
	/**
	 * 加载部门树
	 * @return
	 * @throws Exception
	 */
	public List<Tree> getObjectTree(String fid) throws Exception;
	
	/**
	 * 异步加载部门树
	 * @return
	 * @throws Exception
	 */
	public List<Tree> getAsyncObjectTree(String fid) throws Exception;
	
	/**
	 * 保存对象（添加、修改）
	 * @param obj
	 */
	public void saveObject(B_interlink obj , A_Employee employee) throws Exception;
	
	/**
	 * 删除对象
	 * @param parameters
	 */
	public void delObject(ParametersUtil parameters) throws Exception;
	
	/**
	 * 审核对象
	 * @param parameters
	 */
	public void auditObject(ParametersUtil parameters) throws Exception;
	
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public B_interlink getObjectById(ParametersUtil parameters) throws Exception;
}
