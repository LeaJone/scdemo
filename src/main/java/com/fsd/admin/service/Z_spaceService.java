package com.fsd.admin.service;

import com.fsd.core.bean.Tree;
import com.fsd.core.util.ParametersUtil;
import com.fsd.core.service.BaseService;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_space;

import java.util.List;

public interface Z_spaceService extends BaseService<Z_space, String>{
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	Z_space save(Z_space obj, A_Employee employee) throws Exception;
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	Z_space getObjectById(ParametersUtil parameters) throws Exception;
	/**
	 * 获得空间树结构
	 * @return
	 * @throws Exception
	 */
	List<Tree> getSpaceTree() throws Exception;
}
