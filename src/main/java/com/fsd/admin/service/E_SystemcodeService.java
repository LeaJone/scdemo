package com.fsd.admin.service;

import java.util.List;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.E_Systemcode;
import com.fsd.core.service.BaseService;

public interface E_SystemcodeService extends BaseService<E_Systemcode, String> {

	/**
	 * 加载对象集合
	 * @return
	 * @throws Exception
	 */
	List<E_Systemcode> getObjectListByCompanyID(A_Employee employee) throws Exception;
}
