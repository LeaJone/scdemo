package com.fsd.admin.service;

import java.util.Map;

import com.fsd.admin.entity.A_LoginInfo;
import com.fsd.admin.model.F_flowdealt;
import com.fsd.admin.model.F_flownodeterm;
import com.fsd.admin.model.F_flowrecord;
import com.fsd.core.util.ParametersUtil;


public interface F_flowWorkService {
	
	/**
     * 检查工作流是否启动状态
     * @param flowCode
     * @param companyID
     * @return boolean
     * @throws Exception
     */
    public boolean checkFlowProjectQYZT(String flowCode, String companyID) throws Exception;
    
	/**
	 * 提交保存工作流程对象
	 * @param flowCode 流程编码
	 * @param objID 数据对象ID
	 * @param typeID 数据类型ID
	 * @param typeName 数据类型名称
	 * @param objCode 数据对象Code
	 * @param objDepict 数据对象描述
	 * @param objJson 数据对象Json数据
	 * @param companyID 数据对象所属单位
	 * @param realName 申请人姓名
	 * @param isCheck 是否检查工作流程存在
	 * @throws Exception
	 */
    public void saveWorkFlowSubmit(String flowCode, String objID, String typeID, String typeName, String objCode, 
    		String objDepict, Map<String, Object> mapJson, String companyID, String realName, boolean isCheck) throws Exception;
}
