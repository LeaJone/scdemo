package com.fsd.admin.service;

import com.fsd.core.util.ParametersUtil;
import com.fsd.core.service.BaseService;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.F_activiti;

import java.util.List;
import java.util.Map;

public interface F_activitiService extends BaseService<F_activiti, String>{
    /**
	 * 加载流程设计分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getDesignPageList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 加载流程定义列表
	 * @param param
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getProcessDefinitionPageList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 加载流程实例列表
	 * @param param
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getProcessInstancePageList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 加载用户任务列表
	 * @param param
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getTaskPageList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 任务处理
	 * @param taskId
	 * @param state
	 * @throws Exception
	 */
	void disposeTask(String taskId, String state, String message) throws Exception;
	/**
	 * 选择任务处理
	 * @param taskId
	 * @param ids
	 * @throws Exception
	 */
	void disposeChooseTask(String taskId, String ids) throws Exception;
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	void save(F_activiti obj, A_Employee employee) throws Exception;
	/**
	 * 发布流程
	 * @param parameters
	 * @throws Exception
	 */
	void deployObject(ParametersUtil parameters, A_Employee employee) throws Exception;
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	void deleteModel(ParametersUtil parameters, A_Employee employee) throws Exception;
	/**
	 * 删除已部署的模型
	 * @param parameters
	 * @param employee
	 * @throws Exception
	 */
	void deleteDeployment(ParametersUtil parameters, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	F_activiti getObjectById(ParametersUtil parameters) throws Exception;
	/**
	 * 根据任务ID加载流程历史审核信息
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	List<Map<String, Object>> getHistoryListByTaskId(ParametersUtil parameters) throws Exception;
}
