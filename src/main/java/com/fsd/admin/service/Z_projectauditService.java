package com.fsd.admin.service;

import com.fsd.core.util.ParametersUtil;
import com.fsd.core.service.BaseService;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_projectaudit;

public interface Z_projectauditService extends BaseService<Z_projectaudit, String>{
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
    ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 编辑或更新
	 * @param projectid
	 * @param state
	 * @param remark
	 * @throws Exception
	 */
	public void saveOrUpdateProjectAudit(String type, String projectid, String state, String remark) throws Exception;
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
    void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
    Z_projectaudit getObjectById(ParametersUtil parameters) throws Exception;
	/**
	 * 根据项目ID加载项目审核情况
	 * @param projectid
	 * @return
	 * @throws Exception
	 */
	Z_projectaudit getProjectAuditByProjectId(String projectid) throws Exception;
}
