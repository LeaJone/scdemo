package com.fsd.admin.service;

import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Sys_Databackup;
import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;

public interface Sys_DatabackupService extends BaseService<Sys_Databackup, String> {
	/**
	 * 加载备份数据（分页）
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getDataPageList(ParametersUtil param) throws Exception;
	
	/**
	 * 根据Id加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	Sys_Databackup getObjectById(ParametersUtil parameters) throws Exception;
	
	/**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	void delObject(ParametersUtil parameters) throws Exception;
	
	/**
	 * 数据备份操作
	 * @throws Exception
	 */
	void saveDatabackup(A_Employee employee) throws Exception;
	
	/**
	 * 恢复数据(数据还原)
	 * @throws Exception
	 */
	void recoveryData(ParametersUtil parameters) throws Exception;
}
