package com.fsd.admin.service;

import java.util.List;
import java.util.Map;

import com.fsd.core.util.ParametersUtil;
import com.fsd.core.model.BaseModel;
import com.fsd.core.service.BaseService;
import com.fsd.admin.model.A_Employee;

public interface Z_fbtjService extends BaseService<BaseModel, String>{
	
	/**
	 * 统计查询(单位)
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, Object>> getObjectStat(ParametersUtil param, A_Employee employee) throws Exception;
	
	/**
	 * 下载(单位)
	 * @param parameters
	 * @param employee
	 * @param rootPath
	 * @param httpRootPath
	 * @return
	 * @throws Exception
	 */
	public Map<String, String> download(ParametersUtil parameters, A_Employee employee, String rootPath, String httpRootPath) throws Exception;
	
	/**
	 * 统计查询(作者)
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, Object>> getObjectStatAuthor(ParametersUtil param, A_Employee employee) throws Exception;
	
	/**
	 * 生成下载文件(作者)
	 */
	public Map<String, String> downloadAuthor(ParametersUtil parameters, A_Employee employee, String rootPath, String httpRootPath) throws Exception;
	
	/**
	 * 统计查询(发布登记部门)
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, Object>> getObjectStatFb(ParametersUtil param, A_Employee employee) throws Exception;
	
	/**
	 * 生成下载文件(发布登记部门)
	 */
	public Map<String, String> downloadFb(ParametersUtil parameters, A_Employee employee, String rootPath, String httpRootPath) throws Exception;
	

	/**
	 * 生成厅机关文章
	 * @throws Exception
	 */
	public void saveScWzTjg(ParametersUtil parameters, A_Employee employee) throws Exception;
	/**
	 * 生成直属事业单位文章
	 * @throws Exception
	 */
	public void saveScWzSydw(ParametersUtil parameters, A_Employee employee) throws Exception;
}
