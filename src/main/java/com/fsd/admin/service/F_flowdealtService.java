package com.fsd.admin.service;

import java.util.List;

import com.fsd.core.service.BaseService;
import com.fsd.admin.model.F_flowdealt;

public interface F_flowdealtService extends BaseService<F_flowdealt, String>{
	
	/**
	 * 根据记录ID获取处理对象集合
	 * @param recordID
	 * @return
	 * @throws Exception
	 */
	public List<F_flowdealt> getObjectListByRecordID(String recordID) throws Exception;
	
	/**
	 * 根据记录ID及用户ID获取处理对象
	 * @param recordID
	 * @param employeeID
	 * @return
	 * @throws Exception
	 */
	public F_flowdealt getObjectByRecordIDEmployeeID(String recordID, String employeeID) throws Exception;
}
