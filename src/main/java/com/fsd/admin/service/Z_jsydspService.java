package com.fsd.admin.service;

import java.util.Map;

import com.fsd.admin.entity.Z_jsydspkz;
import com.fsd.core.bean.ImportExcel;
import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_jsydsp;

public interface Z_jsydspService extends BaseService<Z_jsydsp, String>{
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public void save(Z_jsydsp obj, Z_jsydspkz objkz, A_Employee employee) throws Exception;
	/**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public Map<String, String> getObjectById(ParametersUtil parameters) throws Exception;
	/**
	 * 导入Excel
	 * @param obj
	 * @param employee
	 * @param diskUrl
	 * @throws Exception
	 */
	public void saveImportObject(ImportExcel obj, A_Employee employee, String diskUrl) throws Exception;
}
