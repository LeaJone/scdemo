package com.fsd.admin.service;

import java.util.List;

import com.fsd.core.common.BusinessException;
import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.B_wordkeybad;

public interface B_wordkeybadService extends BaseService<B_wordkeybad, String>{
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;

	/**
	 * 按照类型获取集合
	 * @param wordType
	 * @return
	 * @throws Exception
	 */
	public List<B_wordkeybad> getWordkeybadListByType(String wordType, A_Employee employee) throws Exception;
	
	/**
	 * 保存对象（添加、修改）
	 * @param obj
	 */
	public void saveObject(B_wordkeybad obj , A_Employee employee) throws Exception;
	
	/**
	 * 删除对象
	 * @param parameters
	 */
	public void delObject(ParametersUtil parameters) throws Exception;
	
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public B_wordkeybad getObjectById(ParametersUtil parameters) throws Exception;

	/**
	 * 修改对象状态
	 * @param parameters
	 */
	public void updateEnabledObject(ParametersUtil parameters) throws Exception;  
	
	/**
	 * 检查文本值输入过滤
	 * @throws BusinessException 
	 */
	public void checkTextByWordType(String wordType, String describe, String text, A_Employee employee) throws BusinessException;
}
