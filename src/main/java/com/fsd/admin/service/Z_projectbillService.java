package com.fsd.admin.service;

import com.fsd.core.util.ParametersUtil;
import com.fsd.core.service.BaseService;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_projectbill;

import java.util.List;

public interface Z_projectbillService extends BaseService<Z_projectbill, String>{
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getObjectPageList1(ParametersUtil param, A_Employee employee) throws Exception;
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	void save(Z_projectbill obj, A_Employee employee) throws Exception;
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	Z_projectbill getObjectById(ParametersUtil parameters) throws Exception;
	/**
	 * 更新审批状态
	 * @param parameters
	 * @throws Exception
	 */
	void updateEnabledObject(ParametersUtil parameters) throws Exception;
	/**
	 * 根据项目ID加载项目报账信息
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	List<Z_projectbill> getListByProjectId(String projectId) throws Exception;
	/**
	 * 根据项目ID和报账级别加载项目报账信息
	 * @param projectId
	 * @param level
	 * @return
	 * @throws Exception
	 */
	List<Z_projectbill> getListByProjectIdAndLevel(String projectId, String level) throws Exception;
}
