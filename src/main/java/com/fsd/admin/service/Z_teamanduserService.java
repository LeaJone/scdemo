package com.fsd.admin.service;

import com.fsd.core.util.ParametersUtil;
import com.fsd.core.service.BaseService;

import java.util.List;

import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_teamanduser;

public interface Z_teamanduserService extends BaseService<Z_teamanduser, String>{
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
    /**
	 * 编辑对象
	 * @param param
	 * @param employee
	 * @throws Exception
	 */
	public ParametersUtil save(ParametersUtil param, A_Employee employee) throws Exception;
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public Z_teamanduser getObjectById(ParametersUtil parameters) throws Exception;
	/**
	 * 加载列表数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<Z_teamanduser> getTeamanduserList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 加载列表数据
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public List<Z_teamanduser> getTeamById(ParametersUtil parameters, A_Employee employee) throws Exception;
}
