package com.fsd.admin.service;

import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;
import com.fsd.core.wechat.entity.message.resp.TemplateMessage;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.E_pushtemplates;

public interface E_pushtemplatesService extends BaseService<E_pushtemplates, String>{
	
	
	//============================  基本信息管理  ============================
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
	
	/**
	 * 保存对象（添加、修改）
	 * @param obj
	 */
	public void saveObject(E_pushtemplates obj, A_Employee employee) throws Exception;
	
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public E_pushtemplates getObjectById(ParametersUtil parameters) throws Exception;

	/**
	 * 修改对象状态
	 * @param parameters
	 */
	public void updateEnabledObject(ParametersUtil parameters, A_Employee employee) throws Exception;
	

	

	//============================  推送管理  ============================
	/**
	 * 消息推送
	 * @param wechatApiId 微信号ApiID
	 * @param templateCode 推送模板编码
	 * @param employeeId 被推送人ID
	 * @param message 推送消息对象
	 * @throws Exception
	 */
	public void sendTemplateMessage(String wechatApiId, String templateCode, String employeeId, TemplateMessage message) throws Exception;
	/**
	 * 消息推送
	 * @param wechatApiId 微信号ApiID
	 * @param templateCode 推送模板编码
	 * @param employee 被推送人
	 * @param message 推送消息对象
	 * @throws Exception
	 */
	public void sendTemplateMessage(String wechatApiId, String templateCode, A_Employee employee, TemplateMessage message) throws Exception;
	/**
	 * 消息推送
	 * @param wechatApiId 微信号ApiID
	 * @param templateCode 推送模板编码
	 * @param employeeId 被推送人ID
	 * @param message 推送消息对象
	 * @param type业务类型
	 * @param businessId业务数据编号
	 * @param popedom操作权限
	 * @throws Exception
	 */
	public void sendTemplateMessage(String wechatApiId, String templateCode, String employeeId, TemplateMessage message, 
			String type, String businessId, String popedom) throws Exception;
	/**
	 * 消息推送
	 * @param wechatApiId 微信号ApiID
	 * @param templateCode 推送模板编码
	 * @param employee 被推送人
	 * @param message 推送消息对象
	 * @param type业务类型
	 * @param businessId业务数据编号
	 * @param popedom操作权限
	 * @throws Exception
	 */
	public void sendTemplateMessage(String wechatApiId, String templateCode, A_Employee employee, TemplateMessage message, 
			String type, String businessId, String popedom) throws Exception;
}
