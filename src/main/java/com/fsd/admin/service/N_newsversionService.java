package com.fsd.admin.service;

import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.N_newsversion;
import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;

public interface N_newsversionService extends BaseService<N_newsversion, String>{
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getObjectPageList(ParametersUtil param) throws Exception;

	/**
	 * 保存对象（添加、修改）
	 * @param obj
	 */
	void saveObject(N_newsversion obj, A_Employee employee) throws Exception;
	
	/**
	 * 删除对象
	 * @param parameters
	 */
	void delObject(ParametersUtil parameters) throws Exception;
	
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	N_newsversion getObjectById(ParametersUtil parameters) throws Exception;
}
