package com.fsd.admin.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.fsd.core.bean.CheckTree;
import com.fsd.core.bean.Tree;
import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.entity.A_LoginInfo;
import com.fsd.admin.model.A_Company;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.B_parameterlist;
import com.fsd.admin.model.B_subject;

public interface B_subjectService extends BaseService<B_subject, String>{
	/**
	 * 获取权限树
	 * @param user
	 * @return
	 */
	public List<CheckTree> getQxObjectCheck(ParametersUtil parameters, A_Employee employee, Map<String, List<String>> popdomMap) throws Exception;
	/**
	 * 获取权限树
	 * @param user
	 * @return
	 */
	public List<CheckTree> getAllQxObjectCheck(ParametersUtil parameters, A_Employee employee) throws Exception;

	/**
	 * 获取栏目结构
	 * @param id
	 * @return
	 */
	public B_subject getObjectStructByID(String id, String companyid) throws Exception;
	/**
	 * 获取栏目子集
	 * @param fid
	 * @param companyid
	 * @return
	 * @throws Exception
	 */
	public List<B_subject> queryObjectListById(String fid, String companyid) throws Exception;
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 异步加载对象树
	 * @return
	 * @throws Exception
	 */
	public List<Tree> getAsyncObjectTree(String fid, boolean isQuery, A_Employee employee) throws Exception;
	/**
	 * 异步加载对象树
	 * @return
	 * @throws Exception
	 */
	public List<Tree> getAsyncObjectTreeByPopdom(String fid, boolean isQuery, A_Employee employee, Map<String, List<String>> popdomMap) throws Exception;
	/**
	 * 一次性加载部门树，根据权限加载
	 * @return
	 * @throws Exception
	 */
	public List<Tree> getAllObjectTreeByPopdom(ParametersUtil param, boolean isQuery, A_LoginInfo login) throws Exception;
	/**
	 * 查询窗口表格显示，根据权限加载
	 * @return
	 * @throws Exception
	 */
	public List<B_subject> getAllObjectListByPopdom(ParametersUtil param, boolean isQuery, A_LoginInfo login) throws Exception;
	/**
	 * 保存对象（添加、修改）
	 * @param obj
	 */
	public void saveObject(B_subject obj, A_Employee employee) throws Exception;
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public B_subject getObjectById(ParametersUtil parameters) throws Exception;
	/**
	 * 删除对象
	 * @param parameters
	 */
	public void delObject(ParametersUtil parameters) throws Exception;
	
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<Object> getObjectStat(ParametersUtil param, A_Employee employee) throws Exception;

	/**
	 * 顺序排序
	 * @param parameters
	 */
	public void updateSortObject(ParametersUtil parameters) throws Exception;
	/**
	 * 修改状态
	 * @param parameters
	 */
	public void updateEnabledObject(ParametersUtil parameters, A_Employee employee) throws Exception;
	/**
	 * 公开栏目
	 * @param parameters
	 */
	public void updateGklmObject(ParametersUtil parameters, A_Employee employee) throws Exception;
	/**
	 * 栏目文章关联查询
	 * @param parameters
	 */
	public ParametersUtil getObjectRelateArticleNum(ParametersUtil parameters, A_Employee employee) throws Exception;
	/**
	 * 栏目文章及关联文章迁移
	 * @param parameters
	 */
	public void updateObjectMoveRelateArticle(ParametersUtil parameters, A_Employee employee) throws Exception;
	/**
	 * Excel导出
	 */
	public Map<String, String> download(ParametersUtil parameters, A_Employee employee, String rootPath, String httpRootPath) throws Exception;
}
