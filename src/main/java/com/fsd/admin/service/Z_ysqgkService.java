package com.fsd.admin.service;

import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.entity.A_LoginInfo;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_jsydba;
import com.fsd.admin.model.Z_ysqgk;

public interface Z_ysqgkService extends BaseService<Z_ysqgk, String>, F_flowWorkService{
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 加载分页数据，加载所属回复人数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageListByEmployee(ParametersUtil param, A_LoginInfo loginInfo) throws Exception;
	/**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public void save(Z_ysqgk obj, A_Employee employee) throws Exception;
	/**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public Z_ysqgk getObjectById(ParametersUtil parameters) throws Exception;
	/**
	 * 修改状态
	 * @param parameters
	 */
	public void updateEnabledObject(ParametersUtil parameters) throws Exception;
	/**
	 * 提交回复
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public void saveHuifu(Z_ysqgk obj, A_Employee employee) throws Exception;
	
	/**
	 * 处理未进入流程的信箱数据
	 * @param parameters 
	 * @throws Exception
	 */
	public void saveDealtObject(ParametersUtil parameters, A_Employee employee) throws Exception;
}
