package com.fsd.admin.service;

import java.util.List;

import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.entity.A_LoginInfo;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_email;

public interface A_EmployeeService extends BaseService<A_Employee, String>{
	
	/**
	 * 根据登录名和密码得到用户
	 * @param name 登录名
	 * @param password 登陆密码
	 * @return
	 * @throws Exception
	 */
	public A_Employee getObjectLogin(String name , String password) throws Exception;
	
	/**
	 * 根据用户名加载对象
	 * @param username
	 * @return
	 * @throws Exception
	 */
	public A_Employee getObjectByUsername(String username);
	
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_LoginInfo loginUser) throws Exception;

	/**
	 * 加载分页数据，查询管理员、普通用户
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getTeamuserPageList(ParametersUtil param, A_LoginInfo loginInfo) throws Exception;
	
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList1(ParametersUtil param, A_LoginInfo loginUser) throws Exception;
	/**
	 * 加载分页数据，查询管理员、普通用户
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageAllList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 加载数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<A_Employee> getObjectAllList(ParametersUtil param, A_LoginInfo loginInfo) throws Exception;

	/**
	 * 加载部门所属数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<A_Employee> getObjectListByBranchID(ParametersUtil param, A_LoginInfo loginUser) throws Exception;
	
	/**
	 * 保存对象（添加、修改）
	 * @param branch
	 */
	public void saveObject(A_Employee obj , A_Employee a_employee) throws Exception;
	
	/**
	 * 用户信息批量导入
	 * @param branch
	 */
	public void saveXxdr(A_Employee obj , A_Employee a_employee, String rootpath) throws Exception;

	/**
	 * 保存自助修改
	 * @param branch
	 */
	public void saveObjectSelf(A_Employee obj , A_Employee a_employee) throws Exception;

	/**
	 * 修改用户所属单位名称
	 * @param parameters
	 */
	public void updateBranchName(String branchID, String branchName) throws Exception;
	
	/**
	 * 删除对象
	 * @param parameters
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
	
	/**
	 * 用户重置密码
	 * @param parameters
	 */
	public void updatePwdObject(ParametersUtil parameters, A_Employee employee) throws Exception;
	
	/**
	 * 修改人员状态
	 * @param parameters
	 */
	public void updateEnabledObject(ParametersUtil parameters, A_Employee employee) throws Exception;
	
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public A_Employee getObjectById(ParametersUtil parameters) throws Exception;
	
	/**
	 * 修改当前登录用户的密码
	 * @param old_pwd 旧密码
	 * @param new_pwd1 第一遍输入的新密码
	 * @param new_pwd2 第二遍输入的新密码
	 * @param a_employee 当前登录用户
	 * @throws Exception
	 */
	public A_Employee updateUserPwd(String old_pwd, String new_pwd1, String new_pwd2, A_Employee a_employee) throws Exception;
	/**
	 * 根据用户姓名，部门名称，手机，座机 加载数据列表
	 * @param param
	 * @param type
	 * @param text
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectList(ParametersUtil param, String type, String text) throws Exception;
	/**
	 * 获得加密字符串
	 * @param companyid
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public String getEncipherPassWord(String companyid, String password) throws Exception;
	
	/**
	 * 根据公司ID获取管理员信息
	 * @param companyid
	 * @return
	 * @throws Exception
	 */
	public Object getObjectByCompany(String id) throws Exception;
	

	//============================  会员信息  ============================
	/**
	 * 加载分页数据，查询会员
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageListHY(ParametersUtil param, A_Employee employee) throws Exception;
	
	/**
	 * 删除微信绑定
	 * @param employeeid 用户编号
	 * @return
	 * @throws Exception
	 */
	public void delWechatBind(String employeeid) throws Exception;
	
	/**
	 * 修改微信推送状态
	 * @param parameters
	 */
	public void updateIsWeChatPushObject(ParametersUtil parameters, A_Employee employee) throws Exception;
	
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public A_Employee getUserinfoById(String id) throws Exception;
	/**
	 * 统计用户类型数量（教师、学生、专家、企业）
	 * @return
	 * @throws Exception
	 */
	public List<A_Employee> getCountNumbers_xs() throws Exception; //学生
	public List<A_Employee> getCountNumbers_js() throws Exception; //教师
	public List<A_Employee> getCountNumbers_zj() throws Exception; //专家
	public List<A_Employee> getCountNumbers_qy() throws Exception; //企业
	
	/**
	 * 加载分页数据，查询管理员、普通用户(专家)
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getZhuanjiaPageList(ParametersUtil param, A_LoginInfo loginInfo) throws Exception;
	
	/**
	 * 保存对象（添加、修改）(专家)
	 */
	public String saveZhuanjia(ParametersUtil parameters, A_Employee employee , A_Employee a_employee) throws Exception;
	/**
	 * 根据用户类型加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageListByType(ParametersUtil param, A_LoginInfo loginInfo) throws Exception;
	/**
	 * 根据院系ID查询该院系的管理员
	 * @param academyid
	 * @return
	 * @throws Exception
	 */
	public A_Employee getDepartMentAdminAcademyid(String academyid) throws Exception;

	/**
	 * 用户数据和统一身份认证同步
	 * @param employee
	 * @throws Exception
	 */
	public void updateSyncEmployee(A_Employee employee) throws Exception;
}
