package com.fsd.admin.service;

import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.B_templatelist;
import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;

public interface B_templatelistService extends BaseService<B_templatelist, String>{

	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
	
	/**
	 * 保存对象（添加、修改）
	 * @param obj
	 */
	public void saveObject(B_templatelist obj , A_Employee employee) throws Exception;
	
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public B_templatelist getObjectById(ParametersUtil parameters) throws Exception;
}
