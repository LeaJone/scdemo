package com.fsd.admin.service;

import java.util.List;

import com.fsd.admin.model.E_Autoreplykey;
import com.fsd.core.service.BaseService;

public interface E_AutoreplykeyService extends BaseService<E_Autoreplykey, String> {

	/**
	 * 彻底删除对象
	 * @param autoReplyID
	 */
	void delObjectByAutoReplyID(String autoReplyID) throws Exception;
	
	/**
	 * 加载所需集合
	 * @return
	 * @throws Exception
	 */
	List<E_Autoreplykey> getObjectListByAutoReplyID(String autoReplyID) throws Exception;
	
	/**
	 * 加载微信号所需集合
	 * @return
	 * @throws Exception
	 */
	List<E_Autoreplykey> getObjectListByWeChatID(String weChatID) throws Exception;
}
