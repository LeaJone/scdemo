package com.fsd.admin.service;

import java.util.List;

import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.F_flownode;

public interface F_flownodeService extends BaseService<F_flownode, String>{
	
	/**
	 * 根据数据对象ID加载对象
	 * @param previousID
	 * @return
	 * @throws Exception
	 */
	public F_flownode getObjectByPreviousID(String previousID)  throws Exception;
	
	/**
	 * 根据项目ID加载数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<F_flownode> getObjectListByProjectID(ParametersUtil param, A_Employee employee) throws Exception;
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public void save(F_flownode obj, A_Employee employee) throws Exception;
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public F_flownode getObjectById(ParametersUtil parameters) throws Exception;
	
}
