package com.fsd.admin.service;

import com.fsd.core.util.ParametersUtil;
import com.fsd.core.service.BaseService;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_xkjnjs;

public interface Z_xkjnjsService extends BaseService<Z_xkjnjs, String>{
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 保存学科技能竞赛基本信息
	 * @param param
	 * @param obj
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	Z_xkjnjs saveInfo(ParametersUtil param, Z_xkjnjs obj, A_Employee employee) throws Exception;
	/**
	 * 保存学科技能竞赛负责人信息
	 * @param param
	 * @param obj
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	Z_xkjnjs saveLeader(ParametersUtil param, Z_xkjnjs obj, A_Employee employee) throws Exception;
	/**
	 * 保存项目简介信息
	 * @param param
	 * @param obj
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	Z_xkjnjs saveAbstract(ParametersUtil param, Z_xkjnjs obj, A_Employee employee) throws Exception;
	/**
	 * 保存实施方案及预期目标
	 * @param param
	 * @param obj
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	Z_xkjnjs saveScheme(ParametersUtil param, Z_xkjnjs obj, A_Employee employee) throws Exception;
	/**
	 * 保存配套条件
	 * @param param
	 * @param obj
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	Z_xkjnjs saveCondition(ParametersUtil param, Z_xkjnjs obj, A_Employee employee) throws Exception;
	/**
	 * 保存培训方案
	 * @param param
	 * @param obj
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	Z_xkjnjs saveTrain(ParametersUtil param, Z_xkjnjs obj, A_Employee employee) throws Exception;
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	void save(String rootPath, ParametersUtil param, Z_xkjnjs obj, A_Employee employee) throws Exception;
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	Z_xkjnjs getObjectById(ParametersUtil parameters) throws Exception;
	/**
	 * 提交项目
	 * @param param
	 * @throws Exception
	 */
	void saveProjectSubmit(String rootpath, ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 根据项目ID加载项目当前审核节点信息
	 * @param projctId
	 * @return
	 * @throws Exception
	 */
	String getProjectAuditStep(String projctId) throws Exception;
}
