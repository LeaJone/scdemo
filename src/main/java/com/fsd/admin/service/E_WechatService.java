package com.fsd.admin.service;

import java.util.List;

import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.E_wechat;
import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;

public interface E_WechatService extends BaseService<E_wechat, String> {

	/**
	 * 根据ApiID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	E_wechat getObjectByApiId(String apiid)  throws Exception;
	
	/**
	 * 根据ID获取微信账号对象，并检查账号完整性
	 * @param wxid
	 * @return
	 * @throws Exception
	 */
	E_wechat getWeChatCheckInfo(String wxid)  throws Exception;
	
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	List<E_wechat> getObjectList(ParametersUtil param, A_Employee employee) throws Exception;
	
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	E_wechat getObjectById(ParametersUtil parameters) throws Exception;
	
	/**
	 * 保存对象（添加、修改）
	 * @param obj
	 * @param a_employee
	 * @param a_company
	 * @throws Exception
	 */
	void saveObject(E_wechat obj, A_Employee employee) throws Exception;
	
	/**
	 * 删除对象
	 * @param parameters
	 */
	void delObject(ParametersUtil parameters) throws Exception;
	
	/**
	 * 测试自动回复接口
	 * @param parameters
	 */
	String testMessage(ParametersUtil param) throws Exception;
}
