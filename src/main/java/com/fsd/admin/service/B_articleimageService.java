package com.fsd.admin.service;

import java.util.List;

import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Company;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.B_articleimage;

public interface B_articleimageService extends BaseService<B_articleimage, String>{
	/**
	 * 根据文章ID加载数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<B_articleimage> getObjectListByArticleId(ParametersUtil param, A_Employee employee) throws Exception; 
	/**
	 * 根据文章ID加载数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageListByArticleId(ParametersUtil param, A_Employee employee) throws Exception; 
	/**
	 * 保存对象（添加、修改）
	 * @param obj
	 * @param a_employee
	 * @param a_company
	 * @throws Exception
	 */
	public void saveObject(B_articleimage obj, A_Employee employee) throws Exception;
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public B_articleimage getObjectById(ParametersUtil parameters) throws Exception;
	/**
	 * 删除对象
	 * @param parameters
	 */
	@SuppressWarnings("unchecked")
	public void delObject(ParametersUtil parameters) throws Exception;
	
	/**
	 * 修改状态
	 * @param parameters
	 */
	public void updateEnabledObject(ParametersUtil parameters) throws Exception;
}
