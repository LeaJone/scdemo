package com.fsd.admin.service.impl;

import java.util.List;

import javax.annotation.Resource;
import org.springframework.stereotype.Repository;

import com.fsd.core.common.BusinessException;
import com.fsd.core.service.impl.BaseServiceImpl;
import org.apache.log4j.Logger;
import com.fsd.admin.model.F_flowdealt;
import com.fsd.admin.service.F_flowdealtService;
import com.fsd.admin.dao.F_flowdealtDao;
import com.fsd.admin.entity.A_LoginInfo;

@Repository("f_flowdealtServiceImpl")
public class F_flowdealtServiceImpl extends BaseServiceImpl<F_flowdealt, String> implements F_flowdealtService{

	//流程记录处理状态 f_fdclzt
	/**
	 * 待处理
	 */
	public static final String fddcl1 = "f_fddcl";
	public static final String fddcl2 = "待处理";
	/**
	 * 同意
	 */
	public static final String fdty1 = "f_fdty";
	public static final String fdty2 = "同意";
	/**
	 * 拒绝
	 */
	public static final String fdjj1 = "f_fdjj";
	public static final String fdjj2 = "拒绝";
	/**
	 * 退回
	 */
	public static final String fdth1 = "f_fdth";
	public static final String fdth2 = "退回";
	/**
	 * 撤回
	 */
	public static final String fdch1 = "f_fdch";
	public static final String fdch2 = "撤回";
	/**
	 * 转交
	 */
	public static final String fdzj1 = "f_fdzj";
	public static final String fdzj2 = "转交";
	/**
	 * 提取
	 */
	public static final String fdtq1 = "f_fdtq";
	public static final String fdtq2 = "提取";
	/**
	 * 释放
	 */
	public static final String fdsf1 = "f_fdsf";
	public static final String fdsf2 = "释放";
	
	
    private static final Logger log = Logger.getLogger(F_flowdealtServiceImpl.class);
    private String depict = "记录处理";
    
    @Resource(name = "f_flowdealtDaoImpl")
	public void setBaseDao(F_flowdealtDao f_flowdealtDao) {
		super.setBaseDao(f_flowdealtDao);
	}
	
	@Resource(name = "f_flowdealtDaoImpl")
	private F_flowdealtDao objectDao;
	
	/**
	 * 根据记录ID获取处理对象集合
	 * @param recordID
	 * @return
	 * @throws Exception
	 */
	public List<F_flowdealt> getObjectListByRecordID(String recordID) throws Exception{
		String sql = "from F_flowdealt t where recordid = '" + recordID + "'";
		return objectDao.queryByHql(sql);
	}
	
	/**
	 * 根据记录ID及用户ID获取处理对象
	 * @param recordID
	 * @param employeeID
	 * @return
	 * @throws Exception
	 */
	public F_flowdealt getObjectByRecordIDEmployeeID(String recordID, String employeeID) throws Exception{
		String sql = "from F_flowdealt t where recordid = '" + recordID + "' and valueInfo = '" + employeeID + "'";
		List<F_flowdealt> list = objectDao.queryByHql(sql);
		if (list.size() == 0)
			throw new BusinessException(depict + "数据不存在!流程记录ID：" + recordID);
		return list.get(0);
	}
}
