package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.bean.Tree;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.dao.T_courseDao;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.T_course;
import com.fsd.admin.service.T_courseService;

/**
 * 课程信息Service实现类
 * @author Administrator
 *
 */
@Service("t_courseServiceImpl")
public class T_courseServiceImpl extends BaseServiceImpl<T_course, String> implements T_courseService{
    
    private static final Logger log = Logger.getLogger(T_courseServiceImpl.class);
    private String depict = "课程信息：";
    
    @Resource(name = "t_courseDaoImpl")
	public void setBaseDao(T_courseDao T_courseDao) {
		super.setBaseDao(T_courseDao);
	}
	
	@Resource(name = "t_courseDaoImpl")
	private T_courseDao objectDao;
		
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("f_deleted", 0L));
		c.addOrder(Order.asc("f_order"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("name") != null && !"".equals(objectMap.get("name"))){
				c.add(Restrictions.like("f_title", "%" + objectMap.get("name") + "%"));
			}
			if (objectMap.get("fid") != null && !"".equals(objectMap.get("fid"))) {
				c.add(Restrictions.eq("f_coursetypeid", objectMap.get("fid")));
			}
		}
		return objectDao.findPager(param , c);
	}
    
	/**
	 * 异步加载对象树
	 * @return
	 * @throws Exception
	 */
	public List<Tree> getAsyncObjectTree(String fid) throws Exception {
		List<Tree> treelist = new ArrayList<Tree>();
		List<T_course> list = objectDao.queryByHql("from T_course where f_coursetypeid = '" + fid + "' and f_deleted = 0 and f_state = 'ztqy' order by f_order asc");
		Tree menu = null;
		for (T_course obj : list) {
			menu = new Tree();
			menu.setId(obj.getId());
			menu.setText(obj.getF_title());
			treelist.add(menu);
		}
		return treelist;
	}
	
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public void save(T_course obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
        	if (obj.getId().equals(obj.getF_coursetypeid())){
    			throw new BusinessException(depict + "所属类型不得选择自己！");
        	}
            T_course old_obj = objectDao.get(obj.getId());
           
            old_obj.setF_coursetypeid(obj.getF_coursetypeid());
            old_obj.setF_coursetypename(obj.getF_coursetypename());
			old_obj.setF_title(obj.getF_title());
			old_obj.setF_abstract(obj.getF_abstract());
			old_obj.setF_imageurl(obj.getF_imageurl());
			old_obj.setF_islink(obj.getF_islink());
			old_obj.setF_link(obj.getF_link());
			old_obj.setF_order(obj.getF_order());
			old_obj.setF_updateuser(employee.getId());
			old_obj.setF_updatedate(this.getData());
			old_obj.setF_score(obj.getF_score());
			old_obj.setF_flow(obj.getF_flow());
			old_obj.setF_required(obj.getF_required());
			old_obj.setF_recommend(obj.getF_recommend());
			old_obj.setF_expirationtime(obj.getF_expirationtime());
            objectDao.update(old_obj);
        }else{
            //添加
            obj.setId(this.getUUID());//设置主键
			obj.setF_adduser(employee.getId());
			obj.setF_adddate(this.getData());
			obj.setF_state("ztqy");
			obj.setF_deleted(0L);
			objectDao.save(obj);
        }
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		String hql = "";
		objectDao.executeHql("update T_course t set t.f_deleted = 1 where t.id in (" + ids + ")");
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public T_course getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		T_course obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		return obj;		
	}	
	
	/**
	 * 修改启用&停用状态
	 * @param parameters
	 * @param employee
	 * @throws Exception
	 */
	public void updateEnabledObject(ParametersUtil parameters, A_Employee employee) throws Exception {
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> idList = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		for (String id : idList) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		boolean isEnabled = (Boolean) objectMap.get("isenabled");
		if (isEnabled) {
			objectDao.executeHql("update T_course set f_state = 'ztqy' where id in (" + ids + ")");
		} else {
			objectDao.executeHql("update T_course set f_state = 'ztty' where id in (" + ids + ")");
		}
	}
}
