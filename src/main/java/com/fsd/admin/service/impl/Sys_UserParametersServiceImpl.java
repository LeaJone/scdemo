package com.fsd.admin.service.impl;

import javax.annotation.Resource;
import org.springframework.stereotype.Repository;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.admin.model.Sys_UserParameters;
import com.fsd.admin.service.Sys_UserParametersService;
import com.fsd.admin.dao.Sys_UserParametersDao;

@Repository("Sys_UserParametersServiceImpl")
public class Sys_UserParametersServiceImpl extends MainServiceImpl<Sys_UserParameters, String> implements Sys_UserParametersService{
    
    @Resource(name = "Sys_UserParametersDaoImpl")
	public void setBaseDao(Sys_UserParametersDao Sys_UserParametersDao) {
		super.setBaseDao(Sys_UserParametersDao);
	}
	
	@Resource(name = "Sys_UserParametersDaoImpl")
	private Sys_UserParametersDao Sys_UserParametersDao;
}
