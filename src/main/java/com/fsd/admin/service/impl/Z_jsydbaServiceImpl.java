package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.fsd.core.bean.ImportExcel;
import org.springframework.stereotype.Repository;

import com.fsd.core.common.BusinessException;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.util.DateTimeUtil;
import com.fsd.core.util.ExcelUtil;
import com.fsd.core.util.ParametersUtil;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_jsydba;
import com.fsd.admin.service.Z_jsydbaService;
import com.fsd.admin.dao.Z_jsydbaDao;
import com.google.gson.Gson;

@Repository("z_jsydbaServiceImpl")
public class Z_jsydbaServiceImpl extends BaseServiceImpl<Z_jsydba, String> implements Z_jsydbaService{
    
    private static final Logger log = Logger.getLogger(Z_jsydbaServiceImpl.class);
    private String depict = "";
    
    @Resource(name = "z_jsydbaDaoImpl")
	public void setBaseDao(Z_jsydbaDao Z_jsydbaDao) {
		super.setBaseDao(Z_jsydbaDao);
	}
	
	@Resource(name = "z_jsydbaDaoImpl")
	private Z_jsydbaDao objectDao;
	
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@Override
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		c.addOrder(Order.desc("pzsj"));
		c.addOrder(Order.asc("xmmc"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("xmmc") != null && !"".equals(objectMap.get("xmmc"))){
				c.add(Restrictions.like("xmmc", "%" + objectMap.get("xmmc") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}
	
	/**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	@Override
	public void save(Z_jsydba obj, A_Employee employee) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			Z_jsydba old_obj = objectDao.get(obj.getId());
			
			old_obj.setXmmc(obj.getXmmc());
			old_obj.setSqdw(obj.getSqdw());
			old_obj.setYdqw(obj.getYdqw());
			old_obj.setXmlx(obj.getXmlx());
			old_obj.setSqmj(obj.getSqmj());
			old_obj.setPzsj(obj.getPzsj());
			old_obj.setPzwh(obj.getPzwh());
			old_obj.setRemark(obj.getRemark());
			old_obj.setUpdatedate(this.getData());//设置修改日期
			old_obj.setUpdateemployeeid(employee.getId());//设置修改用户id
			old_obj.setUpdateemployeename(employee.getRealname());//设置修改用户姓名
			
			objectDao.update(old_obj);
		}else{
			//添加
			obj.setId(this.getUUID());//设置主键
			obj.setCompanyid(employee.getCompanyid());//所属单位
			obj.setAdddate(this.getData());//设置添加日期
			obj.setAddemployeeid(employee.getId());//设置添加用户id
			obj.setAddemployeename(employee.getRealname());//设置添加用户姓名
			obj.setDeleted("0");//设置删除标志(0:正常 1：已删除)
			
			objectDao.save(obj);
		}
	}
	
	/**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	@Override
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		String hql = "";
		objectDao.executeHql("update Z_jsydba t set t.deleted = '1', t.updatedate = '" + this.getData() + 
				"', t.updateemployeeid = '" + employee.getId() + "', t.updateemployeename = '" + employee.getRealname() + 
				"' where t.id in (" + ids + ")");
	}
	
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	@Override
	public Z_jsydba getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		Z_jsydba obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		
		return obj;
	}
	
	/**
	 * 导入Excel
	 * @param obj
	 * @param employee
	 * @param diskUrl
	 * @throws Exception
	 */
	@Override
	public void saveImportObject(ImportExcel obj, A_Employee employee, String diskUrl) throws Exception{
		System.out.println(diskUrl);
		System.out.println(obj.getExcel());
		List<Object> list = ExcelUtil.readExcel(diskUrl + obj.getExcel(), Integer.parseInt(obj.getSheet()), Integer.parseInt(obj.getKsh()), Integer.parseInt(obj.getKsl()), Integer.parseInt(obj.getHs()), Integer.parseInt(obj.getLs()));
		for (int i = 0; i < list.size(); i++) {
			List<Object> olist = (List<Object>) list.get(i);
			
			Z_jsydba jsydba = new Z_jsydba();
			
			jsydba.setId(getUUID());//设置主键
			jsydba.setCompanyid(employee.getCompanyid());//所属单位
			jsydba.setAdddate(this.getData());//设置添加日期
			jsydba.setAddemployeeid(employee.getId());//设置添加用户id
			jsydba.setAddemployeename(employee.getRealname());//设置添加用户姓名
			jsydba.setDeleted("0");//设置删除标志(0:正常 1：已删除)
			
			jsydba.setXmmc(olist.get(0).toString().trim());//设置项目名称
			jsydba.setSqdw(olist.get(1).toString().trim());//设置申请单位
			jsydba.setYdqw(olist.get(2).toString().trim());//设置用地区位
			jsydba.setXmlx(olist.get(3).toString().trim());//设置项目类型
			jsydba.setSqmj(olist.get(4).toString().trim());//设置批准面积
			
			String pzsj = DateTimeUtil.formatDate(olist.get(5).toString().trim(), "dd/MM/yyyy", "yyyyMMdd");
			jsydba.setPzsj(pzsj);//设置批准时间
			jsydba.setPzwh(olist.get(6).toString().trim());//设置批准文号
			
			objectDao.save(jsydba);
		}
	}
}
