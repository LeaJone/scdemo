package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_projectgroup;
import com.fsd.admin.service.Z_projectgroupService;
import com.fsd.admin.dao.Z_projectgroupDao;

@Repository("z_projectgroupServiceImpl")
public class Z_projectgroupServiceImpl extends BaseServiceImpl<Z_projectgroup, String> implements Z_projectgroupService{
    
    private static final Logger log = Logger.getLogger(Z_projectgroupServiceImpl.class);
    private String depict = "";
    
    @Resource(name = "z_projectgroupDaoImpl")
	public void setBaseDao(Z_projectgroupDao Z_projectgroupDao) {
		super.setBaseDao(Z_projectgroupDao);
	}
	
	@Resource(name = "z_projectgroupDaoImpl")
	private Z_projectgroupDao objectDao;
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
    @Override
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("f_deleted", "0"));
		c.addOrder(Order.asc("f_adddate"));
		param = objectDao.findPager(param , c);
		List list = param.getData();
		Z_projectgroup projectgroup = new Z_projectgroup();
		projectgroup.setF_name("未分组");
		projectgroup.setId("wfz");
		list.add(projectgroup);
		param.setData(list);
		return param;
	}

	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@Override
	public ParametersUtil getObjectPageList1(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("f_deleted", "0"));
		c.addOrder(Order.asc("f_adddate"));
		return objectDao.findPager(param , c);
	}
    
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
    @Override
	public void save(Z_projectgroup obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
            Z_projectgroup old_obj = objectDao.get(obj.getId());
			old_obj.setF_name(obj.getF_name());
			old_obj.setF_describe(obj.getF_describe());
            old_obj.setF_lastupdatedate(this.getData());//设置修改日期
			old_obj.setF_updateemployeeid(employee.getId());//设置修改用户id
			old_obj.setF_updateemployeename(employee.getRealname());//设置修改用户姓名
            objectDao.update(old_obj);
        }else{
            //添加
            obj.setId(this.getUUID());//设置主键
			obj.setF_adddate(this.getData());//设置添加日期
			obj.setF_addemployeeid(employee.getId());//设置添加用户id
			obj.setF_addemployeename(employee.getRealname());//设置添加用户姓名
			obj.setF_deleted("0");//设置删除标志(0:正常 1：已删除)
			objectDao.save(obj);
        }
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
    @Override
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		for (String id : dir) {
			this.delete(id);
		}
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
    @Override
	public Z_projectgroup getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		Z_projectgroup obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		
		return obj;
	}

	/**
	 * 保存项目分组
	 * @param parameters
	 * @throws Exception
	 */
	@Override
	public void saveProjectGroup(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("groupid") == null && "".equals(objectMap.get("groupid"))){
			throw new BusinessException(depict + "缺少项目分组编码");
		}
		if(objectMap.get("projectids") == null && "".equals(objectMap.get("projectids"))){
			throw new BusinessException(depict + "缺少项目编码");
		}
		String groupid = objectMap.get("groupid").toString();
		Z_projectgroup projectgroup = this.get(groupid);
		ArrayList<String> projectids = (ArrayList<String>)objectMap.get("projectids");
		for (String projectid : projectids) {
			String sql = "update z_project set f_classifyid = ?, f_classifyname = ? where id = ?";
			objectDao.executeSql(sql, groupid, projectgroup.getF_name(), projectid);
		}
	}
}
