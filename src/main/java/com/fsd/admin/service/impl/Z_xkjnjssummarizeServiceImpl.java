package com.fsd.admin.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;

import com.fsd.admin.model.Z_xkjnjs;
import com.fsd.admin.service.Z_xkjnjsService;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_xkjnjssummarize;
import com.fsd.admin.service.Z_xkjnjssummarizeService;
import com.fsd.admin.dao.Z_xkjnjssummarizeDao;

@Repository("z_xkjnjssummarizeServiceImpl")
public class Z_xkjnjssummarizeServiceImpl extends BaseServiceImpl<Z_xkjnjssummarize, String> implements Z_xkjnjssummarizeService{
    
    private static final Logger log = Logger.getLogger(Z_xkjnjssummarizeServiceImpl.class);
    private String depict = "";
    
    @Resource(name = "z_xkjnjssummarizeDaoImpl")
	public void setBaseDao(Z_xkjnjssummarizeDao Z_xkjnjssummarizeDao) {
		super.setBaseDao(Z_xkjnjssummarizeDao);
	}
	
	@Resource(name = "z_xkjnjssummarizeDaoImpl")
	private Z_xkjnjssummarizeDao objectDao;

    @Autowired
    private Z_xkjnjsService xkjnjsService;
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
    @Override
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
        c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		c.add(Restrictions.eq("deleted", "0"));
		c.addOrder(Order.asc("adddate"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("title") != null && !"".equals(objectMap.get("title"))){
				c.add(Restrictions.like("title", "%" + objectMap.get("title") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}

	/**
	 * 保存学科技能竞赛总结报告基本信息
	 * @param param
	 * @param obj
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	public Z_xkjnjssummarize saveInfo(ParametersUtil param, Z_xkjnjssummarize obj, A_Employee employee) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			Z_xkjnjssummarize old_obj = objectDao.get(obj.getId());
			old_obj.setF_joingroup(obj.getF_joingroup());
			old_obj.setF_joinpeople(obj.getF_joinpeople());
			old_obj.setF_prizegroup(obj.getF_prizegroup());
			old_obj.setF_prizepeople(obj.getF_prizepeople());
			old_obj.setF_matchtime(obj.getF_matchtime());
			old_obj.setF_matchdays(obj.getF_matchdays());
			old_obj.setF_money(obj.getF_money());
			objectDao.update(old_obj);
			return old_obj;
		}else{
			obj.setId(this.getUUID());
			obj.setF_adddate(this.getData());
			obj.setF_addemployeeid(employee.getId());
			obj.setF_deleted("0");
			obj.setF_statusid("0");
			obj.setF_statusname("草稿");
			objectDao.save(obj);

			Z_xkjnjs xkjnjs = xkjnjsService.get(obj.getF_fid());
			if(xkjnjs == null){
				throw new BusinessException("系统错误，学科技能竞赛申报数据不存在，请联系管理员");
			}
			xkjnjs.setF_issummarize("cg");
			return obj;
		}
	}

	/**
	 * 保存项目背景信息
	 * @param param
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	public Z_xkjnjssummarize saveBackground(ParametersUtil param, Z_xkjnjssummarize obj) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			Z_xkjnjssummarize old_obj = objectDao.get(obj.getId());
			old_obj.setF_background(obj.getF_background());
			objectDao.update(old_obj);
			return old_obj;
		}else{
			throw new BusinessException("系统错误，请联系管理员");
		}
	}

	/**
	 * 保存项目实施过程信息
	 * @param param
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	public Z_xkjnjssummarize saveImplementcourse(ParametersUtil param, Z_xkjnjssummarize obj) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			Z_xkjnjssummarize old_obj = objectDao.get(obj.getId());
			old_obj.setF_implementcourse(obj.getF_implementcourse());
			objectDao.update(old_obj);
			return old_obj;
		}else{
			throw new BusinessException("系统错误，请联系管理员");
		}
	}

	/**
	 * 保存项目竞赛过程信息
	 * @param param
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	public Z_xkjnjssummarize saveMatchcourse(ParametersUtil param, Z_xkjnjssummarize obj) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			Z_xkjnjssummarize old_obj = objectDao.get(obj.getId());
			old_obj.setF_matchcourse(obj.getF_matchcourse());
			objectDao.update(old_obj);
			return old_obj;
		}else{
			throw new BusinessException("系统错误，请联系管理员");
		}
	}

	/**
	 * 保存项目竞赛成果信息
	 * @param param
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	public Z_xkjnjssummarize saveAchievement(ParametersUtil param, Z_xkjnjssummarize obj) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			Z_xkjnjssummarize old_obj = objectDao.get(obj.getId());
			old_obj.setF_achievement(obj.getF_achievement());
			objectDao.update(old_obj);
			return old_obj;
		}else{
			throw new BusinessException("系统错误，请联系管理员");
		}
	}

	/**
	 * 保存项目不足与改进信息
	 * @param param
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	public Z_xkjnjssummarize saveSummarize(ParametersUtil param, Z_xkjnjssummarize obj) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			Z_xkjnjssummarize old_obj = objectDao.get(obj.getId());
			old_obj.setF_summarize(obj.getF_summarize());
			objectDao.update(old_obj);
			return old_obj;
		}else{
			throw new BusinessException("系统错误，请联系管理员");
		}
	}

	/**
	 * 保存项目中遇到的问题信息
	 * @param param
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	public Z_xkjnjssummarize saveQuestion(ParametersUtil param, Z_xkjnjssummarize obj) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			Z_xkjnjssummarize old_obj = objectDao.get(obj.getId());
			old_obj.setF_question(obj.getF_question());
			objectDao.update(old_obj);
			return old_obj;
		}else{
			throw new BusinessException("系统错误，请联系管理员");
		}
	}

	/**
	 * 保存项目创新信息
	 * @param param
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	public Z_xkjnjssummarize saveInnovate(ParametersUtil param, Z_xkjnjssummarize obj) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			Z_xkjnjssummarize old_obj = objectDao.get(obj.getId());

			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("type") == null && "".equals(objectMap.get("type"))){
				throw new BusinessException("系统错误，请联系管理员");
			}

			Z_xkjnjs xkjnjs = xkjnjsService.get(old_obj.getF_fid());
			if(xkjnjs == null){
				throw new BusinessException("系统错误，学科技能竞赛申报数据不存在，请联系管理员");
			}
			String type = objectMap.get("type").toString();
			if("cg".equals(type)){
				obj.setF_statusid("0");
				obj.setF_statusname("草稿");
				xkjnjs.setF_issummarize("cg");
			}else if("tj".equals(type)){
				obj.setF_statusid("1");
				obj.setF_statusname("提交待审核");
				xkjnjs.setF_issummarize("tj");
			}else{
				throw new BusinessException("系统错误，请联系管理员");
			}
			xkjnjsService.update(xkjnjs);
			//修改

			old_obj.setF_innovate(obj.getF_innovate());
			objectDao.update(old_obj);
			return old_obj;
		}else{
			throw new BusinessException("系统错误，请联系管理员");
		}
	}

    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
    @Override
	public void save(Z_xkjnjssummarize obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
            Z_xkjnjssummarize old_obj = objectDao.get(obj.getId());
            
            old_obj.setF_lastupdatedate(this.getData());//设置修改日期
			old_obj.setF_updateemployeeid(employee.getId());//设置修改用户id
			old_obj.setF_updateemployeename(employee.getRealname());//设置修改用户姓名
            objectDao.update(old_obj);
        }else{
            //添加
            obj.setId(this.getUUID());//设置主键
			obj.setF_adddate(this.getData());//设置添加日期
			obj.setF_addemployeeid(employee.getId());//设置添加用户id
			obj.setF_addemployeename(employee.getRealname());//设置添加用户姓名
			obj.setF_deleted("0");//设置删除标志(0:正常 1：已删除)
			objectDao.save(obj);
        }
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
    @Override
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		String hql = "";
		objectDao.executeHql("update Z_xkjnjssummarize t set t.deleted = '1', t.updatedate = '" + this.getData() + 
				"', t.updateemployeeid = '" + employee.getId() + "', t.updateemployeename = '" + employee.getRealname() + 
				"' where t.id in (" + ids + ")");
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
    @Override
	public Z_xkjnjssummarize getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		Z_xkjnjssummarize obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		
		return obj;
	}

	/**
	 * 根据FID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public Z_xkjnjssummarize getObjectByFid(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("fid") == null && "".equals(objectMap.get("fid"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		String sql = "select * from Z_xkjnjssummarize where f_fid = ?";
		List<Z_xkjnjssummarize> list = objectDao.queryBySql1(sql, objectMap.get("fid").toString());
		if(list != null && list.size() > 0){
			return list.get(0);
		}else{
			throw new BusinessException(depict + "数据不存在!");
		}
	}
}
