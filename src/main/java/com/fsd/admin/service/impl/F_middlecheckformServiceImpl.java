package com.fsd.admin.service.impl;

import com.fsd.admin.dao.Z_projectDao;
import com.fsd.admin.dao.Z_teamanduserDao;
import com.fsd.admin.model.*;
import com.fsd.admin.service.*;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.FilterHTML;
import com.fsd.core.util.ParametersUtil;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.fsd.admin.dao.F_middlecheckformDao;

/**
 * 项目中期检查任务书Service实现类
 * @author Administrator
 *
 */
@Repository("f_middlecheckformServiceImpl")
public class F_middlecheckformServiceImpl extends MainServiceImpl<F_middlecheckform, String> implements F_middlecheckformService{

    private static String depict = "项目中期检查表";
    
    @Resource(name = "f_middlecheckformDaoImpl")
	public void setBaseDao(F_middlecheckformDao middlecheckformDao) {
		super.setBaseDao(middlecheckformDao);
	}
	
	@Resource(name = "f_middlecheckformDaoImpl")
	private F_middlecheckformDao objectDao;

	@Resource(name = "z_projectDaoImpl")
	private Z_projectDao projectDao;

	@Resource(name = "z_teamanduserDaoImpl")
	private Z_teamanduserDao teamanduserDao;

	@Autowired
	private Z_projectService projectService;
	
	/**
	 * 提交执行计划书
	 * @param obj
	 */
	public void saveMiddlecheckform(F_middlecheckform obj, A_Employee employee) throws Exception{
		if(obj != null && obj.getId() != null && !"".equals(obj.getId())){
			F_middlecheckform old_object = this.get(obj.getId());
			old_object.setF_projectprogress(obj.getF_projectprogress());
			old_object.setF_mainproblem(obj.getF_mainproblem());
			old_object.setF_resourceuse(obj.getF_resourceuse());
			old_object.setF_existproblem(obj.getF_existproblem());
			objectDao.update(old_object);
		}else{
			throw new BusinessException("系统错误，请联系管理员");
		}
	}
	
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public F_middlecheckform getMiddlecheckformById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("f_deleted", "0"));
		c.add(Restrictions.eq("f_projectid", objectMap.get("id").toString()));
		List<F_middlecheckform> list = objectDao.getList(c);
		if(list.size() != 0){
			// 去HTML标签
			F_middlecheckform obj = list.get(0);
			obj.setF_projectprogress(FilterHTML.delHTMLTag(obj.getF_projectprogress()));
			obj.setF_projectphaseresult(FilterHTML.delHTMLTag(obj.getF_projectphaseresult()));
			obj.setF_mainproblem(FilterHTML.delHTMLTag(obj.getF_mainproblem()));
			obj.setF_resourceuse(FilterHTML.delHTMLTag(obj.getF_resourceuse()));
			obj.setF_projectfunds(FilterHTML.delHTMLTag(obj.getF_projectfunds()));
			objectDao.update(obj);
			return obj;
		}else{
			return null;
		}
	}

	/**
	 * 获取是否提交中期检查数据
	 * @param projectid
	 * @return
	 * @throws Exception
	 */
	private String getIsSubMiddleCheck(String projectid) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("f_deleted", "0"));
		c.add(Restrictions.eq("f_projectid", projectid));
		List<F_middlecheckform> list = objectDao.getList(c);
		if(list.size() != 0){
			return "true";
		}else{
			return "false";
		}
	}

	/**
	 * 根据项目ID加载项目中期检查信息
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public F_middlecheckform saveOrgetMiddlecheckformByProjectId(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("projectid") == null && "".equals(objectMap.get("projectid"))){
			throw new BusinessException(depict + "缺少项目ID!");
		}
		String projectid = objectMap.get("projectid").toString();
		String sql = "select * from F_middlecheckform where f_projectId = ? and f_deleted = ?";
		List<F_middlecheckform> list = objectDao.queryBySql1(sql, projectid, "0");
		if(list != null && list.size() > 0){
			return list.get(0);
		}else{
			Z_project project = projectService.get(projectid);
			if(project == null){
				throw new BusinessException(depict + "项目信息错误，请联系管理员!");
			}
			F_middlecheckform middlecheckform = new F_middlecheckform();
			middlecheckform.setId(this.getUUID());
			middlecheckform.setF_projectid(projectid);
			middlecheckform.setF_projectname(project.getF_name());
			middlecheckform.setF_projectprogress("一、研究项目的进展和经费使用情况（含中期目标完成情况及预期成果）：");
			middlecheckform.setF_mainproblem("二、项目执行时遇到的关键问题，拟采取的措施：");
			middlecheckform.setF_resourceuse("四、项目研究利用资源情况（实验室、研究所、实训基地的名称及地点）");
			middlecheckform.setF_existproblem("八、存在问题、建议及需要说明的情况");
			middlecheckform.setF_deleted("0");
			objectDao.save(middlecheckform);

			project.setF_ismiddle("true");
			projectService.update(project);
			return middlecheckform;
		}
	}

	/**
	 * 根据项目ID加载中期检查
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public F_middlecheckform getObjectByProjectId(String projectId) throws Exception{
		String sql = "select * from F_middlecheckform where f_projectid = ? and f_deleted = ?";
		List<F_middlecheckform> list = objectDao.queryBySql1(sql, projectId, "0");
		if(list != null && list.size() >0){
			return list.get(0);
		}
		return null;
	}
}
