package com.fsd.admin.service.impl;

import java.util.List;
import javax.annotation.Resource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import com.fsd.admin.dao.E_AutoreplylistDao;
import com.fsd.admin.model.E_Autoreplylist;
import com.fsd.admin.service.E_AutoreplylistService;

@Service("e_autoreplylistServiceImpl")
public class E_AutoreplylistServiceImpl extends MainServiceImpl<E_Autoreplylist, String> implements E_AutoreplylistService {
	
	private static final Log log = LogFactory.getLog(E_AutoreplylistServiceImpl.class);
    private String depict = "自动回复列表";
	
    @Resource(name = "e_autoreplylistDaoImpl")
	public void setBaseDao(E_AutoreplylistDao e_autoreplylistDao) {
		super.setBaseDao(e_autoreplylistDao);
	}
    
	@Resource(name = "e_autoreplylistDaoImpl")
	private E_AutoreplylistDao objectDao;
	
	/**
	 * 彻底删除对象
	 * @param autoReplyID
	 */
	public void delObjectByAutoReplyID(String autoReplyID) throws Exception{
		objectDao.executeHql("delete from E_Autoreplylist where autoreplyid in (" + autoReplyID + ")");
	}
	
	/**
	 * 加载所需集合
	 * @return
	 * @throws Exception
	 */
	public List<E_Autoreplylist> getObjectListByAutoReplyID(String autoReplyID) throws Exception{
		return objectDao.queryByHql("from E_Autoreplylist a where a.autoreplyid = '"+ autoReplyID +"' order by a.sort");	
	}
	
	/**
	 * 加载微信号所需集合
	 * @return
	 * @throws Exception
	 */
	public List<E_Autoreplylist> getObjectListByWeChatID(String weChatID) throws Exception{
		return objectDao.queryByHql("from E_Autoreplylist a where a.wechatid = '"+ weChatID +"' order by a.sort");	
	}
}
