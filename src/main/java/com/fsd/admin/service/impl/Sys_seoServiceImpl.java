package com.fsd.admin.service.impl;

import com.fsd.admin.dao.Sys_seoDao;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Sys_seo;
import com.fsd.admin.service.Sys_seoService;
import com.fsd.core.common.BusinessException;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.util.ParametersUtil;
import com.google.gson.Gson;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

@Service("sys_seoServiceImpl")
public class Sys_seoServiceImpl extends BaseServiceImpl<Sys_seo, String> implements Sys_seoService {
    
    private String depict = "全站SEO优化 - ";
	
	@Resource(name = "sys_seoDaoImpl")
	private Sys_seoDao objectDao;
    
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public void save(Sys_seo obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
            Sys_seo old_obj = objectDao.get(obj.getId());
            old_obj.setCompanyid(employee.getCompanyid());
            old_obj.setTitle(obj.getTitle());
            old_obj.setKeywords(obj.getKeywords());
            old_obj.setDescription(obj.getDescription());
            old_obj.setRemark(obj.getRemark());
            objectDao.update(old_obj);
        }else{
            //添加
            obj.setId(this.getUUID());//设置主键
            obj.setCompanyid(employee.getCompanyid());
			objectDao.save(obj);
        }
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		String id = (String) objectMap.get("id");
		String hql = "update Sys_seo set Title='', Keywords='', Description='' where id = ?";
		objectDao.executeHql(hql, id);
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public Sys_seo getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		String id = (String) objectMap.get("id");
		Sys_seo obj = objectDao.get(id);
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		return obj;
	}
}
