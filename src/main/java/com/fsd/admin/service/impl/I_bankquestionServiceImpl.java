package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.fsd.core.common.BusinessException;
import com.fsd.core.util.Config;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.I_bankquestion;
import com.fsd.admin.service.I_bankinfoService;
import com.fsd.admin.service.I_bankquestionService;
import com.fsd.admin.dao.I_bankquestionDao;

@Repository("i_bankquestionServiceImpl")
public class I_bankquestionServiceImpl extends MainServiceImpl<I_bankquestion, String> implements I_bankquestionService{
    
    private static final Logger log = Logger.getLogger(I_bankquestionServiceImpl.class);
    private String depict = "题目库题目";
    
    @Resource(name = "i_bankquestionDaoImpl")
	public void setBaseDao(I_bankquestionDao I_bankquestionDao) {
		super.setBaseDao(I_bankquestionDao);
	}
	
	@Resource(name = "i_bankquestionDaoImpl")
	private I_bankquestionDao objectDao;
	@Resource(name = "i_bankinfoServiceImpl")
	private I_bankinfoService bankinfoService;
	
	//i_tmlx 题目类型
	/**
     * 单选
     */
	public static final String tmlxxz1 = "i_tmlxxz";
	public static final String tmlxxz2 = "单选";
	/**
     * 多选
     */
	public static final String tmlxdx1 = "i_tmlxdx";
	public static final String tmlxdx2 = "多选";
	/**
     * 排序
     */
	public static final String tmlxpx1 = "i_tmlxpx";
	public static final String tmlxpx2 = "排序";
	/**
     * 填空
     */
	public static final String tmlxtk1 = "i_tmlxtk";
	public static final String tmlxtk2 = "填空";
	/**
     * 阅读
     */
	public static final String tmlxyd1 = "i_tmlxyd";
	public static final String tmlxyd2 = "阅读";
	/**
     * 简答
     */
	public static final String tmlxjd1 = "i_tmlxjd";
	public static final String tmlxjd2 = "简答";
	
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
        c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		c.add(Restrictions.eq("deleted", "0"));
		c.addOrder(Order.asc("adddate"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("fid") != null && !"".equals(objectMap.get("fid"))){
            	List<String> idList = this.bankinfoService.getObjectChildIDList(
						objectMap.get("fid").toString(), employee);
				idList.add(objectMap.get("fid").toString());
				c.add(Restrictions.in("bankinfoid", idList));
			}
			if(objectMap.get("title") != null && !"".equals(objectMap.get("title"))){
				c.add(Restrictions.like("title", "%" + objectMap.get("title") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}
    
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public String save(I_bankquestion obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
            I_bankquestion old_obj = objectDao.get(obj.getId());
            old_obj.setBankinfoid(obj.getBankinfoid());
            old_obj.setBankinfoname(obj.getBankinfoname());
            old_obj.setTypecode(obj.getTypecode());
            old_obj.setTypename(obj.getTypename());
            old_obj.setLevelcode(obj.getLevelcode());
            old_obj.setLevelname(obj.getLevelname());
            old_obj.setKeynotecode(obj.getKeynotecode());
            old_obj.setKeynotename(obj.getKeynotename());
            old_obj.setQuestiontypeid(obj.getQuestiontypeid());
            old_obj.setQuestiontypename(obj.getQuestiontypename());
            old_obj.setProblem(obj.getProblem());
            old_obj.setContent(obj.getContent());
            old_obj.setKeynote(obj.getKeynote());
            old_obj.setParse(obj.getParse());
            old_obj.setSort(obj.getSort());
            old_obj.setRemark(obj.getRemark());
            old_obj.setAuditing("false");
            old_obj.setUpdatedate(this.getData());//设置修改日期
			old_obj.setUpdateemployeeid(employee.getId());//设置修改用户id
			old_obj.setUpdateemployeename(employee.getRealname());//设置修改用户姓名
            objectDao.update(old_obj);
        }else{
            //添加
            obj.setId(this.getUUID());//设置主键
			obj.setCompanyid(employee.getCompanyid());//所属单位
			obj.setAuditing("false");
			obj.setAdddate(this.getData());//设置添加日期
			obj.setAddemployeeid(employee.getId());//设置添加用户id
			obj.setAddemployeename(employee.getRealname());//设置添加用户姓名
			obj.setDeleted("0");//设置删除标志(0:正常 1：已删除)
			objectDao.save(obj);
        }
        return obj.getId();
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		String hql = "";
		objectDao.executeHql("update I_bankquestion t set t.deleted = '1', t.updatedate = '" + this.getData() + 
				"', t.updateemployeeid = '" + employee.getId() + "', t.updateemployeename = '" + employee.getRealname() + 
				"' where t.id in (" + ids + ")");
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public I_bankquestion getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		I_bankquestion obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		
		return obj;
	}
	
	/**
	 * 顺序排序
	 * @param parameters
	 */
	public void updateSortObject(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		List<String> idList = (List<String>) objectMap.get("ids");
		String ids = this.getSQLinByIDList(idList);
		I_bankquestion obj = objectDao.get(objectMap.get("id").toString());
		if (obj == null)
			throw new BusinessException(depict + "获取定位对象为空!");
		List<I_bankquestion> objList = objectDao.queryByHql("from I_bankquestion t where " +
				"t.bankinfoid = '" + obj.getBankinfoid() + "' and t.deleted = '0' " +
				"and t.id not in (" + ids + ") " +
				"and t.sort >= " + obj.getSort() + " order by t.sort asc");
		long sort = obj.getSort();
		for (String id : idList) {
			objectDao.executeHql("update I_bankquestion t set t.sort = " + sort + " where t.id = '" + id + "'");
			sort++;
		}
		for (I_bankquestion obj1 : objList) {
			objectDao.executeHql("update I_bankquestion t set t.sort = " + sort + " where t.id = '" + obj1.getId() + "'");
			sort++;
		}
	}
	
	/**
	 * 修改状态
	 * @param parameters
	 */
	@SuppressWarnings("unchecked")
	public void updateEnabledObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> idList = (ArrayList<String>) objectMap.get("ids");
		String ids = this.getSQLinByIDList(idList);
		boolean isEnabled = (Boolean) objectMap.get("isenabled");
		if (isEnabled){
			objectDao.executeHql("update I_bankquestion t set t.auditing = 'true', t.updatedate = '" + this.getData() + 
				"', t.updateemployeeid = '" + employee.getId() + "', t.updateemployeename = '" + employee.getRealname() + 
				"' where t.id in (" + ids + ")");
			this.sysLogService.saveObject(Config.LOGTYPEBC, this.depict + " 修改“审核” " + ids, employee, I_bankquestionServiceImpl.class);
		}else{
			objectDao.executeHql("update I_bankquestion t set t.auditing = 'false', t.updatedate = '" + this.getData() + 
				"', t.updateemployeeid = '" + employee.getId() + "', t.updateemployeename = '" + employee.getRealname() + 
				"' where t.id in (" + ids + ")");
			this.sysLogService.saveObject(Config.LOGTYPEBC, this.depict + " 修改“停审” " + ids, employee, I_bankquestionServiceImpl.class);
		}
	}
}
