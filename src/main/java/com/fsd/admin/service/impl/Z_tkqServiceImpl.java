package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;

import com.fsd.core.bean.ImportExcel;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.DateTimeUtil;
import com.fsd.core.util.ExcelUtil;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_tkq;
import com.fsd.admin.service.Z_tkqService;
import com.fsd.admin.dao.Z_tkqDao;

@Repository("z_tkqServiceImpl")
public class Z_tkqServiceImpl extends BaseServiceImpl<Z_tkq, String> implements Z_tkqService{
    
    private static final Logger log = Logger.getLogger(Z_tkqServiceImpl.class);
    private String depict = "";
    
    @Resource(name = "z_tkqDaoImpl")
	public void setBaseDao(Z_tkqDao Z_tkqDao) {
		super.setBaseDao(Z_tkqDao);
	}
	
	@Resource(name = "z_tkqDaoImpl")
	private Z_tkqDao objectDao;
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@Override
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
        c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		c.add(Restrictions.eq("deleted", "0"));
		c.addOrder(Order.asc("adddate"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("xkzh") != null && !"".equals(objectMap.get("xkzh"))){
				c.add(Restrictions.like("xkzh", "%" + objectMap.get("xkzh") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}
    
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	@Override
	public void save(Z_tkq obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
            Z_tkq old_obj = objectDao.get(obj.getId());
            old_obj.setXkzh(obj.getXkzh());
            old_obj.setXmmc(obj.getXmmc());
            old_obj.setSqr(obj.getSqr());
            old_obj.setKcdw(obj.getKcdw());
            old_obj.setKckzbm(obj.getKckzbm());
            old_obj.setYxqq(obj.getYxqq());
            old_obj.setYxqz(obj.getYxqz());
            old_obj.setZmj(obj.getZmj());
            old_obj.setDjq(obj.getDjq());
            old_obj.setDjz(obj.getDjz());
            old_obj.setBwq(obj.getBwq());
            old_obj.setBwz(obj.getBwz());
            old_obj.setUpdatedate(this.getData());//设置修改日期
			old_obj.setUpdateemployeeid(employee.getId());//设置修改用户id
			old_obj.setUpdateemployeename(employee.getRealname());//设置修改用户姓名
            objectDao.update(old_obj);
        }else{
            //添加
            obj.setId(this.getUUID());//设置主键
			obj.setCompanyid(employee.getCompanyid());//所属单位
			obj.setAdddate(this.getData());//设置添加日期
			obj.setAddemployeeid(employee.getId());//设置添加用户id
			obj.setAddemployeename(employee.getRealname());//设置添加用户姓名
			obj.setDeleted("0");//设置删除标志(0:正常 1：已删除)
			objectDao.save(obj);
        }
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	@Override
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		String hql = "";
		objectDao.executeHql("update Z_tkq t set t.deleted = '1', t.updatedate = '" + this.getData() + 
				"', t.updateemployeeid = '" + employee.getId() + "', t.updateemployeename = '" + employee.getRealname() + 
				"' where t.id in (" + ids + ")");
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	@Override
	public Z_tkq getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		Z_tkq obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		
		return obj;
	}
	
	/**
	 * 导入Excel
	 * @param obj
	 * @param employee
	 * @param diskUrl
	 * @throws Exception
	 */
	@Override
	public void saveImportObject(ImportExcel obj, A_Employee employee, String diskUrl) throws Exception{
		System.out.println(diskUrl);
		System.out.println(obj.getExcel());
		List<Object> list = ExcelUtil.readExcel(diskUrl + obj.getExcel(), Integer.parseInt(obj.getSheet()), Integer.parseInt(obj.getKsh()), Integer.parseInt(obj.getKsl()), Integer.parseInt(obj.getHs()), Integer.parseInt(obj.getLs()));
		for (int i = 0; i < list.size(); i++) {
			List<Object> olist = (List<Object>) list.get(i);
			
			Z_tkq tkq = new Z_tkq();
			tkq.setId(getUUID());//设置主键ID
			tkq.setCompanyid(employee.getCompanyid());//设置所属单位
			tkq.setAdddate(this.getData());//设置添加日期
			tkq.setAddemployeeid(employee.getAddemployeeid());//设置添加用户ID
			tkq.setAddemployeename(employee.getAddemployeename());//设置添加用户姓名
			tkq.setDeleted("0");//设置删除标志(0：正常；1：已删除)
			
			tkq.setXkzh(olist.get(0).toString().trim());//设置许可证号
			tkq.setXmmc(olist.get(1).toString().trim());//设置项目名称
			tkq.setSqr(olist.get(2).toString().trim());//设置申请人
			tkq.setKcdw(olist.get(3).toString().trim());//设置勘察单位
			tkq.setKckzbm(olist.get(4).toString().trim());//设置勘察矿种编码
			String yxqq = DateTimeUtil.formatDate(olist.get(5).toString().trim(), "yyyy-MM-dd", "yyyyMMdd");
			tkq.setYxqq(yxqq);//设置有效期起
			String yxqz = DateTimeUtil.formatDate(olist.get(6).toString().trim(), "yyyy-MM-dd", "yyyyMMdd");
			tkq.setYxqz(yxqz);//设置有效期止
			tkq.setZmj(olist.get(7).toString().trim());//设置总面积
			tkq.setDjq(olist.get(8).toString().trim());//设置东经起
			tkq.setDjz(olist.get(9).toString().trim());//设置东经止
			tkq.setBwq(olist.get(10).toString().trim());//设置北纬起
			tkq.setBwz(olist.get(11).toString().trim());//设置北纬止
			objectDao.save(tkq);
		}
	}
}
