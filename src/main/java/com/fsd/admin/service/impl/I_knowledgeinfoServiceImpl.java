package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.fsd.core.common.BusinessException;
import com.fsd.core.util.Config;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.I_knowledgeinfo;
import com.fsd.admin.service.I_bankinfoService;
import com.fsd.admin.service.I_knowledgeinfoService;
import com.fsd.admin.dao.I_knowledgeinfoDao;

@Repository("i_knowledgeinfoServiceImpl")
public class I_knowledgeinfoServiceImpl extends MainServiceImpl<I_knowledgeinfo, String> implements I_knowledgeinfoService{
    
    private static final Logger log = Logger.getLogger(I_knowledgeinfoServiceImpl.class);
    private String depict = "知识库";
    
    @Resource(name = "i_knowledgeinfoDaoImpl")
	public void setBaseDao(I_knowledgeinfoDao I_knowledgeinfoDao) {
		super.setBaseDao(I_knowledgeinfoDao);
	}
	
	@Resource(name = "i_knowledgeinfoDaoImpl")
	private I_knowledgeinfoDao objectDao;
    @Resource(name = "i_bankinfoServiceImpl")
	private I_bankinfoService bankinfoService;

	//知识数据状态 i_zssjzt
	/**
     * 状态启用
     */
	public static final String zssjqy1 = "i_zssjqy";
	public static final String zssjqy2 = "启用";
	/**
     * 状态停用
     */
	public static final String zssjty1 = "i_zssjty";
	public static final String zssjty2 = "停用";
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
        c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		c.add(Restrictions.eq("deleted", "0"));
		c.addOrder(Order.asc("sort"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("fid") != null && !"".equals(objectMap.get("fid"))){
				if (!I_bankinfoServiceImpl.fsdzsk1.equals(objectMap.get("fid"))){
					List<String> idList = this.bankinfoService.getObjectChildIDList(
							objectMap.get("fid").toString(), employee);
					idList.add(objectMap.get("fid").toString());
					c.add(Restrictions.in("bankinfoid", idList));
				}
			}
			if(objectMap.get("title") != null && !"".equals(objectMap.get("title"))){
				c.add(Restrictions.like("title", "%" + objectMap.get("title") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}
    
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public ParametersUtil save(I_knowledgeinfo obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
            I_knowledgeinfo old_obj = objectDao.get(obj.getId());
			old_obj.setBankinfoid(obj.getBankinfoid());//修改所属题库id
			old_obj.setBankinfoname(obj.getBankinfoname());//修改所属题库名称
			old_obj.setLevelcode(obj.getLevelcode());//修改难度编码
			old_obj.setLevelname(obj.getLevelname());//修改难度名称
			old_obj.setKeynotecode(obj.getKeynotecode());//修改重点编码
			old_obj.setKeynotename(obj.getKeynotename());//修改重点名称
			old_obj.setQuestiontypeid(obj.getQuestiontypeid());//修改题目分类编号
			old_obj.setQuestiontypename(obj.getQuestiontypename());//修改题目分类名称
			old_obj.setTitle(obj.getTitle());//修改标题
			old_obj.setSubtitle(obj.getSubtitle());//修改副标题
			old_obj.setShowtitle(obj.getShowtitle());//修改显示标题
			old_obj.setAbstracts(obj.getAbstracts());//修改简介
			old_obj.setContent(obj.getContent());//修改内容
			old_obj.setKeynote(obj.getKeynote());//修改重点
			old_obj.setParse(obj.getParse());//修改解析
			old_obj.setAurl(obj.getAurl());//修改外链接
			old_obj.setIsaurl(obj.getIsaurl());//修改是否外链接
			old_obj.setSource(obj.getSource());//修改来源
			old_obj.setSourceurl(obj.getSourceurl());//修改来源链接
			old_obj.setSourceisurl(obj.getSourceisurl());//修改来源是否链接
			old_obj.setSort(obj.getSort());//修改顺序号
			old_obj.setRemark(obj.getRemark());//修改备注
			old_obj.setImageurl1(obj.getImageurl1());//修改图片
			old_obj.setVideourl(obj.getVideourl());//修改内容视频
			old_obj.setVoiceurl(obj.getVoiceurl());//修改内容语音

			old_obj.setAuditing("false");//标记未审批
            old_obj.setUpdatedate(this.getData());//设置修改日期
			old_obj.setUpdateemployeeid(employee.getId());//设置修改用户id
			old_obj.setUpdateemployeename(employee.getRealname());//设置修改用户姓名
            objectDao.update(old_obj);
        }else{
            //添加
            obj.setId(this.getUUID());//设置主键
			obj.setCompanyid(employee.getCompanyid());//所属单位
			obj.setAuditing("false");//标记未审批
			obj.setAdddate(this.getData());//设置添加日期
			obj.setAddemployeeid(employee.getId());//设置添加用户id
			obj.setAddemployeename(employee.getRealname());//设置添加用户姓名
			obj.setDeleted("0");//设置删除标志(0:正常 1：已删除)
			objectDao.save(obj);
        }
        this.sysLogService.saveObject(Config.LOGTYPEBC, this.depict + obj.getId() + obj.getTitle(), employee, I_knowledgeinfoServiceImpl.class);
        ParametersUtil util = new ParametersUtil();
		util.addSendObject("id", obj.getId());
		return util;
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> idList = (ArrayList<String>) objectMap.get("ids");
		String ids = this.getSQLinByIDList(idList);
		objectDao.executeHql("update I_knowledgeinfo t set t.deleted = '1', t.updatedate = '" + this.getData() + 
				"', t.updateemployeeid = '" + employee.getId() + "', t.updateemployeename = '" + employee.getRealname() + 
				"' where t.id in (" + ids + ")");
		this.sysLogService.saveObject(Config.LOGTYPESC, this.depict + ids, employee, I_knowledgeinfoServiceImpl.class);
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public I_knowledgeinfo getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		I_knowledgeinfo obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		
		return obj;
	}
	
	/**
	 * 顺序排序
	 * @param parameters
	 */
	public void updateSortObject(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		List<String> idList = (List<String>) objectMap.get("ids");
		String ids = this.getSQLinByIDList(idList);
		I_knowledgeinfo objDW = objectDao.get(objectMap.get("id").toString());
		if (objDW == null)
			throw new BusinessException(depict + "获取定位对象为空!");
		List<I_knowledgeinfo> objList = objectDao.queryByHql("from I_knowledgeinfo t where " +
				"t.bankinfoid = '" + objDW.getBankinfoid() + "' and t.deleted = '0' " +
				"and t.id not in (" + ids + ") " +
				"and t.sort >= " + objDW.getSort() + " order by t.sort asc");
		long sort = objDW.getSort();
		for (String id : idList) {
			objectDao.executeHql("update I_knowledgeinfo t set t.sort = " + sort + " where t.id = '" + id + "'");
			sort++;
		}
		for (I_knowledgeinfo obj : objList) {
			objectDao.executeHql("update I_knowledgeinfo t set t.sort = " + sort + " where t.id = '" + obj.getId() + "'");
			sort++;
		}
	}
	
	/**
	 * 修改状态
	 * @param parameters
	 */
	public void updateEnabledObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> idList = (ArrayList<String>) objectMap.get("ids");
		String ids = this.getSQLinByIDList(idList);
		boolean isEnabled = (Boolean) objectMap.get("isenabled");
		if (isEnabled){
			objectDao.executeHql("update I_knowledgeinfo t set t.auditing = 'true', t.auditingdate = '" + this.getData() + 
					"', t.auditingempid = '" + employee.getId() + 
					"', t.auditingempname = '" + employee.getRealname() + "' where t.id in (" + ids + ")");
			this.sysLogService.saveObject(Config.LOGTYPEBC, this.depict + " 修改“审核通过” " + ids, employee, I_knowledgeinfoServiceImpl.class);
		}else{
			objectDao.executeHql("update I_knowledgeinfo t set t.auditing = 'false' where t.id in (" + ids + ")");
			this.sysLogService.saveObject(Config.LOGTYPEBC, this.depict + " 修改“取消审核” " + ids, employee, I_knowledgeinfoServiceImpl.class);
		}
	}

}
