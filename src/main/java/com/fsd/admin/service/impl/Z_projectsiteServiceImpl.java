package com.fsd.admin.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;

import com.fsd.core.util.DateTimeUtil;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_projectsite;
import com.fsd.admin.service.Z_projectsiteService;
import com.fsd.admin.dao.Z_projectsiteDao;

@Repository("z_projectsiteServiceImpl")
public class Z_projectsiteServiceImpl extends BaseServiceImpl<Z_projectsite, String> implements Z_projectsiteService{
    
    private static final Logger log = Logger.getLogger(Z_projectsiteServiceImpl.class);
    private String depict = "";
    
    @Resource(name = "z_projectsiteDaoImpl")
	public void setBaseDao(Z_projectsiteDao Z_projectsiteDao) {
		super.setBaseDao(Z_projectsiteDao);
	}
	
	@Resource(name = "z_projectsiteDaoImpl")
	private Z_projectsiteDao objectDao;
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
    @Override
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
        c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		c.add(Restrictions.eq("deleted", "0"));
		c.addOrder(Order.asc("adddate"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("title") != null && !"".equals(objectMap.get("title"))){
				c.add(Restrictions.like("title", "%" + objectMap.get("title") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}

	/**
	 * 保存基本信息
	 * @param param
	 * @param obj
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	public Z_projectsite saveInfo(ParametersUtil param, String rootPaht, Z_projectsite obj, A_Employee employee) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			Z_projectsite old_obj = objectDao.get(obj.getId());
			if(obj.getF_applypdfurl().indexOf("pdf") != -1){
				String pdfUrl = obj.getF_applypdfurl();
				String swfUrl = pdfUrl.substring(0, pdfUrl.lastIndexOf(".")+1) + "swf";
				pdf2SWF(rootPaht + pdfUrl, rootPaht + swfUrl);
				old_obj.setF_applypdfurl(swfUrl);
			}else if(obj.getF_applypdfurl().indexOf("swf") != -1){

			}else{
				throw new BusinessException("请上传PDF格式的申报书");
			}
			objectDao.update(old_obj);
			return old_obj;
		}else{
			if(obj.getF_applypdfurl().indexOf("pdf") != -1){
				obj.setId(this.getUUID());
				String pdfUrl = obj.getF_applypdfurl();
				String swfUrl = pdfUrl.substring(0, pdfUrl.lastIndexOf(".")+1) + "swf";
				pdf2SWF(rootPaht + pdfUrl, rootPaht + swfUrl);
				obj.setF_applypdfurl(swfUrl);
				obj.setF_adddate(this.getData());
				obj.setF_addemployeeid(employee.getId());
				obj.setF_deleted("0");
				objectDao.save(obj);
				return obj;
			}else{
				throw new BusinessException("请上传PDF格式的申报书");
			}
		}
	}

	public void pdf2SWF(String sourceFile, String destFile) throws Exception{
		String SWFTOOLS_HOME = "/usr/local/swftools/bin/pdf2swf";
		//命令行命令
		String cmd = SWFTOOLS_HOME + " /" + sourceFile + " -o /" + destFile;
		//Runtime执行后返回创建的进程对象
		Process pro = Runtime.getRuntime().exec(cmd);
		BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(pro.getInputStream()));
		while (bufferedReader.readLine() != null);
		pro.waitFor();
	}

	/**
	 * 保存项目简介信息
	 * @param param
	 * @param obj
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	public Z_projectsite saveAbstract(ParametersUtil param, Z_projectsite obj, A_Employee employee) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			Z_projectsite old_obj = objectDao.get(obj.getId());
			old_obj.setF_abstract(obj.getF_abstract());
			old_obj.setF_videourl(obj.getF_videourl());
			objectDao.update(old_obj);
			return old_obj;
		}else{
			throw new BusinessException("系统错误，请联系管理员");
		}
	}

	/**
	 * 保存团队风采信息
	 * @param param
	 * @param obj
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	public Z_projectsite saveTeammien(ParametersUtil param, Z_projectsite obj, A_Employee employee) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			Z_projectsite old_obj = objectDao.get(obj.getId());
			old_obj.setF_teammien(obj.getF_teammien());
			objectDao.update(old_obj);
			return old_obj;
		}else{
			throw new BusinessException("系统错误，请联系管理员");
		}
	}

	/**
	 * 保存成果信息
	 * @param param
	 * @param obj
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	public Z_projectsite saveAchievement(ParametersUtil param, Z_projectsite obj, A_Employee employee) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			Z_projectsite old_obj = objectDao.get(obj.getId());
			old_obj.setF_achievement(obj.getF_achievement());
			objectDao.update(old_obj);
			return old_obj;
		}else{
			throw new BusinessException("系统错误，请联系管理员");
		}
	}

	/**
	 * 保存指导老师信息
	 * @param param
	 * @param obj
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	public Z_projectsite saveAdviser(ParametersUtil param, Z_projectsite obj, A_Employee employee) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			Z_projectsite old_obj = objectDao.get(obj.getId());
			old_obj.setF_adviser(obj.getF_adviser());
			objectDao.update(old_obj);
			return old_obj;
		}else{
			throw new BusinessException("系统错误，请联系管理员");
		}
	}

	/**
	 * 保存支撑材料信息
	 * @param param
	 * @param obj
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	public Z_projectsite saveMaterials(ParametersUtil param, Z_projectsite obj, A_Employee employee) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			Z_projectsite old_obj = objectDao.get(obj.getId());
			old_obj.setF_materials(obj.getF_materials());
			objectDao.update(old_obj);
			return old_obj;
		}else{
			throw new BusinessException("系统错误，请联系管理员");
		}
	}

    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
    @Override
	public void save(Z_projectsite obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
            Z_projectsite old_obj = objectDao.get(obj.getId());
            
            old_obj.setF_lastupdatedate(this.getData());//设置修改日期
			old_obj.setF_updateemployeeid(employee.getId());//设置修改用户id
			old_obj.setF_updateemployeename(employee.getRealname());//设置修改用户姓名
            objectDao.update(old_obj);
        }else{
            //添加
            obj.setId(this.getUUID());//设置主键
			obj.setF_adddate(this.getData());//设置添加日期
			obj.setF_addemployeeid(employee.getId());//设置添加用户id
			obj.setF_addemployeename(employee.getRealname());//设置添加用户姓名
			obj.setF_deleted("0");//设置删除标志(0:正常 1：已删除)
			objectDao.save(obj);
        }
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
    @Override
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		String hql = "";
		objectDao.executeHql("update Z_projectsite t set t.deleted = '1', t.updatedate = '" + this.getData() + 
				"', t.updateemployeeid = '" + employee.getId() + "', t.updateemployeename = '" + employee.getRealname() + 
				"' where t.id in (" + ids + ")");
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
    @Override
	public Z_projectsite getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		Z_projectsite obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		
		return obj;
	}

	/**
	 * 根据所属项目ID加载对象
	 * @param fid
	 * @return
	 * @throws Exception
	 */
	public Z_projectsite getObjectByFid(String fid) throws Exception{
		String sql = "select * from Z_projectsite where f_fid = ?";
		List<Z_projectsite> list = objectDao.queryBySql1(sql, fid);
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}
}
