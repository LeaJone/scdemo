package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import org.springframework.stereotype.Repository;

import com.fsd.core.common.BusinessException;
import com.fsd.core.util.Config;
import com.fsd.core.util.ParametersUtil;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.F_flownodeterm;
import com.fsd.admin.model.F_flowproject;
import com.fsd.admin.service.F_flownodetermService;
import com.fsd.admin.dao.F_flownodetermDao;
import com.google.gson.Gson;

@Repository("f_flownodetermServiceImpl")
public class F_flownodetermServiceImpl extends MainServiceImpl<F_flownodeterm, String> implements F_flownodetermService{
	
	//自动审批 节点条件属性类型 f_ftplx
	/**
	 * 字符串
	 */
	public static final String ftpzf1 = "f_ftpzf";
	public static final String ftpzf2 = "字符串";
	/**
	 * 整型
	 */
	public static final String ftpzx1 = "f_ftpzx";
	public static final String ftpzx2 = "整型";
	/**
	 * 浮点 
	 */
	public static final String ftpfd1 = "f_ftpfd";
	public static final String ftpfd2 = "浮点";
	/**
	 * 时间
	 */
	public static final String ftpsj1 = "f_ftpsj";
	public static final String ftpsj2 = "时间";
	/**
	 * 程序处理
	 */
	public static final String ftpcx1 = "f_ftpcx";
	public static final String ftpcx2 = "程序";
	

	//节点条件运算符类型 f_ftflx
	/**
	 * 等号
	 */
	public static final String ftfdh1 = "f_ftfdh";
	public static final String ftfdh2 = "等于";
	/**
	 * 大于
	 */
	public static final String ftfdy1 = "f_ftfdy";
	public static final String ftfdy2 = "大于";
	/**
	 * 大于等于
	 */
	public static final String ftfdd1 = "f_ftfdd";
	public static final String ftfdd2 = "大于等于";
	/**
	 * 小于
	 */
	public static final String ftfxy1 = "f_ftfxy";
	public static final String ftfxy2 = "小于";
	/**
	 * 小于等于
	 */
	public static final String ftfxd1 = "f_ftfxd";
	public static final String ftfxd2 = "小于等于";
	/**
	 * 包含
	 */
	public static final String ftfbh1 = "f_ftfbh";
	public static final String ftfbh2 = "包含";
	/**
	 * 不包含
	 */
	public static final String ftfbb1 = "f_ftfbb";
	public static final String ftfbb2 = "不包含";

	
	//数据取值方式 f_ftqzfs
	/**
	 * 不取值，不取值做条件，一般配合判断程序处理接口
	 */
	public static final String ftqzbq1 = "f_ftqzbq";
	public static final String ftqzbq2 = "不取值";
	/**
	 * 属性，提交对象中提取属性值
	 */
	public static final String ftqzsx1 = "f_ftqzsx";
	public static final String ftqzsx2 = "属性";
	/**
	 * 程序，系统程序接口获取值
	 */
	public static final String ftqzcx1 = "f_ftqzcx";
	public static final String ftqzcx2 = "程序";
	
	
    private static final Logger log = Logger.getLogger(F_flownodetermServiceImpl.class);
    private String depict = "流程节点条件";
    
    @Resource(name = "f_flownodetermDaoImpl")
	public void setBaseDao(F_flownodetermDao f_flownodetermDao) {
		super.setBaseDao(f_flownodetermDao);
	}
	
	@Resource(name = "f_flownodetermDaoImpl")
	private F_flownodetermDao objectDao;
	

	/**
	 * 根据节点对象ID加载对象集合
	 * @param previousID
	 * @return
	 * @throws Exception
	 */
	public List<F_flownodeterm> getObjectListByNodeID(String nodeID)  throws Exception{
		return objectDao.queryByHql("from F_flownodeterm t where deleted = '0' and nodeid = '" + nodeID + "'");
	}

	/**
	 * 根据节点对象ID加载对象集合
	 * @param previousID
	 * @return
	 * @throws Exception
	 */
	public List<F_flownodeterm> getObjectListByNodeID(ParametersUtil param, A_Employee employee)  throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		c.addOrder(Order.asc("sort"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("nodeid") != null && !"".equals(objectMap.get("nodeid"))){
				c.add(Restrictions.eq("nodeid", objectMap.get("nodeid").toString()));
			}
		}
		return objectDao.getList(c);
	}
    
    /**
	 * 保存对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public void save(F_flownodeterm obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
        	F_flownodeterm old_obj = objectDao.get(obj.getId());
        	old_obj.setPropertyname(obj.getPropertyname());
        	old_obj.setPropertydepict(obj.getPropertydepict());
        	old_obj.setPropertytype(obj.getPropertytype());
        	old_obj.setPropertytypename(obj.getPropertytypename());
        	old_obj.setOperator(obj.getOperator());
        	old_obj.setOperatorname(obj.getOperatorname());
        	old_obj.setValuewaycode(obj.getValuewaycode());
        	old_obj.setValuewayname(obj.getValuewayname());
        	old_obj.setValueinfo(obj.getValueinfo());
        	old_obj.setValuedepict(obj.getValuedepict());
        	old_obj.setSort(obj.getSort());
        	old_obj.setUpdatedate(this.getData());//设置修改日期
        	old_obj.setUpdateemployeeid(employee.getId());//设置修改用户id
        	old_obj.setUpdateemployeename(employee.getRealname());//设置修改用户姓名
            objectDao.update(old_obj);
        }else{
            //添加
            obj.setId(this.getUUID());//设置主键
			obj.setCompanyid(employee.getCompanyid());//所属单位
			obj.setAdddate(this.getData());//设置添加日期
			obj.setAddemployeeid(employee.getId());//设置添加用户id
			obj.setAddemployeename(employee.getRealname());//设置添加用户姓名
			obj.setDeleted("0");//设置删除标志(0:正常 1：已删除)
			objectDao.save(obj);
        }
        this.sysLogService.saveObject(Config.LOGTYPEBC, this.depict + obj.getId() + obj.getValuedepict(), employee, F_flownodetermServiceImpl.class);
	}
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = this.getSQLinByIDList(dir);
		objectDao.executeHql("update F_flownodeterm t set t.deleted = '1', t.updatedate = '" + this.getData() + 
				"', t.updateemployeeid = '" + employee.getId() + "', t.updateemployeename = '" + employee.getRealname() + 
				"' where t.id in (" + ids + ")");
		this.sysLogService.saveObject(Config.LOGTYPESC, this.depict + ids, employee, F_flownodetermServiceImpl.class);
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public F_flownodeterm getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		F_flownodeterm obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		return obj;
	}
	
	/**
	 * 顺序排序
	 * @param parameters
	 */
	public void updateSortObject(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		List<String> idList = (List<String>) objectMap.get("ids");
		String ids = this.getSQLinByIDList(idList);
		F_flownodeterm objDW = objectDao.get(objectMap.get("id").toString());
		if (objDW == null)
			throw new BusinessException(depict + "获取定位对象为空!");
		List<F_flownodeterm> objList = objectDao.queryByHql("from F_flownodeterm t where " +
				"t.nodeid = '" + objDW.getNodeid() + "' and t.deleted = '0' " +
				"and t.id not in (" + ids + ") " +
				"and t.sort >= " + objDW.getSort() + " order by t.sort asc");
		long sort = objDW.getSort();
		for (String id : idList) {
			objectDao.executeHql("update F_flownodeterm t set t.sort = " + sort + " where t.id = '" + id + "'");
			sort++;
		}
		for (F_flownodeterm obj : objList) {
			objectDao.executeHql("update F_flownodeterm t set t.sort = " + sort + " where t.id = '" + obj.getId() + "'");
			sort++;
		}
	}
}
