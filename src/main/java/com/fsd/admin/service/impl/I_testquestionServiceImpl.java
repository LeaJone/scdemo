package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.I_testquestion;
import com.fsd.admin.service.I_testquestionService;
import com.fsd.admin.dao.I_testquestionDao;

@Repository("i_testquestionServiceImpl")
public class I_testquestionServiceImpl extends BaseServiceImpl<I_testquestion, String> implements I_testquestionService{
    
    private static final Logger log = Logger.getLogger(I_testquestionServiceImpl.class);
    private String depict = "试题信息";
    
    @Resource(name = "i_testquestionDaoImpl")
	public void setBaseDao(I_testquestionDao I_testquestionDao) {
		super.setBaseDao(I_testquestionDao);
	}
	
	@Resource(name = "i_testquestionDaoImpl")
	private I_testquestionDao objectDao;
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
		Criteria c = objectDao.createCriteria();
        c.add(Restrictions.eq("companyid", employee.getCompanyid()));
        c.add(Restrictions.eq("testinfoid", objectMap.get("fid")));
		if(objectMap.get("name") != null && !"".equals(objectMap.get("name"))){
			c.add(Restrictions.like("problem", "%" + objectMap.get("name") + "%"));
		}
		c.add(Restrictions.eq("deleted", "0"));
		c.addOrder(Order.asc("adddate"));
		return objectDao.findPager(param , c);
	}
	
	/**
	 * 加载所有分页数据
	 * @param param
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageListAll(ParametersUtil param, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
		Criteria c = objectDao.createCriteria();
        c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		if(objectMap.get("key") != null && !"".equals(objectMap.get("key"))){
			c.add(Restrictions.like("problem", "%" + objectMap.get("key") + "%"));
		}
		c.add(Restrictions.eq("deleted", "0"));
		c.addOrder(Order.asc("adddate"));
		return objectDao.findPager(param , c);
	}
    
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public void save(I_testquestion obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
            I_testquestion old_obj = objectDao.get(obj.getId());
            
            old_obj.setTestinfoid(obj.getTestinfoid());
            old_obj.setTestinfoname(obj.getTestinfoname());
            old_obj.setTypecode(obj.getTypecode());
            old_obj.setTypename(obj.getTypename());
            old_obj.setCode(obj.getCode());
            old_obj.setProblem(obj.getProblem());
            //old_obj.setDescription(obj.getDescription());
            old_obj.setKeynote(obj.getKeynote());
            old_obj.setParse(obj.getParse());
            old_obj.setScore(obj.getScore());
            old_obj.setSort(obj.getSort());
            old_obj.setRemark(obj.getRemark());
            old_obj.setUpdatedate(this.getData());//设置修改日期
			old_obj.setUpdateemployeeid(employee.getId());//设置修改用户id
			old_obj.setUpdateemployeename(employee.getRealname());//设置修改用户姓名
            objectDao.update(old_obj);
        }else{
            //添加
            obj.setId(this.getUUID());//设置主键
			obj.setCompanyid(employee.getCompanyid());//所属单位
			obj.setAdddate(this.getData());//设置添加日期
			obj.setAddemployeeid(employee.getId());//设置添加用户id
			obj.setAddemployeename(employee.getRealname());//设置添加用户姓名
			obj.setDeleted("0");//设置删除标志(0:正常 1：已删除)
			objectDao.save(obj);
        }
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		String hql = "";
		objectDao.executeHql("update I_testquestion t set t.deleted = '1', t.updatedate = '" + this.getData() + 
				"', t.updateemployeeid = '" + employee.getId() + "', t.updateemployeename = '" + employee.getRealname() + 
				"' where t.id in (" + ids + ")");
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public I_testquestion getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		I_testquestion obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		
		return obj;
	}
	
	/**
	 * 组织试题树
	 * @param parameters
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	public List<I_testquestion> getTestquestionTree(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		Criteria c = objectDao.createCriteria();
        c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		if(objectMap != null && objectMap.get("key") != null && !"".equals(objectMap.get("key"))){
			c.add(Restrictions.like("name", "%" + objectMap.get("key") + "%"));
		}
		if(objectMap != null && objectMap.get("fid") != null && !"".equals(objectMap.get("fid"))){
			c.add(Restrictions.eq("testinfoid", objectMap.get("fid").toString()));
		}
		c.add(Restrictions.eq("deleted", "0"));
		c.addOrder(Order.asc("adddate"));
		return objectDao.getList(c);
	}
}
