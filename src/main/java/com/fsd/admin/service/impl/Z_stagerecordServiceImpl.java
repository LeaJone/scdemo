package com.fsd.admin.service.impl;

import com.fsd.admin.dao.Z_projectDao;
import com.fsd.admin.dao.Z_stagerecordDao;
import com.fsd.admin.dao.Z_teamanduserDao;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_project;
import com.fsd.admin.model.Z_stagerecord;
import com.fsd.admin.model.Z_teamanduser;
import com.fsd.admin.service.Z_stagerecordService;
import com.fsd.core.common.BusinessException;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.util.ParametersUtil;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

@Service("z_stagerecordServiceImpl")
public class Z_stagerecordServiceImpl extends BaseServiceImpl<Z_stagerecord, String> implements Z_stagerecordService {

    private String depict = "阶段成果";
    
    @Resource(name = "z_stagerecordDaoImpl")
	public void setBaseDao(Z_stagerecordDao Z_stagerecordDao) {
		super.setBaseDao(Z_stagerecordDao);
	}
	
	@Resource(name = "z_stagerecordDaoImpl")
	private Z_stagerecordDao objectDao;

	@Resource(name = "z_projectDaoImpl")
	private Z_projectDao projectDao;

	@Resource(name = "z_teamanduserDaoImpl")
	private Z_teamanduserDao teamanduserDao;

	/**
	 * 加载项目分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Z_project> getProjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = projectDao.createCriteria();
		c.add(Restrictions.eq("f_delete", "0"));
		List<Z_teamanduser> teamanduser = teamanduserDao.queryByHql("from Z_teamanduser where f_delete = '0' and f_userid = '" + employee.getId() + "'");
		if(teamanduser.size() != 0){
			String[] teamids = new String[teamanduser.size()];
			for (int i = 0; i < teamanduser.size(); i++) {
				teamids[i] = teamanduser.get(i).getF_teamid();
			}
			c.add(Restrictions.in("f_teamid", teamids));
		}else{
			c.add(Restrictions.eq("f_teamid", "XXXXX"));
		}
		c.add(Restrictions.eq("f_statusid", "5"));
		c.addOrder(Order.asc("f_adddate"));
		List<Z_project> oldList = (List<Z_project>)projectDao.findPager(param , c).getData();
		List<Z_project> newList = new ArrayList<>();
		for (int i = 0; i < oldList.size(); i++) {
			Z_project obj = projectDao.get(oldList.get(i).getId());
			obj.setF_subnum(String.valueOf(getCount(obj.getId())));
			newList.add(obj);
		}
		return newList;
	}
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
    @Override
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("f_delete", "0"));
		c.addOrder(Order.desc("f_subtime"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("fid") != null && !"".equals(objectMap.get("fid"))){
				c.add(Restrictions.eq("f_code", objectMap.get("fid")));
			}
		}
		return objectDao.findPager(param , c);
	}
    
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	@Override
	public void save(Z_stagerecord obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			Z_stagerecord old_obj = objectDao.get(obj.getId());
			old_obj.setF_subtime(this.getData());
			old_obj.setF_user(employee.getRealname());
			old_obj.setF_content(obj.getF_content());
			old_obj.setF_file(obj.getF_file());
			objectDao.update(old_obj);
		}else{
			//添加
			obj.setId(this.getUUID());
			obj.setF_delete("0");
			obj.setF_subtime(this.getData());
			obj.setF_user(employee.getRealname());
			objectDao.save(obj);
        }
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	@Override
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		objectDao.executeHql("update Z_stagerecord set f_delete = '1' where id in (" + ids + ")");
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	@Override
	public Z_stagerecord getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		Z_stagerecord obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		return obj;
	}

	/**
	 * 获取提交次数
	 * @param projectid
	 * @return
	 * @throws Exception
	 */
	private int getCount(String projectid) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("f_delete", "0"));
		c.add(Restrictions.eq("f_code", projectid));
		List<Z_stagerecord> list = objectDao.getList(c);
		return list.size();
	}

}
