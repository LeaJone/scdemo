package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.annotation.Resource;
import org.springframework.stereotype.Repository;

import com.fsd.core.common.BusinessException;
import com.fsd.core.util.Config;
import com.fsd.core.util.ParametersUtil;
import com.fsd.core.util.SpringContextHolder;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.fsd.admin.model.F_flowdealt;
import com.fsd.admin.model.F_flownode;
import com.fsd.admin.model.F_flownodeterm;
import com.fsd.admin.model.F_flowobject;
import com.fsd.admin.model.F_flowproject;
import com.fsd.admin.model.F_flowrecord;
import com.fsd.admin.service.F_flowWorkService;
import com.fsd.admin.service.F_flowdealtService;
import com.fsd.admin.service.F_flownodeService;
import com.fsd.admin.service.F_flownodetermService;
import com.fsd.admin.service.F_flowobjectService;
import com.fsd.admin.service.F_flowprojectService;
import com.fsd.admin.service.F_flowrecordService;
import com.fsd.admin.service.WorkFlowService;
import com.fsd.admin.dao.F_flowrecordDao;
import com.fsd.admin.entity.A_LoginInfo;
import com.google.gson.Gson;

@Repository("f_flowrecordServiceImpl")
public class F_flowrecordServiceImpl extends MainServiceImpl<F_flowrecord, String> implements F_flowrecordService{

	//流程记录状态 f_frjlzt
	public static final String frdsp1 = "f_frdsp";
	public static final String frdsp2 = "待审批";
	public static final String frspz1 = "f_frspz";
	public static final String frspz2 = "审批中";
	public static final String frty1 = "f_frty";
	public static final String frty2 = "同意";
	public static final String frjj1 = "f_frjj";
	public static final String frjj2 = "拒绝";
	public static final String frth1 = "f_frth";
	public static final String frth2 = "退回";
	public static final String frch1 = "f_frch";
	public static final String frch2 = "撤回";
	public static final String frzj1 = "f_frzj";
	public static final String frzj2 = "转交";
	
	//数据处理状态 f_sjclzt
	public static final String sjds1 = "f_sjds";
	public static final String sjds2 = "待审批";
	public static final String sjlc1 = "f_sjlc";
	public static final String sjlc2 = "流程中";
	public static final String sjwc1 = "f_sjwc";
	public static final String sjwc2 = "完成";
	public static final String sjth1 = "f_sjth";
	public static final String sjth2 = "退回";
	public static final String sjch1 = "f_sjch";
	public static final String sjch2 = "撤回";
	public static final String sjzz1 = "f_sjzz";
	public static final String sjzz2 = "终止";
	
	
    private static final Logger log = Logger.getLogger(F_flowrecordServiceImpl.class);
    private String depict = "流程记录";
    
	private static final Lock lock = new ReentrantLock();
    
    @Resource(name = "f_flowrecordDaoImpl")
	public void setBaseDao(F_flowrecordDao f_flowrecordDao) {
		super.setBaseDao(f_flowrecordDao);
	}
	
	@Resource(name = "f_flowrecordDaoImpl")
	private F_flowrecordDao objectDao;

    @Resource(name = "f_flowprojectServiceImpl")
	private F_flowprojectService flowProjectService;
    @Resource(name = "f_flownodeServiceImpl")
	private F_flownodeService flowNodeService;
    @Resource(name = "f_flownodetermServiceImpl")
	private F_flownodetermService flowNodeTermService;
    @Resource(name = "f_flowobjectServiceImpl")
	private F_flowobjectService flowObjectService;
    @Resource(name = "f_flowdealtServiceImpl")
	private F_flowdealtService flowDealtService;
	
	/**
	 * 获取流程记录中，提取审批的流程对象ID集合
	 * @param loginInfo
	 * @return
	 * @throws Exception
	 */
	public List<String> getFlowObjectIDListByExtract(A_LoginInfo loginInfo) throws Exception{
		List<String> objIDList = new ArrayList<String>();
		Map objIDMap = null;
		//当前操作用户节点
		String sql = "select distinct a.flowobjectid " +
				" from F_flowrecord a, F_flowdealt b " +
				" where a.id = b.recordid and a.dealtstatus = '" + frdsp1 + "' " +
				" and b.valueinfo = '" + loginInfo.getEmployee().getId() + "' ";
		List<Object> valueList = objectDao.queryBySql(sql);
		for (int i = 0; i < valueList.size(); i++) {
			objIDMap = (Map)valueList.get(i);
			objIDList.add(objIDMap.get("FLOWOBJECTID").toString());
		}
		//部门审批节点
		if (this.checkPopedom(Config.POPEDOMgzljgsp, loginInfo.getPopedomMap().get(Config.POPEDOMMEUN))){
			sql = "select distinct a.flowobjectid " +
					" from F_flowrecord a, F_flowdealt b " +
					" where a.id = b.recordid and a.nodetype = '" + F_flownodeServiceImpl.fnbmp + "' " +
					" and a.dealtstatus = '" + frdsp1 + "' " +
					" and b.valueinfo = '" + loginInfo.getEmployee().getBranchid() + "' ";
			valueList = objectDao.queryBySql(sql);
			for (int i = 0; i < valueList.size(); i++) {
				objIDMap = (Map)valueList.get(i);
				objIDList.add(objIDMap.get("FLOWOBJECTID").toString());
			}
		}
		return objIDList;
	}
	
	/**
	 * 获取流程记录中，释放审批的流程对象ID集合
	 * @param loginInfo
	 * @return
	 * @throws Exception
	 */
	public List<String> getFlowObjectIDListByRelease(A_LoginInfo loginInfo) throws Exception{
		List<String> objIDList = new ArrayList<String>();
		Map objIDMap = null;
		//当前操作用户节点
		String sql = "select distinct a.flowobjectid " +
				" from F_flowrecord a, F_flowdealt b " +
				" where a.id = b.recordid " +
				" and a.isextract = 'true' " +//开启提取机制
				" and a.dealtstatus = '" + frspz1 + "' " +
				" and b.valueinfo = '" + loginInfo.getEmployee().getId() + "' ";
		List<Object> valueList = objectDao.queryBySql(sql);
		for (int i = 0; i < valueList.size(); i++) {
			objIDMap = (Map)valueList.get(i);
			objIDList.add(objIDMap.get("FLOWOBJECTID").toString());
		}
		return objIDList;
	}
	
	/**
	 * 获取流程记录中，审批中的流程对象ID集合
	 * @param loginInfo
	 * @return
	 * @throws Exception
	 */
	public List<String> getFlowObjectIDListByPending(A_LoginInfo loginInfo) throws Exception{
		List<String> objIDList = new ArrayList<String>();
		Map objIDMap = null;
		//当前操作用户节点
		//没有提取机制
		String sql = "select distinct a.flowobjectid " +
				" from F_flowrecord a, F_flowdealt b " +
				" where a.id = b.recordid " +
				" and a.isextract = 'false' " +//没有提取机制
				" and a.dealtstatus = '" + frspz1 + "' " +
				" and b.dealtresults = '" + F_flowdealtServiceImpl.fddcl1 + "' " +
				" and b.valueinfo = '" + loginInfo.getEmployee().getId() + "' ";
		List<Object> valueList = objectDao.queryBySql(sql);
		for (int i = 0; i < valueList.size(); i++) {
			objIDMap = (Map)valueList.get(i);
			objIDList.add(objIDMap.get("FLOWOBJECTID").toString());
		}
		//开启提取机制
		sql = "select distinct a.flowobjectid " +
				" from F_flowrecord a, F_flowdealt b " +
				" where a.id = b.recordid " +
				" and a.isextract = 'true' " +//开启提取机制
				" and a.dealtstatus = '" + frspz1 + "' " +
				" and b.dealtresults = '" + F_flowdealtServiceImpl.fdtq1 + "' " +
				" and b.valueinfo = '" + loginInfo.getEmployee().getId() + "' ";
		valueList = objectDao.queryBySql(sql);
		for (int i = 0; i < valueList.size(); i++) {
			objIDMap = (Map)valueList.get(i);
			objIDList.add(objIDMap.get("FLOWOBJECTID").toString());
		}
		//部门审批节点
		if (this.checkPopedom(Config.POPEDOMgzljgsp, loginInfo.getPopedomMap().get(Config.POPEDOMMEUN))){
			sql = "select distinct a.flowobjectid " +
					" from F_flowrecord a, F_flowdealt b " +
					" where a.id = b.recordid " +
					" and a.nodetype = '" + F_flownodeServiceImpl.fnbmp + "' " +
					" and a.isextract = 'false' " +//没有提取机制
					" and a.dealtstatus = '" + frspz1 + "' " +
					" and b.dealtresults = '" + F_flowdealtServiceImpl.fddcl1 + "' " +
					" and b.valueinfo = '" + loginInfo.getEmployee().getBranchid() + "' ";
			valueList = objectDao.queryBySql(sql);
			for (int i = 0; i < valueList.size(); i++) {
				objIDMap = (Map)valueList.get(i);
				objIDList.add(objIDMap.get("FLOWOBJECTID").toString());
			}
		}
		return objIDList;
	}
	
	/**
	 * 获取流程记录中，可撤回的流程对象ID集合
	 * @param loginInfo
	 * @return
	 * @throws Exception
	 */
	public List<String> getFlowObjectIDListByRecall(A_LoginInfo loginInfo) throws Exception{
		List<String> objIDList = new ArrayList<String>();
		Map objIDMap = null;
		//当前操作用户节点
		String sql = "select distinct a.nextrecordid " +
				" from F_flowrecord a, F_flowdealt b " +
				" where a.id = b.recordid " +
				" and b.dealtresults in ('" + F_flowdealtServiceImpl.fdty1 + 
				"', '" + F_flowdealtServiceImpl.fdth1 + "', '" + F_flowdealtServiceImpl.fdzj1 + "') " +
				" and b.valueinfo = '" + loginInfo.getEmployee().getId() + "' ";
		String value = "";
		String ids = "";
		List<Object> valueList = objectDao.queryBySql(sql);
		for (int i = 0; i < valueList.size(); i++) {
			objIDMap = (Map)valueList.get(i);
			value = objIDMap.get("NEXTRECORDID").toString();
			if (value != null && !value.equals("")){
				if (!ids.equals("")){
					ids += ",";
				}
				ids += value;
			}
		}
		if (!ids.equals("")){
			objIDList.addAll(this.getFlowObjectIDByAutoNodeID(ids));
		}
		return objIDList;
	}
	/**
	 * 递归获取流程对象ID集合
	 * @param ids
	 * @return
	 */
	private List<String> getFlowObjectIDByAutoNodeID(String ids) throws Exception{
		List<String> objIDList = new ArrayList<String>();
		Map objIDMap = null;
		//不是自动节点记录
		//没有提取机制
		String sql = "select distinct a.flowobjectid " +
				" from F_flowrecord a, F_flowdealt b " +
				" where a.id = b.recordid " +
				" and a.nodetype <> '" + F_flownodeServiceImpl.fnzdp + "' " +//自动
				" and a.nodetype <> '" + F_flownodeServiceImpl.fndrb + "' " +//多人并
				" and a.isextract = 'false' " +//没有提取机制
				" and a.dealtstatus = '" + frspz1 + "' " +
				" and a.id in (" + ids + ") ";
		List<Object> valueList = objectDao.queryBySql(sql);
		for (int i = 0; i < valueList.size(); i++) {
			objIDMap = (Map)valueList.get(i);
			objIDList.add(objIDMap.get("FLOWOBJECTID").toString());
		}
		//开启提取机制
		sql = "select distinct a.flowobjectid " +
				" from F_flowrecord a, F_flowdealt b " +
				" where a.id = b.recordid " +
				" and a.nodetype <> '" + F_flownodeServiceImpl.fnzdp + "' " +//自动
				" and a.nodetype <> '" + F_flownodeServiceImpl.fndrb + "' " +//多人并
				" and a.isextract = 'true' " +//开启提取机制
				" and a.dealtstatus = '" + frdsp1 + "' " +
				" and a.id in (" + ids + ") ";
		valueList = objectDao.queryBySql(sql);
		for (int i = 0; i < valueList.size(); i++) {
			objIDMap = (Map)valueList.get(i);
			objIDList.add(objIDMap.get("FLOWOBJECTID").toString());
		}
		//多人并审批
		sql = "select distinct a.flowobjectid, a.id " +
				" from F_flowrecord a, F_flowdealt b " +
				" where a.id = b.recordid " +
				" and a.nodetype = '" + F_flownodeServiceImpl.fndrb + "' " +//多人并
				" and a.dealtstatus = '" + frspz1 + "' " +
				" and a.id in (" + ids + ") ";
		valueList = objectDao.queryBySql(sql);
		List<F_flowdealt> dealtList = null;
		boolean isTrue = true;
		for (int i = 0; i < valueList.size(); i++) {
			objIDMap = (Map)valueList.get(i);
			dealtList = this.flowDealtService.getObjectListByRecordID(objIDMap.get("ID").toString());
			isTrue = true;
			for (F_flowdealt deal : dealtList) {
				if (!deal.getDealtresults().equals(F_flowdealtServiceImpl.fddcl1)){
					isTrue = false;
					break;
				}
			}
			if (isTrue)
				objIDList.add(objIDMap.get("FLOWOBJECTID").toString());
		}
		//自动节点记录
		sql = "select distinct a.nextrecordid " +
				" from F_flowrecord a, F_flowdealt b " +
				" where a.id = b.recordid " +
				" and a.nodetype = '" + F_flownodeServiceImpl.fnzdp + "' " +
				" and a.id in (" + ids + ") ";
		valueList = objectDao.queryBySql(sql);
		ids = "";
		String value = "";
		for (int i = 0; i < valueList.size(); i++) {
			objIDMap = (Map)valueList.get(i);
			value = objIDMap.get("NEXTRECORDID").toString();
			if (!ids.equals("")){
				ids += ",";
			}
			ids += value;
		}
		if (!ids.equals("")){
			objIDList.addAll(this.getFlowObjectIDByAutoNodeID(ids));
		}
		return objIDList;
	}

	/**
	 * 获取流程记录中，处理过的流程对象ID集合
	 * @param loginInfo
	 * @return
	 * @throws Exception
	 */
	public List<String> getFlowObjectIDListByPassed(A_LoginInfo loginInfo) throws Exception{
		List<String> objIDList = new ArrayList<String>();
		Map objIDMap = null;
		//当前操作用户节点
		String sql = "select distinct a.flowobjectid " +
				" from F_flowrecord a, F_flowdealt b " +
				" where a.id = b.recordid " +
				" and a.dealtstatus <> '" + frdsp1 + "' " +
				" and a.dealtstatus <> '" + frspz1 + "' " +
				" and b.valueinfo = '" + loginInfo.getEmployee().getId() + "' ";
		List<Object> valueList = objectDao.queryBySql(sql);
		for (int i = 0; i < valueList.size(); i++) {
			objIDMap = (Map)valueList.get(i);
			objIDList.add(objIDMap.get("FLOWOBJECTID").toString());
		}
		return objIDList;
	}
	
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public F_flowrecord getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		F_flowrecord obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		return obj;
	}
	
	/**
	 * 根据nextRecordID加载对象
	 * @param nextRecordID
	 * @return
	 * @throws Exception
	 */
	public F_flowrecord getObjectByNextRecordId(String nextRecordID) throws Exception{
		List<F_flowrecord> objList = objectDao.queryByHql(
				"from F_flowrecord t where t.nextrecordid = ?", nextRecordID);
		if (objList.size() > 0)
			return objList.get(0);
		return null;
	}
	
	/**
	 * 根据ID加载记录对象、处理对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getRecordDealtByid(ParametersUtil parameters, A_LoginInfo loginInfo) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		F_flowrecord obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		F_flowdealt dealtObj = this.flowDealtService.getObjectByRecordIDEmployeeID(obj.getId(), loginInfo.getEmployee().getId());
		ParametersUtil util = new ParametersUtil();
		util.addSendObject("record", obj);
		util.addSendObject("dealt", dealtObj);
		return util;
	}
	
	/**
	 * 修改流程记录状态，待审批提取操作
	 * @param param
	 * @param loginInfo
	 * @throws Exception
	 */
	public void updateDealtStatusExtractByID(ParametersUtil param, A_LoginInfo loginInfo) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		lock.lock();
		try {
			List<F_flowrecord> objList = objectDao.queryByHql("from F_flowrecord t where t.id in (" + ids + ")");
			if (objList.size() == 0){
				return;
			}
			List<F_flowdealt> dealList = null;
			for (F_flowrecord obj : objList) {
				if (!obj.getIsextract().equals("true")){
					throw new BusinessException(depict + "节点类型未开启提取机制，无法进行提取操作！<br/>" +
							"节点名称：" + obj.getNodename());
				}
				if(!obj.getDealtstatus().equals(frdsp1)){
					throw new BusinessException(depict + "审批状态不是待审批，无法进行提取操作！<br/>" +
							"当前状态：" + obj.getDealtstatusname() + " 节点名称：" + obj.getNodename());
				}
				dealList = this.flowDealtService.getObjectListByRecordID(obj.getId());
				if (dealList == null || dealList.size() == 0){
					throw new BusinessException(depict + "未获取到处理记录，无法进行提取操作！<br/>" +
							"节点名称：" + obj.getNodename());
				}
				if (obj.getNodetype().equals(F_flownodeServiceImpl.fnbmp)){
					F_flowdealt deal = dealList.get(0);
					deal.setDealtresults(F_flowdealtServiceImpl.fdtq1);
					deal.setDealtresultsname(F_flowdealtServiceImpl.fdtq2);
					deal.setValueinfo(loginInfo.getEmployee().getId());
					deal.setValuedepict(loginInfo.getEmployee().getRealname());
					this.flowDealtService.update(deal);
				}else{
					boolean isTrue = true;
					for (F_flowdealt deal : dealList) {
						if (deal.getValueinfo().equals(loginInfo.getEmployee().getId())){
							deal.setDealtresults(F_flowdealtServiceImpl.fdtq1);
							deal.setDealtresultsname(F_flowdealtServiceImpl.fdtq2);
							this.flowDealtService.update(deal);
							isTrue = false;
						}
					}
					if (isTrue)
						throw new BusinessException(depict + "未获取到处理记录，无法进行提取操作！<br/>节点名称：" + obj.getNodename());
				}
			}
			objectDao.executeHql("update F_flowrecord t set t.dealtstatus = '" + frspz1 + 
					"', t.dealtstatusname = '" + frspz2 + 
					"' where t.id in (" + ids + ")");
		} finally {
			lock.unlock();
		}
	}
	
	/**
	 * 修改流程记录状态，审批中释放操作
	 * @param param
	 * @param loginInfo
	 * @throws Exception
	 */
	public void updateDealtStatusReleaseByID(ParametersUtil param, A_LoginInfo loginInfo) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		List<F_flowrecord> objList = objectDao.queryByHql("from F_flowrecord t where t.id in (" + ids + ")");
		if (objList.size() == 0){
			return;
		}
		List<F_flowdealt> dealList = null;
		for (F_flowrecord obj : objList) {
			if (!obj.getIsextract().equals("true")){
				throw new BusinessException(depict + "节点类型未开启提取机制，无法进行释放操作！<br/>" +
						"节点名称：" + obj.getNodename());
			}
			if(!obj.getDealtstatus().equals(frspz1)){
				throw new BusinessException(depict + "审批状态不是审批中，无法进行释放操作！<br/>" +
						"当前状态：" + obj.getDealtstatusname() + " 节点名称：" + obj.getNodename());
			}
			dealList = this.flowDealtService.getObjectListByRecordID(obj.getId());
			if (dealList == null || dealList.size() == 0){
				throw new BusinessException(depict + "未获取到处理记录，无法进行释放操作！<br/>" +
						"节点名称：" + obj.getNodename());
			}
			if (obj.getNodetype().equals(F_flownodeServiceImpl.fnbmp)){
				F_flowdealt deal = dealList.get(0);
				deal.setDealtresults(F_flowdealtServiceImpl.fdsf1);
				deal.setDealtresultsname(F_flowdealtServiceImpl.fdsf2);
				deal.setValueinfo(loginInfo.getEmployee().getBranchid());
				deal.setValuedepict(loginInfo.getEmployee().getBranchname());
				this.flowDealtService.update(deal);
			}else{
				for (F_flowdealt deal : dealList) {
					if (deal.getValueinfo().equals(loginInfo.getEmployee().getId())){
						deal.setDealtresults(F_flowdealtServiceImpl.fdsf1);
						deal.setDealtresultsname(F_flowdealtServiceImpl.fdsf2);
						this.flowDealtService.update(deal);
					}
				}
			}
		}
		objectDao.executeHql("update F_flowrecord t set t.dealtstatus = '" + frdsp1 + 
				"', t.dealtstatusname = '" + frdsp2 + 
				"' where t.id in (" + ids + ")");
	}
	
	private F_flowrecord getFlowRecordObject(String flowRecordID, F_flowobject flowObj, 
			F_flowproject project, F_flownode node){
		F_flowrecord record = new F_flowrecord();
		record.setId(flowRecordID);
		record.setCmpanyid(project.getCompanyid());
		record.setProjectid(project.getId());
		record.setProjectcode(project.getCode());
		record.setProjectname(project.getName());
		record.setOperatepath(project.getOperatepath());
		record.setOperatename(project.getOperatename());
		record.setEntitypath(project.getEntitypath());
		record.setEntityname(project.getEntityname());
		record.setWindowpath(project.getWindowpath());
		record.setWindowname(project.getWindowname());
		record.setDataobjectid(flowObj.getDataobjectid());
		record.setDataobjectname(flowObj.getDataobjectname());
		record.setNodeid(node.getId());
		record.setNodecode(node.getCode());
		record.setNodename(node.getName());
		record.setNodetype(node.getNodetype());
		record.setNodetypename(node.getNodetypename());
		record.setPreviousid(node.getPreviousid());
		record.setPreviousname(node.getPreviousname());
		record.setNextid(node.getNextid());
		record.setNextname(node.getNextname());
		record.setNexttype(node.getNexttype());
		record.setNexttypename(node.getNexttypename());
		record.setFalseid(node.getFalseid());
		record.setFalsename(node.getFalsename());
		record.setIsdeliver(node.getIsdeliver());
		record.setIsextract(node.getIsextract());
		record.setIsrefusal(node.getIsrefusal());
		record.setIsreturn(node.getIsreturn());
		record.setFlowobjectid(flowObj.getId());
		if (node.getIsextract().equals("true")){
			record.setDealtstatus(F_flowrecordServiceImpl.frdsp1);
			record.setDealtstatusname(F_flowrecordServiceImpl.frdsp2);
		}else{
			record.setDealtstatus(F_flowrecordServiceImpl.frspz1);
			record.setDealtstatusname(F_flowrecordServiceImpl.frspz2);
		}
		record.setStartdate(this.getData());
		return record;
	}
	private F_flowdealt getFlowDealtObject(F_flowobject flowObj, F_flowrecord record, 
			F_flownode node, F_flownodeterm term, 
			String zjBraID, String zjBraName, String zjEmpID, String zjEmpName, 
			A_LoginInfo loginInfo){
		F_flowdealt dealt = new F_flowdealt();
		dealt.setId(this.getUUID());
		dealt.setCmpanyid(flowObj.getCompanyid());
		dealt.setFlowobjectid(flowObj.getId());
		dealt.setRecordid(record.getId());
		dealt.setTermid(term.getId());
		dealt.setPropertyname(term.getPropertyname());
		dealt.setPropertydepict(term.getPropertydepict());
		dealt.setPropertytype(term.getPropertytype());
		dealt.setPropertytypename(term.getPropertytypename());
		dealt.setValuewaycode(term.getValuewaycode());
		dealt.setValuewayname(term.getValuewayname());
		if (zjEmpID != null && !zjEmpID.trim().equals("") &&
				zjEmpName != null && !zjEmpName.trim().equals("")){
    		dealt.setOperator(zjBraID);
    		dealt.setOperatorname(zjBraName);
    		dealt.setValueinfo(zjEmpID);
    		dealt.setValuedepict(zjEmpName);
		}else{
    		dealt.setOperator(term.getOperator());
    		dealt.setOperatorname(term.getOperatorname());
    		if (node.getNodetype().equals(F_flownodeServiceImpl.fnbmp)){
	    		dealt.setValueinfo(loginInfo.getEmployee().getBranchid());
	    		dealt.setValuedepict(loginInfo.getEmployee().getBranchname());
    		}else{
	    		dealt.setValueinfo(term.getValueinfo());
	    		dealt.setValuedepict(term.getValuedepict());
    		}
		}
		dealt.setSort(term.getSort());
		dealt.setDealtresults(F_flowdealtServiceImpl.fddcl1);
		dealt.setDealtresultsname(F_flowdealtServiceImpl.fddcl2);
		return dealt;
	}
	/**
	 * 流程记录保存，及保存处理记录
	 * @param flowRecordID
	 * @param flowObj
	 * @param project
	 * @param node
	 * @param loginInfo
	 * @throws Exception
	 */
	public void saveRecordAndDealt(String flowRecordID, F_flowobject flowObj, 
			F_flowproject project, F_flownode node, A_LoginInfo loginInfo) throws Exception{
		this.saveRecordAndDealt(flowRecordID, flowObj, project, node, 
				null, null, null, null, null, loginInfo);
	}
	/**
	 * 流程记录保存，及保存处理记录
	 * @param flowRecordID
	 * @param flowObj
	 * @param project
	 * @param node
	 * @param termIDList
	 * @param zjBraID
	 * @param zjBraName
	 * @param zjEmpID
	 * @param zjEmpName
	 * @param loginInfo
	 * @throws Exception
	 */
	public void saveRecordAndDealt(String flowRecordID, F_flowobject flowObj, 
			F_flowproject project, F_flownode node, ArrayList<String> termIDList, 
			String zjBraID, String zjBraName, String zjEmpID, String zjEmpName, 
			A_LoginInfo loginInfo) throws Exception{
		F_flowrecord record = this.getFlowRecordObject(flowRecordID, flowObj, project, node);
		this.save(record);

		//节点条件
		List<F_flownodeterm> termList = flowNodeTermService.getObjectListByNodeID(node.getId());
		if (termList == null || termList.size() == 0){
			throw new BusinessException("工作流程节点条件不存在，节点ID：“" + node.getId() + "”、节点名：“" + node.getName() + "”!");
		}
		//处理记录
		boolean isTrue = true;
		F_flowdealt dealt = null;
		for (F_flownodeterm term : termList) {
			if (termIDList != null){
				if (termIDList.indexOf(term.getId()) == -1){
					continue;
				}
			}
    		dealt = this.getFlowDealtObject(flowObj, record, node, term, 
    				zjBraID, zjBraName, zjEmpID, zjEmpName, loginInfo);
    		flowDealtService.save(dealt);
    		isTrue = false;
    		if (zjEmpID != null)
    			break;
		}
		if (isTrue){
			throw new BusinessException("工作流程节点条件不存在，节点ID：“" + node.getId() + "”、节点名：“" + node.getName() + "”!");
		}
	}
	
	/**
	 * 保存处理结果
	 * @param param
	 * @param loginInfo
	 * @throws Exception
	 */
	public void saveDealtResults(ParametersUtil param, A_LoginInfo loginInfo) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
		if(objectMap.get("results") == null || "".equals(objectMap.get("results"))){
			throw new BusinessException(depict + "保存处理结果，未获取到结果类型，无法保存！");
		}
		String results = objectMap.get("results").toString();
		if(objectMap.get("dealtid") == null || "".equals(objectMap.get("dealtid"))){
			throw new BusinessException(depict + "保存处理结果，未获取到处理记录ID，无法保存！");
		}
		String opinionDepict = "";
		if(objectMap.get("depict") != null){
			opinionDepict = objectMap.get("depict").toString().trim();
		}
		lock.lock();
		try {
			F_flowdealt dealt = this.flowDealtService.get(objectMap.get("dealtid").toString());
			if (dealt == null){
				throw new BusinessException(depict + "保存处理结果，未获取到处理记录对象，无法保存！");
			}
			F_flowrecord record = this.get(dealt.getRecordid());
			if (!record.getDealtstatus().equals(frspz1)){
				throw new BusinessException(depict + "保存处理结果，当前流程记录状态未在审批中，无法保存！<br/>" +
						"当前流程状态：" + record.getDealtstatusname());
			}
			F_flowproject project = this.flowProjectService.get(record.getProjectid());
			if (!project.getStatus().equals(F_flowprojectServiceImpl.fpqy1)){
				throw new BusinessException("工作流程项目已停止，项目编码：“" + project.getCode() + 
						"”、项目名：“" + project.getName() + "”!");
			}
			F_flownode nextNode = this.flowNodeService.get(record.getNextid());
			WorkFlowService operateClass = (WorkFlowService) SpringContextHolder.getBean(record.getOperatepath());
			if (operateClass == null){
				throw new BusinessException(depict + "保存处理结果，业务操作类不存在，无法保存！");
			}
			String zjBranchID = null;//转交信息
			String zjBranchName = null;
			String zjEmployeeID = null;
			String zjEmployeeName = null;
			ArrayList<String> nextTermIDList = null;//选择节点选择处理ID集合
			F_flowobject object = this.flowObjectService.get(dealt.getFlowobjectid());
		
			//流程处理记录
			dealt.setValueinfo(loginInfo.getEmployee().getId());
			dealt.setValuedepict(loginInfo.getEmployee().getRealname());
			dealt.setDealtdate(this.getData());
			dealt.setOpiniondepict(opinionDepict);
			dealt.setObjectjson(object.getObjectjson());
			record.setEnddate(dealt.getDealtdate());
			//同意
			if (results.equals(F_flowdealtServiceImpl.fdty1)){
				dealt.setDealtresults(F_flowdealtServiceImpl.fdty1);
				dealt.setDealtresultsname(F_flowdealtServiceImpl.fdty2);
				record.setDealtstatus(frty1);
				record.setDealtstatusname(frty2);
				if (nextNode.getNodetype().equals(F_flownodeServiceImpl.fnxzp)){
					//下一节点是选择审批节点
					if(objectMap.get("nextdealtidlist") == null){
						throw new BusinessException(depict + "保存处理结果，下一节点是选择审批，未获取到选择处理ID，无法保存！");
					}
					nextTermIDList = (ArrayList<String>) objectMap.get("nextdealtidlist");
					if (nextTermIDList.size() == 0){
						throw new BusinessException(depict + "保存处理结果，下一节点是选择审批，未获取到选择处理ID，无法保存！");
					}
				}else if (nextNode.getNodetype().equals(F_flownodeServiceImpl.fnzxy) ||
						nextNode.getNodetype().equals(F_flownodeServiceImpl.fnzxd)){
					//下一节点是自动选择节点
					if(objectMap.get("nextdealtidlist") == null){
						throw new BusinessException(depict + "保存处理结果，下一节点是自动选择审批，未获取到选择处理ID，无法保存！");
					}
					nextTermIDList = (ArrayList<String>) objectMap.get("nextdealtidlist");
					if (nextTermIDList.size() == 0){
						throw new BusinessException(depict + "保存处理结果，下一节点是自动选择审批，未获取到选择处理ID，无法保存！");
					}
				}
			}
			//拒绝
			else if (results.equals(F_flowdealtServiceImpl.fdjj1)){
				if (record.getIsrefusal().equals("false")){
					throw new BusinessException(depict + "保存处理结果，当前流程不允许拒绝操作，无法保存！");
				}
				dealt.setDealtresults(F_flowdealtServiceImpl.fdjj1);
				dealt.setDealtresultsname(F_flowdealtServiceImpl.fdjj2);
				record.setDealtstatus(frjj1);
				record.setDealtstatusname(frjj2);
				if (record.getFalseid() == null || record.getFalseid().equals("")){
					object.setFlowstatus(F_flowobjectServiceImpl.fozz1);
					object.setFlowstatusname(F_flowobjectServiceImpl.fozz2);
					this.flowObjectService.update(object);
					this.flowDealtService.update(dealt);
					this.update(record);
					//终止流程调用数据对象处理
					operateClass.saveWorkFlowStop(record, dealt, sjzz1, sjzz2, loginInfo);
					return;
				}
				nextNode =  this.flowNodeService.get(record.getFalseid());
			}
			//退回
			else if (results.equals(F_flowdealtServiceImpl.fdth1)){
				dealt.setDealtresults(F_flowdealtServiceImpl.fdth1);
				dealt.setDealtresultsname(F_flowdealtServiceImpl.fdth2);
				record.setDealtstatus(frth1);
				record.setDealtstatusname(frth2);
				nextNode = this.findPreviousNode(record.getPreviousid());
				if (nextNode == null){
					this.flowDealtService.update(dealt);
					this.update(record);
					//退回到流程申请人调用
					operateClass.saveWorkFlowBackOriginal(record, dealt, sjth1, sjth2, loginInfo);
					return;
				}
			}
			//撤回
			else if (results.equals(F_flowdealtServiceImpl.fdch1)){
				dealt.setDealtresults(F_flowdealtServiceImpl.fdch1);
				dealt.setDealtresultsname(F_flowdealtServiceImpl.fdch2);
				record.setDealtstatus(frch1);
				record.setDealtstatusname(frch2);
				nextNode = this.findPreviousNode(record.getPreviousid());
				if (nextNode == null){
					this.flowDealtService.update(dealt);
					this.update(record);
					//撤回到流程申请人调用
					operateClass.saveWorkFlowRecallOriginal(record, dealt, sjch1, sjch2, loginInfo);
					return;
				}
			}
			//转交
			else if (results.equals(F_flowdealtServiceImpl.fdzj1)){
				//处理转交人问题
				if(objectMap.get("zjbranchid") == null || "".equals(objectMap.get("zjbranchid"))){
					throw new BusinessException(depict + "保存处理结果，当前流程转交，未获取到转交人机构ID，无法保存！");
				}
				zjBranchID = objectMap.get("zjbranchid").toString();
				if(objectMap.get("zjbranchname") == null || "".equals(objectMap.get("zjbranchname"))){
					throw new BusinessException(depict + "保存处理结果，当前流程转交，未获取到转交人机构名称，无法保存！");
				}
				zjBranchName = objectMap.get("zjbranchname").toString();
				if(objectMap.get("zjemployeeid") == null || "".equals(objectMap.get("zjemployeeid"))){
					throw new BusinessException(depict + "保存处理结果，当前流程转交，未获取到转交人ID，无法保存！");
				}
				zjEmployeeID = objectMap.get("zjemployeeid").toString();
				if(objectMap.get("zjemployeename") == null || "".equals(objectMap.get("zjemployeename"))){
					throw new BusinessException(depict + "保存处理结果，当前流程转交，未获取到转交人姓名，无法保存！");
				}
				zjEmployeeName = objectMap.get("zjemployeename").toString();
				dealt.setDealtresults(F_flowdealtServiceImpl.fdzj1);
				dealt.setDealtresultsname(F_flowdealtServiceImpl.fdzj2);
				record.setDealtstatus(frzj1);
				record.setDealtstatusname(frzj2);
				nextNode = this.flowNodeService.get(record.getNodeid());
			}
			this.flowDealtService.update(dealt);
			
			//根据节点类型处理，多人并处理
			if (record.getNodetype().equals(F_flownodeServiceImpl.fndrb)){
				List<F_flowdealt> dealtList = this.flowDealtService.getObjectListByRecordID(record.getId());
				boolean isTrue = false;
				for (F_flowdealt objDealt : dealtList) {
					if (!objDealt.getDealtresults().equals(F_flowdealtServiceImpl.fdty1)){
						isTrue = true;
						break;
					}
				}//会签未完成
				if (isTrue){
					return;
				}
			}
			
			//根据下一节点进行操作
			if (nextNode == null)
				throw new BusinessException(depict + "保存处理结果<br/>节点名称：'" + record.getNodename() + 
						"'的下一个工作流程节点不存在，无法保存！");

			String flowRecordID = this.getUUID();
			record.setNextrecordid(flowRecordID);
			//自动节点
			if (nextNode.getNodetype().equals(F_flownodeServiceImpl.fnzdp) ||
					nextNode.getNodetype().equals(F_flownodeServiceImpl.fnzxy) ||
					nextNode.getNodetype().equals(F_flownodeServiceImpl.fnzxd)){
				JSONObject jObj = new JSONObject(dealt.getObjectjson());
				JSONObject jData = jObj.getJSONObject("data");
				//自动节点处理过程
				StringBuilder builder = new StringBuilder(flowRecordID);
				nextNode = this.executeAutoNode(builder, object, project, loginInfo, 
						operateClass, nextNode, nextTermIDList, jData);
				flowRecordID = builder.toString();
			}
			//结束节点
			if (nextNode.getNodetype().equals(F_flownodeServiceImpl.fnjsd)){
				object.setFlowstatus(F_flowobjectServiceImpl.fowc1);
				object.setFlowstatusname(F_flowobjectServiceImpl.fowc2);
				object.setNodeid(nextNode.getId());
				object.setNodename(nextNode.getName());
				this.flowObjectService.update(object);
				this.update(record);
				//完成流程全过程
				operateClass.saveWorkFlowFinish(record, dealt, sjwc1, sjwc2, loginInfo);
				return;
			}
			//下一级操作步骤
			object.setNodeid(nextNode.getId());
			object.setNodename(nextNode.getName());
			object.setRecordid(flowRecordID);
			this.flowObjectService.update(object);
			this.update(record);
			//流程记录保存
			this.saveRecordAndDealt(flowRecordID, object, project, nextNode, nextTermIDList, 
					zjBranchID, zjBranchName, zjEmployeeID, zjEmployeeName, loginInfo);
			//当前流程操作完成
			operateClass.saveWorkFlowNodeDealt(record, dealt, sjlc1, sjlc2, loginInfo);
		} finally {
			lock.unlock();
		}
	}
	/**
	 * 查找上一节点
	 * @param nodeid
	 * @return
	 */
	private F_flownode findPreviousNode(String nodeid) throws Exception{
		F_flownode node = this.flowNodeService.get(nodeid);
		if (node == null){
			F_flowproject project = this.flowProjectService.get(nodeid);
			if (project == null)
				throw new BusinessException(depict + "保存处理结果，退回操作未找到上一节点，无法保存！");
			return null;
		}
		if (node.getNodetype().equals(F_flownodeServiceImpl.fnzdp)){
			node = this.findPreviousNode(node.getPreviousid());
		}
		return node;
	}
	
	/**
	 * 执行自动节点流程处理
	 * @param node
	 * @param jObj
	 * @return
	 * @throws Exception
	 */
	public F_flownode executeAutoNode(StringBuilder flowRecordID, F_flowobject flowObj, F_flowproject project, 
			A_LoginInfo loginInfo, WorkFlowService operateClass, F_flownode node, 
			ArrayList<String> nextTermIDList, JSONObject jObj) throws Exception{
		List<F_flownodeterm> termList = this.flowNodeTermService.getObjectListByNodeID(node.getId());
		if (termList == null || termList.size() == 0)
			throw new BusinessException(depict + "保存处理结果，自动节点条件未设置，无法保存！<br/>" +
					"节点名称：" + node.getName());
		boolean isTrue = false;
		F_flowrecord record = this.getFlowRecordObject(flowRecordID.toString(), flowObj, project, node);
		flowRecordID.setLength(0);
		flowRecordID.append(this.getUUID());
		record.setNextrecordid(flowRecordID.toString());
		record.setEnddate(this.getData());
		this.save(record);
		F_flowdealt dealt = null;
		
		//自动审批节点处理数据
		if (node.getNodetype().equals(F_flownodeServiceImpl.fnzdp)){
			F_flownodeterm term = termList.get(0);
			dealt = this.getFlowDealtObject(flowObj, record, node, term, 
    				null, null, null, null, loginInfo);
			String value = null;
			if (term.getValuewaycode().equals(F_flownodetermServiceImpl.ftqzsx1)){
				if (term.getPropertyname() == null || term.getPropertyname().equals(""))
					throw new BusinessException(depict + "保存处理结果，自动节点属性名未设置，无法保存！<br/>" +
							"节点名称：" + node.getName());
				value = jObj.getString(term.getPropertyname());
			}else if(term.getValuewaycode().equals(F_flownodetermServiceImpl.ftqzcx1)){
				value = operateClass.saveWorkFlowNodeAutoValue(record, dealt, sjlc1, sjlc2, loginInfo);
			}else if(term.getValuewaycode().equals(F_flownodetermServiceImpl.ftqzbq1)){
				value = "";
			}else{
				throw new BusinessException(depict + "保存处理结果，自动节点未指定取值方式，无法保存！<br/>" +
						"节点名称：" + node.getName());
			}
			if (value == null){
				throw new BusinessException(depict + "保存处理结果，自动节点比较值为空（Null），无法保存！<br/>" +
						"节点名称：" + node.getName());
			}
			//字符串比较
			if (term.getPropertytype().equals(F_flownodetermServiceImpl.ftpzf1)){
				if (term.getOperator().equals(F_flownodetermServiceImpl.ftfdh1)){
					isTrue = value.equals(term.getValueinfo());
				} else if (term.getOperator().equals(F_flownodetermServiceImpl.ftfdy1)){
					isTrue = value.compareTo(term.getValueinfo()) > 0;
				} else if (term.getOperator().equals(F_flownodetermServiceImpl.ftfdd1)){
					isTrue = value.compareTo(term.getValueinfo()) >= 0;
				} else if (term.getOperator().equals(F_flownodetermServiceImpl.ftfxy1)){
					isTrue = value.compareTo(term.getValueinfo()) < 0;
				} else if (term.getOperator().equals(F_flownodetermServiceImpl.ftfxd1)){
					isTrue = value.compareTo(term.getValueinfo()) <= 0;
				} else if (term.getOperator().equals(F_flownodetermServiceImpl.ftfbh1)){
					isTrue = value.indexOf(term.getValueinfo()) != -1;
				} else if (term.getOperator().equals(F_flownodetermServiceImpl.ftfbb1)){
					isTrue = value.indexOf(term.getValueinfo()) == -1;
				} else {
					throw new BusinessException(depict + "保存处理结果，自动节点运算符未找到，无法保存！<br/>" +
						"节点名称：" + node.getName());
				}
			//时间比较
			} else if (term.getPropertytype().equals(F_flownodetermServiceImpl.ftpsj1)){
				if (term.getOperator().equals(F_flownodetermServiceImpl.ftfdh1)){
					isTrue = value.equals(term.getValueinfo());
				} else if (term.getOperator().equals(F_flownodetermServiceImpl.ftfdy1)){
					isTrue = value.compareTo(term.getValueinfo()) > 0;
				} else if (term.getOperator().equals(F_flownodetermServiceImpl.ftfdd1)){
					isTrue = value.compareTo(term.getValueinfo()) >= 0;
				} else if (term.getOperator().equals(F_flownodetermServiceImpl.ftfxy1)){
					isTrue = value.compareTo(term.getValueinfo()) < 0;
				} else if (term.getOperator().equals(F_flownodetermServiceImpl.ftfxd1)){
					isTrue = value.compareTo(term.getValueinfo()) <= 0;
				} else if (term.getOperator().equals(F_flownodetermServiceImpl.ftfbh1)){
					throw new BusinessException(depict + "保存处理结果，自动节点时间值无包含比较，无法保存！<br/>" +
						"节点名称：" + node.getName());
				} else if (term.getOperator().equals(F_flownodetermServiceImpl.ftfbb1)){
					throw new BusinessException(depict + "保存处理结果，自动节点时间值无不包含比较，无法保存！<br/>" +
						"节点名称：" + node.getName());
				} else {
					throw new BusinessException(depict + "保存处理结果，自动节点运算符未找到，无法保存！<br/>" +
						"节点名称：" + node.getName());
				}
			//整型比较
			} else if (term.getPropertytype().equals(F_flownodetermServiceImpl.ftpzx1)){
				int valueInt = Integer.parseInt(value);
				int termInt = Integer.parseInt(term.getValueinfo());
				if (term.getOperator().equals(F_flownodetermServiceImpl.ftfdh1)){
					isTrue = valueInt == termInt;
				} else if (term.getOperator().equals(F_flownodetermServiceImpl.ftfdy1)){
					isTrue = valueInt > termInt;
				} else if (term.getOperator().equals(F_flownodetermServiceImpl.ftfdd1)){
					isTrue = valueInt >= termInt;
				} else if (term.getOperator().equals(F_flownodetermServiceImpl.ftfxy1)){
					isTrue = valueInt < termInt;
				} else if (term.getOperator().equals(F_flownodetermServiceImpl.ftfxd1)){
					isTrue = valueInt <= termInt;
				} else if (term.getOperator().equals(F_flownodetermServiceImpl.ftfbh1)){
					throw new BusinessException(depict + "保存处理结果，自动节点整型值无包含比较，无法保存！<br/>" +
						"节点名称：" + node.getName());
				} else if (term.getOperator().equals(F_flownodetermServiceImpl.ftfbb1)){
					throw new BusinessException(depict + "保存处理结果，自动节点整型值无不包含比较，无法保存！<br/>" +
						"节点名称：" + node.getName());
				} else {
					throw new BusinessException(depict + "保存处理结果，自动节点运算符未找到，无法保存！<br/>" +
						"节点名称：" + node.getName());
				}
			//浮点比较
			} else if (term.getPropertytype().equals(F_flownodetermServiceImpl.ftpzx1)){
				double valueDou = Double.parseDouble(value);
				double termDou = Double.parseDouble(term.getValueinfo());
				if (term.getOperator().equals(F_flownodetermServiceImpl.ftfdh1)){
					isTrue = valueDou == termDou;
				} else if (term.getOperator().equals(F_flownodetermServiceImpl.ftfdy1)){
					isTrue = valueDou > termDou;
				} else if (term.getOperator().equals(F_flownodetermServiceImpl.ftfdd1)){
					isTrue = valueDou >= termDou;
				} else if (term.getOperator().equals(F_flownodetermServiceImpl.ftfxy1)){
					isTrue = valueDou < termDou;
				} else if (term.getOperator().equals(F_flownodetermServiceImpl.ftfxd1)){
					isTrue = valueDou <= termDou;
				} else if (term.getOperator().equals(F_flownodetermServiceImpl.ftfbh1)){
					throw new BusinessException(depict + "保存处理结果，自动节点浮点值无包含比较，无法保存！<br/>" +
						"节点名称：" + node.getName());
				} else if (term.getOperator().equals(F_flownodetermServiceImpl.ftfbb1)){
					throw new BusinessException(depict + "保存处理结果，自动节点浮点值无不包含比较，无法保存！<br/>" +
						"节点名称：" + node.getName());
				} else {
					throw new BusinessException(depict + "保存处理结果，自动节点运算符未找到，无法保存！<br/>" +
						"节点名称：" + node.getName());
				}
			} else if (term.getPropertytype().equals(F_flownodetermServiceImpl.ftpcx1)){
				//自动流程节点数据对象处理
				isTrue = operateClass.saveWorkFlowNodeAuto(record, dealt, sjlc1, sjlc2, loginInfo);
			} else {
				throw new BusinessException(depict + "保存处理结果，自动节点比较值类型未找到，无法保存！<br/>" +
						"节点名称：" + node.getName());
			}
			//处理结果保存
			dealt.setValueinfo(String.valueOf(isTrue));
			dealt.setDealtdate(record.getEnddate());
			dealt.setOpiniondepict("自动处理");
			dealt.setObjectjson(flowObj.getObjectjson());
			if (isTrue){
				dealt.setDealtresults(F_flowdealtServiceImpl.fdty1);
				dealt.setDealtresultsname(F_flowdealtServiceImpl.fdty2);
			}else{
				dealt.setDealtresults(F_flowdealtServiceImpl.fdjj1);
				dealt.setDealtresultsname(F_flowdealtServiceImpl.fdjj2);
			}
    		flowDealtService.save(dealt);
		} else if (node.getNodetype().equals(F_flownodeServiceImpl.fnzxy) ||
				node.getNodetype().equals(F_flownodeServiceImpl.fnzxd)){
			List<F_flowdealt> dealtList = new ArrayList<F_flowdealt>();
			for (F_flownodeterm term : termList) {
				if (nextTermIDList != null){
					if (nextTermIDList.indexOf(term.getId()) == -1){
						continue;
					}
				}
				dealt = this.getFlowDealtObject(flowObj, record, node, term, 
	    				null, null, null, null, loginInfo);
				dealtList.add(dealt);
			}
			if (dealtList.size() == 0){
				throw new BusinessException(depict + "保存处理结果，自动节点选择结果未空，无法保存！<br/>" +
						"节点名称：" + node.getName());
			}
			if (node.getNodetype().equals(F_flownodeServiceImpl.fnzxy)){
				if (dealtList.size() != 1){
					throw new BusinessException(depict + "保存处理结果，自动节点单选择结果不唯一，无法保存！<br/>" +
							"节点名称：" + node.getName());
				}
				isTrue = operateClass.saveWorkFlowNodeAutoChooseSingle(record, dealtList.get(0), sjlc1, sjlc2, loginInfo);
			}else{
				isTrue = operateClass.saveWorkFlowNodeAutoChooseMulti(record, dealtList, sjlc1, sjlc2, loginInfo);
			}
			for (F_flowdealt obj : dealtList) {
				dealt.setDealtdate(record.getEnddate());
				dealt.setOpiniondepict("自动处理");
				dealt.setObjectjson(flowObj.getObjectjson());
				if (isTrue){
					dealt.setDealtresults(F_flowdealtServiceImpl.fdty1);
					dealt.setDealtresultsname(F_flowdealtServiceImpl.fdty2);
				}else{
					dealt.setDealtresults(F_flowdealtServiceImpl.fdjj1);
					dealt.setDealtresultsname(F_flowdealtServiceImpl.fdjj2);
				}
	    		flowDealtService.save(dealt);
			}
		}
		
		F_flownode nextNode = null;
		if (isTrue){
			nextNode = this.flowNodeService.get(node.getNextid());
			if (nextNode == null)
				throw new BusinessException(depict + "保存处理结果，<br/>“" + node.getName() + 
						"”节点的下一个工作流程(True)节点不存在，无法保存！");
		}else{
			nextNode = this.flowNodeService.get(node.getFalseid());
			if (nextNode == null)
				throw new BusinessException(depict + "保存处理结果，<br/>“" + node.getName() + 
						"”节点的下一个工作流程(False)节点不存在，无法保存！");
		}
		if (nextNode.getNodetype().equals(F_flownodeServiceImpl.fnzdp)){
			nextNode = this.executeAutoNode(flowRecordID, flowObj, project, loginInfo, operateClass, nextNode, null, jObj);
		}else if (nextNode.getNodetype().equals(F_flownodeServiceImpl.fnzxy) ||
				nextNode.getNodetype().equals(F_flownodeServiceImpl.fnzxd)){
			throw new BusinessException(depict + "保存处理结果，<br/>“" + node.getName() + 
					"”节点的下一个工作流程节点不得为自动选择节点，无法保存！");
		}
		return nextNode;
	}
}
