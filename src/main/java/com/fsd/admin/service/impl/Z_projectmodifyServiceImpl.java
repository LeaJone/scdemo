package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.Map;
import javax.annotation.Resource;

import com.fsd.admin.model.Z_project;
import com.fsd.admin.service.Z_projectService;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_projectmodify;
import com.fsd.admin.service.Z_projectmodifyService;
import com.fsd.admin.dao.Z_projectmodifyDao;

@Repository("z_projectmodifyServiceImpl")
public class Z_projectmodifyServiceImpl extends BaseServiceImpl<Z_projectmodify, String> implements Z_projectmodifyService{
    
    private static final Logger log = Logger.getLogger(Z_projectmodifyServiceImpl.class);
    private String depict = "";
    
    @Resource(name = "z_projectmodifyDaoImpl")
	public void setBaseDao(Z_projectmodifyDao Z_projectmodifyDao) {
		super.setBaseDao(Z_projectmodifyDao);
	}
	
	@Resource(name = "z_projectmodifyDaoImpl")
	private Z_projectmodifyDao objectDao;

    @Autowired
    private Z_projectService projectService;

    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
    @Override
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("f_deleted", "0"));
		c.addOrder(Order.asc("f_adddate"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("projectid") != null && !"".equals(objectMap.get("projectid"))){
				c.add(Restrictions.eq("f_projectid", objectMap.get("projectid")));
			}else{
				throw new BusinessException("系统错误，缺少项目信息");
			}
		}
		return objectDao.findPager(param , c);
	}
    
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
    @Override
	public void save(Z_projectmodify obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
            Z_projectmodify old_obj = objectDao.get(obj.getId());
			old_obj.setF_modifytypeid(obj.getF_modifytypeid());
			old_obj.setF_modifytypename(obj.getF_modifytypename());
			old_obj.setF_modifyinfo(obj.getF_modifyinfo());
			old_obj.setF_modifyreason(obj.getF_modifyreason());
            old_obj.setF_lastupdatedate(this.getData());//设置修改日期
			old_obj.setF_updateemployeeid(employee.getId());//设置修改用户id
			old_obj.setF_updateemployeename(employee.getRealname());//设置修改用户姓名
            objectDao.update(old_obj);
        }else{
            //添加
            obj.setId(this.getUUID());//设置主键

			Z_project project = projectService.get(obj.getF_projectid());
			if(project == null){
				throw new BusinessException("项目信息错误，请联系管理员");
			}
			obj.setF_projectname(project.getF_name());
			obj.setF_adddate(this.getData());//设置添加日期
			obj.setF_addemployeeid(employee.getId());//设置添加用户id
			obj.setF_addemployeename(employee.getRealname());//设置添加用户姓名
			obj.setF_deleted("0");//设置删除标志(0:正常 1：已删除)
			objectDao.save(obj);
        }
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
    @Override
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		for (String id : dir) {
			objectDao.delete(id);
		}
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
    @Override
	public Z_projectmodify getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		Z_projectmodify obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		
		return obj;
	}
}
