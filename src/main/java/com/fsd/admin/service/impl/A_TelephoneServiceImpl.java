package com.fsd.admin.service.impl;

import javax.annotation.Resource;
import org.springframework.stereotype.Repository;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.admin.model.A_Telephone;
import com.fsd.admin.service.A_TelephoneService;
import com.fsd.admin.dao.A_TelephoneDao;

@Repository("A_TelephoneServiceImpl")
public class A_TelephoneServiceImpl extends MainServiceImpl<A_Telephone, String> implements A_TelephoneService{
    
    @Resource(name = "A_TelephoneDaoImpl")
	public void setBaseDao(A_TelephoneDao A_TelephoneDao) {
		super.setBaseDao(A_TelephoneDao);
	}
	
	@Resource(name = "A_TelephoneDaoImpl")
	private A_TelephoneDao A_TelephoneDao;
}
