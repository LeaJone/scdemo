package com.fsd.admin.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.usermodel.Range;

import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.DateTimeUtil;
import com.fsd.core.util.ParametersUtil;
import com.fsd.core.util.PdfUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_edumuke;
import com.fsd.admin.model.Z_eduprofession;
import com.fsd.admin.model.Z_eduproject;
import com.fsd.admin.service.Z_eduprojectService;
import com.fsd.admin.dao.Z_eduprojectDao;

@Repository("z_eduprojectServiceImpl")
public class Z_eduprojectServiceImpl extends BaseServiceImpl<Z_eduproject, String> implements Z_eduprojectService{
    
    private static final Logger log = Logger.getLogger(Z_eduprojectServiceImpl.class);
    private String depict = "";
    
    @Resource(name = "z_eduprojectDaoImpl")
	public void setBaseDao(Z_eduprojectDao Z_eduprojectDao) {
		super.setBaseDao(Z_eduprojectDao);
	}
	
	@Resource(name = "z_eduprojectDaoImpl")
	private Z_eduprojectDao objectDao;
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
    @Override
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("f_deleted", "0"));
		c.add(Restrictions.eq("f_addemployeeid", employee.getId()));
		c.addOrder(Order.asc("f_adddate"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("name") != null && !"".equals(objectMap.get("name"))){
				c.add(Restrictions.like("f_name", "%" + objectMap.get("name") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}
    
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
    @Override
	public void save(String rootpath,Z_eduproject obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
        	File file = new File("uploadfiles/projectfile/" + obj.getId() + ".pdf");//删除PDF文件
    		if(file.exists()) {
    			file.delete();
    		}
            //修改
            Z_eduproject old_obj = objectDao.get(obj.getId());
            old_obj.setF_name(obj.getF_name());
            old_obj.setF_basename(obj.getF_basename());
            old_obj.setF_principalname(obj.getF_principalname());
            old_obj.setF_money(obj.getF_money());
            old_obj.setF_member(obj.getF_member());
            
            old_obj.setF_lastupdatedate(this.getData());//设置修改日期
			old_obj.setF_updateemployeeid(employee.getId());//设置修改用户id
			old_obj.setF_updateemployeename(employee.getRealname());//设置修改用户姓名
			doPdf(rootpath,old_obj);//生成修改后的PDF文件
            objectDao.update(old_obj);
        }else{
            //添加
            obj.setId(this.getUUID());//设置主键
            obj.setF_addemployeeid(employee.getId());
			obj.setF_adddate(this.getData());//设置添加日期
			obj.setF_addemployeeid(employee.getId());//设置添加用户id
			obj.setF_addemployeename(employee.getRealname());//设置添加用户姓名
			obj.setF_deleted("0");//设置删除标志(0:正常 1：已删除)
			doPdf(rootpath,obj);//生成PDF文件
			objectDao.save(obj);
        }
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
    @Override
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		String hql = "";
		objectDao.executeHql("update Z_eduproject t set t.f_deleted = '1', t.f_lastupdatedate = '" + this.getData() + 
				"', t.f_updateemployeeid = '" + employee.getId() + "', t.f_updateemployeename = '" + employee.getRealname() + 
				"' where t.id in (" + ids + ")");
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
    @Override
	public Z_eduproject getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		Z_eduproject obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		
		return obj;
	}
    private void doPdf(String rootpath,Z_eduproject obj) throws Exception {
    	
    	String pdfurl = "uploadfiles/projectfile/" + obj.getId() + ".doc";
		obj.setF_pdfurl(pdfurl);
		//生成PDF文件
		String templatePath = rootpath + "uploadfiles/pdftemplate/edu_project.doc";
		String outPaht = rootpath + pdfurl;
		Map<String,String> map= new HashMap<String,String>();
		map.put("project_name", obj.getF_name());
		map.put("project_principalname", obj.getF_principalname());
		map.put("project_date", DateTimeUtil.formatDate(obj.getF_adddate(), "yyyyMMddHHmmss", "yyyy年MM月dd日"));
		InputStream inputStream = Z_eduprojectServiceImpl.class.getClassLoader().getResourceAsStream(templatePath);
	      //InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(tmpFile);
	      HWPFDocument document = null;
	      try {
	          document = new HWPFDocument(inputStream);
	      } catch (IOException e) {
	          e.printStackTrace();
	      }
	      // 读取文本内容
	      Range bodyRange = document.getRange();
	      // 替换内容
	      for (Map.Entry<String, String> entry : map.entrySet()) {
	          bodyRange.replaceText("${" + entry.getKey() + "}", entry.getValue());
	      }

	      //导出到文件
	      try {
	          ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	          document.write(byteArrayOutputStream);
	          OutputStream outputStream = new FileOutputStream(outPaht);
	          outputStream.write(byteArrayOutputStream.toByteArray());
	          outputStream.close();
	      } catch (IOException e) {
	          e.printStackTrace();
	      }
    }

	@Override
	public void uploadFile(Z_eduproject obj, A_Employee loginUser) {
		Z_eduproject old_obj = objectDao.get(obj.getId());
		old_obj.setF_pdfurl(obj.getF_pdfurl());
		objectDao.save(old_obj);
	}

	@Override
	public List<Z_eduproject> selectAll(String start, String end) {
		return objectDao.queryBySql1("select * from Z_eduproject where f_adddate > ? and f_adddate < ? and f_deleted = ?", start, end, "0");
	}
}
