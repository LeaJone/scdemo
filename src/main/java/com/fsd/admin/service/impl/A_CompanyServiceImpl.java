package com.fsd.admin.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.fsd.admin.model.*;
import com.fsd.admin.service.*;
import com.fsd.core.util.CopyDirectory;
import com.fsd.core.util.EncryptUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.fsd.core.common.BusinessException;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.util.Config;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.dao.A_CompanyDao;
import com.google.gson.Gson;

@Repository("A_CompanyServiceImpl")
public class A_CompanyServiceImpl extends MainServiceImpl<A_Company, String> implements A_CompanyService{

	private String depict = "单位";
	
    @Resource(name = "A_CompanyDaoImpl")
	public void setBaseDao(A_CompanyDao A_CompanyDao) {
		super.setBaseDao(A_CompanyDao);
	}
	
	@Resource(name = "A_CompanyDaoImpl")
	private A_CompanyDao objectDao;

	@Resource(name = "A_EmployeeServiceImpl")
	private A_EmployeeService employeeService;

	@Autowired
	private A_templateService templateService;

	@Autowired
	private B_parametertypeService parametertypeService;

	@Autowired
	private B_parameterlistService parameterlistService;
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public A_Company getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		return this.getObjectById(objectMap.get("id"));
	}
	/**
	 * 根据ID加载对象
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public A_Company getObjectById(Object id) throws Exception{
		A_Company obj = objectDao.get(id.toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		return obj;
	}
	
	/**
	 * 站群专用跳转站点查询方法
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<A_Company> getObjectListBySiteChange(ParametersUtil param) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.addOrder(Order.asc("id"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("name") != null && !"".equals(objectMap.get("name"))){
				c.add(Restrictions.like("name", "%" + objectMap.get("name") + "%"));
			}
		}
		return objectDao.getList(c);
	}
	
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.add(Restrictions.not(Restrictions.eq("id", "FSDCOMPANY")));
		c.addOrder(Order.asc("name"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("name") != null && !"".equals(objectMap.get("name"))){
				c.add(Restrictions.like("name", "%" + objectMap.get("name") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}

	/**
	 * 保存对象
	 * @param obj
	 * @param employee
	 * @param rootPath
	 * @throws Exception
	 */
	@Override
	public void saveObject(A_Company obj, A_Employee employee, String rootPath) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			A_Company company = objectDao.get(obj.getId());
			if(company == null){
				throw new BusinessException(depict + "数据不存在!");
			}

			// 如果站点编码发生变化
			if(!company.getCode().equals(obj.getCode())){
				throw new BusinessException(depict + "站点编码不允许修改!");
			}
			if(!company.getTemplateid().equals(obj.getTemplateid())){
				// 拷贝模板文件
				A_template template = templateService.get(obj.getTemplateid());
				File templateFile = new File(rootPath + template.getPaht());
				File siteFile = new File(rootPath + "/template/" + obj.getCode());
				if(siteFile.exists()){
					siteFile.delete();
				}
				siteFile.mkdirs();
				CopyDirectory.copyFileOrDir(templateFile.getPath(), siteFile.getPath());
				// 拷贝模板资源文件
				File templateSourceFile = new File(rootPath + "/resource/" + template.getCode());
				File siteSourceFile = new File(rootPath + "/resource/" + obj.getCode());
				if(siteSourceFile.exists()){
					siteSourceFile.delete();
				}
				siteSourceFile.mkdirs();
				CopyDirectory.copyFileOrDir(templateSourceFile.getPath(), siteSourceFile.getPath());

				// 创建站点参数
				// createParameter(obj.getCode(), employee1);
			}

			company.setCode(obj.getCode());
			company.setName(obj.getName());
			company.setSymbol(obj.getSymbol());
			company.setDefaulttypeid(obj.getDefaulttypeid());
			company.setDefaulttypename(obj.getDefaulttypename());
			company.setAreatypeid(obj.getAreatypeid());
			company.setAreatypename(obj.getAreatypename());
			company.setProfessiontypeid(obj.getProfessiontypeid());
			company.setProfessiontypename(obj.getProfessiontypename());
			company.setStatus(obj.getStatus());
			company.setStatusname(obj.getStatusname());
			company.setRemark(obj.getRemark());
			company.setTemplateid(obj.getTemplateid());
			company.setTemplatename(obj.getTemplatename());
			//设置修改日期
			company.setUpdatedate(this.getData());
			//设置修改用户id
			company.setUpdateemployeeid(employee.getId());
			//设置修改用户姓名
			company.setUpdateemployeename(employee.getRealname());
			objectDao.update(company);
		}else{
			//添加
			obj.setId(obj.getCode());
			//设置添加日期
			obj.setAdddate(this.getData());
			//设置添加用户id
			obj.setAddemployeeid(employee.getId());
			//设置添加用户姓名
			obj.setAddemployeename(employee.getRealname());
			//设置删除标志(0:正常 1：已删除)
			obj.setDeleted("0");

			// 拷贝模板文件
			A_template template = templateService.get(obj.getTemplateid());
			File templateFile = new File(rootPath + template.getPaht());
			File siteFile = new File(rootPath + "/template/" + obj.getCode());
			if(siteFile.exists()){
				throw new BusinessException(depict + "站点编码重复!");
			}
			siteFile.mkdirs();
			CopyDirectory.copyFileOrDir(templateFile.getPath(), siteFile.getPath());
			// 拷贝模板资源文件
			File templateSourceFile = new File(rootPath + "/resource/" + template.getCode());
			File siteSourceFile = new File(rootPath + "/resource/" + obj.getCode());
			if(siteSourceFile.exists()){
				throw new BusinessException(depict + "站点编码重复!");
			}
			siteSourceFile.mkdirs();
			CopyDirectory.copyFileOrDir(templateSourceFile.getPath(), siteSourceFile.getPath());

			// 创建站点默认管理员用户
			A_Employee employee1 = new A_Employee();
			employee1.setId(this.getUUID());
			employee1.setCompanyid(obj.getId());
			employee1.setCompanyname(obj.getName());
			employee1.setLoginname("admin" + obj.getCode());
			employee1.setPassword(EncryptUtils.md5Password("123456", "123456"));
			employee1.setIsadmin(true);
			employee1.setStatus("ryztqy");
			employee1.setStatusname("启用");
			employee1.setDeleted("0");
			employeeService.save(employee1);

			// 创建站点参数
			createParameter(template.getCode(), employee1);

			objectDao.save(obj);
		}
		this.sysLogService.saveObject(Config.LOGTYPEBC, this.depict + obj.getId() + obj.getName(), employee, A_CompanyServiceImpl.class);
	}
	
	/**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		objectDao.executeHql("update A_Company t set t.deleted = '1' where t.id in (" + ids + ")");
	}
	
	/**
	 * 修改状态
	 * @param parameters
	 */
	@SuppressWarnings("unchecked")
	public void updateEnabledObject(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		boolean isEnabled = (Boolean) objectMap.get("isenabled");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		if (isEnabled)
			objectDao.executeHql("update A_Company t set t.status = 'dwztqy', t.statusname = '启用' where t.id in (" + ids + ")");
		else
			objectDao.executeHql("update A_Company t set t.status = 'dwztty', t.statusname = '停用' where t.id in (" + ids + ")");
	}

	public void createParameter(String templateCode, A_Employee employee1) throws Exception{
		// 创建站点参数
		if("temp1".equals(templateCode)){
			B_parametertype parametertype = new B_parametertype();
			parametertype.setId(this.getUUID());
			parametertype.setCompanyid(employee1.getCompanyid());
			parametertype.setParentid(employee1.getCompanyid());
			parametertype.setParentname("参数类型");
			parametertype.setName("首页模块");
			parametertype.setSymbol("symk");
			parametertype.setSort(Long.valueOf(1));
			parametertype.setIsleaf("true");
			parametertype.setAdddate(this.getData());
			parametertype.setAddemployeeid(employee1.getId());
			parametertype.setDeleted("0");
			parametertypeService.save(parametertype);

			B_parameterlist parameterlist = new B_parameterlist();
			parameterlist.setId(employee1.getCompanyid() + "-temp1-1");
			parameterlist.setCompanyid(employee1.getCompanyid());
			parameterlist.setParametertypeid(parametertype.getId());
			parameterlist.setName("参数1");
			parameterlist.setTitle("参数1");
			parameterlist.setSort(Long.valueOf(1));
			parameterlist.setStatus("csqy");
			parameterlist.setStatusname("启用");
			parameterlist.setAdddate(this.getData());
			parameterlist.setAddemployeeid(employee1.getId());
			parameterlist.setDeleted("0");
			parameterlistService.save(parameterlist);

			parameterlist = new B_parameterlist();
			parameterlist.setId(employee1.getCompanyid() + "-temp1-2");
			parameterlist.setCompanyid(employee1.getCompanyid());
			parameterlist.setParametertypeid(parametertype.getId());
			parameterlist.setName("参数2");
			parameterlist.setTitle("参数2");
			parameterlist.setSort(Long.valueOf(1));
			parameterlist.setStatus("csqy");
			parameterlist.setStatusname("启用");
			parameterlist.setAdddate(this.getData());
			parameterlist.setAddemployeeid(employee1.getId());
			parameterlist.setDeleted("0");
			parameterlistService.save(parameterlist);

			parameterlist = new B_parameterlist();
			parameterlist.setId(employee1.getCompanyid() + "-temp1-3");
			parameterlist.setCompanyid(employee1.getCompanyid());
			parameterlist.setParametertypeid(parametertype.getId());
			parameterlist.setName("参数3");
			parameterlist.setTitle("参数3");
			parameterlist.setSort(Long.valueOf(1));
			parameterlist.setStatus("csqy");
			parameterlist.setStatusname("启用");
			parameterlist.setAdddate(this.getData());
			parameterlist.setAddemployeeid(employee1.getId());
			parameterlist.setDeleted("0");
			parameterlistService.save(parameterlist);

			parameterlist = new B_parameterlist();
			parameterlist.setId(employee1.getCompanyid() + "-temp1-4");
			parameterlist.setCompanyid(employee1.getCompanyid());
			parameterlist.setParametertypeid(parametertype.getId());
			parameterlist.setName("参数4");
			parameterlist.setTitle("参数4");
			parameterlist.setSort(Long.valueOf(1));
			parameterlist.setStatus("csqy");
			parameterlist.setStatusname("启用");
			parameterlist.setAdddate(this.getData());
			parameterlist.setAddemployeeid(employee1.getId());
			parameterlist.setDeleted("0");
			parameterlistService.save(parameterlist);

			parameterlist = new B_parameterlist();
			parameterlist.setId(employee1.getCompanyid() + "-temp1-5");
			parameterlist.setCompanyid(employee1.getCompanyid());
			parameterlist.setParametertypeid(parametertype.getId());
			parameterlist.setName("参数5");
			parameterlist.setTitle("参数5");
			parameterlist.setSort(Long.valueOf(1));
			parameterlist.setStatus("csqy");
			parameterlist.setStatusname("启用");
			parameterlist.setAdddate(this.getData());
			parameterlist.setAddemployeeid(employee1.getId());
			parameterlist.setDeleted("0");
			parameterlistService.save(parameterlist);

			parameterlist = new B_parameterlist();
			parameterlist.setId(employee1.getCompanyid() + "-temp1-6");
			parameterlist.setCompanyid(employee1.getCompanyid());
			parameterlist.setParametertypeid(parametertype.getId());
			parameterlist.setName("参数6");
			parameterlist.setTitle("参数6");
			parameterlist.setSort(Long.valueOf(1));
			parameterlist.setStatus("csqy");
			parameterlist.setStatusname("启用");
			parameterlist.setAdddate(this.getData());
			parameterlist.setAddemployeeid(employee1.getId());
			parameterlist.setDeleted("0");
			parameterlistService.save(parameterlist);

			parameterlist = new B_parameterlist();
			parameterlist.setId(employee1.getCompanyid() + "-temp1-7");
			parameterlist.setCompanyid(employee1.getCompanyid());
			parameterlist.setParametertypeid(parametertype.getId());
			parameterlist.setName("参数7");
			parameterlist.setTitle("参数7");
			parameterlist.setSort(Long.valueOf(1));
			parameterlist.setStatus("csqy");
			parameterlist.setStatusname("启用");
			parameterlist.setAdddate(this.getData());
			parameterlist.setAddemployeeid(employee1.getId());
			parameterlist.setDeleted("0");
			parameterlistService.save(parameterlist);

			parameterlist = new B_parameterlist();
			parameterlist.setId(employee1.getCompanyid() + "-temp1-8");
			parameterlist.setCompanyid(employee1.getCompanyid());
			parameterlist.setParametertypeid(parametertype.getId());
			parameterlist.setName("参数8");
			parameterlist.setTitle("参数8");
			parameterlist.setSort(Long.valueOf(1));
			parameterlist.setStatus("csqy");
			parameterlist.setStatusname("启用");
			parameterlist.setAdddate(this.getData());
			parameterlist.setAddemployeeid(employee1.getId());
			parameterlist.setDeleted("0");
			parameterlistService.save(parameterlist);

			parameterlist = new B_parameterlist();
			parameterlist.setId(employee1.getCompanyid() + "-temp1-9");
			parameterlist.setCompanyid(employee1.getCompanyid());
			parameterlist.setParametertypeid(parametertype.getId());
			parameterlist.setName("参数9");
			parameterlist.setTitle("参数9");
			parameterlist.setSort(Long.valueOf(1));
			parameterlist.setStatus("csqy");
			parameterlist.setStatusname("启用");
			parameterlist.setAdddate(this.getData());
			parameterlist.setAddemployeeid(employee1.getId());
			parameterlist.setDeleted("0");
			parameterlistService.save(parameterlist);

			parameterlist = new B_parameterlist();
			parameterlist.setId(employee1.getCompanyid() + "-temp1-10");
			parameterlist.setCompanyid(employee1.getCompanyid());
			parameterlist.setParametertypeid(parametertype.getId());
			parameterlist.setName("参数10");
			parameterlist.setTitle("参数10");
			parameterlist.setSort(Long.valueOf(1));
			parameterlist.setStatus("csqy");
			parameterlist.setStatusname("启用");
			parameterlist.setAdddate(this.getData());
			parameterlist.setAddemployeeid(employee1.getId());
			parameterlist.setDeleted("0");
			parameterlistService.save(parameterlist);

			parameterlist = new B_parameterlist();
			parameterlist.setId(employee1.getCompanyid() + "-temp1-12");
			parameterlist.setCompanyid(employee1.getCompanyid());
			parameterlist.setParametertypeid(parametertype.getId());
			parameterlist.setName("参数11");
			parameterlist.setTitle("参数11");
			parameterlist.setSort(Long.valueOf(1));
			parameterlist.setStatus("csqy");
			parameterlist.setStatusname("启用");
			parameterlist.setAdddate(this.getData());
			parameterlist.setAddemployeeid(employee1.getId());
			parameterlist.setDeleted("0");
			parameterlistService.save(parameterlist);

			parameterlist = new B_parameterlist();
			parameterlist.setId(employee1.getCompanyid() + "-temp1-13");
			parameterlist.setCompanyid(employee1.getCompanyid());
			parameterlist.setParametertypeid(parametertype.getId());
			parameterlist.setName("参数12");
			parameterlist.setTitle("参数12");
			parameterlist.setSort(Long.valueOf(1));
			parameterlist.setStatus("csqy");
			parameterlist.setStatusname("启用");
			parameterlist.setAdddate(this.getData());
			parameterlist.setAddemployeeid(employee1.getId());
			parameterlist.setDeleted("0");
			parameterlistService.save(parameterlist);

			parameterlist = new B_parameterlist();
			parameterlist.setId(employee1.getCompanyid() + "-temp1-14");
			parameterlist.setCompanyid(employee1.getCompanyid());
			parameterlist.setParametertypeid(parametertype.getId());
			parameterlist.setName("参数13");
			parameterlist.setTitle("参数13");
			parameterlist.setSort(Long.valueOf(1));
			parameterlist.setStatus("csqy");
			parameterlist.setStatusname("启用");
			parameterlist.setAdddate(this.getData());
			parameterlist.setAddemployeeid(employee1.getId());
			parameterlist.setDeleted("0");
			parameterlistService.save(parameterlist);

			parameterlist = new B_parameterlist();
			parameterlist.setId(employee1.getCompanyid() + "-temp1-15");
			parameterlist.setCompanyid(employee1.getCompanyid());
			parameterlist.setParametertypeid(parametertype.getId());
			parameterlist.setName("参数14");
			parameterlist.setTitle("参数14");
			parameterlist.setSort(Long.valueOf(1));
			parameterlist.setStatus("csqy");
			parameterlist.setStatusname("启用");
			parameterlist.setAdddate(this.getData());
			parameterlist.setAddemployeeid(employee1.getId());
			parameterlist.setDeleted("0");
			parameterlistService.save(parameterlist);

			parameterlist = new B_parameterlist();
			parameterlist.setId(employee1.getCompanyid() + "-temp1-16");
			parameterlist.setCompanyid(employee1.getCompanyid());
			parameterlist.setParametertypeid(parametertype.getId());
			parameterlist.setName("参数15");
			parameterlist.setTitle("参数15");
			parameterlist.setSort(Long.valueOf(1));
			parameterlist.setStatus("csqy");
			parameterlist.setStatusname("启用");
			parameterlist.setAdddate(this.getData());
			parameterlist.setAddemployeeid(employee1.getId());
			parameterlist.setDeleted("0");
			parameterlistService.save(parameterlist);

			parameterlist = new B_parameterlist();
			parameterlist.setId(employee1.getCompanyid() + "-temp1-17");
			parameterlist.setCompanyid(employee1.getCompanyid());
			parameterlist.setParametertypeid(parametertype.getId());
			parameterlist.setName("参数16");
			parameterlist.setTitle("参数16");
			parameterlist.setSort(Long.valueOf(1));
			parameterlist.setStatus("csqy");
			parameterlist.setStatusname("启用");
			parameterlist.setAdddate(this.getData());
			parameterlist.setAddemployeeid(employee1.getId());
			parameterlist.setDeleted("0");
			parameterlistService.save(parameterlist);

			parameterlist = new B_parameterlist();
			parameterlist.setId(employee1.getCompanyid() + "-temp1-18");
			parameterlist.setCompanyid(employee1.getCompanyid());
			parameterlist.setParametertypeid(parametertype.getId());
			parameterlist.setName("参数17");
			parameterlist.setTitle("参数17");
			parameterlist.setSort(Long.valueOf(1));
			parameterlist.setStatus("csqy");
			parameterlist.setStatusname("启用");
			parameterlist.setAdddate(this.getData());
			parameterlist.setAddemployeeid(employee1.getId());
			parameterlist.setDeleted("0");
			parameterlistService.save(parameterlist);

			parameterlist = new B_parameterlist();
			parameterlist.setId(employee1.getCompanyid() + "-temp1-19");
			parameterlist.setCompanyid(employee1.getCompanyid());
			parameterlist.setParametertypeid(parametertype.getId());
			parameterlist.setName("参数18");
			parameterlist.setTitle("参数18");
			parameterlist.setSort(Long.valueOf(1));
			parameterlist.setStatus("csqy");
			parameterlist.setStatusname("启用");
			parameterlist.setAdddate(this.getData());
			parameterlist.setAddemployeeid(employee1.getId());
			parameterlist.setDeleted("0");
			parameterlistService.save(parameterlist);

			parameterlist = new B_parameterlist();
			parameterlist.setId(employee1.getCompanyid() + "-temp1-20");
			parameterlist.setCompanyid(employee1.getCompanyid());
			parameterlist.setParametertypeid(parametertype.getId());
			parameterlist.setName("参数19");
			parameterlist.setTitle("参数19");
			parameterlist.setSort(Long.valueOf(1));
			parameterlist.setStatus("csqy");
			parameterlist.setStatusname("启用");
			parameterlist.setAdddate(this.getData());
			parameterlist.setAddemployeeid(employee1.getId());
			parameterlist.setDeleted("0");
			parameterlistService.save(parameterlist);

			parameterlist = new B_parameterlist();
			parameterlist.setId(employee1.getCompanyid() + "-temp1-21");
			parameterlist.setCompanyid(employee1.getCompanyid());
			parameterlist.setParametertypeid(parametertype.getId());
			parameterlist.setName("参数20");
			parameterlist.setTitle("参数20");
			parameterlist.setSort(Long.valueOf(1));
			parameterlist.setStatus("csqy");
			parameterlist.setStatusname("启用");
			parameterlist.setAdddate(this.getData());
			parameterlist.setAddemployeeid(employee1.getId());
			parameterlist.setDeleted("0");
			parameterlistService.save(parameterlist);

			parameterlist = new B_parameterlist();
			parameterlist.setId(employee1.getCompanyid() + "-temp1-22");
			parameterlist.setCompanyid(employee1.getCompanyid());
			parameterlist.setParametertypeid(parametertype.getId());
			parameterlist.setName("参数21");
			parameterlist.setTitle("参数21");
			parameterlist.setSort(Long.valueOf(1));
			parameterlist.setStatus("csqy");
			parameterlist.setStatusname("启用");
			parameterlist.setAdddate(this.getData());
			parameterlist.setAddemployeeid(employee1.getId());
			parameterlist.setDeleted("0");
			parameterlistService.save(parameterlist);

			parameterlist = new B_parameterlist();
			parameterlist.setId(employee1.getCompanyid() + "-temp1-23");
			parameterlist.setCompanyid(employee1.getCompanyid());
			parameterlist.setParametertypeid(parametertype.getId());
			parameterlist.setName("参数22");
			parameterlist.setTitle("参数22");
			parameterlist.setSort(Long.valueOf(1));
			parameterlist.setStatus("csqy");
			parameterlist.setStatusname("启用");
			parameterlist.setAdddate(this.getData());
			parameterlist.setAddemployeeid(employee1.getId());
			parameterlist.setDeleted("0");
			parameterlistService.save(parameterlist);

			parameterlist = new B_parameterlist();
			parameterlist.setId(employee1.getCompanyid() + "-temp1-24");
			parameterlist.setCompanyid(employee1.getCompanyid());
			parameterlist.setParametertypeid(parametertype.getId());
			parameterlist.setName("参数23");
			parameterlist.setTitle("参数23");
			parameterlist.setSort(Long.valueOf(1));
			parameterlist.setStatus("csqy");
			parameterlist.setStatusname("启用");
			parameterlist.setAdddate(this.getData());
			parameterlist.setAddemployeeid(employee1.getId());
			parameterlist.setDeleted("0");
			parameterlistService.save(parameterlist);

			parametertype = new B_parametertype();
			parametertype.setId(employee1.getCompanyid() + "-temp1-11");
			parametertype.setCompanyid(employee1.getCompanyid());
			parametertype.setParentid(employee1.getCompanyid());
			parametertype.setParentname("参数类型");
			parametertype.setName("首页组图");
			parametertype.setSymbol("syzt");
			parametertype.setSort(Long.valueOf(1));
			parametertype.setIsleaf("true");
			parametertype.setAdddate(this.getData());
			parametertype.setAddemployeeid(employee1.getId());
			parametertype.setDeleted("0");
			parametertypeService.save(parametertype);
		}
	}
}
