package com.fsd.admin.service.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.fsd.admin.service.Sys_SystemLogService;
import com.fsd.core.service.impl.BaseServiceImpl;


public class MainServiceImpl<T, PK extends Serializable> extends BaseServiceImpl<T, PK>{

    @Resource(name = "Sys_SystemLogServiceImpl")
	protected Sys_SystemLogService sysLogService;
    
    /**
     * 获取ID字符串sql
     * @param idList
     * @return
     */
    protected String getSQLinByIDList(List<String> idList){
    	String ids = "";
    	if (idList != null && idList.size() > 0){
    		for (String id : idList) {
    			if (!ids.equals("")){
    				ids += ",";
    			}
    			ids += "'" + id + "'";
			}
    	}
    	return ids;
    }
    
    /**
     * 检查权限是否拥有权限
     * @param code
     * @param type
     * @param popedomMap
     * @return
     */
    protected boolean checkPopedom(String code, String type, Map<String, List<String>> popedomMap){
    	if (popedomMap != null && popedomMap.size() > 0){
    		if (popedomMap.containsKey(type)){
    			if (popedomMap.get(type).indexOf(code) != -1){
    				return true;
    			}
    		}
    	}
    	return false;
    }
    /**
     * 检查权限是否拥有权限
     * @param code
     * @param popedomList
     * @return
     */
    protected boolean checkPopedom(String code, List<String> popedomList){
    	if (popedomList != null && popedomList.size() > 0){
    		if (popedomList.indexOf(code) != -1){
    			return true;
    		}
    	}
		return false;
    }
}
