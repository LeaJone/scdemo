package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.bean.Tree;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_qdflb;
import com.fsd.admin.model.Z_qdnrb;
import com.fsd.admin.service.Z_qdnrbService;
import com.fsd.admin.dao.Z_qdnrbDao;

@Repository("z_qdnrbServiceImpl")
public class Z_qdnrbServiceImpl extends BaseServiceImpl<Z_qdnrb, String> implements Z_qdnrbService{
    
    private static final Logger log = Logger.getLogger(Z_qdnrbServiceImpl.class);
    private String depict = "";
    
    @Resource(name = "z_qdnrbDaoImpl")
	public void setBaseDao(Z_qdnrbDao Z_qdnrbDao) {
		super.setBaseDao(Z_qdnrbDao);
	}
	
	@Resource(name = "z_qdnrbDaoImpl")
	private Z_qdnrbDao objectDao;
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		c.addOrder(Order.asc("adddate"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("title") != null && !"".equals(objectMap.get("title"))){
				c.add(Restrictions.like("bt", "%" + objectMap.get("title") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}
    
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public void save(Z_qdnrb obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
            Z_qdnrb old_obj = objectDao.get(obj.getId());
            
            old_obj.setLbid(obj.getLbid());//设置修改类别ID
            old_obj.setParentid(obj.getParentid());//设置修改上级内容
            old_obj.setBt(obj.getBt());//设置修改标题
            old_obj.setCbjg(obj.getCbjg());//设置修改承办机关
            old_obj.setSsyj(obj.getSsyj());//设置修改实施依据
            old_obj.setSszt(obj.getSszt());//设置修改实施主体
            old_obj.setZrsx(obj.getZrsx());//设置修改责任事项
            old_obj.setZzqk(obj.getZzqk());//设置修改追责情形
            old_obj.setIsaurl(obj.getIsaurl());//设置修改是否外链接
            old_obj.setAurl(obj.getAurl());//设置修改外链接
            old_obj.setIsaurlstuff(obj.getIsaurlstuff());//设置修改是否资料链接
            old_obj.setAurlstuff(obj.getAurlstuff());//设置修改资料链接
            old_obj.setIsaurlguides(obj.getIsaurlguides());//设置修改是否指南链接
            old_obj.setAurlguides(obj.getAurlguides());//设置修改指南链接
            old_obj.setBz(obj.getBz());//设置修改备注
            old_obj.setSort(obj.getSort());//设置修改序号
            old_obj.setUpdatedate(this.getData());//设置修改日期
			old_obj.setUpdateemployeeid(employee.getId());//设置修改用户id
			old_obj.setUpdateemployeename(employee.getRealname());//设置修改用户姓名
            objectDao.update(old_obj);
        }else{
            //添加
            obj.setId(this.getUUID());//设置主键
            obj.setCompanyid(employee.getCompanyid());//所属单位
			obj.setAdddate(this.getData());//设置添加日期
			obj.setAddemployeeid(employee.getId());//设置添加用户id
			obj.setAddemployeename(employee.getRealname());//设置添加用户姓名
			obj.setDeleted("0");//设置删除标志(0:正常 1：已删除)
			objectDao.save(obj);
			
        }
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		String hql = "";
		objectDao.executeHql("update Z_qdnrb t set t.deleted = '1', t.updatedate = '" + this.getData() + 
				"', t.updateemployeeid = '" + employee.getId() + "', t.updateemployeename = '" + employee.getRealname() + 
				"' where t.id in (" + ids + ")");
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public Z_qdnrb getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		Z_qdnrb obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		
		return obj;
	}
	
	/**
	 * 修改状态
	 * @param parameters
	 */
	public void updateEnabledObject(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		boolean isEnabled = (Boolean) objectMap.get("isenabled");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		if (isEnabled)
			objectDao.executeHql("update Z_qdnrb t set t.auditing = 'true' where t.id in (" + ids + ")");
		else
			objectDao.executeHql("update Z_qdnrb t set t.auditing = 'false' where t.id in (" + ids + ")");
	}
}
