package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.bean.Tree;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.dao.T_coursetypeDao;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.T_coursetype;
import com.fsd.admin.service.T_coursetypeService;

/**
 * 课程分类Service实现类
 * @author Administrator
 *
 */
@Service("t_coursetypeServiceImpl")
public class T_coursetypeServiceImpl extends BaseServiceImpl<T_coursetype, String> implements T_coursetypeService{
    
    private static final Logger log = Logger.getLogger(T_coursetypeServiceImpl.class);
	
    private String depict = "课程类型信息：";
    
	@Resource(name = "t_coursetypeDaoImpl")
	private T_coursetypeDao objectDao;
    
	/**
	 * 检查是否是子节点
	 * @param fid
	 * @return
	 */
	private int checkLeaf(String fid) {
		long num = objectDao.getCount("f_parentid = '" + fid + "'", "f_state = 'ztqy'", "f_deleted = 0");
		if (num > 0)
			return 0;
		return 1;
	}
	
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("f_deleted", 0L));
		c.addOrder(Order.asc("f_order"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if (objectMap.get("fid") != null && !"".equals(objectMap.get("fid"))) {
				c.add(Restrictions.eq("f_parentid", objectMap.get("fid")));
			}
			if (objectMap.get("name") != null && !"".equals(objectMap.get("name"))) {
				c.add(Restrictions.like("f_title", "%" + objectMap.get("name") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}
    
	/**
	 * 异步加载对象树
	 * @return
	 * @throws Exception
	 */
	public List<Tree> getAsyncObjectTree(String fid) throws Exception {
		List<Tree> treelist = new ArrayList<Tree>();
		List<T_coursetype> list = objectDao.queryByHql("from T_coursetype where f_parentid = '" + fid + "' and f_deleted = 0 and f_state = 'ztqy' order by f_order asc");
		Tree menu = null;
		for (T_coursetype obj : list) {
			menu = new Tree();
			menu.setId(obj.getId());
			menu.setText(obj.getF_title());
			if (obj.getF_leaf() == 1)
				menu.setLeaf(true);
			else
				menu.setLeaf(false);
			treelist.add(menu);
		}
		return treelist;
	}
	
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public void save(T_coursetype obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
        	if (obj.getId().equals(obj.getF_parentid())){
    			throw new BusinessException(depict + "所属类型不得选择自己！");
        	}
			T_coursetype old_obj = objectDao.get(obj.getId());
			String fid = old_obj.getF_parentid();
			old_obj.setF_parentid(obj.getF_parentid());
			old_obj.setF_title(obj.getF_title());;
			old_obj.setF_order(obj.getF_order());
			old_obj.setF_updateuser(employee.getId());
			old_obj.setF_updatedate(this.getData());
			old_obj.setF_imageurl(obj.getF_imageurl());
			objectDao.update(old_obj);
			if (!fid.equals(obj.getF_parentid())) {
				objectDao.executeHql("update T_coursetype set f_leaf = " + this.checkLeaf(obj.getF_parentid()) + " where id = '" + obj.getF_parentid() + "'");
				objectDao.executeHql("update T_coursetype set f_leaf = " + this.checkLeaf(fid) + " where id = '" + fid + "'");
			}
        }else{
            //添加
            obj.setId(this.getUUID());//设置主键
        	obj.setF_adduser(employee.getId());
			obj.setF_adddate(this.getData());
			obj.setF_leaf(1L);
			obj.setF_state("ztqy");
			obj.setF_deleted(0L);
			objectDao.save(obj);
			objectDao.executeHql("update T_coursetype set f_leaf = 0 where id = '" + obj.getF_parentid() + "'");
        }
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		String oid = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
			oid = id;
		}
		String hql = "";
		objectDao.executeHql("update T_coursetype t set t.f_deleted = 1 where t.id in (" + ids + ")");
		T_coursetype obj = objectDao.get(oid);
		if (obj != null) {
			objectDao.executeHql("update T_coursetype set f_leaf = " + this.checkLeaf(obj.getF_parentid()) + " where id = '" + obj.getF_parentid() + "'");
		}
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public T_coursetype getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		String id = objectMap.get("id").toString();
		if (id == null || "".equals(id)) {
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		T_coursetype obj = objectDao.get(id);
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		return obj;
	}
	
	/**
	 * 修改启用状态
	 * @param parameters
	 * @param employee
	 * @throws Exception
	 */
	public void updateEnabledObject(ParametersUtil parameters, A_Employee employee) throws Exception {
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> idList = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		String oid = "";
		for (String id : idList) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
			oid = id;
		}
		boolean isEnabled = (Boolean) objectMap.get("isenabled");
		if (isEnabled) {
			objectDao.executeHql("update T_coursetype set f_state = 'ztqy' where id in (" + ids + ")");
		} else {
			objectDao.executeHql("update T_coursetype set f_state = 'ztty' where id in (" + ids + ")");
		}
		T_coursetype obj = objectDao.get(oid);
		if (obj != null) {
			objectDao.executeHql("update T_coursetype set f_leaf = " + this.checkLeaf(obj.getF_parentid()) + " where id = '" + obj.getF_parentid() + "'");
		}
	}
}
