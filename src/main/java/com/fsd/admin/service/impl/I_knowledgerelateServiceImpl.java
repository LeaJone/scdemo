package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.fsd.admin.dao.I_knowledgeinfoDao;
import com.fsd.admin.dao.I_knowledgerelateDao;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.I_knowledgerelate;
import com.fsd.admin.service.I_knowledgerelateService;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.Config;
import com.fsd.core.util.ParametersUtil;
import com.google.gson.Gson;

@Repository("i_knowledgerelateServiceImpl")
public class I_knowledgerelateServiceImpl extends MainServiceImpl<I_knowledgerelate, String> implements I_knowledgerelateService{

	 private static final Logger log = Logger.getLogger(I_knowledgeinfoServiceImpl.class);
	    private String depict = "知识关联";
	    
	    @Resource(name = "i_knowledgerelateDaoImpl")
		public void setBaseDao(I_knowledgerelateDao I_knowledgerelateDao) {
			super.setBaseDao(I_knowledgerelateDao);
		}
		
		@Resource(name = "i_knowledgerelateDaoImpl")
		private I_knowledgerelateDao objectDao;
		@Resource(name = "i_knowledgeinfoDaoImpl")
		private I_knowledgeinfoDao infoDao;
	    
	    /**
		 * 加载分页数据
		 * @param param
		 * @return
		 * @throws Exception
		 */
		public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
			Criteria c = objectDao.createCriteria();
			if(param.getJsonData() != null && !"".equals(param.getJsonData())){
				Gson gs = new Gson();
				Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
				if(objectMap.get("fid") != null && !"".equals(objectMap.get("fid"))){
					c.add(Restrictions.eq("belongsid", objectMap.get("fid")));
				}else{
					c.add(Restrictions.eq("belongsid", "0"));
				}
			}
			return objectDao.findPager(param , c);
		}
		
		/**
		 * 加载不分页数据
		 * @return
		 * @throws Exception
		 */
		public List getObjectList(ParametersUtil param) throws Exception{
			List list = null;
			if(param.getJsonData() != null && !"".equals(param.getJsonData())){
				Gson gs = new Gson();
				Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
				String sql = "select i.title,g.id,g.type,g.belongsid,g.relateid from i_knowledgeinfo i,i_knowledgerelate g where i.id = g.relateid and 1=1";
				if(objectMap.get("fid") != null && !"".equals(objectMap.get("fid"))){
					sql += " and g.belongsid = '" + objectMap.get("fid") + "'";
				}else{
					sql += " and 0=2";
				}
				list = objectDao.queryBySql(sql);
			}
			return list;
		}
	    
	    /**
		 * 编辑对象
		 * @param obj
		 * @param employee
		 * @throws Exception
		 */
		public void save(ParametersUtil parameters, A_Employee employee) throws Exception {
			// 添加
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
			ArrayList<String> idList = (ArrayList<String>) objectMap.get("ids");
			for (String id : idList) {
				I_knowledgerelate knowledgerelate = new I_knowledgerelate();
				knowledgerelate.setId(this.getUUID());
				knowledgerelate.setType("关联");
				knowledgerelate.setRelateid(id);
				knowledgerelate.setBelongsid(objectMap.get("ssid").toString());
				objectDao.save(knowledgerelate);
				this.sysLogService.saveObject(Config.LOGTYPEBC,this.depict + knowledgerelate.getId(), employee, I_knowledgeinfoServiceImpl.class);
			} 
		}
	    
	    /**
		 * 删除对象
		 * @param parameters
		 * @throws Exception
		 */
		public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
			ArrayList<String> idList = (ArrayList<String>) objectMap.get("ids");
			for (String id : idList) {
				objectDao.delete(id);
				this.sysLogService.saveObject(Config.LOGTYPESC, this.depict + id, employee, I_knowledgeinfoServiceImpl.class);
			}
		}
	    
	    /**
		 * 根据ID加载对象
		 * @param parameters
		 * @return
		 * @throws Exception
		 */
		public I_knowledgerelate getObjectById(ParametersUtil parameters) throws Exception{
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
			if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
				throw new BusinessException(depict + "获取缺少ID参数!");
			}
			I_knowledgerelate obj = objectDao.get(objectMap.get("id").toString());
			if(obj == null){
				throw new BusinessException(depict + "数据不存在!");
			}
			return obj;
		}
}
