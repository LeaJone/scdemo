package com.fsd.admin.service.impl;

import com.fsd.admin.dao.J_replyDao;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.J_reply;
import com.fsd.admin.service.J_replyService;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.Config;
import com.fsd.core.util.ParametersUtil;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Map;

@Service("j_replyServiceImpl")
public class J_replyServiceImpl extends MainServiceImpl<J_reply, String> implements J_replyService {

    private String depict = "帖子回复";
    
    @Resource(name = "j_replyDaoImpl")
	public void setBaseDao(J_replyDao J_replyDao) {
		super.setBaseDao(J_replyDao);
	}
	
	@Resource(name = "j_replyDaoImpl")
	private J_replyDao objectDao;
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.addOrder(Order.desc("retime"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("fid") != null && !"".equals(objectMap.get("fid")) && !J_boardServiceImpl.FSDBOARD.equals(objectMap.get("fid"))){
				c.add(Restrictions.eq("postsid", objectMap.get("fid")));
			}
			if(objectMap.get("blackid") != null && !"".equals(objectMap.get("blackid"))){
				c.add(Restrictions.eq("reuserid", objectMap.get("blackid")));
			}
			if(objectMap.get("ptitle") != null && !"".equals(objectMap.get("ptitle"))){
				c.add(Restrictions.like("poststitle", "%" + objectMap.get("ptitle") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> idList = (ArrayList<String>) objectMap.get("ids");
		String ids = this.getSQLinByIDList(idList);
		objectDao.executeHql("update J_reply set deleted = '1' where id in (" + ids + ")");
		this.sysLogService.saveObject(Config.LOGTYPESC, this.depict + ids, employee, J_replyServiceImpl.class);
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public J_reply getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		J_reply obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		return obj;
	}

}
