package com.fsd.admin.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import org.springframework.stereotype.Repository;

import com.fsd.core.common.BusinessException;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.util.Config;
import com.fsd.core.util.ParametersUtil;
import com.fsd.core.util.SpringContextHolder;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.F_flowdealt;
import com.fsd.admin.model.F_flowobject;
import com.fsd.admin.model.F_flowrecord;
import com.fsd.admin.service.F_flowWorkService;
import com.fsd.admin.service.F_flowdealtService;
import com.fsd.admin.service.F_flowobjectService;
import com.fsd.admin.service.F_flowrecordService;
import com.fsd.admin.dao.F_flowobjectDao;
import com.fsd.admin.entity.A_LoginInfo;
import com.google.gson.Gson;

@Repository("f_flowobjectServiceImpl")
public class F_flowobjectServiceImpl extends MainServiceImpl<F_flowobject, String> implements F_flowobjectService{

	//流程对象状态  f_fodxzt
	public static final String folcz1 = "f_folcz";
	public static final String folcz2 = "流程中";
	public static final String fowc1 = "f_fowc";
	public static final String fowc2 = "完成";
	public static final String fozz1 = "f_fozz";
	public static final String fozz2 = "终止";
	
    private static final Logger log = Logger.getLogger(F_flowobjectServiceImpl.class);
    private String depict = "流程对象";
    
    @Resource(name = "f_flowobjectDaoImpl")
	public void setBaseDao(F_flowobjectDao f_flowobjectDao) {
		super.setBaseDao(f_flowobjectDao);
	}
	
	@Resource(name = "f_flowobjectDaoImpl")
	private F_flowobjectDao objectDao;

    @Resource(name = "f_flowrecordServiceImpl")
	private F_flowrecordService flowRecordService;
    @Resource(name = "f_flowdealtServiceImpl")
	private F_flowdealtService flowDealtService;
    
    /**
     * 分页查询条件
     * @param param
     * @param c
     */
    private void getQueryWhere(ParametersUtil param, Criteria c){
		c.addOrder(Order.desc("applydate"));
    	if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("employeename") != null && !"".equals(objectMap.get("employeename"))){
				c.add(Restrictions.like("employeename", "%" + objectMap.get("employeename") + "%"));
			}
		}
    }
	
	/**
	 * 加载分页数据，申请所有数据
	 * @param param
	 * @param loginInfo
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageListByManage(ParametersUtil param, A_LoginInfo loginInfo) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("companyid", loginInfo.getEmployee().getCompanyid()));
		this.getQueryWhere(param, c);
		return objectDao.findPager(param , c);
	}
	
	/**
	 * 加载分页数据，提取审批数据
	 * @param param
	 * @param loginInfo
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageListByExtract(ParametersUtil param, A_LoginInfo loginInfo) throws Exception{
		List<String> objIDList = this.flowRecordService.getFlowObjectIDListByExtract(loginInfo);
		if(objIDList.size() == 0){
			ParametersUtil util = new ParametersUtil();
			return util;
		}
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("companyid", loginInfo.getEmployee().getCompanyid()));
		c.add(Restrictions.in("id", objIDList));
		this.getQueryWhere(param, c);
		return objectDao.findPager(param , c);
	}

	/**
	 * 加载分页数据，释放审批数据
	 * @param param
	 * @param loginInfo
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageListByRelease(ParametersUtil param, A_LoginInfo loginInfo) throws Exception{
		List<String> objIDList = this.flowRecordService.getFlowObjectIDListByRelease(loginInfo);
		if(objIDList.size() == 0){
			ParametersUtil util = new ParametersUtil();
			return util;
		}
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("companyid", loginInfo.getEmployee().getCompanyid()));
		c.add(Restrictions.in("id", objIDList));
		this.getQueryWhere(param, c);
		return objectDao.findPager(param , c);
	}
	
	/**
	 * 加载分页数据，审批中数据
	 * @param param
	 * @param loginInfo
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageListByPending(ParametersUtil param, A_LoginInfo loginInfo) throws Exception{
		List<String> objIDList = this.flowRecordService.getFlowObjectIDListByPending(loginInfo);
		if(objIDList.size() == 0){
			ParametersUtil util = new ParametersUtil();
			return util;
		}
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("companyid", loginInfo.getEmployee().getCompanyid()));
		c.add(Restrictions.in("id", objIDList));
		this.getQueryWhere(param, c);
		return objectDao.findPager(param , c);
	}
	
	/**
	 * 加载分页数据，可撤回数据
	 * @param param
	 * @param loginInfo
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageListByRecall(ParametersUtil param, A_LoginInfo loginInfo) throws Exception{
		List<String> objIDList = this.flowRecordService.getFlowObjectIDListByRecall(loginInfo);
		if(objIDList.size() == 0){
			ParametersUtil util = new ParametersUtil();
			return util;
		}
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("companyid", loginInfo.getEmployee().getCompanyid()));
		c.add(Restrictions.in("id", objIDList));
		this.getQueryWhere(param, c);
		return objectDao.findPager(param , c);
	}
	
	/**
	 * 加载分页数据，处理过数据
	 * @param param
	 * @param loginInfo
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageListByPassed(ParametersUtil param, A_LoginInfo loginInfo) throws Exception{
		List<String> objIDList = this.flowRecordService.getFlowObjectIDListByPassed(loginInfo);
		if(objIDList.size() == 0){
			ParametersUtil util = new ParametersUtil();
			return util;
		}
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("companyid", loginInfo.getEmployee().getCompanyid()));
		c.add(Restrictions.in("id", objIDList));
		this.getQueryWhere(param, c);
		return objectDao.findPager(param , c);
	}
	
	/**
	 * 加载分页数据，本人提交申请
	 * @param param
	 * @param loginInfo
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageListBySelf(ParametersUtil param, A_LoginInfo loginInfo) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("companyid", loginInfo.getEmployee().getCompanyid()));
		c.add(Restrictions.eq("employeeid", loginInfo.getEmployee().getId()));
		this.getQueryWhere(param, c);
		return objectDao.findPager(param , c);
	}
	
	/**
	 * 根据数据对象ID加载流程对象，在数据保存时使用，判断流程对象是否存在
	 * @param objid
	 * @return
	 * @throws Exception
	 */
	public F_flowobject getObjectByDataObjId(String objid) throws Exception{
		String sql = "from F_flowobject t where dataobjectid = '" + objid + "'";
		List<F_flowobject> objList = objectDao.queryByHql(sql);
		if (objList != null && objList.size() > 0){
			return objList.get(0);
		}
		return null;
	}
	
	/**
	 * 获取数据对象，根据审批记录ID
	 * @param parameters
	 * @param loginInfo
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getDataObjectByRecordID(ParametersUtil parameters, A_LoginInfo loginInfo) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数！");
		}
		F_flowrecord record = flowRecordService.get(objectMap.get("id").toString());
		if(record == null){
			throw new BusinessException(depict + "流程记录数据不存在！");
		}
		if (!record.getDealtstatus().equals(F_flowrecordServiceImpl.frspz1)){
			throw new BusinessException(depict + "流程记录已不在审批中，无法进行审批操作！当前状态：" + record.getDealtstatusname());
		}
		boolean isTrue = true;
		F_flowdealt dealt = null;
		List<F_flowdealt> dealtList = flowDealtService.getObjectListByRecordID(record.getId());
		if(record.getNodetype().equals(F_flownodeServiceImpl.fnbmp)){
			for (F_flowdealt dealtObj : dealtList) {
				if (dealtObj.getValueinfo().equals(loginInfo.getEmployee().getBranchid())){
					if (this.checkPopedom(Config.POPEDOMgzljgsp, loginInfo.getPopedomMap().get(Config.POPEDOMMEUN))){
						isTrue = false;
						dealt = dealtObj;
						break;
					}
				}
			}
		}else if(record.getNodetype().equals(F_flownodeServiceImpl.fnzdp)){
			throw new BusinessException(depict + "，当前节点未自动审批，无法进行审批操作！");
		}else{
			for (F_flowdealt dealtObj : dealtList) {
				if (dealtObj.getValueinfo().equals(loginInfo.getEmployee().getId())){
					isTrue = false;
					dealt = dealtObj;
					break;
				}
			}
		}
		if (isTrue){
			throw new BusinessException(depict + "未匹配到审批权限，无法进行审批操作！");
		}
		if(dealt == null){
			throw new BusinessException(depict + "处理记录数据不存在！");
		}
		if (!dealt.getDealtresults().equals(F_flowdealtServiceImpl.fddcl1)){
			throw new BusinessException(depict + "处理记录已处理，无法进行审批操作！当前状态：" + dealt.getDealtresultsname());
		}
		
		ParametersUtil util = new ParametersUtil();
		util.addSendObject("flowrecord", record);
		util.addSendObject("flowdealt", dealt);
		return util;
	}
	
}
