package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import org.springframework.stereotype.Repository;

import com.fsd.core.common.BusinessException;
import com.fsd.core.util.Config;
import com.fsd.core.util.ParametersUtil;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.B_wordkeybad;
import com.fsd.admin.service.B_wordkeybadService;
import com.fsd.admin.dao.B_wordkeybadDao;
import com.google.gson.Gson;

@Repository("b_wordkeybadServiceImpl")
public class B_wordkeybadServiceImpl extends MainServiceImpl<B_wordkeybad, String> implements B_wordkeybadService{
    
    private static final Logger log = Logger.getLogger(B_wordkeybadServiceImpl.class);
    private String depict = "关键词";
    
    @Resource(name = "b_wordkeybadDaoImpl")
	public void setBaseDao(B_wordkeybadDao b_wordkeybadDao) {
		super.setBaseDao(b_wordkeybadDao);
	}
	
	@Resource(name = "b_wordkeybadDaoImpl")
	private B_wordkeybadDao objectDao;
	
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		c.addOrder(Order.asc("name"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("fid") != null && !"".equals(objectMap.get("fid"))){
				c.add(Restrictions.or(
						Restrictions.eq("wordtypeid", objectMap.get("fid")),
						Restrictions.eq("baseid", objectMap.get("fid"))
						));
			}
			if(objectMap.get("name") != null && !"".equals(objectMap.get("name"))){
				c.add(Restrictions.like("name", "%" + objectMap.get("name") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}
	
	/**
	 * 按照类型获取集合
	 * @param wordType
	 * @return
	 * @throws Exception
	 */
	public List<B_wordkeybad> getWordkeybadListByType(String wordType, A_Employee employee) throws Exception{
		String sql = "from B_wordkeybad where deleted = '0' and status = 'gjcqy' " +
				" and companyid = '" + employee.getCompanyid() + "' " +
				" and baseid = '" + wordType + "' ";
		return objectDao.queryByHql(sql);
	}
	
	/**
	 * 保存对象（添加、修改）
	 * @param obj
	 */
	public void saveObject(B_wordkeybad obj, A_Employee employee) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			B_wordkeybad obj_old = objectDao.get(obj.getId());
			obj_old.setWordtypeid(obj.getWordtypeid());
			obj_old.setWordtypename(obj.getWordtypename());
			obj_old.setName(obj.getName());
			obj_old.setSymbol(obj.getSymbol());
			obj_old.setRemark(obj.getRemark());
			obj_old.setStatus(obj.getStatus());
			obj_old.setStatusname(obj.getStatusname());
			obj_old.setUpdatedate(this.getData());//设置修改日期
			obj_old.setUpdateemployeeid(employee.getId());//设置修改用户id
			obj_old.setUpdateemployeename(employee.getRealname());//设置修改用户姓名
			objectDao.update(obj_old);
		}else{
			//添加
			obj.setId(this.getUUID());//设置主键
			obj.setCompanyid(employee.getCompanyid());//所属单位
			obj.setAdddate(this.getData());//设置添加日期
			obj.setAddemployeeid(employee.getId());//设置添加用户id
			obj.setAddemployeename(employee.getRealname());//设置添加用户姓名
			obj.setDeleted("0");//设置删除标志(0:正常 1：已删除)
			objectDao.save(obj);
		}
		this.sysLogService.saveObject(Config.LOGTYPEBC, this.depict + obj.getId() + obj.getName(), employee, B_wordkeybadServiceImpl.class);
	}
	
	/**
	 * 删除对象
	 * @param parameters
	 */
	@SuppressWarnings("unchecked")
	public void delObject(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		objectDao.executeHql("update B_wordkeybad t set t.deleted = '1' where t.id in (" + ids + ")");
	}
	
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public B_wordkeybad getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		B_wordkeybad obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		return obj;
	}
	
	/**
	 * 修改对象状态
	 * @param parameters
	 */
	@SuppressWarnings("unchecked")
	public void updateEnabledObject(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		boolean isEnabled = (Boolean) objectMap.get("isenabled");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		if (isEnabled)
			objectDao.executeHql("update B_wordkeybad t set t.status = 'gjcqy', t.statusname = '启用' where t.id in (" + ids + ")");
		else
			objectDao.executeHql("update B_wordkeybad t set t.status = 'gjcty', t.statusname = '停用' where t.id in (" + ids + ")");
	}
	
	/**
	 * 检查文本值输入过滤
	 * @throws BusinessException 
	 */
	public void checkTextByWordType(String wordType, String describe, String text, A_Employee employee) throws BusinessException{
		if (text == null || text.trim().length() == 0){
			return;
		}
		if (describe == null || describe.equals("")){
			describe = "内容";
		}
		String sql = "from B_wordkeybad where deleted = '0' and status = 'gjcqy' " +
				" and companyid = '" + employee.getCompanyid() + "' " +
				" and baseid = '" + wordType + "' ";
		List<B_wordkeybad> list = objectDao.queryByHql(sql);
		if (list != null && list.size() > 0){
			for (B_wordkeybad obj : list) {
				if (text.indexOf(obj.getName()) > 0){
					if (wordType.equals(Config.WORDTYPEBAD))
						throw new BusinessException(describe + "中包含敏感词信息，检测词是：" + obj.getName());
					else
						throw new BusinessException(describe + "中包含关键词信息，检测词是：" + obj.getName());
				}
			}
		}
	}
}
