package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import com.fsd.core.bean.Tree;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.Config;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.F_flowtype;
import com.fsd.admin.service.F_flowtypeService;
import com.fsd.admin.dao.F_flowtypeDao;

@Repository("f_flowtypeServiceImpl")
public class F_flowtypeServiceImpl extends MainServiceImpl<F_flowtype, String> implements F_flowtypeService{
    
    private static final Logger log = Logger.getLogger(F_flowtypeServiceImpl.class);
    private String depict = "流程类型";
    
    @Resource(name = "f_flowtypeDaoImpl")
	public void setBaseDao(F_flowtypeDao F_flowtypeDao) {
		super.setBaseDao(F_flowtypeDao);
	}
	
	@Resource(name = "f_flowtypeDaoImpl")
	private F_flowtypeDao objectDao;
	
	//流程项目状态 f_lcywlx
	/**
	 * 字典类型
	 */
	public static final String lcywzd1 = "f_lcywzd";
	public static final String lcywzd2 = "字典类型";
	/**
	 * 对象类型
	 */
	public static final String lcywdx1 = "f_lcywdx";
	public static final String lcywdx2 = "对象类型";
	/**
	 * 编码类型
	 */
	public static final String lcywbm1 = "f_lcywbm";
	public static final String lcywbm2 = "编码类型";
	
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
        c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		c.add(Restrictions.eq("deleted", "0"));
		c.addOrder(Order.asc("adddate"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("title") != null && !"".equals(objectMap.get("title"))){
				c.add(Restrictions.like("title", "%" + objectMap.get("title") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}
    
    /**
	 * 加载树数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<Tree> getObjectTreeList(String fid, A_Employee employee) throws Exception{
		List<Tree> treelist = new ArrayList<Tree>();
		List<F_flowtype> allObjectList = objectDao.queryByHql(
				"from F_flowtype t where t.companyid = '" + employee.getCompanyid() + "' and t.deleted = '0' order by t.sort asc");
		if (allObjectList != null && allObjectList.size() > 0){
			Tree tree = null;
			for (F_flowtype obj : allObjectList) {
				tree = new Tree();
				tree.setId(obj.getId());
				tree.setText(obj.getName());
				tree.setLeaf(true);
				tree.setObj(obj);
				treelist.add(tree);
			}
		}
		return treelist;
	}
    
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public void save(F_flowtype obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
            F_flowtype old_obj = objectDao.get(obj.getId());
            
            
            objectDao.update(old_obj);
        }else{
            //添加
            obj.setId(this.getUUID());//设置主键
			obj.setCompanyid(employee.getCompanyid());//所属单位
			objectDao.save(obj);
        }
        this.sysLogService.saveObject(Config.LOGTYPEBC, this.depict + obj.getId() + obj.getName(), employee, F_flowtypeServiceImpl.class);
	}
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = this.getSQLinByIDList(dir);
		objectDao.executeHql("update F_flowtype t set t.deleted = '1', t.updatedate = '" + this.getData() + 
				"', t.updateemployeeid = '" + employee.getId() + "', t.updateemployeename = '" + employee.getRealname() + 
				"' where t.id in (" + ids + ")");
		this.sysLogService.saveObject(Config.LOGTYPESC, this.depict + ids, employee, F_flowtypeServiceImpl.class);
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public F_flowtype getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		F_flowtype obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		return obj;
	}
}
