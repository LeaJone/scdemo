package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import org.springframework.stereotype.Repository;

import com.fsd.core.common.BusinessException;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.util.Config;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Sys_PopedomAllocate;
import com.fsd.admin.model.Sys_PopedomGroup;
import com.fsd.admin.service.Sys_PopedomAllocateService;
import com.fsd.admin.service.Sys_PopedomGroupService;
import com.fsd.admin.dao.Sys_PopedomAllocateDao;
import com.fsd.admin.dao.Sys_PopedomGroupDao;
import com.google.gson.Gson;

@Repository("Sys_PopedomAllocateServiceImpl")
public class Sys_PopedomAllocateServiceImpl extends MainServiceImpl<Sys_PopedomAllocate, String> implements Sys_PopedomAllocateService{
    
	private String depict = "权限分配";
	
    @Resource(name = "Sys_PopedomAllocateDaoImpl")
	public void setBaseDao(Sys_PopedomAllocateDao Sys_PopedomAllocateDao) {
		super.setBaseDao(Sys_PopedomAllocateDao);
	}
	
	@Resource(name = "Sys_PopedomAllocateDaoImpl")
	private Sys_PopedomAllocateDao objectDao;
	
	/**
	 * 通过sql查询所需集合
	 * @param sql
	 * @return
	 */
	public List<Sys_PopedomAllocate> getObjectBySql(String sql){
		return objectDao.queryByHql(sql);
	}

	/**
	 * 获取用户权限组
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public List<String> getUserPopedomGroupIds(A_Employee user) throws Exception{
		List<String> idList = new ArrayList<String>();
		List<Sys_PopedomAllocate> groupList = objectDao.queryByHql("from Sys_PopedomAllocate t where t.popedomtype = 'qxlxyh' and t.belongid = '" + user.getId() + "' ");
		for (Sys_PopedomAllocate obj : groupList) {
			if (idList.indexOf(obj.getPopedomid()) == -1){
				idList.add(obj.getPopedomid());
			}
		}
		return idList;
	}
	
	/**
	 * 获取用户权限集合
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public Map<String, List<String>> getUserPopedomMap(A_Employee user) throws Exception{
		Map<String, List<String>> popedomMap = new HashMap<String, List<String>>();
		popedomMap.put(Config.POPEDOMMEUN, new ArrayList<String>());
		popedomMap.put(Config.POPEDOMSUBJECT, new ArrayList<String>());
		popedomMap.put(Config.POPEDOMSUBJECTW, new ArrayList<String>());
		popedomMap.put(Config.POPEDOMBRANCH, new ArrayList<String>());
		if("rylbgly".equals(user.getType())){
			user.setIsadmin(true);
			return popedomMap;
		}
		List<String> groupList = this.getUserPopedomGroupIds(user);
		if (groupList.size() == 0)
			return popedomMap;
		String ids = "";
		for (String id : groupList) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		List<Sys_PopedomAllocate> allPopedomList = objectDao.queryByHql("from Sys_PopedomAllocate t where t.belongid in (" + ids + ")");
		for (Sys_PopedomAllocate obj : allPopedomList) {
			if (popedomMap.containsKey(obj.getPopedomtype())){
				popedomMap.get(obj.getPopedomtype()).add(obj.getPopedomid());
			}
		}
		return popedomMap;
	}
	
	/**
	 * 保存角色权限信息
	 * @param parameters
	 * @throws Exception
	 */
	public void saveRolePopedom(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少角色ID参数!");
		}
		if(objectMap.get("ids") == null){
			throw new BusinessException(depict + "获取缺少权限ID参数!");
		}
		if(objectMap.get("qxlx") == null){
			throw new BusinessException(depict + "获取缺少权限类型参数!");
		}
		//先清空当前角色的类型权限信息
		String qxlx = objectMap.get("qxlx").toString();
		objectDao.executeHql("delete Sys_PopedomAllocate t where t.popedomtype = '" + qxlx + 
				"' and t.belongid = '" + objectMap.get("id") + "'");
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String qxid = objectMap.get("id").toString();
		//循环将新权限保存进去
		for (String id : dir) {
			Sys_PopedomAllocate obj = new Sys_PopedomAllocate();
			obj.setId(this.getUUID());
			obj.setBelongid(qxid);//角色编号
			obj.setPopedomid(id);
			obj.setPopedomtype(qxlx);
			objectDao.save(obj);
		}
	}
	

	
	/**
	 * 保存人员权限信息
	 * @param parameters
	 * @throws Exception
	 */
	public void saveUserPopedom(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("userids") == null){
			throw new BusinessException(depict + "获取缺少用户ID参数!");
		}
		ArrayList<String> uidList = (ArrayList<String>) objectMap.get("userids");
		ArrayList<String> ridList = (ArrayList<String>) objectMap.get("roleids");
		String uids = "";
		for (String id : uidList) {
			if (!uids.equals("")){
				uids += ",";
			}
			uids += "'" + id + "'";
		}
		//先清空当前角色的权限信息
		objectDao.executeHql("delete Sys_PopedomAllocate t where t.popedomtype = '" + Config.POPEDOMUSER + "' and t.belongid in (" + uids + ")");
		//循环将新权限保存进去
		if (ridList == null || ridList.size() == 0)
			return;
		Sys_PopedomAllocate obj = null;
		for (String uid : uidList) {
			for (String rid : ridList) {
				obj = new Sys_PopedomAllocate();
				obj.setId(this.getUUID());
				obj.setBelongid(uid);//人员编号
				obj.setPopedomid(rid);//角色编号
				obj.setPopedomtype(Config.POPEDOMUSER);
				objectDao.save(obj);
			}
		}
	}

	/**
	 * 根据人员ID集合获取所属角色ID集合
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public List<String> getRoleIdsByUserIds(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("ids") == null){
			throw new BusinessException(depict + "获取缺少人员ID参数!");
		}
		ArrayList<String> idList = (ArrayList<String>) objectMap.get("ids");
		String sql = "";
		if(idList.size() == 1){
			sql = "t.belongid = '" + idList.get(0) + "'";
		}else{
			for (String id : idList) {
				if (!"".equals(sql)){
					sql += ",";
				}
				sql += "'" + id + "'";
			}
			sql = "t.belongid in (" + sql + ")";
		}
		idList.clear();
		List<Sys_PopedomAllocate> groupList = objectDao.queryByHql("from Sys_PopedomAllocate t where t.popedomtype = '" + Config.POPEDOMUSER + "' and " + sql);
		for (Sys_PopedomAllocate obj : groupList) {
			if (idList.indexOf(obj.getPopedomid()) == -1){
				idList.add(obj.getPopedomid());
			}
		}
		return idList;
	}

	/**
	 * 根据角色ID集合获取所属权限ID集合
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public Map<String, List<String>> getPopedomIdsByRoleIds(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("ids") == null){
			throw new BusinessException(depict + "获取缺少角色ID参数!");
		}
		ArrayList<String> idList = (ArrayList<String>) objectMap.get("ids");
		String sql = "";
		if(idList.size() == 1){
			sql = "t.belongid = '" + idList.get(0) + "'";
		}else{
			for (String id : idList) {
				if (!"".equals(sql)){
					sql += ",";
				}
				sql += "'" + id + "'";
			}
			sql = "t.belongid in (" + sql + ")";
		}
		Map<String, List<String>> popedomMap = new HashMap<String, List<String>>();
		popedomMap.put(Config.POPEDOMMEUN, new ArrayList<String>());
		popedomMap.put(Config.POPEDOMSUBJECT, new ArrayList<String>());
		popedomMap.put(Config.POPEDOMSUBJECTW, new ArrayList<String>());
		popedomMap.put(Config.POPEDOMBRANCH, new ArrayList<String>());
		List<Sys_PopedomAllocate> groupList = objectDao.queryByHql("from Sys_PopedomAllocate t where 1 = 1 and " + sql);
		
		for (Sys_PopedomAllocate obj : groupList) {
			if (popedomMap.containsKey(obj.getPopedomtype())){
				if (popedomMap.get(obj.getPopedomtype()).indexOf(obj.getPopedomid()) == -1){
					popedomMap.get(obj.getPopedomtype()).add(obj.getPopedomid());
				}
			}
		}
		return popedomMap;
	}

	/**
	 * 根据权限ID加载权限管理信息
	 * @param popedomId
	 * @return
	 * @throws Exception
	 */
	@Override
	public Sys_PopedomAllocate getObjectByPopedomIdAndType(String popedomId, String type) throws Exception{
		String sql = "select * from Sys_PopedomAllocate where popedomid = ? and popedomtype = ?";
		List<Sys_PopedomAllocate> list = objectDao.queryBySql1(sql, popedomId, type);
		if (list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}
}
