package com.fsd.admin.service.impl;

import com.fsd.admin.dao.A_EmployeeDao;
import com.fsd.admin.dao.Sys_PopedomAllocateDao;
import com.fsd.admin.dao.Z_qyrzDao;
import com.fsd.admin.dao.Z_qyrzjlDao;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Sys_PopedomAllocate;
import com.fsd.admin.model.Z_qyrz;
import com.fsd.admin.model.Z_qyrzjl;
import com.fsd.admin.service.Z_qyrzService;
import com.fsd.core.common.BusinessException;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.util.IdcardValidator;
import com.fsd.core.util.Mail;
import com.fsd.core.util.ParametersUtil;
import com.google.gson.Gson;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Repository("z_qyrzServiceImpl")
public class Z_qyrzServiceImpl extends BaseServiceImpl<Z_qyrz, String> implements Z_qyrzService {
    
    private static final Logger log = Logger.getLogger(Z_qyrzServiceImpl.class);
    private String depict = "";
    
    @Resource(name = "z_qyrzDaoImpl")
	public void setBaseDao(Z_qyrzDao Z_qyrzDao) {
		super.setBaseDao(Z_qyrzDao);
	}
	
	@Resource(name = "z_qyrzDaoImpl")
	private Z_qyrzDao objectDao;

	@Resource(name = "A_EmployeeDaoImpl")
	private A_EmployeeDao employeeDao;

	@Resource(name = "Sys_PopedomAllocateDaoImpl")
	private Sys_PopedomAllocateDao popedomAllocateDao;

	@Resource(name = "z_qyrzjlDaoImpl")
	private Z_qyrzjlDao qyrzjlDao;
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
    @Override
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.addOrder(Order.desc("subtime"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("qymc") != null && !"".equals(objectMap.get("qymc"))){
				c.add(Restrictions.like("qymc", "%" + objectMap.get("qymc") + "%"));
			}
			if(employee.getType().equals("rylbptyh")){
				c.add(Restrictions.eq("rowid", employee.getLoginname()));
			}
		}
		return objectDao.findPager(param , c);
	}
    
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
    @Override
	public void save(Z_qyrz obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
			Z_qyrz old_obj = objectDao.get(obj.getId());
			old_obj.setQymc(obj.getQymc());
			old_obj.setQyxz_code(obj.getQyxz_code());
			old_obj.setQyxz_name(obj.getQyxz_name());
			old_obj.setQyyx(obj.getQyyx());
			old_obj.setFrdb(obj.getFrdb());
			try{
				IdcardValidator.IDCardValidate2(obj.getFrsfzh().toLowerCase());
				old_obj.setFrsfzh(obj.getFrsfzh());
			}catch(Exception ex){
				throw new BusinessException("你的身份证号不正确，请修改后重新提交！");
			}
			old_obj.setFrlxdh(obj.getFrlxdh());
			old_obj.setJyfw(obj.getJyfw());
			old_obj.setZycp(obj.getZycp());
			old_obj.setZcsj(obj.getZcsj());
			old_obj.setZcdd(obj.getZcdd());
			old_obj.setZczb(obj.getZczb());
			old_obj.setYyzz(obj.getYyzz());
			old_obj.setSubtime(this.getData());
			old_obj.setStatus("0");
            objectDao.update(old_obj);
			Z_qyrzjl qyrzjl = new Z_qyrzjl();
			qyrzjl.setId(this.getUUID());
			qyrzjl.setQyid(obj.getId());
			qyrzjl.setQymc(obj.getQymc());
			qyrzjl.setTime(this.getData());
			qyrzjl.setType("信息变更");
			qyrzjl.setRemark("企业信息变更");
			qyrzjlDao.save(qyrzjl);
        }
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
    @Override
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		Z_qyrz obj = objectDao.get(objectMap.get("id").toString());
		Criteria c = employeeDao.createCriteria();
		c.add(Restrictions.eq("loginname", obj.getRowid()));
		List<A_Employee> list = c.list();
		if(list.size() != 0){
			employeeDao.delete(list.get(0));
		}
		obj.setDeleted("1");
		objectDao.update(obj);
		Z_qyrzjl qyrzjl = new Z_qyrzjl();
		qyrzjl.setId(this.getUUID());
		qyrzjl.setQyid(obj.getId());
		qyrzjl.setQymc(obj.getQymc());
		qyrzjl.setTime(this.getData());
		qyrzjl.setType("企业毕业");
		qyrzjl.setRemark("企业正式毕业");
		qyrzjlDao.save(qyrzjl);
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
    @Override
	public Z_qyrz getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		Z_qyrz obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null) {
			throw new BusinessException(depict + "数据不存在!");
		}
		return obj;
	}

	/**
	 * 审核对象
	 * @param parameters
	 */
	@Override
	public void updateAuditObject(ParametersUtil parameters, A_Employee employee) throws Exception{
        Gson gs = new Gson();
        Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
        String id = objectMap.get("id").toString();
        String type = objectMap.get("type").toString();
        String reason = objectMap.get("reason").toString() ;
        Z_qyrz obj = objectDao.get(id);
        if(obj.getStatus().equals("0")){
            if(type.equals("1")) {
				if (obj.getRowid() != null && !obj.getRowid().equals("")) {
					obj.setStatus("1");
					objectDao.update(obj);
					reason = "你好！<br> 你修改的企业信息审核已通过！<br> 门户网站：http://www.lzit.edu.cn/";
					Mail.send("smtp.139.com", "lumingbao@139.com", obj.getQyyx(), "审核结果通知", reason, "lumingbao@139.com", "lumingbao0228");
				} else {
					obj.setStatus("1");
					obj.setRowid(this.getData());
					objectDao.update(obj);
					A_Employee emp = new A_Employee();
					String empId = this.getUUID();
					emp.setId(empId);
					emp.setCompanyid("FSDCOMPANY");
					emp.setCompanyname("兰州工业学院");
					emp.setLoginname(obj.getRowid());
					emp.setCode(obj.getRowid());
					emp.setPassword("3980e4044675f6339248ee0c735c7d72");
					emp.setRealname(obj.getQymc());
					emp.setSymbol(obj.getId());
					emp.setUsertypeid("qy");
					emp.setUsertypename("企业");
					emp.setEmail(obj.getQyyx());
					emp.setMobile(obj.getFrlxdh());
					emp.setIdcard(obj.getFrsfzh());
					emp.setType("rylbptyh");
					emp.setTypename("普通用户");
					emp.setStatus("ryztqy");
					emp.setStatusname("启用");
					emp.setDeleted("0");
					employeeDao.save(emp);
					Sys_PopedomAllocate popedomAllocate = new Sys_PopedomAllocate();
					popedomAllocate.setId(this.getUUID());
					popedomAllocate.setPopedomid("e4b5451c77e74ab7ae873a52f6c05ccf");
					popedomAllocate.setPopedomtype("qxlxyh");
					popedomAllocate.setBelongid(empId);
					popedomAllocateDao.save(popedomAllocate);
					Z_qyrzjl qyrzjl = new Z_qyrzjl();
					qyrzjl.setId(this.getUUID());
					qyrzjl.setQyid(obj.getId());
					qyrzjl.setQymc(obj.getQymc());
					qyrzjl.setTime(this.getData());
					qyrzjl.setType("企业入驻");
					qyrzjl.setRemark("企业正式入驻");
					qyrzjlDao.save(qyrzjl);
					reason = "你好！<br> 感谢你入驻兰州工业学院！<br> 你的登陆账号为：" + obj.getRowid() + "，登陆密码为：123456。请点击以下链接登录到管理平台：<br> http://127.0.0.1/admin （该链接在入驻期间有效）";
					Mail.send("smtp.139.com", "lumingbao@139.com", obj.getQyyx(), "审核结果通知", reason, "lumingbao@139.com", "lumingbao0228");
				}
			}else if(type.equals("-1")){
                obj.setStatus("-1");
                objectDao.update(obj);
				Z_qyrzjl qyrzjl = new Z_qyrzjl();
				qyrzjl.setId(this.getUUID());
				qyrzjl.setQyid(obj.getId());
				qyrzjl.setQymc(obj.getQymc());
				qyrzjl.setTime(this.getData());
				qyrzjl.setType("审核驳回");
				qyrzjl.setRemark("原因："+reason);
				qyrzjlDao.save(qyrzjl);
				reason = "你好！<br> 你的入驻申请已被驳回！<br> 原因：" + reason + "<br> 门户网站：http://www.lzit.edu.cn/";
                Mail.send("smtp.139.com", "lumingbao@139.com", obj.getQyyx(), "审核结果通知", reason, "lumingbao@139.com", "lumingbao0228");
            }
        } else {
            throw new BusinessException("该信息已处理，请勿重复审核！");
        }
	}
}
