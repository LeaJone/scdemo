package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;

import com.fsd.admin.model.Z_project;
import com.fsd.admin.service.Z_projectService;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_projectending;
import com.fsd.admin.service.Z_projectendingService;
import com.fsd.admin.dao.Z_projectendingDao;

@Repository("z_projectendingServiceImpl")
public class Z_projectendingServiceImpl extends BaseServiceImpl<Z_projectending, String> implements Z_projectendingService{
    
    private static final Logger log = Logger.getLogger(Z_projectendingServiceImpl.class);
    private String depict = "";
    
    @Resource(name = "z_projectendingDaoImpl")
	public void setBaseDao(Z_projectendingDao Z_projectendingDao) {
		super.setBaseDao(Z_projectendingDao);
	}
	
	@Resource(name = "z_projectendingDaoImpl")
	private Z_projectendingDao objectDao;

	@Autowired
	private Z_projectService projectService;
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
    @Override
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
        c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		c.add(Restrictions.eq("deleted", "0"));
		c.addOrder(Order.asc("adddate"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("title") != null && !"".equals(objectMap.get("title"))){
				c.add(Restrictions.like("title", "%" + objectMap.get("title") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}
    
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
    @Override
	public void save(Z_projectending obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
            Z_projectending old_obj = objectDao.get(obj.getId());
			old_obj.setF_executeid(obj.getF_executeid());
			old_obj.setF_executename(obj.getF_executename());
			old_obj.setF_background(obj.getF_background());
			old_obj.setF_progressachievement(obj.getF_progressachievement());
			old_obj.setF_achievement(obj.getF_achievement());
			old_obj.setF_moneyuse(obj.getF_moneyuse());
            old_obj.setF_lastupdatedate(this.getData());//设置修改日期
			old_obj.setF_updateemployeeid(employee.getId());//设置修改用户id
			old_obj.setF_updateemployeename(employee.getRealname());//设置修改用户姓名
            objectDao.update(old_obj);
        }else{
            //添加
            obj.setId(this.getUUID());//设置主键
			obj.setF_adddate(this.getData());//设置添加日期
			obj.setF_addemployeeid(employee.getId());//设置添加用户id
			obj.setF_addemployeename(employee.getRealname());//设置添加用户姓名
			obj.setF_deleted("0");//设置删除标志(0:正常 1：已删除)
			objectDao.save(obj);
        }
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
    @Override
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		for (String id : dir) {
			objectDao.delete(id);
		}
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
    @Override
	public Z_projectending getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		Z_projectending obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		
		return obj;
	}

	/**
	 * 根据项目ID加载项目结题信息
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public Z_projectending saveOrGetProjectEndingByProjectId(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("projectid") == null && "".equals(objectMap.get("projectid"))){
			throw new BusinessException(depict + "缺少项目ID!");
		}
		String projectid = objectMap.get("projectid").toString();
		String sql = "select * from Z_projectending where f_projectId = ? and f_deleted = ?";
		List<Z_projectending> list = objectDao.queryBySql1(sql, projectid, "0");
		if(list != null && list.size() > 0){
			return list.get(0);
		}else{
			Z_project project = projectService.get(projectid);
			if(project == null){
				throw new BusinessException(depict + "项目信息错误，请联系管理员!");
			}
			Z_projectending projectending = new Z_projectending();
			projectending.setId(this.getUUID());
			projectending.setF_projectid(projectid);
			projectending.setF_projectname(project.getF_name());
			projectending.setF_background("二、立项背景（研究现状、趋势、研究意义等，400字左右）");
			projectending.setF_progressachievement("四、项目实施的进展情况及初步取得的成果");
			projectending.setF_achievement("六、项目结题成果简介：包括发表论文（应注明论文题目、发表刊物、发表时间、作者、是否被SCI、EI收录等详细信息）、专利（专利申请及获批专利名称、专利号、申请人、获得日期等信息）、实物、软件、图纸、获奖证书、商业计划书、公司运营报告、营业执照等。");
			projectending.setF_moneyuse("七、经费使用情况：");
			projectending.setF_deleted("0");
			objectDao.save(projectending);
			project.setF_isending("true");
			projectService.update(project);
			return projectending;
		}
	}

	/**
	 * 根据项目ID加载结项信息
	 * @param projectid
	 * @return
	 * @throws Exception
	 */
	public Z_projectending getObjectByProjectId(String projectid) throws Exception{
		String sql = "select * from Z_projectending where f_projectId = ? and f_deleted = ?";
		List<Z_projectending> list = objectDao.queryBySql1(sql, projectid, "0");
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}
}
