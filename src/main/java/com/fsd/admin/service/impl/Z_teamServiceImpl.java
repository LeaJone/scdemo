package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_team;
import com.fsd.admin.service.A_EmployeeService;
import com.fsd.admin.service.Z_teamService;
import com.fsd.admin.dao.Z_teamDao;

@Service("z_teamServiceImpl")
public class Z_teamServiceImpl extends BaseServiceImpl<Z_team, String> implements Z_teamService{
    
    private static final Logger log = Logger.getLogger(Z_teamServiceImpl.class);
    private String depict = "";
    
    @Resource(name = "z_teamDaoImpl")
	public void setBaseDao(Z_teamDao Z_teamDao) {
		super.setBaseDao(Z_teamDao);
	}
	
	@Resource(name = "z_teamDaoImpl")
	private Z_teamDao objectDao;
	
	@Resource(name = "A_EmployeeServiceImpl")
	private A_EmployeeService userService;
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("f_deltet", "0"));
		if(!employee.isIsadmin()){
			c.add(Restrictions.eq("f_creatorid", employee.getId()));
		}
		c.addOrder(Order.desc("f_adddate"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("name") != null && !"".equals(objectMap.get("name"))){
				c.add(Restrictions.like("f_name", "%" + objectMap.get("name") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}
    
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public void save(Z_team obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
            Z_team old_obj = objectDao.get(obj.getId());
            old_obj.setF_name(obj.getF_name());
			old_obj.setF_leadername(obj.getF_leadername());
			old_obj.setF_photo1(obj.getF_photo1());
			old_obj.setF_photo2(obj.getF_photo2());
            old_obj.setF_abstract(obj.getF_abstract());
			old_obj.setF_member(obj.getF_member());
			old_obj.setF_projects(obj.getF_projects());
            objectDao.update(old_obj);
        }else{
            //添加
        	String id = this.getUUID();
            obj.setId(id);
            obj.setF_adddate(this.getData());
            obj.setF_creatorid(employee.getId());
            obj.setF_creatorname(employee.getRealname());
            obj.setF_deltet("0");
			objectDao.save(obj);
        }
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		objectDao.executeHql("update Z_team t set t.f_deltet = '1' where t.id in (" + ids + ")");
		objectDao.executeHql("update Z_teamanduser z set z.f_delete = '1' where z.f_teamid in (" + ids + ")");
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public Z_team getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		Z_team obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		
		return obj;
	}
	
	/**
	 * 加载列表数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<Z_team> getTeamList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("f_deltet", "0"));
		c.addOrder(Order.desc("f_adddate"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("txt") != null && !"".equals(objectMap.get("txt"))){
				c.add(Restrictions.like("f_name", "%" + objectMap.get("txt") + "%"));
			}
		}
		return objectDao.getList(c);
	}
}
