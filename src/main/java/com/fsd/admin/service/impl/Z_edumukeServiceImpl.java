package com.fsd.admin.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.fsd.admin.dao.Z_edumukeDao;
import com.fsd.admin.dao.Z_eduprofessionDao;
import com.fsd.admin.dao.Z_eduprojectDao;
import com.fsd.admin.dao.Z_edupromoteDao;
import com.fsd.admin.dao.Z_eduteacherDao;
import com.fsd.admin.dao.Z_eduteamDao;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_edumuke;
import com.fsd.admin.model.Z_eduprofession;
import com.fsd.admin.model.Z_eduproject;
import com.fsd.admin.model.Z_edupromote;
import com.fsd.admin.model.Z_eduteacher;
import com.fsd.admin.model.Z_eduteam;
import com.fsd.admin.service.Z_edumukeService;
import com.fsd.core.common.BusinessException;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.util.DateTimeUtil;
import com.fsd.core.util.ParametersUtil;
import com.fsd.core.util.PdfUtil;
import com.google.gson.Gson;


@Repository("z_edumukeServiceImpl")
public class Z_edumukeServiceImpl extends BaseServiceImpl<Z_edumuke, String> implements Z_edumukeService{

	private static final Logger log = Logger.getLogger(Z_edumukeServiceImpl.class);
	private String depict = "";

	@Resource(name = "z_edumukeDaoImpl")
	public void setBaseDao(Z_edumukeDao Z_edumukeDao) {
		super.setBaseDao(Z_edumukeDao);
	}

	@Resource(name = "z_edumukeDaoImpl")
	private Z_edumukeDao objectDao;

	@Resource(name = "z_eduprofessionDaoImpl")
	private Z_eduprofessionDao eduprofessionDao;

	@Resource(name = "z_eduprojectDaoImpl")
	private Z_eduprojectDao eduprojectDao;

	@Resource(name = "z_edupromoteDaoImpl")
	private Z_edupromoteDao edupromoteDao;

	@Resource(name = "z_eduteacherDaoImpl")
	private Z_eduteacherDao eduteacherDao;

	@Resource(name = "z_eduteamDaoImpl")
	private Z_eduteamDao eduteamDao;

	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@Override
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("f_addemployeeid", employee.getId()));
		c.add(Restrictions.eq("f_deleted", "0"));
		c.addOrder(Order.asc("f_adddate"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("name") != null && !"".equals(objectMap.get("name"))){
				c.add(Restrictions.like("f_name", "%" + objectMap.get("name") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}

	/**
	 * 修改 / 录入课程
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	@Override
	public void save(String rootpath, Z_edumuke obj, A_Employee employee) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			File file = new File("uploadfiles/projectfile/" + obj.getId() + ".doc");//删除PDF文件
			if(file.exists()) {
				file.delete();
			}
			//修改
			Z_edumuke old_obj = objectDao.get(obj.getId());
			old_obj.setF_name(obj.getF_name());
			old_obj.setF_principalname(obj.getF_principalname());
			old_obj.setF_typename(obj.getF_typename());
			old_obj.setF_typeid(obj.getF_typeid());
			old_obj.setF_teamname(obj.getF_teamname());
			old_obj.setF_period(obj.getF_period());
			old_obj.setF_score(obj.getF_score());
			old_obj.setF_money(obj.getF_money());
			old_obj.setF_basename(obj.getF_basename());
			old_obj.setF_teamid(obj.getF_teamid());

			old_obj.setF_lastupdatedate(this.getData());//设置修改日期
			old_obj.setF_updateemployeeid(employee.getId());//设置修改用户id
			old_obj.setF_updateemployeename(employee.getRealname());//设置修改用户姓名
			doPdf(rootpath,old_obj);//生成修改后的PDF文件
			objectDao.update(old_obj);
		}else{
			//添加
			obj.setId(this.getUUID());//设置主键
			obj.setF_addemployeeid(employee.getId());
			obj.setF_adddate(this.getData());//设置添加日期
			obj.setF_addemployeeid(employee.getId());//设置添加用户id
			obj.setF_addemployeename(employee.getRealname());//设置添加用户姓名
			obj.setF_deleted("0");//设置删除标志(0:正常 1：已删除)

			doPdf(rootpath,obj);//生成PDF文件

			objectDao.save(obj);
		}
	}

	/**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	@Override
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		String hql = "";
		objectDao.executeHql("update Z_edumuke t set t.f_deleted = '1', t.f_lastupdatedate = '" + this.getData() + 
				"', t.f_updateemployeeid = '" + employee.getId() + "', t.f_updateemployeename = '" + employee.getRealname() + 
				"' where t.id in (" + ids + ")");
	}

	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	@Override
	public Z_edumuke getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		Z_edumuke obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}

		return obj;
	}
	private void doPdf(String rootpath,Z_edumuke obj) throws Exception {

		String pdfurl = "uploadfiles/projectfile/" + obj.getId() + ".doc";
		obj.setF_pdfurl(pdfurl);
		//生成PDF文件
		String templatePath = rootpath + "uploadfiles/pdftemplate/edu_muke.doc";
		String outPath = rootpath + pdfurl;
		Map<String, Object> data = new HashMap<>();
		data.put("m_name",obj.getF_name());
		data.put("m_date",DateTimeUtil.formatDate(obj.getF_adddate(), "yyyyMMddHHmmss", "yyyy年MM月dd日"));
		PdfUtil.createWordByTemplate(data, templatePath, outPath);
	}

	@Override
	public void uploadFile(Z_edumuke obj, A_Employee loginUser) {
		Z_edumuke old_obj = objectDao.get(obj.getId());
		old_obj.setF_pdfurl(obj.getF_pdfurl());
		objectDao.save(old_obj);
	}

	@Override
	public void downExcel(A_Employee loginUser) {
		List<Z_eduproject> userList = eduprojectDao.queryByHql("from Z_eduproject");
		List userList2 = objectDao.queryByHql("from Z_edumuke");
		System.out.println(userList2);
		List userList3 = eduteacherDao.queryByHql("from Z_eduteacher");
		List userList4 = eduteamDao.queryByHql("from Z_eduteam");
		List userList5 = eduprofessionDao.queryByHql("from Z_eduprofession");
		List userList6 = edupromoteDao.queryByHql("from Z_edupromote");
		try  
		{  
			FileOutputStream fout = new FileOutputStream("c:/Users/Administrator/Desktop/report.xls");
			getValue(userList,userList2,userList3,userList4,userList5,userList6, fout);
			fout.close();  
		}  
		catch (Exception e)  
		{  
			e.printStackTrace();  
		} 
	}

	@Override
	public List<Z_edumuke> selectAll(String start, String end) {
		return objectDao.queryBySql1("select * from Z_edumuke where f_adddate > ? and f_adddate < ? and f_deleted = ?", start, end, "0");
	}

	public void getValue(List<Z_eduproject> userList,List<Z_edumuke> userList2,List<Z_eduteacher> userList3,List<Z_eduteam> userList4,List<Z_eduprofession> userList5,List<Z_edupromote> userList6,FileOutputStream fout){
		try{
			//1.创建工作簿
			HSSFWorkbook workbook = new HSSFWorkbook();
			//1.1创建合并单元格对象
			CellRangeAddress callRangeAddress = new CellRangeAddress(0,0,0,7);//起始行,结束行,起始列,结束列
			CellRangeAddress callRangeAddress1 = new CellRangeAddress(1,1,0,7);//起始行,结束行,起始列,结束列
			//班组与时间start
			CellRangeAddress callRangeAddress20 = new CellRangeAddress(2,2,0,2);//起始行,结束行,起始列,结束列
			CellRangeAddress callRangeAddress21 = new CellRangeAddress(2,2,3,4);//起始行,结束行,起始列,结束列
			CellRangeAddress callRangeAddress22 = new CellRangeAddress(2,2,5,7);//起始行,结束行,起始列,结束列
			//联系方式一行显示
			CellRangeAddress callRangeAddress31 = new CellRangeAddress(3,3,0,7);//起始行,结束行,起始列,结束列
			//标题
			CellRangeAddress callRangeAddress41 = new CellRangeAddress(4,4,0,0);//起始行,结束行,起始列,结束列
			CellRangeAddress callRangeAddress42 = new CellRangeAddress(4,4,1,1);//起始行,结束行,起始列,结束列
			CellRangeAddress callRangeAddress43 = new CellRangeAddress(4,4,2,3);//起始行,结束行,起始列,结束列
			CellRangeAddress callRangeAddress44 = new CellRangeAddress(4,4,4,4);//起始行,结束行,起始列,结束列
			CellRangeAddress callRangeAddress45 = new CellRangeAddress(4,4,5,5);//起始行,结束行,起始列,结束列
			CellRangeAddress callRangeAddress46 = new CellRangeAddress(4,4,6,6);//起始行,结束行,起始列,结束列
			CellRangeAddress callRangeAddress47 = new CellRangeAddress(4,4,7,7);//起始行,结束行,起始列,结束列
			//项目
			CellRangeAddress callRangeAddressType1 = new CellRangeAddress(5,userList.size()+4,1,1);//起始行,结束行,起始列,结束列
			CellRangeAddress callRangeAddressType2 = new CellRangeAddress(userList.size()+5,userList.size()+userList2.size()+4,1,1);//起始行,结束行,起始列,结束列
			CellRangeAddress callRangeAddressType3 = new CellRangeAddress(userList.size()+userList2.size()+5,userList.size()+userList2.size()+userList3.size()+4,1,1);//起始行,结束行,起始列,结束列
			CellRangeAddress callRangeAddressType4 = new CellRangeAddress(userList.size()+userList2.size()+userList3.size()+5,userList.size()+userList2.size()+userList3.size()+userList4.size()+4,1,1);//起始行,结束行,起始列,结束列
			CellRangeAddress callRangeAddressType5 = new CellRangeAddress(userList.size()+userList2.size()+userList3.size()+userList4.size()+5,userList.size()+userList2.size()+userList3.size()+userList4.size()+userList5.size()+4,1,1);//起始行,结束行,起始列,结束列
			CellRangeAddress callRangeAddressType6 = new CellRangeAddress(userList.size()+userList2.size()+userList3.size()+userList4.size()+userList5.size()+5,userList.size()+userList2.size()+userList3.size()+userList4.size()+userList5.size()+userList6.size()+4,1,1);//起始行,结束行,起始列,结束列

			//附件字样字体
			HSSFCellStyle headStyle = createCellStyle(workbook,(short)14,false,false,true,"仿宋",false);
			//标题字体样式
			HSSFCellStyle erStyle = createCellStyle(workbook,(short)20,false,true,false,"方正小标宋简体",false);
			//推荐栏字体样式
			HSSFCellStyle sanStyle1 = createCellStyle(workbook,(short)12,false,true,false,"宋体",false);

			HSSFCellStyle sanStyle = createCellStyle(workbook,(short)12,false,true,false,"宋体",true);
			//联系人
			HSSFCellStyle colStyle = createCellStyle(workbook,(short)14,false,false,true,"仿宋",false);
			//内容样式
			HSSFCellStyle cellStyle = createCellStyle(workbook,(short)12,false,true,false,"黑体",true);
			//2.创建工作表
			HSSFSheet sheet = workbook.createSheet("教改项目");
			//2.1加载合并单元格对象
			sheet.addMergedRegion(callRangeAddress);
			sheet.addMergedRegion(callRangeAddress1);
			sheet.addMergedRegion(callRangeAddress20);
			sheet.addMergedRegion(callRangeAddress21);
			sheet.addMergedRegion(callRangeAddress22);
			sheet.addMergedRegion(callRangeAddress31);
			sheet.addMergedRegion(callRangeAddress41);
			sheet.addMergedRegion(callRangeAddress42);
			sheet.addMergedRegion(callRangeAddress43);
			sheet.addMergedRegion(callRangeAddress44);
			sheet.addMergedRegion(callRangeAddress45);
			sheet.addMergedRegion(callRangeAddress46);
			sheet.addMergedRegion(callRangeAddress47);
			sheet.addMergedRegion(callRangeAddressType1);
			sheet.addMergedRegion(callRangeAddressType2);
			sheet.addMergedRegion(callRangeAddressType3);
			sheet.addMergedRegion(callRangeAddressType4);
			sheet.addMergedRegion(callRangeAddressType5);
			sheet.addMergedRegion(callRangeAddressType6);
			//设置默认列宽高
			sheet.setDefaultRowHeight((short)800);
			sheet.setColumnWidth(0, 7*256);
			sheet.setColumnWidth(1, 17*256);
			sheet.setColumnWidth(2, 6*256);
			sheet.setColumnWidth(3, 40*256);
			sheet.setColumnWidth(4, 13*256);
			sheet.setColumnWidth(5, 18*256);
			sheet.setColumnWidth(6, 16*256);
			sheet.setColumnWidth(7, 17*256);
			//3.创建行
			//3.1创建头标题行;并且设置头标题
			HSSFRow row = sheet.createRow(0);
			HSSFCell cell = row.createCell(0);
			//表头区域
			cell.setCellStyle(headStyle);
			cell.setCellValue("附件1：");
			//表名区域
			HSSFRow rower = sheet.createRow(1);
			HSSFCell celler = rower.createCell(0);
			celler.setCellStyle(erStyle);
			celler.setCellValue("2019年高等学校创新创业教育改革项目推荐汇总表");
			//时间盖章区域
			HSSFRow rowsan = sheet.createRow(2);
			HSSFCell cellsan = rowsan.createCell(0);
			HSSFCell cellsan1 = rowsan.createCell(3);
			HSSFCell cellsan2 = rowsan.createCell(5);
			cellsan.setCellStyle(sanStyle1);
			cellsan.setCellValue("推荐学校（盖章）：");
			cellsan1.setCellStyle(sanStyle1);
			cellsan1.setCellValue("");
			cellsan2.setCellStyle(sanStyle1);
			cellsan2.setCellValue("填报日期：     年    月    日");
			//联系方式区域
			HSSFRow rowphone = sheet.createRow(3);
			HSSFCell cellphone = rowphone.createCell(0);
			cellphone.setCellStyle(colStyle);
			cellphone.setCellValue("联系人：         办公电话：            手机：                e-mail:");
			//标题区域
			HSSFRow row2 = sheet.createRow(4);
			String[] titles = {"序号","项目类型","项目名称","","项目主持人","项目参与人","所属学校","网评网址"};//""为占位字符串
			for(int i=0;i<titles.length;i++)
			{
				HSSFCell cell2 = row2.createCell(i);
				//加载单元格样式
				cell2.setCellStyle(cellStyle);
				if(i==0) {cell2.setCellStyle(sanStyle);}
				cell2.setCellValue(titles[i]);
			}
			//项目部分1
			if(userList != null){
				int i=1;
				for(int j=0;j<userList.size();j++)
				{
					//创建数据行,前面有两行,头标题行和列标题行
					HSSFRow row3 = sheet.createRow(j+5);
					HSSFCell cell0 = row3.createCell(0);
					cell0.setCellStyle(sanStyle);
					cell0.setCellValue(i++);
					if(j==0) {
						HSSFCell cell1 = row3.createCell(1);
						cell1.setCellStyle(sanStyle);
						cell1.setCellValue("创新创业教育教学改革研究项目");
					}else {
						HSSFCell cell1 = row3.createCell(1);
						cell1.setCellStyle(sanStyle);
						cell1.setCellValue("");
					}


					HSSFCell cell2 = row3.createCell(2);
					cell2.setCellStyle(sanStyle);
					cell2.setCellValue(j+1);

					HSSFCell cell3 = row3.createCell(3);
					cell3.setCellStyle(sanStyle);
					cell3.setCellValue(userList.get(j).getF_name());

					HSSFCell cell4 = row3.createCell(4);
					cell4.setCellStyle(sanStyle);
					cell4.setCellValue(userList.get(j).getF_principalname());

					HSSFCell cell5 = row3.createCell(5);
					cell5.setCellStyle(sanStyle);
					cell5.setCellValue(userList.get(j).getF_member());

					HSSFCell cell6 = row3.createCell(6);
					cell6.setCellStyle(sanStyle);
					cell6.setCellValue("");

					HSSFCell cell7= row3.createCell(7);
					cell7.setCellStyle(sanStyle);
					cell7.setCellValue("");
				}
			}
			//慕课部分2
			if(userList2 != null){
				int i=userList.size()+1;
				for(int j=0;j<userList2.size();j++)
				{
					//创建数据行,前面有两行,头标题行和列标题行
					HSSFRow row3 = sheet.createRow(j+userList.size()+5);
					HSSFCell cell0 = row3.createCell(0);
					cell0.setCellStyle(sanStyle);
					cell0.setCellValue(i++);

					if(j==0) {
						HSSFCell cell1 = row3.createCell(1);
						cell1.setCellStyle(sanStyle);
						cell1.setCellValue("创新创业教育慕课");
					}else {
						HSSFCell cell1 = row3.createCell(1);
						cell1.setCellStyle(sanStyle);
						cell1.setCellValue("");
					}

					HSSFCell cell2 = row3.createCell(2);
					cell2.setCellStyle(sanStyle);
					cell2.setCellValue(j+1);

					HSSFCell cell3 = row3.createCell(3);
					cell3.setCellStyle(sanStyle);
					cell3.setCellValue(userList2.get(j).getF_name());

					HSSFCell cell4 = row3.createCell(4);
					cell4.setCellStyle(sanStyle);
					cell4.setCellValue(userList2.get(j).getF_principalname());

					HSSFCell cell5 = row3.createCell(5);
					cell5.setCellStyle(sanStyle);
					cell5.setCellValue("");

					HSSFCell cell6 = row3.createCell(6);
					cell6.setCellStyle(sanStyle);
					cell6.setCellValue("");

					HSSFCell cell7= row3.createCell(7);
					cell7.setCellStyle(sanStyle);
					cell7.setCellValue("");

				}
			}
			//名师部分3
			if(userList3 != null){
				int i=userList.size()+userList2.size()+1;
				for(int j=0;j<userList3.size();j++)
				{
					//创建数据行,前面有两行,头标题行和列标题行
					HSSFRow row3 = sheet.createRow(j+userList.size()+userList2.size()+5);
					HSSFCell cell0 = row3.createCell(0);
					cell0.setCellStyle(sanStyle);
					cell0.setCellValue(i++);

					if(j==0) {
						HSSFCell cell1 = row3.createCell(1);
						cell1.setCellStyle(sanStyle);
						cell1.setCellValue("创新创业教育教学名师");
					}else {
						HSSFCell cell1 = row3.createCell(1);
						cell1.setCellStyle(sanStyle);
						cell1.setCellValue("");
					}

					HSSFCell cell2 = row3.createCell(2);
					cell2.setCellStyle(sanStyle);
					cell2.setCellValue(j+1);

					HSSFCell cell3 = row3.createCell(3);
					cell3.setCellStyle(sanStyle);
					cell3.setCellValue(userList3.get(j).getF_candidatename());

					HSSFCell cell4 = row3.createCell(4);
					cell4.setCellStyle(sanStyle);
					cell4.setCellValue("");

					HSSFCell cell5 = row3.createCell(5);
					cell5.setCellStyle(sanStyle);
					cell5.setCellValue("");

					HSSFCell cell6 = row3.createCell(6);
					cell6.setCellStyle(sanStyle);
					cell6.setCellValue("");

					HSSFCell cell7= row3.createCell(7);
					cell7.setCellStyle(sanStyle);
					cell7.setCellValue("");

				}
			}
			//团队部分4
			if(userList4 != null){
				int i=userList.size()+userList2.size()+userList3.size()+1;
				for(int j=0;j<userList4.size();j++)
				{
					//创建数据行,前面有两行,头标题行和列标题行
					HSSFRow row3 = sheet.createRow(j+userList.size()+userList2.size()+userList3.size()+5);
					HSSFCell cell0 = row3.createCell(0);
					cell0.setCellStyle(sanStyle);
					cell0.setCellValue(i++);

					if(j==0) {
						HSSFCell cell1 = row3.createCell(1);
						cell1.setCellStyle(sanStyle);
						cell1.setCellValue("创新创业教育教学团队");
					}else {
						HSSFCell cell1 = row3.createCell(1);
						cell1.setCellStyle(sanStyle);
						cell1.setCellValue("");
					}

					HSSFCell cell2 = row3.createCell(2);
					cell2.setCellStyle(sanStyle);
					cell2.setCellValue(j+1);

					HSSFCell cell3 = row3.createCell(3);
					cell3.setCellStyle(sanStyle);
					cell3.setCellValue(userList4.get(j).getF_teamname());

					HSSFCell cell4 = row3.createCell(4);
					cell4.setCellStyle(sanStyle);
					cell4.setCellValue(userList4.get(j).getF_principalname());

					HSSFCell cell5 = row3.createCell(5);
					cell5.setCellStyle(sanStyle);
					cell5.setCellValue("");

					HSSFCell cell6 = row3.createCell(6);
					cell6.setCellStyle(sanStyle);
					cell6.setCellValue("");

					HSSFCell cell7= row3.createCell(7);
					cell7.setCellStyle(sanStyle);
					cell7.setCellValue("");

				}
			}
			//专业部分5
			if(userList5 != null){
				int i=userList.size()+userList2.size()+userList3.size()+userList4.size()+1;
				for(int j=0;j<userList5.size();j++)
				{
					//创建数据行,前面有两行,头标题行和列标题行
					HSSFRow row3 = sheet.createRow(j+userList.size()+userList2.size()+userList3.size()+userList4.size()+5);
					HSSFCell cell0 = row3.createCell(0);
					cell0.setCellStyle(sanStyle);
					cell0.setCellValue(i++);

					if(j==0) {
						HSSFCell cell1 = row3.createCell(1);
						cell1.setCellStyle(sanStyle);
						cell1.setCellValue("创新创业教育试点改革专业");
					}else {
						HSSFCell cell1 = row3.createCell(1);
						cell1.setCellStyle(sanStyle);
						cell1.setCellValue("");
					}

					HSSFCell cell2 = row3.createCell(2);
					cell2.setCellStyle(sanStyle);
					cell2.setCellValue(j+1);

					HSSFCell cell3 = row3.createCell(3);
					cell3.setCellStyle(sanStyle);
					cell3.setCellValue(userList5.get(j).getF_name());

					HSSFCell cell4 = row3.createCell(4);
					cell4.setCellStyle(sanStyle);
					cell4.setCellValue(userList5.get(j).getF_principalname());

					HSSFCell cell5 = row3.createCell(5);
					cell5.setCellStyle(sanStyle);
					cell5.setCellValue("");

					HSSFCell cell6 = row3.createCell(6);
					cell6.setCellStyle(sanStyle);
					cell6.setCellValue("");

					HSSFCell cell7= row3.createCell(7);
					cell7.setCellStyle(sanStyle);
					cell7.setCellValue("");
				}
			}
			//创新创业教育改革示范高校6
			if(userList6 != null){
				int i=userList.size()+userList2.size()+userList3.size()+userList4.size()+userList5.size()+1;
				for(int j=0;j<userList6.size();j++)
				{
					//创建数据行,前面有两行,头标题行和列标题行
					HSSFRow row3 = sheet.createRow(j+userList.size()+userList2.size()+userList3.size()+userList4.size()+userList5.size()+5);
					HSSFCell cell0 = row3.createCell(0);
					cell0.setCellStyle(sanStyle);
					cell0.setCellValue(i++);

					if(j==0) {
						HSSFCell cell1 = row3.createCell(1);
						cell1.setCellStyle(sanStyle);
						cell1.setCellValue("创新创业教育改革示范高校");
					}else {
						HSSFCell cell1 = row3.createCell(1);
						cell1.setCellStyle(sanStyle);
						cell1.setCellValue("");
					}

					HSSFCell cell2 = row3.createCell(2);
					cell2.setCellStyle(sanStyle);
					cell2.setCellValue(j+1);

					HSSFCell cell3 = row3.createCell(3);
					cell3.setCellStyle(sanStyle);
					cell3.setCellValue(userList6.get(j).getF_company());

					HSSFCell cell4 = row3.createCell(4);
					cell4.setCellStyle(sanStyle);
					cell4.setCellValue(userList6.get(j).getF_principalname());

					HSSFCell cell5 = row3.createCell(5);
					cell5.setCellStyle(sanStyle);
					cell5.setCellValue("");

					HSSFCell cell6 = row3.createCell(6);
					cell6.setCellStyle(sanStyle);
					cell6.setCellValue("");

					HSSFCell cell7= row3.createCell(7);
					cell7.setCellStyle(sanStyle);
					cell7.setCellValue("");
				}
			}


			//5.输出
			workbook.write(fout);
			//            workbook.close();
			//out.close();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	private HSSFCellStyle createCellStyle(HSSFWorkbook workbook, short fontsize,boolean flag,boolean flag1,boolean flag2,String fontStyle,boolean flag3) {
		// TODO Auto-generated method stub
		HSSFCellStyle style = workbook.createCellStyle();
		//是否水平居中
		if(flag1){
			style.setAlignment(HSSFCellStyle.ALIGN_CENTER);//水平居中
		}
		if(flag2) {
			style.setAlignment(HSSFCellStyle.ALIGN_LEFT);//水平居中
		}
		style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);//垂直居中
		style .setWrapText(true);
		if(flag3) {
			style.setBorderBottom(HSSFCellStyle.BORDER_THIN); //下边框
			style.setBorderLeft(HSSFCellStyle.BORDER_THIN);//左边框
			style.setBorderTop(HSSFCellStyle.BORDER_THIN);//上边框
			style.setBorderRight(HSSFCellStyle.BORDER_THIN);//右边框
		}
		//创建字体
		HSSFFont font = workbook.createFont();
		//设置字体
		font.setFontName(fontStyle);
		//是否加粗字体
		if(flag){
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		}
		font.setFontHeightInPoints(fontsize);
		//加载字体
		style.setFont(font);
		return style;
	}

}
