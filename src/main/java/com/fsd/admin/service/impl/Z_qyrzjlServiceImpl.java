package com.fsd.admin.service.impl;

import com.fsd.admin.dao.Z_qyrzDao;
import com.fsd.admin.dao.Z_qyrzjlDao;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_qyrz;
import com.fsd.admin.model.Z_qyrzjl;
import com.fsd.admin.service.Z_qyrzService;
import com.fsd.admin.service.Z_qyrzjlService;
import com.fsd.core.common.BusinessException;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.util.ParametersUtil;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Repository("z_qyrzjlServiceImpl")
public class Z_qyrzjlServiceImpl extends BaseServiceImpl<Z_qyrzjl, String> implements Z_qyrzjlService {
    
    @Resource(name = "z_qyrzjlDaoImpl")
	public void setBaseDao(Z_qyrzjlDao Z_qyrzjlDao) {
		super.setBaseDao(Z_qyrzjlDao);
	}
	
	@Resource(name = "z_qyrzjlDaoImpl")
	private Z_qyrzjlDao objectDao;

	@Resource(name = "z_qyrzDaoImpl")
	private Z_qyrzDao qyrzDao;

	@Resource(name = "z_qyrzjlDaoImpl")
	private Z_qyrzjlDao qyrzjlDao;
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
    @Override
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.addOrder(Order.desc("time"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("qymc") != null && !"".equals(objectMap.get("qymc"))){
				c.add(Restrictions.like("qymc", "%" + objectMap.get("qymc") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}

	/**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	@Override
	public void save(Z_qyrzjl obj, A_Employee employee) throws Exception{
		if(getObjectByQyid(obj.getQyid(), "毕业申请")){
			throw new BusinessException("该企业已提交毕业申请，请勿重复提交，耐心等待审核！");
		}
		obj.setId(this.getUUID());
		obj.setTime(this.getData());
		obj.setType("毕业申请");
		objectDao.save(obj);
		Z_qyrz qyrz = qyrzDao.get(obj.getQyid());
		qyrz.setStatus("10086");
		qyrzDao.update(qyrz);
		Z_qyrzjl qyrzjl = new Z_qyrzjl();
		qyrzjl.setId(this.getUUID());
		qyrzjl.setQyid(obj.getQyid());
		qyrzjl.setQymc(obj.getQymc());
		qyrzjl.setTime(this.getData());
		qyrzjl.setType("毕业申请");
		qyrzjl.setRemark("原因："+obj.getRemark());
		qyrzjlDao.save(qyrzjl);
	}

	/**
	 * 根据条件查找对象
	 * @return
	 * @throws Exception
	 */
	@Override
	public boolean getObjectByQyid(String id, String type) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("qyid", id));
		c.add(Restrictions.eq("type", type));
		List<Z_qyrzjl> list = c.list();
		if(list.size() != 0){
			return true;
		}else{
			return false;
		}
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
    @Override
	public Z_qyrzjl getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException("获取缺少ID参数!");
		}
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("qyid", objectMap.get("id").toString()));
		c.add(Restrictions.eq("type", "毕业申请"));
		List<Z_qyrzjl> list = c.list();
		if(list.size() != 0){
			return list.get(0);
		}else{
			return null;
		}
	}

	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	@Override
	public Z_qyrzjl getObject(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException("获取缺少ID参数!");
		}
		Z_qyrzjl obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null) {
			throw new BusinessException("数据不存在!");
		}
		return obj;
	}

}
