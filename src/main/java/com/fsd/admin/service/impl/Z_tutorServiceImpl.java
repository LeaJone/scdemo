package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_course;
import com.fsd.admin.model.Z_tutor;
import com.fsd.admin.service.Z_tutorService;
import com.fsd.admin.dao.Z_tutorDao;

@Repository("z_tutorServiceImpl")
public class Z_tutorServiceImpl extends BaseServiceImpl<Z_tutor, String> implements Z_tutorService{
    
    private static final Logger log = Logger.getLogger(Z_tutorServiceImpl.class);
    private String depict = "";
    
    @Resource(name = "z_tutorDaoImpl")
	public void setBaseDao(Z_tutorDao Z_tutorDao) {
		super.setBaseDao(Z_tutorDao);
	}
	
	@Resource(name = "z_tutorDaoImpl")
	private Z_tutorDao objectDao;
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
    @Override
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("f_deleted", "0"));
		c.add(Restrictions.eq("f_addemployeeid", employee.getId()));
		c.addOrder(Order.asc("f_sort"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("name") != null && !"".equals(objectMap.get("name"))){
				c.add(Restrictions.like("f_name", "%" + objectMap.get("name") + "%"));
			}
			if(objectMap.get("type") != null && !"".equals(objectMap.get("type"))){
				String type = objectMap.get("type").toString();
				if("xy".equals(type)){
					c.add(Restrictions.eq("f_isfriend", "true"));
				}else{
					c.add(Restrictions.eq("f_typecode", type));
				}
			}
		}
		return objectDao.findPager(param , c);
	}
    
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
    @Override
	public Z_tutor save(Z_tutor obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
            Z_tutor old_obj = objectDao.get(obj.getId());
			old_obj.setF_typecode(obj.getF_typecode());
			old_obj.setF_typename(obj.getF_typename());
			old_obj.setF_name(obj.getF_name());
			old_obj.setF_age(obj.getF_age());
			old_obj.setF_degree(obj.getF_degree());
			old_obj.setF_professionalTitle(obj.getF_professionalTitle());
			old_obj.setF_sort(obj.getF_sort());
			old_obj.setF_sexid(obj.getF_sexid());
			old_obj.setF_sexname(obj.getF_sexname());
			old_obj.setF_courses(obj.getF_courses());
			old_obj.setF_direction(obj.getF_direction());
			old_obj.setF_helpspeciality(obj.getF_helpspeciality());
			old_obj.setF_workunit(obj.getF_workunit());
			old_obj.setF_remark(obj.getF_remark());
			old_obj.setF_imgurl(obj.getF_imgurl());
			old_obj.setF_resume(obj.getF_resume());
			old_obj.setF_isfriend(obj.getF_isfriend());
			old_obj.setF_practice(obj.getF_practice());
			//设置修改日期
            old_obj.setF_lastupdatedate(this.getData());
			//设置修改用户id
			old_obj.setF_updateemployeeid(employee.getId());
			//设置修改用户姓名
			old_obj.setF_updateemployeename(employee.getRealname());
            objectDao.update(old_obj);
            return old_obj;
        }else{
            //添加主键
            obj.setId(this.getUUID());//设置主键
			//设置添加日期
			obj.setF_adddate(this.getData());
			//设置添加用户id
			obj.setF_addemployeeid(employee.getId());
			//设置添加用户姓名
			obj.setF_addemployeename(employee.getRealname());
			//设置删除标志(0:正常 1：已删除)
			obj.setF_statusid("ty");
			obj.setF_statusname("停用");
			obj.setF_deleted("0");
			objectDao.save(obj);
			return obj;
        }
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
    @Override
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		for (String id : dir) {
			Z_tutor tutor = objectDao.get(id);
			tutor.setF_deleted("1");
			objectDao.update(tutor);
		}
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
    @Override
	public Z_tutor getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		Z_tutor obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		
		return obj;
	}

	@Override
	public void saveMessage(Z_tutor obj, A_Employee loginUser) {
		Z_tutor old_obj = objectDao.get(obj.getId());
		old_obj.setF_statusid("jjsh");
		old_obj.setF_statusname("拒绝审核");
		old_obj.setF_reason(obj.getF_reason());
		objectDao.save(old_obj);
	}

	@Override
	public void updateCheckedObject(ParametersUtil parameters, A_Employee employee) {
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> idList = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		String oid = "";
		for (String id : idList) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
			oid = id;
		}
		boolean isEnabled = (Boolean) objectMap.get("state");
		if (isEnabled) {
			objectDao.executeHql("update Z_tutor set f_statusid = 'qy',f_statusname='启用'  where id in (" + ids + ")");
		} else {
			objectDao.executeHql("update Z_tutor set f_statusid = 'ty',f_statusname='停用'  where id in (" + ids + ")");
		}
	}
}
