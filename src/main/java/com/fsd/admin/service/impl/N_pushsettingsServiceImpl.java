package com.fsd.admin.service.impl;

import com.fsd.admin.dao.N_pushsettingsDao;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.N_pushsettings;
import com.fsd.admin.service.N_pushsettingsService;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.ParametersUtil;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Map;

@Repository("n_pushsettingsServiceImpl")
public class N_pushsettingsServiceImpl extends MainServiceImpl<N_pushsettings, String> implements N_pushsettingsService {

    private String depict = "推送应用设置";
    
    @Resource(name = "n_pushsettingsDaoImpl")
	public void setBaseDao(N_pushsettingsDao n_pushsettingsDao) {
		super.setBaseDao(n_pushsettingsDao);
	}
	
	@Resource(name = "n_pushsettingsDaoImpl")
	private N_pushsettingsDao objectDao;
	
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@Override
	public ParametersUtil getObjectPageList(ParametersUtil param) throws Exception{
		Criteria c = objectDao.createCriteria();
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("appname") != null && !"".equals(objectMap.get("appname"))){
				c.add(Restrictions.eq("appname", objectMap.get("appname")));
			}
		}
		return objectDao.findPager(param , c);
	}

	/**
	 * 保存对象（添加、修改）
	 * @param obj
	 */
	@Override
	public void saveObject(N_pushsettings obj , A_Employee employee) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			N_pushsettings obj_old = objectDao.get(obj.getId());
			obj_old.setAppid(obj.getAppid());
			obj_old.setAppkey(obj.getAppkey());
			obj_old.setAppname(obj.getAppname());
			obj_old.setAppsecret(obj.getAppsecret());
			obj_old.setMastersecret(obj.getMastersecret());
			objectDao.update(obj_old);
		}else{
			//添加
			obj.setId(this.getUUID());//设置主键
			objectDao.save(obj);
		}
	}
	
	/**
	 * 删除对象
	 * @param parameters
	 */
	@Override
	public void delObject(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		for (String id : dir) {
			objectDao.delete(id);
		}
	}
	
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	@Override
	public N_pushsettings getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		N_pushsettings obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		return obj;
	}
}
