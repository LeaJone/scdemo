package com.fsd.admin.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.DateTimeUtil;
import com.fsd.core.util.ParametersUtil;
import com.fsd.core.util.PdfUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_edumuke;
import com.fsd.admin.model.Z_eduprofession;
import com.fsd.admin.service.Z_eduprofessionService;
import com.fsd.admin.dao.Z_eduprofessionDao;

@Repository("z_eduprofessionServiceImpl")
public class Z_eduprofessionServiceImpl extends BaseServiceImpl<Z_eduprofession, String> implements Z_eduprofessionService{
    
    private static final Logger log = Logger.getLogger(Z_eduprofessionServiceImpl.class);
    private String depict = "";
    
    @Resource(name = "z_eduprofessionDaoImpl")
	public void setBaseDao(Z_eduprofessionDao Z_eduprofessionDao) {
		super.setBaseDao(Z_eduprofessionDao);
	}
	
	@Resource(name = "z_eduprofessionDaoImpl")
	private Z_eduprofessionDao objectDao;
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
    @Override
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("f_deleted", "0"));
		c.add(Restrictions.eq("f_addemployeeid", employee.getId()));
		c.addOrder(Order.asc("f_adddate"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("name") != null && !"".equals(objectMap.get("name"))){
				c.add(Restrictions.like("f_name", "%" + objectMap.get("name") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}
    
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
    @Override
	public void save(String rootpath,Z_eduprofession obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
        	File file = new File("uploadfiles/projectfile/" + obj.getId() + ".doc");//删除PDF文件
    		if(file.exists()) {
    			file.delete();
    		}
            //修改
            Z_eduprofession old_obj = objectDao.get(obj.getId());
            old_obj.setF_name(obj.getF_name());
            old_obj.setF_teamid(obj.getF_teamid());
            old_obj.setF_teamname(obj.getF_teamname());
            old_obj.setF_principalname(obj.getF_principalname());
            old_obj.setF_basename(obj.getF_basename());
            old_obj.setF_money(obj.getF_money());
            
            old_obj.setF_lastupdatedate(this.getData());//设置修改日期
			old_obj.setF_updateemployeeid(employee.getId());//设置修改用户id
			old_obj.setF_updateemployeename(employee.getRealname());//设置修改用户姓名
			
			doPdf(rootpath,old_obj,old_obj.getF_lastupdatedate());//生成修改后的PDF文件

            objectDao.update(old_obj);
        }else{
            //添加
            obj.setId(this.getUUID());//设置主键
            obj.setF_addemployeeid(employee.getId());
			obj.setF_adddate(this.getData());//设置添加日期
			obj.setF_addemployeeid(employee.getId());//设置添加用户id
			obj.setF_addemployeename(employee.getRealname());//设置添加用户姓名
			obj.setF_deleted("0");//设置删除标志(0:正常 1：已删除)
			doPdf(rootpath,obj,obj.getF_adddate());//生成PDF文件
			objectDao.save(obj);
        }
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
    @Override
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		String hql = "";
		objectDao.executeHql("update Z_eduprofession t set t.f_deleted = '1', t.f_lastupdatedate = '" + this.getData() + 
				"', t.f_updateemployeeid = '" + employee.getId() + "', t.f_updateemployeename = '" + employee.getRealname() + 
				"' where t.id in (" + ids + ")");
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
    @Override
	public Z_eduprofession getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		Z_eduprofession obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		
		return obj;
	}
    private void doPdf(String rootpath,Z_eduprofession obj,String date) throws Exception {
    	
    	String pdfurl = "uploadfiles/projectfile/" + obj.getId() + ".doc";
		obj.setF_pdfurl(pdfurl);
		String tmpFile = rootpath + "uploadfiles/pdftemplate/edu_profession.doc";
		String outPath = rootpath + pdfurl;
		//生成Word文件
		Map<String,Object> map= new HashMap<String,Object>();
        map.put("p_name",obj.getF_name());
        map.put("p_principalname",obj.getF_principalname());
        map.put("p_date",DateTimeUtil.formatDate(date, "yyyyMMddHHmmss", "yyyy年MM月dd日"));
		PdfUtil.createWordByTemplate(map, tmpFile, outPath);
    }

	@Override
	public void uploadFile(Z_eduprofession obj, A_Employee loginUser) {
		Z_eduprofession old_obj = objectDao.get(obj.getId());
		old_obj.setF_pdfurl(obj.getF_pdfurl());
		objectDao.save(old_obj);
	}

    @Override
    public List<Z_eduprofession> selectAll(String start, String end) {
		return objectDao.queryBySql1("select * from Z_eduprofession where f_adddate > ? and f_adddate < ? and f_deleted = ?", start, end, "0");
    }
}
