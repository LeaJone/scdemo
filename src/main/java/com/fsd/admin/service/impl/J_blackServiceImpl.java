package com.fsd.admin.service.impl;

import com.fsd.admin.dao.J_blackDao;
import com.fsd.admin.dao.J_postsDao;
import com.fsd.admin.dao.J_replyDao;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.J_black;
import com.fsd.admin.model.J_posts;
import com.fsd.admin.model.J_reply;
import com.fsd.admin.service.J_blackService;
import com.fsd.admin.service.J_postsService;
import com.fsd.admin.service.J_replyService;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.ParametersUtil;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service("j_blackServiceImpl")
public class J_blackServiceImpl extends MainServiceImpl<J_black, String> implements J_blackService {

    private String depict = "黑名单";
    
    @Resource(name = "j_blackDaoImpl")
	public void setBaseDao(J_blackDao J_blackDao) {
		super.setBaseDao(J_blackDao);
	}
	
	@Resource(name = "j_blackDaoImpl")
	private J_blackDao objectDao;

	@Resource(name = "j_postsDaoImpl")
	private J_postsDao postsDao;

	@Resource(name = "j_replyDaoImpl")
	private J_replyDao replyDao;

	@Autowired
	private J_postsService postsService;

	@Autowired
	private J_replyService replyService;
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("username") != null && !"".equals(objectMap.get("username"))){
				c.add(Restrictions.like("username", "%" + objectMap.get("username") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, String isscreen) throws Exception {
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> idList = (ArrayList<String>) objectMap.get("ids");
		String ids = this.getSQLinByIDList(idList);
		if(isscreen.equals("true")){
			for (int i = 0; i < idList.size(); i++) {
				J_black black = objectDao.get(idList.get(i));
				postsDao.executeHql("update J_posts set isscreen = 'false' where pubuserid = '" + black.getUserid() + "'");
				replyDao.executeHql("update J_reply set isscreen = 'false' where reuserid = '" + black.getUserid() + "'");
			}
			objectDao.executeHql("delete from J_black where id in (" + ids + ")");
		}else{
			objectDao.executeHql("update J_black set deleted = '1' where id in (" + ids + ")");
		}
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public J_black getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		J_black obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		return obj;
	}

	/**
	 * 添加黑名单（帖子）
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public void saveObject(ParametersUtil parameters, String isscreen) throws Exception{
		boolean isFlag = true;
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> idList = (ArrayList<String>) objectMap.get("ids");
		for (int i = 0; i < idList.size(); i++) {
			J_posts posts = postsService.get(idList.get(i));
			J_black black = getBlackByUserId(posts.getPubuserid());
			if(black != null){
				if(black.getDeleted().equals("1")){
					isFlag = false;
					black.setDeleted("0");
					objectDao.update(black);
				}else{
					throw new BusinessException("你好，用户" + black.getUsername() + "已被列为黑名单，请不要重复添加！");
				}

			}
			if(isFlag){
				J_black obj = new J_black();
				obj.setId(this.getUUID());
				obj.setUserid(posts.getPubuserid());
				obj.setUsername(posts.getPubusername());
				obj.setDeleted("0");
				objectDao.save(obj);
			}
			if(isscreen.equals("true")){
				postsDao.executeHql("update J_posts set isscreen = 'true' where pubuserid = '" + posts.getPubuserid() + "'");
				replyDao.executeHql("update J_reply set isscreen = 'true' where reuserid = '" + posts.getPubuserid() + "'");
			}
		}
	}

	/**
	 * 添加黑名单（回复）
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public void saveHfObject(ParametersUtil parameters, String isscreen) throws Exception{
		boolean isFlag = true;
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> idList = (ArrayList<String>) objectMap.get("ids");
		for (int i = 0; i < idList.size(); i++) {
			J_reply reply = replyService.get(idList.get(i));
			J_black black = getBlackByUserId(reply.getReuserid());
			if(black != null){
				if(black.getDeleted().equals("1")){
					isFlag = false;
					black.setDeleted("0");
					objectDao.update(black);
				}else{
					throw new BusinessException("你好，用户" + black.getUsername() + "已被列为黑名单，请不要重复添加！");
				}

			}
			if(isFlag){
				J_black obj = new J_black();
				obj.setId(this.getUUID());
				obj.setUserid(reply.getReuserid());
				obj.setUsername(reply.getReusername());
				obj.setDeleted("0");
				objectDao.save(obj);
			}
			if(isscreen.equals("true")){
				postsDao.executeHql("update J_posts set isscreen = 'true' where pubuserid = '" + reply.getReuserid() + "'");
				replyDao.executeHql("update J_reply set isscreen = 'true' where reuserid = '" + reply.getReuserid() + "'");
			}
		}
	}

	/**
	 * 根据ID加载黑名单
	 * @return
	 * @throws Exception
	 */
	public J_black getBlackByUserId(String userid) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("userid", userid));
		List<J_black> list = objectDao.getList(c);
		if(list.size() != 0){
			return list.get(0);
		}else{
			return null;
		}
	}

}
