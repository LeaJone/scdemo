package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import com.fsd.admin.dao.Z_emailDao;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.B_article;
import com.fsd.admin.model.B_subject;
import com.fsd.admin.model.Z_email;
import com.fsd.admin.service.Z_emailService;
import com.fsd.admin.service.impl.MainServiceImpl;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.Config;
import com.fsd.core.util.EhcacheUtil;
import com.fsd.core.util.ParametersUtil;
import com.google.gson.Gson;

/**
 * 站内信 - sevice实现类
 * @author Administrator
 *
 */
@Repository("z_emailServiceImpl")
public class Z_emailServiceImpl extends MainServiceImpl<Z_email, String> implements Z_emailService {
	
	private static String depict = "信件：";
	
	@Resource(name = "z_emailDaoImpl")
	public void setBaseDao(Z_emailDao z_emailDao) {
		super.setBaseDao(z_emailDao);
	}
	
	@Resource(name = "z_emailDaoImpl")
	private Z_emailDao objectDao;
	
	/**
	 * 发送邮件
	 * @param obj
	 */
	public void saveEmailFirst(Z_email obj, A_Employee employee) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			Z_email obj_old = objectDao.get(obj.getId());
			obj_old.setMessagetitle(obj.getMessagetitle());;
			obj_old.setMessagetext(obj.getMessagetext());
			obj_old.setSavetime(this.getData());
			obj_old.setPushstatusid("false");
			obj_old.setPushstatus("未发送");
			obj_old.setSendid(employee.getId());
			obj_old.setSendname(employee.getRealname());
			obj_old.setDeleted("0");
			obj_old.setIsstar("false");
			objectDao.update(obj_old);
		}else{
			//保存
			obj.setId(this.getUUID());//设置主键
			obj.setSavetime(this.getData());
			obj.setPushstatusid("false");
			obj.setPushstatus("未发送");
			obj.setSendid(employee.getId());
			obj.setSendname(employee.getRealname());
			obj.setDeleted("0");
			obj.setIsstar("false");
			objectDao.save(obj);
		}
	}
	
	/**
	 * 发送邮件
	 * @param obj
	 */
	public void saveEmail(Z_email obj, A_Employee employee) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			Z_email obj_old = objectDao.get(obj.getId());
			obj_old.setMessagetitle(obj.getMessagetitle());;
			obj_old.setMessagetext(obj.getMessagetext());
			obj_old.setSavetime(this.getData());
			obj_old.setPushstatusid("true");
			obj_old.setPushstatus("已发送");
			obj_old.setSendid(employee.getId());
			obj_old.setSendname(employee.getRealname());
			obj_old.setSenddate(this.getData());
			obj_old.setRecid(obj.getRecid());
			obj_old.setRecname(obj.getRecname());
			obj_old.setReadstatusid("false");
			obj_old.setReadstatus("未读");
			obj_old.setDeleted("0");
			obj_old.setIsstar("false");
			objectDao.update(obj_old);
		}else{
			//发送
			obj.setId(this.getUUID());//设置主键
			obj.setSavetime(this.getData());
			obj.setPushstatusid("true");
			obj.setPushstatus("已发送");
			obj.setSendid(employee.getId());
			obj.setSendname(employee.getRealname());
			obj.setSenddate(this.getData());
			obj.setReadstatusid("false");
			obj.setReadstatus("未读");
			obj.setDeleted("0");
			obj.setIsstar("false");
			objectDao.save(obj);
		}
	}

	/**
	 * 发送系统邮件
	 * @param title
	 * @param content
	 * @param employee
	 * @throws Exception
	 */
	@Override
	public void saveMessage(String title, String content, A_Employee employee) throws Exception{
		Z_email email = new Z_email();
		email.setId(this.getUUID());

		email.setRecid(employee.getId());
		email.setMessagetitle(title);
		email.setMessagetext(content);

		email.setSavetime(this.getData());
		email.setPushstatusid("true");
		email.setPushstatus("已发送");
		email.setSendid("0");
		email.setSendname("系统");
		email.setSenddate(this.getData());

		email.setReadstatusid("false");
		email.setReadstatus("未读");
		email.setDeleted("0");
		email.setIsstar("false");

		objectDao.save(email);
	}

	/**
	 * 加载已删除邮件分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getRecycleEmailPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "1"));
		c.add(Restrictions.or(Restrictions.eq("recid", employee.getId()), Restrictions.eq("sendid", employee.getId())));
		c.addOrder(Order.desc("senddate"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("title") != null && !"".equals(objectMap.get("title"))){
				c.add(Restrictions.like("messagetitle", "%" + objectMap.get("title") + "%"));
			}
		}
		return objectDao.findPager(param, c);
	}
	
	/**
	 * 加载星标邮件分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getStarEmailPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.add(Restrictions.eq("isstar", "true"));  //星标
		c.add(Restrictions.or(Restrictions.eq("recid", employee.getId()), Restrictions.eq("sendid", employee.getId())));
		c.addOrder(Order.desc("senddate"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("title") != null && !"".equals(objectMap.get("title"))){
				c.add(Restrictions.like("messagetitle", "%" + objectMap.get("title") + "%"));
			}
		}
		return objectDao.findPager(param, c);
	}
	
	/**
	 * 加载草稿箱邮件分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getDraftEmailPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.add(Restrictions.eq("pushstatusid", "false"));  //未发送
		c.add(Restrictions.eq("sendid", employee.getId()));
		c.addOrder(Order.desc("savetime"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("title") != null && !"".equals(objectMap.get("title"))){
				c.add(Restrictions.like("messagetitle", "%" + objectMap.get("title") + "%"));
			}
		}
		return objectDao.findPager(param, c);
	}
	
	/**
	 * 加载已发送邮件分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getSendEmailPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.add(Restrictions.eq("pushstatusid", "true"));  //已发送
		c.add(Restrictions.eq("sendid", employee.getId()));
		c.addOrder(Order.desc("senddate"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("title") != null && !"".equals(objectMap.get("title"))){
				c.add(Restrictions.like("messagetitle", "%" + objectMap.get("title") + "%"));
			}
		}
		return objectDao.findPager(param, c);
	}
	
	/**
	 * 加载收件箱分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getInboxEmailPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.add(Restrictions.eq("pushstatusid", "true"));  //未发送
		c.add(Restrictions.eq("recid", employee.getId()));
		c.addOrder(Order.desc("senddate"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("title") != null && !"".equals(objectMap.get("title"))){
				c.add(Restrictions.like("messagetitle", "%" + objectMap.get("title") + "%"));
			}
		}
		return objectDao.findPager(param, c);
	}
	
	/**
	 * 恢复邮件
	 * @param parameters
	 */
	@SuppressWarnings("unchecked")
	public void updateObjectDeleted(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = this.getSQLinByIDList(dir);
		objectDao.executeHql("update Z_email t set t.deleted = '0' where t.id in (" + ids + ")");
	}
	
	/**
	 * 彻底删除邮件
	 * @param parameters
	 */
	@SuppressWarnings("unchecked")
	public void delRealyObject(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = this.getSQLinByIDList(dir);
		objectDao.executeHql("delete from Z_email where id in (" + ids + ")");
	}
	
	/**
	 * 邮件标为已读、未读
	 * @param parameters
	 */
	@SuppressWarnings("unchecked")
	public void updateEnabledObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> idList = (ArrayList<String>) objectMap.get("ids");
		String ids = this.getSQLinByIDList(idList);
		boolean isEnabled = (Boolean) objectMap.get("isenabled");
		if (isEnabled){
			objectDao.executeHql("update Z_email t set t.readstatusid = 'true' ,t.readstatus = '已读' where t.id in (" + ids + ")");
		}else{
			objectDao.executeHql("update Z_email t set t.readstatusid = 'false' ,t.readstatus = '未读' where t.id in (" + ids + ")");
		}
	}
	
	/**
	 * 查看邮件详情
	 * @param id
	 * @return
	 */
	public Z_email getEmail(String id) {
		return objectDao.get(id);
	}
	
	/**
	 * 删除邮件
	 * @param parameters
	 */
	@SuppressWarnings("unchecked")
	public void deletedEmail(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = this.getSQLinByIDList(dir);
		objectDao.executeHql("update Z_email t set t.deleted = '1' where t.id in (" + ids + ")");
	}
	
	/**
	 * 取消邮件星标
	 * @param parameters
	 */
	@SuppressWarnings("unchecked")
	public void updatenostarEmail(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = this.getSQLinByIDList(dir);
		objectDao.executeHql("update Z_email t set t.isstar = 'false' where t.id in (" + ids + ")");
	}
	
	/**
	 * 邮件标星
	 * @param parameters
	 */
	@SuppressWarnings("unchecked")
	public void updatestarEmail(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = this.getSQLinByIDList(dir);
		objectDao.executeHql("update Z_email t set t.isstar = 'true' where t.id in (" + ids + ")");
	}
	
	/**
	 * 根据ID加载邮件对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getEmailById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		Z_email obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		ParametersUtil util = new ParametersUtil();
		util.addSendObject("data", obj);
		return util;
	}
	
	/**
	 * 邮件标为已读、未读
	 * @param parameters
	 */
	@SuppressWarnings("unchecked")
	public void updateEmailToRead(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		String id = (String) objectMap.get("id");
		objectDao.executeSql("update Z_email t set t.readstatusid = 'true' ,t.readstatus = '已读' where t.id = ?", id);
	}

	/**
	 * 统计用户未读邮件数量
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@Override
	public int getCountById(String id) throws Exception{
		String sql = "select * from Z_email where readstatusid = 'false' and readstatus = '未读' and recid = ?";
		List<Z_email> list = objectDao.queryBySql1(sql, id);
		if(list != null){
			return list.size();
		}
		return 0;
	}
}
