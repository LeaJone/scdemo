package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.stereotype.Repository;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.Config;
import com.fsd.core.util.ParametersUtil;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.B_download;
import com.fsd.admin.service.B_downloadService;
import com.fsd.admin.dao.B_downloadDao;
import com.google.gson.Gson;

@Repository("b_downloadServiceImpl")
public class B_downloadServiceImpl extends MainServiceImpl<B_download, String> implements B_downloadService{
    
    private static final Logger log = Logger.getLogger(B_downloadServiceImpl.class);
    private String depict = "文档管理";
    
    @Resource(name = "b_downloadDaoImpl")
	public void setBaseDao(B_downloadDao b_downloadDao) {
		super.setBaseDao(b_downloadDao);
	}
	
	@Resource(name = "b_downloadDaoImpl")
	private B_downloadDao objectDao;
	
	/**
	 * 保存对象（添加、修改）
	 * @param obj
	 * @param a_employee
	 * @param a_company
	 * @throws Exception
	 */
	public void saveObject(B_download obj, A_Employee employee) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			B_download old_obj = objectDao.get(obj.getId());
			old_obj.setName(obj.getName());
			old_obj.setPath(obj.getPath());
			old_obj.setContent(obj.getContent());
			old_obj.setSizes(obj.getSizes());
			old_obj.setLanguage(obj.getLanguage());
			old_obj.setVersion(obj.getVersion());
			old_obj.setClassified(obj.getClassified());
			old_obj.setUpdated(obj.getUpdated());
			old_obj.setAdaptive(obj.getAdaptive());
			old_obj.setRemark(obj.getRemark());
			old_obj.setType(obj.getType());
			old_obj.setTypename(obj.getTypename());
			old_obj.setStatus(obj.getStatus());
			old_obj.setStatusname(obj.getStatusname());
			old_obj.setUpdatedate(this.getData());
			old_obj.setUpdateemployeeid(employee.getId());
			old_obj.setUpdateemployeename(employee.getRealname());
			objectDao.update(old_obj);
		}else{
			//新增
			obj.setId(this.getUUID());
			obj.setCompanyid(employee.getCompanyid());
			obj.setAdddate(this.getData());
			obj.setAddemployeeid(employee.getId());
			obj.setAddemployeename(employee.getRealname());
			obj.setDeleted("0");//默认0标注未删除
			objectDao.save(obj);
		}
		this.sysLogService.saveObject(Config.LOGTYPEBC, this.depict + obj.getId() + obj.getName(), employee, B_downloadServiceImpl.class);
	}
	
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public B_download getObjectByArticleId(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		List<B_download> list = objectDao.queryByHql("from B_download t where t.articleid = '" + objectMap.get("id").toString() + "'");
		if(list == null || list.size() <=0){
			return new B_download();
		}
		return list.get(0);
	}
	
	/**
	 * 删除对象
	 * @param parameters
	 */
	public void delObject(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		objectDao.executeHql("update B_download t set t.deleted = '1' where t.id in (" + ids + ")");
	}
	
	/**
	 * 彻底对象
	 * @param parameters
	 */
	@SuppressWarnings("unchecked")
	public void delRealyObjectByArticleId(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		objectDao.executeHql("delete from B_download where articleid in (" + ids + ")");
	}

	
	
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		c.add(Restrictions.eq("type", "xzwjwd"));
		c.addOrder(Order.desc("adddate"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("name") != null && !"".equals(objectMap.get("name"))){
				c.add(Restrictions.like("name", "%" + objectMap.get("name") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}
	
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public B_download getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		B_download obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		return obj;
	}
	
	/**
	 * 修改对象状态
	 * @param parameters
	 */
	@SuppressWarnings("unchecked")
	public void updateEnabledObject(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		boolean isEnabled = (Boolean) objectMap.get("isenabled");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		if (isEnabled)
			objectDao.executeHql("update B_download t set t.status = 'xzwjqy', t.statusname = '启用' where t.id in (" + ids + ")");
		else
			objectDao.executeHql("update B_download t set t.status = 'xzwjty', t.statusname = '停用' where t.id in (" + ids + ")");
	}
}
