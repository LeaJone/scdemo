package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import com.fsd.core.bean.ImportExcel;
import org.springframework.stereotype.Repository;
import com.fsd.core.common.BusinessException;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.util.DateTimeUtil;
import com.fsd.core.util.ExcelUtil;
import com.fsd.core.util.ParametersUtil;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_ckq;
import com.fsd.admin.service.Z_ckqService;
import com.fsd.admin.dao.Z_ckqDao;
import com.google.gson.Gson;

@Repository("z_ckqServiceImpl")
public class Z_ckqServiceImpl extends BaseServiceImpl<Z_ckq, String> implements Z_ckqService{
    
    private static final Logger log = Logger.getLogger(Z_ckqServiceImpl.class);
    private String depict = "";
    
    @Resource(name = "z_ckqDaoImpl")
	public void setBaseDao(Z_ckqDao Z_ckqDao) {
		super.setBaseDao(Z_ckqDao);
	}
	
	@Resource(name = "z_ckqDaoImpl")
	private Z_ckqDao objectDao;
	
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		c.addOrder(Order.asc("adddate"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("xkzh") != null && !"".equals(objectMap.get("xkzh"))){
				c.add(Restrictions.like("xkzh", "%" + objectMap.get("xkzh") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}
	
	/**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public void save(Z_ckq obj, A_Employee employee) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			Z_ckq old_obj = objectDao.get(obj.getId());
			
			old_obj.setCompanyid(employee.getCompanyid());
			old_obj.setXkzh(obj.getXkzh());
			old_obj.setSqr(obj.getSqr());
			old_obj.setKsmc(obj.getKsmc());
			old_obj.setKczkz(obj.getKczkz());
			old_obj.setSjgm(obj.getSjgm());
			old_obj.setGmdw(obj.getGmdw());
			old_obj.setKcfs(obj.getKcfs());
			old_obj.setKqmj(obj.getKqmj());
			old_obj.setYxqq(obj.getYxqq());
			old_obj.setYxqz(obj.getYxqz());
			old_obj.setSzxzqdm(obj.getSzxzqdm());
			old_obj.setSzxzqmc(obj.getSzxzqmc());
			old_obj.setGdzb(obj.getGdzb());
			old_obj.setRemark(obj.getRemark());
			old_obj.setUpdatedate(this.getData());//设置修改日期
			old_obj.setUpdateemployeeid(employee.getId());//设置修改用户id
			old_obj.setUpdateemployeename(employee.getRealname());//设置修改用户姓名
			
			objectDao.update(old_obj);
		}else{
			//添加
			obj.setId(this.getUUID());//设置主键
			obj.setCompanyid(employee.getCompanyid());//所属单位
			obj.setAdddate(this.getData());//设置添加日期
			obj.setAddemployeeid(employee.getId());//设置添加用户id
			obj.setAddemployeename(employee.getRealname());//设置添加用户姓名
			obj.setDeleted("0");//设置删除标志(0:正常 1：已删除)
			
			objectDao.save(obj);
		}
	}
	
	/**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		String hql = "";
		objectDao.executeHql("update Z_ckq t set t.deleted = '1', t.updatedate = '" + this.getData() + 
				"', t.updateemployeeid = '" + employee.getId() + "', t.updateemployeename = '" + employee.getRealname() + 
				"' where t.id in (" + ids + ")");
	}
	
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public Z_ckq getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		Z_ckq obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		
		return obj;
	}
	
	/**
	 * 导入Excel
	 * @param obj
	 * @param employee
	 * @param diskUrl
	 * @throws Exception
	 */
	public void saveImportObject(ImportExcel obj, A_Employee employee, String diskUrl) throws Exception{
		System.out.println(diskUrl);
		System.out.println(obj.getExcel());
		List<Object> list = ExcelUtil.readExcel(diskUrl + obj.getExcel(), Integer.parseInt(obj.getSheet()), Integer.parseInt(obj.getKsh()), Integer.parseInt(obj.getKsl()), Integer.parseInt(obj.getHs()), Integer.parseInt(obj.getLs()));
		for (int i = 0; i < list.size(); i++) {
			List<Object> olist = (List<Object>) list.get(i);
			
			Z_ckq ckq = new Z_ckq();
			ckq.setId(getUUID());//设置主键ID
			ckq.setCompanyid(employee.getCompanyid());//设置所属单位
			ckq.setAdddate(this.getData());//设置添加日期
			ckq.setAddemployeeid(employee.getAddemployeeid());//设置添加用户ID
			ckq.setAddemployeename(employee.getAddemployeename());//设置添加用户姓名
			ckq.setDeleted("0");//设置删除标志(0：正常；1：已删除)
			
			ckq.setXkzh(olist.get(0).toString().trim());//设置许可证号
			ckq.setSqr(olist.get(1).toString().trim());//设置申请人
			ckq.setKsmc(olist.get(2).toString().trim());//设置矿山名称
			ckq.setKczkz(olist.get(3).toString().trim());//设置开采主矿种
			ckq.setSjgm(olist.get(4).toString().trim());//设置设计规模
			ckq.setGmdw(olist.get(5).toString().trim());//设置规模单位
			ckq.setKcfs(olist.get(6).toString().trim());//设置开采方式
			ckq.setKqmj(olist.get(7).toString().trim());//设置矿区面积
			ckq.setYxqx(olist.get(8).toString().trim());//设置有效期限
			
			String yxqq = DateTimeUtil.formatDate(olist.get(9).toString().trim(), "yyyy-MM-dd", "yyyyMMdd");
			ckq.setYxqq(yxqq);//设置有效期起
			
			String yxqz = DateTimeUtil.formatDate(olist.get(10).toString().trim(), "yyyy-MM-dd", "yyyyMMdd");
			ckq.setYxqz(yxqz);//设置有效期止
			
			ckq.setSzxzqmc(olist.get(11).toString().trim());//设置所在行政区名称
			
			objectDao.save(ckq);
		}
	}
}
