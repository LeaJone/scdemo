package com.fsd.admin.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;
import javax.annotation.Resource;

import com.fsd.core.util.ZipUtil;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.A_template;
import com.fsd.admin.service.A_templateService;
import com.fsd.admin.dao.A_templateDao;

@Service("a_templateServiceImpl")
public class A_templateServiceImpl extends BaseServiceImpl<A_template, String> implements A_templateService{
    
    private static final Logger log = Logger.getLogger(A_templateServiceImpl.class);
    private String depict = "";
    
    @Resource(name = "a_templateDaoImpl")
	public void setBaseDao(A_templateDao A_templateDao) {
		super.setBaseDao(A_templateDao);
	}
	
	@Resource(name = "a_templateDaoImpl")
	private A_templateDao objectDao;
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@Override
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.addOrder(Order.asc("adddate"));
		return objectDao.findPager(param , c);
	}
    
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
    @Override
	public void save(A_template obj, A_Employee employee, String rootPaht) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
            A_template old_obj = objectDao.get(obj.getId());

			old_obj.setCode(obj.getCode());
			old_obj.setName(obj.getName());
			old_obj.setImageurl1(obj.getImageurl1());
			old_obj.setImageurl2(obj.getImageurl2());
			old_obj.setImageurl3(obj.getImageurl3());
			old_obj.setImageurl4(obj.getImageurl4());
			old_obj.setImageurl5(obj.getImageurl5());
			old_obj.setImageurl6(obj.getImageurl6());
			old_obj.setImageurl7(obj.getImageurl7());
			old_obj.setImageurl8(obj.getImageurl8());
			old_obj.setImageurl9(obj.getImageurl9());
			old_obj.setImageurl10(obj.getImageurl10());

			old_obj.setUpdateemployeeid(employee.getId());
			old_obj.setUpdateemployeename(employee.getRealname());
			old_obj.setUpdatedate(this.getData());


			ZipUtil.upzipFile(new File(rootPaht + obj.getPaht()), rootPaht + "/template/" + obj.getCode());
			old_obj.setPaht("/template/" + obj.getCode());
            objectDao.update(old_obj);
        }else{
            //添加
			//设置主键
            obj.setId(this.getUUID());
			obj.setAddemployeeid(employee.getId());
			obj.setAddemployeename(employee.getRealname());
			obj.setAdddate(this.getData());
			obj.setDeleted("0");
			System.out.println(rootPaht + obj.getPaht());
			ZipUtil.upzipFile(new File(rootPaht + obj.getPaht()), rootPaht + "/template/" + obj.getCode());
			obj.setPaht("/template/" + obj.getCode());
			objectDao.save(obj);
        }
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	@Override
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		String hql = "";
		objectDao.executeHql("update A_template t set t.status = -1" + 
				" where t.id in (" + ids + ")");
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	@Override
	public A_template getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		A_template obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		
		return obj;
	}
}
