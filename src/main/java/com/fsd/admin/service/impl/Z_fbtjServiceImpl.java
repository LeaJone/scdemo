package com.fsd.admin.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.fsd.core.common.BusinessException;
import com.fsd.core.model.BaseModel;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Branch;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.B_article;
import com.fsd.admin.service.A_BranchService;
import com.fsd.admin.service.B_articleService;
import com.fsd.admin.service.Z_fbtjService;
import com.fsd.admin.dao.Z_fbtjDao;
import com.google.gson.Gson;

@Repository("z_fbtjServiceImpl")
public class Z_fbtjServiceImpl extends MainServiceImpl<BaseModel, String> implements Z_fbtjService{
    
    private static final Logger log = Logger.getLogger(Z_fbtjServiceImpl.class);
    private String depict = "发布统计";
    
    @Resource(name = "z_fbtjDaoImpl")
	public void setBaseDao(Z_fbtjDao Z_fbtjDao) {
		super.setBaseDao(Z_fbtjDao);
	}
	
	@Resource(name = "z_fbtjDaoImpl")
	private Z_fbtjDao objectDao;

    @Resource(name = "A_BranchServiceImpl")
	private A_BranchService branchService;

    @Resource(name = "b_articleServiceImpl")
	private B_articleService articleService;
	
	/**
	 * 查询SQL(部门)
	 */
	private static String sqlBranch = "select s.name as NAME, " + 
			"(select count(*) from B_article r,A_Branch t  " + 
			"where r.branchid = t.id and t.id = s.id  " + 
			"and r.adddate >= '@#$%0101000000' " + 
			"and r.adddate <= '@#$%0131235959') as JANUARY, " + 
			"(select count(*) from B_article r,A_Branch t  " + 
			"where r.branchid = t.id and t.id = s.id  " + 
			"and r.adddate >= '@#$%0201000000' " + 
			"and r.adddate < '@#$%0301000000') as FEBRUARY, " + 
			"(select count(*) from B_article r,A_Branch t  " + 
			"where r.branchid = t.id and t.id = s.id  " + 
			"and r.adddate >= '@#$%0301000000' " + 
			"and r.adddate <= '@#$%0331235959') as MARCH, " + 
			"(select count(*) from B_article r,A_Branch t  " + 
			"where r.branchid = t.id and t.id = s.id  " + 
			"and r.adddate >= '@#$%0401000000' " + 
			"and r.adddate <= '@#$%0430235959') as APRIL, " + 
			"(select count(*) from B_article r,A_Branch t  " + 
			"where r.branchid = t.id and t.id = s.id  " + 
			"and r.adddate >= '@#$%0501000000' " + 
			"and r.adddate <= '@#$%0531235959') as MAY, " + 
			"(select count(*) from B_article r,A_Branch t  " + 
			"where r.branchid = t.id and t.id = s.id  " + 
			"and r.adddate >= '@#$%0601000000' " + 
			"and r.adddate <= '@#$%0630235959') as JUNE, " + 
			"(select count(*) from B_article r,A_Branch t  " + 
			"where r.branchid = t.id and t.id = s.id  " + 
			"and r.adddate >= '@#$%0701000000' " + 
			"and r.adddate <= '@#$%0731235959') as JULY, " + 
			"(select count(*) from B_article r,A_Branch t  " + 
			"where r.branchid = t.id and t.id = s.id  " + 
			"and r.adddate >= '@#$%0801000000' " + 
			"and r.adddate <= '@#$%0831235959') as AUGUST, " + 
			"(select count(*) from B_article r,A_Branch t  " + 
			"where r.branchid = t.id and t.id = s.id  " + 
			"and r.adddate >= '@#$%0901000000' " + 
			"and r.adddate <= '@#$%0930235959') as SEPTEMBER, " + 
			"(select count(*) from B_article r,A_Branch t  " + 
			"where r.branchid = t.id and t.id = s.id  " + 
			"and r.adddate >= '@#$%1001000000' " + 
			"and r.adddate <= '@#$%1031235959') as OCTOBER, " + 
			"(select count(*) from B_article r,A_Branch t  " + 
			"where r.branchid = t.id and t.id = s.id  " + 
			"and r.adddate >= '@#$%1101000000' " + 
			"and r.adddate <= '@#$%1130235959') as NOVEMBER, " + 
			"(select count(*) from B_article r,A_Branch t  " + 
			"where r.branchid = t.id and t.id = s.id  " + 
			"and r.adddate >= '@#$%1201000000' " + 
			"and r.adddate <= '@#$%1231235959') as DECEMBER, " + 
			"(select count(*) from B_article r,A_Branch t  " + 
			"where r.branchid = t.id and t.id = s.id  " + 
			"and r.adddate >= '@#$%0101000000' " + 
			"and r.adddate <= '@#$%1231235959') as NLJ " + 
			"from A_Branch s where s.id='%$#@' ";
	
	/**
	 * 统计查询(部门)
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, Object>> getObjectStat(ParametersUtil param, A_Employee employee) throws Exception{
		if(param.getJsonData() == null || "".equals(param.getJsonData())){
			throw new BusinessException(depict + "参数不能为空!");
		}
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
		if(objectMap.get("year") == null && objectMap.get("year").equals("")){
			throw new BusinessException(depict + "年份不能为空!");
		}
		if(objectMap.get("branchid") == null && objectMap.get("branchid").equals("")){
			throw new BusinessException(depict + "单位不能为空!");
		}
		return this.getObjectTJBM(objectMap.get("year").toString(), 
				objectMap.get("branchid").toString(), employee.getCompanyid(), false);
	}
	private List<Map<String, Object>> getObjectTJBM(String year, String branchid, String companyid, boolean isQY) throws Exception{
		List<Map<String, Object>> statList = new ArrayList<Map<String, Object>>();
		
		A_Branch branch = branchService.getObjectStructByID(branchid, companyid);
		if (branch.getId().equals(companyid)){
			this.getObjectStatChild(statList, branch.getList(), year, 0, isQY);
		}else{
			List<Object> rowList = new ArrayList<Object>();
			rowList.add(branch);
			this.getObjectStatChild(statList, rowList, year, 0, isQY);
		}
		Map<String, Object> sList = null;
		int January = 0;
		int February = 0;
		int March = 0;
		int April = 0;
		int May = 0;
		int June = 0;
		int July = 0;
		int August = 0;
		int September = 0;
		int October = 0;
		int November = 0;
		int December = 0;
		int nlj = 0;
		for (Object obj : statList) {
			sList = (Map<String, Object>)obj;
			January += Integer.valueOf(sList.get("January").toString());
			February += Integer.valueOf(sList.get("February").toString());
			March += Integer.valueOf(sList.get("March").toString());
			April += Integer.valueOf(sList.get("April").toString());
			May += Integer.valueOf(sList.get("May").toString());
			June += Integer.valueOf(sList.get("June").toString());
			July += Integer.valueOf(sList.get("July").toString());
			August += Integer.valueOf(sList.get("August").toString());
			September += Integer.valueOf(sList.get("September").toString());
			October += Integer.valueOf(sList.get("October").toString());
			November += Integer.valueOf(sList.get("November").toString());
			December += Integer.valueOf(sList.get("December").toString());
			nlj += Integer.valueOf(sList.get("nlj").toString());
		}
		sList = new HashMap<String, Object>();
		sList.put("name", "月累计");
		sList.put("January", January);
		sList.put("February", February);
		sList.put("March", March);
		sList.put("April", April);
		sList.put("May", May);
		sList.put("June", June);
		sList.put("July", July);
		sList.put("August", August);
		sList.put("September", September);
		sList.put("October", October);
		sList.put("November", November);
		sList.put("December", December);
		sList.put("nlj", nlj);
		statList.add(sList);
		return statList;
	}
	private void getObjectStatChild(List<Map<String, Object>> statList, List<Object> rowList, 
			String year, int num, boolean isQY){
		A_Branch branch = null;
		String sql = "";
		String sp = "";
		for (int i = 0; i < num; i++) {
			sp += "　";
		}
		if (!sp.equals("")){
			sp += "|-";
		}
		List<Object> valueList = null;
		Map vList = null;
		Map<String, Object> sList = null;
		for (int i = 0; i < rowList.size(); i++) {
			branch = (A_Branch)rowList.get(i);
			if (isQY){
				if (!branch.getStatuscode().equals(A_BranchServiceImpl.jgztqy1)){
					continue;
				}
			}
			sql = this.sqlBranch.replace("@#$%", year);
			sql = sql.replace("%$#@", branch.getId());
			valueList = objectDao.queryBySql(sql); 
			vList = (Map)valueList.get(0);
			sList = new HashMap<String, Object>();
			sList.put("id", branch.getId());
			sList.put("name", sp + vList.get("NAME").toString());
			sList.put("January", vList.get("JANUARY").toString());
			sList.put("February", vList.get("FEBRUARY").toString());
			sList.put("March", vList.get("MARCH").toString());
			sList.put("April", vList.get("APRIL").toString());
			sList.put("May", vList.get("MAY").toString());
			sList.put("June", vList.get("JUNE").toString());
			sList.put("July", vList.get("JULY").toString());
			sList.put("August", vList.get("AUGUST").toString());
			sList.put("September", vList.get("SEPTEMBER").toString());
			sList.put("October", vList.get("OCTOBER").toString());
			sList.put("November", vList.get("NOVEMBER").toString());
			sList.put("December", vList.get("DECEMBER").toString());
			sList.put("nlj", vList.get("NLJ").toString());
			statList.add(sList);
			if (branch.getList() != null && branch.getList().size() > 0){
				this.getObjectStatChild(statList, branch.getList(), year, num + 1, isQY);
			}
		}
	}
	
	/**
	 * 下载统计查询(部门)
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, Object>> getDownLoadByBranch(ParametersUtil param, A_Employee employee) throws Exception{
		List<Map<String, Object>> statList = new ArrayList<Map<String, Object>>();
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
		String year = objectMap.get("year").toString();
		String branchid = objectMap.get("branch").toString();
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		if(dir.size() > 0 ){
			for(int i = 0; i < dir.size(); i++){
				A_Branch branch = branchService.getObjectStructByID(dir.get(i), employee.getCompanyid());
				List<Object> rowList = new ArrayList<Object>();
				rowList.add(branch);
				this.getBMXZ(statList, rowList, year, 0, false);
			}
		}else{
			A_Branch branch = branchService.getObjectStructByID(branchid, employee.getCompanyid());
			this.getBMXZ(statList, branch.getList(), year, 0, true);
		}
		Map<String, Object> sList = null;
		int January = 0;
		int February = 0;
		int March = 0;
		int April = 0;
		int May = 0;
		int June = 0;
		int July = 0;
		int August = 0;
		int September = 0;
		int October = 0;
		int November = 0;
		int December = 0;
		int nlj = 0;
		for (Object obj : statList) {
			sList = (Map<String, Object>)obj;
			January += Integer.valueOf(sList.get("January").toString());
			February += Integer.valueOf(sList.get("February").toString());
			March += Integer.valueOf(sList.get("March").toString());
			April += Integer.valueOf(sList.get("April").toString());
			May += Integer.valueOf(sList.get("May").toString());
			June += Integer.valueOf(sList.get("June").toString());
			July += Integer.valueOf(sList.get("July").toString());
			August += Integer.valueOf(sList.get("August").toString());
			September += Integer.valueOf(sList.get("September").toString());
			October += Integer.valueOf(sList.get("October").toString());
			November += Integer.valueOf(sList.get("November").toString());
			December += Integer.valueOf(sList.get("December").toString());
			nlj += Integer.valueOf(sList.get("nlj").toString());
		}
		sList = new HashMap<String, Object>();
		sList.put("name", "月累计");
		sList.put("January", January);
		sList.put("February", February);
		sList.put("March", March);
		sList.put("April", April);
		sList.put("May", May);
		sList.put("June", June);
		sList.put("July", July);
		sList.put("August", August);
		sList.put("September", September);
		sList.put("October", October);
		sList.put("November", November);
		sList.put("December", December);
		sList.put("nlj", nlj);
		statList.add(sList);
		return statList;
	}
	private void getBMXZ(List<Map<String, Object>> statList, List<Object> rowList, 
			String year, int num, boolean isChild){
		A_Branch branch = null;
		String sql = "";
		String sp = "";
		for (int i = 0; i < num; i++) {
			sp += "　";
		}
		if (!sp.equals("")){
			sp += "|-";
		}
		List<Object> valueList = null;
		Map vList = null;
		Map<String, Object> sList = null;
		for (int i = 0; i < rowList.size(); i++) {
			branch = (A_Branch)rowList.get(i);
			sql = this.sqlBranch.replace("@#$%", year);
			sql = sql.replace("%$#@", branch.getId());
			valueList = objectDao.queryBySql(sql); 
			vList = (Map)valueList.get(0);
			sList = new HashMap<String, Object>();
			sList.put("id", branch.getId());
			sList.put("name", sp + vList.get("NAME").toString());
			sList.put("January", vList.get("JANUARY").toString());
			sList.put("February", vList.get("FEBRUARY").toString());
			sList.put("March", vList.get("MARCH").toString());
			sList.put("April", vList.get("APRIL").toString());
			sList.put("May", vList.get("MAY").toString());
			sList.put("June", vList.get("JUNE").toString());
			sList.put("July", vList.get("JULY").toString());
			sList.put("August", vList.get("AUGUST").toString());
			sList.put("September", vList.get("SEPTEMBER").toString());
			sList.put("October", vList.get("OCTOBER").toString());
			sList.put("November", vList.get("NOVEMBER").toString());
			sList.put("December", vList.get("DECEMBER").toString());
			sList.put("nlj", vList.get("NLJ").toString());
			statList.add(sList);
			if (isChild){
				if (branch.getList() != null && branch.getList().size() > 0){
					this.getBMXZ(statList, branch.getList(), year, num + 1, true);
				}
			}
		}
	}
	
	/**
	 * 生成下载文件(部门)
	 */
	public Map<String, String> download(ParametersUtil parameters, A_Employee employee, String rootPath, String httpRootPath) throws Exception {
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		List<Map<String, Object>> list = this.getDownLoadByBranch(parameters, employee);
		String path = "uploadfiles/temp/"+this.getUUID()+".xls";
		String url = rootPath+path;
		String httpUrl = httpRootPath+path;
		this.createExcel(list,url);
		Map<String, String> map = new HashMap<String, String>();
		map.put("httpUrl", httpUrl);
		return map;
	}
	
	/**
	 * 生成Excel文件
	 * @param list
	 * @param url
	 * @param sheetName
	 * @throws Exception
	 */
	public void createExcel(List<Map<String, Object>> list, String url) throws Exception{
		WritableWorkbook book = Workbook.createWorkbook(new File(url)); //创建可写入的 Excel 工作薄
		WritableCellFormat format = new WritableCellFormat();
		WritableFont font = new WritableFont(WritableFont.ARIAL,16,WritableFont.BOLD); //字形
		format.setFont(font);
		format.setAlignment(Alignment.CENTRE);
		format.setVerticalAlignment(VerticalAlignment.CENTRE);
		format.setBorder(jxl.format.Border.ALL,jxl.format.BorderLineStyle.THIN);  //边框
		
		WritableCellFormat cellformat = new WritableCellFormat();
		cellformat.setBorder(jxl.format.Border.ALL,jxl.format.BorderLineStyle.THIN);
		cellformat.setWrap(true);  //设置单元格自适应列宽
		
		WritableCellFormat headerformat = new WritableCellFormat();
		WritableFont hFont = new WritableFont(WritableFont.ARIAL,WritableFont.DEFAULT_POINT_SIZE,WritableFont.BOLD); //字形加粗
		headerformat.setBorder(jxl.format.Border.ALL,jxl.format.BorderLineStyle.THIN);
		headerformat.setFont(hFont);
		
		WritableSheet sheet = book.createSheet("发布统计（按单位或部门）", 0);

		sheet.mergeCells(0,0,14,0); //a:最左上的列，b最左上的行，c最右下的列，d最右下的行 （合并单元格）
		sheet.getSettings().setDefaultColumnWidth(15);
		sheet.setColumnView(0,10);  //单独设置列宽 （0代表第几列，10代表宽度）
		sheet.setColumnView(1,40);  //单独设置列宽 （1代表第几列，40代表宽度）
		Label label = new Label(0, 0, "按单位或部门统计发布数据", format);
		sheet.addCell(label);
		for (int i = 0; i < 15; i++) {
			Label label_tem = null;
			switch (i) {
			case 0:
				label_tem = new Label(i, 1, "",headerformat);
				break;
			case 1:
				label_tem = new Label(i, 1, "名称",headerformat);
				break;
			case 2:
				label_tem = new Label(i, 1, "一月",headerformat);
				break;
			case 3:
				label_tem = new Label(i, 1, "二月",headerformat);
				break;
			case 4:
				label_tem = new Label(i, 1, "三月",headerformat);
				break;
			case 5:
				label_tem = new Label(i, 1, "四月",headerformat);
				break;
			case 6:
				label_tem = new Label(i, 1, "五月",headerformat);
				break;
			case 7:
				label_tem = new Label(i, 1, "六月",headerformat);
				break;
			case 8:
				label_tem = new Label(i, 1, "七月",headerformat);
				break;
			case 9:
				label_tem = new Label(i, 1, "八月",headerformat);
				break;
			case 10:
				label_tem = new Label(i, 1, "九月",headerformat);
				break;
			case 11:
				label_tem = new Label(i, 1, "十月",headerformat);
				break;
			case 12:
				label_tem = new Label(i, 1, "十一月",headerformat);
				break;
			case 13:
				label_tem = new Label(i, 1, "十二月",headerformat);
				break;
			case 14:
				label_tem = new Label(i, 1, "年累计",headerformat);
				break;
			default:
				break;
			}
			sheet.addCell(label_tem);
		}
		for (int j = 0; j < list.size(); j++){
			sheet.addCell(new Label(0,j+2, j+1+"",cellformat));
			sheet.addCell(new Label(1,j+2, ((Map)list.get(j)).get("name").toString(),cellformat));
			sheet.addCell(new Label(2,j+2, ((Map)list.get(j)).get("January").toString(),cellformat));
			sheet.addCell(new Label(3,j+2, ((Map)list.get(j)).get("February").toString(),cellformat));
			sheet.addCell(new Label(4,j+2, ((Map)list.get(j)).get("March").toString(),cellformat));
			sheet.addCell(new Label(5,j+2, ((Map)list.get(j)).get("April").toString(),cellformat));
			sheet.addCell(new Label(6,j+2, ((Map)list.get(j)).get("May").toString(),cellformat));
			sheet.addCell(new Label(7,j+2, ((Map)list.get(j)).get("June").toString(),cellformat));
			sheet.addCell(new Label(8,j+2, ((Map)list.get(j)).get("July").toString(),cellformat));
			sheet.addCell(new Label(9,j+2, ((Map)list.get(j)).get("August").toString(),cellformat));
			sheet.addCell(new Label(10,j+2, ((Map)list.get(j)).get("September").toString(),cellformat));
			sheet.addCell(new Label(11,j+2, ((Map)list.get(j)).get("October").toString(),cellformat));
			sheet.addCell(new Label(12,j+2, ((Map)list.get(j)).get("November").toString(),cellformat));
			sheet.addCell(new Label(13,j+2, ((Map)list.get(j)).get("December").toString(),cellformat));
			sheet.addCell(new Label(14,j+2, ((Map)list.get(j)).get("nlj").toString(),cellformat));
		}
		book.write();
		book.close();
	}
		/**
		 * 查询SQL(作者)
		 */
		private static String sqlAuthor = "select '#BranchName#' as Branchname,'#Author#' as Name,"+
						"(select count(*) from B_article r "+
                        "where r.author = '#Author#'"+
						"and r.adddate >= '@#$%0101000000'"+
						"and r.adddate <= '@#$%0131235959') as January,"+
						"(select count(*) from B_article r "+
                        "where r.author = '#Author#'"+
						"and r.adddate >= '@#$%0201000000'"+
						"and r.adddate < '@#$%0301000000') as February,"+
						"(select count(*) from B_article r "+
                        "where r.author = '#Author#'"+
						"and r.adddate >= '@#$%0301000000'"+
						"and r.adddate <= '@#$%0331235959') as March,"+
						"(select count(*) from B_article r "+
                        "where r.author = '#Author#'"+
						"and r.adddate >= '@#$%0401000000'"+
						"and r.adddate <= '@#$%0430235959') as April,"+
						"(select count(*) from B_article r "+
                        "where r.author = '#Author#'"+
						"and r.adddate >= '@#$%0501000000'"+
						"and r.adddate <= '@#$%0531235959') as May,"+
						"(select count(*) from B_article r "+
                        "where r.author = '#Author#'"+
						"and r.adddate >= '@#$%0601000000'"+
						"and r.adddate <= '@#$%0630235959') as June,"+
						"(select count(*) from B_article r "+
                        "where r.author = '#Author#'"+
						"and r.adddate >= '@#$%0701000000'"+
						"and r.adddate <= '@#$%0731235959') as July,"+
						"(select count(*) from B_article r "+
                        "where r.author = '#Author#'"+
						"and r.adddate >= '@#$%0801000000'"+
						"and r.adddate <= '@#$%0831235959') as August,"+
						"(select count(*) from B_article r "+
                        "where r.author = '#Author#'"+
						"and r.adddate >= '@#$%0901000000'"+
						"and r.adddate <= '@#$%0930235959') as September,"+
						"(select count(*) from B_article r "+
                        "where r.author = '#Author#'"+
						"and r.adddate >= '@#$%1001000000'"+
						"and r.adddate <= '@#$%1031235959') as October,"+
						"(select count(*) from B_article r "+
                        "where r.author = '#Author#'"+
						"and r.adddate >= '@#$%1101000000'"+
						"and r.adddate <= '@#$%1130235959') as November,"+
						"(select count(*) from B_article r "+
                        "where r.author = '#Author#'"+
						"and r.adddate >= '@#$%1201000000'"+
						"and r.adddate <= '@#$%1231235959') as December,"+
						"(select count(*) from B_article r "+
                        "where r.author = '#Author#'"+
						"and r.adddate >= '@#$%0101000000'"+
						"and r.adddate <= '@#$%1231235959') as nlj"+
						" from dual";
		
	
	
	/**
	 * 统计查询(作者)
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, Object>> getObjectStatAuthor(ParametersUtil param, A_Employee employee) throws Exception{
		List<Map<String, Object>> statList = new ArrayList<Map<String, Object>>();
		String year = "";
		String author = "";
		String sql = "";
		if(param.getJsonData() == null || "".equals(param.getJsonData())){
			throw new BusinessException(depict + "参数不能为空!");
		}
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
		if(objectMap.get("year") == null && year.equals("")){
			throw new BusinessException(depict + "年份不能为空!");
		}
		year = objectMap.get("year").toString();
		author = objectMap.get("Author").toString();
		sql = "select distinct"
				+ " a.author, a.companyid, b.id branchid, b.name branchname from b_article a, a_branch b"
				+ " where a.companyid = '"+ employee.getCompanyid() +"' and a.branchid = b.id and a.author is not null"
				+ " and a.author like '%"+author+"%' and a.adddate>='"+year+"0101000000'and"
				+ " a.adddate<='"+year+"1231235959' order by b.name, a.author";
		if(objectMap.get("nir") != null && !objectMap.get("nir").equals("") && objectMap.get("bir") != null && !objectMap.get("bir").equals("")){
			ArrayList<String> nir = (ArrayList<String>) objectMap.get("nir");
			String names = this.getSQLinByIDList(nir);
			ArrayList<String> bir = (ArrayList<String>) objectMap.get("bir");
			String branchids = this.getSQLinByIDList(bir);
			if(nir.size() > 0 && bir.size() > 0){
				sql = "select distinct"
						+ " a.author, a.companyid, b.id branchid, b.name branchname from b_article a, a_branch b"
						+ " where a.companyid = '"+ employee.getCompanyid() +"' and a.branchid = b.id and a.author is not null"
						+ " and a.author in ("+names+") and a.branchid in ("+branchids+") and a.adddate>='"+year+"0101000000'and"
						+ " a.adddate<='"+year+"1231235959' order by b.name, a.author";
			}else{
				sql = "select distinct"
						+ " a.author, a.companyid, b.id branchid, b.name branchname from b_article a, a_branch b"
						+ " where a.companyid = '"+ employee.getCompanyid() +"' and a.branchid = b.id and a.author is not null"
						+ " and a.author like '%"+author+"%' and a.adddate>='"+year+"0101000000'and"
						+ " a.adddate<='"+year+"1231235959' order by b.name, a.author";
			}
		}
		List<Object> rowList = objectDao.queryBySql(sql); 
		this.getObjectStatChildZz(statList, rowList, year);
		
		Map<String, Object> sList = null;
		int january = 0;
		int february = 0;
		int march = 0;
		int april = 0;
		int may = 0;
		int june = 0;
		int july = 0;
		int august = 0;
		int september = 0;
		int october = 0;
		int november = 0;
		int december = 0;
		int nlj = 0;
		for (Object obj : statList) {
			sList = (Map<String, Object>)obj;
			january = january + Integer.valueOf(sList.get("January").toString());
			february += Integer.valueOf(sList.get("February").toString());
			march += Integer.valueOf(sList.get("March").toString());
			april += Integer.valueOf(sList.get("April").toString());
			may += Integer.valueOf(sList.get("May").toString());
			june += Integer.valueOf(sList.get("June").toString());
			july += Integer.valueOf(sList.get("July").toString());
			august += Integer.valueOf(sList.get("August").toString());
			september += Integer.valueOf(sList.get("September").toString());
			october += Integer.valueOf(sList.get("October").toString());
			november += Integer.valueOf(sList.get("November").toString());
			december += Integer.valueOf(sList.get("December").toString());
			nlj += Integer.valueOf(sList.get("nlj").toString());
		}
		sList = new HashMap<String, Object>();
		sList.put("BranchName", "");
		sList.put("name", "月累计");
		sList.put("January", january);
		sList.put("February", february);
		sList.put("March", march);
		sList.put("April", april);
		sList.put("May", may);
		sList.put("June", june);
		sList.put("July", july);
		sList.put("August", august);
		sList.put("September", september);
		sList.put("October", october);
		sList.put("November", november);
		sList.put("December", december);
		sList.put("nlj", nlj);
		statList.add(sList);
		return statList;
	}
	private void getObjectStatChildZz(List<Map<String, Object>> statList, List<Object> rowList, String year){
		String sql = "";
		List<Object> valueList = null;
		Map vList = null;
		Map<String, Object> sList = null;
		Map<String, Object> vMap = null;
		for (int i = 0; i < rowList.size(); i++) {
			vMap = (Map)rowList.get(i);
			sql = this.sqlAuthor.replace("@#$%", year).replace("#BranchID#", vMap.get("BRANCHID").toString()).replace("#Author#", vMap.get("AUTHOR").toString()).replace("#BranchName#", vMap.get("BRANCHNAME").toString());
			valueList = objectDao.queryBySql(sql); 
			vList = (Map)valueList.get(0);
			sList = new HashMap<String, Object>();
			sList.put("BranchID", vMap.get("BRANCHID").toString());
			sList.put("BranchName", vList.get("BRANCHNAME").toString());
			sList.put("name",   vList.get("NAME").toString());
			sList.put("January", vList.get("JANUARY").toString());
			sList.put("February", vList.get("FEBRUARY").toString());
			sList.put("March", vList.get("MARCH").toString());
			sList.put("April", vList.get("APRIL").toString());
			sList.put("May", vList.get("MAY").toString());
			sList.put("June", vList.get("JUNE").toString());
			sList.put("July", vList.get("JULY").toString());
			sList.put("August", vList.get("AUGUST").toString());
			sList.put("September", vList.get("SEPTEMBER").toString());
			sList.put("October", vList.get("OCTOBER").toString());
			sList.put("November", vList.get("NOVEMBER").toString());
			sList.put("December", vList.get("DECEMBER").toString());
			sList.put("nlj", vList.get("NLJ").toString());
			statList.add(sList);
		}
	}
	


	/**
	 * 生成下载文件(作者)
	 */
	public Map<String, String> downloadAuthor(ParametersUtil parameters, A_Employee employee, String rootPath, String httpRootPath) throws Exception {
		List<Map<String, Object>> list = this.getObjectStatAuthor(parameters, employee);
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		String path = "uploadfiles/temp/"+this.getUUID()+".xls";
		String url = rootPath+path;
		String httpUrl = httpRootPath+path;
			this.createExcelAuthor(list, url,  "作者统计", objectMap.get("year").toString());
		Map<String, String> map = new HashMap<String, String>();
		map.put("httpUrl", httpUrl);
		return map;
	}

	/**
	 * 生成Excel文件(作者)
	 * @param list
	 * @param order
	 * @param url
	 * @param sheetName
	 * @throws Exception
	 */
	public void createExcelAuthor(List<Map<String, Object>> list, String url, String sheetName, String year) throws Exception{
		WritableWorkbook book = Workbook.createWorkbook(new File(url)); //创建可写入的 Excel 工作薄
		WritableSheet sheet = book.createSheet(sheetName, 0); // 创建 Excel 工作表
		
		sheet.mergeCells(0,0,14,0); //a:最左上的列，b最左上的行，c最右下的列，d最右下的行
		
		WritableFont font1 = new WritableFont(WritableFont.ARIAL,16,WritableFont.BOLD); //字形
		WritableCellFormat format = new WritableCellFormat();
		format.setFont(font1);
		format.setAlignment(Alignment.CENTRE);
		format.setVerticalAlignment(VerticalAlignment.CENTRE);
		Label label = new Label(0, 0, "文章发布作者统计表", format);
		sheet.addCell(label);
		
		sheet.getSettings().setDefaultColumnWidth(20);
		sheet.setColumnView(0,10);  //单独设置列宽 （0代表第几列，10代表宽度）
		
		sheet.mergeCells(8,1,14,1); //a:最左上的列，b最左上的行，c最右下的列，d最右下的行
		format = new WritableCellFormat();
		format.setAlignment(Alignment.RIGHT);
		Label label2 = new Label(8, 1, "时间：" + year + "年", format);
		sheet.addCell(label2);
		
		for (int i = 0; i < 16; i++) {
			Label label_tem = null;
			switch (i) {
			case 0:
				label_tem = new Label(i, 2, "编号");
				break;
			case 1:
				label_tem = new Label(i, 2, "单位名称");
				break;
			case 2:
				label_tem = new Label(i, 2, "作者名称");
				break;
			case 3:
				label_tem = new Label(i, 2, "一月");
				break;
			case 4:
				label_tem = new Label(i, 2, "二月");
				break;
			case 5:
				label_tem = new Label(i, 2, "三月");
				break;
			case 6:
				label_tem = new Label(i, 2, "四月");
				break;
			case 7:
				label_tem = new Label(i, 2, "五月");
				break;
			case 8:
				label_tem = new Label(i, 2, "六月");
				break;
			case 9:
				label_tem = new Label(i, 2, "七月");
				break;
			case 10:
				label_tem = new Label(i, 2, "八月");
				break;
			case 11:
				label_tem = new Label(i, 2, "九月");
				break;
			case 12:
				label_tem = new Label(i, 2, "十月");
				break;
			case 13:
				label_tem = new Label(i, 2, "十一月");
				break;
			case 14:
				label_tem = new Label(i, 2, "十二月");
				break;
			case 15:
				label_tem = new Label(i, 2, "年累计");
				break;
			default:
				break;
			}
			sheet.addCell(label_tem);
		}
		for (int j = 0; j < list.size(); j++) {
			Label ca_tem1 = new Label(0, j+3, j+1+"");
			Label ca_tem2 = new Label(1, j+3, ((Map)list.get(j)).get("BranchName").toString());
			Label ca_tem3 = new Label(2, j+3, ((Map)list.get(j)).get("name").toString());
			Label ca_tem4 = new Label(3, j+3, ((Map)list.get(j)).get("January").toString());
			Label ca_tem5 = new Label(4, j+3, ((Map)list.get(j)).get("February").toString());
			Label ca_tem6 = new Label(5, j+3, ((Map)list.get(j)).get("March").toString());
			Label ca_tem7 = new Label(6, j+3, ((Map)list.get(j)).get("April").toString());
			Label ca_tem8 = new Label(7, j+3, ((Map)list.get(j)).get("May").toString());
			Label ca_tem9 = new Label(8, j+3, ((Map)list.get(j)).get("June").toString());
			Label ca_tem10 = new Label(9, j+3, ((Map)list.get(j)).get("July").toString());
			Label ca_tem11 = new Label(10, j+3, ((Map)list.get(j)).get("August").toString());
			Label ca_tem12 = new Label(11, j+3, ((Map)list.get(j)).get("September").toString());
			Label ca_tem13 = new Label(12, j+3, ((Map)list.get(j)).get("October").toString());
			Label ca_tem14 = new Label(13, j+3, ((Map)list.get(j)).get("November").toString());
			Label ca_tem15 = new Label(14, j+3, ((Map)list.get(j)).get("December").toString());
			Label ca_tem16 = new Label(15, j+3, ((Map)list.get(j)).get("nlj").toString());
			sheet.addCell(ca_tem1);
			sheet.addCell(ca_tem2);
			sheet.addCell(ca_tem3);
			sheet.addCell(ca_tem4);
			sheet.addCell(ca_tem5);
			sheet.addCell(ca_tem6);
			sheet.addCell(ca_tem7);
			sheet.addCell(ca_tem8);
			sheet.addCell(ca_tem9);
			sheet.addCell(ca_tem10);
			sheet.addCell(ca_tem11);
			sheet.addCell(ca_tem12);
			sheet.addCell(ca_tem13);
			sheet.addCell(ca_tem14);
			sheet.addCell(ca_tem15);
			sheet.addCell(ca_tem16);
		}
		book.write();
		book.close();
	}
	
	/**
	 * 统计查询(发布登记部门)
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, Object>> getObjectStatFb(ParametersUtil param, A_Employee employee) throws Exception{
		List<Map<String, Object>> statList = new ArrayList<Map<String, Object>>();
		String startYear = "";
		String startMonth = "";
		String endYear = "";
		String endMonth = "";
		String subid = "";
		String sql = "";
		if(param.getJsonData() == null || "".equals(param.getJsonData())){
			throw new BusinessException(depict + "参数不能为空!");
		}
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
		if(objectMap.get("startYear") == null || "".equals(objectMap.get("startYear"))){
			throw new BusinessException(depict + "起始年份不能为空!");
		}
		if(objectMap.get("startMonth") == null || "".equals(objectMap.get("startMonth"))){
			throw new BusinessException(depict + "起始月份不能为空!");
		}
		if(objectMap.get("endYear") == null || "".equals(objectMap.get("endYear"))){
			throw new BusinessException(depict + "终止年份不能为空!");
		}
		if(objectMap.get("endMonth") == null || "".equals(objectMap.get("endMonth"))){
			throw new BusinessException(depict + "终止月份不能为空!");
		}
		if(objectMap.get("branchid") == null || "".equals(objectMap.get("branchid"))){
			throw new BusinessException(depict + "单位不能为空!");
		}
		startYear = objectMap.get("startYear").toString();
		startMonth = objectMap.get("startMonth").toString();
		endYear = objectMap.get("endYear").toString();
		endMonth = objectMap.get("endMonth").toString();
		subid = objectMap.get("branchid").toString();
		
		if (subid.equals(employee.getCompanyid())){
			sql = "select title, id, subjectname, adddate, addemployeename, typename, wordnum, imagenum from b_article where"
					+ " adddate between '"+(startYear+startMonth+"00000000")+"' and '"+(endYear+endMonth+"31000000")+"' and companyid ='"+employee.getCompanyid()+"' and deleted='0'";
		}else{
			List<String> list = branchService.getObjectStructIDList(subid, employee.getCompanyid());
			String ids = this.getSQLinByIDList(list);
			sql = "select title, id, subjectname, adddate, addemployeename, typename, wordnum, imagenum from b_article where"
					+ " adddate between '"+(startYear+startMonth+"00000000")+"' and '"+(endYear+endMonth+"31000000")+"' and branchid in ("+ids+") and companyid ='"+employee.getCompanyid()+"' and deleted='0'";
		}
		List<Object> valueList = objectDao.queryBySql(sql);
		
		Map vList = null;
		Map<String, Object> sList = null;
		
		for (int i = 0; i < valueList.size(); i++) {
			vList = (Map)valueList.get(i);
			
			sList = new HashMap<String, Object>();
			
			sList.put("title", vList.get("TITLE").toString());
			sList.put("id", vList.get("ID").toString());
			sList.put("typename", vList.get("TYPENAME").toString());
			if( vList.get("WORDNUM") == null){
				sList.put("wordnum", "");
			}else{
				sList.put("wordnum", vList.get("WORDNUM").toString());
			}
			if(vList.get("IMAGENUM") == null){
				sList.put("imagenum", "");
			}else{
				sList.put("imagenum", vList.get("IMAGENUM").toString());
			}
			if(vList.get("SUBJECTNAME") == null){
				sList.put("subjectname", "");
			}else{
				sList.put("subjectname", vList.get("SUBJECTNAME").toString());
			}
			sList.put("adddate", vList.get("ADDDATE").toString());
			sList.put("addemployeename", vList.get("ADDEMPLOYEENAME").toString());
			statList.add(sList);
		}
		return statList;
	}
	
	/**
	 * 下载查询(发布登记部门)
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, Object>> getDownloadByFb(ParametersUtil param, A_Employee employee) throws Exception{
		List<Map<String, Object>> statList = new ArrayList<Map<String, Object>>();
		String startYear = "";
		String startMonth = "";
		String endYear = "";
		String endMonth = "";
		String subid = "";
		String sql = "";
		if(param.getJsonData() == null || "".equals(param.getJsonData())){
			throw new BusinessException(depict + "参数不能为空!");
		}
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
		if(objectMap.get("startYear") == null || "".equals(objectMap.get("startYear"))){
			throw new BusinessException(depict + "起始年份不能为空!");
		}
		if(objectMap.get("startMonth") == null || "".equals(objectMap.get("startMonth"))){
			throw new BusinessException(depict + "起始月份不能为空!");
		}
		if(objectMap.get("endYear") == null || "".equals(objectMap.get("endYear"))){
			throw new BusinessException(depict + "终止年份不能为空!");
		}
		if(objectMap.get("endMonth") == null || "".equals(objectMap.get("endMonth"))){
			throw new BusinessException(depict + "终止月份不能为空!");
		}
		if(objectMap.get("branchid") == null || "".equals(objectMap.get("branchid"))){
			throw new BusinessException(depict + "单位不能为空!");
		}
		startYear = objectMap.get("startYear").toString();
		startMonth = objectMap.get("startMonth").toString();
		endYear = objectMap.get("endYear").toString();
		endMonth = objectMap.get("endMonth").toString();
		subid = objectMap.get("branchid").toString();
		
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		if(dir.size() > 0){
			String ids = this.getSQLinByIDList(dir);
			sql = "select title, id, subjectname, adddate, addemployeename, typename, wordnum, imagenum from b_article where"
					+ " adddate between '"+(startYear+startMonth+"00000000")+"' and '"+(endYear+endMonth+"31000000")+"' and id in ("+ids+") and companyid ='"+employee.getCompanyid()+"' and deleted='0'";
		}else{
			List<String> list = branchService.getObjectStructIDList(subid, employee.getCompanyid());
			String ids = this.getSQLinByIDList(list);
			sql = "select title, id, subjectname, adddate, addemployeename, typename, wordnum, imagenum from b_article where"
					+ " adddate between '"+(startYear+startMonth+"00000000")+"' and '"+(endYear+endMonth+"31000000")+"' and branchid in ("+ids+") and companyid ='"+employee.getCompanyid()+"' and deleted='0'";
		}
		List<Object> valueList = objectDao.queryBySql(sql);
		Map vList = null;
		Map<String, Object> sList = null;
		
		for (int i = 0; i < valueList.size(); i++) {
			vList = (Map)valueList.get(i);
			
			sList = new HashMap<String, Object>();
			
			sList.put("title", vList.get("TITLE").toString());
			sList.put("id", vList.get("ID").toString());
			sList.put("typename", vList.get("TYPENAME").toString());
			if( vList.get("WORDNUM") == null){
				sList.put("wordnum", "");
			}else{
				sList.put("wordnum", vList.get("WORDNUM").toString());
			}
			if(vList.get("IMAGENUM") == null){
				sList.put("imagenum", "");
			}else{
				sList.put("imagenum", vList.get("IMAGENUM").toString());
			}
			if(vList.get("SUBJECTNAME") == null){
				sList.put("subjectname", "");
			}else{
				sList.put("subjectname", vList.get("SUBJECTNAME").toString());
			}
			sList.put("adddate", vList.get("ADDDATE").toString());
			sList.put("addemployeename", vList.get("ADDEMPLOYEENAME").toString());
			statList.add(sList);
		}
		return statList;
	}
	
	/**
	 * 生成下载文件(发布登记部门)
	 */
	public Map<String, String> downloadFb(ParametersUtil parameters, A_Employee employee, String rootPath, String httpRootPath) throws Exception {
		List<Map<String, Object>> list = this.getDownloadByFb(parameters, employee);
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		String path = "uploadfiles/temp/"+this.getUUID()+".xls";
		String url = rootPath+path;
		String httpUrl = httpRootPath+path;
		this.createFBDJExcel(list,url);
		Map<String, String> map = new HashMap<String, String>();
		map.put("httpUrl", httpUrl);
		return map;
	}
	
	/**
	 * 生成Excel文件
	 * @param list
	 * @param url
	 * @param sheetName
	 * @throws Exception
	 */
	public void createFBDJExcel(List<Map<String, Object>> list, String url) throws Exception{
		WritableWorkbook book = Workbook.createWorkbook(new File(url)); //创建可写入的 Excel 工作薄
		WritableCellFormat format = new WritableCellFormat();
		WritableFont font = new WritableFont(WritableFont.ARIAL,16,WritableFont.BOLD); //字形
		format.setFont(font);
		format.setAlignment(Alignment.CENTRE);
		format.setVerticalAlignment(VerticalAlignment.CENTRE);
		format.setBorder(jxl.format.Border.ALL,jxl.format.BorderLineStyle.THIN);  //边框
		
		WritableCellFormat cellformat = new WritableCellFormat();
		cellformat.setBorder(jxl.format.Border.ALL,jxl.format.BorderLineStyle.THIN);
		cellformat.setWrap(true);  //设置单元格自适应列宽
		
		WritableCellFormat headerformat = new WritableCellFormat();
		WritableFont hFont = new WritableFont(WritableFont.ARIAL,WritableFont.DEFAULT_POINT_SIZE,WritableFont.BOLD); //字形加粗
		headerformat.setBorder(jxl.format.Border.ALL,jxl.format.BorderLineStyle.THIN);
		headerformat.setFont(hFont);
		
		WritableSheet sheet = book.createSheet("发布登记（按单位或部门）", 0);

		sheet.mergeCells(0,0,8,0); //a:最左上的列，b最左上的行，c最右下的列，d最右下的行 （合并单元格）
		sheet.getSettings().setDefaultColumnWidth(20);
		sheet.setColumnView(0,10);  //单独设置列宽 （0代表第几列，10代表宽度）
		Label label = new Label(0, 0, "发布登记（按单位或部门）", format);
		sheet.addCell(label);
		for (int i = 0; i < 9; i++) {
			Label label_tem = null;
			switch (i) {
			case 0:
				label_tem = new Label(i, 1, "编号");
				break;
			case 1:
				label_tem = new Label(i, 1, "标题");
				break;
			case 2:
				label_tem = new Label(i, 1, "ID号");
				break;
			case 3:
				label_tem = new Label(i, 1, "信息类别");
				break;
			case 4:
				label_tem = new Label(i, 1, "字数");
				break;
			case 5:
				label_tem = new Label(i, 1, "照片");
				break;
			case 6:
				label_tem = new Label(i, 1, "放置栏目");
				break;
			case 7:
				label_tem = new Label(i, 1, "提交日期");
				break;
			case 8:
				label_tem = new Label(i, 1, "提交人");
				break;
			default:
				break;
			}
			sheet.addCell(label_tem);
		}
		for (int j = 0; j < list.size(); j++){
			sheet.addCell(new Label(0,j+2, j+1+"",cellformat));
			sheet.addCell(new Label(1,j+2, ((Map)list.get(j)).get("title").toString(),cellformat));
			sheet.addCell(new Label(2,j+2, ((Map)list.get(j)).get("id").toString(),cellformat));
			sheet.addCell(new Label(3,j+2, ((Map)list.get(j)).get("typename").toString(),cellformat));
			sheet.addCell(new Label(4,j+2, ((Map)list.get(j)).get("wordnum").toString(),cellformat));
			sheet.addCell(new Label(5,j+2, ((Map)list.get(j)).get("imagenum").toString(),cellformat));
			sheet.addCell(new Label(6,j+2, ((Map)list.get(j)).get("subjectname").toString(),cellformat));
			sheet.addCell(new Label(7,j+2, ((Map)list.get(j)).get("adddate").toString(),cellformat));
			sheet.addCell(new Label(8,j+2, ((Map)list.get(j)).get("addemployeename").toString(),cellformat));
		}
		book.write();
		book.close();
	}
	
	/**
	 * 生成厅机关文章
	 * @throws Exception
	 */
	public void saveScWzTjg(ParametersUtil param, A_Employee employee) throws Exception{
		if(param.getJsonData() == null || "".equals(param.getJsonData())){
			throw new BusinessException(depict + "时间参数不能为空!");
		}
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
		if(objectMap.get("year") == null || "".equals(objectMap.get("year"))){
			throw new BusinessException(depict + "年份不能为空!");
		}
		if(objectMap.get("month") == null || "".equals(objectMap.get("month"))){
			throw new BusinessException(depict + "月份不能为空!");
		}
		
		String year = objectMap.get("year").toString();
		String month = String.valueOf(Integer.valueOf(objectMap.get("month").toString()));
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date()); 
		String id = "STJG" + year + month;
		if (articleService.get(id) != null)
			throw new BusinessException(id + " 厅机关本月统计文章已存在！");
		
		List<Map<String, Object>> objList = this.getObjectTJBM(year, "29", employee.getCompanyid(), true);
        String field = "";
        switch (Integer.valueOf(month)){
            case 1:
                field = "January";
                break;
            case 2:
                field = "February";
                break;
            case 3:
                field = "March";
                break;
            case 4:
                field = "April";
                break;
            case 5:
                field = "May";
                break;
            case 6:
                field = "June";
                break;
            case 7:
                field = "July";
                break;
            case 8:
                field = "August";
                break;
            case 9:
                field = "September";
                break;
            case 10:
                field = "October";
                break;
            case 11:
                field = "November";
                break;
            case 12:
                field = "December";
                break;
        }
        String content = "<P class=MsoNormal style=\"TEXT-ALIGN: left; MARGIN: 0cm 0cm 0pt; LINE-HEIGHT: 150%; TEXT-INDENT: 27pt; mso-char-indent-count: 2.0; mso-pagination: widow-orphan\" align=left></P>" +
                "<P class=MsoNormal style=\"TEXT-ALIGN: left; MARGIN: 0cm 0cm 0pt; LINE-HEIGHT: 150%; TEXT-INDENT: 27pt; mso-char-indent-count: 2.0; mso-pagination: widow-orphan\" align=left><SPAN lang=EN-US style=\"FONT-SIZE: 12pt; FONT-FAMILY: 宋体; COLOR: #474747; LETTER-SPACING: 0.75pt; LINE-HEIGHT: 150%; mso-bidi-font-family: 宋体; mso-font-kerning: 0pt\">" + year + "</SPAN><SPAN style=\"FONT-SIZE: 12pt; FONT-FAMILY: 宋体; COLOR: #474747; LETTER-SPACING: 0.75pt; LINE-HEIGHT: 150%; mso-bidi-font-family: 宋体; mso-font-kerning: 0pt\">年<SPAN lang=EN-US>" + month + "</SPAN>月，";
        content += "国土资源厅机关共发布政务信息<SPAN lang=EN-US>" + objList.get(objList.size() - 1).get(field).toString() + "</SPAN>条。其中";
        for (int i = 0; i < objList.size() - 1; i++)
        {
            content += "，" + objList.get(i).get("name").toString().replace("　", "").replace("|-", "").trim() + "<SPAN lang=EN-US>" + objList.get(i).get(field).toString() + "</SPAN>条";
        }
        content += "。</SPAN><SPAN lang=EN-US style=\"FONT-SIZE: 12pt; FONT-FAMILY: 宋体; LINE-HEIGHT: 150%; mso-bidi-font-family: 宋体; mso-font-kerning: 0pt\"><?xml:namespace prefix = \"o\" ns = \"urn:schemas-microsoft-com:office:office\" /><o:p></o:p></SPAN></P>";
        content += "<P class=MsoNormal style=\"TEXT-ALIGN: left; MARGIN: 0cm 0cm 0pt; mso-pagination: widow-orphan\" align=left></P>" +
                 "<P class=MsoNormal style=\"TEXT-ALIGN: center; MARGIN: 0cm 0cm 0pt; LINE-HEIGHT: 150%; mso-pagination: widow-orphan\" align=center><SPAN style=\"FONT-SIZE: 12pt; FONT-FAMILY: 宋体; COLOR: #474747; LETTER-SPACING: 0.75pt; LINE-HEIGHT: 150%; mso-bidi-font-family: 宋体; mso-font-kerning: 0pt\">甘肃省国土资源网络安全和信息化领导小组办公室<SPAN lang=EN-US><o:p></o:p></SPAN></SPAN></P>" +
                 "<P class=MsoNormal style=\"TEXT-ALIGN: center; MARGIN: 0cm 0cm 0pt; LINE-HEIGHT: 150%; mso-pagination: widow-orphan\" align=center><SPAN lang=EN-US style=\"FONT-SIZE: 12pt; FONT-FAMILY: 宋体; COLOR: #474747; LETTER-SPACING: 0.75pt; LINE-HEIGHT: 150%; mso-bidi-font-family: 宋体; mso-font-kerning: 0pt\"><SPAN style=\"mso-no-proof: yes\">" + 
                 String.valueOf(calendar.get(Calendar.YEAR)) + "</SPAN></SPAN><SPAN style=\"FONT-SIZE: 12pt; FONT-FAMILY: 宋体; COLOR: #474747; LETTER-SPACING: 0.75pt; LINE-HEIGHT: 150%; mso-bidi-font-family: 宋体; mso-font-kerning: 0pt; mso-no-proof: yes\">年<SPAN lang=EN-US>" + 
                 String.valueOf(calendar.get(Calendar.MONTH) + 1) + "</SPAN>月<SPAN lang=EN-US>" + 
                 String.valueOf(calendar.get(Calendar.DATE)) + "</SPAN>日</SPAN></P>" +
                 "<P class=MsoNormal style=\"TEXT-ALIGN: center; MARGIN: 0cm 0cm 0pt; LINE-HEIGHT: 150%; mso-pagination: widow-orphan\" align=center><SPAN style=\"FONT-SIZE: 12pt; FONT-FAMILY: 宋体; COLOR: #474747; LETTER-SPACING: 0.75pt; LINE-HEIGHT: 150%; mso-bidi-font-family: 宋体; mso-font-kerning: 0pt; mso-no-proof: yes\"></SPAN><SPAN lang=EN-US style=\"FONT-SIZE: 12pt; FONT-FAMILY: 宋体; COLOR: #474747; LETTER-SPACING: 0.75pt; LINE-HEIGHT: 150%; mso-bidi-font-family: 宋体; mso-font-kerning: 0pt\"><o:p></o:p></SPAN>&nbsp;</P>";

        B_article obj = new B_article();
		obj.setId(id);
		obj.setCompanyid(employee.getCompanyid());
		obj.setSubjectid("SUBJ92");
		obj.setSubjectname("发布统计");
		obj.setType("wzwz");
		obj.setTypename("文章");
		obj.setTitle(year + "年" + month + "月国土资源厅机关政务信息公开情况公布");
		obj.setShowtitle(obj.getTitle());
		obj.setAbstracts(obj.getTitle());
		obj.setSource("甘肃国土资源厅");
		obj.setContent(content);
		obj.setFirstly("false");
		obj.setSlide("false");
		obj.setShowtype("wzxspt");
		obj.setShowtypename("普通文章");
		obj.setAuditing("false");
		obj.setWordnum(0L);
		obj.setImagenum(0L);
		obj.setBranchid("51");
		obj.setBranchname("信息中心");
		obj.setAdddate(this.getData());
		obj.setAddemployeeid(employee.getId());
		obj.setAddemployeename(employee.getRealname());
		obj.setDeleted("0");
		objectDao.save(obj);
	}
	
	/**
	 * 生成直属事业单位文章
	 * @throws Exception
	 */
	public void saveScWzSydw(ParametersUtil param, A_Employee employee) throws Exception{
		if(param.getJsonData() == null || "".equals(param.getJsonData())){
			throw new BusinessException(depict + "时间参数不能为空!");
		}
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
		if(objectMap.get("year") == null || "".equals(objectMap.get("year"))){
			throw new BusinessException(depict + "年份不能为空!");
		}
		if(objectMap.get("month") == null || "".equals(objectMap.get("month"))){
			throw new BusinessException(depict + "月份不能为空!");
		}
		
		String year = objectMap.get("year").toString();
		String month = String.valueOf(Integer.valueOf(objectMap.get("month").toString()));
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date()); 
		String id = "ZSDW" + year + month;
		if (articleService.get(id) != null)
			throw new BusinessException(id + " 事业单位本月统计文章已存在！");
		
		List<Map<String, Object>> objList = this.getObjectTJBM(year, "52", employee.getCompanyid(), true);
        String field = "";
        switch (Integer.valueOf(month)){
            case 1:
                field = "January";
                break;
            case 2:
                field = "February";
                break;
            case 3:
                field = "March";
                break;
            case 4:
                field = "April";
                break;
            case 5:
                field = "May";
                break;
            case 6:
                field = "June";
                break;
            case 7:
                field = "July";
                break;
            case 8:
                field = "August";
                break;
            case 9:
                field = "September";
                break;
            case 10:
                field = "October";
                break;
            case 11:
                field = "November";
                break;
            case 12:
                field = "December";
                break;
        }
        String content = "<P class=MsoNormal style=\"TEXT-ALIGN: left; MARGIN: 0cm 0cm 0pt; LINE-HEIGHT: 150%; TEXT-INDENT: 27pt; mso-char-indent-count: 2.0; mso-pagination: widow-orphan\" align=left></P>" +
                "<P class=MsoNormal style=\"TEXT-ALIGN: left; MARGIN: 0cm 0cm 0pt; LINE-HEIGHT: 150%; TEXT-INDENT: 27pt; mso-char-indent-count: 2.0; mso-pagination: widow-orphan\" align=left><SPAN lang=EN-US style=\"FONT-SIZE: 12pt; FONT-FAMILY: 宋体; COLOR: #474747; LETTER-SPACING: 0.75pt; LINE-HEIGHT: 150%; mso-bidi-font-family: 宋体; mso-font-kerning: 0pt\">" + year + "</SPAN><SPAN style=\"FONT-SIZE: 12pt; FONT-FAMILY: 宋体; COLOR: #474747; LETTER-SPACING: 0.75pt; LINE-HEIGHT: 150%; mso-bidi-font-family: 宋体; mso-font-kerning: 0pt\">年<SPAN lang=EN-US>" + month + "</SPAN>月，";
        content += "国土资源厅各直属事业单位，配合国土资源厅发布工作与服务信息<SPAN lang=EN-US>" + objList.get(objList.size() - 1).get(field).toString() + "</SPAN>条。其中";
        for (int i = 0; i < objList.size() - 1; i++)
        {
            content += "，" + objList.get(i).get("name").toString().replace("　", "").replace("|-", "").trim() + "<SPAN lang=EN-US>" + objList.get(i).get(field).toString() + "</SPAN>条";
        }
        content += "。</SPAN><SPAN lang=EN-US style=\"FONT-SIZE: 12pt; FONT-FAMILY: 宋体; LINE-HEIGHT: 150%; mso-bidi-font-family: 宋体; mso-font-kerning: 0pt\"><?xml:namespace prefix = \"o\" ns = \"urn:schemas-microsoft-com:office:office\" /><o:p></o:p></SPAN></P>";
        content += "<P class=MsoNormal style=\"TEXT-ALIGN: left; MARGIN: 0cm 0cm 0pt; mso-pagination: widow-orphan\" align=left></P>" +
                 "<P class=MsoNormal style=\"TEXT-ALIGN: center; MARGIN: 0cm 0cm 0pt; LINE-HEIGHT: 150%; mso-pagination: widow-orphan\" align=center><SPAN style=\"FONT-SIZE: 12pt; FONT-FAMILY: 宋体; COLOR: #474747; LETTER-SPACING: 0.75pt; LINE-HEIGHT: 150%; mso-bidi-font-family: 宋体; mso-font-kerning: 0pt\">甘肃省国土资源网络安全和信息化领导小组办公室<SPAN lang=EN-US><o:p></o:p></SPAN></SPAN></P>" +
                 "<P class=MsoNormal style=\"TEXT-ALIGN: center; MARGIN: 0cm 0cm 0pt; LINE-HEIGHT: 150%; mso-pagination: widow-orphan\" align=center><SPAN lang=EN-US style=\"FONT-SIZE: 12pt; FONT-FAMILY: 宋体; COLOR: #474747; LETTER-SPACING: 0.75pt; LINE-HEIGHT: 150%; mso-bidi-font-family: 宋体; mso-font-kerning: 0pt\"><SPAN style=\"mso-no-proof: yes\">" + 
                 String.valueOf(calendar.get(Calendar.YEAR)) + "</SPAN></SPAN><SPAN style=\"FONT-SIZE: 12pt; FONT-FAMILY: 宋体; COLOR: #474747; LETTER-SPACING: 0.75pt; LINE-HEIGHT: 150%; mso-bidi-font-family: 宋体; mso-font-kerning: 0pt; mso-no-proof: yes\">年<SPAN lang=EN-US>" + 
                 String.valueOf(calendar.get(Calendar.MONTH) + 1) + "</SPAN>月<SPAN lang=EN-US>" + 
                 String.valueOf(calendar.get(Calendar.DATE)) + "</SPAN>日</SPAN></P>" +
                 "<P class=MsoNormal style=\"TEXT-ALIGN: center; MARGIN: 0cm 0cm 0pt; LINE-HEIGHT: 150%; mso-pagination: widow-orphan\" align=center><SPAN style=\"FONT-SIZE: 12pt; FONT-FAMILY: 宋体; COLOR: #474747; LETTER-SPACING: 0.75pt; LINE-HEIGHT: 150%; mso-bidi-font-family: 宋体; mso-font-kerning: 0pt; mso-no-proof: yes\"></SPAN><SPAN lang=EN-US style=\"FONT-SIZE: 12pt; FONT-FAMILY: 宋体; COLOR: #474747; LETTER-SPACING: 0.75pt; LINE-HEIGHT: 150%; mso-bidi-font-family: 宋体; mso-font-kerning: 0pt\"><o:p></o:p></SPAN>&nbsp;</P>";

        B_article obj = new B_article();
		obj.setId(id);
		obj.setCompanyid(employee.getCompanyid());
		obj.setSubjectid("SUBJ92");
		obj.setSubjectname("发布统计");
		obj.setType("wzwz");
		obj.setTypename("文章");
		obj.setTitle(year + "年" + month + "月国土资源厅直属事业单位信息发布情况公布");
		obj.setShowtitle(obj.getTitle());
		obj.setAbstracts(obj.getTitle());
		obj.setSource("甘肃国土资源厅");
		obj.setContent(content);
		obj.setFirstly("false");
		obj.setSlide("false");
		obj.setShowtype("wzxspt");
		obj.setShowtypename("普通文章");
		obj.setAuditing("false");
		obj.setWordnum(0L);
		obj.setImagenum(0L);
		obj.setBranchid("51");
		obj.setBranchname("信息中心");
		obj.setAdddate(this.getData());
		obj.setAddemployeeid(employee.getId());
		obj.setAddemployeename(employee.getRealname());
		obj.setDeleted("0");
		objectDao.save(obj);
	}
}

