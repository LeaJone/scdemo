package com.fsd.admin.service.impl;

import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_teamanduser;
import com.fsd.admin.service.A_EmployeeService;
import com.fsd.admin.service.Z_teamanduserService;
import com.fsd.admin.dao.Z_teamanduserDao;

@Service("z_teamanduserServiceImpl")
public class Z_teamanduserServiceImpl extends BaseServiceImpl<Z_teamanduser, String> implements Z_teamanduserService{
    
    private static final Logger log = Logger.getLogger(Z_teamanduserServiceImpl.class);
    private String depict = "";
    
    @Resource(name = "z_teamanduserDaoImpl")
	public void setBaseDao(Z_teamanduserDao Z_teamanduserDao) {
		super.setBaseDao(Z_teamanduserDao);
	}
	
	@Resource(name = "z_teamanduserDaoImpl")
	private Z_teamanduserDao objectDao;
	
	@Resource(name = "A_EmployeeServiceImpl")
	private A_EmployeeService userService;
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("f_delete", "0"));
		return objectDao.findPager(param , c);
	}
    
    /**
	 * 编辑对象
	 * @param parameters
	 * @param employee
	 * @throws Exception
	 */
	public ParametersUtil save(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		String id = (String) objectMap.get("id");
		String tdid = (String) objectMap.get("tdid");
		List<Z_teamanduser> list = objectDao.queryByHql("from Z_teamanduser where f_userid = '" + id + "' and f_teamid = '" + tdid + "' and f_delete = '0'");
		if (list.size() > 0){
			throw new BusinessException(depict + "所选人已经是团队的一员，请不要重复添加!");
		}
		Z_teamanduser obj = new Z_teamanduser();
		A_Employee emp = userService.getUserinfoById(id);
		obj.setId(this.getUUID());
	   	obj.setF_teamid(tdid);
		obj.setF_userid(id);
		obj.setUsername(emp.getRealname());
		obj.setUsersex(emp.getSexname());
		obj.setUseracademy(emp.getAcademyname());
		obj.setUserbranch(emp.getBranchname());
		obj.setUsernumber(emp.getCode());
		obj.setF_delete("0");
		objectDao.save(obj);
        ParametersUtil util = new ParametersUtil();
		util.addSendObject("id", obj.getF_teamid());
        return util;
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		String id = (String) objectMap.get("id");
		objectDao.delete(id);
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public Z_teamanduser getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		Z_teamanduser obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		
		return obj;
	}
	
	/**
	 * 加载列表数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<Z_teamanduser> getTeamanduserList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("name") != null && !"".equals(objectMap.get("name"))){
				c.add(Restrictions.eq("f_teamid", objectMap.get("name")));
			}else {
				c.add(Restrictions.eq("f_teamid", "-1"));
			}
		}
		c.add(Restrictions.eq("f_delete", "0"));
		return objectDao.getList(c);
	}

	/**
	 * 加载列表数据
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public List<Z_teamanduser> getTeamById(ParametersUtil parameters, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") != null && !"".equals(objectMap.get("id"))){
			c.add(Restrictions.eq("f_teamid", objectMap.get("id")));
		}
		if(objectMap.get("txt") != null && !"".equals(objectMap.get("txt"))){
			c.add(Restrictions.like("username", "%" + objectMap.get("txt") + "%"));
		}
		c.add(Restrictions.eq("f_delete", "0"));
		return objectDao.getList(c);
	}
	
}
