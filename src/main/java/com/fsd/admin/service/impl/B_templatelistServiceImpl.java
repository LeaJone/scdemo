package com.fsd.admin.service.impl;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.fsd.admin.dao.B_templatelistDao;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.B_templatelist;
import com.fsd.admin.service.B_templatelistService;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.Config;
import com.fsd.core.util.ParametersUtil;
import com.google.gson.Gson;

@Repository("b_templatelistServiceImpl")
public class B_templatelistServiceImpl extends MainServiceImpl<B_templatelist, String> implements B_templatelistService{
	
	private static final Logger log = Logger.getLogger(B_templatelistServiceImpl.class);
    private String depict = "参数列表";
    
    @Resource(name = "b_templatelistDaoImpl")
	public void setBaseDao(B_templatelistDao b_templatelistDao) {
		super.setBaseDao(b_templatelistDao);
	}
	
	@Resource(name = "b_templatelistDaoImpl")
	private B_templatelistDao objectDao;
	
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		c.addOrder(Order.asc("sort"));
		c.addOrder(Order.asc("name"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("fid") != null && !"".equals(objectMap.get("fid"))){
				c.add(Restrictions.or(
						Restrictions.eq("templatetypeid", objectMap.get("fid")),
						Restrictions.eq("companyid", objectMap.get("fid"))
						));
			}
			if(objectMap.get("name") != null && !"".equals(objectMap.get("name"))){
				c.add(Restrictions.like("name", "%" + objectMap.get("name") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}
	
	/**
	 * 保存对象（添加、修改）
	 * @param obj
	 */
	public void saveObject(B_templatelist obj, A_Employee employee) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			B_templatelist obj_old = objectDao.get(obj.getId());
			obj_old.setCode(obj.getCode());	
			obj_old.setName(obj.getName());
			obj_old.setTitle(obj.getTitle());
			obj_old.setBelongsid(obj.getBelongsid());
			obj_old.setImagepath(obj.getImagepath());
			obj_old.setUrlpath(obj.getUrlpath());
			obj_old.setParameter1(obj.getParameter1());
			obj_old.setParameter2(obj.getParameter2());
			obj_old.setParameter3(obj.getParameter3());
			obj_old.setRemark(obj.getRemark());
			obj_old.setSort(obj.getSort());
			obj_old.setUpdatedate(this.getData());//设置修改日期
			obj_old.setUpdateemployeeid(employee.getId());//设置修改用户id
			obj_old.setUpdateemployeename(employee.getRealname());//设置修改用户姓名
			objectDao.update(obj_old);
		}else{
			//添加
			
		}
		this.sysLogService.saveObject(Config.LOGTYPEBC, this.depict + obj.getId() + obj.getName(), employee, B_templatelistServiceImpl.class);
	}
	
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public B_templatelist getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		B_templatelist obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		return obj;
	}
}
