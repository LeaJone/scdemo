package com.fsd.admin.service.impl;

import javax.annotation.Resource;
import org.springframework.stereotype.Repository;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.admin.model.A_Accounts;
import com.fsd.admin.service.A_AccountsService;
import com.fsd.admin.dao.A_AccountsDao;

@Repository("A_AccountsServiceImpl")
public class A_AccountsServiceImpl extends MainServiceImpl<A_Accounts, String> implements A_AccountsService{
    
    @Resource(name = "A_AccountsDaoImpl")
	public void setBaseDao(A_AccountsDao A_AccountsDao) {
		super.setBaseDao(A_AccountsDao);
	}
	
	@Resource(name = "A_AccountsDaoImpl")
	private A_AccountsDao A_AccountsDao;
}
