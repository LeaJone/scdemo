package com.fsd.admin.service.impl;

import com.fsd.admin.dao.N_pushpublishDao;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.N_pushpublish;
import com.fsd.admin.service.N_pushpublishService;
import com.fsd.admin.service.N_pushsettingsService;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

@Repository("n_pushpublishServiceImpl")
public class N_pushpublishServiceImpl extends MainServiceImpl<N_pushpublish, String> implements N_pushpublishService {
    
    @Resource(name = "n_pushpublishDaoImpl")
	public void setBaseDao(N_pushpublishDao n_pushpublishDao) {
		super.setBaseDao(n_pushpublishDao);
	}
	
	@Resource(name = "n_pushpublishDaoImpl")
	private N_pushpublishDao objectDao;

	@Resource(name = "n_pushsettingsServiceImpl")
	private N_pushsettingsService pushsettingsService;

	/**
	 * 保存对象(发布推送通知)
	 * @param obj
	 */
	@Override
	public void saveObject(N_pushpublish obj , A_Employee employee) throws Exception{
		obj.setId(this.getUUID());
		obj.setAdddate(this.getData());
		objectDao.save(obj);
	}
}
