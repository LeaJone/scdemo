package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;

import com.fsd.admin.model.Z_project;
import com.fsd.admin.service.Z_projectService;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.F_middlechecknext;
import com.fsd.admin.service.F_middlechecknextService;
import com.fsd.admin.dao.F_middlechecknextDao;

@Repository("f_middlechecknextServiceImpl")
public class F_middlechecknextServiceImpl extends BaseServiceImpl<F_middlechecknext, String> implements F_middlechecknextService{
    
    private static final Logger log = Logger.getLogger(F_middlechecknextServiceImpl.class);
    private String depict = "";
    
    @Resource(name = "f_middlechecknextDaoImpl")
	public void setBaseDao(F_middlechecknextDao F_middlechecknextDao) {
		super.setBaseDao(F_middlechecknextDao);
	}
	
	@Resource(name = "f_middlechecknextDaoImpl")
	private F_middlechecknextDao objectDao;

    @Autowired
    private Z_projectService projectService;
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
    @Override
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("f_deleted", "0"));
		c.addOrder(Order.asc("f_adddate"));
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
		if(objectMap.get("fid") != null && !"".equals(objectMap.get("fid"))){
			c.add(Restrictions.eq("f_fid", objectMap.get("fid")));
		}else{
			throw new BusinessException("系统错误，请联系管理员");
		}
		return objectDao.findPager(param , c);
	}

    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
    @Override
	public void save(F_middlechecknext obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
            F_middlechecknext old_obj = objectDao.get(obj.getId());
			old_obj.setF_starttime(obj.getF_starttime());
			old_obj.setF_stoptime(obj.getF_stoptime());
			old_obj.setF_content(obj.getF_content());
            old_obj.setF_lastupdatedate(this.getData());//设置修改日期
			old_obj.setF_updateemployeeid(employee.getId());//设置修改用户id
			old_obj.setF_updateemployeename(employee.getRealname());//设置修改用户姓名
            objectDao.update(old_obj);
        }else{
            //添加
            obj.setId(this.getUUID());//设置主键
			Z_project project = projectService.get(obj.getF_projectid());
			if(project == null){
				throw new BusinessException("项目信息错误，请联系管理员");
			}
			obj.setF_projectname(project.getF_name());
			obj.setF_adddate(this.getData());//设置添加日期
			obj.setF_addemployeeid(employee.getId());//设置添加用户id
			obj.setF_addemployeename(employee.getRealname());//设置添加用户姓名
			obj.setF_deleted("0");//设置删除标志(0:正常 1：已删除)
			objectDao.save(obj);
        }
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
    @Override
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		for (String id : dir) {
			objectDao.delete(id);
		}
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
    @Override
	public F_middlechecknext getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		F_middlechecknext obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		
		return obj;
	}

	/**
	 * 根据项目ID加载中期检查下一阶段计划列表
	 * @param projectid
	 * @return
	 * @throws Exception
	 */
	public List<F_middlechecknext> getListByProjectId(String projectid) throws Exception{
    	String sql = "select * from F_middlechecknext where f_projectid = ? and f_deleted = ?";
    	return objectDao.queryBySql1(sql, projectid, "0");
	}
}
