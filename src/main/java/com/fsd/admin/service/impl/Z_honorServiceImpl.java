package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_honor;
import com.fsd.admin.service.Z_honorService;
import com.fsd.admin.dao.Z_honorDao;

@Repository("z_honorServiceImpl")
public class Z_honorServiceImpl extends BaseServiceImpl<Z_honor, String> implements Z_honorService{
    
    private static final Logger log = Logger.getLogger(Z_honorServiceImpl.class);
    private String depict = "";
    
    @Resource(name = "z_honorDaoImpl")
	public void setBaseDao(Z_honorDao Z_honorDao) {
		super.setBaseDao(Z_honorDao);
	}
	
	@Resource(name = "z_honorDaoImpl")
	private Z_honorDao objectDao;
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
    @Override
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("f_deleted", "0"));
		c.addOrder(Order.asc("f_adddate"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("fid") != null && !"".equals(objectMap.get("fid"))){
				c.add(Restrictions.eq("f_fid", objectMap.get("fid")));
			}else{
				c.add(Restrictions.eq("f_fid", objectMap.get("abc")));
			}
		}
		return objectDao.findPager(param , c);
	}
    
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
    @Override
	public void save(Z_honor obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
            Z_honor old_obj = objectDao.get(obj.getId());

			old_obj.setF_name(obj.getF_name());
			old_obj.setF_organization(obj.getF_organization());
			old_obj.setF_getthetime(obj.getF_getthetime());
			old_obj.setF_imageurl(obj.getF_imageurl());

			old_obj.setF_lastupdatedate(this.getData());
			old_obj.setF_updateemployeeid(employee.getId());
			old_obj.setF_updateemployeename(employee.getRealname());
            objectDao.update(old_obj);
        }else{
            //添加
			//设置主键
            obj.setId(this.getUUID());
			obj.setF_adddate(this.getData());
			obj.setF_addemployeeid(employee.getId());
			obj.setF_addemployeename(employee.getRealname());
			obj.setF_deleted("0");
			objectDao.save(obj);
        }
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
    @Override
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		for (String id : dir) {
			objectDao.delete(id);
		}
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
    @Override
	public Z_honor getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		Z_honor obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		
		return obj;
	}

	/**
	 * 根据Fid加载列表
	 * @param fid
	 * @return
	 * @throws Exception
	 */
	public List<Z_honor> getListByFid(String fid) throws Exception{
    	String sql = "select * from z_honor where f_fid = ?";
    	return objectDao.queryBySql1(sql, fid);
	}
}
