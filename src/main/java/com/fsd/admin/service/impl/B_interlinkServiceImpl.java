package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import org.springframework.stereotype.Repository;

import com.fsd.core.bean.Tree;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.Config;
import com.fsd.core.util.ParametersUtil;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.B_interlink;
import com.fsd.admin.service.B_interlinkService;
import com.fsd.admin.dao.B_interlinkDao;
import com.google.gson.Gson;

@Repository("b_interlinkServiceImpl")
public class B_interlinkServiceImpl extends MainServiceImpl<B_interlink, String> implements B_interlinkService{
    
    private static final Logger log = Logger.getLogger(B_interlinkServiceImpl.class);
    private String depict = "友情链接";
    
    @Resource(name = "b_interlinkDaoImpl")
	public void setBaseDao(B_interlinkDao b_interlinkDao) {
		super.setBaseDao(b_interlinkDao);
	}
	
	@Resource(name = "b_interlinkDaoImpl")
	private B_interlinkDao objectDao;

	
	private String checkLeaf(String fid){
		long num = objectDao.getCount("parentid = '"+ fid +"'", "deleted = '0'");
		if (num > 0)
			return "false";
		return "true";
	}
	
	private List<B_interlink> queryObjectListById(String fid){
		return objectDao.queryByHql("from B_interlink a where a.parentid = '"+ fid +"' and a.deleted = '0' order by sort asc");
	}
	
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		c.addOrder(Order.asc("sort"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("fid") != null && !"".equals(objectMap.get("fid"))){
				c.add(Restrictions.eq("parentid", objectMap.get("fid")));
			}
			if(objectMap.get("name") != null && !"".equals(objectMap.get("name"))){
				c.add(Restrictions.like("name", "%" + objectMap.get("name") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}
	
	/**
	 * 加载对象树
	 * @return
	 * @throws Exception
	 */
	public List<Tree> getObjectTree(String fid) throws Exception{
		List<B_interlink> list = objectDao.queryByHql("from B_interlink a where a.baseid = '"+ fid +"' and a.deleted = '0' order by sort asc");
		List<Tree> treelist = this.getObjectTree(fid, list);
		return treelist;
	}
	private List<Tree> getObjectTree(String fid, List<B_interlink> list) throws Exception{
		List<Tree> treelist = new ArrayList<Tree>();
		Tree node = null;
		for (B_interlink obj : list) {
			if (obj.getParentid().equals(fid)){
				node = new Tree();
				node.setId(obj.getId());
				node.setText(obj.getName());
				node.setLeaf(Boolean.valueOf(obj.getIsleaf()));
				if(!node.isLeaf()){
					node.setChildren(this.getObjectTree(obj.getId(), list));
				}
				treelist.add(node);
			}
			
		}
		return treelist;
	}
	
	/**
	 * 异步加载对象树
	 * @return
	 * @throws Exception
	 */
	public List<Tree> getAsyncObjectTree(String fid) throws Exception{
		List<Tree> treelist = new ArrayList<Tree>();
		List<B_interlink> list = this.queryObjectListById(fid);
		Tree menu = null;
		for (B_interlink obj : list) {
			menu = new Tree();
			menu.setId(obj.getId());
			menu.setText(obj.getName());
			menu.setLeaf(Boolean.valueOf(obj.getIsleaf()));
			treelist.add(menu);
		}
		return treelist;
	}
	
	/**
	 * 保存对象（添加、修改）
	 * @param obj
	 */
	public void saveObject(B_interlink obj , A_Employee employee) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			B_interlink obj_old = objectDao.get(obj.getId());
			obj_old.setParentid(obj.getParentid());
			obj_old.setParentname(obj.getParentname());
			obj_old.setName(obj.getName());
			obj_old.setUrl(obj.getUrl());
			obj_old.setImageurl1(obj.getImageurl1());
			obj_old.setImageurl2(obj.getImageurl2());
			obj_old.setSort(obj.getSort());
			obj_old.setRemark(obj.getRemark());
			obj_old.setAuditing("false");
			obj_old.setUpdatedate(this.getData());//设置修改日期
			obj_old.setUpdateemployeeid(employee.getId());//设置修改用户id
			obj_old.setUpdateemployeename(employee.getRealname());//设置修改用户姓名
			objectDao.update(obj_old);
		}else{
			//添加
			obj.setId(this.getUUID());//设置主键
			obj.setCompanyid(employee.getCompanyid());//所属单位
			obj.setAdddate(this.getData());//设置添加日期
			obj.setAddemployeeid(employee.getId());//设置添加用户id
			obj.setAddemployeename(employee.getRealname());//设置添加用户姓名
			obj.setIsleaf("true");
			obj.setAuditing("false");
			obj.setDeleted("0");//设置删除标志(0:正常 1：已删除)
			objectDao.save(obj);
			objectDao.executeHql("update B_interlink t set t.isleaf = 'false' where t.id = '" + obj.getParentid() + "'");
		}
		this.sysLogService.saveObject(Config.LOGTYPEBC, this.depict + obj.getId() + obj.getName(), employee, B_interlinkServiceImpl.class);
	}
	
	/**
	 * 删除对象
	 * @param parameters
	 */
	@SuppressWarnings("unchecked")
	public void delObject(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		String oid = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
			oid = id;
		}
		objectDao.executeHql("update B_interlink t set t.deleted = '1' where t.id in (" + ids + ")");
		B_interlink obj = objectDao.get(oid);
		if (obj != null){
			objectDao.executeHql("update B_interlink t set t.isleaf = '" + this.checkLeaf(obj.getParentid()) + "' where t.id = '" + obj.getParentid() + "'");
		}
	}
	
	/**
	 * 审核对象
	 * @param parameters
	 */
	@SuppressWarnings("unchecked")
	public void auditObject(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String status = objectMap.get("status").toString();
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		objectDao.executeHql("update B_interlink t set t.auditing = '"+ status +"' where t.id in (" + ids + ")");
	}
	
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public B_interlink getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		B_interlink obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		return obj;
	}
}
