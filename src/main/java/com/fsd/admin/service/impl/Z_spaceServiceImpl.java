package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;

import com.fsd.core.bean.Tree;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_space;
import com.fsd.admin.service.Z_spaceService;
import com.fsd.admin.dao.Z_spaceDao;

@Repository("z_spaceServiceImpl")
public class Z_spaceServiceImpl extends BaseServiceImpl<Z_space, String> implements Z_spaceService{
    
    private static final Logger log = Logger.getLogger(Z_spaceServiceImpl.class);
    private String depict = "";
    
    @Resource(name = "z_spaceDaoImpl")
	public void setBaseDao(Z_spaceDao Z_spaceDao) {
		super.setBaseDao(Z_spaceDao);
	}
	
	@Resource(name = "z_spaceDaoImpl")
	private Z_spaceDao objectDao;
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
    @Override
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("f_deleted", "0"));
		c.addOrder(Order.asc("f_adddate"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("name") != null && !"".equals(objectMap.get("name"))){
				c.add(Restrictions.like("f_name", "%" + objectMap.get("name") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}
    
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
    @Override
	public Z_space save(Z_space obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
            Z_space old_obj = objectDao.get(obj.getId());
			old_obj.setF_name(obj.getF_name());
			old_obj.setF_vrurl(obj.getF_vrurl());
			old_obj.setF_ds(obj.getF_ds());
			old_obj.setF_rs(obj.getF_rs());
			old_obj.setF_rzqy(obj.getF_rzqy());
			old_obj.setF_zxwcxjj(obj.getF_zxwcxjj());
			old_obj.setF_scjj(obj.getF_scjj());
			old_obj.setF_schd(obj.getF_schd());
			old_obj.setF_cyjs(obj.getF_cyjs());
			old_obj.setF_zscq(obj.getF_zscq());
			old_obj.setF_abstract(obj.getF_abstract());

			old_obj.setF_updateemployeeid(employee.getId());
			old_obj.setF_updateemployeename(employee.getRealname());
			old_obj.setF_lastupdatedate(this.getData());
            objectDao.update(old_obj);
            return old_obj;
        }else{
            //添加
			obj.setId(this.getUUID());
			obj.setF_adddate(this.getData());
			obj.setF_addemployeeid(employee.getId());
			obj.setF_addemployeename(employee.getRealname());
			obj.setF_deleted("0");
			objectDao.save(obj);
			return obj;
        }
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
    @Override
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		for (String id : dir) {
			objectDao.delete(id);
		}
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
    @Override
	public Z_space getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		Z_space obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		
		return obj;
	}

	/**
	 * 获得空间树结构
	 * @return
	 * @throws Exception
	 */
	public List<Tree> getSpaceTree() throws Exception{
    	String sql = "select * from z_space where f_deleted = ?";
		List<Tree> treelist = new ArrayList<>();
    	List<Z_space> list = objectDao.queryBySql1(sql, "0");
		Tree tree = null;
		if(list != null && list.size() > 0){
			for (Z_space z_space : list) {
				tree = new Tree();
				tree.setId(z_space.getId());
				tree.setText(z_space.getF_name());
				tree.setLeaf(true);
				treelist.add(tree);
			}
		}
		return treelist;
	}
}
