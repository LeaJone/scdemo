package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.Config;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.I_testinfo;
import com.fsd.admin.service.I_bankinfoService;
import com.fsd.admin.service.TP_testinfoService;
import com.fsd.admin.dao.I_testinfoDao;

@Repository("tp_testinfoServiceImpl")
public class TP_testinfoServiceImpl extends MainServiceImpl<I_testinfo, String> implements TP_testinfoService{
    
    private static final Logger log = Logger.getLogger(TP_testinfoServiceImpl.class);
    private String depict = "投票库";
    
    @Resource(name = "i_testinfoDaoImpl")
	public void setBaseDao(I_testinfoDao I_testinfoDao) {
		super.setBaseDao(I_testinfoDao);
	}
	
	@Resource(name = "i_testinfoDaoImpl")
	private I_testinfoDao objectDao;
    @Resource(name = "i_bankinfoServiceImpl")
	private I_bankinfoService bankinfoService;
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
        c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		c.add(Restrictions.eq("deleted", "0"));
		c.addOrder(Order.desc("adddate"));
        if(param.getJsonData() != null && !"".equals(param.getJsonData())){
        	Gson gs = new Gson();
    		Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
            if(objectMap.get("fid") != null && !"".equals(objectMap.get("fid"))){
            	List<String> idList = this.bankinfoService.getObjectChildIDList(
						objectMap.get("fid").toString(), employee);
				idList.add(objectMap.get("fid").toString());
				c.add(Restrictions.in("bankinfoid", idList));
			}
    		if(objectMap.get("name") != null && !"".equals(objectMap.get("name"))){
    			c.add(Restrictions.like("name", "%" + objectMap.get("name") + "%"));
    		}
        }
		return objectDao.findPager(param , c);
	}
    
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public void save(I_testinfo obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
            I_testinfo old_obj = objectDao.get(obj.getId());
            old_obj.setBankinfoid(obj.getBankinfoid());
            old_obj.setBankinfoname(obj.getBankinfoname());
            old_obj.setName(obj.getName());
            old_obj.setDescription(obj.getDescription());
            old_obj.setStratdate(obj.getStratdate());
            old_obj.setEnddate(obj.getEnddate());
            old_obj.setRemark(obj.getRemark());
            old_obj.setStatus("i_tjztty");
            old_obj.setStatusname("停用");
            old_obj.setUpdatedate(this.getData());//设置修改日期
			old_obj.setUpdateemployeeid(employee.getId());//设置修改用户id
			old_obj.setUpdateemployeename(employee.getRealname());//设置修改用户姓名
            objectDao.update(old_obj);
        }else{
            //添加
            obj.setId(this.getUUID());//设置主键
			obj.setCompanyid(employee.getCompanyid());//所属单位
			obj.setStatus("i_tjztty");
			obj.setStatusname("停用");
			obj.setAdddate(this.getData());//设置添加日期
			obj.setAddemployeeid(employee.getId());//设置添加用户id
			obj.setAddemployeename(employee.getRealname());//设置添加用户姓名
			obj.setDeleted("0");//设置删除标志(0:正常 1：已删除)
			objectDao.save(obj);
        }
        this.sysLogService.saveObject(Config.LOGTYPEBC, this.depict + obj.getId() + obj.getName(), employee, TP_testinfoServiceImpl.class);
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = this.getSQLinByIDList(dir);
		objectDao.executeHql("update I_testinfo t set t.deleted = '1', t.updatedate = '" + this.getData() + 
				"', t.updateemployeeid = '" + employee.getId() + "', t.updateemployeename = '" + employee.getRealname() + 
				"' where t.id in (" + ids + ")");
		this.sysLogService.saveObject(Config.LOGTYPESC, this.depict + ids, employee, TP_testinfoServiceImpl.class);
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public I_testinfo getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		I_testinfo obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		
		return obj;
	}
	
	/**
	 * 修改状态
	 * @param parameters
	 */
	@SuppressWarnings("unchecked")
	public void updateEnabledObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> idList = (ArrayList<String>) objectMap.get("ids");
		String ids = this.getSQLinByIDList(idList);
		boolean isEnabled = (Boolean) objectMap.get("isenabled");
		if (isEnabled){
			objectDao.executeHql("update I_testinfo t set t.status = 'i_tjztqy', t.statusname = '启用', t.updatedate = '" + this.getData() + 
				"', t.updateemployeeid = '" + employee.getId() + "', t.updateemployeename = '" + employee.getRealname() + 
				"' where t.id in (" + ids + ")");
			this.sysLogService.saveObject(Config.LOGTYPEBC, this.depict + " 修改“启用” " + ids, employee, TP_testinfoServiceImpl.class);
		}else{
			objectDao.executeHql("update I_testinfo t set t.status = 'i_tjztty', t.statusname = '停用', t.updatedate = '" + this.getData() + 
				"', t.updateemployeeid = '" + employee.getId() + "', t.updateemployeename = '" + employee.getRealname() + 
				"' where t.id in (" + ids + ")");
			this.sysLogService.saveObject(Config.LOGTYPEBC, this.depict + " 修改“停用” " + ids, employee, TP_testinfoServiceImpl.class);
		}
	}
}
