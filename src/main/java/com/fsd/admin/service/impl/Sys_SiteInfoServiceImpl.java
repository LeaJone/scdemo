package com.fsd.admin.service.impl;

import javax.annotation.Resource;
import org.springframework.stereotype.Repository;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.admin.model.Sys_SiteInfo;
import com.fsd.admin.service.Sys_SiteInfoService;
import com.fsd.admin.dao.Sys_SiteInfoDao;

@Repository("Sys_SiteInfoServiceImpl")
public class Sys_SiteInfoServiceImpl extends MainServiceImpl<Sys_SiteInfo, String> implements Sys_SiteInfoService{
    
    @Resource(name = "Sys_SiteInfoDaoImpl")
	public void setBaseDao(Sys_SiteInfoDao Sys_SiteInfoDao) {
		super.setBaseDao(Sys_SiteInfoDao);
	}
	
	@Resource(name = "Sys_SiteInfoDaoImpl")
	private Sys_SiteInfoDao Sys_SiteInfoDao;
}
