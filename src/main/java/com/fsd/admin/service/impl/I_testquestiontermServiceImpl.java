package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.I_testquestionterm;
import com.fsd.admin.service.I_testquestiontermService;
import com.fsd.admin.dao.I_testquestiontermDao;

@Repository("i_testquestiontermServiceImpl")
public class I_testquestiontermServiceImpl extends BaseServiceImpl<I_testquestionterm, String> implements I_testquestiontermService{
    
    private static final Logger log = Logger.getLogger(I_testquestiontermServiceImpl.class);
    private String depict = "试题条件";
    
    @Resource(name = "i_testquestiontermDaoImpl")
	public void setBaseDao(I_testquestiontermDao I_testquestiontermDao) {
		super.setBaseDao(I_testquestiontermDao);
	}
	
	@Resource(name = "i_testquestiontermDaoImpl")
	private I_testquestiontermDao objectDao;
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
		Criteria c = objectDao.createCriteria();
        c.add(Restrictions.eq("companyid", employee.getCompanyid()));
        c.add(Restrictions.eq("testquestionid", objectMap.get("fid")));
		c.add(Restrictions.eq("deleted", "0"));
		c.addOrder(Order.asc("adddate"));
		return objectDao.findPager(param , c);
	}
    
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public void save(I_testquestionterm obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
            I_testquestionterm old_obj = objectDao.get(obj.getId());
            
            old_obj.setTestinfoid(obj.getTestinfoid());
            //old_obj.setTestinfoname(obj.getTestinfoname());
            old_obj.setTestquestionid(obj.getTestquestionid());
            //old_obj.setTestquestionname(obj.getTestquestionname());
            old_obj.setCode(obj.getCode());
            old_obj.setDescription(obj.getDescription());
            old_obj.setIsanswer(obj.getIsanswer());
            old_obj.setParse(obj.getParse());
            old_obj.setScore(obj.getScore());
            old_obj.setSort(obj.getSort());
            old_obj.setRemark(obj.getRemark());
            old_obj.setUpdatedate(this.getData());//设置修改日期
			old_obj.setUpdateemployeeid(employee.getId());//设置修改用户id
			old_obj.setUpdateemployeename(employee.getRealname());//设置修改用户姓名
            objectDao.update(old_obj);
        }else{
            //添加
            obj.setId(this.getUUID());//设置主键
			obj.setCompanyid(employee.getCompanyid());//所属单位
			obj.setAdddate(this.getData());//设置添加日期
			obj.setAddemployeeid(employee.getId());//设置添加用户id
			obj.setAddemployeename(employee.getRealname());//设置添加用户姓名
			obj.setDeleted("0");//设置删除标志(0:正常 1：已删除)
			objectDao.save(obj);
        }
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		String hql = "";
		objectDao.executeHql("update I_testquestionterm t set t.deleted = '1', t.updatedate = '" + this.getData() + 
				"', t.updateemployeeid = '" + employee.getId() + "', t.updateemployeename = '" + employee.getRealname() + 
				"' where t.id in (" + ids + ")");
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public I_testquestionterm getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		I_testquestionterm obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		
		return obj;
	}
}
