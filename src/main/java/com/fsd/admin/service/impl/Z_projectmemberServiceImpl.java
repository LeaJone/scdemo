package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;

import com.fsd.admin.model.Z_project;
import com.fsd.admin.service.Z_projectService;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_projectmember;
import com.fsd.admin.service.Z_projectmemberService;
import com.fsd.admin.dao.Z_projectmemberDao;

@Repository("z_projectmemberServiceImpl")
public class Z_projectmemberServiceImpl extends BaseServiceImpl<Z_projectmember, String> implements Z_projectmemberService{
    
    private static final Logger log = Logger.getLogger(Z_projectmemberServiceImpl.class);
    private String depict = "";
    
    @Resource(name = "z_projectmemberDaoImpl")
	public void setBaseDao(Z_projectmemberDao Z_projectmemberDao) {
		super.setBaseDao(Z_projectmemberDao);
	}
	
	@Resource(name = "z_projectmemberDaoImpl")
	private Z_projectmemberDao objectDao;

    @Autowired
    private Z_projectService projectService;
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
    @Override
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("f_deleted", "0"));
		c.addOrder(Order.asc("f_adddate"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("projectid") != null && !"".equals(objectMap.get("projectid"))){
				c.add(Restrictions.eq("f_projectid", objectMap.get("projectid")));
			}else{
				c.add(Restrictions.eq("f_projectid", "abc"));
			}
		}
		return objectDao.findPager(param , c);
	}
    
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
    @Override
	public void save(Z_projectmember obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
            Z_projectmember old_obj = objectDao.get(obj.getId());
            
            old_obj.setF_lastupdatedate(this.getData());//设置修改日期
			old_obj.setF_updateemployeeid(employee.getId());//设置修改用户id
			old_obj.setF_updateemployeename(employee.getRealname());//设置修改用户姓名
            objectDao.update(old_obj);
        }else{
            //添加
			String sql = "select * from Z_projectmember where f_projectid = ? and f_memberid = ? and f_deleted = ?";
			List<Z_projectmember> list = objectDao.queryBySql1(sql, obj.getF_projectid(), obj.getF_memberid(), "0");
			if(list != null && list.size() > 0){
				throw new BusinessException("该项目成员已存在");
			}
			Z_project project = projectService.get(obj.getF_projectid());
			if(project.getF_leaderid().equals(obj.getF_memberid())){
				throw new BusinessException("该人员为项目负责人，不能再作为项目成员");
			}

			String sql1 = "select * from Z_projectmember where f_memberid = ? and f_projectid <> ? and f_deleted = ?";
			List<Z_projectmember> list1 = objectDao.queryBySql1(sql1, obj.getF_memberid(), obj.getF_projectid(), "0");
			if(list1 != null && list1.size() > 1){
				throw new BusinessException("每个人员最多只能参加两个项目，请确认后重新提交");
			}

            obj.setId(this.getUUID());//设置主键
			obj.setF_adddate(this.getData());//设置添加日期
			obj.setF_addemployeeid(employee.getId());//设置添加用户id
			obj.setF_addemployeename(employee.getRealname());//设置添加用户姓名
			obj.setF_deleted("0");//设置删除标志(0:正常 1：已删除)
			objectDao.save(obj);
        }
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
    @Override
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		for (String id : dir) {
			objectDao.delete(id);
		}
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
    @Override
	public Z_projectmember getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		Z_projectmember obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		
		return obj;
	}

	/**
	 * 根据项目编码加载项目成员列表
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<Z_projectmember> getListByProjectId(String projectId) throws Exception{
    	String sql = "select * from Z_projectmember where f_projectid = ? and f_deleted = ?";
    	return objectDao.queryBySql1(sql, projectId, "0");
	}

	/**
	 * 根据项目ID删除项目成员
	 * @param projectId
	 * @throws Exception
	 */
	@Override
	public void delObjectByProjectId(String projectId) throws Exception{
    	String sql = "delete from Z_projectmember where f_projectid = ?";
    	objectDao.executeSql(sql, projectId);
	}
}
