package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.BeanToMapUtil;
import com.fsd.core.util.Config;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.dao.B_articleDao;
import com.fsd.admin.dao.Z_articlefzDao;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.B_article;
import com.fsd.admin.model.Z_articlefz;
import com.fsd.admin.service.B_articlerelateService;
import com.fsd.admin.service.Z_articlefzService;
import com.fsd.admin.service.Z_ldxxglService;
import com.google.gson.Gson;

@Repository("z_ldxxglServiceImpl")
public class Z_ldxxglServiceImpl extends MainServiceImpl<Z_articlefz, String> implements Z_ldxxglService{
    
    private static final Logger log = Logger.getLogger(Z_ldxxglServiceImpl.class);
    private String depict = "领导信息";
    
    @Resource(name = "z_articlefzDaoImpl")
   	public void setBaseDao(Z_articlefzDao Z_articlefzDao) {
   		super.setBaseDao(Z_articlefzDao);
   	}
   	
   	@Resource(name = "z_articlefzDaoImpl")
   	private Z_articlefzDao objectDao;
    
   	@Resource(name = "b_articleDaoImpl")
	private B_articleDao articleDao;
   	
   	@Resource(name = "b_articlerelateServiceImpl")
	private B_articlerelateService articleRelateService;
   	
    @Resource(name = "z_articlefzServiceImpl")
   	private Z_articlefzService articleFzService;
   	
    /**
   	 * 加载分页数据
   	 * @param param
   	 * @return
   	 * @throws Exception
   	 */
   	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
   		Criteria c = articleDao.createCriteria();
   		c.add(Restrictions.eq("type", "wzld"));
   		c.add(Restrictions.eq("companyid", employee.getCompanyid()));
   		c.add(Restrictions.eq("deleted", "0"));
   		c.addOrder(Order.desc("adddate"));
   		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
   			Gson gs = new Gson();
   			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
   			if(objectMap.get("name") != null && !"".equals(objectMap.get("name"))){
   				c.add(Restrictions.like("title", "%" + objectMap.get("name") + "%"));
   			}
   		}
   		return articleDao.findPager(param , c);
   	}
       
   	/**
	 * 保存文本文章
	 * @param obj
	 * @param param
	 * @param employee
	 * @throws Exception
	 */
	public ParametersUtil save(B_article obj, String fznr, ParametersUtil param, A_Employee employee) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			B_article obj_old = articleDao.get(obj.getId());
			obj_old.setSubjectid(obj.getSubjectid());
			obj_old.setSubjectname(obj.getSubjectname());
			obj_old.setTitle(obj.getTitle());
			obj_old.setSubtitle(obj.getSubtitle());
			obj_old.setShowtitle(obj.getShowtitle());
			obj_old.setTitlecolor(obj.getTitlecolor());
			obj_old.setKeyword(obj.getKeyword());
			obj_old.setIsaurl(obj.getIsaurl());
			obj_old.setAurl(obj.getAurl());
			obj_old.setAuthor(obj.getAuthor());
			obj_old.setSource(obj.getSource());
			obj_old.setImageurl1(obj.getImageurl1());
			obj_old.setImageurl2(obj.getImageurl2());
			obj_old.setAbstracts(obj.getAbstracts());
			obj_old.setContent(obj.getContent());
			obj_old.setRecommend(obj.getRecommend());
			obj_old.setFirstly(obj.getFirstly());
			obj_old.setVideo(obj.getVideo());
			obj_old.setRoll(obj.getRoll());
			obj_old.setHeadline(obj.getHeadline());
			obj_old.setHot(obj.getHot());
			obj_old.setSlide(obj.getSlide());
			obj_old.setComments(obj.getComments());
			obj_old.setRemark(obj.getRemark());
			obj_old.setBranchid(obj.getBranchid());
			obj_old.setBranchname(obj.getBranchname());
			obj_old.setAuditing("false");//标记未审批
			obj_old.setUpdatedate(this.getData());
			obj_old.setUpdateemployeeid(employee.getId());
			obj_old.setUpdateemployeename(employee.getRealname());
			if (obj.getAdddate() != null && !obj.getAdddate().equals("")
				&& obj.getAdddate().length() == 14){
				obj_old.setAdddate(obj.getAdddate());
			}
			if (!"true".equals(obj.getFirstly())){
				obj_old.setFirstly("false");
			}
			if (!"true".equals(obj.getSlide())){
				obj_old.setSlide("false");//设置修改幻灯
			}
			articleDao.update(obj_old);
			
			Z_articlefz fz_old_obj = articleFzService.getObjectByArticleId(obj.getId());
			
			fz_old_obj.setUpdatedate(this.getData());//设置修改日期
			fz_old_obj.setUpdateemployeeid(employee.getId());//设置修改用户id
			fz_old_obj.setUpdateemployeename(employee.getRealname());//设置修改用户姓名
			
			fz_old_obj.setFznr(fznr);
			objectDao.update(fz_old_obj);
		}else{
			//添加
			obj.setId(this.getUUID());
			obj.setCompanyid(employee.getCompanyid());
			obj.setAuditing("false");//标记未审批
			obj.setAddemployeeid(employee.getId());
			obj.setAddemployeename(employee.getRealname());
			obj.setDeleted("0");//0标识未删除
			if (obj.getAdddate() == null || obj.getAdddate().length() != 14){
				obj.setAdddate(this.getData());//设置添加日期
			}
			if (!"true".equals(obj.getFirstly())){
				obj.setFirstly("false");
			}
			if (!"true".equals(obj.getSlide())){
				obj.setSlide("false");//设置修改幻灯
			}
			articleDao.save(obj);
			
			Z_articlefz fz_old_obj = new Z_articlefz();
			fz_old_obj.setId(this.getUUID());
			fz_old_obj.setFznr(fznr);
			fz_old_obj.setArticleid(obj.getId());
			
			fz_old_obj.setAdddate(this.getData());//设置添加日期
			fz_old_obj.setAddemployeeid(employee.getId());//设置添加用户id
			fz_old_obj.setAddemployeename(employee.getRealname());//设置添加用户姓名
			fz_old_obj.setDeleted("0");//设置删除标志(0:正常 1：已删除)
			
			objectDao.save(fz_old_obj);
		}
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			articleRelateService.delObject(obj.getId(), Config.ARTICLERELATESUBJECT);
			articleRelateService.delObject(obj.getId(), Config.ARTICLERELATESUBJECTXG);
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			ArrayList<String> sidList = (ArrayList<String>) objectMap.get(Config.ARTICLERELATESUBJECT);
			ArrayList<String> sxgidList = (ArrayList<String>) objectMap.get(Config.ARTICLERELATESUBJECTXG);
			if (sidList.size() > 0){
				for (String id : sidList) {
					articleRelateService.saveArticleRelate(obj.getId(), id, Config.ARTICLERELATESUBJECT);
				}
			}
			if (sxgidList.size() > 0){
				for (String id : sxgidList) {
					articleRelateService.saveArticleRelate(obj.getId(), id, Config.ARTICLERELATESUBJECTXG);
				}
			}
		}
		ParametersUtil util = new ParametersUtil();
		util.addSendObject("id", obj.getId());
		return util;
	}
       
    /**
   	 * 删除对象
   	 * @param parameters
   	 * @throws Exception
   	 */
   	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
   		Gson gs = new Gson();
   		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
   		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
   		String ids = "";
   		for (String id : dir) {
   			if (!ids.equals("")){
   				ids += ",";
   			}
   			ids += "'" + id + "'";
   		}
   		String hql = "";
   		objectDao.executeHql("update B_article t set t.deleted = '1', t.updatedate = '" + this.getData() + 
   				"', t.updateemployeeid = '" + employee.getId() + "', t.updateemployeename = '" + employee.getRealname() + 
   				"' where t.id in (" + ids + ")");
   		objectDao.executeHql("update Z_articlefz t set t.deleted = '1', t.updatedate = '" + this.getData() + 
   				"', t.updateemployeeid = '" + employee.getId() + "', t.updateemployeename = '" + employee.getRealname() + 
   				"' where t.articleid in (" + ids + ")");
   	}
       
       /**
   	 * 根据ID加载对象
   	 * @param parameters
   	 * @return
   	 * @throws Exception
   	 */
   	public ParametersUtil getObjectById(ParametersUtil parameters) throws Exception{
   		Gson gs = new Gson();
   		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
   		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
   			throw new BusinessException(depict + "获取缺少ID参数!");
   		}
   		B_article article = articleDao.get(objectMap.get("id").toString());
   		Z_articlefz articleFz = articleFzService.getObjectByArticleId(objectMap.get("id").toString());
   		if(article == null){
   			throw new BusinessException(depict + "数据不存在!");
   		}
   		if(articleFz == null){
   			Z_articlefz fzobj = new Z_articlefz();
   			fzobj.setArticleid(article.getId());
			
   			fzobj.setAdddate(article.getAdddate());//设置添加日期
			fzobj.setAddemployeeid(article.getAddemployeeid());//设置添加用户id
			fzobj.setAddemployeename(article.getAddemployeename());//设置添加用户姓名
			fzobj.setDeleted("0");//设置删除标志(0:正常 1：已删除)
			
			objectDao.save(fzobj);
   		}
   		Map<String, String> map = BeanToMapUtil.convertBean(article);
   		map.put("fznr", articleFz.getFznr());
   		
   		ParametersUtil util = new ParametersUtil();
		util.addSendObject("data", map);
		util.addSendObject(Config.ARTICLERELATESUBJECT, 
				articleRelateService.getObjectIDList(article.getId(), Config.ARTICLERELATESUBJECT));
		util.addSendObject(Config.ARTICLERELATESUBJECTXG, 
				articleRelateService.getObjectIDList(article.getId(), Config.ARTICLERELATESUBJECTXG));
   		return util;
   	}
   	
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil saveGetObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		B_article article = articleDao.get(objectMap.get("id").toString());
		Z_articlefz articleFz = articleFzService.getObjectByArticleId(objectMap.get("id").toString());
		if(article == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		if(articleFz == null){
			articleFz = new Z_articlefz();
			articleFz.setId(this.getUUID());
			articleFz.setArticleid(article.getId());
			
			articleFz.setAdddate(article.getAdddate());//设置添加日期
			articleFz.setAddemployeeid(article.getAddemployeeid());//设置添加用户id
			articleFz.setAddemployeename(article.getAddemployeename());//设置添加用户姓名
			articleFz.setDeleted("0");//设置删除标志(0:正常 1：已删除)
			
			objectDao.save(articleFz);
		}
		Map<String, String> map = BeanToMapUtil.convertBean(article);
		map.put("fznr", articleFz.getFznr());
		
		ParametersUtil util = new ParametersUtil();
		util.addSendObject("data", map);
		util.addSendObject(Config.ARTICLERELATESUBJECT, 
				articleRelateService.getObjectIDList(article.getId(), Config.ARTICLERELATESUBJECT));
		util.addSendObject(Config.ARTICLERELATESUBJECTXG, 
				articleRelateService.getObjectIDList(article.getId(), Config.ARTICLERELATESUBJECTXG));
		return util;
	}
   	
   	/**
	 * 修改对象状态
	 * @param parameters
	 */
	public void updateEnabledObject(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		boolean isEnabled = (Boolean) objectMap.get("isenabled");
		String ids = this.getSQLinByIDList(dir);
		if (isEnabled)
			objectDao.executeHql("update B_article t set auditing = 'true' where t.id in (" + ids + ")");
		else
			objectDao.executeHql("update B_article t set auditing = 'false' where t.id in (" + ids + ")");
	}
}
