package com.fsd.admin.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.Config;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.H_qualified;
import com.fsd.admin.service.A_EmployeeService;
import com.fsd.admin.service.H_qualifiedService;
import com.fsd.admin.service.Sys_SystemParametersService;
import com.fsd.admin.dao.H_qualifiedDao;

@Repository("h_qualifiedServiceImpl")
public class H_qualifiedServiceImpl extends MainServiceImpl<H_qualified, String> implements H_qualifiedService{

	private String depict = "会员资格：";
	
    @Resource(name = "h_qualifiedDaoImpl")
	public void setBaseDao(H_qualifiedDao objectDao) {
		super.setBaseDao(objectDao);
	}
	
	@Resource(name = "h_qualifiedDaoImpl")
	private H_qualifiedDao objectDao;
	
	@Resource(name = "Sys_SystemParametersServiceImpl")
	private Sys_SystemParametersService sysParamService;
	
	//用户
    @Resource(name = "A_EmployeeServiceImpl")
	private A_EmployeeService employeeService;
    
    //会员级别  h_hyjb
  	/**
     * 注册会员
     */
  	public static final String hyjbzc1 = "h_hyjbzc";
  	public static final String hyjbzc2 = "注册会员";
  	/**
     * 铜牌会员
     */
  	public static final String hyjbtp1 = "h_hyjbtp";
  	public static final String hyjbtp2 = "铜牌会员";
  	/**
     * 银牌会员
     */
  	public static final String hyjbyp1 = "h_hyjbyp";
  	public static final String hyjbyp2 = "银牌会员";
  	/**
     * 金牌会员
     */
  	public static final String hyjbjp1 = "h_hyjbjp";
  	public static final String hyjbjp2 = "金牌会员";
  	/**
     * 钻石会员
     */
  	public static final String hyjbzs1 = "h_hyjbzs";
  	public static final String hyjbzs2 = "钻石会员";
    
  	
	/**
	 * 保存对象添加
	 * @param branch
	 */
	public void saveObject(String employeeid, A_Employee employee) throws Exception{
		A_Employee emplObject = this.employeeService.get(employeeid);
		if (emplObject == null){
			throw new BusinessException(depict + "当前人员信息未获取到，无法注册添加！");
		}
		this.saveObject(emplObject, employee);
	}

	/**
	 * 保存对象添加
	 * @param branch
	 */
	public void saveObject(A_Employee emplObject, A_Employee employee) throws Exception{
		long num = this.objectDao.getCount("employeeid = '" + emplObject.getId() + "'");
		if (num > 0){
			return;
		}
		H_qualified qualified = new H_qualified();
		qualified.setId(this.getUUID());//设置主键
		qualified.setCompanyid(emplObject.getCompanyid());
		qualified.setEmployeeid(emplObject.getId());
		qualified.setNickname(emplObject.getNickname());
		qualified.setRealname(emplObject.getRealname());
		qualified.setSymbol(emplObject.getSymbol());
		qualified.setGradecode(hyjbzc1);
		qualified.setGradename(hyjbzc2);
		qualified.setMoneys(0d);
		qualified.setSpends(0d);
		qualified.setPoints(0d);
		qualified.setAdddate(employee.getAdddate());
		qualified.setAddemployeeid(employee.getAddemployeeid());
		qualified.setAddemployeename(employee.getAddemployeename());
		qualified.setDeleted("0");
		objectDao.save(qualified);
		this.sysLogService.saveObject(Config.LOGTYPEBC, this.depict + qualified.getId() + qualified.getRealname(), employee, H_qualifiedServiceImpl.class);
	}
}
