package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import com.fsd.admin.model.Z_tutor;
import com.fsd.admin.service.Z_tutorService;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_tutortrain;
import com.fsd.admin.service.Z_tutortrainService;
import com.fsd.admin.dao.Z_tutortrainDao;

@Repository("z_tutortrainServiceImpl")
public class Z_tutortrainServiceImpl extends BaseServiceImpl<Z_tutortrain, String> implements Z_tutortrainService{
    
    private static final Logger log = Logger.getLogger(Z_tutortrainServiceImpl.class);
    private String depict = "";
    
    @Resource(name = "z_tutortrainDaoImpl")
	public void setBaseDao(Z_tutortrainDao Z_tutortrainDao) {
		super.setBaseDao(Z_tutortrainDao);
	}
	
	@Resource(name = "z_tutortrainDaoImpl")
	private Z_tutortrainDao objectDao;

    @Autowired
    private Z_tutorService tutorService;
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
    @Override
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("f_deleted", "0"));
		c.addOrder(Order.asc("f_adddate"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("tutorid") != null && !"".equals(objectMap.get("tutorid"))){
				c.add(Restrictions.eq("f_tutorid", objectMap.get("tutorid")));
			}else{
				c.add(Restrictions.eq("f_tutorid", "abc"));
			}
		}
		return objectDao.findPager(param , c);
	}
    
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
    @Override
	public void save(Z_tutortrain obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
            Z_tutortrain old_obj = objectDao.get(obj.getId());

			old_obj.setF_content(obj.getF_content());
			old_obj.setF_starttime(obj.getF_starttime());
			old_obj.setF_days(obj.getF_days());
			old_obj.setF_address(obj.getF_address());
			old_obj.setF_sponsor(obj.getF_sponsor());

			//设置修改日期
            old_obj.setF_lastupdatedate(this.getData());
			//设置修改用户id
			old_obj.setF_updateemployeeid(employee.getId());
			//设置修改用户姓名
			old_obj.setF_updateemployeename(employee.getRealname());
            objectDao.update(old_obj);
        }else{
            //添加
			//设置主键
            obj.setId(this.getUUID());

			Z_tutor tutor = tutorService.get(obj.getF_tutorid());
			obj.setF_name(tutor.getF_name());

			//设置添加日期
			obj.setF_adddate(this.getData());
			//设置添加用户id
			obj.setF_addemployeeid(employee.getId());
			//设置添加用户姓名
			obj.setF_addemployeename(employee.getRealname());
			//设置删除标志(0:正常 1：已删除)
			obj.setF_deleted("0");
			objectDao.save(obj);
        }
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
    @Override
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		for (String id : dir) {
			objectDao.delete(id);
		}
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
    @Override
	public Z_tutortrain getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		Z_tutortrain obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		
		return obj;
	}

	/**
	 * 根据导师ID加载培训信息
	 * @param tutorid
	 * @return
	 * @throws Exception
	 */
	public List<Z_tutortrain> getListByTutorid(String tutorid) throws Exception{
    	String sql = "select * from Z_tutortrain where f_tutorid = ? and f_deleted = ?";
    	return objectDao.queryBySql1(sql, tutorid, "0");
	}
}
