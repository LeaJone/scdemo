package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import org.springframework.stereotype.Repository;

import com.fsd.core.common.BusinessException;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.util.Config;
import com.fsd.core.util.ParametersUtil;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.A_notify;
import com.fsd.admin.service.A_notifyService;
import com.fsd.admin.dao.A_notifyDao;
import com.google.gson.Gson;

@Repository("A_notifyServiceImpl")
public class A_notifyServiceImpl extends MainServiceImpl<A_notify, String> implements A_notifyService{
    
    private static final Logger log = Logger.getLogger(A_notifyServiceImpl.class);
    private String depict = "通知";
    
    @Resource(name = "A_notifyDaoImpl")
	public void setBaseDao(A_notifyDao A_notifyDao) {
		super.setBaseDao(A_notifyDao);
	}
	
	@Resource(name = "A_notifyDaoImpl")
	private A_notifyDao objectDao;
	
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		c.addOrder(Order.desc("firstly"));
		c.addOrder(Order.desc("adddate"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("title") != null && !"".equals(objectMap.get("title"))){
				c.add(Restrictions.like("title", "%" + objectMap.get("title") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}
	
	/**
	 * 加载所有数据
	 * @return
	 * @throws Exception
	 */
	public List<A_notify> getObjectListQY(A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.add(Restrictions.eq("status", "tzqy"));
		c.addOrder(Order.desc("firstly"));
		c.addOrder(Order.desc("adddate"));
		c.add(Restrictions.or(
				Property.forName("overdue").isNull(),
				Restrictions.eq("overdue", ""),
//				Restrictions.ne("overdue", null),
				Restrictions.ge("overdue", this.getData1() + "000000")
				));
		return objectDao.getList(c);
	}
	
	/**
	 * 保存角色
	 * @param popedomgroup
	 * @throws Exception
	 */
	public void saveObject(A_notify obj, A_Employee employee) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			A_notify obj_old = objectDao.get(obj.getId());
			obj_old.setTitle(obj.getTitle());
			obj_old.setAbstracts(obj.getAbstracts());
			obj_old.setContent(obj.getContent());
			obj_old.setType(obj.getType());
			obj_old.setTypename(obj.getTypename());
			obj_old.setOverdue(obj.getOverdue());
			obj_old.setRemark(obj.getRemark());
			obj_old.setStatus(obj.getStatus());
			obj_old.setStatusname(obj.getStatusname());
			obj_old.setUpdatedate(this.getData());//设置修改日期
			obj_old.setUpdateemployeeid(employee.getId());//设置修改用户id
			obj_old.setUpdateemployeename(employee.getRealname());//设置修改用户姓名
			objectDao.update(obj_old);
		}else{
			//添加
			obj.setId(this.getUUID());//设置主键
			obj.setCompanyid(employee.getCompanyid());//所属单位
			obj.setAdddate(this.getData());//设置添加日期
			obj.setAddemployeeid(employee.getId());//设置添加用户id
			obj.setAddemployeename(employee.getRealname());//设置添加用户姓名
			obj.setFirstly("false");
			obj.setDeleted("0");//设置删除标志(0:正常 1：已删除)
			objectDao.save(obj);
		}
		this.sysLogService.saveObject(Config.LOGTYPEBC, this.depict + obj.getId() + obj.getTitle(), employee, A_notifyServiceImpl.class);
	}
	
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public A_notify getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		A_notify obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		return obj;
	}
	
	/**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		objectDao.executeHql("update A_notify t set t.deleted = '1' where t.id in (" + ids + ")");
	}
	
	/**
	 * 修改对象置顶状态
	 * @param parameters
	 */
	@SuppressWarnings("unchecked")
	public void updateFirstlyObject(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		boolean isEnabled = (Boolean) objectMap.get("isenabled");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		if (isEnabled)
			objectDao.executeHql("update A_notify t set t.firstly = 'true' where t.id in (" + ids + ")");
		else
			objectDao.executeHql("update A_notify t set t.firstly = 'false' where t.id in (" + ids + ")");
	}
	
	/**
	 * 修改对象状态
	 * @param parameters
	 */
	@SuppressWarnings("unchecked")
	public void updateEnabledObject(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		boolean isEnabled = (Boolean) objectMap.get("isenabled");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		if (isEnabled)
			objectDao.executeHql("update A_notify t set t.status = 'tzqy', t.statusname = '启用' where t.id in (" + ids + ")");
		else
			objectDao.executeHql("update A_notify t set t.status = 'tzty', t.statusname = '停用' where t.id in (" + ids + ")");
	}
}
