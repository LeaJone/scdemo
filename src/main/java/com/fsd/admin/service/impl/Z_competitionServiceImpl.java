package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_competition;
import com.fsd.admin.service.Z_competitionService;
import com.fsd.admin.dao.Z_competitionDao;

@Repository("z_competitionServiceImpl")
public class Z_competitionServiceImpl extends BaseServiceImpl<Z_competition, String> implements Z_competitionService{
    
    private static final Logger log = Logger.getLogger(Z_competitionServiceImpl.class);
    private String depict = "";
    
    @Resource(name = "z_competitionDaoImpl")
	public void setBaseDao(Z_competitionDao Z_competitionDao) {
		super.setBaseDao(Z_competitionDao);
	}
	
	@Resource(name = "z_competitionDaoImpl")
	private Z_competitionDao objectDao;
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
    @Override
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("f_deleted", "0"));
		c.addOrder(Order.asc("f_adddate"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("name") != null && !"".equals(objectMap.get("name"))	){
				c.add(Restrictions.like("f_name", "%" + objectMap.get("name") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}
    
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
    @Override
	public void save(Z_competition obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
            Z_competition old_obj = objectDao.get(obj.getId());

			old_obj.setF_name(obj.getF_name());
			old_obj.setF_typeid(obj.getF_typeid());
			old_obj.setF_typename(obj.getF_typename());
			old_obj.setF_levelid(obj.getF_levelid());
			old_obj.setF_levelname(obj.getF_levelname());
			old_obj.setF_sponsor(obj.getF_sponsor());
			old_obj.setF_imgurl(obj.getF_imgurl());
			old_obj.setF_starttime(obj.getF_starttime());
			old_obj.setF_stoptime(obj.getF_stoptime());

            objectDao.update(old_obj);
        }else{
            //添加
			//设置主键
            obj.setId(this.getUUID());
			obj.setF_adddate(this.getData());
			obj.setF_deleted("0");
			objectDao.save(obj);
        }
    }

    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
    @Override
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		for (String id : dir) {
			Z_competition competition = objectDao.get(id);
			competition.setF_deleted("1");
			objectDao.update(competition);
		}
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
    @Override
	public Z_competition getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		Z_competition obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		
		return obj;
	}
}
