package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import org.springframework.stereotype.Repository;

import com.fsd.core.common.BusinessException;
import com.fsd.core.util.Config;
import com.fsd.core.util.ParametersUtil;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.F_flowproject;
import com.fsd.admin.service.F_flowprojectService;
import com.fsd.admin.dao.F_flowprojectDao;
import com.google.gson.Gson;

@Repository("f_flowprojectServiceImpl")
public class F_flowprojectServiceImpl extends MainServiceImpl<F_flowproject, String> implements F_flowprojectService{
	//bpmn2.0  业务流程建模与标注
	
	//流程项目状态 f_fpxmzt
	/**
	 * 启用状态
	 */
	public static final String fpqy1 = "f_fpqy";
	public static final String fpqy2 = "启用";
	/**
	 * 停用状态
	 */
	public static final String fpty1 = "f_fpty";
	public static final String fpty2 = "停用";
	
	//流程受理状态 f_fpslzt
	/**
	 * 启用状态
	 */
	public static final String fpslqy1 = "f_fpslqy";
	public static final String fpslqy2 = "启用";
	/**
	 * 停用状态
	 */
	public static final String fpslty1 = "f_fpslty";
	public static final String fpslty2 = "启用";
		
    private static final Logger log = Logger.getLogger(F_flowprojectServiceImpl.class);
    private String depict = "流程项目";
    
    @Resource(name = "f_flowprojectDaoImpl")
	public void setBaseDao(F_flowprojectDao f_flowprojectDao) {
		super.setBaseDao(f_flowprojectDao);
	}
	
	@Resource(name = "f_flowprojectDaoImpl")
	private F_flowprojectDao objectDao;
	
	/**
	 * 根据数据对象ID加载对象
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public F_flowproject getObjectByCode(String code, String companyid)  throws Exception{
		String sql = "from F_flowproject t where companyid = '" + companyid + "' and code = '" + code + "'";
		List<F_flowproject> objList = objectDao.queryByHql(sql);
		if (objList != null && objList.size() > 0){
			return objList.get(0);
		}
		return null;
	}
	
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		c.addOrder(Order.asc("sort"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("typeid") != null && !"".equals(objectMap.get("typeid"))){
				c.add(Restrictions.eq("typeid", objectMap.get("typeid")));
			}
			if(objectMap.get("name") != null && !"".equals(objectMap.get("name"))){
				c.add(Restrictions.like("name", "%" + objectMap.get("name") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}

    
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public void save(F_flowproject obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
        	F_flowproject old_obj = objectDao.get(obj.getId());
        	old_obj.setName(obj.getName());
        	old_obj.setDescribe(obj.getDescribe());
        	old_obj.setOperatename(obj.getOperatename());
        	old_obj.setOperatepath(obj.getOperatepath());
        	old_obj.setEntityname(obj.getEntityname());
        	old_obj.setEntitypath(obj.getEntitypath());
        	old_obj.setWindowname(obj.getWindowname());
        	old_obj.setWindowpath(obj.getWindowpath());
        	old_obj.setSort(obj.getSort());
        	old_obj.setStatus(fpty1);
        	old_obj.setStatusname(fpty2);
        	old_obj.setAcceptcode(fpslty1);
        	old_obj.setAcceptname(fpslty2);
        	old_obj.setUpdatedate(this.getData());//设置修改日期
        	old_obj.setUpdateemployeeid(employee.getId());//设置修改用户id
        	old_obj.setUpdateemployeename(employee.getRealname());//设置修改用户姓名
            objectDao.update(old_obj);
        }else{
            //添加
            obj.setId(this.getUUID());//设置主键
			obj.setCompanyid(employee.getCompanyid());//所属单位
			obj.setAdddate(this.getData());//设置添加日期
			obj.setAddemployeeid(employee.getId());//设置添加用户id
			obj.setAddemployeename(employee.getRealname());//设置添加用户姓名
			obj.setDeleted("0");//设置删除标志(0:正常 1：已删除)
			obj.setStatus(fpty1);
			obj.setStatusname(fpty2);
			obj.setAcceptcode(fpslty1);
			obj.setAcceptname(fpslty2);
			objectDao.save(obj);
        }
        this.sysLogService.saveObject(Config.LOGTYPEBC, this.depict + obj.getId() + obj.getName(), employee, F_flowprojectServiceImpl.class);
	}
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = this.getSQLinByIDList(dir);
		objectDao.executeHql("update F_flowproject t set t.deleted = '1', t.updatedate = '" + this.getData() + 
				"', t.updateemployeeid = '" + employee.getId() + "', t.updateemployeename = '" + employee.getRealname() + 
				"' where t.id in (" + ids + ")");
		this.sysLogService.saveObject(Config.LOGTYPESC, this.depict + ids, employee, F_flowprojectServiceImpl.class);
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public F_flowproject getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		F_flowproject obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		return obj;
	}
	
	/**
	 * 修改状态
	 * @param parameters
	 */
	@SuppressWarnings("unchecked")
	public void updateEnabledObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> idList = (ArrayList<String>) objectMap.get("ids");
		String ids = this.getSQLinByIDList(idList);
		boolean isEnabled = (Boolean) objectMap.get("isenabled");
		if (objectMap.get("type").toString().equals("lcsl")){
			if (isEnabled){
				objectDao.executeHql("update F_flowproject t set t.acceptcode = '" + fpslqy1 + "', t.acceptname = '启用', t.updatedate = '" + this.getData() + 
					"', t.updateemployeeid = '" + employee.getId() + "', t.updateemployeename = '" + employee.getRealname() + 
					"' where t.id in (" + ids + ")");
				this.sysLogService.saveObject(Config.LOGTYPEBC, this.depict + " 修改流程受理状态“启用” " + ids, employee, F_flowprojectServiceImpl.class);
			}else{
				objectDao.executeHql("update F_flowproject t set t.acceptcode = '" + fpslty1 + "', t.acceptname = '停用', t.updatedate = '" + this.getData() + 
					"', t.updateemployeeid = '" + employee.getId() + "', t.updateemployeename = '" + employee.getRealname() + 
					"' where t.id in (" + ids + ")");
				this.sysLogService.saveObject(Config.LOGTYPEBC, this.depict + " 修改流程受理状态“停用” " + ids, employee, F_flowprojectServiceImpl.class);
			}
		}else{
			if (isEnabled){
				objectDao.executeHql("update F_flowproject t set t.status = '" + fpqy1 + "', t.statusname = '启用', t.updatedate = '" + this.getData() + 
					"', t.updateemployeeid = '" + employee.getId() + "', t.updateemployeename = '" + employee.getRealname() + 
					"' where t.id in (" + ids + ")");
				this.sysLogService.saveObject(Config.LOGTYPEBC, this.depict + " 修改项目状态“启用” " + ids, employee, F_flowprojectServiceImpl.class);
			}else{
				objectDao.executeHql("update F_flowproject t set t.status = '" + fpty1 + "', t.statusname = '停用', t.updatedate = '" + this.getData() + 
					"', t.updateemployeeid = '" + employee.getId() + "', t.updateemployeename = '" + employee.getRealname() + 
					"' where t.id in (" + ids + ")");
				this.sysLogService.saveObject(Config.LOGTYPEBC, this.depict + " 修改项目状态“停用” " + ids, employee, F_flowprojectServiceImpl.class);
			}
		}
	}
	
	/**
	 * 顺序排序
	 * @param parameters
	 */
	public void updateSortObject(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		List<String> idList = (List<String>) objectMap.get("ids");
		String ids = this.getSQLinByIDList(idList);
		F_flowproject objDW = objectDao.get(objectMap.get("id").toString());
		if (objDW == null)
			throw new BusinessException(depict + "获取定位对象为空!");
		List<F_flowproject> objList = objectDao.queryByHql("from F_flowproject t where " +
				"t.typeid = '" + objDW.getTypeid() + "' and t.deleted = '0' " +
				"and t.id not in (" + ids + ") " +
				"and t.sort >= " + objDW.getSort() + " order by t.sort asc");
		long sort = objDW.getSort();
		for (String id : idList) {
			objectDao.executeHql("update F_flowproject t set t.sort = " + sort + " where t.id = '" + id + "'");
			sort++;
		}
		for (F_flowproject obj : objList) {
			objectDao.executeHql("update F_flowproject t set t.sort = " + sort + " where t.id = '" + obj.getId() + "'");
			sort++;
		}
	}
}
