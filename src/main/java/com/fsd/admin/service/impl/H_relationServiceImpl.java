package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.stereotype.Repository;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.Config;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.H_relation;
import com.fsd.admin.service.A_EmployeeService;
import com.fsd.admin.service.H_qualifiedService;
import com.fsd.admin.service.H_relationService;
import com.fsd.admin.service.Sys_SystemParametersService;
import com.fsd.admin.dao.H_relationDao;
import com.google.gson.Gson;

@Repository("h_relationServiceImpl")
public class H_relationServiceImpl extends MainServiceImpl<H_relation, String> implements H_relationService{

	private String depict = "会员关联：";
	
    @Resource(name = "h_relationDaoImpl")
	public void setBaseDao(H_relationDao objectDao) {
		super.setBaseDao(objectDao);
	}
	
	@Resource(name = "h_relationDaoImpl")
	private H_relationDao objectDao;
	
	@Resource(name = "Sys_SystemParametersServiceImpl")
	private Sys_SystemParametersService sysParamService;
	
	//用户
    @Resource(name = "A_EmployeeServiceImpl")
	private A_EmployeeService employeeService;
    @Resource(name = "h_qualifiedServiceImpl")
	private H_qualifiedService qualifiedService;

	
	/**
	 * 保存对象添加
	 * @param branch
	 */
	public void saveObject(ParametersUtil param, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
		if(objectMap.get("id") == null || "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "当前人员信息ID未获取到，无法注册添加！");
		}
		this.saveObject(objectMap.get("id").toString(), employee);
	}
	/**
	 * 保存对象添加
	 * @param branch
	 */
	public void saveObject(String employeeid, A_Employee employee) throws Exception{
		this.qualifiedService.saveObject(employeeid, employee);
		long num = objectDao.getCount("companyid = '" + employee.getCompanyid() + "'", "employeeid = '" + employeeid + "'");
		if (num > 0){
			return;
		}
		H_relation relation = new H_relation();
		relation.setId(this.getUUID());//设置主键
		relation.setCompanyid(employee.getCompanyid());
		relation.setEmployeeid(employeeid);
		relation.setAdddate(employee.getAdddate());
		relation.setAddemployeeid(employee.getAddemployeeid());
		relation.setAddemployeename(employee.getAddemployeename());
		objectDao.save(relation);
		this.sysLogService.saveObject(Config.LOGTYPEBC, this.depict + employee.getId() + employee.getRealname(), employee, H_relationServiceImpl.class);
	}
	/**
	 * 保存对象添加
	 * @param branch
	 */
	public void saveObject(A_Employee emplObject, A_Employee employee) throws Exception{
		this.qualifiedService.saveObject(emplObject, employee);
		long num = objectDao.getCount("companyid = '" + employee.getCompanyid() + "'", "employeeid = '" + emplObject.getId() + "'");
		if (num > 0){
			return;
		}
		H_relation relation = new H_relation();
		relation.setId(this.getUUID());//设置主键
		relation.setCompanyid(employee.getCompanyid());
		relation.setEmployeeid(emplObject.getId());
		relation.setAdddate(employee.getAdddate());
		relation.setAddemployeeid(employee.getAddemployeeid());
		relation.setAddemployeename(employee.getAddemployeename());
		objectDao.save(relation);
		this.sysLogService.saveObject(Config.LOGTYPEBC, this.depict + employee.getId() + employee.getRealname(), employee, H_relationServiceImpl.class);
	}
	
	/**
	 * 保存对象添加
	 * @param branch
	 */
	public void saveObjectList(ParametersUtil param, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		for (String id : dir) {
			this.saveObject(id, employee);
		}
	}
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = this.getSQLinByIDList(dir);
		String hql = "delete from H_relation where companyid = '" + employee.getCompanyid() + "' and employeeid in (" + ids + ")";
		objectDao.executeHql(hql);
		this.sysLogService.saveObject(Config.LOGTYPESC, this.depict + ids, employee, H_relationServiceImpl.class);
	}
}
