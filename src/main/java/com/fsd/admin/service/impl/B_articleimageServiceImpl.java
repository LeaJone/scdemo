package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import javax.annotation.Resource;
import org.springframework.stereotype.Repository;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.Config;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.B_articleimage;
import com.fsd.admin.service.B_articleimageService;
import com.fsd.admin.dao.B_articleimageDao;
import com.google.gson.Gson;

@Repository("b_articleimageServiceImpl")
public class B_articleimageServiceImpl extends MainServiceImpl<B_articleimage, String> implements B_articleimageService{
    
    private static final Logger log = Logger.getLogger(B_articleimageServiceImpl.class);
    private String depict = "组图";
    
    @Resource(name = "b_articleimageDaoImpl")
	public void setBaseDao(B_articleimageDao b_articleimageDao) {
		super.setBaseDao(b_articleimageDao);
	}
	
	@Resource(name = "b_articleimageDaoImpl")
	private B_articleimageDao objectDao;

	/**
	 * 根据文章ID加载数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<B_articleimage> getObjectListByArticleId(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		c.addOrder(Order.asc("sort"));
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
		c.add(Restrictions.eq("articleid", ""+ objectMap.get("fid") +""));
		return  objectDao.getList(c);
	}
	
	/**
	 * 根据文章ID加载数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageListByArticleId(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		c.addOrder(Order.asc("sort"));
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
		c.add(Restrictions.eq("articleid", ""+ objectMap.get("fid") +""));
		return  objectDao.findPager(param, c);
	}
	
	/**
	 * 保存对象（添加、修改）
	 * @param obj
	 * @param a_employee
	 * @param a_company
	 * @throws Exception
	 */
	public void saveObject(B_articleimage obj, A_Employee employee) throws Exception{
		if(obj.getId() != null  && !"".equals(obj.getId())){
			//修改
			B_articleimage old_obj = objectDao.get(obj.getId());
			if(old_obj == null){
				throw new BusinessException(depict + "数据不存在!");
			}
			old_obj.setSort(obj.getSort());
			old_obj.setImageurl(obj.getImageurl());
			old_obj.setIsaurl(obj.getIsaurl());
			old_obj.setAurl(obj.getAurl());
			old_obj.setStatus(obj.getStatus());
			old_obj.setStatusname(obj.getStatusname());
			old_obj.setTitle(obj.getTitle());
			old_obj.setContent(obj.getContent());
			old_obj.setUpdatedate(this.getData());
			old_obj.setUpdateemployeeid(employee.getId());
			old_obj.setUpdateemployeename(employee.getRealname());
			objectDao.update(old_obj);
		}else{
			//新增
			obj.setId(this.getUUID());
			obj.setCompanyid(employee.getCompanyid());
			obj.setAdddate(this.getData());
			obj.setAddemployeeid(employee.getId());
			obj.setAddemployeename(employee.getRealname());
			obj.setDeleted("0");//0标记为 未删除
			objectDao.save(obj);
		}
		this.sysLogService.saveObject(Config.LOGTYPEBC, this.depict + obj.getId() + obj.getTitle(), employee, B_articleimageServiceImpl.class);
	}
	
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public B_articleimage getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		B_articleimage obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		return obj;
	}
	
	/**
	 * 删除对象
	 * @param parameters
	 */
	@SuppressWarnings("unchecked")
	public void delObject(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		objectDao.executeHql("update B_articleimage t set t.deleted = '1' where t.id in (" + ids + ")");
	}
	
	/**
	 * 修改状态
	 * @param parameters
	 */
	@SuppressWarnings("unchecked")
	public void updateEnabledObject(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		boolean isEnabled = (Boolean) objectMap.get("isenabled");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		if (isEnabled)
			objectDao.executeHql("update B_articleimage t set t.status = 'wzztqy', t.statusname = '启用' where t.id in (" + ids + ")");
		else
			objectDao.executeHql("update B_articleimage t set t.status = 'wzztty', t.statusname = '停用' where t.id in (" + ids + ")");
	}
}
