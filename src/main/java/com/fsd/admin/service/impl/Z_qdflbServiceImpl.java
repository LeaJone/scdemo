package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.bean.Tree;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_qdflb;
import com.fsd.admin.service.Z_qdflbService;
import com.fsd.admin.dao.Z_qdflbDao;

@Repository("z_qdflbServiceImpl")
public class Z_qdflbServiceImpl extends BaseServiceImpl<Z_qdflb, String> implements Z_qdflbService{
    
    private static final Logger log = Logger.getLogger(Z_qdflbServiceImpl.class);
    private String depict = "";
    
    @Resource(name = "z_qdflbDaoImpl")
	public void setBaseDao(Z_qdflbDao Z_qdflbDao) {
		super.setBaseDao(Z_qdflbDao);
	}
	
	@Resource(name = "z_qdflbDaoImpl")
	private Z_qdflbDao objectDao;
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		c.addOrder(Order.asc("adddate"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("title") != null && !"".equals(objectMap.get("title"))){
				c.add(Restrictions.like("title", "%" + objectMap.get("title") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}
    
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public void save(Z_qdflb obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
            Z_qdflb old_obj = objectDao.get(obj.getId());
            
            old_obj.setParentid(obj.getParentid());
            old_obj.setParentname(obj.getParentname());
            old_obj.setTitle(obj.getTitle());
            old_obj.setImageurl1(obj.getImageurl1());
            old_obj.setImageurl2(obj.getImageurl2());
            old_obj.setSort(obj.getSort());
            old_obj.setIsleaf(obj.getIsleaf());
            old_obj.setIsaurl(obj.getIsaurl());
            old_obj.setAurl(obj.getAurl());
            old_obj.setRemark(obj.getRemark());
            
            old_obj.setUpdatedate(this.getData());//设置修改日期
			old_obj.setUpdateemployeeid(employee.getId());//设置修改用户id
			old_obj.setUpdateemployeename(employee.getRealname());//设置修改用户姓名
            objectDao.update(old_obj);
        }else{
            //添加
            obj.setId(this.getUUID());//设置主键
			obj.setCompanyid(employee.getCompanyid());//所属单位
			obj.setAdddate(this.getData());//设置添加日期
			obj.setAddemployeeid(employee.getId());//设置添加用户id
			obj.setAddemployeename(employee.getRealname());//设置添加用户姓名
			obj.setDeleted("0");//设置删除标志(0:正常 1：已删除)
			obj.setIsleaf("true");//新添加的必然是叶子节点
			objectDao.save(obj);
			
			//将其父节点改为非叶子节点
			objectDao.executeHql("update Z_qdflb t set t.isleaf = 'false' where t.id = '" + obj.getParentid() + "'");
        }
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		String hql = "";
		objectDao.executeHql("update Z_qdflb t set t.deleted = '1', t.updatedate = '" + this.getData() + 
				"', t.updateemployeeid = '" + employee.getId() + "', t.updateemployeename = '" + employee.getRealname() + 
				"' where t.id in (" + ids + ")");
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public Z_qdflb getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		Z_qdflb obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		
		return obj;
	}
	
	/**
	 * 异步加载对象树
	 * @return
	 * @throws Exception
	 */
	public List<Tree> getAsyncObjectTree(String fid, A_Employee employee) throws Exception{
		List<Z_qdflb> list = objectDao.queryByHql("from Z_qdflb where companyid = ? and parentid = ?", employee.getCompanyid(), fid);
		List<Tree> treeList = new ArrayList<Tree>();
		Tree tree = null;
		for(Z_qdflb obj: list){
			tree = new Tree();
			tree.setId(obj.getId());
			tree.setText(obj.getTitle());
			tree.setLeaf(Boolean.valueOf(obj.getIsleaf()));
			treeList.add(tree);
		}
		return treeList;
	}
}
