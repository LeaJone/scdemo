package com.fsd.admin.service.impl;

import com.fsd.admin.model.*;
import com.fsd.admin.service.*;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.ParametersUtil;
import com.google.gson.Gson;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import java.util.ArrayList;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.stereotype.Repository;
import com.fsd.admin.dao.F_prospectusDao;

/**
 * 计划任务书Service实现类
 * @author Administrator
 *
 */
@Repository("f_prospectusServiceImpl")
public class F_prospectusServiceImpl extends MainServiceImpl<F_prospectus, String> implements F_prospectusService{
    
    private static final Logger log = Logger.getLogger(F_prospectusServiceImpl.class);
    private static String depict = "文章：";
    
    @Resource(name = "f_prospectusDaoImpl")
	public void setBaseDao(F_prospectusDao prospectusDao) {
		super.setBaseDao(prospectusDao);
	}
	
	@Resource(name = "f_prospectusDaoImpl")
	private F_prospectusDao objectDao;
	
	/**
	 * 加载执行计划书
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getProspectusPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("f_deleted", "0"));
		c.addOrder(Order.desc("f_submittime"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("title") != null && !"".equals(objectMap.get("title"))){
				c.add(Restrictions.like("f_projectname", "%" + objectMap.get("title") + "%"));
			}
		}
		return objectDao.findPager(param, c);
	}
	
	/**
	 * 删除执行计划书（未提交的）
	 * @param parameters
	 */
	public void deletedProspectus(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = this.getSQLinByIDList(dir);
		objectDao.executeHql("update F_prospectus p set p.f_deleted = '1' where p.id in (" + ids + ") and p.f_submitstatus = 'wtj'");
	}
	
	/**
	 * 保存执行计划书
	 * @param obj
	 */
	public void saveProspectusFirst(F_prospectus obj, A_Employee employee) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			F_prospectus obj_old = objectDao.get(obj.getId());
			obj_old.setF_projectid(obj.getF_projectid());
			obj_old.setF_projectname(obj.getF_projectname());
			obj_old.setF_projectcontent(obj.getF_projectcontent());
			obj_old.setF_accessory(obj.getF_accessory());
			obj_old.setF_submituserid(employee.getId());
			obj_old.setF_submitusername(employee.getRealname());
			obj_old.setF_submittime(this.getData());
			obj_old.setF_deleted("0");
			objectDao.update(obj_old);
		}else{
			//保存
			obj.setId(this.getUUID());//设置主键
			obj.setF_submituserid(employee.getId());
			obj.setF_submitusername(employee.getRealname());
			obj.setF_submittime(this.getData());
			obj.setF_submitstatus("wtj"); //未提交
			obj.setF_deleted("0");
			objectDao.save(obj);
		}
	}
	
	/**
	 * 提交执行计划书
	 * @param obj
	 */
	public void saveProspectus(F_prospectus obj, A_Employee employee) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			F_prospectus obj_old = objectDao.get(obj.getId());
			obj_old.setF_projectid(obj.getF_projectid());
			obj_old.setF_projectname(obj.getF_projectname());
			obj_old.setF_projectcontent(obj.getF_projectcontent());
			obj_old.setF_accessory(obj.getF_accessory());
			obj_old.setF_submituserid(employee.getId());
			obj_old.setF_submitusername(employee.getRealname());
			obj_old.setF_submittime(this.getData());
			obj_old.setF_deleted("0");
			objectDao.update(obj_old);
		}else{
			//提交
			obj.setId(this.getUUID());//设置主键
			obj.setF_submituserid(employee.getId());
			obj.setF_submitusername(employee.getRealname());
			obj.setF_submittime(this.getData());
			obj.setF_submitstatus("ytj"); //已提交
			obj.setF_deleted("0");
			objectDao.save(obj);
		}
	}
	
	/**
	 * 根据ID加载邮件对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getProspectusById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		F_prospectus obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		ParametersUtil util = new ParametersUtil();
		util.addSendObject("data", obj);
		return util;
	} 
}
