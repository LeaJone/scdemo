package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.Config;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.J_posts;
import com.fsd.admin.model.Z_tutor;
import com.fsd.admin.service.J_postsService;
import com.fsd.admin.dao.J_postsDao;

@Service("j_postsServiceImpl")
public class J_postsServiceImpl extends MainServiceImpl<J_posts, String> implements J_postsService{

    private String depict = "帖子";
    
    @Resource(name = "j_postsDaoImpl")
	public void setBaseDao(J_postsDao J_postsDao) {
		super.setBaseDao(J_postsDao);
	}
	
	@Resource(name = "j_postsDaoImpl")
	private J_postsDao objectDao;
	
	/**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
    @Override
	public void save(J_posts obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
        	J_posts old_obj = objectDao.get(obj.getId());
			old_obj.setTitle(obj.getTitle());
			old_obj.setContent(obj.getContent());
            objectDao.update(old_obj);
        }
    }
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.addOrder(Order.desc("pubtime"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("fid") != null && !"".equals(objectMap.get("fid")) && !J_boardServiceImpl.FSDBOARD.equals(objectMap.get("fid"))){
				c.add(Restrictions.eq("boardid", objectMap.get("fid")));
			}
			if(objectMap.get("blackid") != null && !"".equals(objectMap.get("blackid"))){
				c.add(Restrictions.eq("pubuserid", objectMap.get("blackid")));
			}
			if(objectMap.get("title") != null && !"".equals(objectMap.get("title"))){
				c.add(Restrictions.like("title", "%" + objectMap.get("title") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> idList = (ArrayList<String>) objectMap.get("ids");
		String ids = this.getSQLinByIDList(idList);
		objectDao.executeHql("update J_posts set deleted = '1' where id in (" + ids + ")");
		this.sysLogService.saveObject(Config.LOGTYPESC, this.depict + ids, employee, J_postsServiceImpl.class);
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public J_posts getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		J_posts obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		return obj;
	}
	
	/**
	 * 置顶对象
	 * @param parameters
	 */
	public void updateFirstlyObject(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> idList = (ArrayList<String>) objectMap.get("ids");
		String ids = this.getSQLinByIDList(idList);
		String status = objectMap.get("firstly").toString();
		objectDao.executeHql("update J_posts set firstly = '"+ status +"' where id in (" + ids + ")");
	}
}
