package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.fsd.core.common.BusinessException;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.util.Config;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Branch;
import com.fsd.admin.model.A_CompanyType;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Sys_PopedomAllocate;
import com.fsd.admin.model.Sys_PopedomGroup;
import com.fsd.admin.service.Sys_PopedomAllocateService;
import com.fsd.admin.service.Sys_PopedomGroupService;
import com.fsd.admin.dao.Sys_PopedomGroupDao;
import com.google.gson.Gson;

@Repository("Sys_PopedomGroupServiceImpl")
public class Sys_PopedomGroupServiceImpl extends MainServiceImpl<Sys_PopedomGroup, String> implements Sys_PopedomGroupService{
    
	private String depict = "角色";
	
    @Resource(name = "Sys_PopedomGroupDaoImpl")
	public void setBaseDao(Sys_PopedomGroupDao Sys_PopedomGroupDao) {
		super.setBaseDao(Sys_PopedomGroupDao);
	}
	
	@Resource(name = "Sys_PopedomGroupDaoImpl")
	private Sys_PopedomGroupDao objectDao;
	
    @Resource(name = "Sys_PopedomAllocateServiceImpl")
	private Sys_PopedomAllocateService allocateService;
	
	
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		c.addOrder(Order.asc("sort"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("name") != null && !"".equals(objectMap.get("name"))){
				c.add(Restrictions.like("name", "%" + objectMap.get("name") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}
	
	/**
	 * 加载所有数据
	 * @return
	 * @throws Exception
	 */
	public List<Sys_PopedomGroup> getObjectList(A_Employee employee) throws Exception{
		return objectDao.queryByHql("from Sys_PopedomGroup a where a.companyid = '"+ employee.getCompanyid() +"' order by sort asc");
	}
	
	/**
	 * 加载所有数据
	 * @return
	 * @throws Exception
	 */
	public List<Sys_PopedomGroup> getObjectListEmployee(A_Employee employee) throws Exception{
		String sql = "";
		if (!employee.isIsadmin()){
			List<Sys_PopedomAllocate> groupList = 
					allocateService.getObjectBySql(
							"from Sys_PopedomAllocate t where t.popedomtype = '" + 
					Config.POPEDOMUSER + "' and t.belongid = '" + employee.getId() + "'");
			for (Sys_PopedomAllocate obj : groupList) {
				if (!"".equals(sql)){
					sql += ",";
				}
				sql += "'" + obj.getPopedomid() + "'";
			}
			sql = " and a.id in (" + sql + ") ";
		}
		sql = "from Sys_PopedomGroup a where a.companyid = '"+ employee.getCompanyid() + "' " + sql + " order by sort asc";
		return objectDao.queryByHql(sql);
	}
	
	/**
	 * 保存角色
	 * @param popedomgroup
	 * @throws Exception
	 */
	public void saveObject(Sys_PopedomGroup obj, A_Employee employee) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			Sys_PopedomGroup obj_old = objectDao.get(obj.getId());
			obj_old.setName(obj.getName());
			obj_old.setSymbol(obj.getSymbol());
			obj_old.setSort(obj.getSort());
			obj_old.setRemark(obj.getRemark());
			objectDao.update(obj_old);
		}else{
			//添加
			obj.setId(this.getUUID());
			obj.setCompanyid(employee.getCompanyid());
			objectDao.save(obj);
		}
		this.sysLogService.saveObject(Config.LOGTYPEBC, this.depict + obj.getId() + obj.getName(), employee, Sys_PopedomGroupServiceImpl.class);
	}
	
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public Sys_PopedomGroup getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		Sys_PopedomGroup popedomgroup = objectDao.get(objectMap.get("id").toString());
		if(popedomgroup == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		return popedomgroup;
	}
	
	/**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		for(int i=0;i<dir.size();i++){
			objectDao.delete(dir.get(i).toString());
		}
	}

	/**
	 * 根据Code获得对象
	 * @param code
	 * @return
	 * @throws Exception
	 */
	@Override
	public Sys_PopedomGroup getObjectByCode(String code) throws Exception{
		String sql = "select * from Sys_PopedomGroup where code = ?";
		List<Sys_PopedomGroup> list = objectDao.queryBySql1(sql, code);
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}
}
