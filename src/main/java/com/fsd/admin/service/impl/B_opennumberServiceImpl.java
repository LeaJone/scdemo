package com.fsd.admin.service.impl;

import javax.annotation.Resource;
import org.springframework.stereotype.Repository;
import org.apache.log4j.Logger;

import com.fsd.admin.model.B_opennumber;
import com.fsd.admin.service.B_opennumberService;
import com.fsd.admin.dao.B_opennumberDao;

@Repository("b_opennumberServiceImpl")
public class B_opennumberServiceImpl extends MainServiceImpl<B_opennumber, String> implements B_opennumberService{
    
    private static final Logger log = Logger.getLogger(B_opennumberServiceImpl.class);
    private String depict = "打开次数";
    
    @Resource(name = "b_opennumberDaoImpl")
	public void setBaseDao(B_opennumberDao b_opennumberDao) {
		super.setBaseDao(b_opennumberDao);
	}
	
	@Resource(name = "b_opennumberDaoImpl")
	private B_opennumberDao objectDao;
	

	//记数类型
	/**
	 * 文章打开
	 */
	public static final String jswzdk = "b_jswzdk";
	/**
	 * 点赞数量
	 */
	public static final String jsdzsl = "b_jsdzsl";
}
