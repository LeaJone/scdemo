package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.Config;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.I_testquestion;
import com.fsd.admin.model.I_testquestionterm;
import com.fsd.admin.service.WJ_testquestiontermService;
import com.fsd.admin.dao.I_testquestiontermDao;

@Repository("wj_testquestiontermServiceImpl")
public class WJ_testquestiontermServiceImpl extends MainServiceImpl<I_testquestionterm, String> implements WJ_testquestiontermService{
    
    private static final Logger log = Logger.getLogger(WJ_testquestiontermServiceImpl.class);
    private String depict = "";
    
    @Resource(name = "i_testquestiontermDaoImpl")
	public void setBaseDao(I_testquestiontermDao I_testquestiontermDao) {
		super.setBaseDao(I_testquestiontermDao);
	}
	
	@Resource(name = "i_testquestiontermDaoImpl")
	private I_testquestiontermDao objectDao;
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
        c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		c.add(Restrictions.eq("deleted", "0"));
		c.addOrder(Order.asc("sort"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
        	Gson gs = new Gson();
    		Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
    		if(objectMap.get("fid") != null && !"".equals(objectMap.get("fid"))){
                c.add(Restrictions.eq("testquestionid", objectMap.get("fid")));
    		}
        }
		return objectDao.findPager(param , c);
	}
    
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public void save(I_testquestionterm obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
            I_testquestionterm old_obj = objectDao.get(obj.getId());
            
            old_obj.setTestinfoid(obj.getTestinfoid());
            //old_obj.setTestinfoname(obj.getTestinfoname());
            old_obj.setTestquestionid(obj.getTestquestionid());
            //old_obj.setTestquestionname(obj.getTestquestionname());
            old_obj.setCode(obj.getCode());
            old_obj.setDescription(obj.getDescription());
            old_obj.setIsanswer(obj.getIsanswer());
            old_obj.setParse(obj.getParse());
            old_obj.setScore(obj.getScore());
            old_obj.setSort(obj.getSort());
            old_obj.setRemark(obj.getRemark());
            old_obj.setUpdatedate(this.getData());//设置修改日期
			old_obj.setUpdateemployeeid(employee.getId());//设置修改用户id
			old_obj.setUpdateemployeename(employee.getRealname());//设置修改用户姓名
            objectDao.update(old_obj);
        }else{
            //添加
            obj.setId(this.getUUID());//设置主键
			obj.setCompanyid(employee.getCompanyid());//所属单位
			obj.setAdddate(this.getData());//设置添加日期
			obj.setAddemployeeid(employee.getId());//设置添加用户id
			obj.setAddemployeename(employee.getRealname());//设置添加用户姓名
			obj.setDeleted("0");//设置删除标志(0:正常 1：已删除)
			objectDao.save(obj);
        }
        this.sysLogService.saveObject(Config.LOGTYPEBC, this.depict + obj.getId() + obj.getCode(), employee, WJ_testquestiontermServiceImpl.class);
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = this.getSQLinByIDList(dir);
		objectDao.executeHql("update I_testquestionterm t set t.deleted = '1', t.updatedate = '" + this.getData() + 
				"', t.updateemployeeid = '" + employee.getId() + "', t.updateemployeename = '" + employee.getRealname() + 
				"' where t.id in (" + ids + ")");
		this.sysLogService.saveObject(Config.LOGTYPESC, this.depict + ids, employee, WJ_testquestiontermServiceImpl.class);
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public I_testquestionterm getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		I_testquestionterm obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		return obj;
	}

	/**
	 * 顺序排序
	 * @param parameters
	 */
	public void updateSortObject(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		List<String> idList = (List<String>) objectMap.get("ids");
		String ids = this.getSQLinByIDList(idList);
		I_testquestionterm obj = objectDao.get(objectMap.get("id").toString());
		if (obj == null)
			throw new BusinessException(depict + "获取定位对象为空!");
		List<I_testquestionterm> objList = objectDao.queryByHql("from I_testquestionterm t where " +
				"t.testinfoid = '" + obj.getTestinfoid() + "' and t.deleted = '0' " +
				"and t.id not in (" + ids + ") " +
				"and t.sort >= " + obj.getSort() + " order by t.sort asc");
		long sort = obj.getSort();
		for (String id : idList) {
			objectDao.executeHql("update I_testquestionterm t set t.sort = " + sort + " where t.id = '" + id + "'");
			sort++;
		}
		for (I_testquestionterm obj1 : objList) {
			objectDao.executeHql("update I_testquestionterm t set t.sort = " + sort + " where t.id = '" + obj1.getId() + "'");
			sort++;
		}
	}
}
