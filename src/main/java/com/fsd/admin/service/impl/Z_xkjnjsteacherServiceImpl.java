package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_xkjnjsteacher;
import com.fsd.admin.service.Z_xkjnjsteacherService;
import com.fsd.admin.dao.Z_xkjnjsteacherDao;

@Repository("z_xkjnjsteacherServiceImpl")
public class Z_xkjnjsteacherServiceImpl extends BaseServiceImpl<Z_xkjnjsteacher, String> implements Z_xkjnjsteacherService{
    
    private static final Logger log = Logger.getLogger(Z_xkjnjsteacherServiceImpl.class);
    private String depict = "";
    
    @Resource(name = "z_xkjnjsteacherDaoImpl")
	public void setBaseDao(Z_xkjnjsteacherDao Z_xkjnjsteacherDao) {
		super.setBaseDao(Z_xkjnjsteacherDao);
	}
	
	@Resource(name = "z_xkjnjsteacherDaoImpl")
	private Z_xkjnjsteacherDao objectDao;
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
    @Override
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("f_deleted", "0"));
		c.addOrder(Order.asc("f_adddate"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			c.add(Restrictions.eq("f_fid", objectMap.get("projectid")));
		}else{
			c.add(Restrictions.eq("f_fid", "0"));
		}
		return objectDao.findPager(param , c);
	}
    
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
    @Override
	public void save(Z_xkjnjsteacher obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
            Z_xkjnjsteacher old_obj = objectDao.get(obj.getId());
            
            old_obj.setF_lastupdatedate(this.getData());//设置修改日期
			old_obj.setF_updateemployeeid(employee.getId());//设置修改用户id
			old_obj.setF_updateemployeename(employee.getRealname());//设置修改用户姓名
            objectDao.update(old_obj);
        }else{
            //添加
            obj.setId(this.getUUID());//设置主键
			obj.setF_adddate(this.getData());//设置添加日期
			obj.setF_addemployeeid(employee.getId());//设置添加用户id
			obj.setF_addemployeename(employee.getRealname());//设置添加用户姓名
			obj.setF_deleted("0");//设置删除标志(0:正常 1：已删除)
			objectDao.save(obj);
        }
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
    @Override
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		for (String id : dir) {
			objectDao.delete(id);
		}
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
    @Override
	public Z_xkjnjsteacher getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		Z_xkjnjsteacher obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		
		return obj;
	}

	/**
	 * 根据父级ID加载列表
	 * @param fid
	 * @return
	 * @throws Exception
	 */
	public List<Z_xkjnjsteacher> getListByFid(String fid) throws Exception{
    	String sql = "select * from Z_xkjnjsteacher where f_fid = ?";
    	return objectDao.queryBySql1(sql, fid);
	}
}
