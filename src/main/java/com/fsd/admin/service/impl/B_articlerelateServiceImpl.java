package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import javax.annotation.Resource;
import org.springframework.stereotype.Repository;
import com.fsd.admin.model.B_articlerelate;
import com.fsd.admin.service.B_articlerelateService;
import com.fsd.admin.dao.B_articlerelateDao;

@Repository("b_articlerelateServiceImpl")
public class B_articlerelateServiceImpl extends MainServiceImpl<B_articlerelate, String> implements B_articlerelateService{
    
    private static final Logger log = Logger.getLogger(B_articlerelateServiceImpl.class);
    private String depict = "文章关联";
    
    @Resource(name = "b_articlerelateDaoImpl")
	public void setBaseDao(B_articlerelateDao b_articlerelateDao) {
		super.setBaseDao(b_articlerelateDao);
	}
	
	@Resource(name = "b_articlerelateDaoImpl")
	private B_articlerelateDao objectDao;
	
	//wzgllx  文章关联类型
	/**
     * 文章关联栏目
     */
	public static final String wzgllm1 = "wzgllm";
	public static final String wzgllm2 = "文章关联栏目";
	/**
     * 文章关联相关栏目
     */
	public static final String wzglxg1 = "wzglxg";
	public static final String wzglxg2 = "文章关联相关栏目";
	

	/**
	 * 加载所需ID数据
	 * @return
	 * @throws Exception
	 */
	public List<String> getObjectIDList(String articleID, String joinType) throws Exception{
		List<B_articlerelate> objList = objectDao.queryByHql(
				"from B_articlerelate t where t.articleid = '" + articleID + 
				"' and t.jointype = '" + joinType + "'");
		List<String> idList = new ArrayList<String>();
		for (B_articlerelate obj : objList) {
			idList.add(obj.getJoinid());
		}
		return idList;
	}
	
	/**
	 * 加载所需数据
	 * @return
	 * @throws Exception
	 */
	public List<B_articlerelate> getObjectList(String articleID, String joinType) throws Exception{
		return objectDao.queryByHql("from B_articlerelate t where t.articleid = '" + articleID + "' and t.jointype = '" + joinType + "'");
	}
	
	/**
	 * 保存文章关联
	 */
	public void saveArticleRelate(String articleID, String joinID, String joinType) throws Exception{
		B_articlerelate obj = new B_articlerelate();
		obj.setId(this.getUUID());
		obj.setArticleid(articleID);
		obj.setJoinid(joinID);
		obj.setJointype(joinType);
		objectDao.save(obj);
	}
	
	/**
	 * 删除对象
	 */
	public void delObject(String articleID, String joinType) throws Exception{
		objectDao.executeHql("delete B_articlerelate t where t.articleid = '" + articleID + "' and t.jointype = '" + joinType + "'");
	}
}
