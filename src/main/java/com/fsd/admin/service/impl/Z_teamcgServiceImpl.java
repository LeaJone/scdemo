package com.fsd.admin.service.impl;

import com.fsd.admin.dao.Z_teamcgDao;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_teamcg;
import com.fsd.admin.service.Z_teamcgService;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.util.ParametersUtil;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Repository("z_teamcgServiceImpl")
public class Z_teamcgServiceImpl extends BaseServiceImpl<Z_teamcg, String> implements Z_teamcgService {
    
    @Resource(name = "z_teamcgDaoImpl")
	public void setBaseDao(Z_teamcgDao Z_teamcgDao) {
		super.setBaseDao(Z_teamcgDao);
	}
	
	@Resource(name = "z_teamcgDaoImpl")
	private Z_teamcgDao objectDao;

	/**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	@Override
	public void save(Z_teamcg obj, A_Employee employee) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			Z_teamcg old_obj = objectDao.get(obj.getId());
			old_obj.setContent(obj.getContent());
			old_obj.setFile(obj.getFile());
			old_obj.setSubdate(this.getData());
			objectDao.update(old_obj);
		}else{
			//添加
			obj.setId(this.getUUID());
			obj.setDeleted("0");
			obj.setSubdate(this.getData());
			objectDao.save(obj);
		}
	}

	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	@Override
	public Z_teamcg getObjectById(ParametersUtil parameters) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		if(parameters.getJsonData() != null && !"".equals(parameters.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
			if(objectMap.get("teamid") != null && !"".equals(objectMap.get("teamid"))){
				c.add(Restrictions.eq("teamid", objectMap.get("teamid")));
			}
			if(objectMap.get("type") != null && !"".equals(objectMap.get("type"))){
				c.add(Restrictions.eq("type", objectMap.get("type")));
			}
		}
		List<Z_teamcg> list = objectDao.getList(c);
		if(list.size() != 0 ){
			return list.get(0);
		}else{
			return null;
		}
	}

}
