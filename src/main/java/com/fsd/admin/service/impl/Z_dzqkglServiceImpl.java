package com.fsd.admin.service.impl;

import java.util.Map;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.dao.B_articleDao;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_articlefz;
import com.fsd.admin.service.Z_dzqkglService;
import com.google.gson.Gson;

@Repository("z_dzqkglServiceImpl")
public class Z_dzqkglServiceImpl extends MainServiceImpl<Z_articlefz, String> implements Z_dzqkglService{
    
    private static final Logger log = Logger.getLogger(Z_dzqkglServiceImpl.class);
    private String depict = "电子期刊";
    
   	@Resource(name = "b_articleDaoImpl")
	private B_articleDao articleDao;
   	
    /**
   	 * 加载分页数据
   	 * @param param
   	 * @return
   	 * @throws Exception
   	 */
   	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
   		Criteria c = articleDao.createCriteria();
   		c.add(Restrictions.eq("type", "wzts"));
   		c.add(Restrictions.eq("companyid", employee.getCompanyid()));
   		c.add(Restrictions.eq("deleted", "0"));
   		c.addOrder(Order.desc("adddate"));
   		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
   			Gson gs = new Gson();
   			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
   			if(objectMap.get("name") != null && !"".equals(objectMap.get("name"))){
   				c.add(Restrictions.like("title", "%" + objectMap.get("name") + "%"));
   			}
   		}
   		return articleDao.findPager(param , c);
   	}
       
}
