package com.fsd.admin.service.impl;

import com.fsd.admin.dao.Z_qycgDao;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_qycg;
import com.fsd.admin.service.Z_qycgService;
import com.fsd.core.common.BusinessException;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.util.ParametersUtil;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository("z_qycgServiceImpl")
public class Z_qycgServiceImpl extends BaseServiceImpl<Z_qycg, String> implements Z_qycgService {
    
    @Resource(name = "z_qycgDaoImpl")
	public void setBaseDao(Z_qycgDao Z_qycgDao) {
		super.setBaseDao(Z_qycgDao);
	}
	
	@Resource(name = "z_qycgDaoImpl")
	private Z_qycgDao objectDao;

	/**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	@Override
	public void save(Z_qycg obj, A_Employee employee) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			Z_qycg old_obj = objectDao.get(obj.getId());
			old_obj.setContent(obj.getContent());
			old_obj.setFile(obj.getFile());
			old_obj.setSubdate(this.getData());
			objectDao.update(old_obj);
		}else{
			//添加
			obj.setId(this.getUUID());
			obj.setDeleted("0");
			obj.setSubdate(this.getData());
			objectDao.save(obj);
		}
	}

	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	@Override
	public Z_qycg getObjectById(ParametersUtil parameters) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		if(parameters.getJsonData() != null && !"".equals(parameters.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
			if(objectMap.get("qyid") != null && !"".equals(objectMap.get("qyid"))){
				c.add(Restrictions.eq("qyid", objectMap.get("qyid")));
			}
			if(objectMap.get("type") != null && !"".equals(objectMap.get("type"))){
				c.add(Restrictions.eq("type", objectMap.get("type")));
			}
		}
		List<Z_qycg> list = objectDao.getList(c);
		if(list.size() != 0 ){
			return list.get(0);
		}else{
			return null;
		}
	}

}
