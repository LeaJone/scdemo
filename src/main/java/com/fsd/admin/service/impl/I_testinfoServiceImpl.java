package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.bean.Tree;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.I_testinfo;
import com.fsd.admin.service.I_testinfoService;
import com.fsd.admin.dao.I_testinfoDao;

@Repository("i_testinfoServiceImpl")
public class I_testinfoServiceImpl extends BaseServiceImpl<I_testinfo, String> implements I_testinfoService{
    
    private static final Logger log = Logger.getLogger(I_testinfoServiceImpl.class);
    private String depict = "题卷信息";
    
    @Resource(name = "i_testinfoDaoImpl")
	public void setBaseDao(I_testinfoDao I_testinfoDao) {
		super.setBaseDao(I_testinfoDao);
	}
	
	@Resource(name = "i_testinfoDaoImpl")
	private I_testinfoDao objectDao;
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
		Criteria c = objectDao.createCriteria();
        c.add(Restrictions.eq("companyid", employee.getCompanyid()));
        c.add(Restrictions.eq("bankinfoid", objectMap.get("fid")));
		if(objectMap.get("name") != null && !"".equals(objectMap.get("name"))){
			c.add(Restrictions.like("name", "%" + objectMap.get("name") + "%"));
		}
		c.add(Restrictions.eq("deleted", "0"));
		c.addOrder(Order.asc("adddate"));
		return objectDao.findPager(param , c);
	}
	
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageListAll(ParametersUtil param, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
		Criteria c = objectDao.createCriteria();
        c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		if(objectMap.get("key") != null && !"".equals(objectMap.get("key"))){
			c.add(Restrictions.like("name", "%" + objectMap.get("key") + "%"));
		}
		c.add(Restrictions.eq("deleted", "0"));
		c.addOrder(Order.asc("adddate"));
		return objectDao.findPager(param , c);
	}
    
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public void save(I_testinfo obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
            I_testinfo old_obj = objectDao.get(obj.getId());
            old_obj.setKindcode(obj.getKindcode());
            old_obj.setKindname(obj.getKindname());
            old_obj.setBankinfoid(obj.getBankinfoid());
            old_obj.setBankinfoname(obj.getBankinfoname());
            old_obj.setName(obj.getName());
            old_obj.setSymbol(obj.getSymbol());
            old_obj.setDescription(obj.getDescription());
            old_obj.setStratdate(obj.getStratdate());
            old_obj.setEnddate(obj.getEnddate());
            old_obj.setAnswertime(obj.getAnswertime());
            old_obj.setTotalscore(obj.getTotalscore());
            old_obj.setRemark(obj.getRemark());
            old_obj.setStatus(obj.getStatus());
            old_obj.setStatusname(obj.getStatusname());
            old_obj.setUpdatedate(this.getData());//设置修改日期
			old_obj.setUpdateemployeeid(employee.getId());//设置修改用户id
			old_obj.setUpdateemployeename(employee.getRealname());//设置修改用户姓名
            objectDao.update(old_obj);
        }else{
            //添加
            obj.setId(this.getUUID());//设置主键
			obj.setCompanyid(employee.getCompanyid());//所属单位
			obj.setAdddate(this.getData());//设置添加日期
			obj.setAddemployeeid(employee.getId());//设置添加用户id
			obj.setAddemployeename(employee.getRealname());//设置添加用户姓名
			obj.setDeleted("0");//设置删除标志(0:正常 1：已删除)
			objectDao.save(obj);
        }
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		String hql = "";
		objectDao.executeHql("update I_testinfo t set t.deleted = '1', t.updatedate = '" + this.getData() + 
				"', t.updateemployeeid = '" + employee.getId() + "', t.updateemployeename = '" + employee.getRealname() + 
				"' where t.id in (" + ids + ")");
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public I_testinfo getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		I_testinfo obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		
		return obj;
	}
	
	/**
	 * 组织题卷树
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getTestInfoTree(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		Criteria c = objectDao.createCriteria();
        c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		if(objectMap != null && objectMap.get("key") != null && !"".equals(objectMap.get("key"))){
			c.add(Restrictions.like("name", "%" + objectMap.get("key") + "%"));
		}
		c.add(Restrictions.eq("deleted", "0"));
		c.addOrder(Order.asc("adddate"));
		return objectDao.findPager(parameters, c);
	}
}
