package com.fsd.admin.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import com.fsd.core.bean.FsdFile;
import com.fsd.core.common.BusinessException;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.util.DiskUtil;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Sys_SystemLog;
import com.fsd.admin.service.Sys_TemplateService;
import com.google.gson.Gson;

@Repository("Sys_TemplateServiceImpl")
public class Sys_TemplateServiceImpl extends BaseServiceImpl<Sys_SystemLog, String> implements Sys_TemplateService{

    private String depict = "模板";
    
	
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<FsdFile> getObjectPageList(ParametersUtil param, A_Employee employee, String rootPath) throws Exception{
		String url = rootPath + "template";
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("url") != null && !"".equals(objectMap.get("url"))){
				url = rootPath + objectMap.get("url").toString();
			}
		}
		List<FsdFile> list = DiskUtil.getFileList(url);
		for (int i = 0; i < list.size(); i++) {
			list.get(i).setUrl(list.get(i).getUrl().replaceAll(rootPath, ""));
		}
		return list;
	}
	
	/**
	 * 重命名文件
	 * @param rootPath
	 * @param url
	 * @param name
	 * @throws Exception
	 */
	public void renameFile(String rootPath, String url, String name) throws Exception{
		String[] dir = url.split("/");
		String newPath = "";
		for (int i = 0; i < dir.length; i++) {
			if(i < dir.length-1){
				newPath = newPath + dir[i] + "/";
			}
		}
		File oldfile=new File(rootPath + url); 
        File newfile=new File(rootPath+"/"+newPath + name);
        if(!oldfile.exists()){
        	throw new BusinessException("文件不存在！");
        }
        if(newfile.exists()){//若在该目录下已经有一个文件和新文件名相同，则不允许重命名 
        	throw new BusinessException(name + "已经存在！");
        }else{
        	boolean con = oldfile.renameTo(newfile);
        	if(!con){
        		throw new BusinessException("重命名失败！");
        	}
        }
	}
	
	/**
	 * 删除文件
	 * @param parameters
	 * @param rootPath
	 * @throws Exception
	 */
	public void delFiles(ParametersUtil param, String rootPath) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
		String url = rootPath + objectMap.get("url").toString();
		File file=new File(url);
		if(file.isDirectory()){
			if(DiskUtil.getFileList(url).size() != 0){
				throw new BusinessException("该文件夹不为空，不能删除！");
			}
		}
		file.delete();
	}
	
	/**
	 * 加载文件内容
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public Map<String, String> getFileContent(ParametersUtil parameters, String rootPath) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("url") == null && "".equals(objectMap.get("url"))){
			throw new BusinessException(depict + "获取缺少URL参数!");
		}
		String url = objectMap.get("url").toString();
		File file = new File(rootPath + url);
		InputStreamReader isr = new InputStreamReader(new FileInputStream(file), "UTF-8");
		BufferedReader br = new BufferedReader(isr);//构造一个BufferedReader类来读取文件
		String s = null;
		String result = "";
		while((s = br.readLine())!=null){//使用readLine方法，一次读一行
			result = result + "\n" +s;
		}
		br.close();
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("content", result);
		map.put("url", url);
		return map;
	}
	
	/**
	 * 提交文件内容
	 * @param url
	 * @param content
	 * @param rootPath
	 * @throws Exception
	 */
	public void saveFile(String url, String content, String rootPath) throws Exception{
		File file = new File(rootPath + url);
		OutputStream os = new FileOutputStream(file);
		os.write(content.getBytes());  
		os.flush();  
		os.close();
	}
}
