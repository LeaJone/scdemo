package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;

import com.fsd.admin.entity.Z_jsydspkz;
import com.fsd.core.bean.ImportExcel;
import org.springframework.stereotype.Repository;
import com.fsd.core.common.BusinessException;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.util.BeanToMapUtil;
import com.fsd.core.util.DateTimeUtil;
import com.fsd.core.util.ExcelUtil;
import com.fsd.core.util.ParametersUtil;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_jsydsp;
import com.fsd.admin.service.Z_jsydspService;
import com.fsd.admin.dao.Z_jsydspDao;
import com.google.gson.Gson;

@Repository("z_jsydspServiceImpl")
public class Z_jsydspServiceImpl extends BaseServiceImpl<Z_jsydsp, String> implements Z_jsydspService{
    
    private static final Logger log = Logger.getLogger(Z_jsydspServiceImpl.class);
    private String depict = "";
    
    @Resource(name = "z_jsydspDaoImpl")
	public void setBaseDao(Z_jsydspDao Z_jsydspDao) {
		super.setBaseDao(Z_jsydspDao);
	}
	
	@Resource(name = "z_jsydspDaoImpl")
	private Z_jsydspDao objectDao;
	
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@Override
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		c.addOrder(Order.desc("pzsj"));
		c.addOrder(Order.desc("sort"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("xmmc") != null && !"".equals(objectMap.get("xmmc"))){
				c.add(Restrictions.like("xmmc", "%" + objectMap.get("xmmc") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}
	
	/**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	@Override
	public void save(Z_jsydsp obj, Z_jsydspkz objkz, A_Employee employee) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			Z_jsydsp old_obj = objectDao.get(obj.getId());
			old_obj.setXmmc(obj.getXmmc());
			old_obj.setPzwh(obj.getPzwh());
			old_obj.setPzsj(obj.getPzsj());
			old_obj.setPzmj(obj.getPzmj());
			old_obj.setSort(obj.getSort());
			old_obj.setUpdatedate(this.getData());//设置修改日期
			old_obj.setUpdateemployeeid(employee.getId());//设置修改用户id
			old_obj.setUpdateemployeename(employee.getRealname());//设置修改用户姓名
			
			StringBuffer sb = new StringBuffer();
			sb.append(objkz.getXj() + ",");//农用地
			sb.append(objkz.getGd() + ",");//耕地
			sb.append(objkz.getYd() + ",");//园地
			sb.append(objkz.getWlyd() + ",");//未利用地
			sb.append(objkz.getJsyd() + ",");//建设用地
			sb.append(objkz.getZzyd() + ",");//住宅用地
			sb.append(objkz.getSfyd() + ",");//商服用地
			sb.append(objkz.getGkccyd() + ",");//工矿仓储用地
			sb.append(objkz.getGgglfwyd() + ",");//公共管理与公共服务用地
			sb.append(objkz.getJtysyd() + ",");//交通运输用地
			sb.append(objkz.getSlss() + ",");//水利设施用地
			sb.append(objkz.getNyyd() + ",");//能源用地
			sb.append(objkz.getQt());//其他用地
			
			old_obj.setYdmj(sb.toString());//用地面积
			
			objectDao.update(old_obj);
		}else{
			//添加
			obj.setId(this.getUUID());//设置主键
			obj.setCompanyid(employee.getCompanyid());//所属单位
			obj.setAdddate(this.getData());//设置添加日期
			obj.setAddemployeeid(employee.getId());//设置添加用户id
			obj.setAddemployeename(employee.getRealname());//设置添加用户姓名
			obj.setDeleted("0");//设置删除标志(0:正常 1：已删除)
			obj.setPznf(obj.getPzsj().substring(0, 4));
			
			StringBuffer sb = new StringBuffer();
			sb.append(objkz.getXj() + ",");//农用地
			sb.append(objkz.getGd() + ",");//耕地
			sb.append(objkz.getYd() + ",");//园地
			sb.append(objkz.getWlyd() + ",");//未利用地
			sb.append(objkz.getJsyd() + ",");//建设用地
			sb.append(objkz.getZzyd() + ",");//住宅用地
			sb.append(objkz.getSfyd() + ",");//商服用地
			sb.append(objkz.getGkccyd() + ",");//工矿仓储用地
			sb.append(objkz.getGgglfwyd() + ",");//公共管理与公共服务用地
			sb.append(objkz.getJtysyd() + ",");//交通运输用地
			sb.append(objkz.getSlss() + ",");//水利设施用地
			sb.append(objkz.getNyyd() + ",");//能源用地
			sb.append(objkz.getQt());//其他用地
			
			obj.setYdmj(sb.toString());//用地面积
			
			objectDao.save(obj);
		}
	}
	
	/**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	@Override
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		String hql = "";
		objectDao.executeHql("update Z_jsydsp t set t.deleted = '1', t.updatedate = '" + this.getData() + 
				"', t.updateemployeeid = '" + employee.getId() + "', t.updateemployeename = '" + employee.getRealname() + 
				"' where t.id in (" + ids + ")");
	}
	
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, String> getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		Z_jsydsp obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		Map<String, String> map = BeanToMapUtil.convertBean(obj);
		String []dir = obj.getYdmj().split(",", -1);
		map.put("xj", dir[0]);
		map.put("gd", dir[1]);
		map.put("yd", dir[2]);
		map.put("wlyd", dir[3]);
		map.put("jsyd", dir[4]);
		map.put("zzyd", dir[5]);
		map.put("sfyd", dir[6]);
		map.put("gkccyd", dir[7]);
		map.put("ggglfwyd", dir[8]);
		map.put("jtysyd", dir[9]);
		map.put("slss", dir[10]);
		map.put("nyyd", dir[11]);
		map.put("qt", dir[12]);
		return map;
	}
	
	/**
	 * 导入Excel
	 * @param obj
	 * @param employee
	 * @param diskUrl
	 * @throws Exception
	 */
	@Override
	public void saveImportObject(ImportExcel obj, A_Employee employee, String diskUrl) throws Exception{
		System.out.println(diskUrl);
		System.out.println(obj.getExcel());
		List<Object> list = ExcelUtil.readExcel(diskUrl + obj.getExcel(), Integer.parseInt(obj.getSheet()), Integer.parseInt(obj.getKsh()), Integer.parseInt(obj.getKsl()), Integer.parseInt(obj.getHs()), Integer.parseInt(obj.getLs()));
		for (int i = 0; i < list.size(); i++) {
			List<Object> olist = (List<Object>) list.get(i);
			
			Z_jsydsp jsydsp = new Z_jsydsp();
			jsydsp.setId(this.getUUID());//设置主键
			jsydsp.setCompanyid(employee.getCompanyid());//所属单位
			jsydsp.setAdddate(this.getData());//设置添加日期
			jsydsp.setAddemployeeid(employee.getId());//设置添加用户id
			jsydsp.setAddemployeename(employee.getRealname());//设置添加用户姓名
			jsydsp.setDeleted("0");//设置删除标志(0:正常 1：已删除)
			
			jsydsp.setSort(Long.valueOf(olist.get(0).toString().trim()));//顺序号
			
			jsydsp.setXmmc(olist.get(1).toString().trim());//项目名称
			jsydsp.setXmwz(olist.get(2).toString().trim());//项目位置
			
			String pzsj = DateTimeUtil.formatDate(olist.get(4).toString().trim(), "dd/MM/yyyy", "yyyyMMdd");
			jsydsp.setPzsj(pzsj);//批准时间
			
			DateTimeUtil dateutile = new DateTimeUtil(pzsj, "yyyyMMdd");
			jsydsp.setPznf(String.valueOf(dateutile.getYear()));//批准年份
			jsydsp.setPzwh("甘政国土发【"+String.valueOf(dateutile.getYear())+"】"+olist.get(0).toString().trim());//批准文号
			jsydsp.setPzmj(olist.get(5).toString().trim());//批准面积
			
			StringBuffer sb = new StringBuffer();
			sb.append(olist.get(6).toString().trim() + ",");//农用地
			sb.append(olist.get(7).toString().trim() + ",");//耕地
			sb.append(olist.get(8).toString().trim() + ",");//园地
			sb.append(olist.get(9).toString().trim() + ",");//未利用地
			sb.append(olist.get(10).toString().trim() + ",");//建设用地
			sb.append(olist.get(11).toString().trim() + ",");//住宅用地
			sb.append(olist.get(12).toString().trim() + ",");//商服用地
			sb.append(olist.get(13).toString().trim() + ",");//工矿仓储用地
			sb.append(olist.get(14).toString().trim() + ",");//公共管理与公共服务用地
			sb.append(olist.get(15).toString().trim() + ",");//交通运输用地
			sb.append(olist.get(16).toString().trim() + ",");//水利设施用地
			sb.append(olist.get(17).toString().trim() + ",");//能源用地
			sb.append(olist.get(18).toString().trim());//其他用地
				
			jsydsp.setYdmj(sb.toString());//用地面积
				
				
			objectDao.save(jsydsp);
		}
	}
}
