package com.fsd.admin.service.impl;

import java.util.List;
import javax.annotation.Resource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import com.fsd.admin.dao.E_AutoreplykeyDao;
import com.fsd.admin.model.E_Autoreplykey;
import com.fsd.admin.service.E_AutoreplykeyService;
import com.fsd.admin.service.impl.MainServiceImpl;

@Service("e_autoreplykeyServiceImpl")
public class E_AutoreplykeyServiceImpl extends MainServiceImpl<E_Autoreplykey, String> implements E_AutoreplykeyService {

	private static final Log log = LogFactory.getLog(E_AutoreplykeyServiceImpl.class);
    private String depict = "自动回复关键字";
	
    @Resource(name = "e_autoreplykeyDaoImpl")
   	public void setBaseDao(E_AutoreplykeyDao e_autoreplykeyDao) {
   		super.setBaseDao(e_autoreplykeyDao);
   	}
    
	@Resource(name = "e_autoreplykeyDaoImpl")
	private E_AutoreplykeyDao objectDao;
	
	/**
	 * 彻底删除对象
	 * @param autoReplyID
	 */
	public void delObjectByAutoReplyID(String autoReplyID) throws Exception{
		objectDao.executeHql("delete from E_Autoreplykey where autoreplyid in (" + autoReplyID + ")");
	}
	
	/**
	 * 加载所需集合
	 * @return
	 * @throws Exception
	 */
	public List<E_Autoreplykey> getObjectListByAutoReplyID(String autoReplyID) throws Exception{
		return objectDao.queryByHql("from E_Autoreplykey a where a.autoreplyid = '"+ autoReplyID +"'");	
	}
	
	/**
	 * 加载微信号所需集合
	 * @return
	 * @throws Exception
	 */
	public List<E_Autoreplykey> getObjectListByWeChatID(String weChatID) throws Exception{
		return objectDao.queryByHql("from E_Autoreplykey a where a.wechatid = '"+ weChatID +"'");	
	}
}
