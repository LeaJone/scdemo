package com.fsd.admin.service.impl;

import com.fsd.admin.dao.N_newsversionDao;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.N_newsversion;
import com.fsd.admin.service.N_newsversionService;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.ParametersUtil;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Map;

@Repository("n_newsversionServiceImpl")
public class N_newsversionServiceImpl extends MainServiceImpl<N_newsversion, String> implements N_newsversionService{

    private String depict = "新闻客户端版本";
    
    @Resource(name = "n_newsversionDaoImpl")
	public void setBaseDao(N_newsversionDao n_newsversionDao) {
		super.setBaseDao(n_newsversionDao);
	}
	
	@Resource(name = "n_newsversionDaoImpl")
	private N_newsversionDao objectDao;
	
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@Override
	public ParametersUtil getObjectPageList(ParametersUtil param) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.addOrder(Order.desc("pubdate"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("version") != null && !"".equals(objectMap.get("version"))){
				c.add(Restrictions.eq("version", objectMap.get("version")));
			}
		}
		return objectDao.findPager(param , c);
	}

	/**
	 * 保存对象（添加、修改）
	 * @param obj
	 */
	@Override
	public void saveObject(N_newsversion obj , A_Employee employee) throws Exception{
		if(obj.getId() != null && !"".equals(obj.getId())){
			//修改
			N_newsversion obj_old = objectDao.get(obj.getId());
			obj_old.setVersion(obj.getVersion());
			obj_old.setIosurl(obj.getIosurl());
			obj_old.setAndroidurl(obj.getAndroidurl());
			obj_old.setRemark(obj.getRemark());
			objectDao.update(obj_old);
		}else{
			//添加
			obj.setId(this.getUUID());//设置主键
			obj.setPubdate(this.getData());//设置发布日期
			objectDao.save(obj);
		}
	}
	
	/**
	 * 删除对象
	 * @param parameters
	 */
	@Override
	public void delObject(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		for (String id : dir) {
			objectDao.delete(id);
		}
	}
	
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	@Override
	public N_newsversion getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		N_newsversion obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		return obj;
	}
}
