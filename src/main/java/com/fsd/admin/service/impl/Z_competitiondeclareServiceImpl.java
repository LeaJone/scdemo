package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.Map;
import javax.annotation.Resource;

import com.fsd.admin.model.Z_competition;
import com.fsd.admin.service.Z_competitionService;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_competitiondeclare;
import com.fsd.admin.service.Z_competitiondeclareService;
import com.fsd.admin.dao.Z_competitiondeclareDao;

@Repository("z_competitiondeclareServiceImpl")
public class Z_competitiondeclareServiceImpl extends BaseServiceImpl<Z_competitiondeclare, String> implements Z_competitiondeclareService{
    
    private static final Logger log = Logger.getLogger(Z_competitiondeclareServiceImpl.class);
    private String depict = "";
    
    @Resource(name = "z_competitiondeclareDaoImpl")
	public void setBaseDao(Z_competitiondeclareDao Z_competitiondeclareDao) {
		super.setBaseDao(Z_competitiondeclareDao);
	}
	
	@Resource(name = "z_competitiondeclareDaoImpl")
	private Z_competitiondeclareDao objectDao;

    @Autowired
    private Z_competitionService  competitionService;
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
    @Override
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("f_deleted", "0"));
		c.addOrder(Order.asc("f_adddate"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("name") != null && !"".equals(objectMap.get("name"))){
				c.add(Restrictions.like("f_name", "%" + objectMap.get("name") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}
    
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
    @Override
	public void save(Z_competitiondeclare obj, A_Employee employee) throws Exception{
        if(obj.getId() != null && !"".equals(obj.getId())){
            //修改
            Z_competitiondeclare old_obj = objectDao.get(obj.getId());

			Z_competition competition = competitionService.get(obj.getF_fid());
			old_obj.setF_fname(competition.getF_name());
			old_obj.setF_fid(obj.getF_fid());
			old_obj.setF_name(obj.getF_name());
			old_obj.setF_sbs(obj.getF_sbs());
			old_obj.setF_zipurl(obj.getF_zipurl());
			old_obj.setF_abstract(obj.getF_abstract());
            objectDao.update(old_obj);
        }else{
            //添加
			//设置主键
            obj.setId(this.getUUID());
			Z_competition competition = competitionService.get(obj.getF_fid());
			obj.setF_fname(competition.getF_name());
			obj.setF_adddate(this.getData());
			obj.setF_deleted("0");
			objectDao.save(obj);
        }
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
    @Override
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		for (String id : dir) {
			Z_competitiondeclare competitiondeclare = objectDao.get(id);
			competitiondeclare.setF_deleted("1");
			objectDao.update(competitiondeclare);
		}
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
    @Override
	public Z_competitiondeclare getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		Z_competitiondeclare obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		
		return obj;
	}
}
