package com.fsd.admin.service.impl;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.*;
import javax.annotation.Resource;
import com.fsd.admin.model.A_Branch;
import com.fsd.admin.model.Sys_PopedomAllocate;
import com.fsd.admin.service.A_BranchService;
import com.fsd.admin.service.Sys_PopedomAllocateService;
import com.fsd.core.util.*;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.fsd.core.common.BusinessException;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.service.A_EmployeeService;
import com.fsd.admin.service.Sys_SystemParametersService;
import com.fsd.admin.dao.A_EmployeeDao;
import com.fsd.admin.entity.A_LoginInfo;
import com.google.gson.Gson;
import jxl.Sheet;
import jxl.Workbook;

@Repository("A_EmployeeServiceImpl")
public class A_EmployeeServiceImpl extends MainServiceImpl<A_Employee, String> implements A_EmployeeService{

	private String depict = "";
	
    @Resource(name = "A_EmployeeDaoImpl")
	public void setBaseDao(A_EmployeeDao A_EmployeeDao) {
		super.setBaseDao(A_EmployeeDao);
	}
	
	@Resource(name = "A_EmployeeDaoImpl")
	private A_EmployeeDao objectDao;
	
	@Resource(name = "Sys_SystemParametersServiceImpl")
	private Sys_SystemParametersService parametersService;

	@Autowired
	private Sys_PopedomAllocateService popedomAllocateService;

	@Autowired
	private A_BranchService branchService;
	
	//人员类别  rylb
	/**
     * 普通用户
     */
	public static final String rylbptyh1 = "rylbptyh";
	public static final String rylbptyh2 = "普通用户";
	/**
     * 管理员
     */
	public static final String rylbgly1 = "rylbgly";
	public static final String rylbgly2 = "管理员";
	/**
     * 会员
     */
	public static final String rylbhy1 = "rylbhy";
	public static final String rylbhy2 = "会员";
	
	//人员状态   ryzt
	/**
     * 启用
     */
	public static final String ryztqy1 = "ryztqy";
	public static final String ryztqy2 = "启用";
	/**
     * 停用
     */
	public static final String ryztty1 = "ryztty";
	public static final String ryztty2 = "停用";
	
	/**
	 * 根据登录名和密码得到用户
	 * @param name 登录名
	 * @param password 登陆密码
	 * @return
	 * @throws Exception
	 */
	public A_Employee getObjectLogin(String name , String password) throws Exception{
		List<A_Employee> list = objectDao.queryByHql("from A_Employee e where e.deleted = 0 and e.loginname = '"+name+"' and e.password = '"+password+"'");
		if(list.size() > 0){
			return list.get(0);
		}else{
			return null;
		}
	}
	
	/**
	 * 根据用户名加载对象
	 * @param username
	 * @return
	 * @throws Exception
	 */
	public A_Employee getObjectByUsername(String username){
		List<A_Employee> list = objectDao.queryByHql("from A_Employee where loginname = ? and deleted = 0", username);
		if(list.size() > 0){
			return list.get(0);
		}
		return null;
	}
	
	/**
	 * 加载分页数据，查询管理员、普通用户
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_LoginInfo loginInfo) throws Exception{
		A_Employee employee = loginInfo.getEmployee();
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		c.add(Restrictions.ne("id", "FSDADMIN"));
		c.add(Restrictions.ne("usertypeid", "zj"));  //不是专家
		c.add(Restrictions.sqlRestriction(" loginname not like 'admin%' "));
		c.add(Restrictions.in("type", new String[]{rylbptyh1, rylbgly1}));
		c.addOrder(Order.asc("realname"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if (employee.isIsadmin()){
				if(objectMap.get("fid") != null && !"".equals(objectMap.get("fid"))){
					c.add(Restrictions.or(
							Restrictions.eq("branchid", objectMap.get("fid")),
							Restrictions.eq("companyid", objectMap.get("fid"))
							));
				}
			}else{
				if(objectMap.get("fid") != null && !"".equals(objectMap.get("fid"))){
					List<String> branchIDs = new ArrayList<String>();
					List<String> menuIDs = loginInfo.getPopedomMap().get(Config.POPEDOMMEUN);
					if (objectMap.get("fid").equals(employee.getCompanyid())){
						branchIDs.addAll(loginInfo.getPopedomMap().get(Config.POPEDOMBRANCH));
						if (menuIDs.indexOf(Config.POPEDOMjggly) != -1){//机构管理员
							branchIDs.add(employee.getBranchid());
						}
					}else{
						if (menuIDs.indexOf(Config.POPEDOMjggly) != -1){//机构管理员
							if (employee.getBranchid().equals(objectMap.get("fid"))){
								branchIDs.add(employee.getBranchid());
							}
						}
						if (branchIDs.size() == 0){
							List<String> bIDs = loginInfo.getPopedomMap().get(Config.POPEDOMBRANCH);
							if (bIDs.indexOf(objectMap.get("fid")) != -1){
								branchIDs.add(objectMap.get("fid").toString());
							}
						}
					}
					if (branchIDs.size() == 0){
						branchIDs.add("XXXXXX");
					}
					c.add(Restrictions.in("branchid", branchIDs));
				}
			}
			if(objectMap.get("name") != null && !"".equals(objectMap.get("name"))){
				c.add(Restrictions.or(
						Restrictions.like("realname", "%" + objectMap.get("name") + "%"),
						Restrictions.like("symbol", "%" + objectMap.get("name") + "%")
						));
			}
		}
		return objectDao.findPager(param , c);
	}

	/**
	 * 加载分页数据，查询管理员、普通用户
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getTeamuserPageList(ParametersUtil param, A_LoginInfo loginInfo) throws Exception{
		A_Employee employee = loginInfo.getEmployee();
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		c.add(Restrictions.ne("id", "FSDADMIN"));
		c.add(Restrictions.ne("usertypeid", "qy"));  //不是企业
		c.add(Restrictions.ne("usertypeid", "zj"));  //不是专家
		c.add(Restrictions.sqlRestriction(" loginname not like 'admin%' "));
		c.add(Restrictions.in("type", new String[]{rylbptyh1, rylbgly1}));
		c.addOrder(Order.asc("realname"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
            if(objectMap.get("fid") != null && !"".equals(objectMap.get("fid"))){
                c.add(Restrictions.eq("branchid", objectMap.get("fid")));
            }
			if(objectMap.get("name") != null && !"".equals(objectMap.get("name"))){
				c.add(Restrictions.or(
						Restrictions.like("realname", "%" + objectMap.get("name") + "%"),
						Restrictions.like("symbol", "%" + objectMap.get("name") + "%")
				));
			}
		}
		return objectDao.findPager(param , c);
	}
	
	/**
	 * 加载分页数据，查询管理员、普通用户
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList1(ParametersUtil param, A_LoginInfo loginInfo) throws Exception{
		A_Employee employee = loginInfo.getEmployee();
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		c.add(Restrictions.ne("id", "FSDADMIN"));
		c.add(Restrictions.sqlRestriction(" loginname not like 'admin%' "));
		c.add(Restrictions.in("type", new String[]{rylbptyh1, rylbgly1}));
		c.addOrder(Order.asc("realname"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if (employee.isIsadmin()){
				if(objectMap.get("fid") != null && !"".equals(objectMap.get("fid"))){
					c.add(Restrictions.or(
							Restrictions.eq("branchid", objectMap.get("fid")),
							Restrictions.eq("companyid", objectMap.get("fid"))
							));
				}
			}else{
				if(objectMap.get("fid") != null && !"".equals(objectMap.get("fid"))){
					List<String> branchIDs = new ArrayList<String>();
					List<String> menuIDs = loginInfo.getPopedomMap().get(Config.POPEDOMMEUN);
					if (objectMap.get("fid").equals(employee.getCompanyid())){
						branchIDs.addAll(loginInfo.getPopedomMap().get(Config.POPEDOMBRANCH));
						if (menuIDs.indexOf(Config.POPEDOMjggly) != -1){//机构管理员
							branchIDs.add(employee.getBranchid());
						}
					}else{
						if (menuIDs.indexOf(Config.POPEDOMjggly) != -1){//机构管理员
							if (employee.getBranchid().equals(objectMap.get("fid"))){
								branchIDs.add(employee.getBranchid());
							}
						}
						if (branchIDs.size() == 0){
							List<String> bIDs = loginInfo.getPopedomMap().get(Config.POPEDOMBRANCH);
							if (bIDs.indexOf(objectMap.get("fid")) != -1){
								branchIDs.add(objectMap.get("fid").toString());
							}
						}
					}
					if (branchIDs.size() == 0){
						branchIDs.add("XXXXXX");
					}
					c.add(Restrictions.in("branchid", branchIDs));
				}
			}
			if(objectMap.get("name") != null && !"".equals(objectMap.get("name"))){
				c.add(Restrictions.or(
						Restrictions.like("realname", "%" + objectMap.get("name") + "%"),
						Restrictions.like("symbol", "%" + objectMap.get("name") + "%")
						));
			}
		}
		return objectDao.findPager(param , c);
	}
	
	/**
	 * 加载专家分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getZhuanjiaPageList(ParametersUtil param, A_LoginInfo loginInfo) throws Exception{
		A_Employee employee = loginInfo.getEmployee();
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		c.add(Restrictions.eq("usertypeid", "zj"));  //专家
		c.addOrder(Order.asc("realname"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("name") != null && !"".equals(objectMap.get("name"))){
				c.add(Restrictions.like("realname", "%" + objectMap.get("name") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}
	
	/**
	 * 加载分页数据，查询管理员、普通用户
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageAllList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		c.add(Restrictions.ne("id", "FSDADMIN"));
		c.add(Restrictions.sqlRestriction(" loginname not like 'admin%' "));
		c.add(Restrictions.in("type", new String[]{rylbptyh1, rylbgly1}));
		c.addOrder(Order.asc("branchname"));
		c.addOrder(Order.asc("realname"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("name") != null && !"".equals(objectMap.get("name"))){
				c.add(Restrictions.or(
						Restrictions.like("realname", "%" + objectMap.get("name") + "%"),
						Restrictions.like("symbol", "%" + objectMap.get("name") + "%")
						));
			}
			if(objectMap.get("branchname") != null && !"".equals(objectMap.get("branchname"))){
				c.add(Restrictions.like("branchname", "%" + objectMap.get("branchname") + "%"));
			}
			if(objectMap.get("status") != null && !"".equals(objectMap.get("status"))){
				c.add(Restrictions.eq("status", objectMap.get("status")));
			}
		}
		return objectDao.findPager(param , c);
	}
	
	/**
	 * 加载数据，查询管理员、普通用户
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<A_Employee> getObjectAllList(ParametersUtil param, A_LoginInfo loginInfo) throws Exception{
		A_Employee employee = loginInfo.getEmployee();
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.add(Restrictions.eq("usertypeid", "xs"));
		c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		c.add(Restrictions.ne("id", "FSDADMIN"));
		c.add(Restrictions.sqlRestriction(" loginname not like 'admin%' "));
		c.add(Restrictions.in("type", new String[]{rylbptyh1, rylbgly1}));
		c.addOrder(Order.asc("academyname"));
		c.addOrder(Order.asc("branchname"));
		c.addOrder(Order.asc("realname"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("txt") != null && !"".equals(objectMap.get("txt"))){
				c.add(Restrictions.or(
						Restrictions.like("realname", "%" + objectMap.get("txt") + "%")
						//Restrictions.like("symbol", "%" + objectMap.get("realname") + "%")
						));
			}
			if(objectMap.get("branchname") != null && !"".equals(objectMap.get("branchname"))){
				c.add(Restrictions.like("branchname", "%" + objectMap.get("branchname") + "%"));
			}
			if(objectMap.get("status") != null && !"".equals(objectMap.get("status"))){
				c.add(Restrictions.eq("status", objectMap.get("status")));
			}
		}
		return objectDao.getList(c);
	}
	
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<A_Employee> getObjectListByBranchID(ParametersUtil param, A_LoginInfo loginInfo) throws Exception{
		String branchID = "";
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("branchid") != null && !"".equals(objectMap.get("branchid"))){
				branchID = " and a.branchid = '" + objectMap.get("branchid").toString() + "' ";
			}
		}
		return objectDao.queryByHql("from A_Employee a where a.deleted = '0' "+ branchID +" order by code asc");
	}
	
	/**
	 * 保存对象（添加、修改）
	 */
	public void saveObject(A_Employee employee , A_Employee a_employee) throws Exception{
		if (employee.getLoginname() != null && !employee.getLoginname().equals("")){
			String loginName = employee.getLoginname().toLowerCase();
			if (loginName.indexOf("admin") == 0){ 
				throw new BusinessException(depict + "登录名不得以“admin”字符为前缀!");
			}
		}
		if(employee.getId() != null && !"".equals(employee.getId())){
			//修改
			if("true".equals(employee.getDepartmentadmin())){
				if(checkAcademyidAdmin(employee.getAcademyid(), employee.getId())){
					throw new BusinessException(depict + "用户所属院系已有院系管理员，如要设置当前用户为院系管理员，请先撤销其他院系管理员!");
				}
			}
			List<A_Employee> userList = objectDao.queryByHql("from A_Employee t where t.deleted = '0' and t.id <> '" + employee.getId() + "' and t.loginname = '" + employee.getCode() + "' ");
			if (userList.size() > 0){
				throw new BusinessException(depict + "用户已经存在!");
			}
			userList = objectDao.queryByHql("from A_Employee t where t.deleted = '0' and t.companyid = '" + employee.getCompanyid() + "' " +
					" and t.id <> '" + employee.getId() + "' and t.code = '" + employee.getCode() + "' ");
			if (userList.size() > 0){
				throw new BusinessException(depict + "用户已经存在!");
			}
			A_Employee employee_old = objectDao.get(employee.getId());
			if(employee_old == null){
				throw new BusinessException(depict + "数据不存在!");
			}
			boolean isRealName = false;
			if (!employee_old.getRealname().equals(employee.getRealname()))
				isRealName = true;
			employee_old.setCode(employee.getCode());
			employee_old.setBranchid(employee.getBranchid());
			employee_old.setBranchname(employee.getBranchname());
			employee_old.setLoginname(employee.getCode());
			employee_old.setAcademyid(employee.getAcademyid());
			employee_old.setRegisterclass(employee.getRegisterclass());
			employee_old.setAcademyname(employee.getAcademyname());
			employee_old.setRealname(employee.getRealname());
			employee_old.setSymbol(employee.getSymbol());
			employee_old.setEmail(employee.getEmail());
			employee_old.setUsertypeid(employee.getUsertypeid());
			employee_old.setUsertypename(employee.getUsertypename());
			employee_old.setImagelogo(employee.getImagelogo());
			employee_old.setAreatypeid(employee.getAreatypeid());
			employee_old.setAreatypename(employee.getAreatypename());
			employee_old.setJobtypeid(employee.getJobtypeid());
			employee_old.setJobtypename(employee.getJobtypename());
			employee_old.setSex(employee.getSex());
			employee_old.setSexname(employee.getSexname());
			employee_old.setType(employee.getType());
			employee_old.setTypename(employee.getTypename());
			employee_old.setStatus(employee.getStatus());
			employee_old.setStatusname(employee.getStatusname());
			employee_old.setBlood(employee.getBlood());
			employee_old.setBloodname(employee.getBloodname());
			employee_old.setChildnumber(employee.getChildnumber());
			employee_old.setBodyheight(employee.getBodyheight());
			employee_old.setWeight(employee.getWeight());
			employee_old.setDuty(employee.getDuty());
			employee_old.setMobile(employee.getMobile());
			employee_old.setTelephone(employee.getTelephone());
			employee_old.setRoomnumber(employee.getRoomnumber());
			employee_old.setRemark(employee.getRemark());
			employee_old.setUpdatedate(this.getData());//设置修改日期
			employee_old.setUpdateemployeeid(employee.getId());//设置修改用户id
			employee_old.setUpdateemployeename(employee.getRealname());//设置修改用户姓名
			employee_old.setRegisterclassid(employee.getRegisterclassid());
			employee_old.setRegisterclass(employee.getRegisterclass());
			employee_old.setDepartmentadmin(employee.getDepartmentadmin());
			objectDao.update(employee_old);
			if (isRealName){
				//修改相关模块人员信息
				this.updateAEmployeeInfo(parametersService.getParameterMapByCore(a_employee.getCompanyid()), 
						employee_old.getClass().getSimpleName(), employee_old);
			}
		}else{
			//添加
			if("true".equals(employee.getDepartmentadmin())){
				if(checkAcademyidAdmin(employee.getAcademyid(), employee.getId())){
					throw new BusinessException(depict + "用户所属院系已有院系管理员，如要设置当前用户为院系管理员，请先撤销其他院系管理员!");
				}
			}
			List<A_Employee> userList = objectDao.queryByHql("from A_Employee t where t.deleted = '0' and t.loginname = '" + employee.getCode() + "' ");
			if (userList.size() > 0){
				throw new BusinessException(depict + "用户已经存在!");
			}
			userList = objectDao.queryByHql("from A_Employee t where t.deleted = '0' and t.companyid = '" + employee.getCompanyid() + "' and t.code = '" + employee.getCode() + "' ");
			if (userList.size() > 0){
				throw new BusinessException(depict + "用户已存在!");
			}
			employee.setId(this.getUUID());//设置主键
			employee.setAdddate(this.getData());//设置添加日期
			employee.setAddemployeeid(a_employee.getId());//设置添加用户id
			employee.setAddemployeename(a_employee.getRealname());//设置添加用户姓名
			employee.setPassword(this.getEncipherPassWord(employee.getCompanyid(), "123456"));
			employee.setDeleted("0");//设置删除标志(0:正常 1：已删除)
			employee.setLoginname(employee.getCode());
			objectDao.save(employee);
		}
		this.sysLogService.saveObject(Config.LOGTYPEBC, this.depict + employee.getId() + employee.getRealname(), employee, A_EmployeeServiceImpl.class);
	}
	
	/**
	 * 保存对象（添加、修改）(专家)
	 */
	public String saveZhuanjia(ParametersUtil parameters, A_Employee employee, A_Employee a_employee) throws Exception{
		if (employee.getLoginname() != null && !employee.getLoginname().equals("")){
			String loginName = employee.getLoginname().toLowerCase();
			if (loginName.indexOf("admin") == 0){ 
				throw new BusinessException(depict + "登录名不得以“admin”字符为前缀!");
			}
		}
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") != null && !"".equals(objectMap.get("id"))){
			employee.setId(objectMap.get("id").toString());
		}
		if(employee.getId() != null && !"".equals(employee.getId())){
			//修改
			List<A_Employee> userList = objectDao.queryByHql("from A_Employee where deleted = '0' and id != '" + employee.getId() + "' and loginname = '" + employee.getCode() + "' ");
			if (userList.size() > 0){
				throw new BusinessException(depict + "用户已经存在!");
			}
			userList = objectDao.queryByHql("from A_Employee t where t.deleted = '0' and t.companyid = '" + employee.getCompanyid() + "' " +
					" and t.id <> '" + employee.getId() + "' and t.code = '" + employee.getCode() + "' ");
			if (userList.size() > 0){
				throw new BusinessException(depict + "用户已经存在!");
			}
			A_Employee employee_old = objectDao.get(employee.getId());
			if(employee_old == null){
				throw new BusinessException(depict + "数据不存在!");
			}
			boolean isRealName = false;
			if (!employee_old.getRealname().equals(employee.getRealname()))
				isRealName = true;
			if(employee.getCode() != null){
				employee_old.setCode(employee.getCode());
				employee_old.setLoginname(employee.getCode());
			}
			if(employee.getBranchid() != null)
				employee_old.setBranchid(employee.getBranchid());
			if(employee.getBranchname() != null)
				employee_old.setBranchname(employee.getBranchname());
			if(employee.getAcademyid() != null)
				employee_old.setAcademyid(employee.getAcademyid());
			if(employee.getRegisterclass() != null)
				employee_old.setRegisterclass(employee.getRegisterclass());
			if(employee.getAcademyname() != null)
				employee_old.setAcademyname(employee.getAcademyname());
			if(employee.getRealname() != null)
				employee_old.setRealname(employee.getRealname());
			if(employee.getSymbol() != null)
				employee_old.setSymbol(employee.getSymbol());
			if(employee.getEmail() != null)
				employee_old.setEmail(employee.getEmail());
			if(employee.getUsertypeid() != null)
				employee_old.setUsertypeid(employee.getUsertypeid());
			if(employee.getUsertypename() != null)
				employee_old.setUsertypename(employee.getUsertypename());
			if(employee.getAreatypeid() != null)
				employee_old.setAreatypeid(employee.getAreatypeid());
			if(employee.getAreatypename() != null)
				employee_old.setAreatypename(employee.getAreatypename());
			if(employee.getJobtypeid() != null)
				employee_old.setJobtypeid(employee.getJobtypeid());
			if(employee.getJobtypename() != null)
				employee_old.setJobtypename(employee.getJobtypename());
			if(employee.getSex() != null)
				employee_old.setSex(employee.getSex());
			if(employee.getSexname() != null)
				employee_old.setSexname(employee.getSexname());
			if(employee.getType() != null)
				employee_old.setType(employee.getType());
			if(employee.getTypename() != null)
				employee_old.setTypename(employee.getTypename());
			if(employee.getStatus() != null)
				employee_old.setStatus(employee.getStatus());
			if(employee.getStatusname() != null)
				employee_old.setStatusname(employee.getStatusname());
			if(employee.getBlood() != null)
				employee_old.setBlood(employee.getBlood());
			if(employee.getBloodname() != null)
				employee_old.setBloodname(employee.getBloodname());
			if(employee.getChildnumber() != null)
				employee_old.setChildnumber(employee.getChildnumber());
			if(employee.getBodyheight() != null)
				employee_old.setBodyheight(employee.getBodyheight());
			if(employee.getWeight() != null)
				employee_old.setWeight(employee.getWeight());
			if(employee.getDuty() != null)
				employee_old.setDuty(employee.getDuty());
			if(employee.getMobile() != null)
				employee_old.setMobile(employee.getMobile());
			if(employee.getTelephone() != null)
				employee_old.setTelephone(employee.getTelephone());
			if(employee.getRoomnumber() != null)
				employee_old.setRoomnumber(employee.getRoomnumber());
			if(employee.getRemark() != null)
				employee_old.setRemark(employee.getRemark());
			if(employee.getXueliid() != null)
				employee_old.setXueliid(employee.getXueliid());
			if(employee.getXueliname() != null)
				employee_old.setXueliname(employee.getXueliname());
			if(employee.getXueweiid() != null)
				employee_old.setXueweiid(employee.getXueweiid());
			if(employee.getXueweiname() != null)
				employee_old.setXueweiname(employee.getXueweiname());
			if(employee.getResourceid() != null)
				employee_old.setResourceid(employee.getResourceid());
			if(employee.getResourcename() != null)
				employee_old.setResourcename(employee.getResourcename());
			employee_old.setUsertypeid("zj");
			employee_old.setUsertypename("专家");
			if(employee.getRegisterclassid() != null)
				employee_old.setRegisterclassid(employee.getRegisterclassid());
			if(employee.getRegisterclass() != null)
				employee_old.setRegisterclass(employee.getRegisterclass());
			if(employee.getWorkaddress() != null)
				employee_old.setWorkaddress(employee.getWorkaddress());
			if(employee.getStudymajorid() != null)
				employee_old.setStudymajorid(employee.getStudymajorid());
			if(employee.getStudymajorname() != null)
				employee_old.setStudymajorname(employee.getStudymajorname());
			if(employee.getBusymajorid() != null)
				employee_old.setBusymajorid(employee.getBusymajorid());
			if(employee.getBusymajorname() != null)
				employee_old.setBusymajorname(employee.getBusymajorname());
			employee_old.setUpdatedate(this.getData());//设置修改日期
			employee_old.setUpdateemployeeid(employee.getId());//设置修改用户id
			employee_old.setUpdateemployeename(employee.getRealname());//设置修改用户姓名
			objectDao.update(employee_old);
			if (isRealName){
				//修改相关模块人员信息
				this.updateAEmployeeInfo(parametersService.getParameterMapByCore(a_employee.getCompanyid()), 
						employee_old.getClass().getSimpleName(), employee_old);
			}
			return employee_old.getId();
		}else{
			//添加
			List<A_Employee> userList = objectDao.queryByHql("from A_Employee t where t.deleted = '0' and t.loginname = '" + employee.getCode() + "' ");
			if (userList.size() > 0){
				throw new BusinessException(depict + "用户已经存在!");
			}
			userList = objectDao.queryByHql("from A_Employee t where t.deleted = '0' and t.companyid = '" + employee.getCompanyid() + "' and t.code = '" + employee.getCode() + "' ");
			if (userList.size() > 0){
				throw new BusinessException(depict + "用户已存在!");
			}
			String id = this.getUUID();
			employee.setId(id);//设置主键
			employee.setAdddate(this.getData());//设置添加日期
			employee.setAddemployeeid(a_employee.getId());//设置添加用户id
			employee.setAddemployeename(a_employee.getRealname());//设置添加用户姓名
			employee.setPassword(EncryptUtils.md5Password("123456", "123456"));
			employee.setUsertypeid("zj");
			employee.setUsertypename("专家");
			employee.setDeleted("0");//设置删除标志(0:正常 1：已删除)
			employee.setLoginname(employee.getCode());
			employee.setCompanyid("FSDCOMPANY");
			employee.setCompanyname("兰州工业学院");
			objectDao.save(employee);

			//为新增的专家用户绑定角色
			Sys_PopedomAllocate sys_popedomAllocate = new Sys_PopedomAllocate();
			sys_popedomAllocate.setId(this.getUUID());
			sys_popedomAllocate.setPopedomid("bb3898c4ff4e4d4086a4adcab881345d");
			sys_popedomAllocate.setPopedomtype("qxlxyh");
			sys_popedomAllocate.setBelongid(employee.getId());
			popedomAllocateService.save(sys_popedomAllocate);
			return id;
		}
	}
	
	/**
	 * 保存自行修改
	 */
	public void saveObjectSelf(A_Employee employee , A_Employee a_employee) throws Exception{
		if(employee.getId() != null && !"".equals(employee.getId())){
			//修改
			List<A_Employee> userList = objectDao.queryByHql("from A_Employee t where t.deleted = '0' and t.id <> '" + employee.getId() + "' and t.loginname = '" + employee.getLoginname() + "' ");
			if (userList.size() > 0){
				throw new BusinessException(depict + "登录名已存在!");
			}
			A_Employee employee_old = objectDao.get(employee.getId());
			if(employee_old == null){
				throw new BusinessException(depict + "数据不存在!");
			}
			employee_old.setLoginname(employee.getLoginname());
			employee_old.setRealname(employee.getRealname());
			employee_old.setSymbol(employee.getSymbol());
			employee_old.setEmail(employee.getEmail());
			employee_old.setUsertypeid(employee.getUsertypeid());
			employee_old.setUsertypename(employee.getUsertypename());
			employee_old.setAreatypeid(employee.getAreatypeid());
			employee_old.setAreatypename(employee.getAreatypename());
			employee_old.setJobtypeid(employee.getJobtypeid());
			employee_old.setJobtypename(employee.getJobtypename());
			employee_old.setSex(employee.getSex());
			employee_old.setSexname(employee.getSexname());
			employee_old.setBlood(employee.getBlood());
			employee_old.setBloodname(employee.getBloodname());
			employee_old.setChildnumber(employee.getChildnumber());
			employee_old.setBodyheight(employee.getBodyheight());
			employee_old.setWeight(employee.getWeight());
			employee_old.setDuty(employee.getDuty());
			employee_old.setMobile(employee.getMobile());
			employee_old.setTelephone(employee.getTelephone());
			employee_old.setRoomnumber(employee.getRoomnumber());
			employee_old.setRemark(employee.getRemark());
			employee_old.setUpdatedate(this.getData());//设置修改日期
			employee_old.setUpdateemployeeid(a_employee.getId());//设置修改用户id
			employee_old.setUpdateemployeename(a_employee.getRealname());//设置修改用户姓名
			objectDao.update(employee_old);
		}
		this.sysLogService.saveObject(Config.LOGTYPEBC, this.depict + employee.getId() + employee.getRealname() + "自助修改信息", employee, A_EmployeeServiceImpl.class);
	}
	
	/**
	 * 修改用户所属单位名称
	 */
	public void updateBranchName(String branchID, String branchName) throws Exception{
		objectDao.executeHql("update A_Employee t set t.branchname = '" + branchName + 
				"'  where t.branchid = '" + branchID + "'");
	}
	
	/**
	 * 删除对象
	 * @param parameters
	 */
	@SuppressWarnings("unchecked")
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> idList = (ArrayList<String>) objectMap.get("ids");
		String ids = this.getSQLinByIDList(idList);
		objectDao.executeHql("update A_Employee t set t.deleted = '1', t.updatedate = '" + this.getData() + 
				"', t.updateemployeeid = '" + employee.getId() + "', t.updateemployeename = '" + employee.getRealname() + 
				"' where t.id in (" + ids + ")");
		this.sysLogService.saveObject(Config.LOGTYPESC, this.depict + ids, employee, A_EmployeeServiceImpl.class);
	}
	
	/**
	 * 用户密码重置
	 * @param parameters
	 */
	@SuppressWarnings("unchecked")
	public void updatePwdObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> idList = (ArrayList<String>) objectMap.get("ids");
		String ids = this.getSQLinByIDList(idList);
		String pwd = this.getEncipherPassWord(employee.getCompanyid(), "123456");
		objectDao.executeHql("update A_Employee t set t.password = '" + pwd + "', t.updatedate = '" + this.getData() + 
				"', t.updateemployeeid = '" + employee.getId() + "', t.updateemployeename = '" + employee.getRealname() + 
				"' where t.id in (" + ids + ")");
		this.sysLogService.saveObject(Config.LOGTYPEMM, this.depict + " 重置用户密码 " + ids, employee, A_EmployeeServiceImpl.class);
	}
	
	/**
	 * 修改人员状态
	 * @param parameters
	 */
	@SuppressWarnings("unchecked")
	public void updateEnabledObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> idList = (ArrayList<String>) objectMap.get("ids");
		String ids = this.getSQLinByIDList(idList);
		boolean isEnabled = (Boolean) objectMap.get("isenabled");
		if (isEnabled){
			objectDao.executeHql("update A_Employee t set t.status = 'ryztqy', t.statusname = '启用', t.updatedate = '" + this.getData() + 
				"', t.updateemployeeid = '" + employee.getId() + "', t.updateemployeename = '" + employee.getRealname() + 
				"' where t.id in (" + ids + ")");
			this.sysLogService.saveObject(Config.LOGTYPEBC, this.depict + " 修改“启用” " + ids, employee, A_EmployeeServiceImpl.class);
		}else{
			objectDao.executeHql("update A_Employee t set t.status = 'ryztty', t.statusname = '停用', t.updatedate = '" + this.getData() + 
				"', t.updateemployeeid = '" + employee.getId() + "', t.updateemployeename = '" + employee.getRealname() + 
				"' where t.id in (" + ids + ")");
			this.sysLogService.saveObject(Config.LOGTYPEBC, this.depict + " 修改“停用” " + ids, employee, A_EmployeeServiceImpl.class);
		}
	}
	
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public A_Employee getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException(depict + "获取缺少ID参数!");
		}
		A_Employee obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		return obj;
	}
	
	/**
	 * 修改当前登录用户的密码
	 * @param old_pwd 旧密码
	 * @param new_pwd1 第一遍输入的新密码
	 * @param new_pwd2 第二遍输入的新密码
	 * @throws Exception
	 */
	public A_Employee updateUserPwd(String old_pwd, String new_pwd1, String new_pwd2, A_Employee employee) throws Exception{
		if(null == old_pwd || null == new_pwd1 || null == new_pwd2){
			throw new BusinessException(depict + "参数错误!");
		}
		old_pwd = this.getEncipherPassWord(employee.getCompanyid(), old_pwd);
		if(!old_pwd.equals(employee.getPassword())){
			throw new BusinessException(depict + "旧密码输入错误!");
		}
		if(!new_pwd1.equals(new_pwd2)){
			throw new BusinessException(depict + "两次输入新密码不一致!");
		}
		
		for (int i = 1; i < new_pwd1.length(); i++) {
			if (new_pwd1.substring(i-1, i).equals(new_pwd1.substring(i, i+1)))
				throw new BusinessException(depict + "请不要输入连续相同的字母或数字!");
		}
		new_pwd2 = this.getEncipherPassWord(employee.getCompanyid(), new_pwd2);
		
		String passwordtime = this.getData();
		
		employee.setPassword(new_pwd2);
		employee.setPasswordtime(passwordtime);
		
		objectDao.executeHql("update A_Employee t set t.password = '"+ new_pwd2 +"', t.passwordtime = '"+ passwordtime +"' where t.id = '"+ employee.getId() +"'");
		this.sysLogService.saveObject(Config.LOGTYPEMM, 
				this.depict + " 自行修改密码," + employee.getId() + "-" + employee.getRealname(), 
				employee, A_EmployeeServiceImpl.class);
		return employee;
	}
	
	/**
	 * 根据用户姓名，部门名称，手机，座机 加载数据列表
	 * @param param
	 * @param type
	 * @param text
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectList(ParametersUtil param, String type, String text) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		if("name".equals(type)){
			c.add(Restrictions.like("realname", "%"+text+"%"));
		}else if("jigou".equals(type)){
			c.add(Restrictions.like("branchname", "%"+text+"%"));
		}else if("shouji".equals(type)){
			c.add(Restrictions.like("mobile", "%"+text+"%"));
		}else if("dianhua".equals(type)){
			c.add(Restrictions.like("telephone", "%"+text+"%"));
		}
		return objectDao.findPager(param, c);
	}
	
	/**
	 * 获得加密字符串
	 * @param companyid
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public String getEncipherPassWord(String companyid, String password) throws Exception{
		String type = parametersService.getParameterValueByCode("FSDPWDENCIPHER", companyid);
		if("sha1".equals(type)){
			return SHA1.ToSha1(password);
		}else if("md5".equals(type)){
			return EncryptUtils.md5Password(password, password);
		}else{
			return password;
		}
	}

	/**
	 * 站群专用跳转站点查询管理员方法，根据companyid查询
	 */
	public Object getObjectByCompany(String companyid) throws Exception {
		Criteria c = objectDao.createCriteria();
		Gson gs = new Gson();
		c.add(Restrictions.eq("deleted", "0"));
		c.add(Restrictions.eq("companyid", companyid));
		c.add(Restrictions.like("loginname", "admin%"));
		if(c.list().size() != 0){
			return c.list().get(0);
		}else{
			return null;
		}
	}


	//============================  会员信息  ============================
	/**
	 * 加载分页数据，查询会员
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageListHY(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		c.add(Restrictions.eq("type", rylbhy1));
		c.addOrder(Order.asc("realname"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("name") != null && !"".equals(objectMap.get("name"))){
				c.add(Restrictions.or(
						Restrictions.like("realname", "%" + objectMap.get("name") + "%"),
						Restrictions.like("symbol", "%" + objectMap.get("name") + "%")
						));
			}
		}
		return objectDao.findPager(param, c);
	}
	
	/**
	 * 删除微信绑定
	 * @param employeeid 用户编号
	 * @return
	 * @throws Exception
	 */
	public void delWechatBind(String employeeid) throws Exception {
		A_Employee employee = this.get(employeeid);
		if(employee == null){
			throw new BusinessException(depict + "数据不存在!");
		}
		employee.setIswechatbind("false");
		this.update(employee);
		objectDao.executeHql("update A_Employee t set t.iswechatbind = 'false', t.updatedate = '" + this.getData() + 
				"', t.updateemployeeid = '" + employee.getId() + 
				"', t.updateemployeename = '" + employee.getRealname() + 
				"' where t.id = '" + employeeid + "'");
        this.sysLogService.saveObject(Config.LOGTYPEBC, this.depict + employee.getId() + employee.getRealname() + "解除微信绑定", 
        		employee, A_EmployeeServiceImpl.class);
	}
	
	/**
	 * 修改微信推送状态
	 * @param parameters
	 */
	@SuppressWarnings("unchecked")
	public void updateIsWeChatPushObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> idList = (ArrayList<String>) objectMap.get("ids");
		String ids = this.getSQLinByIDList(idList);
		objectDao.executeHql("update A_Employee t set t.iswechatpush = '" + objectMap.get("iswechatpush") + 
				"', t.updatedate = '" + this.getData() + "', t.updateemployeeid = '" + employee.getId() + 
				"', t.updateemployeename = '" + employee.getRealname() + "' where t.id in (" + ids + ")");
		this.sysLogService.saveObject(Config.LOGTYPEBC, 
					this.depict + "修改微信推送状态 “" + objectMap.get("iswechatpush") + "”" + ids, 
					employee, A_EmployeeServiceImpl.class);
	}
	
	/**
	 * 根据ID加载对象
	 * @return
	 * @throws Exception
	 */
	public A_Employee getUserinfoById(String id) throws Exception{
		return objectDao.get(id);
	}
	
	/**
	 * 统计用户类型数量（学生、教师、专家、企业）
	 * @return
	 * @throws Exception
	 */
	public List<A_Employee> getCountNumbers_xs() throws Exception {
		String sql = "select id from A_Employee where usertypeid = 'xs' and deleted = '0'";
		return objectDao.queryBySql(sql);	
	}
	public List<A_Employee> getCountNumbers_js() throws Exception {
		String sql = "select id from A_Employee where usertypeid = 'js' and deleted = '0'";
		return objectDao.queryBySql(sql);	
	}
	public List<A_Employee> getCountNumbers_zj() throws Exception {
		String sql = "select id from A_Employee where usertypeid = 'zj' and deleted = '0'";
		return objectDao.queryBySql(sql);	
	}
	public List<A_Employee> getCountNumbers_qy() throws Exception {
		String sql = "select id from A_Employee where usertypeid = 'qy' and deleted = '0'";
		return objectDao.queryBySql(sql);	
	}

	
	public void saveXxdr(A_Employee employee , A_Employee a_employee, String rootpath) throws Exception {
		InputStream inputStream=new FileInputStream(rootpath+"/uploadfiles/FSDCOMPANY/video/muban.xls");
		Workbook book=Workbook.getWorkbook(inputStream);
		Sheet sheet=book.getSheet(0);
		List<Object> list=ExcelUtil.readExcel(rootpath+"/uploadfiles/FSDCOMPANY/video/muban.xls", 1, 1, 0, sheet.getRows(), sheet.getColumns());
		for (int i = 1; i < list.size(); i++) {
			String[] s = list.get(i).toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", "").split(",");
			List<String> ls = Arrays.asList(s);
			String code = ls.get(0);
			String name = ls.get(1);
			String sex = ls.get(2);
			String sexname = ls.get(3);
			String academyid = ls.get(4);
			String academyname = ls.get(5);
			String branchid = ls.get(6);
			String branchname = ls.get(7);
			String calss = ls.get(8);
			String idcard = ls.get(9);
			String mobile = ls.get(10);
			String email = ls.get(11);
			String usertypeid = ls.get(12);
			String usertypename = ls.get(13);
			
			A_Employee user = new A_Employee();
			//添加
			List<A_Employee> userList = objectDao.queryByHql("from A_Employee t where t.deleted = '0' and t.loginname = '" + code + "' ");
			if (userList.size() > 0){
				throw new BusinessException(depict + "用户" + code + "已经存在!");
			}
			userList = objectDao.queryByHql("from A_Employee t where t.deleted = '0' and t.companyid = '" + employee.getCompanyid() + "' and t.code = '" + code + "' ");
			if (userList.size() > 0){
				throw new BusinessException(depict + "用户" + code + "已存在!");
			}
			user.setId(this.getUUID());
			user.setCompanyid("FSDCOMPANY");
			user.setCompanyname("兰州工业学院");
			user.setBranchid(branchid);
			user.setBranchname(branchname);
			user.setCode(code);
			user.setLoginname(code);
			user.setPassword(this.getEncipherPassWord(employee.getCompanyid(), "123456"));
			user.setRealname(name);
			//user.setSymbol(symbol);
			user.setUsertypeid(usertypeid);
			user.setUsertypename(usertypename);
			user.setSex(sex);
			user.setSexname(sexname);
			user.setEmail(email);
			user.setType("rylbptyh");
			user.setTypename("普通用户");
			user.setStatus("ryztqy");
			user.setStatusname("启用");
			user.setMobile(mobile);
			user.setIdcard(idcard);
			user.setDeleted("0");
			user.setAdddate(this.getData());
			user.setAddemployeeid(a_employee.getId());
			user.setAddemployeename(a_employee.getRealname());
			user.setAcademyid(academyid);
			user.setAcademyname(academyname);
			user.setRegisterclass(calss);
			
			objectDao.save(user);
		}
	}

	/**
	 * 根据用户类型加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageListByType(ParametersUtil param, A_LoginInfo loginInfo) throws Exception{
		A_Employee employee = loginInfo.getEmployee();
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("deleted", "0"));
		c.add(Restrictions.eq("companyid", employee.getCompanyid()));
		c.addOrder(Order.asc("realname"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
            if(objectMap.get("type") != null && !"".equals(objectMap.get("type"))){
                c.add(Restrictions.eq("usertypeid", objectMap.get("type")));
            }

			if(objectMap.get("name") != null && !"".equals(objectMap.get("name"))){
                c.add(Restrictions.like("realname", "%" + objectMap.get("name") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}

	/**
	 * 检查院系是否已有院系管理员
	 * @param academyid
	 * @param userid
	 * @return
	 * @throws Exception
	 */
	public boolean checkAcademyidAdmin(String academyid, String userid) throws Exception{
		String sql = "select * from A_Employee where academyid = ? and departmentadmin = ? and id <> ?";
		List<A_Employee> list = objectDao.queryBySql1(sql, academyid, "true", userid);
		if(list != null && list.size() > 0){
			return true;
		}
		return false;
	}

	/**
	 * 根据院系ID查询该院系的管理员
	 * @param branchid
	 * @return
	 * @throws Exception
	 */
	@Override
	public A_Employee getDepartMentAdminAcademyid(String branchid) throws Exception{
		String sql = "select * from A_Employee where branchid = ? and departmentadmin = ?";
		List<A_Employee> list = objectDao.queryBySql1(sql, branchid, "true");
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}

	/**
	 * 用户数据和统一身份认证同步
	 * @param employee
	 * @throws Exception
	 */
	@Override
	public void updateSyncEmployee(A_Employee employee) throws Exception {
		LDAPAuthentication ldap = new LDAPAuthentication();
		try{
			ldap.LDAP_connect();
		}catch (Exception e){
			throw new BusinessException("统一身份认证接口连接失败，请联系管理员");
		}

		List<A_Employee> list = this.getAllList();
		List<String> newList = new ArrayList<>();
		for (A_Employee a_employee : list) {
			newList.add(a_employee.getLoginname());
		}

		List<A_Branch> branchList = branchService.getAllList();
		List<Map<String, String>> newBranchList = new ArrayList<>();
		for (A_Branch branch : branchList) {
			Map<String, String> map = new HashMap<>();
			map.put(branch.getName(), branch.getId());
			newBranchList.add(map);
		}

		List<Map<String, String>> teacherList = ldap.getTeacherList();
		for (Map<String, String> stringStringMap : teacherList) {
			A_Employee user = new A_Employee();
			user.setId(stringStringMap.get("gh"));//设置主键
			user.setCode(stringStringMap.get("gh"));
			user.setUsertypeid("js");
			user.setUsertypename("教师");
			user.setDepartmentadmin("false");
			user.setLoginname(stringStringMap.get("gh"));
			user.setRealname(stringStringMap.get("xm"));
			user.setEmail(stringStringMap.get("dzyj"));
			user.setIdcard(stringStringMap.get("sfzh"));
			user.setCompanyid("FSDCOMPANY");
			user.setCompanyname("兰州工业学院");
			user.setType("rylbptyh");
			user.setTypename("普通用户");
			user.setStatus("ryztqy");
			user.setStatusname("启用");
			user.setDeleted("0");//设置删除标志(0:正常 1：已删除)
			user.setAdddate(this.getData());//设置添加日期
			user.setAddemployeeid(employee.getId());//设置添加用户id
			user.setAddemployeename(employee.getRealname());//设置添加用户姓名
			user.setPassword(EncryptUtils.md5Password("cxcy123456", "cxcy123456"));
			String branchName = stringStringMap.get("yx");
			if(branchName.equals("软件学院")){
				branchName = "软件工程学院";
			}
			Map<String, String> branch = getBranchId(newBranchList, branchName);
			if(branch != null){
				user.setBranchid(branch.get(branchName));
				user.setBranchname(branchName);
			}else{
				System.out.println("院系：" + branchName);
				throw new BusinessException("老师数据同步失败，机构数据不同步，请先同步机构信息");
			}
			objectDao.saveOrUpdate(user);

			//为新增的老师用户绑定角色
			Sys_PopedomAllocate sys_popedomAllocate = new Sys_PopedomAllocate();
			sys_popedomAllocate.setId(this.getUUID());
			sys_popedomAllocate.setPopedomid("1d08b21edffc41cc9eb72d243b040ff0");
			sys_popedomAllocate.setPopedomtype("qxlxyh");
			sys_popedomAllocate.setBelongid(user.getId());
			popedomAllocateService.save(sys_popedomAllocate);
		}

		List<Map<String, String>> studentList = ldap.getStudentList();
		for (Map<String, String> stringStringMap : studentList) {
			A_Employee user = new A_Employee();
			user.setId(stringStringMap.get("xh"));//设置主键
			user.setCode(stringStringMap.get("xh"));
			user.setUsertypeid("xs");
			user.setUsertypename("学生");
			user.setDepartmentadmin("false");
			user.setLoginname(stringStringMap.get("xh"));
			user.setRealname(stringStringMap.get("xm"));
			user.setEmail(stringStringMap.get("dzyj"));
			user.setMobile(stringStringMap.get("dh"));
			user.setIdcard(stringStringMap.get("sfzh"));
			user.setCompanyid("FSDCOMPANY");
			user.setCompanyname("兰州工业学院");
			user.setType("rylbptyh");
			user.setTypename("普通用户");
			user.setStatus("ryztqy");
			user.setStatusname("启用");
			user.setDeleted("0");//设置删除标志(0:正常 1：已删除)
			user.setAdddate(this.getData());//设置添加日期
			user.setAddemployeeid(employee.getId());//设置添加用户id
			user.setAddemployeename(employee.getRealname());//设置添加用户姓名
			user.setPassword(EncryptUtils.md5Password("cxcy123456", "cxcy123456"));

			String bj = stringStringMap.get("bj");
			user.setRegisterclass(bj);

			Map<String, String> branch = getBranch(bj);
			if(branch != null){
				user.setBranchid(branch.get("id"));
				user.setBranchname(branch.get("name"));
			}else{
				System.out.println("班级：" + bj);
				throw new BusinessException("学生数据同步失败，机构数据不同步，请先同步机构信息");
			}
			objectDao.saveOrUpdate(user);

			//为新增的学生用户绑定角色
			Sys_PopedomAllocate sys_popedomAllocate = new Sys_PopedomAllocate();
			sys_popedomAllocate.setId(this.getUUID());
			sys_popedomAllocate.setPopedomid("5928b90e43e54bb7955fcf49ff15c040");
			sys_popedomAllocate.setPopedomtype("qxlxyh");
			sys_popedomAllocate.setBelongid(user.getId());
			popedomAllocateService.save(sys_popedomAllocate);
		}
	}

	public Map<String, String> getBranchId(List<Map<String, String>> list, String branchName){
		for (Map<String, String> stringStringMap : list) {
			for(String key : stringStringMap.keySet()){
				if(key.equals(branchName)){
					return stringStringMap;
				}
			}
		}
		return null;
	}


	public Map<String, String> getBranch(String bj){
		Map<String, String> map = new HashMap<>();
		if(bj.indexOf("自动化（本）") != -1){
			map.put("id", "d714ebc09a5a4e148c94e2b5c77eea9b");
			map.put("name", "电气工程学院");
			return map;
		}
		if(bj.indexOf("电气") != -1){
			map.put("id", "d714ebc09a5a4e148c94e2b5c77eea9b");
			map.put("name", "电气工程学院");
			return map;
		}
		if(bj.indexOf("机设（本）") != -1){
			map.put("id", "b5e5cf79ef6a4106b80116cd64235458");
			map.put("name", "机电工程学院");
			return map;
		}
		if(bj.indexOf("软件") != -1){
			map.put("id", "b8231cdc377a46989cb6068dea7cc8e0");
			map.put("name", "软件工程学院");
			return map;
		}
		if(bj.indexOf("图像") != -1){
			map.put("id", "7d1f5608065047ee882d50b038d256e3");
			map.put("name", "艺术设计学院");
			return map;
		}
		if(bj.indexOf("轨道") != -1){
			map.put("id", "d714ebc09a5a4e148c94e2b5c77eea9b");
			map.put("name", "电气工程学院");
			return map;
		}
		if(bj.indexOf("动漫") != -1){
			map.put("id", "7d1f5608065047ee882d50b038d256e3");
			map.put("name", "艺术设计学院");
			return map;
		}
		if(bj.indexOf("机电") != -1){
			map.put("id", "b5e5cf79ef6a4106b80116cd64235458");
			map.put("name", "机电工程学院");
			return map;
		}
		if(bj.indexOf("网络") != -1){
			map.put("id", "b8231cdc377a46989cb6068dea7cc8e0");
			map.put("name", "软件工程学院");
			return map;
		}
		if(bj.indexOf("汽检") != -1){
			map.put("id", "686116010ceb416e845a196969fb12fe");
			map.put("name", "汽车工程学院");
			return map;
		}
		if(bj.indexOf("车辆") != -1){
			map.put("id", "686116010ceb416e845a196969fb12fe");
			map.put("name", "汽车工程学院");
			return map;
		}
		if(bj.indexOf("材控") != -1){
			map.put("id", "cba8de80b63a4b01b3bbc5fa6f8fcc12");
			map.put("name", "材料工程学院");
			return map;
		}
		if(bj.indexOf("焊接技术") != -1){
			map.put("id", "cba8de80b63a4b01b3bbc5fa6f8fcc12");
			map.put("name", "材料工程学院");
			return map;
		}
		if(bj.indexOf("物流") != -1){
			map.put("id", "915c71f2451a4f539039716167f06bc8");
			map.put("name", "经济管理学院");
			return map;
		}
		if(bj.indexOf("金融") != -1){
			map.put("id", "915c71f2451a4f539039716167f06bc8");
			map.put("name", "经济管理学院");
			return map;
		}
		if(bj.indexOf("人力") != -1){
			map.put("id", "915c71f2451a4f539039716167f06bc8");
			map.put("name", "经济管理学院");
			return map;
		}
		if(bj.indexOf("汽电") != -1){
			map.put("id", "686116010ceb416e845a196969fb12fe");
			map.put("name", "汽车工程学院");
			return map;
		}
		if(bj.indexOf("艺术学院") != -1){
			map.put("id", "7d1f5608065047ee882d50b038d256e3");
			map.put("name", "艺术设计学院");
			return map;
		}
		if(bj.indexOf("财管") != -1){
			map.put("id", "915c71f2451a4f539039716167f06bc8");
			map.put("name", "经济管理学院");
			return map;
		}
		if(bj.indexOf("会计") != -1){
			map.put("id", "915c71f2451a4f539039716167f06bc8");
			map.put("name", "经济管理学院");
			return map;
		}
		if(bj.indexOf("营销") != -1){
			map.put("id", "915c71f2451a4f539039716167f06bc8");
			map.put("name", "经济管理学院");
			return map;
		}
		if(bj.indexOf("日语") != -1){
			map.put("id", "f3fdd4a6090c493584f4d721af2ea363");
			map.put("name", "外语学院");
			return map;
		}
		if(bj.indexOf("商英") != -1){
			map.put("id", "f3fdd4a6090c493584f4d721af2ea363");
			map.put("name", "外语学院");
			return map;
		}
		if(bj.indexOf("文秘") != -1){
			map.put("id", "7f4fbf8898554527b3f733e9757aee71");
			map.put("name", "马克思主义学院");
			return map;
		}
		if(bj.indexOf("视觉") != -1){
			map.put("id", "7d1f5608065047ee882d50b038d256e3");
			map.put("name", "艺术设计学院");
			return map;
		}
		if(bj.indexOf("焊接质量") != -1){
			map.put("id", "cba8de80b63a4b01b3bbc5fa6f8fcc12");
			map.put("name", "材料工程学院");
			return map;
		}
		if(bj.indexOf("造价") != -1){
			map.put("id", "c3e5d5a538cd4e25b9208d4c407e0fe5");
			map.put("name", "土木工程学院");
			return map;
		}
		if(bj.indexOf("建设") != -1){
			map.put("id", "c3e5d5a538cd4e25b9208d4c407e0fe5");
			map.put("name", "土木工程学院");
			return map;
		}
		if(bj.indexOf("市政") != -1){
			map.put("id", "c3e5d5a538cd4e25b9208d4c407e0fe5");
			map.put("name", "土木工程学院");
			return map;
		}
		if(bj.indexOf("土木") != -1){
			map.put("id", "c3e5d5a538cd4e25b9208d4c407e0fe5");
			map.put("name", "土木工程学院");
			return map;
		}
		if(bj.indexOf("通信") != -1){
			map.put("id", "c022e6596b074ae1b5153309b736a7e7");
			map.put("name", "电子信息工程学院");
			return map;
		}
		if(bj.indexOf("应电") != -1){
			map.put("id", "c022e6596b074ae1b5153309b736a7e7");
			map.put("name", "电子信息工程学院");
			return map;
		}
		if(bj.indexOf("电信") != -1){
			map.put("id", "c022e6596b074ae1b5153309b736a7e7");
			map.put("name", "电子信息工程学院");
			return map;
		}
		if(bj.indexOf("数媒") != -1){
			map.put("id", "b8231cdc377a46989cb6068dea7cc8e0");
			map.put("name", "软件工程学院");
			return map;
		}
		if(bj.indexOf("汽车") != -1){
			map.put("id", "686116010ceb416e845a196969fb12fe");
			map.put("name", "汽车工程学院");
			return map;
		}
		if(bj.indexOf("机设") != -1){
			map.put("id", "b5e5cf79ef6a4106b80116cd64235458");
			map.put("name", "机电工程学院");
			return map;
		}
		if(bj.indexOf("材料（本）") != -1){
			map.put("id", "cba8de80b63a4b01b3bbc5fa6f8fcc12");
			map.put("name", "材料工程学院");
			return map;
		}
		if(bj.indexOf("理化检测") != -1){
			map.put("id", "cba8de80b63a4b01b3bbc5fa6f8fcc12");
			map.put("name", "材料工程学院");
			return map;
		}
		if(bj.indexOf("焊接") != -1){
			map.put("id", "cba8de80b63a4b01b3bbc5fa6f8fcc12");
			map.put("name", "材料工程学院");
			return map;
		}
		if(bj.indexOf("经济") != -1){
			map.put("id", "915c71f2451a4f539039716167f06bc8");
			map.put("name", "经济管理学院");
			return map;
		}
		if(bj.indexOf("建环") != -1){
			map.put("id", "c3e5d5a538cd4e25b9208d4c407e0fe5");
			map.put("name", "土木工程学院");
			return map;
		}
		if(bj.indexOf("自动化") != -1){
			map.put("id", "d714ebc09a5a4e148c94e2b5c77eea9b");
			map.put("name", "电气工程学院");
			return map;
		}
		if(bj.indexOf("复合材料") != -1){
			map.put("id", "cba8de80b63a4b01b3bbc5fa6f8fcc12");
			map.put("name", "材料工程学院");
			return map;
		}
		if(bj.indexOf("环境设计") != -1){
			map.put("id", "7d1f5608065047ee882d50b038d256e3");
			map.put("name", "艺术设计学院");
			return map;
		}
		if(bj.indexOf("秘书") != -1){
			map.put("id", "7f4fbf8898554527b3f733e9757aee71");
			map.put("name", "马克思主义学院");
			return map;
		}
		if(bj.indexOf("电商") != -1){
			map.put("id", "915c71f2451a4f539039716167f06bc8");
			map.put("name", "经济管理学院");
			return map;
		}
		if(bj.indexOf("给排水") != -1){
			map.put("id", "c3e5d5a538cd4e25b9208d4c407e0fe5");
			map.put("name", "土木工程学院");
			return map;
		}
		if(bj.indexOf("测控") != -1){
			map.put("id", "b5e5cf79ef6a4106b80116cd64235458");
			map.put("name", "机电工程学院");
			return map;
		}
		if(bj.indexOf("应英") != -1){
			map.put("id", "f3fdd4a6090c493584f4d721af2ea363");
			map.put("name", "外语学院");
			return map;
		}
		if(bj.indexOf("应日") != -1){
			map.put("id", "f3fdd4a6090c493584f4d721af2ea363");
			map.put("name", "外语学院");
			return map;
		}
		if(bj.indexOf("机制") != -1){
			map.put("id", "b5e5cf79ef6a4106b80116cd64235458");
			map.put("name", "机电工程学院");
			return map;
		}
		if(bj.indexOf("检测") != -1){
			map.put("id", "cba8de80b63a4b01b3bbc5fa6f8fcc12");
			map.put("name", "材料工程学院");
			return map;
		}
		if(bj.indexOf("国贸") != -1){
			map.put("id", "915c71f2451a4f539039716167f06bc8");
			map.put("name", "经济管理学院");
			return map;
		}
		return null;
	}
}
