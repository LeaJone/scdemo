package com.fsd.admin.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import com.fsd.core.service.impl.BaseServiceImpl;
import com.fsd.core.common.BusinessException;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_projectaudit;
import com.fsd.admin.service.Z_projectauditService;
import com.fsd.admin.dao.Z_projectauditDao;

@Service("z_projectauditServiceImpl")
public class Z_projectauditServiceImpl extends BaseServiceImpl<Z_projectaudit, String> implements Z_projectauditService{
    
    private static final Logger log = Logger.getLogger(Z_projectauditServiceImpl.class);
	
	@Resource(name = "z_projectauditDaoImpl")
	private Z_projectauditDao objectDao;
    
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception{
		Criteria c = objectDao.createCriteria();
		c.add(Restrictions.eq("status", 0L));
		c.addOrder(Order.asc("seq"));
		if(param.getJsonData() != null && !"".equals(param.getJsonData())){
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(param.getJsonData(), Map.class);
			if(objectMap.get("name") != null && !"".equals(objectMap.get("name"))){
				c.add(Restrictions.like("name", "%" + objectMap.get("name") + "%"));
			}
		}
		return objectDao.findPager(param , c);
	}

	/**
	 * 编辑或更新
	 * @param projectid
	 * @param state
	 * @param remark
	 * @throws Exception
	 */
	@Override
	public void saveOrUpdateProjectAudit(String taskName, String projectid, String state, String remark) throws Exception{
		Z_projectaudit projectaudit = this.getProjectAuditByProjectId(projectid);
		if(projectaudit == null){
			projectaudit = new Z_projectaudit();
			projectaudit.setId(this.getUUID());
		}
		projectaudit.setF_projectid(projectid);
		if(taskName.equals("指导老师审核")){
			projectaudit.setF_audit1(state);
			projectaudit.setF_remark1(remark);
		}else if(taskName.equals("初审")){
			projectaudit.setF_audit2(state);
			projectaudit.setF_remark2(remark);
		}else if(taskName.equals("专家评分")){
			projectaudit.setF_audit3(state);
			projectaudit.setF_remark3(remark);
		}else if(taskName.equals("立项审核")){
			projectaudit.setF_audit4(state);
			projectaudit.setF_remark4(remark);
		}else if(taskName.equals("调整申请")){
			projectaudit.setF_audit5(state);
			projectaudit.setF_remark5(remark);
		}
		objectDao.saveOrUpdate(projectaudit);
    }
    
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		ArrayList<String> dir = (ArrayList<String>) objectMap.get("ids");
		String ids = "";
		for (String id : dir) {
			if (!ids.equals("")){
				ids += ",";
			}
			ids += "'" + id + "'";
		}
		String hql = "";
		objectDao.executeHql("update Z_projectaudit t set t.status = -1" + 
				" where t.id in (" + ids + ")");
	}
    
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public Z_projectaudit getObjectById(ParametersUtil parameters) throws Exception{
		Gson gs = new Gson();
		Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
		if(objectMap.get("id") == null && "".equals(objectMap.get("id"))){
			throw new BusinessException("获取缺少ID参数!");
		}
		Z_projectaudit obj = objectDao.get(objectMap.get("id").toString());
		if(obj == null){
			throw new BusinessException("数据不存在!");
		}
		
		return obj;
	}

	/**
	 * 根据项目ID加载项目审核情况
	 * @param projectid
	 * @return
	 * @throws Exception
	 */
	@Override
	public Z_projectaudit getProjectAuditByProjectId(String projectid) throws Exception{
		String sql = "select * from z_projectaudit where f_projectid = ?";
		List<Z_projectaudit> list = objectDao.queryBySql(sql, projectid);
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}
}
