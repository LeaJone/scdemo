package com.fsd.admin.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Sys_SystemMenu;
import com.fsd.admin.model.Sys_SystemMenuPopup;
import com.fsd.admin.service.Sys_SystemMenuPopupService;
import com.fsd.admin.service.Sys_SystemMenuService;
import com.fsd.admin.dao.Sys_SystemMenuPopupDao;
import com.google.gson.Gson;

@Repository("Sys_SystemMenuPopupServiceImpl")
public class Sys_SystemMenuPopupServiceImpl extends MainServiceImpl<Sys_SystemMenuPopup, String> implements Sys_SystemMenuPopupService{
    
    @Resource(name = "Sys_SystemMenuPopupDaoImpl")
	public void setBaseDao(Sys_SystemMenuPopupDao Sys_SystemMenuPopupDao) {
		super.setBaseDao(Sys_SystemMenuPopupDao);
	}
	
	@Resource(name = "Sys_SystemMenuPopupDaoImpl")
	private Sys_SystemMenuPopupDao Sys_SystemMenuPopupDao;
	
	@Resource(name = "Sys_SystemMenuServiceImpl")
	private Sys_SystemMenuService menuService;
	
	/**
	 * 保存用户快捷菜单
	 * @param employee
	 * @param parameters
	 * @throws Exception
	 */
	public void saveObject(A_Employee employee, ParametersUtil parameters) throws Exception{
		if(parameters.getJsonData() != null && !"".equals(parameters.getJsonData())){
			delObjectByEmployeeid(employee.getId());
			Gson gs = new Gson();
			Map objectMap = gs.fromJson(parameters.getJsonData(), Map.class);
			List<String> ids = (List<String>) objectMap.get("ids");
			for (int i = 0; i < ids.size(); i++) {
				Sys_SystemMenuPopup model = new Sys_SystemMenuPopup();
				model.setId(this.getUUID());
				model.setEmployeeid(employee.getId());
				model.setMenucode(ids.get(i));
				model.setSort((long) (i+1));
				Sys_SystemMenuPopupDao.save(model);
			}
		}
	}
	
	/**
	 * 通过用户ID删除该用户的所有快捷菜单
	 * @param Employeeid
	 */
	public void delObjectByEmployeeid(String Employeeid){
		Sys_SystemMenuPopupDao.executeHql("delete from Sys_SystemMenuPopup where employeeid = '"+ Employeeid +"'");
	}
	
	/**
	 * 根据用户ID加载用户的快捷菜单列表
	 * @param Employeeid
	 * @return
	 */
	public List<Sys_SystemMenuPopup> getObjectListByEmployeeid(String Employeeid) throws Exception{
		Criteria criteria = Sys_SystemMenuPopupDao.createCriteria();
		criteria.add(Restrictions.eq("employeeid", Employeeid));
		return Sys_SystemMenuPopupDao.getList(criteria);
	}
}
