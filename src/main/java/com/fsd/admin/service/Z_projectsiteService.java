package com.fsd.admin.service;

import com.fsd.core.util.ParametersUtil;
import com.fsd.core.service.BaseService;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_projectsite;

public interface Z_projectsiteService extends BaseService<Z_projectsite, String>{
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	void save(Z_projectsite obj, A_Employee employee) throws Exception;
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	Z_projectsite getObjectById(ParametersUtil parameters) throws Exception;
	/**
	 * 保存基本信息
	 * @param param
	 * @param obj
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	Z_projectsite saveInfo(ParametersUtil param, String rootPaht, Z_projectsite obj, A_Employee employee) throws Exception;
	/**
	 * 保存项目简介信息
	 * @param param
	 * @param obj
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	Z_projectsite saveAbstract(ParametersUtil param, Z_projectsite obj, A_Employee employee) throws Exception;
	/**
	 * 保存团队风采信息
	 * @param param
	 * @param obj
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	Z_projectsite saveTeammien(ParametersUtil param, Z_projectsite obj, A_Employee employee) throws Exception;
	/**
	 * 保存成果信息
	 * @param param
	 * @param obj
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	Z_projectsite saveAchievement(ParametersUtil param, Z_projectsite obj, A_Employee employee) throws Exception;
	/**
	 * 保存指导老师信息
	 * @param param
	 * @param obj
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	Z_projectsite saveAdviser(ParametersUtil param, Z_projectsite obj, A_Employee employee) throws Exception;
	/**
	 * 保存支撑材料信息
	 * @param param
	 * @param obj
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	Z_projectsite saveMaterials(ParametersUtil param, Z_projectsite obj, A_Employee employee) throws Exception;
	/**
	 * 根据所属项目ID加载对象
	 * @param fid
	 * @return
	 * @throws Exception
	 */
	public Z_projectsite getObjectByFid(String fid) throws Exception;
}
