package com.fsd.admin.service;

import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Sys_seo;
import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;

public interface Sys_seoService extends BaseService<Sys_seo, String> {
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public void save(Sys_seo obj, A_Employee employee) throws Exception;
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public Sys_seo getObjectById(ParametersUtil parameters) throws Exception;
}
