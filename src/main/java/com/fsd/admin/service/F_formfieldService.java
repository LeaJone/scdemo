package com.fsd.admin.service;

import com.fsd.admin.model.F_form;
import com.fsd.core.util.ParametersUtil;
import com.fsd.core.service.BaseService;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.F_formfield;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface F_formfieldService extends BaseService<F_formfield, String>{
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	void save(F_formfield obj, A_Employee employee) throws Exception;
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	F_formfield getObjectById(ParametersUtil parameters) throws Exception;
	/**
	 * 创建表单对应的数据库表，并且保存表单字段
	 * @param form
	 * @param list
	 * @return
	 * @throws Exception
	 */
	void saveFormField(F_form form, List<Map<String, Object>> list) throws Exception;
}
