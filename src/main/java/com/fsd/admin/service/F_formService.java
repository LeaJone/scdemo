package com.fsd.admin.service;

import com.fsd.core.util.ParametersUtil;
import com.fsd.core.service.BaseService;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.F_form;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

public interface F_formService extends BaseService<F_form, String>{
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	void save(F_form obj, A_Employee employee) throws Exception;
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	F_form getObjectById(ParametersUtil parameters) throws Exception;
	/**
	 * 修改状态
	 * @param parameters
	 * @param employee
	 * @throws Exception
	 */
	void updateEnabledObject(ParametersUtil parameters, A_Employee employee) throws Exception;
	/**
	 * 保存表单设计
	 * @param parameters
	 * @param employee
	 * @throws Exception
	 */
	void saveFormDesign(ParametersUtil parameters, A_Employee employee) throws Exception;
	/**
	 * 保存表单数据
	 * @param parameters
	 * @throws Exception
	 */
	void saveForm(ParametersUtil parameters) throws Exception;
	/**
	 * 加载表单数据
	 * @param parameters
	 * @throws Exception
	 */
	Map<String, Object> getObjectForm(ParametersUtil parameters) throws Exception;
	/**
	 * 获取党员发起人信息
	 * @param proc_inst_id
	 * @return
	 * @throws Exception
	 */
	String getInitUser(String proc_inst_id) throws Exception;
	/**
	 * Word表单导出
	 */
	Map<String, String> exportForm(ParametersUtil parameters, String rootPath, String httpRootPath) throws Exception;
}
