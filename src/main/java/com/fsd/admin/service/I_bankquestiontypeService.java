package com.fsd.admin.service;

import java.util.List;
import java.util.Map;

import com.fsd.core.util.ParametersUtil;
import com.fsd.core.bean.CheckTree;
import com.fsd.core.bean.Tree;
import com.fsd.core.service.BaseService;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.I_bankinfo;
import com.fsd.admin.model.I_bankquestiontype;

public interface I_bankquestiontypeService extends BaseService<I_bankquestiontype, String>{
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
	
	/**
	 * 异步加载对象树
	 * @return
	 * @throws Exception
	 */
	public List<Tree> getAsyncObjectTree(String fid, A_Employee employee) throws Exception;
	
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public void save(I_bankquestiontype obj, A_Employee employee) throws Exception;
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public I_bankquestiontype getObjectById(ParametersUtil parameters) throws Exception;
	
	/**
	 * 顺序排序
	 * @param parameters
	 */
	public void updateSortObject(ParametersUtil parameters) throws Exception;
	
	/**
	 * 获取权限树
	 * @param user
	 * @return
	 */
	public List<CheckTree> getAllQxObjectCheck(ParametersUtil parameters, A_Employee employee) throws Exception;

	/**
	 * 异步加载对象树
	 * @return
	 * @throws Exception
	 */
	public List<Tree> getAsyncObjectTreeByPopdom(String fid, A_Employee employee, Map<String, List<String>> popdomMap) throws Exception;
}
