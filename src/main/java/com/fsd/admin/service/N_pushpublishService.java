package com.fsd.admin.service;

import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.N_pushpublish;
import com.fsd.core.service.BaseService;

public interface N_pushpublishService extends BaseService<N_pushpublish, String>{

	/**
	 * 保存对象
	 * @param obj
	 */
	void saveObject(N_pushpublish obj, A_Employee employee) throws Exception;
}
