package com.fsd.admin.service;

import com.fsd.core.util.ParametersUtil;
import com.fsd.core.service.BaseService;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.J_posts;

public interface J_postsService extends BaseService<J_posts, String>{
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	J_posts getObjectById(ParametersUtil parameters) throws Exception;
	/**
	 * 置顶对象
	 * @param parameters
	 */
	void updateFirstlyObject(ParametersUtil parameters) throws Exception;
	/**
	 * 修改帖子
	 * @param obj
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	void save(J_posts obj, A_Employee employee) throws Exception;
}
