package com.fsd.admin.service;

import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_email;
import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;

/**
 * 站内信 - service
 * @author Administrator
 *
 */
public interface Z_emailService extends BaseService<Z_email, String> {
	/**
	 * 加载已删除邮件分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getRecycleEmailPageList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 加载星标邮件分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getStarEmailPageList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 加载草稿箱邮件分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getDraftEmailPageList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 加载已发送邮件分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getSendEmailPageList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 加载收件箱邮件分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getInboxEmailPageList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 恢复邮件
	 * @param parameters
	 */
	void updateObjectDeleted(ParametersUtil parameters) throws Exception;
	/**
	 * 彻底删除邮件
	 * @param parameters
	 */
	void delRealyObject(ParametersUtil parameters) throws Exception;
	/**
	 * 邮件标为已读、未读
	 * @param parameters
	 */
	void updateEnabledObject(ParametersUtil parameters, A_Employee employee) throws Exception;
	/**
	 * 查看邮件详情
	 * @param id
	 * @return
	 */
	Z_email getEmail(String id);
	/**
	 * 删除邮件
	 * @param parameters
	 */
	void deletedEmail(ParametersUtil parameters) throws Exception;
	/**
	 * 取消邮件星标
	 * @param parameters
	 */
	void updatenostarEmail(ParametersUtil parameters) throws Exception;
	/**
	 * 星标邮件
	 * @param parameters
	 */
	void updatestarEmail(ParametersUtil parameters) throws Exception;
	/**
	 * 发送邮件
	 * @param obj
	 */
	void saveEmail(Z_email obj, A_Employee employee) throws Exception;
	/**
	 * 保存邮件
	 * @param obj
	 */
	void saveEmailFirst(Z_email obj, A_Employee employee) throws Exception;
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getEmailById(ParametersUtil parameters) throws Exception;
	/**
	 * 邮件标为已读
	 * @param parameters
	 */
	void updateEmailToRead(ParametersUtil parameters) throws Exception;
	/**
	 * 统计用户未读邮件数量
	 * @param id
	 * @return
	 * @throws Exception
	 */
    int getCountById(String id) throws Exception;
	/**
	 * 发送系统邮件
	 * @param title
	 * @param content
	 * @param employee
	 * @throws Exception
	 */
	void saveMessage(String title, String content, A_Employee employee) throws Exception;
}
