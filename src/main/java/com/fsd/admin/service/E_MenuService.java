package com.fsd.admin.service;

import java.util.List;
import com.fsd.admin.entity.A_LoginInfo;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.E_Menu;
import com.fsd.core.bean.Tree;
import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;

public interface E_MenuService extends BaseService<E_Menu, String> {

	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
	
	/**
	 * 异步加载对象树
	 * @return
	 * @throws Exception
	 */
	List<Tree> getAsyncObjectTree(String fid, A_Employee employee) throws Exception;
	
	/**
	 * 一次性加载部门树，根据权限加载
	 * @return
	 * @throws Exception
	 */
	List<Tree> getAllObjectTree(ParametersUtil param, A_LoginInfo login) throws Exception;
	
	/**
	 * 保存对象（添加、修改）
	 * @param obj
	 */
	void saveObject(E_Menu obj, A_Employee employee) throws Exception;
	
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	E_Menu getObjectById(ParametersUtil parameters) throws Exception;
	
	/**
	 * 删除对象
	 * @param parameters
	 */
	void delObject(ParametersUtil parameters) throws Exception;
	
	/**
	 * 顺序排序
	 * @param parameters
	 */
	void updateSortObject(ParametersUtil parameters) throws Exception;
	
	/**
	 * 获取微信平台菜单
	 * @param param
	 * @return
	 */
	List<Tree> getWeChatMenu(ParametersUtil param) throws Exception;
	
	/**
	 * 同步微信平台菜单
	 * @param param
	 * @return
	 */
	void synchroUserMenu(ParametersUtil param) throws Exception;
}
