package com.fsd.admin.service;

import java.util.List;
import com.fsd.admin.entity.A_LoginInfo;
import com.fsd.admin.model.A_Employee;
import com.fsd.core.service.BaseService;
import com.fsd.admin.model.Sys_SystemDictionary;
import com.fsd.core.util.ParametersUtil;

public interface Sys_SystemDictionaryService extends BaseService<Sys_SystemDictionary, String>{

	/**
	 * 根据字典类型Key加载字典列表
	 * @param key
	 * @return
	 * @throws Exception
	 */
	List<Sys_SystemDictionary> getObjectListByKey(String key) throws Exception;
	/**
	 * 加载字典类型分页列表
	 * @param param
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getDictionaryTypePageList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 加载字典分页列表
	 * @param param
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getDictionaryPageList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 编辑字典类型
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	void saveDictionaryType(Sys_SystemDictionary obj, A_Employee employee) throws Exception;
	/**
	 * 编辑字典
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	void saveDictionary(Sys_SystemDictionary obj, A_Employee employee) throws Exception;
	/**
	 * 删除字典类型
	 * @param parameters
	 * @throws Exception
	 */
	void delDictionaryType(ParametersUtil parameters, A_Employee employee) throws Exception;
	/**
	 * 删除字典
	 * @param parameters
	 * @throws Exception
	 */
	void delDictionary(ParametersUtil parameters, A_Employee employee) throws Exception;
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	Sys_SystemDictionary getObjectById(ParametersUtil parameters) throws Exception;
}
