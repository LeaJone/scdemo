package com.fsd.admin.service;

import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;

import javax.servlet.http.HttpServletRequest;

import com.fsd.admin.model.A_Employee;

public interface H_MemberService extends BaseService<A_Employee, String>{

	//============================  注册管理  ============================
	/**
	 * 加载分页数据，查询所属注册会员
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageListByRegister(ParametersUtil param, A_Employee employee) throws Exception;
	

	//============================  会员管理  ============================
	/**
	 * 加载分页数据，查询删除会员
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageListRecycle(ParametersUtil param, A_Employee employee) throws Exception;
	
	/**
	 * 加载分页数据，查询会员
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageListByAll(ParametersUtil param, A_Employee employee) throws Exception;
	
	/**
	 * 加载分页数据，查询所属管理会员
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageListByCompany(ParametersUtil param, A_Employee employee) throws Exception;

	/**
	 * 保存对象（添加、修改）
	 * @param branch
	 */
	public void saveObject(A_Employee employee , A_Employee a_employee) throws Exception;
	
	/**
	 * 保存自行修改
	 * @param branch
	 */
	public void saveObjectSelf(A_Employee employee , A_Employee a_employee) throws Exception;
	
	/**
	 * 充值推送消息测试
	 * @param param
	 * @throws Exception
	 */
	public void sendMessageRecharge(ParametersUtil param) throws Exception;
	/**
	 * 消费推送消息测试
	 * @param param
	 * @throws Exception
	 */
	public void sendMessagePay(ParametersUtil param) throws Exception;
	/**
	 * 预约推送消息测试
	 * @param param
	 * @throws Exception
	 */
	public void sendMessageReserve(ParametersUtil param) throws Exception;
}
