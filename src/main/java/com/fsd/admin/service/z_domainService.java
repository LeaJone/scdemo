package com.fsd.admin.service;

import com.fsd.admin.entity.A_LoginInfo;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.z_domain;
import com.fsd.core.bean.CheckTree;
import com.fsd.core.bean.Tree;
import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;

import java.util.List;
import java.util.Map;

public interface z_domainService extends BaseService<z_domain, String>{
	/**
	 * 获取权限树
	 * @return
	 */
	List<CheckTree> getQxObjectCheck(ParametersUtil parameters, A_Employee employee, Map<String, List<String>> popdomMap) throws Exception;
	/**
	 * 获取权限树
	 * @return
	 */
	List<CheckTree> getAllQxObjectCheck(ParametersUtil parameters, A_Employee employee) throws Exception;

	/**
	 * 获取栏目结构
	 * @param id
	 * @return
	 */
	z_domain getObjectStructByID(String id, String companyid) throws Exception;
	/**
	 * 获取栏目子集
	 * @param fid
	 * @param companyid
	 * @return
	 * @throws Exception
	 */
	List<z_domain> queryObjectListById(String fid, String companyid) throws Exception;
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 异步加载对象树
	 * @return
	 * @throws Exception
	 */
	List<Tree> getAsyncObjectTree(String fid, boolean isQuery, A_Employee employee) throws Exception;
	/**
	 * 异步加载对象树
	 * @return
	 * @throws Exception
	 */
	List<Tree> getAsyncObjectTreeByPopdom(String fid, boolean isQuery, A_Employee employee, Map<String, List<String>> popdomMap) throws Exception;
	/**
	 * 一次性加载部门树，根据权限加载
	 * @return
	 * @throws Exception
	 */
	List<Tree> getAllObjectTreeByPopdom(ParametersUtil param, boolean isQuery, A_LoginInfo login) throws Exception;
	/**
	 * 加载所有学科代码
	 * @return
	 * @throws Exception
	 */
	List<z_domain> getListByParentid() throws Exception;
	/**
	 * 查询窗口表格显示，根据权限加载
	 * @return
	 * @throws Exception
	 */
	List<z_domain> getAllObjectListByPopdom(ParametersUtil param, boolean isQuery, A_LoginInfo login) throws Exception;
	/**
	 * 保存对象（添加、修改）
	 * @param obj
	 */
	void saveObject(z_domain obj, A_Employee employee) throws Exception;
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	z_domain getObjectById(ParametersUtil parameters) throws Exception;
	/**
	 * 删除对象
	 * @param parameters
	 */
	void delObject(ParametersUtil parameters) throws Exception;
	
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	List<Object> getObjectStat(ParametersUtil param, A_Employee employee) throws Exception;

	/**
	 * 顺序排序
	 * @param parameters
	 */
	void updateSortObject(ParametersUtil parameters) throws Exception;
	/**
	 * 修改状态
	 * @param parameters
	 */
	void updateEnabledObject(ParametersUtil parameters, A_Employee employee) throws Exception;
	/**
	 * 公开栏目
	 * @param parameters
	 */
	void updateGklmObject(ParametersUtil parameters, A_Employee employee) throws Exception;
	/**
	 * 栏目文章关联查询
	 * @param parameters
	 */
	ParametersUtil getObjectRelateArticleNum(ParametersUtil parameters, A_Employee employee) throws Exception;
	/**
	 * 栏目文章及关联文章迁移
	 * @param parameters
	 */
	void updateObjectMoveRelateArticle(ParametersUtil parameters, A_Employee employee) throws Exception;
	/**
	 * Excel导出
	 */
	Map<String, String> download(ParametersUtil parameters, A_Employee employee, String rootPath, String httpRootPath) throws Exception;
}
