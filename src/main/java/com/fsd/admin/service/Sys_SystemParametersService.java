package com.fsd.admin.service;

import java.util.List;
import java.util.Map;

import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Sys_SystemParameters;

public interface Sys_SystemParametersService extends BaseService<Sys_SystemParameters, String>{

	/**
	 * 刷新缓存信息
	 * @param companyid
	 */
	public void refreshEhcache(String companyid);
	/**
	 * 获取系统参数信息，为Core工程提供相关功能模块的加载使用
	 */
	public Map<String, String> getParameterMapByCore(String companyid) throws Exception;
	
	/**
	 * 获取系统参数信息
	 */
	public String getParameterValueByCode(String code, String companyid) throws Exception;
	/**
	 * 获取系统参数信息列表
	 */
	public List<Sys_SystemParameters> getParameterObjectListByCode(String code) throws Exception;
	/**
	 * 获取系统参数信息
	 */
	public String getParameterValueByCodeSql(String code, String companyid) throws Exception;
	public String getParameterValueByCodeSql(String code);
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 加载所有数据
	 * @return
	 * @throws Exception
	 */
	public List<Sys_SystemParameters> getObjectList(A_Employee employee) throws Exception;
	/**
	 * 保存角色
	 * @param popedomgroup
	 * @throws Exception
	 */
	public void saveObject(Sys_SystemParameters object, A_Employee employee) throws Exception;
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public Sys_SystemParameters getObjectById(ParametersUtil parameters)  throws Exception;
	/**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters) throws Exception;
	/**
	 * 修改参数值
	 * @param code
	 * @param value
	 * @param companyid
	 * @throws Exception
	 */
	public void updateObjectByValue(String code, String value, String companyid) throws Exception;
	public void updateObjectByValue(String code, String value) throws Exception;
	/**
	 * Excel导出
	 * @param employee
	 * @param rootPath
	 * @param httpRootPath
	 * @return
	 * @throws Exception
	 */
	public Map<String, String> download(A_Employee employee, String rootPath, String httpRootPath) throws Exception;
}
