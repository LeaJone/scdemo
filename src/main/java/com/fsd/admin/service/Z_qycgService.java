package com.fsd.admin.service;

import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_qycg;
import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;

public interface Z_qycgService extends BaseService<Z_qycg, String>{
	/**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	void save(Z_qycg obj, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	Z_qycg getObjectById(ParametersUtil parameters) throws Exception;
}
