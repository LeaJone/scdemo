package com.fsd.admin.service;

import java.util.List;

import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Sys_PopedomGroup;

public interface Sys_PopedomGroupService extends BaseService<Sys_PopedomGroup, String>{
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 加载所有数据
	 * @return
	 * @throws Exception
	 */
	List<Sys_PopedomGroup> getObjectList(A_Employee employee) throws Exception;
	/**
	 * 加载所有数据
	 * @return
	 * @throws Exception
	 */
	 List<Sys_PopedomGroup> getObjectListEmployee(A_Employee employee) throws Exception;
	/**
	 * 保存角色
	 * @param popedomgroup
	 * @throws Exception
	 */
	 void saveObject(Sys_PopedomGroup popedomgroup, A_Employee employee) throws Exception;
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	Sys_PopedomGroup getObjectById(ParametersUtil parameters)  throws Exception;
	/**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	void delObject(ParametersUtil parameters) throws Exception;
	/**
	 * 根据Code获得对象
	 * @param code
	 * @return
	 * @throws Exception
	 */
	 Sys_PopedomGroup getObjectByCode(String code) throws Exception;
}
