package com.fsd.admin.service;

import com.fsd.core.service.BaseService;

import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.H_qualified;

public interface H_qualifiedService extends BaseService<H_qualified, String>{
	
	/**
	 * 保存对象添加
	 * @param branch
	 */
	public void saveObject(String employeeid, A_Employee employee) throws Exception;
	/**
	 * 保存对象添加
	 * @param branch
	 */
	public void saveObject(A_Employee emplObject, A_Employee employee) throws Exception;
}
