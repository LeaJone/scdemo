package com.fsd.admin.service;

import java.util.List;

import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.B_download;

public interface B_downloadService extends BaseService<B_download, String>{
	/**
	 * 保存对象（添加、修改）
	 * @param obj
	 * @param a_employee
	 * @param a_company
	 * @throws Exception
	 */
	public void saveObject(B_download obj, A_Employee employee) throws Exception;
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public B_download getObjectByArticleId(ParametersUtil parameters) throws Exception;
	/**
	 * 删除对象
	 * @param parameters
	 */
	public void delObject(ParametersUtil parameters) throws Exception;
	/**
	 * 彻底对象
	 * @param parameters
	 */
	public void delRealyObjectByArticleId(ParametersUtil parameters) throws Exception;
	
	

	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public B_download getObjectById(ParametersUtil parameters)  throws Exception;
	/**
	 * 修改对象状态
	 * @param parameters
	 */
	public void updateEnabledObject(ParametersUtil parameters) throws Exception; 
}
