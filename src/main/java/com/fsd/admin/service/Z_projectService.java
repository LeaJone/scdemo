package com.fsd.admin.service;

import com.fsd.core.util.ParametersUtil;
import com.fsd.core.service.BaseService;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_project;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

public interface Z_projectService extends BaseService<Z_project, String>{
    /**
	 * 加载申报分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getObjectApplyPageList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 加载中期检查分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getObjectMiddlePageList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 加载已立项的分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getObjectPageListProspectus(ParametersUtil param, A_Employee employee) throws Exception;
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	Z_project save(String rootpath, ParametersUtil param, Z_project obj, A_Employee employee) throws Exception;
	/**
	 * 保存项目基本信息
	 * @param param
	 * @param obj
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	Z_project saveInfo(ParametersUtil param, Z_project obj, A_Employee employee) throws Exception;
	/**
	 * 保存项目负责人信息
	 * @param param
	 * @param obj
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	Z_project saveLeader(ParametersUtil param, Z_project obj, A_Employee employee) throws Exception;
	/**
	 * 保存第一指导老师信息
	 * @param param
	 * @param obj
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	Z_project saveTeacher(ParametersUtil param, Z_project obj, A_Employee employee) throws Exception;
	/**
	 * 保存第二指导老师信息
	 * @param param
	 * @param obj
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	Z_project saveTeacher2(ParametersUtil param, Z_project obj, A_Employee employee) throws Exception;
	/**
	 * 保存校外第一指导老师信息
	 * @param param
	 * @param obj
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	Z_project saveOutTeacher(ParametersUtil param, Z_project obj, A_Employee employee) throws Exception;
	/**
	 * 保存校外第二指导老师信息
	 * @param param
	 * @param obj
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	Z_project saveOutTeacher2(ParametersUtil param, Z_project obj, A_Employee employee) throws Exception;
	/**
	 * 保存项目简介
	 * @param param
	 * @param obj
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	Z_project saveAbstract(ParametersUtil param, Z_project obj, A_Employee employee) throws Exception;
	/**
	 * 保存申请理由
	 * @param param
	 * @param obj
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	Z_project saveGist(ParametersUtil param, Z_project obj, A_Employee employee) throws Exception;
	/**
	 * 保存项目方案
	 * @param param
	 * @param obj
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	Z_project saveScheme(ParametersUtil param, Z_project obj, A_Employee employee) throws Exception;
	/**
	 * 保存特色与创新点
	 * @param param
	 * @param obj
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	Z_project saveFeature(ParametersUtil param, Z_project obj, A_Employee employee) throws Exception;
	/**
	 * 保存项目进度安排
	 * @param param
	 * @param obj
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	Z_project saveSchedule(ParametersUtil param, Z_project obj, A_Employee employee) throws Exception;
	/**
	 * 提交项目
	 * @param param
	 * @throws Exception
	 */
	void saveProjectSubmit(String rootpath, ParametersUtil param, A_Employee employee) throws Exception;
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	Z_project getObjectById(ParametersUtil parameters) throws Exception;
	/**
	 * 更新项目状态
	 * @param projectId
	 * @param status
	 * @throws Exception
	 */
	void updateProjectStatus(String projectId, String status) throws Exception;
	/**
	 * 项目立项
	 * @param ids
	 * @param typeid
	 * @param money
	 * @throws Exception
	 */
	void updateProjectApproval(String ids, String typeid, String money, String f_schooltypeid, String f_schooltypename) throws Exception;
	/**
	 * 项目中期检查
	 * @param ids
	 * @param typeid
	 * @throws Exception
	 */
	void updateProjectMiddle(String ids, String typeid, String typename) throws Exception;

	/**
	 * 项目结题结果
	 * @param ids
	 * @param ddmbid
	 * @param ddmbname
	 * @param cjpdid
	 * @param cjpdname
	 * @throws Exception
	 */
	void updateProjectEnding(String ids, String ddmbid, String ddmbname, String cjpdid, String cjpdname) throws Exception;
	/**
	 * 根据分组ID加载项目列表
	 * @param groupid
	 * @return
	 * @throws Exception
	 */
	List<Z_project> getListByGroupId(String groupid) throws Exception;
	/**
	 * 更新路演分数
	 * @param projectid
	 * @param scroe
	 * @throws Exception
	 */
	void updateLuyanScore(String projectid, String scroe) throws Exception;
	/**
	 * 导入路演分数
	 * @param rootPath
	 * @param f_url
	 * @throws Exception
	 */
	void updateImportLuyanScore(String rootPath, String f_url) throws Exception;
	/**
	 * 加载报表列表
	 * @param start
	 * @param end
	 * @return
	 * @throws Exception
	 */
	List<Z_project> getReportList(String start, String end) throws Exception;
	/**
	 * 加载报表列表
	 * @param start
	 * @param end
	 * @return
	 * @throws Exception
	 */
	List<Z_project> getReportList1(String start, String end) throws Exception;
	/**
	 * 根据项目ID加载项目当前审核节点信息
	 * @param projctId
	 * @return
	 * @throws Exception
	 */
	String getProjectAuditStep(String projctId) throws Exception;
	/**
	 * 根据用户信息获得项目数量
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	int getProjectCountByUser(A_Employee employee) throws Exception;
	/**
	 * 根据项目编码加载项目信息
	 * @param code
	 * @return
	 * @throws Exception
	 */
	Z_project getObjectByCode(String code) throws Exception;
	/**
	 * 根据团队ID加载项目列表
	 * @param teamId
	 * @return
	 * @throws Exception
	 */
	List<Z_project> getObjectListByTeamId(String teamId) throws Exception;
}
