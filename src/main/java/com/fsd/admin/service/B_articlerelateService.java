package com.fsd.admin.service;

import java.util.List;

import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.B_article;
import com.fsd.admin.model.B_articlerelate;

public interface B_articlerelateService extends BaseService<B_articlerelate, String>{
	/**
	 * 加载所需ID数据
	 * @return
	 * @throws Exception
	 */
	public List<String> getObjectIDList(String articleID, String joinType) throws Exception;
	/**
	 * 加载所需数据
	 * @return
	 * @throws Exception
	 */
	public List<B_articlerelate> getObjectList(String articleID, String joinType) throws Exception;
	/**
	 * 保存文章关联
	 */
	public void saveArticleRelate(String articleID, String joinID, String joinType) throws Exception;
	
	/**
	 * 删除对象
	 */
	public void delObject(String articleID, String joinType) throws Exception;
}
