package com.fsd.admin.service;

import com.fsd.core.util.ParametersUtil;
import com.fsd.core.service.BaseService;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.F_middlechecknext;

import java.util.List;

public interface F_middlechecknextService extends BaseService<F_middlechecknext, String>{
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	void save(F_middlechecknext obj, A_Employee employee) throws Exception;
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	F_middlechecknext getObjectById(ParametersUtil parameters) throws Exception;
	/**
	 * 根据项目ID加载中期检查下一阶段计划列表
	 * @param projectid
	 * @return
	 * @throws Exception
	 */
	List<F_middlechecknext> getListByProjectId(String projectid) throws Exception;
}
