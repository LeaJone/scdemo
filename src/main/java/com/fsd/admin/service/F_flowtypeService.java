package com.fsd.admin.service;

import java.util.List;

import com.fsd.core.util.ParametersUtil;
import com.fsd.core.bean.Tree;
import com.fsd.core.service.BaseService;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.F_flowtype;

public interface F_flowtypeService extends BaseService<F_flowtype, String>{
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 加载树数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<Tree> getObjectTreeList(String fid, A_Employee employee) throws Exception;
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public void save(F_flowtype obj, A_Employee employee) throws Exception;
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public F_flowtype getObjectById(ParametersUtil parameters) throws Exception;
}
