package com.fsd.admin.service;

import java.util.List;

import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Company;
import com.fsd.admin.model.A_Employee;

public interface A_CompanyService extends BaseService<A_Company, String>{

	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public A_Company getObjectById(ParametersUtil parameters) throws Exception;

	/**
	 * 根据ID加载对象
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public A_Company getObjectById(Object id) throws Exception;
	
	/**
	 * 站群专用跳转站点查询方法
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<A_Company> getObjectListBySiteChange(ParametersUtil param) throws Exception;
	
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param) throws Exception;

	/**
	 * 保存对象
	 * @param obj
	 * @param employee
	 * @param rootPath
	 * @throws Exception
	 */
	public void saveObject(A_Company obj, A_Employee employee, String rootPath) throws Exception;
	
	/**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters) throws Exception;
	
	/**
	 * 修改状态
	 * @param parameters
	 */
	public void updateEnabledObject(ParametersUtil parameters) throws Exception;
}
