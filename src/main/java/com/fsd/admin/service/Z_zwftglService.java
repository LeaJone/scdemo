package com.fsd.admin.service;

import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.B_article;
import com.fsd.admin.model.Z_articlefz;
import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;

public interface Z_zwftglService extends BaseService<Z_articlefz, String> {
	
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param) throws Exception;
	
	/**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public ParametersUtil save(Z_articlefz obj, A_Employee employee, B_article article, ParametersUtil parameters) throws Exception;

	/**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;

	/**
	 * 修改状态
	 * @param parameters
	 */
	public void updateEnabledObject(ParametersUtil parameters) throws Exception;
	
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectById(ParametersUtil parameters) throws Exception;
	
	/**
	 * 根据文章ID加载对象
	 * @param articleId
	 * @return
	 * @throws Exception
	 */
	public Z_articlefz getObjectByArticleId(String articleId) throws Exception;
}
