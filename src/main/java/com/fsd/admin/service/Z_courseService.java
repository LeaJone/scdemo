package com.fsd.admin.service;

import com.fsd.core.util.ParametersUtil;
import com.fsd.core.service.BaseService;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_course;

import java.util.List;

public interface Z_courseService extends BaseService<Z_course, String>{
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	void save(String rootPath,Z_course obj, A_Employee employee) throws Exception;
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	Z_course getObjectById(ParametersUtil parameters) throws Exception;
	/**
	 * 根据类型加载课程列表
	 * @param type
	 * @return
	 * @throws Exception
	 */
	List<Z_course> getListByType(String type) throws Exception;
	/**
	 * 根据课程平台加载列表
	 * @param platform
	 * @return
	 * @throws Exception
	 */
	List<Z_course> getListByPlatform(String platform) throws Exception;
	
	/**
	 *  加载个人分页数据
	 * @param parameters
	 * @param loginUser
	 * @return
	 */
	Object getObjectMyPageList(ParametersUtil parameters, A_Employee loginUser);
	/**
	 * 修改启用状态
	 * @param parameters
	 * @param loginUser
	 */
	void updateEnabledObject(ParametersUtil parameters, A_Employee loginUser);
	/**
	 * 修改审核状态
	 * @param parameters
	 * @param employee
	 */
	void updateCheckedObject(ParametersUtil parameters, A_Employee employee);
	/**
	 * 添加原因
	 * @param obj
	 * @param loginUser
	 */
	void saveMessage(Z_course obj, A_Employee loginUser);
	/**
	 * 导入课程
	 * @param rootPath
	 * @param f_url
	 * @throws Exception
	 */
	void updateImportCourse(String rootPath, String f_url) throws Exception;
}
