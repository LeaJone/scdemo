package com.fsd.admin.service;

import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.entity.A_LoginInfo;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.B_leaveword;

public interface B_leavewordService extends BaseService<B_leaveword, String>, F_flowWorkService{
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 加载回收站分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageListRecycle(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 加载分页数据，加载所属回复人数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageListByEmployee(ParametersUtil param, A_LoginInfo loginInfo) throws Exception;
	/**
	 * 保存对象
	 * @param popedomgroup
	 * @throws Exception
	 */
	public void saveObject(B_leaveword object, A_LoginInfo loginInfo) throws Exception;
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public B_leaveword getObjectById(ParametersUtil parameters)  throws Exception;
	/**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
	/**
	 * 审核对象
	 * @param parameters
	 * @throws Exception
	 */
	public void auditObject(ParametersUtil parameters, A_Employee employee) throws Exception;
	/**
	 * 选登对象
	 * @param parameters
	 * @throws Exception
	 */
	public void chooseObject(ParametersUtil parameters, A_Employee employee) throws Exception;
	/**
	 * 恢复对象
	 * @param parameters 
	 * @throws Exception
	 */
	public void recycleObject(ParametersUtil parameters, A_Employee employee) throws Exception;
	
	/**
	 * 处理未进入流程的信箱数据
	 * @param parameters 
	 * @throws Exception
	 */
	public void saveDealtObject(ParametersUtil parameters, A_Employee employee) throws Exception;
	/**
	 * 变更回复人
	 * @param parameters
	 * @throws Exception
	 */
	public void hfrbgObject(ParametersUtil parameters, A_Employee employee) throws Exception;
	
	/**
	 * 敏感标志
	 * @param parameters 
	 * @throws Exception
	 */
	public void flageObject(ParametersUtil parameters, A_Employee employee) throws Exception;
}
