package com.fsd.admin.service;

import com.fsd.core.util.ParametersUtil;
import com.fsd.core.service.BaseService;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_eduteacher;

import java.util.List;

public interface Z_eduteacherService extends BaseService<Z_eduteacher, String>{
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	void save(String rootpath,Z_eduteacher obj, A_Employee employee) throws Exception;
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	Z_eduteacher getObjectById(ParametersUtil parameters) throws Exception;
	/**
	 * 上传申请书
	 * @param obj
	 * @param loginUser
	 */
	void uploadFile(Z_eduteacher obj, A_Employee loginUser);
	/**
	 * 查询所有记录
	 * @return
	 */
	List<Z_eduteacher> selectAll(String start, String end);
}
