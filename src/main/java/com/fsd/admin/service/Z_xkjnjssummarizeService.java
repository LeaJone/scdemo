package com.fsd.admin.service;

import com.fsd.core.util.ParametersUtil;
import com.fsd.core.service.BaseService;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_xkjnjssummarize;

public interface Z_xkjnjssummarizeService extends BaseService<Z_xkjnjssummarize, String>{
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 保存学科技能竞赛总结报告基本信息
	 * @param param
	 * @param obj
	 * @param employee
	 * @return
	 * @throws Exception
	 */
	Z_xkjnjssummarize saveInfo(ParametersUtil param, Z_xkjnjssummarize obj, A_Employee employee) throws Exception;
	/**
	 * 保存项目背景信息
	 * @param param
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	Z_xkjnjssummarize saveBackground(ParametersUtil param, Z_xkjnjssummarize obj) throws Exception;
	/**
	 * 保存项目实施过程信息
	 * @param param
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	Z_xkjnjssummarize saveImplementcourse(ParametersUtil param, Z_xkjnjssummarize obj) throws Exception;
	/**
	 * 保存项目竞赛过程信息
	 * @param param
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	Z_xkjnjssummarize saveMatchcourse(ParametersUtil param, Z_xkjnjssummarize obj) throws Exception;
	/**
	 * 保存项目竞赛成果信息
	 * @param param
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	Z_xkjnjssummarize saveAchievement(ParametersUtil param, Z_xkjnjssummarize obj) throws Exception;
	/**
	 * 保存项目不足与改进信息
	 * @param param
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	Z_xkjnjssummarize saveSummarize(ParametersUtil param, Z_xkjnjssummarize obj) throws Exception;
	/**
	 * 保存项目中遇到的问题信息
	 * @param param
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	Z_xkjnjssummarize saveQuestion(ParametersUtil param, Z_xkjnjssummarize obj) throws Exception;
	/**
	 * 保存项目创新信息
	 * @param param
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	Z_xkjnjssummarize saveInnovate(ParametersUtil param, Z_xkjnjssummarize obj) throws Exception;
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	void save(Z_xkjnjssummarize obj, A_Employee employee) throws Exception;
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	Z_xkjnjssummarize getObjectById(ParametersUtil parameters) throws Exception;
	/**
	 * 根据FID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	Z_xkjnjssummarize getObjectByFid(ParametersUtil parameters) throws Exception;
}
