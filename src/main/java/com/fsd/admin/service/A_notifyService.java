package com.fsd.admin.service;

import java.util.List;

import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.A_notify;

public interface A_notifyService extends BaseService<A_notify, String>{
	/**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
	/**
	 * 加载所有数据
	 * @return
	 * @throws Exception
	 */
	public List<A_notify> getObjectListQY(A_Employee employee) throws Exception;
	/**
	 * 保存角色
	 * @param popedomgroup
	 * @throws Exception
	 */
	public void saveObject(A_notify obj, A_Employee employee) throws Exception;
	/**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public A_notify getObjectById(ParametersUtil parameters)  throws Exception;
	/**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters) throws Exception;

	/**
	 * 修改对象置顶状态
	 * @param parameters
	 */
	public void updateFirstlyObject(ParametersUtil parameters) throws Exception;  

	/**
	 * 修改对象状态
	 * @param parameters
	 */
	public void updateEnabledObject(ParametersUtil parameters) throws Exception;  
  	    
}
