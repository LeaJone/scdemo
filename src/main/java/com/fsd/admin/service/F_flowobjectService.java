package com.fsd.admin.service;

import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;
import com.fsd.admin.entity.A_LoginInfo;
import com.fsd.admin.model.F_flowobject;

public interface F_flowobjectService extends BaseService<F_flowobject, String>{
	
	/**
	 * 加载分页数据， 申请所有数据
	 * @param param
	 * @param loginInfo
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageListByManage(ParametersUtil param, A_LoginInfo loginInfo) throws Exception;
	
	/**
	 * 加载分页数据，提取审核数据
	 * @param param
	 * @param loginInfo
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageListByExtract(ParametersUtil param, A_LoginInfo loginInfo) throws Exception;
	
	/**
	 * 加载分页数据，释放审核数据
	 * @param param
	 * @param loginInfo
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageListByRelease(ParametersUtil param, A_LoginInfo loginInfo) throws Exception;
	
	/**
	 * 加载分页数据，审核中数据
	 * @param param
	 * @param loginInfo
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageListByPending(ParametersUtil param, A_LoginInfo loginInfo) throws Exception;

	/**
	 * 加载分页数据，可撤回数据
	 * @param param
	 * @param loginInfo
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageListByRecall(ParametersUtil param, A_LoginInfo loginInfo) throws Exception;
	
	/**
	 * 加载分页数据， 处理过数据
	 * @param param
	 * @param loginInfo
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageListByPassed(ParametersUtil param, A_LoginInfo loginInfo) throws Exception;
	
	/**
	 * 加载分页数据， 本人提交申请数据
	 * @param param
	 * @param loginInfo
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageListBySelf(ParametersUtil param, A_LoginInfo loginInfo) throws Exception;
	
	/**
	 * 根据数据对象ID加载对象
	 * @param objid
	 * @return
	 * @throws Exception
	 */
	public F_flowobject getObjectByDataObjId(String objid)  throws Exception;
	
	/**
	 * 获取数据对象，根据审批记录ID
	 * @param parameters
	 * @param loginInfo
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getDataObjectByRecordID(ParametersUtil parameters, A_LoginInfo loginInfo) throws Exception;
}
