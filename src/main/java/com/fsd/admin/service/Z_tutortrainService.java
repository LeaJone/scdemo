package com.fsd.admin.service;

import com.fsd.core.util.ParametersUtil;
import com.fsd.core.service.BaseService;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_tutortrain;

import java.util.List;

public interface Z_tutortrainService extends BaseService<Z_tutortrain, String>{
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	void save(Z_tutortrain obj, A_Employee employee) throws Exception;
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	Z_tutortrain getObjectById(ParametersUtil parameters) throws Exception;
	/**
	 * 根据导师ID加载培训信息
	 * @param tutorid
	 * @return
	 * @throws Exception
	 */
	List<Z_tutortrain> getListByTutorid(String tutorid) throws Exception;
}
