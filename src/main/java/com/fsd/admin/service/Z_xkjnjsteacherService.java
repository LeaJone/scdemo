package com.fsd.admin.service;

import com.fsd.core.util.ParametersUtil;
import com.fsd.core.service.BaseService;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_xkjnjsteacher;

import java.util.List;

public interface Z_xkjnjsteacherService extends BaseService<Z_xkjnjsteacher, String>{
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	void save(Z_xkjnjsteacher obj, A_Employee employee) throws Exception;
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	Z_xkjnjsteacher getObjectById(ParametersUtil parameters) throws Exception;
	/**
	 * 根据父级ID加载列表
	 * @param fid
	 * @return
	 * @throws Exception
	 */
	List<Z_xkjnjsteacher> getListByFid(String fid) throws Exception;
}
