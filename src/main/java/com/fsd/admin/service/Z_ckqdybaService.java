package com.fsd.admin.service;

import com.fsd.core.util.ParametersUtil;
import com.fsd.core.service.BaseService;
import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_ckqdyba;

public interface Z_ckqdybaService extends BaseService<Z_ckqdyba, String>{
    /**
	 * 加载分页数据
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ParametersUtil getObjectPageList(ParametersUtil param, A_Employee employee) throws Exception;
    /**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	public void save(Z_ckqdyba obj, A_Employee employee) throws Exception;
    /**
	 * 删除对象
	 * @param parameters
	 * @throws Exception
	 */
	public void delObject(ParametersUtil parameters, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public Z_ckqdyba getObjectById(ParametersUtil parameters) throws Exception;
}
