package com.fsd.admin.service;

import com.fsd.admin.model.A_Employee;
import com.fsd.admin.model.Z_teamcg;
import com.fsd.core.service.BaseService;
import com.fsd.core.util.ParametersUtil;

public interface Z_teamcgService extends BaseService<Z_teamcg, String>{
	/**
	 * 编辑对象
	 * @param obj
	 * @param employee
	 * @throws Exception
	 */
	void save(Z_teamcg obj, A_Employee employee) throws Exception;
    /**
	 * 根据ID加载对象
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	Z_teamcg getObjectById(ParametersUtil parameters) throws Exception;
}
