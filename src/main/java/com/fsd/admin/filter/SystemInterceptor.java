package com.fsd.admin.filter;

import java.io.PrintWriter;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import com.fsd.admin.entity.A_LoginInfo;
import com.fsd.admin.model.A_Employee;
import com.fsd.core.bean.ExceptionInfo;
import com.fsd.core.util.Config;

/**
 * 适配器 - 权限过滤器
 * 
 * @author lumingbao
 */

@Repository
public class SystemInterceptor extends HandlerInterceptorAdapter {

	public @ResponseBody boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = null;

		String url = request.getRequestURI();
		
		// 后台权限过滤
		if (url.indexOf("admin") != -1 && url.indexOf("_qx_") != -1) {
			A_LoginInfo loginInfo = (A_LoginInfo) request.getSession().getAttribute(Config.LOGININFO);
			A_Employee user = loginInfo.getEmployee();
			if (!user.isIsadmin()) {
				String code = url.substring(url.indexOf("_qx_") + 4, url.indexOf(".do"));
				List<String> list = ((A_LoginInfo) request.getSession().getAttribute(Config.LOGININFO)).getPopedomMap().get(Config.POPEDOMMEUN);
				if (list.indexOf(code) == -1) {
					ExceptionInfo ex = new ExceptionInfo();
					ex.setIsNoAuthority(true);
					JSONObject jsonobj = JSONObject.fromObject(ex);
					out = response.getWriter();
					out.append(jsonobj.toString());
					return false;
				}
			}
		}
		return super.preHandle(request, response, handler);
	}
}